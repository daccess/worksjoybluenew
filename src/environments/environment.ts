// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,  
  dialogflow: {  
    angularBot: '5d4bb53fc6a8481e9a420917e879a0a9'  
  },
  firebase: {
    apiKey: "AIzaSyAVWmLn6kzaqZr9csm_NKxHK3QKAGYn5_s",
    authDomain: "ionic-chat-starter-9dcc1.firebaseapp.com",
    databaseURL: "https://ionic-chat-starter-9dcc1.firebaseio.com",
    projectId: "ionic-chat-starter-9dcc1",
    storageBucket: "ionic-chat-starter-9dcc1.appspot.com",
    messagingSenderId: "882978961975",
    appId: "1:882978961975:web:d5b51376c05715c5"
  }
  // firebase : {
  //   apiKey: "AIzaSyDIRN8ERuYpjDz7VcOKr1MwfEvpvnz0ouw",
  //   authDomain: "fir-msg-2d756.firebaseapp.com",
  //   databaseURL: "https://fir-msg-2d756.firebaseio.com",
  //   projectId: "fir-msg-2d756",
  //   storageBucket: "fir-msg-2d756.appspot.com",
  //   messagingSenderId: "22147190875",
  //   appId: "1:22147190875:web:e511c398d3a0e0f6"

  // }
};



/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
