import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrolHeadComponent } from './payrol-head.component';

describe('PayrolHeadComponent', () => {
  let component: PayrolHeadComponent;
  let fixture: ComponentFixture<PayrolHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrolHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrolHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
