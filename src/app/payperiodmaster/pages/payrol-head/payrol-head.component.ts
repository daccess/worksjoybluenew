import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { PayRoleMasterService } from '../../../shared/services/PayRoleHeadMasterService';
import { PayRoleMaster } from '../../../shared/model/PayRoleHeadmodel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

class addcompanym {
  compId : number
};

@Component({
  selector: 'app-payrol-head',
  templateUrl: './payrol-head.component.html',
  styleUrls: ['./payrol-head.component.css']
})
export class PayrolHeadComponent implements OnInit {
  addcompany: addcompanym;
  
  PayRolemodel: PayRoleMaster;
  selectedCompanyobj: any
  companydata: any;
  public selectUndefinedOptionValue:any;
  baseurl = MainURL.HostUrl;
  compId: any;
  StatusFlag:boolean=false
  // compdata = [];
 AllPayRolHead: any;
  isedit = false;
  Selectededitobj: any;
  selectedComp:Number;
  updatecompany: any;
  loggedUser: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  dltmodelFlag: boolean;
  dltObje: any;
  loconame: String;

  constructor(public httpService: HttpClient, 
    public PayRoleservice: PayRoleMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService
    ) { 

    this.addcompany = new addcompanym();
    this.PayRolemodel = new PayRoleMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId
    this. getallPayRoll();
  }

  ngOnInit() {

    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
        this.companydata = data["result"];
      },(err: HttpErrorResponse) => {
      });
    this. getallPayRoll();
  }

  
  error(){
    if(this.loconame != this.PayRolemodel.payRollHeadName){
    let obj={
      compId :this.selectedCompanyobj,
      payRollHeadName:this.PayRolemodel.payRollHeadName
    }
        let url = this.baseurl + '/payrollhead';
        this.httpService.post(url,obj).subscribe((data :any) => {
           this.errordata= data["result"];
           this.msg= data.message
           this.toastr.error(this.msg);
           this.errflag = true;
          },
          (err: HttpErrorResponse) => {
            this.errflag = false;
          });
      }
    }
  onSubmit(f){
    this.PayRolemodel.status=this.flag1;
    this.StatusFlag=false
     if(this.isedit == false){
    this.PayRolemodel.compId = this.selectedCompanyobj;
    this.PayRoleservice.postPayRolMaster(this.PayRolemodel).subscribe(data => {
      f.reset();
      this.resetForm()
      this.toastr.success('PayRolHead-Master Information Inserted Successfully!', 'PayRolHead-Master');
      this. getallPayRoll();
    },(err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'PayRolHead-Master');
    });
  }else{ 
    let url1 = this.baseurl + '/PayRollHead';
  this.PayRolemodel.compId = this.selectedCompanyobj;
  this.PayRolemodel.payRollHeadsId = this.Selectededitobj;
  this.httpService.put(url1,this.PayRolemodel).subscribe(data => {
    f.reset();
    this.resetForm()
    this.toastr.success('PayRolHead-Master Information Updated Successfully!', 'PayRolHead-Master');
    this. getallPayRoll();
    this.isedit=false
  },(err: HttpErrorResponse) => {
    this.isedit=false;
    this.toastr.error('Server Side Error..!', 'PayRolHead-Master');
  });
    
  }
} 

  ngValue(event){
     this.selectedCompanyobj = parseInt(event.target.value);
  }

  dataTableFlag : boolean  = true
  getallPayRoll(){
    let url1 = this.baseurl + '/PayRollHead/';
    this.dataTableFlag  = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
       this.AllPayRolHead = data["result"];
       this.dataTableFlag  = true
       this.chRef.detectChanges(); 
       setTimeout(function ()
        {
         $(function () {
           var table = $('#PayRolTable').DataTable();
         });
       }, 1000);
       
       this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
    this.resetForm();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.payRollHeadsId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deletePayRollhead/';
        this.PayRolemodel.payRollHeadsId= this.dltObje;
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.toastr.success('Delete Successfully', 'PayRolHead-Master');
          this. getallPayRoll();
        },(err: HttpErrorResponse) => {
          this.toastr.error('Server Side Error..!', 'PayRolHead-Master');
        });
      }
  

  }
  edit(object){
    this.Selectededitobj = object.payRollHeadsId;
    this.isedit = true;
   this.editobject(this.Selectededitobj)
   this.StatusFlag=true 
   this.flag1 = object.status;
  }
  flag1 : boolean = false;
  editobject(getobject){

    let urledit = this.baseurl + '/PayRollHead/getById/';
    this.httpService.get(urledit + getobject).subscribe(data => {
        this.PayRolemodel.status = data['result']['status'];
        this.PayRolemodel.payRollHeadName = data['result']['payRollHeadName'];
        this.PayRolemodel.payRollHeadDesc = data['result']['payRollHeadDesc'];
        this.PayRolemodel.payRollHeadType = data['result']['payRollHeadType'];
        var comp_id = data['result']['companyMaster']['compId'];
        this.loconame = this.PayRolemodel.payRollHeadName

        for(var i=0;i<this.companydata.length;i++){
           if(this.companydata[i].compId == comp_id){
             this.selectedCompanyobj =  comp_id;
           }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }
  resetForm(){
    setTimeout(() => {
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
    }, 100);
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}