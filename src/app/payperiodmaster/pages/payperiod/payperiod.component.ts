import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { PayPeriodMaster } from 'src/app/shared/model/payPeriodMaster';
import { PayPeriodMasterService } from '../../../shared/services/payPeriodMaster.service'
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-payperiod',
  templateUrl: './payperiod.component.html',
  styleUrls: ['./payperiod.component.css']
})
export class PayperiodComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  allActiveData: any;
  PayPeriodMasterModel: PayPeriodMaster;
  selectedItems: any[];
  dropdownSettings: {};
  selectedLocation = [];
  Selectededitobj: any;
  isedit: boolean = false;
  locodata: any;
  compId: string;
  loggedUser: any;
  payPeriodName: String;
  errordata: any;
  msg: any;
  errflag: boolean;
  dltmodelFlag: boolean;
  dltObje: any;
  loconame: String;
  selectedEmpTypeObj: any;
  Employeedataall: any;
  DaysArray = []
  selectUndefinedOptionValue : any
  selectedStartDate: number;
  selectedEndDate: number;
  diffrenceDate: number;
  diff = false;
  locationFlag: boolean = false;
  roleHead: any;
  locationdata: any;


  constructor(public httpService: HttpClient, public PayPeriod: PayPeriodMasterService, private toastr: ToastrService, public chRef: ChangeDetectorRef) {
    this.cancleButtonFalg = false;
    this.PayPeriodMasterModel = new PayPeriodMaster();
    this.PayPeriodMasterModel.startDate = 1;
    this.PayPeriodMasterModel.endDate = 31;
    this.selectedEmpTypeObj = "";
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.roleHead = this.loggedUser.mrollMaster.roleName;
   
    this.getemployee();

    for (let i = 1; i <= 31; i++) {
      this.DaysArray.push(i);
    }


  }


  ngOnInit() {
    

    if( this.roleHead=='HR'){
    
      let url = this.baseurl + '/Location/Active/'+this.compId;
      this.httpService.get(url).subscribe(data => {
          this.locationdata = data["result"];
        //console.log("locationwisedata***", this.locationdata );
      },
      (err: HttpErrorResponse) => {
      });
    }  else if(this.roleHead=='Admin'){
      let url = this.baseurl + '/Location/Active/'+this.compId;
      this.httpService.get(url).subscribe(data => {
          this.locationdata = data["result"];
       // console.log("locationwisedata***", this.locationdata );
      },
      (err: HttpErrorResponse) => {
      });

    }
    this.getallPaypeirod();
    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    
  }

  error() {
    if (this.loconame != this.PayPeriodMasterModel.payPeriodName) {
      let obj = {
        compId: this.compId,
        payPeriodName: this.PayPeriodMasterModel.payPeriodName
      }
      let url = this.baseurl + '/payperiodSetting';
      this.httpService.post(url, obj).subscribe(
        (data: any) => {
          this.errordata = data["result"];
          this.msg = data.message
          this.toastr.error(this.msg);
          this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        });
    }
  }

  dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.resetFun()
    this.PayPeriodMasterModel.selectedLocation = [];


  }
  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.payPeriodSettingId;
    this.dltObjfun(this.dltObje)
  }
  dltObjfun(object) {

    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/deletepayperiod/';
      this.PayPeriodMasterModel.payPeriodSettingId = this.dltObje;
      this.httpService.delete(url1 + this.dltObje)
        .subscribe(data => {
          this.toastr.success('Delete Successfully', 'Pay-Period-Master');
          this.getallPaypeirod();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Server Side Error..!', 'Pay-Period-Master');
        });
    }


  }
  StatusFlag: boolean = false
  onSubmit(f) {
    this.StatusFlag = false
    this.PayPeriodMasterModel.status = this.flag1;
    if (this.isedit == false) {
      this.PayPeriodMasterModel.compId = parseInt(this.compId)
      this.PayPeriodMasterModel.selectedLocation = this.selectedLocation;
      console.log(">>>>>>>>>>>>>>>>>>>>>", this.PayPeriodMasterModel);
      this.PayPeriod.postPayPeriodMaster(this.PayPeriodMasterModel)
        .subscribe(data => {
          f.reset();
          this.resetForm();
          this.toastr.success('Pay Period Master Information Inserted Successfully!', 'PayPeriod-Master');
          this.chRef.detectChanges();
          this.PayPeriodMasterModel.selectedLocation = [];
          this.getallPaypeirod();
          // this.PayPeriodMasterModel.cycleType = "Month Start Date"
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Server Side Error..!', 'PayPeriod-Master');
        });

    }
    else {
      let urld = this.baseurl + '/PayPeriodSetting';
      this.PayPeriodMasterModel.payPeriodSettingId = this.Selectededitobj;
      this.PayPeriodMasterModel.compId = parseInt(this.compId)
      this.httpService.put(urld, this.PayPeriodMasterModel)
        .subscribe(data => {
          this.chRef.detectChanges();
          f.reset();
          this.resetForm();
          this.toastr.success('PayPeriod-Master Information Updated Successfully!', 'Pay-Period-Master');
          this.chRef.detectChanges();
          this.getallPaypeirod();
          this.PayPeriodMasterModel.selectedLocation = [];
          this.isedit = false
        },
          (err: HttpErrorResponse) => {
            this.isedit = false;
            this.toastr.error('Server Side Error..!', 'Pay-Period-Master');
          });
    }
  }
  dataTableFlag: boolean = true
  getallPaypeirod() {
    let url = this.baseurl + '/PayPeriodSetting/';
    this.dataTableFlag = false
    this.httpService.get(url + this.compId).subscribe(
      data => {
        this.chRef.detectChanges();
        this.locodata = data["result"];
        this.dataTableFlag = true
        this.chRef.detectChanges();
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable();

          });
        }, 1000);

        this.chRef.detectChanges();

      },
      (err: HttpErrorResponse) => {
      }
    );
  }

  edit(object) {
    this.Selectededitobj = object.payPeriodSettingId;
    this.isedit = true;
    this.StatusFlag = true;
    this.flag1 = object.status;
    this.editobject(this.Selectededitobj);
  }
  flag1: boolean = false;
  editobject(Selectededitobj) {
    let urledit = this.baseurl + '/PayPeriodSetting/getById/';
    this.httpService.get(urledit + Selectededitobj).subscribe(
      data => {
        this.PayPeriodMasterModel.status = data['result']['status'];
        this.PayPeriodMasterModel.payPeriodName = data['result']['payPeriodName'];

        this.PayPeriodMasterModel.endDate = data['result']['endDate'];

        this.PayPeriodMasterModel.selectedLocation = data['result']['selectedLocation'];
        this.PayPeriodMasterModel.payRollProcessingDay = data['result']['payRollProcessingDay'];
        this.PayPeriodMasterModel.startDate = data['result']['startDate'];
        this.loconame = this.PayPeriodMasterModel.payPeriodName;
        this.selectedEmpTypeObj = data['result']['empTypeId'];
        this.PayPeriodMasterModel.empTypeId = data['result']['empTypeId'];
      },
      (err: HttpErrorResponse) => {
      });

  }
  uiswitch(event) {

    this.flag1 = !this.flag1;
  }


  onItemSelect(item: any) {
    if (this.selectedLocation.length == 0) {
      this.selectedLocation.push(item)
    } else {
      this.selectedLocation.push(item);
    }
  }
  onSelectAll(items: any) {
    this.selectedLocation = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedLocation.length; i++) {
      if (this.selectedLocation[i].locationId == item.locationId) {
        this.selectedLocation.splice(i, 1)
      }
    }
  }
  onDeSelectAll(item: any) {
    this.selectedLocation = [];
  }
  selectClickLocation() {
   
    
    if (this.PayPeriodMasterModel.selectedLocation.length == 0) {
      this.locationFlag = true;
    }
    else {
      this.locationFlag = false;
    }
  }
  






  EmpValue(event) {
    this.selectedEmpTypeObj = parseInt(event.target.value);
    this.PayPeriodMasterModel.empTypeId = this.selectedEmpTypeObj;
  }

  getemployee() {
    let testempactive = this.baseurl + '/EmployeeType/Active/';
    this.httpService.get(testempactive + this.compId).subscribe(
      data => {
        this.Employeedataall = data["result"];
      },
      (err: HttpErrorResponse) => {
      });

  }

  resetForm() {

    setTimeout(() => {
      this.PayPeriodMasterModel.startDate = 1
      this.PayPeriodMasterModel.endDate = 31
      this.PayPeriodMasterModel.payRollProcessingDay = this.selectUndefinedOptionValue
      
    }, 100);

  }

  onChange(event){
    this.selectedStartDate = 31 - event;
  }

  onChangeendDate(event){
    this.selectedEndDate = this.selectedStartDate + parseInt(event);
    this.diffrenceDate = this.selectedEndDate + 1;
    if(this.diffrenceDate > 31 || this.diffrenceDate < 30){
      this.diff = true;
    }
    else{
      this.diff = false;
    }
  }

  resetFun() {
    this.isedit = false
    this.StatusFlag= false;
    this.PayPeriodMasterModel.empTypeId=null;
    this.selectedEmpTypeObj = "";
    setTimeout(() => {
      this.PayPeriodMasterModel.startDate = 1;
      this.PayPeriodMasterModel.endDate = 31;
      this.PayPeriodMasterModel.payRollProcessingDay = this.selectUndefinedOptionValue;
    }, 100);
    this.PayPeriodMasterModel = new PayPeriodMaster();
  }
  cancleButtonFalg: boolean = false
  getobj(i) {
    this.getPayPeriodgetId(i.payPeriodSettingId);
  }
  clearCancle() {
    this.cancleButtonFalg = false;
   this.resetFun();
  }

  getPayPeriodgetId(payPeriodSettingId) {
    let getId = this.baseurl + '/PayPeriodSetting/getById/';
    this.httpService.get(getId + payPeriodSettingId).subscribe((data: any) => {
        this.cancleButtonFalg = true;
        this.PayPeriodMasterModel.payPeriodName = data.result.payPeriodName;
        this.PayPeriodMasterModel.endDate = data.result.endDate;
        this.PayPeriodMasterModel.startDate = data.result.startDate;
        this.PayPeriodMasterModel.payRollProcessingDay = data.result.payRollProcessingDay;
        this.PayPeriodMasterModel.selectedLocation = data.result.selectedLocation;
        this.PayPeriodMasterModel.empTypeId=data.result.empTypeId;
        this.selectedEmpTypeObj = data.result.empTypeId;
      })
  }
  
}
