import { routing } from '../../app/payperiodmaster/payperiodmaster.routing';
import { NgModule } from '@angular/core';
 
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';

import { PayperiodmasterComponent } from 'src/app/payperiodmaster/payperiodmaster.component';
import { PayperiodComponent } from 'src/app/payperiodmaster/pages/payperiod/payperiod.component';
import { PayrolHeadComponent } from './pages/payrol-head/payrol-head.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { PayPeriodMasterService } from '../shared/services/payPeriodMaster.service';
import { PayRoleMasterService } from '../shared/services/PayRoleHeadMasterService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';




@NgModule({
    declarations: [
        PayperiodmasterComponent,
        PayperiodComponent,
        PayrolHeadComponent
    
    ],
    imports: [
    routing,
    UiSwitchModule,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule
    ],
    providers: [PayRoleMasterService,PayPeriodMasterService]
  })
  export class PayperiodModule { 
      constructor(){

      }
  }
  