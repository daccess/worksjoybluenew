import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { PayperiodmasterComponent } from 'src/app/payperiodmaster/payperiodmaster.component'
import { PayperiodComponent } from 'src/app/payperiodmaster/pages/payperiod/payperiod.component';
import { PayrolHeadComponent } from './pages/payrol-head/payrol-head.component';

const appRoutes: Routes = [
    { 
        path: '', component: PayperiodmasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'payperiodmaster',
                    pathMatch :'full'
                    
                },
                {
                    path:'payperiodmaster',
                    component: PayperiodComponent
                },
                {
                    path:'payrolhead',
                    component: PayrolHeadComponent
                }
            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);