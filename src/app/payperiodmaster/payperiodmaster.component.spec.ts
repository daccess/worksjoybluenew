import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayperiodmasterComponent } from './payperiodmaster.component';

describe('PayperiodmasterComponent', () => {
  let component: PayperiodmasterComponent;
  let fixture: ComponentFixture<PayperiodmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayperiodmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayperiodmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
