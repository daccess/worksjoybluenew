import { ContractorPresentReportModule } from './contractor-present-report.module';

describe('ContractorPresentReportModule', () => {
  let contractorPresentReportModule: ContractorPresentReportModule;

  beforeEach(() => {
    contractorPresentReportModule = new ContractorPresentReportModule();
  });

  it('should create an instance', () => {
    expect(contractorPresentReportModule).toBeTruthy();
  });
});
