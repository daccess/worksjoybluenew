import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContractorPresentReportComponent } from './contractor-present-report.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
const routes: Routes = [
  {
    path: '', component: ContractorPresentReportComponent,
    children:[
      {
        path:'',
        redirectTo : 'contractorpresentreport',
        pathMatch :'full'
        
      }]
  }
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ContractorPresentReportComponent]
})
export class ContractorPresentReportModule { }
