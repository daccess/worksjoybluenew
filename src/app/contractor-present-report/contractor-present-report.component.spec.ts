import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorPresentReportComponent } from './contractor-present-report.component';

describe('ContractorPresentReportComponent', () => {
  let component: ContractorPresentReportComponent;
  let fixture: ComponentFixture<ContractorPresentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorPresentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorPresentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
