import { Component, OnInit } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';

@Component({
  selector: 'app-contractor-present-report',
  templateUrl: './contractor-present-report.component.html',
  styleUrls: ['./contractor-present-report.component.css']
})
export class ContractorPresentReportComponent implements OnInit {
  toDate: string;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  report_data: any;
  marked_sr=true;
  marked_nature=true;
  marked_name=true;
  marked_address=true;
  marked_contract_type=true;
  marked_period_from=true;
  marked_period_to=true;
  marked_manpower=true;
  marked_licence_from=true;
  marked_licence_to=true;
  marked_wc=true;
  marked_epf=true;
  marked_esic=true;
  marked_gst=true;
  marked_contact=true;
  report_time: string;
  total_month_days: number;
  constructor(public toastr: ToastrService, private datePipe: DatePipe,private InfoService:InfoService,private router: Router) {  // this.fromDate = sessionStorage.getItem("fromDate");
    // this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        
        this.report_data = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
}

  ngOnInit() {
    for(let i=0;i<this.report_data.length;i++){
      this.report_data[i].Sr=i+1;
    }
  }
  toggle_sr(e) {
    this.marked_sr = e.target.checked;
  }
  toggle_emp_id(e) {
    this.marked_nature = e.target.checked;
  }
  toggle_name(e) {
    this.marked_name = e.target.checked;
  }
  toggle_address(e){
    this.marked_address = e.target.checked;
  }
  
//convert timestamp to date
TimestamptoDate(timestamp){
  var dateString = new Date(timestamp);
  let temp_Date=moment(dateString).format("YYYY-MM-DD");
  return temp_Date;
}
  //Excel Export
  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename='Contactor-Register-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  //pdf
  capturePdf() {
    let item_sr = {
      header: "Sr.No"
    }
    let name = {
      header: "Name Of The Contractor"
    }
    let totalCon={
      header: "Total Contractor"
    }
    let Presents = {
      header: "Presents Contractor"
    }
   
    var doc = new jsPDF('landscape', 'px', 'a4');
    // doc.addPage();
    var pageCount = doc.internal.getNumberOfPages();
    for (let i = 0; i < pageCount; i++) {
      doc.setPage(i);
      doc.setTextColor(48, 80, 139)
      doc.setFontSize(10)
      doc.text(580, 12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
    }
    var col = [];
    if (this.marked_sr) {
      col.push(item_sr);
    }
    if (this.marked_name) {
      col.push(name);
    }
    if (this.marked_address) {
      col.push(totalCon);
    }
    if (this.marked_nature) {
      col.push(Presents);
    }
    
    var rows = [];
    var rowCountModNew = this.report_data;
    rowCountModNew.forEach(element => {
      let obj = []
      if (this.marked_sr) {
        obj.push(element.Sr);
      }
      if (this.marked_name) {
        obj.push(element.contractorFirmName);
      }
      if (this.marked_address) {
        obj.push(element.totalContractorCount);
      }
      if (this.marked_nature) {
        obj.push(element.presentContractorCount);
      }
     
      rows.push(obj);
    });

    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14, 'Company Name :' + this.compName)

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12)
    doc.setFontStyle("Arial")
    doc.text(280, 14, 'Contractor Presents Report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    // doc.text(263, 24, 'From date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") + ' ' + 'To date' + ' : ' + this.datePipe.transform(this.toDate, "dd-MMM-yyyy")
    // )

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44, 'Report date' + ' : ' + this.datePipe.transform(this.todaysDate, "dd-MMM-yyyy hh:mm:ss a"))

    doc.autoTable(col, rows, {
      columnStyles: {
        1: {halign:'left'},
        3: {halign:'left'},
        4: {halign:'left'},
        5: {halign:'left'},
      },
      tableLineColor: [190, 191, 191],
      tableLineWidth: 0.5,
      headStyles: {
        fillColor: [103, 132, 130],
      },
      styles: {
        halign: 'center',
        cellPadding: 0.5, fontSize: 6
      },
      theme: 'grid',
      pageBreak:'avoid',
      margin: { top: 60,  bottom: 50 }
    });
    doc.save('Contractor Presents Report' + this.datePipe.transform(this.todaysDate, "dd-MM-yyyy hh:mm:ss") + '.pdf');
  }
}
