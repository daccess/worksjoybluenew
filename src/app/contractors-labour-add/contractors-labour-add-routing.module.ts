import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorsLabourAddComponent } from './contractors-labour-add.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorsLabourAddComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorLabourAdd',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorLabourAdd',
                  component: ContractorsLabourAddComponent
              }

          ]

  }


]
export const addcontractor = RouterModule.forChild(appRoutes);

