import { ContractorsLabourAddModule } from './contractors-labour-add.module';

describe('ContractorsLabourAddModule', () => {
  let contractorsLabourAddModule: ContractorsLabourAddModule;

  beforeEach(() => {
    contractorsLabourAddModule = new ContractorsLabourAddModule();
  });

  it('should create an instance', () => {
    expect(contractorsLabourAddModule).toBeTruthy();
  });
});
