import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { addcontractor } from './contractors-labour-add-routing.module';
import { ContractorsLabourAddComponent } from './contractors-labour-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    addcontractor,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ContractorsLabourAddComponent
  ]
})
export class ContractorsLabourAddModule { }
