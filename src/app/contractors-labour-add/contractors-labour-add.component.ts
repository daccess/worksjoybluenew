import { Component, OnInit, OnDestroy } from "@angular/core";
import { MainURL } from "../shared/configurl";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { departmentMasterService } from "../shared/services/departmentmasterservice";
import { RoleMasterService } from "../shared/services/RoleMasterService";
import { Router } from "@angular/router";

@Component({
  selector: "app-contractors-labour-add",
  templateUrl: "./contractors-labour-add.component.html",
  styleUrls: ["./contractors-labour-add.component.css"],
})
export class ContractorsLabourAddComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  empId: any;
  firstName: any;
  middleName: any;
  lastName: any;
  isChecked:boolean=false;
  gender: any;
  bloodGroup: any;
  contactNumber: any;
  emergencyContactNo: any;
  public areDocumentsUploaded: boolean = false;
  tempAddress: any;
  permanentAddress: any;
  maritalStatus: any;
  aadharNo: any;
  panNumber: any;
  voterCardNo: any;
  
  user: string;
  users: any;
  token: any;
  empIds: any;
  imageUrl: string;
  employeeName: any;
  employeelastname: any;

  DesignationList: any;
  getallDepartment: any;
  allLocations: any;
  fileToUpload: File = null;
  obj1: any = {};
  obj2: any = {};
  obj4: any = {};
  obj3: any = {};
  obj: any = {};
  aadharImg: any;
  experienceLetter: any;
  panCardImg: any;
  receiptUrl1: any;
  receiptUrl4: any;
  policeVerificationDoc: any;
  receiptUrl3: any;
  receiptUrl2: any;
  imagevalidation1: any;
  imagevalidation2: string;
  imagevalidation3: string;
  imagevalidation4: string;
  labourMasterId: any;
  accurrenceDetails: any;
  assetHistories: any = [];
  assetsname: any;

  currentAge: number;
  /*new variable here*/
  pfNumber: any;
  esicNumber: any;
  wcNumber: any;
  policeVerificationDocExpiryDate: any;
  aadharUploadDate: any;
  panUpdateDate: any;
  experienceUpdateDate: any;
  previousEmployer: any;
  jobTitle: any;
  accountNumber:any;
  ifscCode:any;
  bankName:any
  wage: any;
  workedStartedDate: any;
  workedCompletedDate: any;
  jobType: any;
  jobDuties: any;
  employmentType: any;
  toolUsed: any;
  reasonForLeaving: any;
  fresher:any;
  emailId: any;
  workHistoryList: any[] = [];
  dateOfBirth: any;
  conMasterId: any;
  roleId: any;
  conEmpId: any;
  userRoleList: any;
  userRoleLists: any;
  labourID: any;
  allLabourDataEdit: any;
  documentMaster: any = {};
  getSingleLabourList: any;
  labourUpdate: any;
  receiptUrl: any;
  imagevalidation: string;
  locationId: any;
  desigId: any;
  deptId: any;
  contractorForm: any;
  today: Date;
  age: any;
  AllRolesList: any;
  AllRoleslists: any;
  relation: any;
  name: any;
  relationage: any;
  Pgender: any;
  constructor(
    public httpservice: HttpClient,
    public toastr: ToastrService,
    public departmentmasterservice: departmentMasterService,
    public roleservice: RoleMasterService,
    private router:Router
  ) {
    this.today=new Date();
  }

  ngOnInit() {
    this.user = sessionStorage.getItem("loggeduser");
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empIds = this.users.role;
    this.conEmpId = this.users.roleList.empId;
    this.employeeName = this.users.roleList.firstName;
    this.employeelastname = this.users.roleList.lastName;

    this.getDesignation();
    this.getDepartment();
    this.getAllLocation();
    // this.getLabourID()
    this.getallroleid();
    this.getAllRoles();
    this.labourID = sessionStorage.getItem("labourId");

    this.labourUpdate = sessionStorage.getItem("iseditLabour");
    if (this.labourUpdate == "true") {
      
      this.getLabourById();
    }
  }
  ngOnDestroy() {
    sessionStorage.removeItem("labourId");
  }
  getDesignation() {
    let url = this.baseurl + "/getAllDesignations";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(
      (data) => {
        this.DesignationList = data["result"];
      },
      (err: HttpErrorResponse) => {}
    );
  }

  getDepartment() {
    let url = this.baseurl + "/getAllDepartments";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(
      (data) => {
        this.getallDepartment = data["result"];
      },
      (err: HttpErrorResponse) => {}
    );
  }
 
  onCheckboxChange() {
    console.log("Checkbox value:", this.isChecked);
    this.fresher = this.isChecked;
  }
  collapsenext() {

    var toggleButton = document.getElementById('toggleButton');
    var collapseElement = document.getElementById('collapse-2');

    toggleButton.addEventListener('click', function () {
      var isExpanded = collapseElement.classList.contains('show');
      toggleButton.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    });
  }
  
  getAllLocation() {
    let url = this.baseurl + "/getAllLocations";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(
      (data) => {
        this.allLocations = data["result"];
      },
      (err: HttpErrorResponse) => {}
    );
  }
  getAllRoles() {
    let url = this.baseurl + "/getRolesAddUser";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(
      (data) => {
        this.AllRolesList = data
        this.AllRoleslists=this.AllRolesList.result
      },
      (err: HttpErrorResponse) => {}
    );
  }
  // getAllassets() {

  //   let url = this.baseurl + '/getAllAssets';
  //   const tokens = this.token;
  //   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.get(url, { headers }).subscribe(data => {

  //     this.Allassets = data['result']
  //   },
  //     (err: HttpErrorResponse) => {

  //     })

  // }
  // getLabourID() {

  //   let url = this.baseurl + '/getMaxLabourId';
  //   const tokens = this.token;
  //   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.get(url, { headers }).subscribe(data => {
  //     console.log("shafds",data)
  // this.labourMasterId=data;
  // this.empId=this.labourMasterId.result.labourEmpId
  //   },
  //     (err: HttpErrorResponse) => {

  //     })

  // }
  getallroleid() {
    this.roleservice.GetAllRoles1(this.token).subscribe((data) => {
      // this.allRolesData=data['result'];
      console.log("this is the data", data);

      this.userRoleLists = data.result;
      
      // this.roleId = this.userRoleLists.userRoleList[0].roleId;
      console.log("testing", this.roleId);
    });
  }
  getassetsname(e: any) {
    this.assetsname = e.target.assetName;
  }
  // addAssets() {
  //   let obj: any = {
  //     "issuedReason": this.issuedReason,
  //     "issuedate": this.issuedate,
  //     "assetsName": this.assetsname
  //   }
  //   this.assetHistories.push(obj)
  //   console.log("sdfasdf", this.assetHistories)
  //   obj = ''
  // }

  handleFileInput1(file: FileList) {
    this.fileToUpload = file.item(0);
    this.obj1.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.obj1.name = file.item(0).name;
      this.aadharImg = event.target.result.substring(0, 10); // This sets the `aadharImg` to the first 10 characters of the file data.

      // this.aadharImg = event.target.result.substring(0, 10);;
      this.receiptUrl1 = event.target.result;
      this.aadharImg = file.item(0).name;
    };
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        } else {
          flag1 = false;
        }
      };

      setTimeout(() => {
        if (flag1) {
          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.aadharImg = event.target.result;
          };
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation1 = "false";
        } else {
          this.toastr.error("Please upload image upto 1 MB");
          this.imagevalidation1 = "true";
        }
      }, 300);
    }
    this.areDocumentsUploaded = !!this.aadharImg;
  }
  handleFileInput2(file: FileList) {
    this.fileToUpload = file.item(0);
    this.obj2.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.panCardImg = event.target.result.substring(0, 10);
      this.receiptUrl2 = event.target.result;
      this.obj2.name = file.item(0).name;
    };
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        } else {
          flag1 = false;
        }
      };

      setTimeout(() => {
        if (flag1) {
          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.panCardImg = event.target.result;
          };
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation2 = "false";
        } else {
          this.toastr.error("Please upload image upto 1 MB");
          this.imagevalidation2 = "true";
        }
      }, 300);
    }
    this.areDocumentsUploaded = !!this.panCardImg;
  }

  handleFileInput3(file: FileList) {
    this.fileToUpload = file.item(0);
    this.obj3.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.policeVerificationDoc = event.target.result.substring(0, 10);
      this.receiptUrl3 = event.target.result;
      this.obj3.name = file.item(0).name;
    };
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        } else {
          flag1 = false;
        }
      };

      setTimeout(() => {
        if (flag1) {
          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.policeVerificationDoc = event.target.result;
          };
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation3 = "false";
        } else {
          this.toastr.error("Please upload image upto 1 MB");
          this.imagevalidation3 = "true";
        }
      }, 300);
    }
    this.areDocumentsUploaded = !!this.policeVerificationDoc;
  }
  // Add this method to your component class
areAllDocumentsUploaded(): boolean {
  return !!this.aadharImg && !!this.policeVerificationDoc && !!this.experienceLetter;
}

handleFileInput(files: FileList) {
  const maxSizeInBytes = 1 * 1024 * 1024; // 1MB
  var flag1 = "false";
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (file.size > maxSizeInBytes) {
      console.log(`File ${file.name} exceeds 1MB`);
      // Additional actions, such as displaying an error message, can be performed here
      flag1 = "false";
    } else {
      console.log(`File ${file.name} is valid`);
      flag1 = "true";
      // Further processing, such as uploading the image, can be done here
    }
  }
  this.fileToUpload = files.item(0);

  this.obj.name = files.item(0).name;
  //Show image preview
  var reader = new FileReader();
  reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
    this.receiptUrl = event.target.result;
  };
  reader.readAsDataURL(this.fileToUpload);
  // let flag1 = false;
  if (files) {
    var img = new Image();
    img.src = window.URL.createObjectURL(files[0]);
    setTimeout(() => {
      if (flag1 == "true") {
        this.imagevalidation = "false";
        this.fileToUpload = files.item(0);
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.imageUrl = event.target.result;
        };
        reader.readAsDataURL(this.fileToUpload);
      } else {
        // this.toastr.error("Please upload image upto 1 MB")
        this.imagevalidation = "true";
      }
      console.log("this is image validation status", this.imagevalidation);
    }, 300);
  }
}

  handleFileInput4(file: FileList) {
    this.fileToUpload = file.item(0);
    this.obj4.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.experienceLetter = event.target.result.substring(0, 10);
      this.receiptUrl4 = event.target.result;
      this.obj4.name = file.item(0).name;
    };
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        } else {
          flag1 = false;
        }
      };

      setTimeout(() => {
        if (flag1) {
          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.experienceLetter = event.target.result;
          };
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation4 = "false";
        } else {
          this.toastr.error("Please upload image upto 1 MB");
          this.imagevalidation4 = "true";
        }
      }, 300);
    }
    this.areDocumentsUploaded = !!this.experienceLetter;
  }
  AddworkHistoryList() {
    let obje: any = {
      previousEmployer: this.previousEmployer,
      jobTitle: this.jobTitle,
      wage: this.wage,
      workedStartedDate: this.workedStartedDate,
      workedCompletedDate: this.workedCompletedDate,
      jobType: this.jobType,
      jobDuties: this.jobDuties,
      employmentType: this.employmentType,
      toolUsed: this.toolUsed,
      reasonForLeaving: this.reasonForLeaving,
    };
    this.workHistoryList.push(obje);
    obje = "";
  }
  addDocumenthere() {
    
    let obj = {
      aadharImg: this.aadharImg,
      panCardImg: this.panCardImg,
      policeVerificationDoc: this.policeVerificationDoc,
      experienceLetter: this.experienceLetter,
      policeVerificationDocExpiryDate: this.policeVerificationDocExpiryDate,
      // "aadharUploadDate":
      // "panUpdateDate":"2023-06-20",
      // "experienceUpdateDate":"2023-06-20"
      
    };
   
   
    this.documentMaster = obj;
  }

  saveContractorLabours() {
    let obj = {
      roleId: this.roleId,
      // "empId": this.empId,
      firstName: this.firstName,
      middleName: this.middleName,
      lastName: this.lastName,
      gender: this.gender,
      bloodGroup: this.bloodGroup,
      contactNumber: this.contactNumber,
      emergencyContactNo: this.emergencyContactNo,
      emailId: this.emailId,
      tempAddress: this.tempAddress,
      permanentAddress: this.permanentAddress,
      maritalStatus: this.maritalStatus,
      aadharNo: this.aadharNo,
      panNumber: this.panNumber,
      voterCardNo: this.voterCardNo,
      pfNumber: this.pfNumber,
      esicNumber: this.esicNumber,
      wcNumber: this.wcNumber,
      accountNumber: this.accountNumber,
      ifscCode: this.ifscCode,
      bankName: this.bankName,
      
      documentMaster: this.documentMaster,
      workHistoryList: this.workHistoryList,
      assetHistories: this.assetHistories,
      dateOfBirth: this.dateOfBirth,
      conEmpId: this.conEmpId,
      labourImage: this.imageUrl,
      age: this.currentAge,
      locationId: this.locationId,
      desigId: this.desigId,
      deptId: this.deptId,
      fresher:this.fresher,
      relation:this.relation,
      name:this.name,
      relationage:this.age,
      Pgender:this.gender

    };
    let url = this.baseurl + "/createLabourInfo";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    // this.httpservice.post(url,obj, { headers }).subscribe(data => {

    //   // this.allLocations = data['result']
    //   this.toastr.success("Contractor's Labour Inserted Successfully")

    // },
    //   (err: HttpErrorResponse) => {
    //     this.toastr.error("server side Error while add Contractor Labour")

    //   })
    // }

    this.httpservice.post(url, obj, { headers }).subscribe(
      (data) => {
        console.log("Response Data:", data);
        this.toastr.success("Contractor's Labour Inserted Successfully");
        this.resetForm(); // Call the resetForm function after success
        
        // Reload the page
        // window.location.reload();
        this.router.navigateByUrl('/layout/contractorLabourList')
      },
      (err: HttpErrorResponse) => {
        console.error("Error Response:", err);
        this.toastr.error("Server side Error while adding Contractor Labour");
      }
    );
  }

  updatecontractor() {
    let obj11 = {
      roleId: this.roleId,
      // "empId": this.empId,
      firstName: this.firstName,
      middleName: this.middleName,
      lastName: this.lastName,
      gender: this.gender,
      bloodGroup: this.bloodGroup,
      contactNumber: this.contactNumber,
      emergencyContactNo: this.emergencyContactNo,
      emailId: this.emailId,
      tempAddress: this.tempAddress,
      permanentAddress: this.permanentAddress,
      maritalStatus: this.maritalStatus,
      aadharNo: this.aadharNo,
      panNumber: this.panNumber,
      voterCardNo: this.voterCardNo,
      pfNumber: this.pfNumber,
      esicNumber: this.esicNumber,
      wcNumber: this.wcNumber,
      accountNumber: this.accountNumber,
      ifscCode: this.ifscCode,
      bankName: this.bankName,
      documentMaster: this.documentMaster,
      workHistoryList: this.workHistoryList,
      assetHistories: this.assetHistories,
      dateOfBirth: this.dateOfBirth,
      conEmpId: this.conEmpId,
      desigId: this.desigId,
      labourMasterId: this.labourMasterId,
      labourImage: this.imageUrl,
      age: this.currentAge,
      labourPerId: this.labourID,
      locationId: this.locationId,
      fresher:this.fresher,
      
      relation:this.relation,
      name:this.name,
      relationage:this.age,
      Pgender:this.gender
    };
    let url = this.baseurl + "/updateLabourInfo";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.put(url, obj11, { headers }).subscribe(
      (data) => {
        this.toastr.success("Contractor's Labour updated Successfully");
        this.resetForm(); // Call the resetForm function after success
        
        // Reload the page
        // window.location.reload();
        
this.router.navigateByUrl('/layout/contractorLabourList')
        // this.allLocations = data['result']
      },
      (err: HttpErrorResponse) => {
        this.toastr.error("Server side Error while update");
      }
    );
  }
  // editAssets(c, b) {
  //   this.issuedate = c.issuedate;
  //   this.issuedReason=c.issuedReason;

  // }
  ediExperience(c, b) {
    
    this.previousEmployer = c.previousEmployer;
    this.jobTitle = c.jobTitle;
    this.wage = c.wage;
    this.workedStartedDate = c.workedStartedDate;
    this.workedCompletedDate = c.workedCompletedDate;
    this.jobType = c.jobType;
    this.jobDuties = c.jobDuties;
    this.employmentType = c.employmentType;
    this.toolUsed = c.toolUsed;
    this.reasonForLeaving = c.reasonForLeaving;
  }
  subwork: any;
  indexes: any;
  deletework(data, i) {
    this.subwork = data;
    this.indexes = i;
  }
  deleteworks() {
    this.workHistoryList.splice(this.indexes, 1);
  }
  subWeek: any;
  indexx: any;
  deleteAset(data, i) {
    this.subWeek = data;
    this.indexx = i;
  }

  deleteAssets() {
    this.assetHistories.splice(this.indexx, 1);
  }
  isedit: boolean = false;
  getLabourById() {
    // if(this.labourUpdate=='true'){
    //   debugger
    //   this.isedit=true
    // }
    let url = this.baseurl + `/getByIdLabourInfo/${this.labourID}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(
      (data) => {
        this.isedit = true;
        this.getSingleLabourList = data;
        this.allLabourDataEdit = this.getSingleLabourList.result;
        console.log('Labordata:', this.allLabourDataEdit);
        this.roleId = this.allLabourDataEdit.roleId;
        this.labourMasterId = this.allLabourDataEdit.labourMasterId;
        this.assetHistories = this.allLabourDataEdit.assetHistories;
        this.workHistoryList = this.allLabourDataEdit.workHistoryList;
        this.documentMaster = this.allLabourDataEdit.documentMaster;
        // this.empId=this.allLabourDataEdit.empId
        this.firstName = this.allLabourDataEdit.firstName;
        this.middleName = this.allLabourDataEdit.middleName;
        this.lastName = this.allLabourDataEdit.lastName;
        this.gender = this.allLabourDataEdit.gender;
        this.bloodGroup = this.allLabourDataEdit.bloodGroup;
        this.contactNumber = this.allLabourDataEdit.contactNumber;
        this.currentAge=this.allLabourDataEdit.age;
        this.emergencyContactNo = this.allLabourDataEdit.emergencyContactNo;
        this.emailId = this.allLabourDataEdit.emailId;
        this.tempAddress = this.allLabourDataEdit.tempAddress;
        this.permanentAddress = this.allLabourDataEdit.permanentAddress;
        this.maritalStatus = this.allLabourDataEdit.maritalStatus;
        this.aadharNo = this.allLabourDataEdit.aadharNo;
        this.panNumber = this.allLabourDataEdit.panNumber;
        this.voterCardNo = this.allLabourDataEdit.voterCardNo;
        this.pfNumber = this.allLabourDataEdit.pfNumber;
        this.esicNumber = this.allLabourDataEdit.esicNumber;
        this.wcNumber = this.allLabourDataEdit.wcNumber;
        this.accountNumber = this.allLabourDataEdit.accountNumber;
        this.ifscCode = this.allLabourDataEdit.ifscCode;
        this.bankName = this.allLabourDataEdit.bankName;
        this.desigId = this.allLabourDataEdit.designationMaster.desigId;
        this.dateOfBirth = this.allLabourDataEdit.dateOfBirth;
        this.conEmpId = this.allLabourDataEdit.contractorMaster.empId;
        this.locationId = this.allLabourDataEdit.locationMaster.locationId;
        this.fresher=this.allLabourDataEdit.fresher
        this.relation=this.allLabourDataEdit.relation
        this.name=this.allLabourDataEdit.name
        this.relationage=this.allLabourDataEdit.age
        this.Pgender=this.allLabourDataEdit.gender
        





        // relation:this.relation,
        // name:this.name,
        // relationage:this.age,
        // Pgender:this.gender
      },
      (err: HttpErrorResponse) => {}
    );
  }
  calculateAge() {
    const birthDate = new Date(this.dateOfBirth);
    const currentDate = new Date();

    const yearDiff = currentDate.getFullYear() - birthDate.getFullYear();
    const monthDiff = currentDate.getMonth() - birthDate.getMonth();

    // Adjust age if birth month hasn't occurred yet this year
    if (
      monthDiff < 0 ||
      (monthDiff === 0 && currentDate.getDate() < birthDate.getDate())
    ) {
      this.currentAge = yearDiff - 1;
    } else {
      this.currentAge = yearDiff;
    }
  }


  resetForm() {
    // Reset all the form fields to their initial values or clear them.
    this.roleId = '';
    this.firstName = '';
    this.middleName = '';
    this.lastName = '';
    this.gender = '';
    this.bloodGroup = '';
    this.contactNumber = '';
    this.emergencyContactNo = '';
    this.emailId = '';
    this.tempAddress = '';
    this.permanentAddress = '';
    this.maritalStatus = '';
    this.aadharNo = '';
    this.panNumber = '';
    this.voterCardNo = '';
    this.pfNumber = '';
    this.esicNumber = '';
    this.wcNumber = '';
    this.bankName='';
    this.ifscCode='';
    this.accountNumber='';
    this.documentMaster = '';
    this.workHistoryList = [];
    this.assetHistories = [];
    this.dateOfBirth = '';
    this.conEmpId = '';
    this.imageUrl = '';
    // this.currentAge = '';
    this.locationId = '';
    this.desigId = '';
    this.deptId = '';
    this.labourMasterId = '';
    this.labourID = '';

    this.aadharImg = '';
    this.panCardImg = '';
    this.policeVerificationDoc = '';
    this.experienceLetter = '';
    this.policeVerificationDocExpiryDate = '';


    this.previousEmployer = '';
  this.jobTitle = '';
  this.wage = '';
  this.workedStartedDate = '';
  this.workedCompletedDate = '';
  this.jobType = '';
  this.jobDuties = '';
  this.employmentType = '';
  this.toolUsed = '';
  this.reasonForLeaving = '';
    
  }

  
}
