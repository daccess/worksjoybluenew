import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorsLabourAddComponent } from './contractors-labour-add.component';

describe('ContractorsLabourAddComponent', () => {
  let component: ContractorsLabourAddComponent;
  let fixture: ComponentFixture<ContractorsLabourAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorsLabourAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorsLabourAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
