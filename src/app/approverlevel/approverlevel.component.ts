import { Component, OnInit } from '@angular/core';
import { approveService } from './serviceModel/service/approveService';
import { from } from 'rxjs';
import { approveModel } from './serviceModel/Model/approveModel';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from '../../app/shared/configurl';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-approverlevel',
  templateUrl: './approverlevel.component.html',
  styleUrls: ['./approverlevel.component.css'],
  providers: [approveService]
})
export class ApproverlevelComponent implements OnInit {

  workFromHome: any = {};
  leave: any = {};
  onDuty: any = {};
  missPunch: any = {};
  permissionRequest: any = {};
  lateEarly: any = {};
  extraWork: any = {};
  coffRequest: any = {};

  isEdit: boolean;

  approveSettingLevelReqDtoList = [];
  aproveDataModel: approveModel;
  loggedUser: any;
  compId: any;
  baseurl = MainURL.HostUrl
  approveAllData: any = [];
  WFHsecondDisabled: boolean;
  constructor(public appService: approveService,
    public httpService: HttpClient,
    public toastr: ToastrService) {

    this.aproveDataModel = new approveModel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.workFromHome.firstLevel = true;
    this.leave.firstLevel = true;
    this.onDuty.firstLevel = true;
    this.missPunch.firstLevel = true;
    this.permissionRequest.firstLevel = true;
    this.lateEarly.firstLevel = true;
    this.extraWork.firstLevel = true;
    this.coffRequest.firstLevel = true;
    // this.reset();    
    this.getApproveData();


    this.workFromHome.secondLevel = false;
    this.workFromHome.thirdLevel = false;
    this.workFromHome.hrApproverLevel = false;

    this.leave.secondLevel = false;
    this.leave.thirdLevel = false;
    this.leave.hrApproverLevel = false;

    this.onDuty.secondLevel = false;
    this.onDuty.thirdLevel = false;
    this.onDuty.hrApproverLevel = false;

    this.missPunch.secondLevel = false;
    this.missPunch.thirdLevel = false;
    this.missPunch.hrApproverLevel = false;

    this.permissionRequest.secondLevel = false;
    this.permissionRequest.thirdLevel = false;
    this.permissionRequest.hrApproverLevel = false;

    this.lateEarly.secondLevel = false;
    this.lateEarly.thirdLevel = false;
    this.lateEarly.hrApproverLevel = false;

    this.extraWork.secondLevel = false;
    this.extraWork.thirdLevel = false;
    this.extraWork.hrApproverLevel = false;

    this.coffRequest.secondLevel = false;
    this.coffRequest.thirdLevel = false;
    this.coffRequest.hrApproverLevel = false;

  }

  ngOnInit() {
  }

  getApproveData() {
    
    let url4 = this.baseurl + '/ApproveSetting/';
    this.httpService.get(url4 + this.compId).subscribe(
      data => {
        this.approveSettingLevelReqDtoList = [];
        this.approveAllData = data["result"];
        for (let i = 0; i < this.approveAllData.length; i++) {
          if (this.approveAllData[i].requestType == 'Work From Home') {
            this.workFromHome = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'Leave') {
            this.leave = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'On Duty') {
            this.onDuty = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'Miss Punch') {
            this.missPunch = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'Permission Request') {
            this.permissionRequest = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'Late/Early Request') {
            this.lateEarly = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'Extra Work Request') {
            this.extraWork = this.approveAllData[i];
          }

          if (this.approveAllData[i].requestType == 'Coff Request') {
            this.coffRequest = this.approveAllData[i];
          }
        }
        if(this.approveSettingLevelReqDtoList == null){
          this.isEdit = false
        }
        else{
          this.isEdit = true;
        }
      }, (err: HttpErrorResponse) => {
      });
  }

  onSubmit() {
    if(this.isEdit == false){
    this.workFromHome.requestType = 'Work From Home';
    this.workFromHome.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.workFromHome);
    this.leave.requestType = 'Leave';
    this.leave.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.leave);
    this.onDuty.requestType = 'On Duty';
    this.onDuty.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.onDuty);
    this.missPunch.requestType = 'Miss Punch';
    this.missPunch.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.missPunch);
    this.permissionRequest.requestType = 'Permission Request';
    this.permissionRequest.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.permissionRequest);
    this.lateEarly.requestType = 'Late/Early Request';
    this.lateEarly.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.lateEarly);
    this.extraWork.requestType = 'Extra Work Request';
    this.extraWork.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.extraWork);
    this.coffRequest.requestType = 'Coff Request';
    this.coffRequest.compId = this.compId;
    this.approveSettingLevelReqDtoList.push(this.coffRequest);
    this.aproveDataModel.approveSettingLevelReqDtoList = this.approveSettingLevelReqDtoList;
    this.aproveDataModel.compId = this.compId;
    this.approveSettingLevelReqDtoList = [];
    this.appService.postAproveData(this.aproveDataModel).subscribe(data => {
        this.toastr.success('Approver Workflow Level Setting Done Successfully', 'Approvel');
        // this.callParent();
        this.getApproveData();
      },
        (err: HttpErrorResponse) => {
        });
      }
      else{
        this.workFromHome.requestType = 'Work From Home';
        this.workFromHome.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.workFromHome);
        this.leave.requestType = 'Leave';
        this.leave.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.leave);
        this.onDuty.requestType = 'On Duty';
        this.onDuty.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.onDuty);
        this.missPunch.requestType = 'Miss Punch';
        this.missPunch.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.missPunch);
        this.permissionRequest.requestType = 'Permission Request';
        this.permissionRequest.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.permissionRequest);
        this.lateEarly.requestType = 'Late/Early Request';
        this.lateEarly.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.lateEarly);
        this.extraWork.requestType = 'Extra Work Request';
        this.extraWork.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.extraWork);
        this.coffRequest.requestType = 'Coff Request';
        this.coffRequest.compId = this.compId;
        this.approveSettingLevelReqDtoList.push(this.coffRequest);
        this.aproveDataModel.approveSettingLevelReqDtoList = this.approveSettingLevelReqDtoList;
        this.aproveDataModel.compId = this.compId;
        this.approveSettingLevelReqDtoList = [];
        this.appService.postAproveData(this.aproveDataModel).subscribe(data => {
            this.toastr.success('Approver Workflow Level Setting Updated Successfully', 'Approvel');
            this.getApproveData();
          },
          (err: HttpErrorResponse) => {
          });
      }


  }

  WFHthird(event) {
    if (this.workFromHome.thirdLevel = event) {
      this.workFromHome.secondLevel = true;
    }
  }

  WFHHR(event) {
    if (this.workFromHome.hrApproverLevel = event) {
      this.workFromHome.secondLevel = true;
    }
  }

  WFHsecond(event) {
    if (this.workFromHome.secondLevel = event) {
    }
    else {
      this.workFromHome.thirdLevel = false;
      this.workFromHome.hrApproverLevel = false;
    }
  }

  leaveSecond(event) {
    if (this.leave.secondLevel = event) {
    }
    else {
      this.leave.thirdLevel = false;
      this.leave.hrApproverLevel = false;
    }
  }

  leaveThird(event) {
    if (this.leave.thirdLevel = event) {
      this.leave.secondLevel = true;
    }
  }

  leaveHR(event) {
    if (this.leave.hrApproverLevel = event) {
      this.leave.secondLevel = true;
    }
  }

  onDutySecond(event) {
    if (this.onDuty.secondLevel = event) {
    }
    else {
      this.onDuty.thirdLevel = false;
      this.onDuty.hrApproverLevel = false;
    }
  }
  onDutyThird(event) {
    if (this.onDuty.thirdLevel = event) {
      this.onDuty.secondLevel = true;
    }
  }

  onDutyHR(event) {
    if (this.onDuty.hrApproverLevel = event) {
      this.onDuty.secondLevel = true;
    }
  }

  missPunchSecond(event) {
    if (this.missPunch.secondLevel = event) {

    }
    else {
      this.missPunch.thirdLevel = false;
      this.missPunch.hrApproverLevel = false;
    }

  }

  missPunchThird(event) {
    if (this.missPunch.thirdLevel = event) {
      this.missPunch.secondLevel = true;
    }
  }

  missPunchHR(event) {
    if (this.missPunch.hrApproverLevel = event) {
      this.missPunch.secondLevel = true;
    }
  }

  permissionSecond(event) {
    if (this.permissionRequest.secondLevel = event) {
    }
    else {
      this.permissionRequest.thirdLevel = false;
      this.permissionRequest.hrApproverLevel = false;
    }
  }

  permissionThird(event) {
    if (this.permissionRequest.thirdLevel = event) {
      this.permissionRequest.secondLevel = true;
    }
  }
  permissionHR(event) {
    if (this.permissionRequest.hrApproverLevel = event) {
      this.permissionRequest.secondLevel = true;
    }
  }

  LateearlySecond(event) {
    if (this.lateEarly.secondLevel = event) {
    }
    else {
      this.lateEarly.thirdLevel = false;
      this.lateEarly.hrApproverLevel = false;
    }
  }

  LateearlyThird(event) {
    if (this.lateEarly.thirdLevel = event) {
      this.lateEarly.secondLevel = true;
    }
  }

  LateearlyHR(event) {
    if (this.lateEarly.hrApproverLevel = event) {
      this.lateEarly.secondLevel = true;
    }
  }

  extraWorkSecond(event) {
    if (this.extraWork.secondLevel = event) {
    }
    else {
      this.extraWork.thirdLevel = false;
      this.extraWork.hrApproverLevel = false;
    }

  }

  extraWorkThird(event) {
    if (this.extraWork.thirdLevel = event) {
      this.extraWork.secondLevel = true;
    }
  }

  extraWorkHR(event) {
    if (this.extraWork.hrApproverLevel = event) {
      this.extraWork.secondLevel = true;
    }
  }

  coffReqSecond(event) {
    if (this.coffRequest.secondLevel = event) {
    }
    else {
      this.coffRequest.thirdLevel = false;
      this.coffRequest.hrApproverLevel = false;
    }
  }

  coffReqThird(event) {
    if (this.coffRequest.thirdLevel = event) {
      this.coffRequest.secondLevel = true;
    }
  }

  coffReqHR(event) {
    if (this.coffRequest.hrApproverLevel = event) {
      this.coffRequest.secondLevel = true;
    }
  }

  reset() {
    this.workFromHome.secondLevel = false;
    this.workFromHome.thirdLevel = false;
    this.workFromHome.hrApproverLevel = false;
    this.leave.secondLevel = false;
    this.leave.thirdLevel = false;
    this.leave.hrApproverLevel = false;
    this.onDuty.secondLevel = false;
    this.onDuty.thirdLevel = false;
    this.onDuty.hrApproverLevel = false;
    this.missPunch.secondLevel = false;
    this.missPunch.thirdLevel = false;
    this.missPunch.hrApproverLevel = false;
    this.permissionRequest.secondLevel = false;
    this.permissionRequest.thirdLevel = false;
    this.permissionRequest.hrApproverLevel = false;
    this.lateEarly.secondLevel = false;
    this.lateEarly.thirdLevel = false;
    this.lateEarly.hrApproverLevel = false;
    this.extraWork.secondLevel = false;
    this.extraWork.thirdLevel = false;
    this.extraWork.hrApproverLevel = false;
    this.coffRequest.secondLevel = false;
    this.coffRequest.thirdLevel = false;
    this.coffRequest.hrApproverLevel = false;
  }
  ngOnDestroy() {
    this.approveSettingLevelReqDtoList.forEach(subscription => {
      subscription.unsubscribe();
  });
  }
}
