import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproverlevelComponent } from './approverlevel.component';

describe('ApproverlevelComponent', () => {
  let component: ApproverlevelComponent;
  let fixture: ComponentFixture<ApproverlevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproverlevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproverlevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
