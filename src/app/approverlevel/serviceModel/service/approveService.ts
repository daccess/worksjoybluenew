import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { approveModel } from '../Model/approveModel';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class approveService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }
    postAproveData(aprove: approveModel) {
        let url = this.baseurl + '/ApproveSetting';
        var body = JSON.stringify(aprove);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
}