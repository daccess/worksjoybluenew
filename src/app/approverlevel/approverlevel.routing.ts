import { Routes, RouterModule } from '@angular/router'
import { ApproverlevelComponent } from './approverlevel.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ApproverlevelComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'approverlevel',
                    pathMatch :'full'
                },
                {
                    path:'approverlevel',
                    component: ApproverlevelComponent
                }
            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);