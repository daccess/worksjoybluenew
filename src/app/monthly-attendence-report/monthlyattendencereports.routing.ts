import { Routes, RouterModule } from '@angular/router'

import { MonthlyAttendenceReportComponent } from './monthly-attendence-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: MonthlyAttendenceReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'monthlyattendancereports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'monthlyattendancereports',
                    component: MonthlyAttendenceReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);