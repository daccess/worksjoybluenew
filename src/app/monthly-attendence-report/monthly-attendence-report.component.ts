import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
//import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-monthly-attendence-report',
  templateUrl: './monthly-attendence-report.component.html',
  styleUrls: ['./monthly-attendence-report.component.css'],
  providers:[ExcelService]
})
export class MonthlyAttendenceReportComponent implements OnInit {

  AllReportData: any;
  AllMonthReport: any;
  day : any;
  monthday: any;
  exceldata: any;
  exceldata1 = [];
  tablehead = [];
  getDatetablehead = [];
  
  fromDate: any;
  toDate: any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  constructor(private excelService:ExcelService,private datePipe: DatePipe) { 

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();

    this.AllReportData = sessionStorage.getItem('monthlyreportData')
    this.AllMonthReport = JSON.parse(this.AllReportData);
    // this.empName = this.AllReport["0"]

    let getTableHead = sessionStorage.getItem('getDataForTablehead');
    this.tablehead = JSON.parse(getTableHead);

    let getdateh = sessionStorage.getItem('getDateTablehead')
    this.getDatetablehead = JSON.parse(getdateh);
    this.exceldata = this.AllMonthReport;

    for(let i=0; i<this.exceldata.length; i++){
        for (let j = 0; j < this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList.length; j++) {
          this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j];
          if(j !=0){
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].fullName = "";
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].lastName = "";
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].empId = "";
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].deptName = "";
     
               }
               this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].srno = j+1;
              this.exceldata1.push(this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j]);
        }
    }
  }

  ngOnInit() {
 
  }


  exportAsXLSX():void {

    let excelData = new Array();
    for (let i = 0; i < this.exceldata1.length; i++) {

      let obj : any= {
        empId: this.exceldata1[i].empId,
        fullName: this.exceldata1[i].fullName.toString().trim()+' '+this.exceldata1[i].lastName.toString().trim(),
        deptName: this.exceldata1[i].deptName.toString().trim(),
        attendanceStatus: this.exceldata1[i].attendanceStatus
       }

       excelData.push(obj)
    }

    
    this.excelService.exportAsExcelFile(excelData, 'Monthly_Attendence_Report');
  }

  public captureScreen()  
  {  
    
    let item = {
      header:"EmpId",
      // header:"",
      title : "abc"
    }
    let item1 = {
      // header:"Shift",
      header:"Full Name",
      title : "cde"
    }
    let item2 = {
      // header:"Attendance date",
      header:"Dept name",
      title : "efg"
    }
    let item3 = {
      // header:"In Time",
      header:"status",
      title : "ghi"
    }
    let item4 = {
      // header:"In Time",
      header:"Sr.No",
      title : "ghi"
    }
    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [item4,item,item1,item2,item3,];
    var rows = [];
    doc.addPage();
    var pageCount = doc.internal.getNumberOfPages();
    for(let i = 0; i < pageCount; i++) { 
    doc.setPage(i); 
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.text(580,12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
    }

      var rowCountModNew = this.exceldata1;

      rowCountModNew.forEach((element, index) => {
        let obj = [element.srno,element.empId,element.fullName+' '+element.lastName,element.deptName.trim(),element.attendanceStatus]
      rows.push(obj);
    });
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14 ,'Company Name :'+ this.compName)

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(34, 44 ,'A : Absent, P : Present, HF : Half day, WO: Weekly off, HO: Holiday , WOH : Week Off And Holiday')
    
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12);
    doc.setFontStyle("Arial")
    doc.text(280, 14,'Monthly Attendance Report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(8)
    doc.setFontStyle("Arial")
    doc.text(270, 24 ,'From date' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy") + ' ' +' To date' + ' : ' + this.datePipe.transform(this.toDate,"dd-MMM-yyyy")
    )

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"))
    doc.autoTable(col, rows,{
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      headerStyles: {
        fillColor: [103, 132, 130],   
    },
      styles: {
        halign: 'center'
      },
      theme: 'grid',
      margin: { top: 50, left: 5, right: 5, bottom: 50 }
    }
    );
    doc.save('monthly_attendance_report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');

  }  
  
}
