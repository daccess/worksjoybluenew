import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyAttendenceReportComponent } from './monthly-attendence-report.component';

describe('MonthlyAttendenceReportComponent', () => {
  let component: MonthlyAttendenceReportComponent;
  let fixture: ComponentFixture<MonthlyAttendenceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyAttendenceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyAttendenceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
