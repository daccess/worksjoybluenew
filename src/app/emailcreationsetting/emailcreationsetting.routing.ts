import { Routes, RouterModule } from '@angular/router'

import { EmailcreationsettingComponent } from './emailcreationsetting.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: EmailcreationsettingComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'emailcreationsetting',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'emailcreationsetting',
                    component: EmailcreationsettingComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);