import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailcreationsettingComponent } from './emailcreationsetting.component';

describe('EmailcreationsettingComponent', () => {
  let component: EmailcreationsettingComponent;
  let fixture: ComponentFixture<EmailcreationsettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailcreationsettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailcreationsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
