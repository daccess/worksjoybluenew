import { TestBed, inject } from '@angular/core/testing';

import { RolesettingService } from './rolesetting.service';

describe('RolesettingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RolesettingService]
    });
  });

  it('should be created', inject([RolesettingService], (service: RolesettingService) => {
    expect(service).toBeTruthy();
  }));
});
