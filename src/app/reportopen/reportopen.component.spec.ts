import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportopenComponent } from './reportopen.component';

describe('ReportopenComponent', () => {
  let component: ReportopenComponent;
  let fixture: ComponentFixture<ReportopenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportopenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportopenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
