import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkforcemenuComponent } from './workforcemenu.component';

describe('WorkforcemenuComponent', () => {
  let component: WorkforcemenuComponent;
  let fixture: ComponentFixture<WorkforcemenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkforcemenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkforcemenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
