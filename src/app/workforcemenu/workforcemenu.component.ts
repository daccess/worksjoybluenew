import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-workforcemenu',
  templateUrl: './workforcemenu.component.html',
  styleUrls: ['./workforcemenu.component.css']
})
export class WorkforcemenuComponent implements OnInit {
  loggedUser: any = {};
  adminUserName: string;
  adminLoginFlag: boolean = false;
  user: string;
  users: any;
  manpowerrequestFlag: any;
  rolename: any;

  constructor(private router : Router) { }

  ngOnInit() {
   
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user);
    this.manpowerrequestFlag=this.users.roleList.isManpowerRequestAuth
    this.rolename=this.users.roleList.roleName;

    // let temp=document.getElementsByClassName('daterangepicker');
    // console.log(temp);
    // if(temp.length!=0){
    //   temp[0].classList.add("hide");
    // }
    $('.daterangepicker').remove();
    // this.adminUserName = this.loggedUser.username;
   
    // if(this.loggedUser.username == 'Admin' && this.loggedUser.RoleHead == 'Admin'){
    //   this.adminLoginFlag = true;
    // }
    // else{
    //   this.adminLoginFlag = false;
    // }
  }
  goto(){
    sessionStorage.removeItem('EmpDataDetails');
    this.router.navigateByUrl('/layout/pimshr');
  }
  gottoEmployee(){
    this.router.navigateByUrl('/layout/employeelist/employeelist').then(()=>{
      window.location.reload();
    });
  }
}
