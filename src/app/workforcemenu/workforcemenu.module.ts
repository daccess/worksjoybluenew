import { routing } from './workforcemenu.routing';
import { NgModule } from '@angular/core';
import { WorkforcemenuComponent } from './workforcemenu.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        WorkforcemenuComponent

    
    ],
    imports: [
    routing,
    FormsModule,
    CommonModule
    ],
    providers: []
  })
  export class  WorkforcemenuModule { 
      constructor(){

      }
  }
  