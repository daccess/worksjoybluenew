import { Routes, RouterModule } from '@angular/router'
import { WorkforcemenuComponent } from './workforcemenu.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: WorkforcemenuComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'workforcemenu',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'workforcemenu',
                    component: WorkforcemenuComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);