import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabourApproveRejectListComponent } from './labour-approve-reject-list.component';

describe('LabourApproveRejectListComponent', () => {
  let component: LabourApproveRejectListComponent;
  let fixture: ComponentFixture<LabourApproveRejectListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabourApproveRejectListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabourApproveRejectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
