import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { RegularisationService } from '../regularisation/service/regularisation.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-labour-approve-reject-list',
  templateUrl: './labour-approve-reject-list.component.html',
  styleUrls: ['./labour-approve-reject-list.component.css']
})
export class LabourApproveRejectListComponent implements OnInit {
  
  baseurl = MainURL.HostUrl;
  token: any;
  themeColor: string;
  allmanpowerLabourData: any;
  empid: any;
  selectedtablelist='Approved'
  user: string;
  users: any;
  allloactionid: any;
  roleNames: any;
  supervisorEmpIds: any;
  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private regService: RegularisationService,public toastr:ToastrService,private router:Router) { 
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;

  }

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.allloactionid=this.users.roleList.locationIdList
    this.empid=this.users.roleList.empId;
    this.roleNames=this.users.roleList.roleName;
    this.supervisorEmpIds=this.users.roleList.contrctorEmpId
    this.getAllLabourData()
  }

  getselectedList(e:any){
    if(e=='Approved'){
      this.selectedtablelist='Approved'
    }
    if(e=='Rejected'){
      this.selectedtablelist='Rejected'
    }

  }
  getAllLabourData(){

    let url = this.baseurl + `/GetApprovedListContractor?empId=${this.empid}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url,{headers}).subscribe(data => {
    
      this.allmanpowerLabourData = data["result"];
        // this.locationId=this.locationdata[0].locationId
        // this.getallManpowerRequest();
  },
    (err: HttpErrorResponse) => {
  
    })
  
    if(this.roleNames=='Supervisor'){
      let url = this.baseurl + `/GetApprovedListContractor?empId=${this.supervisorEmpIds}`;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpservice.get(url,{headers}).subscribe(data => {
        
          this.allmanpowerLabourData = data["result"];
            // this.locationId=this.locationdata[0].locationId
            // this.getallManpowerRequest();
      },
        (err: HttpErrorResponse) => {
      
        })
    }
      }
}
