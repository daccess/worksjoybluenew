import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';
import { Router } from '@angular/router';
export type AssignType = "number" | "string" | "boolean"
@Component({
  selector: 'app-front-approve-reject-labour',
  templateUrl: './front-approve-reject-labour.component.html',
  styleUrls: ['./front-approve-reject-labour.component.css']
})
export class FrontApproveRejectLabourComponent implements OnInit {
  themeColor: string = "nav-pills-blue";
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  getAllDataApproveReject: any;
  getByDocIds: string;
  getAllDocuments: any;
  allDocumentHere: any;
  adharimage: any;
  labourid: any;
  docIds: any;
  docids: any;
  isvisiblebutton:boolean=false
  manrequestids: string;
  count:any;
  // router: any;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router: Router) { 
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
  }

  ngOnInit() {
    this.getByDocIds=sessionStorage.getItem('documentIds')
    this.manrequestids=sessionStorage.getItem('manReqStatusId')
    this.getAllApproveRejectLabour();
    this.getAllDocumentByIds();
  }
  getAllApproveRejectLabour() {
    let url = this.baseurl + '/getApprovedFrontDeskLabourList';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.getAllDataApproveReject = data["result"];
      setTimeout(function () {

        $(function () {

          var table = $('#approveRejectLabour').DataTable({

            retrieve: true,
      

            "columnDefs": [
              {
                "targets": [6],
                "visible": true
              }
            ]

          })

          
          $('.status-dropdown').on('change', function (e) {
            var status = $(this).val();
            $('.status-dropdown').val(status)
            console.log(status)
            table.column(6).search(status as AssignType).draw();
          })


        });
      }, 100);
      this.Spinner.hide();
    },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();

      });
  }
  getAllDocumentByIds() {
    let url = this.baseurl + `/getFrontDeskDocumentList/${this.getByDocIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.getAllDocuments = data;
      this.allDocumentHere=this.getAllDocuments.result
      console.log('Received data:', this.allDocumentHere);
      console.log('Received contrator name:', this.allDocumentHere);

   if(this.allDocumentHere.documentMaster.experienceDocStatus=='verified'&&this.allDocumentHere.documentMaster.policeDocStatus=='verified'&&this.allDocumentHere.documentMaster.panStatus=='verified'&&this.allDocumentHere.documentMaster.aadharStatus=='verified'){
this.isvisiblebutton=true;
   }
   else{
    this.isvisiblebutton=false;
   }
    },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();

      });
  }
  getDocument(e:any){
    
this.adharimage=e.documentMaster.aadharImg
  }
  getDocumentPan(e:any){
    this.adharimage=e.documentMaster.panCardImg
  }
  getDocumentpolice(e:any){
    this.adharimage=e.documentMaster.policeVerificationDoc
  }

  getDocumentExperiance(e:any){
    this.adharimage=e.documentMaster.experienceLetter
  }
  verifydocument(e:any,docNam:any){
    
  this.labourid=e.labourMasterId;
  this.docIds=e.documentMaster.docId
let obj={
  "remark":"ok",
  "documentName":docNam,
  "status":"verified",
  "approverId":this.empids,
  "labourId":this.labourid,
  "docId":this.docIds,
}
let url = this.baseurl +"/createDocumentHistory";
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpservice.post(url,obj, { headers }).subscribe(data => {

 this.toastr.success("Document verified successfully")
this.getAllDocumentByIds()
},
  (err: HttpErrorResponse) => {
    this.Spinner.hide();
    this.toastr.error(" Failed to  verified Document")

  });
  }
  rejectDocument(e:any,docNam:any){
  this.labourid=e.labourMasterId;
  this.docids=e.documentMaster.docId
let obj={
  "remark":"cancel",
  "documentName":docNam,
  "status":"rejected",
  "approverId":this.empids,
  "labourId":this.labourid,
  "docId":this.docids,
}
let url = this.baseurl +"/createDocumentHistory";
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpservice.post(url,obj, { headers }).subscribe(data => {

 this.toastr.success("Document Rejected successfully")
 this.getAllDocumentByIds()
},
  (err: HttpErrorResponse) => {
    this.Spinner.hide();
    this.toastr.error(" Failed to  Reject Document")

  });
  }
  SaveLabourFromFront_desk(){
  if(this.isvisiblebutton==true){

  
  
let obj={
  "remark":"ok",
  "deskFlag":"Approved",
  "manReqStatusId":this.manrequestids,
  "labourPerId":this.getByDocIds,
 "approverId":this.empids
}
let url = this.baseurl +"/createFrontDeskLabourApproved";
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpservice.post(url,obj, { headers }).subscribe(data => {

 this.toastr.success("Approved successfully")
 
},
  (err: HttpErrorResponse) => {
    this.Spinner.hide();
    this.toastr.error(" Failed to Reject")

  });
  }
  else if(this.isvisiblebutton==false){
this.toastr.error("Approve First All Document")
  }
}
  
  generateProvisionalPass(){
    if(this.isvisiblebutton==true){
    this.router.navigate(['/layout/genratepass'])
    }
    else if(this.isvisiblebutton==false){
      this.toastr.error("Approve First All Document")
    }

  }

}
