import { FrontApproveRejectLabourModule } from './front-approve-reject-labour.module';

describe('FrontApproveRejectLabourModule', () => {
  let frontApproveRejectLabourModule: FrontApproveRejectLabourModule;

  beforeEach(() => {
    frontApproveRejectLabourModule = new FrontApproveRejectLabourModule();
  });

  it('should create an instance', () => {
    expect(frontApproveRejectLabourModule).toBeTruthy();
  });
});
