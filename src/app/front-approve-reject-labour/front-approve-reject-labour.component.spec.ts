import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontApproveRejectLabourComponent } from './front-approve-reject-labour.component';

describe('FrontApproveRejectLabourComponent', () => {
  let component: FrontApproveRejectLabourComponent;
  let fixture: ComponentFixture<FrontApproveRejectLabourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontApproveRejectLabourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontApproveRejectLabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
