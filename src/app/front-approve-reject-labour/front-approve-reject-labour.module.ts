import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FrontDeskRoutings } from './front-approve-reject-labour-routing.module';
import { FrontApproveRejectLabourComponent } from './front-approve-reject-labour.component';

@NgModule({
  imports: [
    CommonModule,
    FrontDeskRoutings
  ],
  declarations: [
    FrontApproveRejectLabourComponent
  ]
})
export class FrontApproveRejectLabourModule { }
