import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontApproveRejectLabourComponent } from './front-approve-reject-labour.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: FrontApproveRejectLabourComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'FrontApproveReject',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'FrontApproveReject',
                  component: FrontApproveRejectLabourComponent
              }

          ]

  },

  // otherwise redirect to home

]

export const FrontDeskRoutings = RouterModule.forChild(appRoutes);