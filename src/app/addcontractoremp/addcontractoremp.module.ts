import { routing } from './addcontractoremp.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { CommonModule, DatePipe } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AddcontractorempComponent } from './addcontractoremp.component';
//imoprt shared data

import {SharedModule} from '../shared/shared.module';
import { FilterFirmNamePipe } from './filter-firm-name.pipe'; 

@NgModule({
    declarations: [
        AddcontractorempComponent,
        FilterFirmNamePipe
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule,
    SharedModule
    ],
    providers: []
  })
  export class AddcontractorempModule { 
      constructor(){

      }
  }
  