import { Component, OnInit } from '@angular/core';
import { MainURL } from './../shared/configurl';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common'
import { NgxSpinnerService } from 'ngx-spinner';
import { contractorEmployeeModel } from '../shared/model/contractorempModel';
import {dataService} from '../shared/services/dataService';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';
@Component({
selector: 'app-addcontractoremp',
templateUrl: './addcontractoremp.component.html',
styleUrls: ['./addcontractoremp.component.css'],
providers: [DatePipe]
})
export class AddcontractorempComponent implements OnInit {
baseurl = MainURL.HostUrl;
DepartmentAllData: any;
selectedDepartmentobj:any
loggedUser: any;
public selectUndefinedOptionValue: any = '';
contractorEmpModel: contractorEmployeeModel;
compId: any;

obje: any={};
receiptUrl: any;
empimage:any

SubDepartmentData: any;
selectedSubDepartmentobj: any;
LocationAllData: any;
selectedLocationobj:any;
weeklyCalendarId: any;
DesignationAllData: any;
selectedDesignationobj:any;
contractServiceData: any
contractNamesData: any
conSerTypeId: any;
selectedconNametobj: any;
selectedServiceobj: any;
Shiftgroupdata: any;
selectedshiftgroupobj: number;
Shiftmastergroupdata: any;
selectedshiftmastergroupobj: number;
WeeklyOffcalendarData: any[];
selectedWeeklyGroupobj: number;
roles_url='RollMaster';
roles:any[]=[];
sanctioning_authorities_url='employeeListWhoHasAuthority';
contractor_supervisor_url='contractorEmpListWhoHasSupervisor';
authorities:any[]=[];
contractor_supervisors=[];
fileToUpload: File = null;
hide_reporting_mgr=true;
selectedEmpTypeobj: any = "";
selectedSubEmpTypeobj: any = "";
contractor_supervisor;
empTypeAllData: any;
subempTypeAllData: any;
  myset: any;
  contractNamesData1: any[];
  uniqueChars: any[];
  ActiveRoll: any;
  object: number;
  selectedObjectRoleMasterId: number;
  RoleHead: any;
  selectedroleobj: number;
  selectedfirmnameobj: number;
  selectedcontractorserviceobj: number;
  selectedLocationId: any=0;
constructor(public httpService: HttpClient,public Spinner: NgxSpinnerService,public datepipe: DatePipe,private toastr: ToastrService,private dataservice : dataService,private router: Router) { 
this.contractorEmpModel = new contractorEmployeeModel();
this.contractorEmpModel.empDocMasterList=[];
this.contractorEmpModel.gender='Male';
let user = sessionStorage.getItem('loggedUser');
this.loggedUser = JSON.parse(user);
this.compId = this.loggedUser.compId;
this.Departmentgroup();
this.locationGRoup( this.selectedLocationId);
this. designationFunction();
this.contractorSerType();
this.shiftmastergroup();
// this.shiptgroup();
//this.weeklyoffdata();
this.empType();
// this.contractorNamedata(this.conSerTypeId);
// this.contractorEmpModel.dateOfBirth = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
}
ngOnInit() {
this.getRoles();

}
Departmentgroup() {
let url3 = this.baseurl + '/Department/Active/';
this.httpService.get(url3 + this.compId).subscribe(
data => {
this.DepartmentAllData = data["result"];
}, (err: HttpErrorResponse) => {
}
);
}
Departmentdata(event) {
//this.selectedSubDepartmentobj = "";
this.selectedDepartmentobj = parseInt(event.target.value);
this.SubDepartmentgroup(this.selectedDepartmentobj);
}
SubDepartmentgroup(selectedValue) {
let url4 = this.baseurl + '/SubDepartment/Active/';
this.httpService.get(url4 + selectedValue).subscribe(
data => {
this.SubDepartmentData = data["result"];
}, (err: HttpErrorResponse) => {
});
}
SubDepartmentdata(event) {
this.selectedSubDepartmentobj = parseInt(event.target.value);
this.SubDepartmentgroup(this.selectedSubDepartmentobj);
}
locationGRoup(e) {
  
  this.selectedLocationId=e;
  if(this.loggedUser.roleHead=='HR'){
    let url4 = this.baseurl + '/LocationWise/';
    this.httpService.get(url4 + this.loggedUser.locationId).subscribe(
    data => {
    this.LocationAllData = data["result"];
    }, (err: HttpErrorResponse) => {
    });
  }
  else if(this.loggedUser.roleHead=='Admin'){
let url4 = this.baseurl + '/Location/Active/';
this.httpService.get(url4 +this.compId).subscribe(
data => {
this.LocationAllData = data["result"];
console.log("adminlocation****",this.LocationAllData);
}, (err: HttpErrorResponse) => {
}
);
}
}
Locationdata(event) {
this.selectedLocationobj = parseInt(event.target.value);
console.log("locationid",this.selectedLocationobj);
this.weeklyCalendarId = parseInt(event.target.value);
sessionStorage.setItem("weeklyCalendarId",this.weeklyCalendarId)
 
this.contractorNamedata(this. selectedLocationId)
this.shiptgroup();
this.weeklyoffdata();
this.getAuthorities();
this.contractNamesData={};
this.shiftmastergroup()
// this.attendance.weeklyoffdata(this.weeklyCalendarId);
//  this.SubDepartmentgroup(this.selectedLocationobj)
}
designationFunction() {
let urldesignation = this.baseurl + '/Designation/Active/';
this.httpService.get(urldesignation + this.compId).subscribe(
data => {
this.DesignationAllData = data["result"];
}, (err: HttpErrorResponse) => {
}
);
}
Designationdata(event) {
this.selectedDesignationobj = parseInt(event.target.value);
//console.log("Designationbind", this.selectedDesignationobj);
}
contractorfirmnamedata(event){
  this.selectedfirmnameobj=parseInt(event.target.value);
  console.log("firmnamebind",  this.selectedfirmnameobj);
}

handleFileInputs(file: FileList) {
  
  // this.Spinner.show()
  this.fileToUpload = file.item(0);
  this.obje.name = file.item(0).name; 
  //Show image preview
  var reader = new FileReader();
  reader.onload = (event: any) => {
    this.empimage = event.target.result;
    this.receiptUrl=event.target.result
   //  this.Spinner.hide();

    
  }
  reader.readAsDataURL(this.fileToUpload);
}

contractorSerType() {

let url = this.baseurl + '/CosSerType/Active/';
this.httpService.get(url + this.compId).subscribe(
(data :  any) => {
this.contractServiceData = data["result"];
console.log("servicetype",this.contractServiceData )
this.conSerTypeId
}, (err: HttpErrorResponse) => {
}
);
}
contractorSerTypevalue(event){
  this.selectedcontractorserviceobj= parseInt(event.target.value);
  console.log("conservice",this.selectedcontractorserviceobj);


 
  this.contractorNamedata(this.selectedLocationId)
}

contractorNamedata(conSerTypeId) {
  { 
   

    //console.log("consorid",conSerTypeId)
    // if(conSerTypeId){
    //   let url = 'contractorList';
    //   this.contractNamesData=[];
    //   this.dataservice.getRecordsByid(url,conSerTypeId).subscribe(res=>{
    //     if(res.statusCode==200){
    //       if(res.result){
    //         for(let i=0;i<res.result.length;i++){
    //            if(res.result[i].length!=0){
    //              this.contractNamesData.push(res.result[i][0]);
    //              //console.log("data",   this.contractNamesData);
    //            }
    //         }
    //       }
    //     }
 // });
 if(this.loggedUser.roleHead=='HR'){
 console.log("dataid",this. selectedLocationId);

       let url = this.baseurl + '/Contractor/';
       this.httpService.get(url+this.selectedcontractorserviceobj +'/'+ this. selectedLocationobj+'/'+this.compId).subscribe(
        (data :  any) => {
          
        this.contractNamesData = data["result"];
       
      console.log("firmnames",this.contractNamesData )
        }, (err: HttpErrorResponse) => {
          this.contractNamesData='';
        }
        );
       
    
    }
   
  else if(this.loggedUser.roleHead=='Admin'){
    console.log("locoidAMIN",this.selectedLocationobj);

    let url = this.baseurl + '/Contractor/';
       this.httpService.get(url+this.selectedcontractorserviceobj +'/'+ this.selectedLocationobj+'/'+this.compId).subscribe(
        (data :  any) => {
        this.contractNamesData = data["result"];
        console.log("firmnames",this.contractNamesData )
      
        }, (err: HttpErrorResponse) => {
          this.contractNamesData='';

        }
        );
  }

}
  
}


shiptgroup() {
  if(this.loggedUser.roleHead=='HR'){
let url = this.baseurl + '/ShiftGroup/Active/';
this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(
data => {
this.Shiftgroupdata = data["result"];
//console.log("shiftdata***",this.Shiftgroupdata);
}, (err: HttpErrorResponse) => {
});
  }
  else if(this.loggedUser.roleHead=='Admin'){
    console.log("selectedlocationidshuft",this.selectedLocationobj);
    let url = this.baseurl + '/ShiftGroup/Active/';
   
this.httpService.get(url + this.compId+'/'+this.selectedLocationobj).subscribe(
data => {
this.Shiftgroupdata = data["result"];
//console.log("shiftdata***",this.Shiftgroupdata);
}, (err: HttpErrorResponse) => {
});


  }

}
shiftmastergroup() {

  if(this.loggedUser.roleHead=='HR'){
    let url = this.baseurl + '/ShiftMaster/ByActive/';
    this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
        this.Shiftmastergroupdata = data["result"];
        console.log(" Shiftgroupdatagenral", this.Shiftgroupdata)
    })
    }    
    else if(this.loggedUser.roleHead=='Admin'){
      console.log("locationid",this.selectedLocationobj);
       
        let url = this.baseurl + '/ShiftMaster/ByActive/';
        this.httpService.get(url + this.compId+'/'+this.selectedLocationobj).subscribe(data => {
            this.Shiftmastergroupdata = data["result"];
           console.log(" Shiftgroupdatagenral", this.Shiftgroupdata)
  
    })
    }
}
shiftgroupvalue(event) {
this.selectedshiftgroupobj = parseInt(event.target.value);
}

shiftgroupmastervalue(event) {
this.selectedshiftmastergroupobj = parseInt(event.target.value)
}
weeklyoffdata() {
  if(this.loggedUser.roleHead=='HR'){
let compId = this.compId;
this.WeeklyOffcalendarData = [];
let url1 = this.baseurl + '/weeklyOfMasterActive/';
this.httpService.get(url1 + compId+'/'+this.loggedUser.locationId).subscribe(
data => {
this.WeeklyOffcalendarData = data["result"];
}, (err: HttpErrorResponse) => {
});
  }
  else if(this.loggedUser.roleHead=='Admin'){
    console.log("locoidAMIN",this.selectedLocationobj);
    let compId = this.compId;
this.WeeklyOffcalendarData = [];
let url1 = this.baseurl + '/weeklyOfMasterActive/';
this.httpService.get(url1 + compId+'/'+this.selectedLocationobj).subscribe(
data => {
this.WeeklyOffcalendarData = data["result"];
}, (err: HttpErrorResponse) => {
});


  }
}
Weeklycalendervalue(event) {
this.selectedWeeklyGroupobj = parseInt(event.target.value)
}
//get roles
getRoles(){
if(this.compId){
this.dataservice.getRecordsByid(this.roles_url,this.compId).subscribe(res=>{
if(res.statusCode==200){
this.roles=res.result;
console.log("myroles",this.roles);
}
});
}
}
// getRoles() {
//   let url = this.baseurl + '/RollMaster/Active/';
//   this.httpService.get(url + this.compId).subscribe(data => {
//       this.ActiveRoll = data["result"];
//       console.log("rolesdata", this.ActiveRoll);
//     }, (err: HttpErrorResponse) => {
//     });

// }
getAuthorities(){
  if(this.loggedUser.roleHead=='HR'){
  let url = this.baseurl + '/employeeListWhoHasAuthority/';
  this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
    this.authorities=data["result"];
  console.log("authdatacontreactor$$",this.authorities);
  }, (err: HttpErrorResponse) => {
  });
}
else if(this.loggedUser.roleHead=='Admin'){
  let url = this.baseurl + '/employeeListWhoHasAuthority/';
  this.httpService.get(url + this.compId+'/'+this.selectedLocationobj).subscribe(data => {
    this.authorities=data["result"];
    console.log("authdatacontreactor$$",this.authorities);
  }, (err: HttpErrorResponse) => {
  });

}
}
getobject(data) {
  this.object = parseInt(data.target.value);
  this.selectedObjectRoleMasterId = this.object;
  //this.editRoel(this.object);
}
// editRoel(object) {
//   let urledit = this.baseurl + '/RollMaster/GetById/';
//   this.httpService.get(urledit + object).subscribe(data => {
//       this.RoleHead = data['result']['roleHead'];
//     },
//     (err: HttpErrorResponse) => {
//     });
// }
//get sanscioning authorities
// getAuthorities(){

// if(this.compId){
// this.dataservice.getRecordsByid(this.sanctioning_authorities_url,this.compId).subscribe(res=>{
// if(res.statusCode==200){
// this.authorities=res.result;
// console.log("authdata",this.authorities);
// }
// })
// }
// }
getContractorSupervisor(contractorId){
  
if(contractorId){
this.dataservice.getRecordsByid(this.contractor_supervisor_url,contractorId).subscribe(res=>{
if(res.statusCode==200){
this.contractor_supervisors=res.result;
//console.log("contractorsupeevisordata",this.contractor_supervisors)
}
})
}
}
//handleFileInput
handleFileInput(files: FileList,name) {
let type=files[0].type;
this.fileToUpload = files.item(0);
this.uploadFileToActivity(this.fileToUpload,name,type)
}
uploadFileToActivity(file,name,type) {
this.dataservice.postFile(file,name).subscribe(data => {
if(data["statusCode"]==200){
if(name=="profilePath"){
this.contractorEmpModel.profilePath=data['result'].imageUrl;
}
if(name=="eduCertificate" || name=="aadharcard" || name=='pancard'){
let temp={
"docName" : name,
"docType" : type,
"docUrl"  : data['result'].imageUrl
}
this.contractorEmpModel.empDocMasterList.push(temp);
}
}
}, error => {
});
}
//setReporting Managers
SetReportingManager(event){
let temp_id=event.target.value;
//role
this.contractorEmpModel.rollMasterId=temp_id;
if(temp_id==3){
this.hide_reporting_mgr=true;
}else{
this.hide_reporting_mgr=true;
}
}
SetCompanySupervisor(event){
//company supervisor
this.contractorEmpModel.empId_supervisor=event.target.value;
}
SetManagers(event){
//contract mgr
this.contractor_supervisor=event.target.value;
}
//setBloodGroup
setBloodGroup(event){
this.contractorEmpModel.bloodGroup=event.target.value;
}
//setStartDate
setStartDate(event){
this.contractorEmpModel.validStartDate=new Date(event.target.value);
}
//setEndDate
setEndDate(event){
this.contractorEmpModel.validEndDate=new Date(event.target.value);
}
//Save Contractor
onSaveContracror(){
this.contractorEmpModel.descId=this.selectedDesignationobj;
this.contractorEmpModel.locationId=this.selectedLocationobj;
this.contractorEmpModel.subDeptId=this.selectedSubDepartmentobj;
this.contractorEmpModel.deptId=this.selectedDepartmentobj;
this.contractorEmpModel.shiftGroupId=this.selectedshiftgroupobj;
this.contractorEmpModel.shiftMasterId=this.selectedshiftmastergroupobj;
this.contractorEmpModel.weeklyOffCalenderMasterId=this.selectedWeeklyGroupobj;
this.contractorEmpModel.employmentType = this.selectedEmpTypeobj; 
this.contractorEmpModel.rollMasterId=this.selectedroleobj;
this.contractorEmpModel.conSerTypeId=this.selectedcontractorserviceobj;
//console.log("servicebinding",this.contractorEmpModel.conSerTypeId);
this.contractorEmpModel.contractorId=this.selectedfirmnameobj;
console.log("servicebinding",this.contractorEmpModel.contractorId);

this.contractorEmpModel.subEmpTypeId =Number(this.selectedSubEmpTypeobj);
// this.shiftgroupmastervalue
//user condition
this.contractorEmpModel.firstLevelReportingManager=this.contractorEmpModel.empId_supervisor;
this.contractorEmpModel.empimageURl= this.empimage
// this.contractorEmpModel.secondLevelReportingManager= '0';
//  if(this.contractorEmpModel.rollMasterId==parseInt('2')){
//   this.contractorEmpModel.firstLevelReportingManager=this.contractor_supervisor;
//   this.contractorEmpModel.secondLevelReportingManager= this.contractorEmpModel.empId_supervisor;
// }
// //contractor mgr
// if(this.contractorEmpModel.rollMasterId==parseInt('3')){
//   this.contractorEmpModel.firstLevelReportingManager=this.contractorEmpModel.empId_supervisor;
// }
let url1 = this.baseurl + '/createempcontractor';
//  if(this.contractorEmpModel.profilePath==null){
//   this.toastr.error("Please upload Employee Photo !");
//   return false;
//  }

this.httpService.post(url1,this.contractorEmpModel).subscribe(
(data : any )=> {
if(data.statusCode==201){
this.toastr.success("Contractor Added Sucessfully !");
setTimeout(() => {
    this.router.navigateByUrl('/layout/employeelist/employeelist')
    .then(()=>{
    window.location.reload();
    });
 }, 1000);
}
else if(data.statusCode==226){
    this.toastr.error("bioId already exists")
  }
  
},(err: HttpErrorResponse) => {
this.toastr.error("Something went wrong..!!");
});

}
//ADD EMPTYPE,SUBEMPTYPE
empType() {
let urlempType = this.baseurl + '/EmployeeType/Active/';
this.httpService.get(urlempType + this.compId).subscribe(data => {
this.empTypeAllData = data["result"];
}, (err: HttpErrorResponse) => {
});
}
selectEmptype(event) {
this.selectedSubEmpTypeobj = "";
this.selectedEmpTypeobj = parseInt(event.target.value)
this.subEmpType(this.selectedEmpTypeobj)
}
subEmpType(selectedValue) {
let urlsubempType = this.baseurl + '/SubEmployeeType/Active/';
this.httpService.get(urlsubempType + selectedValue).subscribe(data => {
this.subempTypeAllData = data["result"];
}, (err: HttpErrorResponse) => {
});
}
selectsubEmptype(event) {
this.selectedSubEmpTypeobj = parseInt(event.target.value);
}
omit_special_char(event)
{   
var k;  
k = event.charCode; 
return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}


}