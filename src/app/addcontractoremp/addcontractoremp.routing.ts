import { Routes, RouterModule } from '@angular/router'
import { AddcontractorempComponent } from './addcontractoremp.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: AddcontractorempComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'addcontractoremp',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'addcontractoremp',
                    component: AddcontractorempComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);