import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcontractorempComponent } from './addcontractoremp.component';

describe('AddcontractorempComponent', () => {
  let component: AddcontractorempComponent;
  let fixture: ComponentFixture<AddcontractorempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcontractorempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcontractorempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
