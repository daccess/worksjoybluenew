import { MedicalExaminationformModule } from './medical-examinationform.module';

describe('MedicalExaminationformModule', () => {
  let medicalExaminationformModule: MedicalExaminationformModule;

  beforeEach(() => {
    medicalExaminationformModule = new MedicalExaminationformModule();
  });

  it('should create an instance', () => {
    expect(medicalExaminationformModule).toBeTruthy();
  });
});
