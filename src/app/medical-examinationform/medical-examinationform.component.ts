import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medical-examinationform',
  templateUrl: './medical-examinationform.component.html',
  styleUrls: ['./medical-examinationform.component.css']
})
export class MedicalExaminationformComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  manrequestId: string;
  allMedicalLabourList: any;
  empfistname: any;
  lastname: any;
  labourId: any;
  gender: any;
  dateOfBirth: any;
  bloodGroup: any;
  contactNumber: any;
  emergancyContact: any;
  age:any;
height:any;
weight:any;
bloodPressure:any;
physicalDisability:any;
physicalDiscription:any;
birthMark:any;
manReqStatusId:any;
labourMasterId:any;
approverId:any;
remark:any;
medicalFlag:any;
  labourPerId: any;
medicalForm: any;
option1: boolean = false;
option2: boolean = false;
  fitUnfit: string;
  labourImage: any;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
    this.manrequestId= sessionStorage.getItem("manReqStatusId")
   }

  ngOnInit() {
this.getAllMedicalLabourList()
  
  }
  getAllMedicalLabourList() {
    let url = this.baseurl + `/getMedicalLabourById/${this.manrequestId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.allMedicalLabourList = data["result"];
      this.empfistname=this.allMedicalLabourList.requestStatus.labourInfo.firstName
      this.lastname=this.allMedicalLabourList.requestStatus.labourInfo.lastName;
       this.age=this.allMedicalLabourList.requestStatus.labourInfo.age;
      this.gender=this.allMedicalLabourList.requestStatus.labourInfo.gender;
      this.dateOfBirth=this.allMedicalLabourList.requestStatus.labourInfo.dateOfBirth;
      this.bloodGroup=this.allMedicalLabourList.requestStatus.labourInfo.bloodGroup;
      this.contactNumber=this.allMedicalLabourList.requestStatus.labourInfo.contactNumber;
      this.emergancyContact=this.allMedicalLabourList.requestStatus.labourInfo.emergencyContactNo;
    this.labourPerId=this.allMedicalLabourList.requestStatus.labourInfo.labourPerId;
    this.fitUnfit=this.allMedicalLabourList.requestStatus.labourInfo.fitUnfit;
    this.labourImage=this.allMedicalLabourList.requestStatus.labourInfo.labourImage
    },
      (err: HttpErrorResponse) => {
      });
  }
ApproveLabourforMedical(){

    let obj={
      "age":this.age,
  "height":this.height,
  "weight":this.weight,
  "bloodPressure":this.bloodPressure,
  "physicalDisability":this.physicalDisability,
  "physicalDiscription":this.physicalDiscription,
  "birthMark":this.birthMark,
  "manReqStatusId":this.manrequestId,
  "labourPerId":this.labourPerId,
  "approverId":this.empids,
  "remark":this.remark,
  "medicalFlag":"Approved",
  "fitUnfit":this.fitUnfit,
    }
    let url = this.baseurl + '/saveMedicalLabourdata';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.post(url,obj, { headers }).subscribe(data => {
      this.toastr.success("medical labour Approved")
      
      this.router.navigateByUrl('layout/medicalLabourList')
  },(err: HttpErrorResponse) => {
    this.toastr.error("error while approve medical labour")
  })
  
 
    
  }
  rejectLabourForMedical(){
    
  
      let obj={
        "age":this.age,
    "height":this.height,
    "weight":this.weight,
    "bloodPressure":this.bloodPressure,
    "physicalDisability":this.physicalDisability,
    "physicalDiscription":this.physicalDiscription,
    "birthMark":this.birthMark,
    "manReqStatusId":this.manrequestId,
    "labourPerId":this.labourPerId,
    "approverId":this.empids,
    "remark":this.remark,
    "medicalFlag":"Rejected",
    "fitUnfit":this.fitUnfit,
      }
      let url = this.baseurl + '/saveMedicalLabourdata';
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.post(url,obj, { headers }).subscribe(data => {
        this.toastr.success("medical labour Rejected successfully")
    },(err: HttpErrorResponse) => {
      this.toastr.error("error while Reject medical labour")
    })
    
  }
  





onCheckboxChange(selectedOption: string) {
  if (selectedOption === 'option1' && this.option1) {
    this.option2 = false;
  } else if (selectedOption === 'option2' && this.option2) {
    this.option1 = false;
  }
  // console.log("this is the selected option",  this.option2 )
if(this.option1==true){
  this.fitUnfit='true'
  console.log( this.fitUnfit)
}
else if(this.option2==true){
  this.fitUnfit='false'
  console.log(this.fitUnfit)
}
}
numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;

}

}