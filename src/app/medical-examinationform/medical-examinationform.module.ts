import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {medicaleExaminationRouting } from './medical-examinationform-routing.module';
import { MedicalExaminationformComponent } from './medical-examinationform.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    medicaleExaminationRouting,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    MedicalExaminationformComponent
  ]
})
export class MedicalExaminationformModule { }
