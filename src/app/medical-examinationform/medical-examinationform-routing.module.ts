import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MedicalExaminationformComponent } from './medical-examinationform.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: MedicalExaminationformComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'medicaleExaminationform',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'medicaleExaminationform',
                  component: MedicalExaminationformComponent
              }

          ]

  },

  // otherwise redirect to home

]

export const medicaleExaminationRouting = RouterModule.forChild(appRoutes);