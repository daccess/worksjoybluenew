import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalExaminationformComponent } from './medical-examinationform.component';

describe('MedicalExaminationformComponent', () => {
  let component: MedicalExaminationformComponent;
  let fixture: ComponentFixture<MedicalExaminationformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalExaminationformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalExaminationformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
