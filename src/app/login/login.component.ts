import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, RouterStateSnapshot } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { Logindata } from '../../app/shared/model/loginmodel';
import { LoginService } from '../../app/shared/services/loginservice';
import { ForgoePass } from '../../app/shared/model/ForgotPassModel'
import { SessionService } from '../shared/services/SessionSrvice';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessagingService } from '../shared/services/messaging.service';
import { RoleGuard } from '../guards/role-guard.service';
import { CompanyMasterService } from '../shared/services/companymasterservice';
import { MainURL } from '../shared/configurl';
import { Location } from '@angular/common';
import { signUpData } from '../../app/shared/model/signUpModel';
import { dataService } from '../shared/services/dataService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  baseurl = MainURL.HostUrl

  model: any = {};
  returnUrl: string;
  flag: boolean = true;
  loginmodel: Logindata;
  ForgoePassw: ForgoePass;
  loginresponse: any;
  value: any;
  roletype: any;
  message: any;
  status: any;
  respo: any;
  todaysThout: string = "";
  thouths = [];
  loggedUser: any;
  fullName: any;
  loginFlag: any;
  gratuityStatus: any;
  lastName: any;
  image: string = "";
  empPerId: any;
  userId: MessagingService;
  passWord: String;
  adminLoginmodel: signUpData;
  signUpFlag: boolean = false;
  loggedUsers: string;
  constructor(
    public toastr: ToastrService,
    private router: Router,
    private RoleGuardService :RoleGuard,
    private route: ActivatedRoute,
    public loginservice: LoginService,
    public SessionServ: SessionService,
    public Spinner: NgxSpinnerService,
    private messagingService: MessagingService,
    private companyMasterService : CompanyMasterService,
    public httpService:HttpClient,
    public dataService: dataService,
    private location: Location) {
      

    this.loginmodel = new Logindata();
    this.ForgoePassw = new ForgoePass();
    this.adminLoginmodel = new signUpData();
    this.thouths = [
      {

      },
      {
        id: 1,
        content: "“Thousands of candles can be lighted from a single candle, and the life of the candle will not be shortened. Happiness never decreases by being shared.”  --Buddha"
      },
      {
        id: 2,
        content: " “Happiness is the art of never holding in your mind the memory of any unpleasant thing that has passed.”"
      },
      {
        id: 3,
        content: " “To be happy, we must not be too concerned with others.”  -Albert Camus."
      },
      {
        id: 4,
        content: " “The moments of happiness we enjoy take us by surprise. It is not that we seize them, but that they seize us.” ---Ashley Montagu"
      },
      {
        id: 5,
        content: "“It isn't what you have, or who you are, or where you are, or what you are doing that makes you happy or unhappy. It is what you think about.” --Dale Carnegie"
      },
      {
        id: 6,
        content: "  “It's a helluva start, being able to recognize what makes you happy.” --Lucille Ball  "
      }, {
        id: 7,
        content: "  “Don’t underestimate the value of Doing Nothing, of just going along, listening to all the things you can’t hear, and not bothering.” --Winnie the Pooh  "
      },
      {
        id: 8,
        content: " “There is only one way to happiness and that is to cease worrying about things which are beyond the power of our will.” --Epictetus  "
      }, {
        id: 9,
        content: "  “We tend to forget that happiness doesn't come as a result of getting something we don't have, but rather of recognizing and appreciating what we do have.”  ---Frederick Keonig  "
      },
      {
        id: 10,
        content: " “Sometimes your joy is the source of your smile, but sometimes your smile can be the source of your joy.”   "
      }, {
        id: 11,
        content: "  “To be kind to all, to like many and love a few, to be needed and wanted by those we love, is certainly the nearest we can come to happiness.”  --S.S  "
      }, {
        id: 12,
        content: "  “Perhaps they are not stars, but rather openings in heaven where the love of our lost ones pours through and shines down upon us to let us know they are happy.”  --S.B  "
      }, {
        id: 13,
        content: " “There are more things to alarm us than to harm us, and we suffer more often in apprehension than reality.” -- A.R "
      }, {
        id: 14,
        content: " “Love is that condition in which the happiness of another person is essential to your own.”  --Robert A. Heinlein  "
      }, {
        id: 15,
        content: " “Happy people plan actions, they don’t plan results.”  --Dennis Waitley  "
      }, {
        id: 16,
        content: " “Happiness is when what you think, what you say, and what you do are in harmony.” --Mahatma Gandhi  "
      }, {
        id: 17,
        content: " “The only joy in the world is to begin.”  ---S.S  "
      }, {
        id: 18,
        content: "  “Some cause happiness wherever they go; others whenever they go”  --Oscar Wilde  "
      }, {
        id: 19,
        content: " “Time you enjoy wasting is not wasted time.” --Marthe Troly-Curtin  "
      }, {
        id: 20,
        content: "  “Nobody can be uncheered with a balloon”  "
      }, {
        id: 21,
        content: " “What you do not want done to yourself, do not do to others.”  "
      }, {
        id: 22,
        content: " “There is only one cause of unhappiness: the false beliefs you have in your head, beliefs so widespread, so commonly held, that it never occurs to you to question them.”  "
      }, {
        id: 23,
        content: " “Happiness is not something ready-made. It comes from your own actions.”  "
      }, {
        id: 24,
        content: "  “When one door of happiness closes, another opens, but often we look so long at the closed door that we do not see the one that has been opened for us.”  "
      }, {
        id: 25,
        content: "  “Happiness depends upon ourselves.”  "
      }, {
        id: 26,
        content: " “The reason people find it so hard to be happy is that they always see the past better than it was, the present worse than it is, and the future less resolved than it will be.”"
      }, {
        id: 27,
        content: "  “It is more fitting for a man to laugh at life than to lament over it.”  "
      }, {
        id: 28,
        content: "  “If men would consider not so much wherein they differ, as wherein they agree, there would be far less of uncharitableness and angry feeling in the world.”  "
      }, {
        id: 29,
        content: "  “Happiness is having a large, loving, caring, close-knit family in another city.”  "
      }, {
        id: 30,
        content: "  “Happiness is not in the mere possession of money; it lies in the joy of achievement, in the thrill of creative effort.”  "
      },
      {
        id: 31,
        content: "  “The pleasure which we most rarely experience gives us greatest delight.”  "
      },

    ]
    this.todaysThout = this.thouths[new Date().getDate()].content;
  }
  loggedUserForLoogoutCheck: any;
  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    let url: string = this.RoleGuardService.returnUrl;
    let user = sessionStorage.getItem('loggedUser');
    // this.loggedUserForLoogoutCheck = JSON.parse(user);
    if (this.loggedUserForLoogoutCheck) {
      this.router.navigateByUrl('/layout/home');
    }
    else {
      sessionStorage.setItem('loggedUser', null);
      this.companyMasterService.show();
    }
  }
  fgotpass() {
    this.flag = !this.flag;
    this.signUpFlag = false;
 }
  backMail(){
    this.otpflag=false;
    this.signUpFlag = false;
  }
  onSubmit() {
    this.Spinner.show();
    // if(this.loginmodel.userName == 'Admin'){

    //   let model = {
    //     username: this.loginmodel.userName,
    //     password: this.loginmodel.password,
    //     roleName:"Admin"
    //   }
    //   let url = "/AdminSignIn"
    //   this.dataService.createRecord(url, model)
    //   .subscribe( data => {
    //     this.Spinner.hide();
    //     console.log(data);
       
    //     this.loggedUser= JSON.stringify(data.result);
    //     sessionStorage.setItem('loggedUser', this.loggedUser);
    //     this.router.navigateByUrl("/layout/settingpage");
    //   })
    // }
  
      this.loginservice.postloginservice(this.loginmodel).subscribe(data => {
      
        this.Spinner.hide();
        this.loggedUsers=JSON.stringify(data);
        console.log("this is the data of logged user",this.loggedUsers)
        sessionStorage.setItem('loggeduser', this.loggedUsers);
        // sessionStorage.setItem('userName', this.loginmodel.userName)
        // sessionStorage.setItem('passward', this.loginmodel.password)
        

        // this.fullName = data['result']['fullName'];
        // this.lastName = data['result']['lastName'];
        // this.loginresponse = data['result']['compId'];
        // this.toastr.success(this.fullName + " " + this.lastName, 'WELCOME');
        // sessionStorage.setItem('aloginFlag', 'true');
        // this.SessionServ.Save(data.result);
        sessionStorage.setItem('themeColor', 'nav-pills-blue');
        this.router.navigateByUrl("/layout/home");
        this.toastr.success("Login Successfully")
        // if(data['result']['employeeStatus'] == "PreOnBoarding"){
        //   this.fullName = data['result']['fullName'];
        //   this.lastName = data['result']['lastName']
        //   this.loginresponse = data['result']['compId']
        //   this.toastr.success(this.fullName + " " + this.lastName, 'WELCOME');
        //   data.result.mrollMaster = {};
        //   data.result.mrollMaster.profile = true;
        //   this.SessionServ.Save(data.result);
        //   sessionStorage.setItem('themeColor', 'nav-pills-blue');
        //   // this.router.navigateByUrl('/layout/emppims')
        //   sessionStorage.setItem('whoClicked','EMP');
        //   sessionStorage.setItem('IsEmployeeEdit', 'true');
        //   this.router.navigateByUrl('/layout/emppims/empdetails?q=personal');
        // }
        // else if (data['result']['loginFlag'] == false) {
        //   this.fullName = data['result']['fullName'];
        //   this.lastName = data['result']['lastName']
        //   this.loginresponse = data['result']['compId']
        //   this.toastr.success(this.fullName + " " + this.lastName, 'WELCOME');
        //   this.SessionServ.Save(data.result);
        //   sessionStorage.setItem('themeColor', 'nav-pills-blue');
        //   this.router.navigateByUrl('/layout/emppims')
        // }
        // else {
         
        // }
        // const userId = 'user001';
        // this.messagingService.requestPermission(userId)
        // this.messagingService.receiveMessage()
        // this.message = this.messagingService.currentMessage
      }, (err: any) => {
        if(err.json().message =="Could not open JPA EntityManager for transaction; nested exception is javax.persistence.PersistenceException: org.hibernate.exception.JDBCConnectionException: Unable to acquire JDBC Connection" || err.json().message == "Could not open JPA EntityManager for transaction; nested exception is javax.persistence.PersistenceException: org.hibernate.exception.GenericJDBCException: Unable to acquire JDBC Connection"){
          this.toastr.error("please wait for a while");
          // this.router.navigateByUrl('/layout/home');
        }
        else if(err.json().message){
          this.toastr.error(err.json().message);
          
        }
        else{
          this.toastr.error("please wait for a while ");
          // this.router.navigateByUrl('/layout/home');
        }
        
        this.Spinner.hide();
      });
    }
   
  
  otpflag : boolean = false;
  postForgrtPass() {
    this.Spinner.show()
    this.loginservice.postForgetPass(this.ForgoePassw).subscribe((res: Response) => {
        this.message = res['message'];
        this.userId=res['result']['userId']
        this.toastr.success(this.message);
        this.otpflag=true;
        this.Spinner.hide()
      }, (err: any) => {
        this.otpflag=false;
        if(err.status == 0){
        this.toastr.error('Internet connection not available');
        }
        else{
          this.toastr.error('Invalid Mail');
        }
        this.Spinner.hide();
        
      });
  }
 eyeFlag : boolean = false;
  seePassword() {
    var x: any = document.getElementById("myInput");
    if (x.type === "password") {
      this.eyeFlag = true;
      x.type = "text";
    } else {
      this.eyeFlag = false;
      x.type = "password";
    }
  }

  eyeFlagn : boolean = false
  seePasswordn() {
    var x: any = document.getElementById("newpassword");
    if (x.type === "password") {
      this.eyeFlagn = true;
      x.type = "text";
    } else {
      this.eyeFlagn = false;
      x.type = "password";
    }
  }

  eyeFlagc : boolean = false
  seePasswordc() {
    var x: any = document.getElementById("confirmnewpassword");
    if (x.type === "password") {
      this.eyeFlagc = true;
      x.type = "text";
    } else {
      this.eyeFlagc = false;
      x.type = "password";
    }
  }
  sameotp : boolean = false;
  samepassword(){
  
this.otpFun()
  }


  otpFun(){
    let obj={
       userId : this.userId,
       otp : this.ForgoePassw.otp
       

    }
    let url1 = this.baseurl + '/otp';
    this.httpService.post(url1,obj).subscribe((data: any) => {
        this.message = data['message'];
        this.toastr.success(this.message);
        this.otpflag = false;
        this.sameotp = true;
      this.otpflag=false
      }, (err: HttpErrorResponse) => { 
        if(err.status == 0){
        this.toastr.error('Please Check Internet connection');
        }
        else{
          
        this.toastr.error('Please Enter Valid OTP!');
        }
      });
    

  }

  changePass(){
    this.ForgoePassw.passWord
    this.ForgoePassw.userId = this.userId
    let obj={
      passWord :this.ForgoePassw.passWord,
      userId :this.userId
    }
    let url1 = this.baseurl + '/ChnageUserPassword';
    this.httpService.post(url1,obj).subscribe((data: any) => {
        this.message = data['message'];
        this.toastr.success(this.message);
        this.otpflag=false
        window.location.reload();
      }, (err: HttpErrorResponse) => {
        if(err.status == 0){
          this.toastr.error('Please Check Internet Connection');
        }
        else{
          this.toastr.error('server side error!..');
        }
      });
    }

  onSubmitAdminForm(){
    let url = "/adminRegistration";
    console.log("sigunupdata",this.adminLoginmodel)
    this.dataService.createRecord(url,this.adminLoginmodel)
    .subscribe(data =>{

        if(data.statusCode == '201'){
          this.toastr.success(data.message);
          this.signUpFlag = false;
          this.flag = true;
        }
        if(data.statusCode == '500'){
          this.toastr.error(data.result.message); 
        }
    },(err:HttpErrorResponse)=>{
         this.toastr.error('Admin already exist');
    })
  }

  goToSignUp(){
    this.signUpFlag = !this.signUpFlag;
    this.flag = false;
    this.otpflag = false;
    this.sameotp = false;
  }
  }


