import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from '../../app/login/login.component';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { MastersComponent } from 'src/app/masters/masters.component';
import { EmpdashboardModule } from 'src/app/empdashboard/empdashboard.module'
import { HomeComponent } from '../home/home.component';
import { AuthGuard } from '../guards/auth-guard.service';
import { RoleGuard } from '../guards/role-guard.service';
import { ReportopenComponent } from '../reportopen/reportopen.component';
import { UploadDocumentComponent } from '../upload-document/upload-document.component';
import { ProvisionalpassComponent } from '../provisionalpass/provisionalpass.component';
import { SalaryHeadComponent } from '../salary-head/salary-head.component';
import { SafetyRemarkComponent } from '../safety-remark/safety-remark.component';
import { EditcontractorlabourComponent } from '../editcontractorlabour/editcontractorlabour.component';
import { GetOriginalPassComponent } from '../get-original-pass/get-original-pass.component';
import { LabourApproveRejectListComponent } from '../labour-approve-reject-list/labour-approve-reject-list.component';
import { LabourReporComponent } from '../labour-repor/labour-repor.component';
import { AllReportsComponent } from '../all-reports/all-reports.component';
import { SignaturePadComponent } from '../signature-pad/signature-pad.component';
import { HodApprovalsComponent } from '../hod-approvals/hod-approvals.component';
import { SupervisorLabourListComponent } from '../supervisor-labour-list/supervisor-labour-list.component';
const appRoutes: Routes = [

  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'reportopen',
    component: ReportopenComponent
  },
  {
    path: 'layout',
    component: LayoutComponent,

    children: [
      {
        path: 'master',
        //component: 'MastersComponent'
        loadChildren: 'src/app/masters/master.module#MasterModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'holiday2',
        loadChildren: 'src/app/holidaymaster/holidaymaster.module#HolidayModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'uploadDocument',
        component: UploadDocumentComponent
      },
      {
        path: 'labourApproveRejectList',
        component: LabourApproveRejectListComponent
      },
      {
         path: 'genratepass', component:  ProvisionalpassComponent
        },
        {
        path:'originalPass',component:GetOriginalPassComponent
        },
        {
          path: 'salaryHead', component:  SalaryHeadComponent
         },
         {
          path: 'labourReport', component:  LabourReporComponent
         },
         {
          path: 'AllReports', component:  AllReportsComponent
         },
         {
          path: 'HodApprovals', component:  HodApprovalsComponent
         },
         {
          path: 'signatureAuthority', component:  SignaturePadComponent
         },
         
         {
          path: 'safetyRemark', component:  SafetyRemarkComponent
         },
         {
          path: 'editcontractorLabour', component:  EditcontractorlabourComponent
         },
         {
          path: 'LabourList', component:  SupervisorLabourListComponent
         },
         
      {
        path: 'shift',
        loadChildren: 'src/app/shiftmaster/shiftmaster.module#ShiftModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'maincontractmaster',
        loadChildren: 'src/app/contractormaster/contractormaster.module#ContractorModule',
      
      },
      {
        path: 'weeklymaster',
        loadChildren: 'src/app/weeklyoffmaster/weeklyoffmaster.module#WleeklyoffModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'paymaster',
        loadChildren: 'src/app/payperiodmaster/payperiodmaster.module#PayperiodModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'asset',
        loadChildren: 'src/app/assetmaster/assetmaster.module#AssetmasterModule',
        canActivate: [RoleGuard]
        
      },
      {
        path: 'documentmaster',
        loadChildren: 'src/app/document-master/document-master.module#DocumentMasterModule',
        canActivate: [RoleGuard]
      },
        {
        path: 'contractorOnBoarding',
        loadChildren: 'src/app/contractor-on-boarding/contractor-on-boarding.module#ContractorOnBoardingModule',
        
      },
      {
        path: 'contractorLabour',
        loadChildren: 'src/app/contractor-labour/contractor-labour.module#ContractorLabourModule',
        
      },
      {
        path: 'contractorLabourAdd',
        loadChildren: 'src/app/contractors-labour-add/contractors-labour-add.module#ContractorsLabourAddModule',
        
      },
      {
        path: 'contractorLabourList',
        loadChildren: 'src/app/contractor-labour-list/contractor-labour-list.module#ContractorLabourListModule',
        
      },
      {
        path: 'contractorinfolist',
        loadChildren: 'src/app/contractor-info-labour-list/contractor-info-labour-list.module#ContractorInfoLabourListModule',
        
      },
      {
        path: 'contractorlabourMasterList',
        loadChildren: 'src/app/contractor-labour-master-lists/contractor-labour-master-lists.module#ContractorLabourMasterListsModule',
        
      },
      {
        path: 'emloyeestaffList',
        loadChildren: 'src/app/employee-staff/employee-staff.module#EmployeeStaffModule',
        
      },
      {
        path: 'frontdeskApproval',
        loadChildren: 'src/app/front-desk-approval/front-desk-approval.module#FrontDeskApprovalModule',
        
      },
      {
        path: 'medicalLabourList',
        loadChildren: 'src/app/medical-labour-list/medical-labour-list.module#MedicalLabourListModule',
        
      },
      {
        path: 'uploadDocumet',
        loadChildren: 'src/app/uploadrejected-document/uploadrejected-document.module#UploadrejectedDocumentModule',
        
      },
      
      {
        path: 'frontdeskdocumenthistorylist',
        loadChildren: 'src/app/frontdesk-document-history-list/frontdesk-document-history-list.module#FrontdeskDocumentHistoryListModule',
        
      },
      {
        path: 'FrontApproveReject',
        loadChildren: 'src/app/front-approve-reject-labour/front-approve-reject-labour.module#FrontApproveRejectLabourModule',
        
      },
      {
        path: 'medicaleExaminationform',
        loadChildren: 'src/app/medical-examinationform/medical-examinationform.module#MedicalExaminationformModule',
        
      },
      {
        path: 'salaryStructure',
        loadChildren: 'src/app/salary-structure/salary-structure.module#SalaryStructureModule',
        
      },
      {
        path: 'safetylabourDoneList',
        loadChildren: 'src/app/safety-labour-done-list/safety-labour-done-list.module#SafetyLabourDoneListModule',
        
      },
      {
        path: 'safetydoneList',
        loadChildren: 'src/app/safety-done-list/safety-done-list.module#SafetyDoneListModule',
        
      },
      {
        path: 'contractorLabourLists',
        loadChildren: 'src/app/contractorlabour-lists/contractorlabour-lists.module#ContractorlabourListsModule',
        
      },
      {
        path: 'addempStaff',
        loadChildren: 'src/app/add-emp-staff/add-emp-staff.module#AddEmpStaffModule',
        
      },
      // {
      //   path: 'employee',
      //   loadChildren: 'src/app/employee/employee.module#EmployeeModule'
      // },
      {
        path: 'employeelist',
        loadChildren: 'src/app/employeelist/employeelist.module#EmployeelistModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'emppimsview',
        loadChildren: 'src/app/view-employee/emppimsmodule.module#EmppimsViewModule',
       
      },
      {
        path: 'pimshr',
        loadChildren: 'src/app/pimshr/pimshr.module#PimshrModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'emppims',
        loadChildren: 'src/app/emppims/emppims.module#EmppimsModule'
      },
      {
        path: 'configuration',
        loadChildren: 'src/app/configuration/configuration.module#ConfigurationModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'leaveconfiguration',
        loadChildren: 'src/app/leaveconfiguration/leaveconfiguration.module#LeaveConfigurationModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'importemployee',
        loadChildren: 'src/app/importemployee/importemployee.module#ImportemployeeModule',
        canActivate: [RoleGuard]
      },
      // {
      //   path: 'rolemaster',
      //   loadChildren: 'src/app/rolemaster/rolemaster.module#RolemasterModule',
      //   canActivate: [RoleGuard]
      // },
      {
        path: 'announcement',
        loadChildren: 'src/app/announcement/announcement.module#AnnouncementModule'
      },
      {
        path: 'changepassword',
        loadChildren: 'src/app/changepassword/changepassword.module#ChangepasswordModule'
      },
      {
        path: 'viewempdetails',
        loadChildren: 'src/app/viewempdetails/viewempdetails.module#ViewempdetailsModule'
      },
      
      {
        path: 'leaverequest',
        loadChildren: 'src/app/leaverequest/leaverequest.module#LeaverequestModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'otherrequests',
        loadChildren: 'src/app/otherrequests/otherrequests.module#OtherrequestsModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'dashboard',
        loadChildren: 'src/app/dashboard/dashboard.module#DashboardModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'empdashboard',
        loadChildren: 'src/app/empdashboard/empdashboard.module#EmpdashboardModule'
      },
      {
        path: 'yearlycalendar',
        loadChildren: 'src/app/yearlycalendar/yearlycalendar.module#YearlycalendarModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'regularisation',
        loadChildren: 'src/app/regularisation/regularisation.module#RegularisationModule'
      },
      {
        path: 'newregularisation',
        loadChildren: 'src/app/new-regularisation/newregularisation.module#NewRegularisationModule',
        
      },
      {
        path: 'contractordataApprovals',
        loadChildren: 'src/app/contractordata-approvals/contractordata-approvals.module#ContractordataApprovalsModule',
        
      },


     
      {
        path: 'mainManpowerRequest',
        loadChildren: 'src/app/main-manpower-request/main-manpower-request.module#MainManpowerRequestModule',
        
      },
      {
        path: 'contractorsDetails',
        loadChildren: 'src/app/contractors-details/contractors-details.module#ContractorsDetailsModule',
        
      },
      {
        path: 'frontDeskAssets',
        loadChildren: 'src/app/frontdesk-assets/frontdesk-assets.module#FrontdeskAssetsModule',
        
      },
      {
        path: 'safetyapproval',
        loadChildren: 'src/app/safetyassets/safetyassets.module#SafetyassetsModule',
        
      },
     
      {
        path: 'queryadmin',
        loadChildren: 'src/app/queryadmin/queryadmin.module#QueryadminModule'
      },
      {
        path: 'Device',
        loadChildren: 'src/app/device-master/DviceMaster.module#DeviceModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'resetpassword',
        loadChildren: 'src/app/resetpassword/resetpassword.module#ResetpasswordModule'
      },
      {
        path: 'extraworkreq',
        loadChildren: 'src/app/extraworkreq/extraworkreq.module#ExtraworkreqModule',
        canActivate: [RoleGuard]
      },
      // {
      //   path:'newyearly',
      //   loadChildren: 'src/app/newyearly/newyearly.module#NewyearlyModule'
      // },
      {
        path: 'creatreport',
        loadChildren: 'src/app/creatreport/creatreport.module#CreatreportModule'
      },
     
      {
        path: 'reports',
        loadChildren: 'src/app/reports/reports.module#ReportsModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'absentreports',
        loadChildren: 'src/app/absentreport/absentreports.module#AbsentReportsModule'
      },
      {
        path: 'workingDurationreports',
        loadChildren: 'src/app/workingdurationreport/workingdurationreports.module#WorkingDurationReportsModule'
      },
      {
        path: 'contractorworkingDurationreports',
        loadChildren: 'src/app/contractor-workingduration/contractor-workingduration.module#ContractorWorkingdurationModule'
      },
      {
        path: 'contractorMusterReport',
        loadChildren: 'src/app/contractor-muster-report/contractor-muster-report.module#ContractorMusterReportModule'
      },

      {
        path: 'contractorMultiplePunchReport',
        loadChildren: 'src/app/contractor-multiple-punch-report/contractor-multiple-punch-report.module#ContractorMultiplePunchReportModule'
      },
      {
        path: 'optinalholiday',
        loadChildren:'src/app/optinalholiday/optinalholiday.module#OptinalholidayModule'
      },
      {
        path: 'monthlyovertimereports',
        loadChildren: 'src/app/monthlyovertimereport/monthlyovertimereports.module#MonthlyovertimeReportsModule'
      },
      {
        path: 'monthlyattendancereports',
        loadChildren: 'src/app/monthly-attendence-report/monthlyattendencereports.module#MonthlyAttendenceReportsModule'
      },
      {
        path: 'multiplereportspunches',
        loadChildren: 'src/app/multiple-report-punches/multiplereportspunches.module#MultipleReportsPunchesModule'
      },
      {
        path: 'shiftassignmentreports',
        loadChildren: 'src/app/shift-assignmentreport/shiftAsdsignmentreport.module#shiftassignmnentReportsModule'
      },
      {
        path: 'headcountreports',
        loadChildren: 'src/app/head-count-report/headCountReports.module#HeadCountReportsModule'
      },
      {
        path: 'leavereports',
        loadChildren: 'src/app/leave-report/leaveReports.module#LeaveReportsModule'
      },
      {
        path: 'leaveRegisterreports',
        loadChildren: 'src/app/leave-register-report/leaveRegisterreport.module#LeaveRegisterReportsModule'
      },
      {
        path: 'leaveTakenReportDaywise',
        loadChildren: 'src/app/leavetakenreportdaywise/leavetakenReportDaywise.module#LeaveTakenDaywiseReportsModule'
      },
      {
        path: 'leaveTakenReport',
        loadChildren: 'src/app/leave-taken-report/leaveTakenReport.module#LeaveTakenReportModule'
      },
      {
        path: 'latecomingreports',
        loadChildren: 'src/app/latecoming-report/lateComing.module#LateComingReportsModule'
      },
      {
        path: 'earlygoingreports',
        loadChildren: 'src/app/early-going-report/EarlyGoing.module#EarlyGoingReportsModule'
      },
     

      //WorkForce reports routing
      {
        path: 'workForceReports',
        loadChildren:'src/app/work-force-reports/work-force-reports.module#WorkForceReportsModule'
      },


      {
        path: 'employeemaster',
        loadChildren: 'src/app/employeemaster/employeemaster.module#EmployeemasterModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'reportview',
        loadChildren: 'src/app/reportview/reportview.module#ReportviewModule',
        // canActivate: [RoleGuard]
      },
    
  
  
  

      {
        path: 'attendencereportview',
        loadChildren: 'src/app/emp-attendence-report/empAttendenceReport.module#ReportAttendenceviewModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'leaveblock',
        loadChildren: 'src/app/leaveblock/leaveblock.module#LeaveblockModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'shiftupload',
        loadChildren: 'src/app/shiftupload/shiftupload.module#ShiftuploadModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'weekly-off',
        loadChildren: 'src/app/weekly-off/weekly-off.module#WeeklyOffModule',
      },
      {
        path: 'hrdashboard',
        loadChildren: 'src/app/hrdashboard/hrdashboard.module#HrdashboardModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'customizedashboard',
        loadChildren: 'src/app/customizedashboard/customizedashboard.module#CustomizedashboardModule',
        canActivate: [RoleGuard]
      },
     
      {
        path: 'rolemasterr',
        loadChildren: 'src/app/rolemasterr/rolemaster.module#RolemasterModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'settingpage',
        loadChildren: 'src/app/settingpage/settingpage.module#SettingpageModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'emphistory',
        loadChildren: 'src/app/emphistory/emphistory.module#EmphistoryModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'socialpage',
        loadChildren: 'src/app/socialpage/socialpage.module#SocialpageModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'clearancepage',
        loadChildren: 'src/app/clearancepage/clearancepage.module#ClearancepageModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'clearancedetailsemp',
        loadChildren: 'src/app/clearancedetailsemp/clearancedetailsemp.module#ClearancedetailsempModule'
      },
      {
        path: 'approverlevel',
        loadChildren: 'src/app/approverlevel/approverlevel.module#ApproverlevelModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'company',
        loadChildren: 'src/app/company/company.module#CompanyModule',
      
      },

      {
        path: 'resignationpage',
        loadChildren: 'src/app/resignationpage/resignationpage.module#ResignationpageModule'
      },
      {
        path: 'transportmaster',
        loadChildren: 'src/app/transportmaster/transportmaster.module#TransportmasterModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'mailsetting',
        loadChildren: 'src/app/mailsetting/mailsetting.module#MailsettingModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'seprationsetting',
        loadChildren: 'src/app/seprationsetting/seprationsetting.module#SeprationsettingModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'bulkuploading',
        loadChildren: 'src/app/bulkuploading/bulkuploading.module#BulkuploadingModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'deactivateemployee',
        loadChildren: 'src/app/deactivateemployee/deactivateemployee.module#DeactivateemployeeModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'resetpwdforsuperv',
        loadChildren: 'src/app/resetpwdforsuperv/resetpwdforsuperv.module#ResetpwdforsupervModule'
      },
     
      {
        path: 'supervisorleaverequest',
        loadChildren: 'src/app/supervisorleave-request/supervisor-request.module#SupervisorLeaveRequestModule',
        canActivate: [RoleGuard]
      },
     
      {
        path: 'selfcertificationlist',
        loadChildren: 'src/app/selfcertificationlist/selfcertificationlist.module#SelfcertificationlistModule',
        canActivate: [RoleGuard]
      },
    
      {
        path: 'leavedashboard',
        loadChildren: 'src/app/leavedashboard/leavedashboard.module#LeavedashboardModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'exitinterviewreport',
        loadChildren: 'src/app/exitinterviewreport/exitinterviewreport.module#ExitinterviewreportModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'advanceleave',
        loadChildren: 'src/app/advanceleave/advanceleave.module#AdvanceleaveModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'mymenu',
        loadChildren: 'src/app/mymenu/mymenu.module#MymenuModule',
        canActivate: [RoleGuard]
      },
      {
        path: 'attendancemenu',
        loadChildren: 'src/app/attendancemenu/attendancemenu.module#AttendancemenuModule',
        canActivate: [RoleGuard]
      }
      ,
      {
        path: 'myteamattendancemenu',
        loadChildren: 'src/app/myteamattendancemenu/myteamattendancemenu.module#MyteamattendancemenuModule',
        
      },
   
      {
        path: 'workforcemenu',
        loadChildren: 'src/app/workforcemenu/workforcemenu.module#WorkforcemenuModule',
        // canActivate: [RoleGuard]
      },
    
      {
        path: 'matrixmanager',
        loadChildren: 'src/app/matrixmanager/matrixmanager.module#MatrixmanagerModule',
        canActivate: [RoleGuard]
      },
     {
        path: 'teammonthlycalendar',
        loadChildren: 'src/app/teammonthlycalendar/teammonthlycalendar.module#TeammonthlycalendarModule'
      },
      {
        path: 'settingsmenu',
        loadChildren: 'src/app/settingsmenu/settingsmenu.module#SettingsmenuModule'
      },
      {
        path: 'preonboarding',
        loadChildren: 'src/app/preonboarding/preonboarding.module#PreonboardingModule',
        // canActivate: [RoleGuard]
      },
      {
        path: 'gatepass',
        loadChildren: 'src/app/gatepass/gatepass.module#GatepassModule'
      },
      {
        path: 'applicationshistory',
        loadChildren: 'src/app/applicationshistory/applicationshistory.module#ApplicationshistoryModule'
      },
      {
        path: 'addcontractoremp',
        loadChildren: 'src/app/addcontractoremp/addcontractoremp.module#AddcontractorempModule'
      },
      {
        path: 'addmanagementemp',
        loadChildren: 'src/app/addmanagementemp/addmanagementemp.module#AddmanagementempModule'
      },
      {
        path: 'satutarysetting',
        loadChildren: 'src/app/satutarysetting/satutarysetting.module#SatutarysettingModule'
      },
      {
        path: 'update_employee',
        loadChildren: 'src/app/employeeupdate/employeeupdate.module#EmployeeupdateModule',
        // canActivate: [RoleGuard]
      },

      /* Leave Reports Module Routing Start */
      {
        path: 'leaveReports',
        loadChildren: 'src/app/leave-reports/leave-reports.module#LeaveReportsModule'
      },

      /* Leave Reports Module Routing End */
     
      {
        path: 'contractorregisterreports',
        loadChildren: 'src/app/contractorregisterreport/contractorregisterreport.module#ContractorregisterreportModule'
      },
      {
        path: 'musterreports',
        loadChildren: 'src/app/musterrerport/musterrerport.module#MusterrerportModule'
      },   
      {
        path: 'attendanceProcessing',
        loadChildren: 'src/app/attendance-processing/attendance-processing.module#AttendanceProcessingModule'
      },
      {
        path: 'queryRaisedReports',
        loadChildren: 'src/app/query-raised-report/query-raised-report.module#QueryRaisedReportModule'
      },
      {
        path:'monthlock',
        loadChildren: 'src/app/month-lock/month-lock.module#MonthLockModule'
      },
      {
        path:'newapproval',
        loadChildren: 'src/app/neapprovalsetting/neapprovalsetting.module#NeapprovalsettingModule'
      },

      {
        path: 'contractorpresentreport',
        loadChildren: 'src/app/contractor-present-report/contractor-present-report.module#ContractorPresentReportModule'
      },
   

      // for passgenrate card
    

     
    ]
  },
  { path: '**', redirectTo: 'login' }
]

export const routing = RouterModule.forRoot(appRoutes,{ useHash: false });
