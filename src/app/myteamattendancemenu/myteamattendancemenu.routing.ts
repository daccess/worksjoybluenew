import { Routes, RouterModule } from '@angular/router'
import { MyteamattendancemenuComponent } from './myteamattendancemenu.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: MyteamattendancemenuComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'myteamattendancemenu',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'myteamattendancemenu',
                    component: MyteamattendancemenuComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);