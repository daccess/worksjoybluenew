import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myteamattendancemenu',
  templateUrl: './myteamattendancemenu.component.html',
  styleUrls: ['./myteamattendancemenu.component.css']
})
export class MyteamattendancemenuComponent implements OnInit {
  loggedUser: any = {};
  hrMode:any;  
  constructor() { }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);

    let hrMode = sessionStorage.getItem('hrMode');
    this.hrMode = JSON.parse(hrMode);
  
  }

}
