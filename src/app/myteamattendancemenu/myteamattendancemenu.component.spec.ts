import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyteamattendancemenuComponent } from './myteamattendancemenu.component';

describe('MyteamattendancemenuComponent', () => {
  let component: MyteamattendancemenuComponent;
  let fixture: ComponentFixture<MyteamattendancemenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyteamattendancemenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyteamattendancemenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
