import { routing } from './myteamattendancemenu.routing';
import { NgModule } from '@angular/core';
import { MyteamattendancemenuComponent } from './myteamattendancemenu.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        MyteamattendancemenuComponent

    
    ],
    imports: [
    routing,
    CommonModule
    ],
    providers: []
  })
  export class  MyteamattendancemenuModule { 
      constructor(){

      }
  }
  