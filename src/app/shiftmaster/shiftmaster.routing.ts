import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';
import { ShiftmasterComponent } from './shiftmaster.component';
import {ShiftComponent } from './pages/shift/shift.component';
import{ ShiftGroupMasterComponent } from './pages/shift-group-master/shift-group-master.component';
import { ShiftpatternComponent } from './pages/shiftpattern/shiftpattern.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: ShiftmasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'shiftgroupmaster',
                    pathMatch :'full'
                    
                },
                {
                    path:'shiftgroupmaster',
                    component: ShiftGroupMasterComponent
                },
                {
                    path:'shiftmaster',
                    component: ShiftComponent
                },
                {
                    path:'shiftpattern',
                    component: ShiftpatternComponent
                }
            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);