import { routing } from './shiftmaster.routing';
import { NgModule } from '@angular/core';
 
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
 import { ShiftmasterComponent } from './shiftmaster.component';
import { ShiftComponent } from './pages/shift/shift.component';
import { ShiftGroupMasterComponent } from './pages/shift-group-master/shift-group-master.component';
import { shiftGroupMasterService } from '../shared/services/shiftgroupservice';
import { shiftMasterService } from '../../app/shared/services/shiftmasterservice';
import { ShiftpatternComponent } from './pages/shiftpattern/shiftpattern.component';
// import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
// import { HolidaymComponent } from './pages/holidaym/holidaym.component';


@NgModule({
    declarations: [
        ShiftmasterComponent,
        ShiftComponent,
        ShiftGroupMasterComponent,
        ShiftpatternComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [shiftGroupMasterService,shiftMasterService]
  })
  export class ShiftModule { 
      constructor(){

      }
  }
  