import { Component, OnInit, Inject } from '@angular/core';
import * as moment from 'moment/moment.js';
import { DOCUMENT } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
declare var $;
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { shiftMasterService } from '../../../shared/services/shiftmasterservice';
import { ShiftMaster } from '../../../shared/model/shiftmaster';
import { ChangeDetectorRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-shift',
  templateUrl: './shift.component.html',
  styleUrls: ['./shift.component.css'],
  providers:[shiftMasterService]
})
export class ShiftComponent implements OnInit {

  selectedShiftGroup=[];
  dropdownList: any
  selectedItems: any
  dropdownSettings: {}

  baseurl = MainURL.HostUrl;
  ShiftGroupId: any;
  shiftGroupMasterdata: any;
  //selectedGroupobj: any;
  selectedflexibleshift: any;
  // dailyworkinghrs: any;
  // divtime: any
  shiftmastermodel: ShiftMaster;

  startTime: any;
  endTime: any;
  totalHours: any;
  totalMinutes: any;
  clearMinutes: any;
  totaltime: any;
  //shiftType: any;

  flag : boolean = true;
  flag2 : boolean = false;
  loggedUser: any;
  compId: any;
  Shiftdata:any=[];
  Selectededitobj: any;
  locationid:any;
  isedit = false;
  dltmodelFlag: boolean;
  dltObje: any;
  todaysTime: string;
  errflag: boolean;
  locationdata: any;
  locationwisedata:any=[];
  locationwisedatas:any=[];
  selectedLocationobj: any;
  selectedGroupobj: number;
  updatedshiftdata: any;
  selectedLocationId: any=0;
  Shiftgroupdata: any;
  constructor(@Inject(DOCUMENT) document, 
  public httpService: HttpClient,
  private toastr: ToastrService, 
  public shiftservice: shiftMasterService,
  public ch : ChangeDetectorRef,
  public Spinner :NgxSpinnerService) { 
    this.shiftmastermodel = new ShiftMaster();
    //this.ShiftGroupId = 1;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.shiftGroupActive(this.selectedLocationId)
  }

ngOnInit() {
  this.getLocationList();
  if (new Date().getMinutes() < 10) {
    if (new Date().getHours() < 10) {
      this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
    }
    else {
      this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
    }
  }
  else {
    if (new Date().getHours() < 10) {
      this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
    }
    else {
      this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
    }
  }
  this.shiftmastermodel.inTime= this.todaysTime;
  this.shiftmastermodel.endTime = this.todaysTime;
  this.shiftmastermodel.signInBeforeTime = this.todaysTime;
  this.shiftmastermodel.flexibleShift = 'No'
  this.selectedflexibleshift = "No"
  this.shiftmastermodel.shiftType ="0"
  //this.shiftmastermodel.locationid=this.selectedLocationobj;
  this.shiftMasterdata()
  // document.getElementById('divtime').style.display="none";
  this.shiftmastermodel.inTime;
  this.shiftmastermodel.dailyWorkingHrs = this.totaltime;
    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'shiftGroupId',
      textField: 'shiftGroupName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }
 shiftGroupActive(e){
  this.selectedLocationId=e;
        if(this.loggedUser.roleHead=='HR'){
        let url = this.baseurl + '/ShiftGroup/Active/';
        this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
            this.Shiftgroupdata = data["result"];
           // console.log(" Shiftgroupdata", this.Shiftgroupdata)
        })
        }    
        else if(this.loggedUser.roleHead=='Admin'){
            let url = this.baseurl + '/ShiftGroup/Active/';
            this.httpService.get(url + this.compId+'/'+this.selectedLocationId).subscribe(data => {
                this.Shiftgroupdata = data["result"];
              //console.log(" Shiftgroupdata", this.Shiftgroupdata)

        })
        }
  
}
edit(object){
  this.Selectededitobj = object.shiftMasterId;
  this.isedit = true;
  this.StatusFlag=true;
  this.flag1 = object.status;
  this.editobject(this.Selectededitobj);
}
flag1 : boolean = false;
editobject(getobject){
 
    let urledit = this.baseurl + '/shiftMasterById/';
    this.httpService.get(urledit + getobject).subscribe(data => {
      
        this.shiftmastermodel.shiftName = data['result']['shiftName'];
        this.shiftmastermodel.shortName = data['result']['shortName'];
        this.shiftmastermodel.shiftDesc = data['result']['shiftDesc'];
        this.shiftmastermodel.shiftCode = data['result']['shiftCode'];
        this.shiftmastermodel.inTime = data['result']['inTime'];
        this.shiftmastermodel.endTime = data['result']['endTime'];
        this.shiftmastermodel.shiftType = data['result']['shiftType'];
        this.shiftmastermodel.shiftColor = data['result']['shiftColor'];
        this.shiftmastermodel.selectedShiftGroup = data['result']['selectedShiftgrp'];
        this.shiftmastermodel.signInBeforeTime = data['result']['signInBeforeTime'];
        this.shiftmastermodel.minutesToWorkForFullDay = data['result']['minutesToWorkForFullDay'];
        this.shiftmastermodel.minutesToWorkForHalfDay = data['result']['minutesToWorkForHalfDay'];
        this.shiftmastermodel.dailyWorkingHrs = data['result']['dailyWorkingHrs'];
        this.shiftmastermodel.flexibleShift = data['result']['flexibleShift'];
        this.shiftmastermodel.flexibleInStartTime = data['result']['flexibleInStartTime'];
        this.shiftmastermodel.flexibleInEndTime = data['result']['flexibleInEndTime'];
        this.shiftmastermodel.coreTimeStartTime = data['result']['coreTimeStartTime'];
        this.shiftmastermodel.coreTimeEndTime = data['result']['coreTimeEndTime'];
        this.shiftmastermodel.flexibleOutStartTime = data['result']['flexibleOutStartTime'];
        this.shiftmastermodel.flexibleOutEndTime = data['result']['flexibleOutEndTime'];
        this.shiftmastermodel.weeklyHrsNeedToWork = data['result']['weeklyHrsNeedToWork'];
        this.shiftmastermodel.shiftAllowance = data['result']['shiftAllowance'];
        this.shiftmastermodel.status = data['result']['status'];
        this.shiftmastermodel.locationId =  data['result']['locationId'];
        this.getShiftGroupLocationwise(this.shiftmastermodel.locationId)
        this.shiftmastermodel.locationId =  data['result']['locationId'];
         //console.log("locationidinedit", this.shiftmastermodel.locationId);
      //console.log("ineditsgn", this.shiftmastermodel.selectedShiftGroup);
      if( this.isedit ==true){
        console.log("falgstatus",this.isedit);
  
        this.shiftmastermodel.shiftName = data['result']['shiftName'];
        this.shiftmastermodel.shortName = data['result']['shortName'];
        this.shiftmastermodel.shiftDesc = data['result']['shiftDesc'];
        this.shiftmastermodel.shiftCode = data['result']['shiftCode'];
        this.shiftmastermodel.inTime = data['result']['inTime'];
        this.shiftmastermodel.endTime = data['result']['endTime'];
        this.shiftmastermodel.shiftType = data['result']['shiftType'];
        this.shiftmastermodel.shiftColor = data['result']['shiftColor'];
        this.shiftmastermodel.selectedShiftGroup = data['result']['selectedShiftgrp'];
        this.shiftmastermodel.signInBeforeTime = data['result']['signInBeforeTime'];
        this.shiftmastermodel.minutesToWorkForFullDay = data['result']['minutesToWorkForFullDay'];
        this.shiftmastermodel.minutesToWorkForHalfDay = data['result']['minutesToWorkForHalfDay'];
        this.shiftmastermodel.dailyWorkingHrs = data['result']['dailyWorkingHrs'];
        this.shiftmastermodel.flexibleShift = data['result']['flexibleShift'];
        this.shiftmastermodel.flexibleInStartTime = data['result']['flexibleInStartTime'];
        this.shiftmastermodel.flexibleInEndTime = data['result']['flexibleInEndTime'];
        this.shiftmastermodel.coreTimeStartTime = data['result']['coreTimeStartTime'];
        this.shiftmastermodel.coreTimeEndTime = data['result']['coreTimeEndTime'];
        this.shiftmastermodel.flexibleOutStartTime = data['result']['flexibleOutStartTime'];
        this.shiftmastermodel.flexibleOutEndTime = data['result']['flexibleOutEndTime'];
        this.shiftmastermodel.weeklyHrsNeedToWork = data['result']['weeklyHrsNeedToWork'];
        this.shiftmastermodel.shiftAllowance = data['result']['shiftAllowance'];
        this.shiftmastermodel.status = data['result']['status'];
        this.shiftmastermodel.locationId =  data['result']['locationId'];
        
        this.shiftmastermodel.locationId =  data['result']['locationId'];
         //console.log("locationidinedit", this.shiftmastermodel.locationId);
      //console.log("ineditsgn", this.shiftmastermodel.selectedShiftGroup);
      }
       
      },
      (err: HttpErrorResponse) => {
      });
  }

uiswitch(event){
  this.flag1 = !this.flag1;
}

timeSet(v){
  this.shiftmastermodel.flexibleShift = v;
  if (new Date().getMinutes() < 10) {
    if (new Date().getHours() < 10) {
      this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
    }
    else {
      this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
    }
  }
  else {
    if (new Date().getHours() < 10) {
      this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
    }
    else {
      this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
    }
  }
  if(this.shiftmastermodel.flexibleShift=="Yes"){
    this.shiftmastermodel.flexibleInStartTime = this.todaysTime;
    this.shiftmastermodel.flexibleInEndTime = this.todaysTime;
    this.shiftmastermodel.coreTimeStartTime = this.todaysTime;
    this.shiftmastermodel.coreTimeEndTime = this.todaysTime;
  }
}

dailyHours(){
  let st = this.shiftmastermodel.inTime.split(":");
  let timeIn1 = st[0];
  var timeIn;
  if(Number(timeIn1) < 10){
    let st1 = timeIn1.split("0");
    timeIn = st1[1];
  }
  else{
    timeIn = timeIn1;
  }
  let et = this.shiftmastermodel.endTime.split(":");
  let timeOut1 = et[0];
  var timeOut;
  if(Number(timeOut1) < 10){
    let et1 = timeOut1.split("0");
    timeOut = et1[1];
  }
  else{
    timeOut = timeOut1;
  }
  this.startTime = moment(this.shiftmastermodel.inTime, 'hh:mm:ss a');
  this.endTime = moment(this.shiftmastermodel.endTime, 'hh:mm:ss a');
  if(timeIn > 12 && timeOut > 12){
    if(timeIn > timeOut){
      let firstDayTime =  moment("24:00", 'hh:mm:ss a');
      let firstDayHours = (firstDayTime.diff(this.startTime, 'hours'));
      let firstDayMinutes = (firstDayTime.diff(this.startTime, 'minutes'));
      let firstDayTotal = firstDayMinutes;

      let middleDayTime =  moment("24:00", 'hh:mm:ss a');
      let middleDateEndTime = moment("12:00", 'hh:mm:ss a');
      let middleDayHours = (middleDayTime.diff(middleDateEndTime, 'hours'));
      let middleDayMinutes = (middleDayTime.diff(middleDateEndTime, 'minutes'));
      let middleDayTotal = middleDayMinutes;

      let secondDayTime =  moment("12:00", 'hh:mm:ss a');
      let secondDayHours = (this.endTime.diff(secondDayTime, 'hours'));
      let secondDayMinutes = (this.endTime.diff(secondDayTime, 'minutes'));
      let secondDayTotal = secondDayMinutes;

      this.clearMinutes = firstDayTotal + secondDayTotal + middleDayTotal;
    }
    else{
      this.totalHours = (this.endTime.diff(this.startTime, 'hours'));
      this.totalMinutes = this.endTime.diff(this.startTime, 'minutes');
      this.clearMinutes = this.totalMinutes;
    }
  
  }
  else if(timeIn > 12 && timeOut < 12){
    let firstDayTime =  moment("24:00", 'hh:mm:ss a');
    let firstDayHours = (firstDayTime.diff(this.startTime, 'hours'));
    let firstDayMinutes = (firstDayTime.diff(this.startTime, 'minutes'));
    let firstDayTotal = firstDayMinutes;

    let secondDayTime =  moment("12:00", 'hh:mm:ss a');
    let secondDayHours = (moment.duration(this.shiftmastermodel.endTime, 'hours').hours()) * 60;
    let secondDayMinutes = moment.duration(this.shiftmastermodel.endTime, 'minutes').minutes();
    let secondDayTotal = secondDayMinutes + secondDayHours;

    this.clearMinutes = firstDayTotal + secondDayTotal;
  }
 
  else{
    if(timeIn < 12 && timeOut < 12){
      if(timeIn  > timeOut){
        let firstDayTime =  moment("12:00", 'hh:mm:ss a');
        let firstDayHours = (firstDayTime.diff(this.startTime, 'hours'));
        let firstDayMinutes = (firstDayTime.diff(this.startTime, 'minutes'));
        let firstDayTotal = Math.abs(firstDayMinutes);
  
        let middleDayTime =  moment("24:00", 'hh:mm:ss a');
        let middleDateEndTime = moment("12:00", 'hh:mm:ss a');
        let middleDayHours = (middleDayTime.diff(middleDateEndTime, 'hours'));
        let middleDayMinutes = (middleDayTime.diff(middleDateEndTime, 'minutes'));
        let middleDayTotal = Math.abs(middleDayMinutes);
  
        let secondDayTime =  moment("12:00", 'hh:mm:ss a');
        let secondDayHours = (moment.duration(this.shiftmastermodel.endTime, 'hours').hours()) * 60;
        let secondDayMinutes = moment.duration(this.shiftmastermodel.endTime, 'minutes').minutes();
        let secondDayTotal = secondDayMinutes + secondDayHours;
    
        this.clearMinutes = Math.abs(firstDayTotal) + Math.abs(secondDayTotal) + Math.abs(middleDayTotal);
      }
      else{
        this.totalHours = (this.endTime.diff(this.startTime, 'hours'));
        this.totalMinutes = this.endTime.diff(this.startTime, 'minutes');
        this.clearMinutes = this.totalMinutes;
      }
    }
    else if(timeIn < 12 && timeOut > 12){
      let firstDayTime =  moment("12:00", 'hh:mm:ss a');
      let firstDayHours = (firstDayTime.diff(this.startTime, 'hours'));
      let firstDayMinutes = (firstDayTime.diff(this.startTime, 'minutes'));
      let firstDayTotal = Math.abs(firstDayMinutes);

      let secondDayTime =  moment("12:00", 'hh:mm:ss a');
      let secondDayHours = (this.endTime.diff(secondDayTime, 'hours'));
      let secondDayMinutes = (this.endTime.diff(secondDayTime, 'minutes'));
      let secondDayTotal = secondDayMinutes;

      this.clearMinutes = firstDayTotal + secondDayTotal;
    }
    else{
      this.totalHours = (this.endTime.diff(this.startTime, 'hours'));
      this.totalMinutes = this.endTime.diff(this.startTime, 'minutes');
      this.clearMinutes = this.totalMinutes;
    }
   
}
  this.shiftmastermodel.dailyWorkingHrs =  Math.abs(this.clearMinutes);
}

StatusFlag:boolean=false
onSubmit(f){
  this.StatusFlag=false;
  if (new Date().getMinutes() < 10) {
    if (new Date().getHours() < 10) {
      this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
    }
    else {
      this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
    }

  }
  else {
    if (new Date().getHours() < 10) {
      this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
    }
    else {
      this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
    }
  }
  this.Spinner.show();
  this.shiftmastermodel.status=this.flag1;
  this.shiftmastermodel.compId = this.compId;
  this.shiftmastermodel.locationId = this.shiftmastermodel.locationId ;
  //console.log("locationidonsubmit", this.shiftmastermodel.locationId );
  if(this.isedit == false){
 
 this.shiftmastermodel.shiftGroupId = this.selectedGroupobj;

this.shiftmastermodel.selectedShiftGroup = this.selectedShiftGroup;
this.shiftmastermodel.flexibleShift = this.selectedflexibleshift;
console.log("selectedgroupnameobj",this.shiftmastermodel.selectedShiftGroup);

 this.shiftservice.postShiftMaster(this.shiftmastermodel).subscribe(data => {
  this.Spinner.hide();
  
f.reset();
this.resetShift();
   this.toastr.success('Shift Master Information Inserted Successfully!', 'Shift-Master');
 
   this.shiftMasterdata();
   this.ch.detectChanges();
 },(err: HttpErrorResponse) => {
    this.Spinner.hide();
    this.toastr.error('Server Side Error!', 'Shift-Master');
 });
  }
  else{
    this.StatusFlag=true;
    let url1 = this.baseurl + '/ShiftMaster';
    this.shiftmastermodel.shiftMasterId= this.Selectededitobj;
    this.shiftmastermodel.locationId =    this.shiftmastermodel.locationId ;
  console.log("locationidupdate", this.shiftmastermodel.locationId );
  console.log("updateddata",this.shiftmastermodel);
    this.httpService.put(url1,this.shiftmastermodel).subscribe(data => {
      this.updatedshiftdata = data["result"];
      console.log("updateddata", this.updatedshiftdata );
      this.Spinner.hide();

      f.reset();
      this.resetShift();
      this.toastr.success('Shift-Master Information Updated Successfully!', 'Shift-Master');
      this.shiftMasterdata();
      this.isedit=false;
      this.ch.detectChanges();

    },(err: HttpErrorResponse) => {
      this.isedit=false
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Shift-Master');
    });
  }
  }

  // shiftMasterdata(){
  //   let url = this.baseurl + '/ShiftMaster/';
  //   this.dataTableFlag = false
  //   this.httpService.get(url+this.compId).subscribe(data => {
  //       this.Shiftdata = data["result"];
  //      // console.log("shifydata",this.Shiftdata);
        
  //       //  for (let i = 0; i < this.Shiftdata.length; i++) {
           
  //       //    if(this.Shiftdata[i].shiftName){
  //       //     this.Shiftdata[i].shiftName = this.Shiftdata[i].shiftName.trim();
  //       //    }
           
  //       //  }
  //       for(let i=0; i<this.Shiftdata.length; i++){
  //         if(this.loggedUser.roleHead == 'Location HR' && this.Shiftdata[i].locationId==this.loggedUser.locationId){
          
  //           this.locationwisedatas.push(this.Shiftdata[i])
  //           //console.log("this is the data", this.locationwisedatas)
  //         }
  //         else if(this.loggedUser.roleHead == 'Admin') {
  //           this.locationwisedatas.push(this.Shiftdata[i])
  //         }
          
  //       }

  //        this.dataTableFlag = true;
  //      setTimeout(function () {
  //        $(function () {
  //          var table = $('#shif').DataTable();
  //        });
  //      }, 1000);
       
  //      this.ch.detectChanges();
     
  //     },
  //     (err: HttpErrorResponse) => {
  //     });
  // }
  dataTableFlag : boolean  = true
  shiftMasterdata(){
   
    if(this.loggedUser.roleHead == 'HR'){
    let url = this.baseurl +'/ShiftMaster/ByActive/';
    this.dataTableFlag = false
    this.httpService.get(url+this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
        this.Shiftdata = data["result"];
       
        console.log("shiftdataHr&&&&&&&&&",   this.Shiftdata);
       

         this.dataTableFlag = true;
       setTimeout(function () {
         $(function () {
           var table = $('#shif').DataTable();
         });
       }, 1000);
       
       this.ch.detectChanges();
     
      },
      (err: HttpErrorResponse) => {
      });
  }
  else if (this.loggedUser.roleHead == 'Admin'){
    let url = this.baseurl + '/ShiftMaster/';
    this.dataTableFlag = false
    this.httpService.get(url+this.compId).subscribe(data => {
        this.Shiftdata = data["result"];
        console.log("shiftdataadmin",   this.Shiftdata);
       
        

         this.dataTableFlag = true;
       setTimeout(function () {
         $(function () {
           var table = $('#shif').DataTable();
         });
       }, 1000);
       
       this.ch.detectChanges();
     
      },
      (err: HttpErrorResponse) => {
      });

  }
}
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetShift();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.shiftMasterId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/shiftmaster/';
        this.shiftmastermodel.shiftMasterId = this.dltObje;
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully', 'Shift-Master');
          this.shiftMasterdata()
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
      }
    }

  }

onItemSelect(item: any) {
  if (this.selectedShiftGroup.length == 0) {
    this.selectedShiftGroup.push(item)
  } 
  else {
    this.selectedShiftGroup.push(item)
  }
  
  if(this.selectedShiftGroup.length>0){
    this.flag = true;
    this.flag2 = true;
  }
  else{
    this.flag = false;
    this.flag2 = false;
  }
}
onSelectAll(items: any) {
  this.selectedShiftGroup = items;
}

OnItemDeSelect(item: any) {
  for (var i = 0; i < this.selectedShiftGroup.length; i++) {
    if (this.selectedShiftGroup[i].locationId == item.locationId) {
      this.selectedShiftGroup.splice(i, 1);
    }
  }
  if(this.selectedShiftGroup.length>0){
    this.flag = true;
    this.flag2 = true;
  }
  else{
    this.flag = false;
    this.flag2 = false;
  }
}
selectClick(){
  if(this.selectedShiftGroup.length>0){
    this.flag = true;
    this.flag2 = true;
  }
  else{
    this.flag = false;
    this.flag2 = false;
  }
}
onDeSelectAll(item: any) {
  this.selectedShiftGroup = [];
  }
  resetShift(){
    setTimeout(() => {
      if (new Date().getMinutes() < 10) {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
        }
    
      }
      else {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
        }
      }
      this.shiftmastermodel.inTime= this.todaysTime;
      this.shiftmastermodel.endTime = this.todaysTime;
      this.shiftmastermodel.signInBeforeTime = this.todaysTime;
      this.shiftmastermodel.flexibleShift = 'No'
      this.selectedflexibleshift = "No"
      this.shiftmastermodel.shiftType ="0"
    }, 100);
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
    setTimeout(() => {
      if (new Date().getMinutes() < 10) {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
        }
    
      }
      else {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
        }
      }
      this.shiftmastermodel.inTime= this.todaysTime;
      this.shiftmastermodel.endTime = this.todaysTime;
      this.shiftmastermodel.signInBeforeTime = this.todaysTime;
      this.shiftmastermodel.flexibleShift = 'No'
      this.selectedflexibleshift = "No"
      this.shiftmastermodel.shiftType ="0"
    }, 100);
    
  }
  ngValue1(event:any){
    
    this.selectedLocationobj = event.target.value;
    //log("locationid", this.selectedLocationobj);
    this.getShiftGroupLocationwise(this.selectedLocationobj)
 }
  getLocationList(){
    let url = this.baseurl + '/Location/Active/'+this.compId;
    this.httpService.get(url).subscribe(data => {
        this.locationdata = data["result"];
       
        
        for(let i=0; i<this.locationdata.length; i++){
          if(this.loggedUser.roleHead == 'HR' && this.locationdata[i].locationId==this.loggedUser.locationId){
          
            this.locationwisedata.push(this.locationdata[i])
            //console.log("this is the data", this.locationwisedata)
          }
          else if(this.loggedUser.roleHead == 'Admin') {
            this.locationwisedata.push(this.locationdata[i])
            //console.log("adminlocationwise", this.locationwisedata)
          }
          
        }

      },
      (err: HttpErrorResponse) => {
      });
  }
  getShiftGroupLocationwise(e){
    
    let locationId = e;
    this.shiftGroupMasterdata = [];
    this.shiftmastermodel.selectedShiftGroup = [];
    let url = this.baseurl + '/getLocationWiseShiftGroupList/';
    this.httpService.get(url + locationId)
    .subscribe(data => {
      this.shiftGroupMasterdata = data["result"];
      console.log("data123",  this.shiftGroupMasterdata)
    },(err: HttpErrorResponse)=>{

    });
  }


}
