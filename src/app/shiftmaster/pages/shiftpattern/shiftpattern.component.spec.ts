import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftpatternComponent } from './shiftpattern.component';

describe('ShiftpatternComponent', () => {
  let component: ShiftpatternComponent;
  let fixture: ComponentFixture<ShiftpatternComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftpatternComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftpatternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
