import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { ShifPattern } from '../../../shared/model/shiftPatternModal';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-shiftpattern',
  templateUrl: './shiftpattern.component.html',
  styleUrls: ['./shiftpattern.component.css']
})
export class ShiftpatternComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  shiftPatternModal: ShifPattern;
  SlectedUndefined: any
  loggedUser: any;
  compId: any;
  shiftList: any;
  AllShift: any;
  getID: any;
  getSecondID: Event;
  getthirdID: Event;
  iseditId: false;
  isedit = false;
  selectUndefinedOptionValue: any
  dltmodelFlag: boolean;
  dltObje: any;
  message: any;
  shiftPatternName: any;
  errordata: any;
  msg: any;
  errflag: boolean;

  StatusFlag:boolean=false
  constructor(
    public httpService: HttpClient,
    private toastr: ToastrService,
    public chRef: ChangeDetectorRef,
    public Spinner: NgxSpinnerService
  ) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.getallShif();
    this.shiftPatternModal = new ShifPattern;
    this.shiftPatternModal.sequenceOne = this.selectUndefinedOptionValue
    this.shiftPatternModal.sequenceTwo = this.selectUndefinedOptionValue
    this.shiftPatternModal.sequenceThree = this.selectUndefinedOptionValue
    this.getAllShifPatternList();
  }

  ngOnInit() {
  }
  uiswitch(event){
    this.flag1 = !this.flag1;
  }
  SaveShifPattern(f) {
    this.StatusFlag=false;
    this.shiftPatternModal.status=this.flag1;
    let sequenceThree : any = this.shiftPatternModal.sequenceThree;
    this.shiftPatternModal.sequenceThree = parseInt(sequenceThree);
    let sequenceOne : any = this.shiftPatternModal.sequenceOne;
    this.shiftPatternModal.sequenceOne = parseInt(sequenceOne);
    let sequencetwo : any = this.shiftPatternModal.sequenceTwo;
    this.shiftPatternModal.sequenceTwo = parseInt(sequencetwo);
    if (this.isedit == false) {
      let url = this.baseurl + '/createShiftMasterPattern';
      this.shiftPatternModal.compId = this.compId;
      this.httpService.post(url, this.shiftPatternModal).subscribe(data => {
          this.getAllShifPatternList();
          this.toastr.success('Shift-Pattern Master Information update Successfully!', 'Shift-Pattern');
          f.reset();
          this.shiftPatternModal.sequenceOne = this.selectUndefinedOptionValue;
          this.shiftPatternModal.sequenceTwo = this.selectUndefinedOptionValue;
          this.shiftPatternModal.sequenceThree = this.selectUndefinedOptionValue;
        },
        (err: HttpErrorResponse) => {
          this.message = err.error.result.message;
        if(this.message){
          this.toastr.error(this.message);
        }
        else{
          this.toastr.error('Server Side Error..!', 'Shift-Pattern');
        }
      });
    }
    else {
      this.errflag = false;
      let url1 = this.baseurl + '/updateShiftMasterPattern';
      this.shiftPatternModal.compId = this.compId;
      this.httpService.put(url1, this.shiftPatternModal).subscribe(data => {
          this.Spinner.hide();
          this.chRef.detectChanges();
          f.reset();
          this.toastr.success('Shift-Pattern Master Information update Successfully!', 'Shift-Pattern');
          this.getAllShifPatternList();
          this.isedit = false;
          this.shiftPatternModal.sequenceOne = this.selectUndefinedOptionValue;
          this.shiftPatternModal.sequenceTwo = this.selectUndefinedOptionValue;
          this.shiftPatternModal.sequenceThree = this.selectUndefinedOptionValue;
        }, (err: HttpErrorResponse) => {
          this.isedit = false;
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Shift-Pattern');
        });
    }
  }
  dataTableFlag: boolean = true
  getAllShifPatternList() {
    let url = this.baseurl + '/shiftMasterPatternByCompanyId/'
    this.dataTableFlag = false
    this.httpService.get(url + this.compId).subscribe((data: any) => {
      this.shiftList = data.result;
      this.dataTableFlag = true;
      this.chRef.detectChanges();
      setTimeout(function () {
        $(function () {
          var table = $('#shifttable').DataTable();
        });
      }, 1000);
    },
    (err: HttpErrorResponse) => {
    });
  }
  getallShif() {
    let url = this.baseurl + '/ShiftMaster/';
    this.httpService.get(url + this.compId).subscribe((data: any) => {
      this.AllShift = data.result;
    },
    (err: HttpErrorResponse) => {
    });
  }
  SequenceOneID(event) {
    this.getID = event;
  }
  SequenceTwo(event) {
    this.getSecondID = event;
  }
  SequenceThree(event) {
    this.getthirdID = event;
  }
  edit(i) {
    this.iseditId = i.shiftPatternId;
    this.isedit = true;
    this.iseditfunction(this.iseditId);
    this.StatusFlag=true;
    this.flag1 = i.status;
  }
  flag1 : boolean = false;
  iseditfunction(selectedvalue) {
    let urledit = this.baseurl + '/shiftMasterPatternById/';
    this.httpService.get(urledit + selectedvalue).subscribe(data => {
        this.shiftPatternModal.shiftPatternName = data['result']['shiftPatternName'];
        this.shiftPatternModal.sequenceOne = data['result']['sequenceOne'];
        this.shiftPatternModal.sequenceTwo = data['result']['sequencetwo'];
        this.shiftPatternModal.sequenceThree = data['result']['sequencethree'];
        this.shiftPatternModal.shiftPatternId = data['result']['shiftPatternId'];
        this.shiftPatternModal.status = data['result']['status'];
      },
      (err: HttpErrorResponse) => {
      });
  }
  error(){
    if (this.shiftPatternName != this.shiftPatternModal.shiftPatternName){
      this.shiftPatternName = this.shiftPatternModal.shiftPatternName
        let url = this.baseurl + '/checkShiftPatternNameExitOrNot/';
        this.httpService.get(url + this.shiftPatternName ).subscribe((data :any) => {
           this.toastr.error(data.message)
           this.errflag = true
          },
          (err: HttpErrorResponse) => {
            this.errflag = false
          });
      }
  }

  dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
    this.Spinner.show();
  }
  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.shiftPatternId;
  }
  dltObjfun(objct) {
    if (this.dltmodelFlag == true) {
      if (this.dltmodelFlag == true) {
        let url1 = this.baseurl + '/deleteShiftMasterPattern/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
            this.Spinner.hide();
            this.chRef.detectChanges();
            this.toastr.success('Delete Successfully');
            this.getAllShifPatternList();
          }, (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!');
          });
      }
    }

  }
  reset(){
    this.isedit = false;
    this.StatusFlag= false;
   this.shiftPatternModal.sequenceOne = this.selectUndefinedOptionValue;
   this.shiftPatternModal.sequenceTwo = this.selectUndefinedOptionValue;
   this.shiftPatternModal.sequenceThree = this.selectUndefinedOptionValue;
  }
}
