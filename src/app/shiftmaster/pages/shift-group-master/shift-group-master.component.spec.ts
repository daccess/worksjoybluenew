import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftGroupMasterComponent } from './shift-group-master.component';

describe('ShiftGroupMasterComponent', () => {
  let component: ShiftGroupMasterComponent;
  let fixture: ComponentFixture<ShiftGroupMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftGroupMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftGroupMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
