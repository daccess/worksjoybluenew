import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { ShiftGroupMaster } from '../../../shared/model/shiftgroupmodel';
import { shiftGroupMasterService } from '../../../shared/services/shiftgroupservice';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-shift-group-master',
  templateUrl: './shift-group-master.component.html',
  styleUrls: ['./shift-group-master.component.css'],
  providers: [shiftGroupMasterService]
})
export class ShiftGroupMasterComponent implements OnInit {

  baseurl = MainURL.HostUrl;
  companydata: any;
  shiftgroupmastermodel: ShiftGroupMaster;
  selectedCompanyobj: any;
  shifgroupdata: any;
  compId: any;
  iseditId: any;
  isedit = false;
  StatusFlag:boolean=false
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  loconame: any;
  locationwisedata:any=[];
  public selectUndefinedOptionValue:any;
  locationdata: any;
  locationwisedatas: any=[];
  selectedLocationobj: any;
 
  constructor(public httpService: HttpClient,
     public shiftpostservice: shiftGroupMasterService , 
     private toastr: ToastrService,
     public chRef: ChangeDetectorRef,
     public Spinner :NgxSpinnerService) 
     { 
    this.shiftgroupmastermodel = new ShiftGroupMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
  
   }

   ngOnInit() {

    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    this.shiftgroupdatalist();
    this.getLocationList();
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
    this.Spinner.show();
  
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.shiftGroupId ;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/shifgrouptmaster/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.Spinner.hide();
          this.chRef.detectChanges();
          this.toastr.success('Delete Successfully', );
          this.shiftgroupdatalist();
    
       
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
      }
    }
  
  }

  error(){
    if( this.loconame != this.shiftgroupmastermodel.shiftGroupName){
    let obj={
      compId : this.selectedCompanyobj,
      shiftGroupName:this.shiftgroupmastermodel.shiftGroupName,
      locationids: this.selectedLocationobj 
      }
      let url = this.baseurl + '/shiftgrp';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        }
      );
    }
  }

  ngValue(event){
    
     this.selectedCompanyobj = parseInt(event.target.value);
   
  }
  ngValue1(event){
    this.selectedLocationobj = parseInt(event.target.value);
 }

  onSubmit(f){
    this.Spinner.show();
    this.StatusFlag=false
    this.shiftgroupmastermodel.status=this.flag1;
    if(this.isedit == false){
    this.shiftgroupmastermodel.compId = this.selectedCompanyobj;
    this.shiftgroupmastermodel.locationId = this.selectedLocationobj;
    this.shiftpostservice.postShiftMaster(this.shiftgroupmastermodel).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      // this.responceShiftGroup = data['result']['']
      this.toastr.success('Shift-Group Master Information Inserted Successfully!', 'ShiftGroup-Master');
      this.shiftgroupdatalist();
    },(err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'ShiftGroup-Master');
    });
      
  
  }
  else {
    this.errflag = false
    let url1 = this.baseurl + '/ShiftGroup';
    this.shiftgroupmastermodel.compId = this.selectedCompanyobj;
    this.shiftgroupmastermodel.shiftGroupId = this.iseditId;
    this.shiftgroupmastermodel.locationId = this.selectedLocationobj;
    this.httpService.put(url1,this.shiftgroupmastermodel).subscribe(data => {
      this.Spinner.hide();
      this.chRef.detectChanges();
      f.reset();
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.toastr.success('Shift-Group Master Information update Successfully!', 'ShiftGroup-Master');
      this.shiftgroupdatalist();
      this.isedit=false;
    
    },(err: HttpErrorResponse) => {
      this.isedit=false;
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'ShiftGroup-Master');
    });

  }

  }
  dataTableFlag : boolean  = true

  shiftgroupdatalist(){
    if(this.loggedUser.roleHead == 'HR'){
      let url = this.baseurl + '/ShiftGroup/Active/';
      this.dataTableFlag = false 
      this.httpService.get(url+this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
        this.shifgroupdata  = data["result"];
         
          //console.log("shiftdataadminHr",    this.shifgroupdata );
         
  
           this.dataTableFlag = true;
         setTimeout(function () {
           $(function () {
             var table = $('#shif').DataTable();
           });
         }, 1000);
         
         this. chRef.detectChanges();
       
        },
        (err: HttpErrorResponse) => {
        });
    }
    else if (this.loggedUser.roleHead == 'Admin'){
      let url = this.baseurl + '/ShiftGroup/';
      this.dataTableFlag = false
      this.httpService.get(url+this.compId).subscribe(data => {
        this.shifgroupdata  = data["result"];
          console.log("shiftdataadmin",     this.shifgroupdata );
         
          
  
           this.dataTableFlag = true;
         setTimeout(function () {
           $(function () {
             var table = $('#shif').DataTable();
           });
         }, 1000);
         
         this. chRef.detectChanges();
       
        },
        (err: HttpErrorResponse) => {
        });
  
    }
  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }

  edit(object){
    this.iseditId = object.shiftGroupId;

    this.isedit = true;

    this.iseditfunction(this.iseditId);
    this.StatusFlag=true
    this.flag1 = object.status;
  }
  flag1 : boolean = false;
  iseditfunction(selectedvalue){
    let urledit = this.baseurl + '/ShiftGroup/getById/';
    this.httpService.get(urledit + selectedvalue).subscribe(data => {
        this.shiftgroupmastermodel.status = data['result']['status'];
        this.shiftgroupmastermodel.shiftGroupName = data['result']['shiftGroupName'];
        this.shiftgroupmastermodel.shiftGroupDesc = data['result']['shiftGroupDesc'];
        var comp_id = data['result']['companyMaster']['compId'];
        var location_id = data['result']['locationId']
        //console.log("locationId****",location_id);
        
        this.loconame = this.shiftgroupmastermodel.shiftGroupName;
        for(var i=0;i<this.companydata.length;i++){
           if(this.companydata[i].compId == comp_id){
             this.selectedCompanyobj =  comp_id;
           }
        }
        for(let i=0;i<this.locationdata.length;i++){
          if(this.locationdata[i].locationId == location_id){
            this.selectedLocationobj =  location_id;
          }
       }





      },
      (err: HttpErrorResponse) => {
      });
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
  getLocationList(){    
  
    let url = this.baseurl + '/Location/Active/'+this.compId;
    this.httpService.get(url).subscribe(data => {
        this.locationdata = data["result"];
        for(let i=0; i<this.locationdata.length; i++){
          if(this.loggedUser.roleHead == 'HR' && this.locationdata[i].locationId==this.loggedUser.locationId){
          
            this.locationwisedata.push(this.locationdata[i])
            //console.log("this is the data", this.locationwisedata)
          }
          else if(this.loggedUser.roleHead == 'Admin') {
            this.locationwisedata.push(this.locationdata[i])
            //console.log("this is the data", this.locationwisedata)
       
          }
          
        }

      },
      (err: HttpErrorResponse) => {
      });
  }

} 
