import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loginFlag: any;
  adminUserName: string;
  adminLoginFlag: boolean = false;
  loggedUser: any;
  user: string;
  users: any;
  token: any;
  empids: any;
  rolename: any;
  manpowerrequestFlag: any;
  empImageUrl: any;
  constructor() { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId
    this.rolename=this.users.roleList.roleName;
    this.manpowerrequestFlag=this.users.roleList.isManpowerRequestAuth
    this.empImageUrl=this.users.roleList.empImageUrl
    // this.loggedUser = JSON.parse(user);
    // this.adminUserName = this.loggedUser.username;
   
    // if(this.loggedUser.username == 'Admin' && this.loggedUser.RoleHead == 'Admin'){
    //   this.adminLoginFlag = true;
    // }
    // else{
    //   this.adminLoginFlag = false;
    // }
  }

}
