import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { EmployeemasterComponent } from 'src/app/employeemaster/employeemaster.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: EmployeemasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'employeemaster',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'employeemaster',
                    component: EmployeemasterComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);