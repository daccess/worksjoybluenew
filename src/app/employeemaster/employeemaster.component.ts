import { Component, OnInit } from '@angular/core';
declare var $;
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employeemaster',
  templateUrl: './employeemaster.component.html',
  styleUrls: ['./employeemaster.component.css']
})
export class EmployeemasterComponent implements OnInit {
  baseurl = MainURL.HostUrl
  Employeedata: any;
  compId: any;
  returnUrl: string;
  dataEmp: any;
  EmpID: any;
  loggedUser: any;
  empPerId: any;
  constructor(public httpService: HttpClient, 
    public chRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute) { 
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.compId = this.loggedUser.compId;
      this.empPerId = this.loggedUser.empPerId;
  }
  ngOnInit() {
    this.alldatademployeelist();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/layout/viewempdetails';
  }
  alldatademployeelist(){
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
       this.Employeedata = data["result"];
       this.chRef.detectChanges(); 
       setTimeout(function () {
         $(function () {
           var table = $('#emptable').DataTable();
         });
       }, 1000);
       this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });

  }
  getEmpDetail(object){
    this.dataEmp = object.empPerId;
    this.EmpID = sessionStorage.setItem('EmpData',this.dataEmp)
    this.router.navigate([this.returnUrl]);
  }
}
