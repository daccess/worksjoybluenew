import { ContractorWorkingdurationModule } from './contractor-workingduration.module';

describe('ContractorWorkingdurationModule', () => {
  let contractorWorkingdurationModule: ContractorWorkingdurationModule;

  beforeEach(() => {
    contractorWorkingdurationModule = new ContractorWorkingdurationModule();
  });

  it('should create an instance', () => {
    expect(contractorWorkingdurationModule).toBeTruthy();
  });
});
