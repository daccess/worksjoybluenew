import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorWorkingdurationComponent } from './contractor-workingduration.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorWorkingdurationComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorworkingDurationreports',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorworkingDurationreports',
                  component: ContractorWorkingdurationComponent
              }

          ]

  }


]
export const routing = RouterModule.forChild(appRoutes);
