import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './contractor-workingduration-routing.module';
import { ContractorWorkingdurationComponent } from './contractor-workingduration.component';


@NgModule({
  imports: [
    CommonModule,
    routing,

  ],
  declarations: [
    ContractorWorkingdurationComponent
  ]
})
export class ContractorWorkingdurationModule { }
