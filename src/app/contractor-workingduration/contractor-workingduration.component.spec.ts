import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorWorkingdurationComponent } from './contractor-workingduration.component';

describe('ContractorWorkingdurationComponent', () => {
  let component: ContractorWorkingdurationComponent;
  let fixture: ComponentFixture<ContractorWorkingdurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorWorkingdurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorWorkingdurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
