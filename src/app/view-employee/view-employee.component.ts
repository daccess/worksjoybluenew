import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from './../shared/configurl';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css'],
  providers:[]
})


export class ViewEmployeeComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  personalData: any =[];
  loggedUser:any;
  whoCliked:any;
  themeColor: string = "nav-pills-blue";
  empPerId: any;
  bioId: any;
  empId:any;
  fullName: any;
  lastName: any;
  profilePath:any;
  constructor(public httpService: HttpClient,private _sanitizer: DomSanitizer) { 
    this.whoCliked = sessionStorage.getItem('whoClicked')
    this.clicked();
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
  }

  click(){
    sessionStorage.setItem('q','personal');
    sessionStorage.setItem('IsEmployeeEdit','true');
  }

  ngOnInit() {
    
  }

  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser;
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
    }
   
    this.getInfo();
  }
  getInfo(){
    // let url = this.baseurl + '/ViewEmployee/';
    let url = this.baseurl + '/employeeMasterbyId/'; 
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.personalData = data["result"];
        if(this.Aws_flag!='true'){
          this.profilePath=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.personalData['profilePath']);
        }else{
          this.profilePath=this.personalData["profilePath"];
        }
        this.empId = this.personalData["empId"];
        this.fullName = this.personalData["fullName"];
        this.lastName = this.personalData["lastName"];
      },(err: HttpErrorResponse) => {
      });
    }
  }
