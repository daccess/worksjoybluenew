import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  baseurl = MainURL.HostUrl;
  empPerId: any;
  ContactData: any;

  // currentAddress: any;
  // permanentAddress: any;

  buildingNoPermant: any;
  buildingNoCurrent: any;
  socityOrLanePermant: any;
  socityOrLaneCurrent: any;
  areaPermant: any;
  areaCurrent: any;
  cityOrTalukaPermant: any;
  cityorTahshilCurrent: any;
  statePermant: any;
  stateCurrent: any;
  pinCodePermant: any;
  pincodeCurrent: any;

  officialContactNumber: any;
  alternateContactNumber: any;
  officialEmailId: any;
  epersonalEmailId: any;
  personalEmailId:any
  nameOfPerson: any;
  relation: any;
  address: any;
  contactNumber: any;
  alternatecontactNumber1: any;
  skypeId: any;
  whatsAppNumber: any;
  personalContactNumber:any
  whoCliked:any;
  loggedUser:any
  constructor(public httpService: HttpClient) {
    this.whoCliked = sessionStorage.getItem('whoClicked')
    this.clicked();
   }

  ngOnInit() {
  }
  click(){
    sessionStorage.setItem('q','contact');
    sessionStorage.setItem('IsEmployeeEdit','true')
  }
  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId
    }
   
    this.getInfo()
  }
  getInfo(){
    let url = this.baseurl + '/ViewEmployee/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.ContactData = data["result"];
        this.buildingNoPermant = this.ContactData["buildingNoPermant"];
        this.buildingNoCurrent = this.ContactData["buildingNoCurrent"];
        this.socityOrLanePermant = this.ContactData["socityOrLanePermant"];
        this.socityOrLaneCurrent = this.ContactData["socityOrLaneCurrent"];
        this.areaPermant = this.ContactData["areaPermant"];
        this.areaCurrent = this.ContactData["areaCurrent"];
        this.cityOrTalukaPermant = this.ContactData["cityOrTalukaPermant"];
        this.cityorTahshilCurrent = this.ContactData["cityorTahshilCurrent"];
        this.statePermant = this.ContactData["statePermant"];
        this.stateCurrent = this.ContactData["stateCurrent"];
        this.pinCodePermant = this.ContactData["pinCodePermant"];
        this.pincodeCurrent = this.ContactData["pincodeCurrent"];
        this.officialContactNumber = this.ContactData["officialContactNumber"];
        this.alternateContactNumber = this.ContactData["alternateContactNumber"];
        this.officialEmailId = this.ContactData["officialEmailId"];
        this.epersonalEmailId = this.ContactData["epersonalEmailId"];
        this.personalEmailId = this.ContactData["personalEmailId"]
        this.nameOfPerson = this.ContactData["nameOfPerson"];
        this.relation = this.ContactData["relation"];
        this.address = this.ContactData["address"];
        this.contactNumber = this.ContactData["emrContactNumber"];
        this.alternatecontactNumber1 = this.ContactData["emrAlternatecontactNumber"];
        this.skypeId = this.ContactData["skypeId"];
        this.whatsAppNumber = this.ContactData["whatsAppNumber"];
      },(err: HttpErrorResponse) => {
      });
      
  }
}
