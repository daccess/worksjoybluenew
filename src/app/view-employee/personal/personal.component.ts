import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  personalData: any;
  empPerId: any;

  bioId: any;
  empId: any;
  deptName: any;
  fullName: any;
  gender: any;
  descName: any;
  dateOfBirth: any;
  age: any;
  contactNumber: any;
  placeOfBirth: any;
  fatheName: any;
  motherName: any;
  maritialStatus: any;
  nationality: any;
  empKnownLangugesResDtoList: any;
  bloodGroup: any;
  wieght:  any;
  height: any;
  physicalDisability: any;
  healthIssues: any;
  anySpecialInstructionforMeal: any;
  familyDetail: any;
  regularMealPreference: any;
  loggedUser:any;
  whoCliked:any;
  personalEmailId: any;
  language: any;
  personalContactNumber: any;
  firstName: any;
  lastName: any;
  middleName: any;
  officialContactNumber: any;
  epersonalEmailId:any
  currentHlthIssue: any;
  currentHlthIssueDet:any
  constructor(public httpService: HttpClient) { 
    this.whoCliked = sessionStorage.getItem('whoClicked')
    this.clicked();
  }
  click(){
    sessionStorage.setItem('q','personal');
    sessionStorage.setItem('IsEmployeeEdit','true');
  }
  ngOnInit() {

  }
 
  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser;
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
    }
    this.getInfo();
  }
  getInfo(){
    // let url = this.baseurl + '/ViewEmployee/';
    let url = this.baseurl + '/employeeMasterbyId/';
    
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.bioId = this.personalData["bioId"];
        this.empId = this.personalData["empId"];
        this.deptName = this.personalData["deptName"];
        this.firstName = this.personalData["firstName"];
        this.fullName = this.personalData["fullName"];
        this.lastName = this.personalData["lastName"];
        this.middleName = this.personalData["middleName"];
        this.language = this.personalData["language"]
        this.gender = this.personalData["gender"];
        this.descName = this.personalData["descName"];
        this.dateOfBirth = this.personalData["dateOfBirth"];
        this.age = this.personalData["age"];
        this.contactNumber = this.personalData["contactNumber"];
        this.personalEmailId = this.personalData["personalEmailId"];
        this.placeOfBirth = this.personalData["placeOfBirth"];
        this.fatheName = this.personalData["fatheName"];
        this.motherName = this.personalData["motherName"];
        this.maritialStatus = this.personalData["maritialStatus"];
        this.nationality = this.personalData["nationality"];
        this.empKnownLangugesResDtoList = this.personalData["empKnownLangugesResDtoList"];
        this.bloodGroup = this.personalData["bloodGroup"];
        this.wieght = this.personalData["wieght"];
        this.height = this.personalData["height"];
        this.physicalDisability = this.personalData["physicalDisability"];
        this.healthIssues = this.personalData["healthIssues"];
        this.healthIssues = this.personalData["healthIssues"];
        this.currentHlthIssue = this.personalData["currentHlthIssue"];
        this.currentHlthIssueDet =this.personalData["currentHlthIssueDet"];
        this.regularMealPreference = this.personalData["regularMealPreference"];
        this.anySpecialInstructionforMeal = this.personalData["anySpecialInstructionforMeal"];
        this.familyDetail = this.personalData["empFamilyMembersResDtoList"];
        this.personalContactNumber = this.personalData["personalContactNumber"];
        this.officialContactNumber = this.personalData["officialContactNumber"];
        this.epersonalEmailId =this.personalData["epersonalEmailId"];
      },(err: HttpErrorResponse) => {
      });
  }
}
