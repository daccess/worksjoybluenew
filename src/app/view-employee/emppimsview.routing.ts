import { Routes, RouterModule } from '@angular/router'

import { ViewEmployeeComponent } from './view-employee.component';
import { BasicComponent } from './basic/basic.component';


const appRoutes: Routes = [
    // { 
             
    //     path: '', component: BasicComponent,  pathMatch : 'full',

    //         children:[
            
    //             {
    //                 path:'emppimsview',
    //                 component: BasicComponent,
    //                 pathMatch : 'full',
    //                 // data : {some_data : 'some value'}
    //             }

    //         ]

    // }

    { 
             
        path: '', component: BasicComponent,

            children:[
                // {
                //     path:'',
                //     redirectTo : 'emppims',
                //     pathMatch :'full'
                    
                // },
            
                {
                    path:'emppimsview',
                    component: BasicComponent
                }

            ]

    },
    {
        path : 'empdetailsview',
        component : ViewEmployeeComponent,
        
    }
  
]


export const routing = RouterModule.forChild(appRoutes);