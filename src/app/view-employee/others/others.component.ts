import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';

@Component({
  selector: 'app-others',
  templateUrl: './others.component.html',
  styleUrls: ['./others.component.css']
})
export class OthersComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  empPerId: any;
  othersData: any =[];
  transportationMode: any;
  pickupPoint: any;
  droPoint: any;
  busStopName:any
  transportPurpose: any;
  empWorkLocationPrefference: any;
  workLocationNowPreferencesResDtoList: any;
  workLocationFuturePreferencesResDtoList: any;
  panNo: any;
  adharNo:  any;
  universalAccNo: any;
  drivingLicenseNo: any;
  passportNo:  any;
  socialSecurityNo: any;
  citizenShip: any;
  areYouNri: any;
  empBankDetailsResDtoList:  any;
  empDocMasterList: any = []; 
  whoCliked:any;
  loggedUser:any
  constructor(public httpService: HttpClient) { 
    this.whoCliked = sessionStorage.getItem('whoClicked');
    this.clicked();
  }

  ngOnInit() {
  }
  click(){
    sessionStorage.setItem('q','others')
    sessionStorage.setItem('IsEmployeeEdit','true')
  }

  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId
    }
   
    this.getInfo()
  }
  getInfo(){
    
    let url = this.baseurl + '/ViewEmployee/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.othersData = data["result"];
        this.workLocationNowPreferencesResDtoList = this.othersData["workLocationNowPreferancesResDtos"];
        this.workLocationFuturePreferencesResDtoList = this.othersData["workLocationFiturePreferancesResDtos"];
        this.panNo = this.othersData["panNo"];
        this.adharNo = this.othersData["adharNo"];
        this.universalAccNo = this.othersData["universalAccNo"];
        this.drivingLicenseNo = this.othersData["drivingLicenseNo"];
        this.passportNo = this.othersData["passportNo"];
        this.socialSecurityNo = this.othersData["socialSecurityNo"];
        this.citizenShip = this.othersData["citizenShip"];
        this.areYouNri = this.othersData["areYouNri"];
        this.empBankDetailsResDtoList = this.othersData["empBankDetailsResDtoList"];
        this.empDocMasterList=this.othersData["empDocMasterList"];
        for (let i = 0; i < this.empDocMasterList.length; i++) {
          var lastIndex = this.empDocMasterList[i].docUrl.lastIndexOf('-');
          this.empDocMasterList[i].fileName = this.empDocMasterList[i].docUrl.substr(lastIndex + 1);
        }
      },(err: HttpErrorResponse) => {
    });
  }
  open(item){
    window.open(item.docUrl);
  }
}
