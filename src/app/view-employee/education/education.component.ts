import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  empPerId: any;
  educationData: any;

  empExperienceResDetailsList: any;
  empEducationalDetailsResDtoList: any;
  highestEduction: any;
  currentyStudying:  any;
  subOfSpecialisation: any;
  planForFutureStudy: any;
  planForFutureStudyDetails: any;
  ifAnyResearchPapers: any;
  researchPapersDetails: any;
  interestedInSports: any;
  sportDetails: any;
  whoCliked:any;
  loggedUser:any
  constructor(public httpService: HttpClient) { 
    this.whoCliked = sessionStorage.getItem('whoClicked')
    this.clicked();
  }
click(){
  sessionStorage.setItem('q','education');
  sessionStorage.setItem('IsEmployeeEdit','true');
}
  ngOnInit() {
  }

  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId
    }
    this.getInfo();
  }
  getInfo(){
    let url = this.baseurl + '/ViewEmployee/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.educationData = data["result"];
        this.empExperienceResDetailsList = this.educationData["empExperienceResDetailsList"];
        this.empEducationalDetailsResDtoList = this.educationData["empEducationalDetailsResDtoList"];
         //this.empFutureStudyAndSportResDtos = this.educationData["empFutureStudyAndSportResDtos"];
         this.highestEduction = this.educationData["highestEduction"];
         this.currentyStudying = this.educationData["currentyStudying"];
         this.subOfSpecialisation = this.educationData["subOfSpecialisation"];
         this.planForFutureStudy = this.educationData["planForFutureStudy"];
         this.planForFutureStudyDetails = this.educationData["planForFutureStudyDetails"];
         this.ifAnyResearchPapers = this.educationData["ifAnyResearchPapers"];
         this.researchPapersDetails = this.educationData["researchPapersDetails"];
         this.interestedInSports = this.educationData["interestedInSports"];
         this.sportDetails = this.educationData["sportDetails"];

    },(err: HttpErrorResponse) => {
    });
  }
}
