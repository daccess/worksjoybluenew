import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';
import { InfoService } from "../../shared/services/infoService";
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Http } from '@angular/http';
@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent implements OnInit {

  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  personalData: any;
  empPerId: any;
  profilepic: any;
  bioId: any;
  empId: any;
  deptName: any;
  fullName: any;
  gender: any;
  descName: any;
  dateOfBirth: any;
  age: any;
  contactNumber: any;
  loggedUser: any;
  personalEmailId: any;
  whoCliked:any;
  image64: any;
  firstName: any;
  middleName: any;
  lastName: any;
  compName: any;
  officialEmailId: any;
  officialContactNumber: any;
  received_message:any;
  inductionData: any;
  fileToUpload: File;
  locationName: any;
  empTypeName: any;
  roleName: any;
  firstLevelReportingManagerName: any;
  secondLevelReportingManagerName: any;
  thirdLevelReportingManagerName: any;
  shiftType: any;
  shiftName: any;
  calenderName: any;
  imageUrl: any;
  showImageFlag: boolean = true;
  imagepathresult: any;
  profileimage: any;
  empid: string;
  user: string;
  users: any;
  token: any;
  labourBasicDetails: any;
  

  constructor(public httpService: HttpClient, public router: Router,  public toastr: ToastrService,private InfoService:  InfoService,private _sanitizer: DomSanitizer,  private http: Http,) { 
    // this.whoCliked = sessionStorage.getItem('whoClicked')
    // this.clicked();
    // let user = sessionStorage.getItem('loggedUser');
    // this.loggedUser = JSON.parse(user);
    // this.compName = this.loggedUser.compName;
    
  //   if(this.Aws_flag=='true'){
  //     this.profilepic = this.loggedUser.profilePath;
  //   }
  //   else{
  //     this.profilepic=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.loggedUser.profilePath);
  //   }
  //   this.showImageFlag = true;
  this.whoCliked = sessionStorage.getItem('whoClicked')
  
  let themeColor = sessionStorage.getItem('themeColor');
 
  this.empid=sessionStorage.getItem('empids')
  }
  click(){
    sessionStorage.setItem('q','personal')
    sessionStorage.setItem('IsEmployeeEdit','true')
  }
  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token
    // this.InfoService.currentMessage.subscribe(message => {
    //   if(message!='default message'){
    //     this.received_message = message;
    //     }
    //   });
    
    // this.InfoService.changeMessage(this.received_message);
    // if(this.received_message=='preonboarding'){
    //   this.getStatusOfInduction();
    // }
    this.getInfo()
  }
 
  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser;
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
    }
    this.getInfo();
  }

 
  //   getInfo(){
    
  //    let url = this.baseurl + '/employeeMasterbyId/';
  //     this.httpService.get(url + this.empPerId).subscribe((data : any ) => {
  //         this.personalData = data["result"];
  //         this.bioId = this.personalData["bioId"];
  //         this.empId = this.personalData["empId"];
  //         this.deptName = this.personalData["deptName"];
  //         this.fullName = this.personalData.fullName;
  //         this.firstName = this.personalData.firstName;
  //         this.middleName = this.personalData.middleName;
  //         this.lastName = this.personalData.lastName;
  //         this.gender = this.personalData["gender"];
  //         this.descName = this.personalData["descName"];
  //         this.dateOfBirth = this.personalData["dateOfBirth"];
  //         this.age = this.personalData["age"];
        
  //        this.contactNumber = this.personalData["personalContactNumber"]
  //         this.personalEmailId = this.personalData["personalEmailId"];
  //         this.officialEmailId = this.personalData["officialEmailId"];
  //         this.officialContactNumber = this.personalData["officialContactNumber"];

  //         this.locationName = this.personalData["locationName"]
  //         this.empTypeName = this.personalData["empTypeName"];
  //         this.roleName = this.personalData["roleName"];
  //         this.firstLevelReportingManagerName = this.personalData["firstLevelReportingManagerName"];
  //         this.secondLevelReportingManagerName = this.personalData["secondLevelReportingManagerName"]
  //         this.thirdLevelReportingManagerName = this.personalData["thirdLevelReportingManagerName"];
  //         this.shiftType = this.personalData["shiftType"]
  //         this.shiftName = this.personalData["shiftName"];
  //         this.calenderName = this.personalData["calenderName"];
       
  //         if(this.Aws_flag=='true'){
  //           this.image64=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.personalData['profilePath']);
  //         }else{
  //           this.image64 = this.personalData['profilePath'];
  //         }
          
  
  //       },(err: HttpErrorResponse) => {
  //       });
  // }

  EmpBasic(){
    this.router.navigateByUrl('layout/workforcemenu/workforcemenu')
  }
  uploadFile(fileLoader) {
    fileLoader.click();
  }
  getStatusOfInduction() {
    let url = this.baseurl + '/getStatusOfInduction/';
    this.httpService.get(url + this.empPerId).subscribe((data : any ) => {
      this.inductionData = data["result"];
    },(err: HttpErrorResponse) => {
    });
  }
 
  onBack(){
    this.router.navigateByUrl('layout/employeelist/employeelist');
  }
  
  handleFileInput(file: FileList) {

 
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
      
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 2000 && height < 2000) {
          flag1 = true;
        }
        else {
          flag1 = false;
        }
      };

      setTimeout(() => {
        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.imageUrl = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.postprofileimage(this.fileToUpload)
        }
        else {
          this.toastr.error("Please upload image upto 2000*2000 pixels")
        }
      }, 300);
    }
  }
  fileToupload() {

  }
  postprofileimage(files) {

    this.ppostFile(this.fileToUpload).subscribe((data: any) => {
    
      if(this.Aws_flag=='true'){
        this.profilepic = JSON.parse(data._body).result.profilePath;
      }else{
        this.profilepic = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + JSON.parse(data._body).result.imageBase64);
        this.UpdateLoginImage(JSON.parse(data._body).result.filPath);
      }
      this.loggedUser.profilePath = this.profilepic;
   
      this.toastr.success('Profile Picture Updated Successfully')

      sessionStorage.setItem("propic", "P")
    })
  }
 
  UpdateLoginImage(filPath){
    let url = this.baseurl + '/updateUserProfilePic';
    let body={
      "userProfilePath":filPath,
      "empPerId":this.empPerId
    }
    this.httpService.post(url,body).subscribe(data => {
    },
    (err: HttpErrorResponse) => {
    });
  }
  ppostFile(fileToUpload: File) {
    let url = this.baseurl + '/UploadEmpProfile';
  
    if(this.Aws_flag!='true'){
      url=this.baseurl + '/fileHandaling';
    }
  
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    formData.append('empPerId', this.empPerId);
    return this.http.post(endpoint, formData).map(x => x);
  
    
  }

  getInfo(){
    debugger
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    // let url = this.baseurl + '/ViewEmployee/';
    let url = this.baseurl + `/getLabourPersonalInfo?empId=${this.empid}`; 
    this.httpService.get(url,{headers}).subscribe(data => {
        this.personalData = data;
        this.labourBasicDetails=this.personalData.result
        this.profilepic=this.labourBasicDetails.labourInfo.labourImage
        this.fullName=this.labourBasicDetails.labourInfo.firstName
        this.lastName=this.labourBasicDetails.labourInfo.lastName
        this.descName=this.labourBasicDetails.labourInfo.designationMaster.desigName
        this.compName=this.labourBasicDetails.labourInfo.contractorMaster.companyMaster.compName
       this.bioId=this.labourBasicDetails.bioId;
       this.deptName=this.labourBasicDetails.departmentMaster.departmentName
       this.dateOfBirth=this.labourBasicDetails.labourInfo.dateOfBirth
       this.officialContactNumber=this.labourBasicDetails.labourInfo.contactNumber
       this.bioId=this.labourBasicDetails.empId
      //  this.descName=this.labourBasicDetails.designationMaster.desigName
       this.gender=this.labourBasicDetails.labourInfo.gender
       this.officialEmailId=this.labourBasicDetails.labourInfo.emailId
       this.locationName=this.labourBasicDetails.locationMaster.locationName
       
      },(err: HttpErrorResponse) => {
      });
    }
}
