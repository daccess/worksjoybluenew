import { routing } from './emppimsview.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { PimshrComponent } from 'src/app/pimshr/pimshr.component';

 import { ViewEmployeeComponent } from '../view-employee/view-employee.component';
import { ContactComponent } from './contact/contact.component';
import { EducationComponent } from './education/education.component';
import { EmployeeComponent } from './employee/employee.component';
import { OthersComponent } from './others/others.component';
import { PersonalComponent } from '../view-employee/personal/personal.component';
import { BasicComponent } from './basic/basic.component';
// import { ViewEmpService } from './service/viewEmpService';

@NgModule({
    declarations: [
        ViewEmployeeComponent,
        ContactComponent,
        EducationComponent,
        EmployeeComponent,
        OthersComponent,
        PersonalComponent,
        BasicComponent,
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule.forRoot()
    ],
    providers: []
  })
  export class EmppimsViewModule { 
      constructor(){

      }
  }
  