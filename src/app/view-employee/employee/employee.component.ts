import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from 'src/app/shared/configurl';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  baseurl = MainURL.HostUrl;
  Alldata = {applicableDate : '',dateOfOffer : '',position : '',offeredBy : '',confirmationDueDate1 : '',packageAccordingToOffer : '',status : ''};
  empPerId: any;
  costCenterNum: any;
  business: any;
  functionUnitName: any;
  deptName: any;
  subDeptName: any;
  locationName: any;
  descName: any;
  gradeName: any;
  employmentType: any;
  subEmploymentType: any;
  empTypeNamecat: any;
  subEmpTypeName: any;
  empRole: any;
  skillCatagory: any;
  startDateOfTraining: any;
  endDateOfTraining: any;
  probationPeriod: any;
  expectedendDAteOfConfirmation: any;
  startDateOfprobation: any;
  dateOfConfirmation: any;
  firstLevelReportingManager: any;
  secondLevelReportingManager: any;
  thirdLevelReportingManager: any;
  sanctioningAuthority: any;
  shiftType: any;
  weeklyOffType: any;
  shiftGroupName: any;
  calenderName: any;
  variablePay: any;
  singlePunchPresent: any;
  negativeAttendenceAllowed: any;
  ptApplcable: any;
  pfApplicable: any;
  pfNumber: any;
  ptLocation: any;
  payMode: any;
  esicNumber: any;
  esicApplicable: any;
  dateOfJoining: any;
  dateOfConfirmationemp: any;
  isJoined: any;
  documentsSubmitted: any;
  documentsVerified: any;
  appointmentLetterIsProcessed: any;

  dateOfOffer: any;
  position: any;
  offeredBy: any;
  confirmationDueDate1: any;
  packageAccordingToOffer: any;
  status: any;

  verificationRefference: any;
  resources: any;

  applicableDate: any;
  extensionDateOfTraining: any;
  shiftName:  any;
  payGroupName: any;
  empPayRollHeadCtcDetailList: any;
  whoCliked:any;
  loggedUser:any
  extentionOFEmpData: any;
 
  constructor(public httpService: HttpClient) { 
    this.whoCliked = sessionStorage.getItem('whoClicked')
    this.clicked();
  }

  ngOnInit(){
  }

  allDataGet(){
    let url = this.baseurl + '/employeeMasterbyId/';
    this.httpService.get(url + this.empPerId).subscribe(data =>{
       this.Alldata = data["result"];
       this.costCenterNum = data["result"]["costCenterName"];
       this.business = this.Alldata["busiGrupName"];
       this.functionUnitName = this.Alldata["functionUnitName"];
       this.deptName = this.Alldata["deptName"];
       this.subDeptName = this.Alldata["subDeptName"];
       this.locationName = this.Alldata["locationName"];
       this.descName = this.Alldata["descName"];
       this.gradeName = this.Alldata["gradeName"];
       this.employmentType = this.Alldata["employmentType"];
       this.subEmploymentType = this.Alldata["subEmploymentType"];
       this.empTypeNamecat = this.Alldata["empTypeName"];
       this.subEmpTypeName = this.Alldata["subEmpTypeName"];
       this.empRole = this.Alldata["roleName"];
       this.skillCatagory = this.Alldata["skillCatagory"];
       this.startDateOfTraining = this.Alldata["startDateOfTraining"];
       this.endDateOfTraining = this.Alldata["endDateOfTraining"];
       this.extensionDateOfTraining = this.Alldata["extensionDateOfTraining"];
       this.startDateOfprobation = this.Alldata["startDateOfprobation"]
       this.probationPeriod = this.Alldata["probationPeriod"];
       this.expectedendDAteOfConfirmation = this.Alldata["expectedendDAteOfConfirmation"];
       this.dateOfConfirmation = this.Alldata["dateOfConfirmation"];
       this.firstLevelReportingManager = this.Alldata["firstLevelReportingManagerName"];
       this.secondLevelReportingManager = this.Alldata["secondLevelReportingManagerName"];
       this.thirdLevelReportingManager = this.Alldata["thirdLevelReportingManagerName"];
       this.sanctioningAuthority = this.Alldata["sanctioningAuthority"];

       this.shiftType = this.Alldata["shiftType"];
       this.shiftName = this.Alldata["shiftName"];
       this.weeklyOffType = this.Alldata["weeklyOffType"];
       this.shiftGroupName = this.Alldata["shiftGroupName"];
       this.calenderName = this.Alldata["calenderName"];
       this.payGroupName = this.Alldata["payGroupName"];
       this.variablePay = this.Alldata["variablePay"];
       this.singlePunchPresent = this.Alldata["singlePunchPresent"];
       this.negativeAttendenceAllowed = this.Alldata["negativeAttendenceAllowed"];
       //this.ptLocation = this.Alldata["ptLocation"];
       this.ptApplcable = this.Alldata["ptApplcable"];
       this.pfApplicable = this.Alldata["pfApplicable"];
       this.pfNumber = this.Alldata["pfNumber"];
       this.ptLocation = this.Alldata["ptLocation"];
       this.payMode = this.Alldata["payMode"];
      this.esicNumber = this.Alldata["esicNumber"];
      this.esicApplicable = this.Alldata["esicApplicable"];

      this.dateOfJoining = this.Alldata["dateOfJoining"];
      this.dateOfConfirmationemp = this.Alldata['dateOfConfDate'];
      this.isJoined = this.Alldata["isJoined"];
      this.documentsSubmitted = this.Alldata["documentsSubmitted"];
      this.documentsVerified = this.Alldata["documentsVerified"];
      this.appointmentLetterIsProcessed = this.Alldata["appointmentLetterIsProcessed"];
      this.dateOfOffer = this.Alldata["dateOfOffer"];
      this.position = this.Alldata["position"];
      this.offeredBy = this.Alldata["offeredBy"];
      this.confirmationDueDate1 = this.Alldata["confirmationDueDate1"];
      this.packageAccordingToOffer = this.Alldata["packageAccordingToOffer"];
      this.status = this.Alldata["status"];
      this.verificationRefference = this.Alldata["empVerificationReferencesList"];
      this.applicableDate = this.Alldata["applicableDate"];
      this.empPayRollHeadCtcDetailList = this.Alldata["empPayRollHeadCtcDetailList"];
      this.resources = this.Alldata["empGivenResourcesList"];
      if(this.subEmploymentType == 'Temporary'){
        this.extentionOFEmpData = this.Alldata['empTemporyDetailsList'];
      }
      else if(this.subEmploymentType == 'Trainee'){
        this.extentionOFEmpData = this.Alldata['empTraineeDetailseList'];
      }
      else{
        this.extentionOFEmpData = this.Alldata['empProbationDetailsList'];
      }
    },
    (err: HttpErrorResponse) => {
    });
  }

  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser;
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
    }
    this.allDataGet();
  }
}
