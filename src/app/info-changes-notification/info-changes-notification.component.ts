import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { EmpInfoService } from './services/infoChnages'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-info-changes-notification',
  templateUrl: './info-changes-notification.component.html',
  styleUrls: ['./info-changes-notification.component.css'],
  providers:[EmpInfoService]
})
export class InfoChangesNotificationComponent implements OnInit {
  baseurl = MainURL.HostUrl
  loggedUser: any;
  compId: any;
  getEmployeeDataChanges: any;
  selectedData=[];
  employeeFieldName: any;
  employeeId: any;
  status: string;
  firstName: any;
  lastName: any;
  deptName: any;
  empPerId: any;

  constructor(public httpService: HttpClient,public EmpInfoService: EmpInfoService, public toastr: ToastrService, public Spinner: NgxSpinnerService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
   }

  ngOnInit() {
    this.getDataInfoChanges();
  }

  getDataInfoChanges(){
    this.Spinner.show();
    let url = this.baseurl + '/employeeInfoChange/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.getEmployeeDataChanges = data['result'];
      
        setTimeout(function () {
          $(function () {
            var table = $('#emptable').DataTable();
          });
        }, 1000);
        this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
      });
  }

  getData(data){
    
    this.selectedData = data;
    console.log("******************",this.selectedData);
    this.firstName = data[0].fullName;
    this.lastName = data[0].lastName;
    this.deptName = data[0].deptName;
    this.empPerId = data[0].empPerId;
    this.employeeId = data[0].empId;
  }

  AcceptChanges(AcceptChanges){
    console.log("***************======",AcceptChanges);
    this.employeeFieldName = AcceptChanges[0].oldObj;
    this.employeeId = AcceptChanges[0].newObj;
    this.status ='Active';
    this.EmpInfoService.ApproveChangeData(this.employeeFieldName,this.employeeId,this.empPerId,this.status).subscribe(data => {
      // location.reload();
      this.toastr.success('Information Approved Successfully..!');
    }, err => {
      this.toastr.error('Server side error..!');
    });

  }

  RejectChanges(RejectChanges){
    this.employeeFieldName = RejectChanges[0].oldObj;
    this.employeeId = RejectChanges[0].newObj;
    this.status ='Rejected';
    this.EmpInfoService.ApproveChangeData(this.employeeFieldName,this.employeeId,this.empPerId,this.status).subscribe(data => {
      // location.reload();
      this.toastr.success('Information Rejected Successfully..!');
    }, err => {
      this.toastr.error('Server side error..!');
    });
  }
}
