import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class EmpInfoService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {
        
    }
    

    ApproveChangeData(employeeFieldName: any, employeeId: any,empPerId: any, status: any){

        let url = this.baseurl+'/empPerInfoChangs?employeeFieldName='+employeeFieldName+'&employeeId='+employeeId+'&empPerId='+empPerId+'&status='+status;
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.put(url, requestOptions).map(x => x.json());
       
      }
}