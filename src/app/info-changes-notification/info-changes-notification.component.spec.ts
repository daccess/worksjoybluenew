import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoChangesNotificationComponent } from './info-changes-notification.component';

describe('InfoChangesNotificationComponent', () => {
  let component: InfoChangesNotificationComponent;
  let fixture: ComponentFixture<InfoChangesNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoChangesNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoChangesNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
