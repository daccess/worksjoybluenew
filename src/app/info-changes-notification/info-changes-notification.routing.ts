import { Routes, RouterModule } from '@angular/router'
import { InfoChangesNotificationComponent } from './info-changes-notification.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: InfoChangesNotificationComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'info-changes-notification',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'info-changes-notification',
                    component: InfoChangesNotificationComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);