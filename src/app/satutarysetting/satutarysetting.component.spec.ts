import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatutarysettingComponent } from './satutarysetting.component';

describe('SatutarysettingComponent', () => {
  let component: SatutarysettingComponent;
  let fixture: ComponentFixture<SatutarysettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatutarysettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatutarysettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
