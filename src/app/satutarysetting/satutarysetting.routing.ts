import { Routes, RouterModule } from '@angular/router'
import { SatutarysettingComponent } from './satutarysetting.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: SatutarysettingComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'satutarysetting',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'satutarysetting',
                    component: SatutarysettingComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);