import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorMultiplePunchReportComponent } from './contractor-multiple-punch-report.component';

describe('ContractorMultiplePunchReportComponent', () => {
  let component: ContractorMultiplePunchReportComponent;
  let fixture: ComponentFixture<ContractorMultiplePunchReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorMultiplePunchReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorMultiplePunchReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
