import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './contractor-multiple-punch-report-routing.module';
import { ContractorMultiplePunchReportComponent } from './contractor-multiple-punch-report.component';


@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [
    ContractorMultiplePunchReportComponent
  ]
})
export class ContractorMultiplePunchReportModule { }
