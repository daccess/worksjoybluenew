import { ContractorMultiplePunchReportModule } from './contractor-multiple-punch-report.module';

describe('ContractorMultiplePunchReportModule', () => {
  let contractorMultiplePunchReportModule: ContractorMultiplePunchReportModule;

  beforeEach(() => {
    contractorMultiplePunchReportModule = new ContractorMultiplePunchReportModule();
  });

  it('should create an instance', () => {
    expect(contractorMultiplePunchReportModule).toBeTruthy();
  });
});
