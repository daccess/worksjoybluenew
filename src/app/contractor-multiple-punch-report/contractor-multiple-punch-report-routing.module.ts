import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorMultiplePunchReportComponent } from './contractor-multiple-punch-report.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorMultiplePunchReportComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorMultiplePunchReport',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorMultiplePunchReport',
                  component: ContractorMultiplePunchReportComponent
              }

          ]

  }


]
export const routing = RouterModule.forChild(appRoutes);
