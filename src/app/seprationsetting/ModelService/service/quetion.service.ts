import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { QuetionM } from '../Model/quetionModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable({
  providedIn: 'root'
})
export class QuetionService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postquetionInfo(que : QuetionM){
    let url = this.baseurl + '/seperationSetting/createExitInterviewQuestion';
    var body = JSON.stringify(que);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getQuestionList(compId){
    let url = this.baseurl + '/seperationSetting/getExitInterviewQuestionList/'+compId;    
    return this.http.get(url).map(x => x.json());
  }

  saveFeedBack(queAns){
    let url = this.baseurl + '/ExitInterviewQuestion/QuestionAndAns';
    var body = JSON.stringify(queAns);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  deactivateEmployee(model){
    let url = this.baseurl + '/updateEmpStatusExitInterview';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,model).map(x => x.json());
  }
}