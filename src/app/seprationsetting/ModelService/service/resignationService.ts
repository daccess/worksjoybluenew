import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { ResignationM } from '../Model/resignationModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class resignationDataService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postResignationInfo(res : ResignationM){
    let url = this.baseurl + '/seperationSetting/createResignationReason';
    var body = JSON.stringify(res);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}