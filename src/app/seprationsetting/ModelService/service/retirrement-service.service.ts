import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { RetirementM } from '../Model/retirementpolicy';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable({
  providedIn: 'root'
})
export class RetirrementServiceService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  /*To create the retirement policy */
  postRetirementInfo(ret : RetirementM){
    let url = this.baseurl + '/seperationSetting/createRetirementPolicy';
    var body = JSON.stringify(ret);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  /*To get the retirement policy information*/
  getRetirementInfo(compId){
    let url = this.baseurl + '/seperationSetting/getRetirementPolicyList/'+compId;
    return this.http.get(url).map(x => x.json());
  }

  /*To update the retirement policy information*/
  updateRetirementInfo(retUpadateModel){
    let url = this.baseurl + '/seperationSetting/updateRetirementPolicy';
    var body = JSON.stringify(retUpadateModel);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,retUpadateModel).map(x => x.json());
  }
}
