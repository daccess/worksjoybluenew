import { Routes, RouterModule } from '@angular/router'

import { SeprationsettingComponent } from './seprationsetting.component';


const appRoutes: Routes = [
    { 
             
        path:'', component: SeprationsettingComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'seprationsetting',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'seprationsetting',
                    component: SeprationsettingComponent
                }

            ]

    }
  
   
 
]

export const routing = RouterModule.forChild(appRoutes);