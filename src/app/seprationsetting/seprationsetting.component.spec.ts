import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeprationsettingComponent } from './seprationsetting.component';

describe('SeprationsettingComponent', () => {
  let component: SeprationsettingComponent;
  let fixture: ComponentFixture<SeprationsettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeprationsettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeprationsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
