import { Component, OnInit } from '@angular/core';
import { ResignationM } from './ModelService/Model/resignationModel';
import { resignationDataService } from './ModelService/service/resignationService';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

import { RetirrementServiceService } from './ModelService/service/retirrement-service.service';
import { RetirementM } from './ModelService/Model/retirementpolicy';

import { QuetionM } from './ModelService/Model/quetionModel';
import { QuetionService } from './ModelService/service/quetion.service'
import { from } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-seprationsetting',
  templateUrl: './seprationsetting.component.html',
  styleUrls: ['./seprationsetting.component.css'],
  providers: [resignationDataService, RetirrementServiceService, QuetionService]
})
export class SeprationsettingComponent implements OnInit {

  resignationModel: ResignationM;
  retireMentModel: RetirementM;
  quetioModel: QuetionM;
  loggedUser: any;
  compId: any;
  baseurl = MainURL.HostUrl;
  Resignationdata: any;
  Resignationeditdata: any;
  editFlag: boolean = false;
  Quetiondata: any;
  editquetionFlag: boolean = false;
  QuetionEditdata: any;
  resigReasonId: any;
  exitInterviewQuestionId: any;
  retirementId: any;

  constructor(public resigService: resignationDataService,
    public toastr: ToastrService,
    public httpService: HttpClient,
    public retireService: RetirrementServiceService,
    public quetionservice: QuetionService,
    public Spinner :NgxSpinnerService) {

    this.resignationModel = new ResignationM();
    this.retireMentModel = new RetirementM();
    this.quetioModel = new QuetionM();

    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.quetioModel.ansType = "";
  }

  ngOnInit() {
    this.getResignationlist();
    this.getquetionList();
    this.getRetirementInfo();
  }

  submitResignation(resignationModel) {
    if (this.editFlag == false) {
      this.resignationModel.compId = this.compId;
      this.Spinner.show();
      this.resigService.postResignationInfo(this.resignationModel).subscribe(data => {
          this.resignationModel = new ResignationM();
          this.Spinner.hide()
          this.toastr.success('Resignation Information Inserted Successfully!', 'Resignation');
          this.getResignationlist();
          // this.router.navigateByUrl('layout/pimshr/details')
          // this.router.navigateByUrl('/layout/employeelist');

        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Resignation');
          });
    }
    else {
this.Spinner.show()
      this.resignationModel.compId = this.compId;
      let url = this.baseurl + '/seperationSetting/updateResignationReason';
      this.httpService.put(url, this.resignationModel).subscribe(
        data => {
          this.resignationModel = new ResignationM();
          this.Spinner.hide()
          this.toastr.success('Resignation Information Updated Successfully!', 'Resignation');
          this.getResignationlist();
          this.editFlag = false;
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide()
          this.toastr.error('Server Side Error..!', 'Resignation');
        });

    }

  }


  getResignationlist() {
    let url = this.baseurl + '/seperationSetting/getResignationReasonList/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.Resignationdata = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }


  editresignation(object) {
    this.resignationModel.resigReasonId = object.resigReasonId;
    this.editData(object.resigReasonId);
    this.editFlag = true;
  }

  editData(id) {

    let url = this.baseurl + '/seperationSetting/getResignationReason/';
    this.httpService.get(url + id).subscribe(data => {
        this.Resignationeditdata = data["result"];
        this.resignationModel = this.Resignationeditdata;
      }, (err: HttpErrorResponse) => {
      });
  }


  DltSeoFlag : boolean = false
  getSeprData(itemresignation){
    this.DltSeoFlag = false;
    this.resigReasonId = itemresignation.resigReasonId;
    this.dltSeparation(this.resigReasonId);
  }


dltSeprEvent(){
  this.Spinner.show()
  this.DltSeoFlag = true
 this.dltSeparation(this.resigReasonId);
}

  dltSeparation(object) {
    if(this.DltSeoFlag == true){
    let url = this.baseurl + '/seperationSetting/deleteResigantionReason/';
    this.httpService.delete(url + this.resigReasonId).subscribe(data => {
        this.Spinner.hide();
        this.toastr.success('Delete Successfully');
        this.getResignationlist();
      }, (err: HttpErrorResponse) => {
        this.Spinner.hide();
        this.toastr.error(err.message);
      });

  }
}

  /*To get the retirement policy information */
  getRetirementInfo(){
    this.retireService.getRetirementInfo(this.compId).subscribe(data => {
      if(data.result){
        this.retirementId = data.result.retirementPolicyId;
      }
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Something went wrong..!', 'Retirement');
    });
  }

  /*To save and update retirement policy */
  submitRetirement(retireMentModel) {
    var updateRetirementModel ={
      compId:'',
      retirementPolicyId: '',
      retirementAge:''
    }

    this.retireMentModel.compId = this.compId;
    this.Spinner.show()
    if(this.retirementId){
      updateRetirementModel.compId = this.compId;
      updateRetirementModel.retirementAge = retireMentModel.retirementAge;
      updateRetirementModel.retirementPolicyId = this.retirementId;

      this.retireService.updateRetirementInfo(updateRetirementModel).subscribe(data => {
        this.Spinner.hide()
        this.toastr.success('Retirement Information Updated Successfully!', 'Retirement');
      },
      (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Something went wrong..!', 'Retirement');
      });
     }

     else{
      this.retireService.postRetirementInfo(this.retireMentModel).subscribe(data => {
        this.Spinner.hide();
        this.toastr.success('Retirement Information Inserted Successfully!', 'Retirement');
      },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Something went wrong..!', 'Retirement');
        });
     }
  }

  submitQuetion(quetioModel) {

    if (!this.editquetionFlag) {
      this.quetioModel.compId = this.compId;
      this.Spinner.show();
      this.quetionservice.postquetionInfo(this.quetioModel).subscribe(data => {
          this.quetioModel = new QuetionM();
          this.quetioModel.compId = this.compId;
          this.Spinner.hide();
          this.toastr.success('Exit Interview Question Inserted Successfully!', 'Question');
          this.getquetionList();
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Quetion');
          });
    }
    else {
      this.Spinner.show();
      this.quetioModel.compId = this.compId;
      let url = this.baseurl + '/seperationSetting/updateExitInterviewQuestion';
      this.httpService.put(url, this.quetioModel).subscribe(data => {
          this.quetioModel = new QuetionM();
          this.quetioModel.compId = this.compId;
          this.Spinner.hide()
          this.toastr.success('Exit Interview Question Updated Successfully!', 'Quetion');
          this.getquetionList();
          this.editquetionFlag = false;
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Quetion');
        });

    }

  }

  getquetionList() {

    this.quetionservice.getQuestionList(this.compId).subscribe(data => {
      this.Quetiondata = data["result"];
    }, (err: HttpErrorResponse) => {
    });

  }

  editQuetion(object) {
    this.quetioModel.exitInterviewQuestionId = object.exitInterviewQuestionId;
    this.editQuetionData(object.exitInterviewQuestionId);
    this.editquetionFlag = true;
  }

  DltQueFlag : boolean = false;
  getQueData(itemresignation){
    this.DltQueFlag = false;
    this.exitInterviewQuestionId = itemresignation.exitInterviewQuestionId;
    this.dltQuetion(this.exitInterviewQuestionId);
  }


dltQueEvent(){
  this.Spinner.show();
  this.DltQueFlag = true;
 this.dltQuetion(this.exitInterviewQuestionId);
}

  dltQuetion(object) {
    if( this.DltQueFlag == true){
    let url = this.baseurl + '/seperationSetting/deleteExitInterviewQuestion/';
    this.httpService.delete(url + this.exitInterviewQuestionId).subscribe(data => {
        this.Spinner.hide();
        this.toastr.success('Delete Successfully');
        this.getquetionList();
      }, (err: HttpErrorResponse) => {
        this.Spinner.hide();
        this.toastr.error('Server Side Error..!');
      });
  }
}

  editQuetionData(editId) {
    let url = this.baseurl + '/seperationSetting/getExitInterviewQuestion/';
    this.httpService.get(url + editId).subscribe(data => {
        this.QuetionEditdata = data["result"];
        this.quetioModel = this.QuetionEditdata;

      }, (err: HttpErrorResponse) => {
      });
  }
}