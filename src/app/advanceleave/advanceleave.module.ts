import { routing } from './advanceleave.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import{ CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { AdvanceleaveComponent } from './advanceleave.component';
import { FilterPipeforAdvance } from './pipe/filter.pipe';
import { SearchPipeForEmpleave } from './pipe/serchpipe';


@NgModule({
    declarations: [
        AdvanceleaveComponent,
        FilterPipeforAdvance,
        SearchPipeForEmpleave,
       
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: []
  })
  export class AdvanceleaveModule { 
      constructor(){

      }
  }
  