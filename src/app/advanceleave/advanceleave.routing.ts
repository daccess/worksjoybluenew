import { Routes, RouterModule } from '@angular/router'
import { AdvanceleaveComponent } from './advanceleave.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: AdvanceleaveComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'advanceleave',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'advanceleave',
                    component: AdvanceleaveComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);