import { Component, OnInit } from '@angular/core';
import {AdvanceLeave} from './Modal/advanceLeave'
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-advanceleave',
  templateUrl: './advanceleave.component.html',
  styleUrls: ['./advanceleave.component.css']
})
export class AdvanceleaveComponent implements OnInit {

  

  baseurl = MainURL.HostUrl
  themeColor: string;
advanceLeaveModal: AdvanceLeave
  loggedUser: any;
  compId: any;
  empOfficialId: any;
  empPerId: any;
  Employeedata: any;
  searchQuery: string;
  empFlag: boolean;
  designation: any;
  LeaveData: any;
  AdvanceLeaveDataList: any;
  tempModal: any ;
  from: any;
  
  constructor(public httpService: HttpClient) { 
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId
    this.empPerId = this.loggedUser.empPerId
    this.advanceLeaveModal = new AdvanceLeave();
    this.tempModal = { fromDate :  '',  toDate: '' , type: 'All' }
    this.advanceLeaveModal.leaveId="selectvalue";
    this.getAllEmployee()
    this. getLeave();
    this.advanceLeaveModal.type = 'AdvanceLeave';
    this.advanceLeaveModal.fromDate=new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.advanceLeaveModal.toDate;
    this.getLeaveList();   
  }


  fromToDate(value) {
    this.from = value;
  }
  selectFromDate(event){
  }
  ngOnInit() {
    
  }
 getLeave(){
  let url = this.baseurl + '/EmpLeavesBalanceDetails/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.LeaveData = data['result']['list'];
    },
    (err: HttpErrorResponse) => {
    });
 }
 dropDown(event){
   this.tempModal.type = event
   this.getLeaveList();
 }

getLeaveList(){
  let url = this.baseurl + '/leaveBankWithdrawData';
  
    this.httpService.post(url,this.tempModal).subscribe(
     ( data : any )=> {
       this.AdvanceLeaveDataList = []
        this.AdvanceLeaveDataList = data.result;
        setTimeout(function () {
          $(function () {
            var table = $('#BusinessTable').DataTable();    
          });
        }, 100);
      
    },
    (err: HttpErrorResponse) => {
      this.AdvanceLeaveDataList = []
    });
}
fromEvent(event){
this.tempModal.fromDate = event
this.getLeaveList();
}
ToEvent(event){
  this.tempModal.toDate = event
  this.getLeaveList();
}

 getLeaveId(item) {
}

  getAllEmployee() {
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(
      data => {
        this.Employeedata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }

  arrowkeyLocation = 0;
  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }

  filterItem(value) {
    if (!value) {
      this.empFlag = false;
    }
    if (value) {
      this.empFlag = true;
    }
  }

  searchEmpPerId: any;
  suggestThis(item) {
    this.searchQuery = item.fullName + " " + item.lastName + " - " + item.empId;
    this.searchEmpPerId = item.empPerId;
    this.designation = item.descName
    this.empPerId = item.empPerId
    this.advanceLeaveModal.empPerId = item.empPerId
    this.empFlag = false;
    this.getLeave();
    // this.ResetPass.empPerId=item.empPerId;
    // this.getuser();
  }
  Fromvalue(event){
this.advanceLeaveModal.fromDate = event.target.value
var oneDay = 24*60*60*1000;
var diffDays = Math.round(Math.abs((new Date(this.advanceLeaveModal.fromDate).getTime() - new Date( this.advanceLeaveModal.toDate).getTime())/(oneDay)));
this.advanceLeaveModal.noOfLeave = diffDays + 1
this.getLeaveList()
  }
  Tovalue(event){
    
this.advanceLeaveModal.toDate = event.target.value
var oneDay = 24*60*60*1000;
var diffDays = Math.round(Math.abs((new Date(this.advanceLeaveModal.fromDate).getTime() - new Date( this.advanceLeaveModal.toDate).getTime())/(oneDay)));
this.advanceLeaveModal.noOfLeave = diffDays + 1
this.getLeaveList()
  }
  SubmitData(f){
    let url = this.baseurl + '/leaveBankWithdraw';
    this.httpService.post(url,this.advanceLeaveModal).subscribe(
      data => {
      f.reset();
      this.advanceLeaveModal.type = 'AdvanceLeave'
    },
    (err: HttpErrorResponse) => {
    });
  }
}
