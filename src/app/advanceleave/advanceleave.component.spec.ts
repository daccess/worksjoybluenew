import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceleaveComponent } from './advanceleave.component';

describe('AdvanceleaveComponent', () => {
  let component: AdvanceleaveComponent;
  let fixture: ComponentFixture<AdvanceleaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceleaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceleaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
