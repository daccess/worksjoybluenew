import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { CalenderMaster } from 'src/app/shared/model/clendermastermodel';
import { CalenderMasterService } from '../../../shared/services/clendermasterservice'
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-calendermaster',
  templateUrl: './calendermaster.component.html',
  styleUrls: ['./calendermaster.component.css']
})
export class CalendermasterComponent implements OnInit {
  dtInstance;
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  datatableElement: DataTableDirective
  dtTrigger: any = new Subject();
  dropdownSettings: {};
  selectedHoliday = [];
  selctWeeklyOf: any[];
  dropdownSet: {};
  weeklyFlag = false;
  selctWeeklyOff = "";
  ClaenderMasterModel: CalenderMaster;
  tempObj: CalenderMaster
  allActiveData: any[];
  baseurl = MainURL.HostUrl;
  compId: any
  Employeedataall: any;
  selectedItems: any[];
  selectedEmpTypeObj: any;
  getvalue: any;
  FinallistObj = {};
  weeklyofarry: any[]
  finallist = [];
  Weeklyaall: any;
  getobject: any;
  isedit: boolean;
  selectedFinal: any = [];
  weekSelect: any = [];
  weeklyoff: any;
  saveDirect: boolean = true;
  loggedUser: any;
  errordata: any;
  msg: any;
  calenderName: any;
  errflag: boolean;
  dltmodelFlag: boolean;
  dltObje: any;
  loconame: String;
  disButton: boolean = true
  employeeTypeMaster: {}
  public selectUndefinedOptionValue: any;
  public selected: string;
  allHolidayData = [];
  Today: string;
  applicabelFromDate: any;
  applicableTo: {};
  applDate : any;

  isWeekDayEditFlag: boolean = false;
  indexOfEditWeekDayRecord: any;
  weeklyOffErrorMsgFlag: boolean = false;
  public SelectWeekDay: string;
  locationwisedata:any=[];
  locationdata: any;
  selectedLocationobj: number;
  locationId: any;
  rollName: any;
  selectedLocationId: any;
  selectedlocationids: any=0;
  selectedWeeklyGroupobj: number;

  constructor(public httpService: HttpClient, public chRef: ChangeDetectorRef, public Calender: CalenderMasterService, public toster: ToastrService) {
    this.Today = new Date().getFullYear()+ "/" + (new Date().getMonth() + 1) + "/" +new Date().getDate();
    this.applicabelFromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.ClaenderMasterModel = new CalenderMaster();
    this.tempObj = new CalenderMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.locationId=this.loggedUser.locationId;
    this.rollName=this.loggedUser.mrollMaster.roleName;
    //console.log("rolnameis",    this.rollName);

  }

  ngOnInit() {
    this.getLocationList();
    this.isedit = false;
    this.getallWeekly(this.selectedlocationids);
    let url1 = this.baseurl + '/Location/Active/1';
    this.httpService.get(url1).subscribe(data => {
        this.allActiveData = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    let testempactive = this.baseurl + '/EmployeeType/Active/';
    this.httpService.get(testempactive + this.compId).subscribe(data => {
        this.Employeedataall = data["result"];
    },
    (err: HttpErrorResponse) => {
    });
    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'holidayMasterId',
      textField: 'holiDayName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    this.weeklyofarry = [
      { weeklyOffWeekId: 8, weekDay: 'None' },
      { weeklyOffWeekId: 1, weekDay: 'Sunday' },
      { weeklyOffWeekId: 2, weekDay: 'Monday' },
      { weeklyOffWeekId: 3, weekDay: 'Tuesday' },
      { weeklyOffWeekId: 4, weekDay: 'Wednesday' },
      { weeklyOffWeekId: 5, weekDay: 'Thursday' },
      { weeklyOffWeekId: 6, weekDay: 'Friday' },
      { weeklyOffWeekId: 7, weekDay: 'Saturday' }
    ];
    this.dropdownSet = {
      singleSelection: false,
      idField: 'weeklyOffWeekId',
      textField: 'weekDay',
      selectAllText: 'Select all',
      unSelectAllText: 'UnSelect all',
      itemsShowLimit: 8,
      allowSearchFilter: true
    };
   this.ClaenderMasterModel.applicabelFromDate = new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getDate();
   this.getallHolidayMasters();
   this.selctWeeklyOff = "Select Week Day"; 
   this.weeklyoff = "Select Weekly Number";

  }

  error() {
    if (this.loconame != this.ClaenderMasterModel.calenderName) {
      this.calenderName = this.ClaenderMasterModel.calenderName
      let url = this.baseurl + '/weeklyOffCalender/';
      this.httpService.get(url + this.calenderName).subscribe((data: any) => {
          this.errordata = data["result"];
          this.msg = data.message
          this.toster.error(this.msg);
          this.errflag = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag = false;
        });
    }
  }

  EmpValue(event) {
    this.selectedEmpTypeObj = parseInt(event.target.value);
  }
  
  Weeklycalendervalue(event) {
    this.selectedWeeklyGroupobj = parseInt(event.target.value)
}


  dataTableFlag : boolean  = true
  getallWeekly(e){

    this.selectedlocationids=e;
    if(this.loggedUser.roleHead=='HR'){
    let testempactive = this.baseurl + '/weeklyOfMaster/';
    this.dataTableFlag   = false
    this.httpService.get(testempactive + this.compId+'/'+this.locationId).subscribe(data => {
        this.Weeklyaall = data["result"];
       // console.log("allweeklydata", this.Weeklyaall);
        this.chRef.detectChanges();
        this.dataTableFlag   = true;
        this.chRef.detectChanges(); 
        setTimeout(function () {
          $(function () {
            var table = $('#CalComp').DataTable();
          });
        }, 1000);
        this.chRef.detectChanges();
      },
   ) }  
   else if(this.loggedUser.roleHead=='Admin'){
    let testempactive = this.baseurl + '/weeklyOfMaster/';
    this.dataTableFlag   = false
    this.httpService.get(testempactive + this.compId+'/'+ this.selectedlocationids).subscribe(data => {
        this.Weeklyaall = data["result"];
        this.chRef.detectChanges();
        this.dataTableFlag   = true;
        this.chRef.detectChanges(); 
        setTimeout(function () {
          $(function () {
            var table = $('#CalComp').DataTable();
          });
        }, 1000);
        this.chRef.detectChanges();
      },
   ) }  
    
  
   }
  
getLocationList(){
    

    let url = this.baseurl + '/Location/Active/'+this.compId;
    this.httpService.get(url).subscribe(data => {
        this.locationdata = data["result"];
        for(let i=0; i<this.locationdata.length; i++){
          if(this.loggedUser.roleHead == 'HR' && this.locationdata[i].locationId==this.loggedUser.locationId){
          
            this.locationwisedata.push(this.locationdata[i])
            
          }
          else if(this.loggedUser.roleHead == 'Admin') {
            this.locationwisedata.push(this.locationdata[i])
          }
          
        }

      },
      (err: HttpErrorResponse) => {
      });
  }
 

  getallHolidayMasters() {
    this.compId=this.loggedUser.compId
    this.applicableTo=this.ClaenderMasterModel.applicabelFromDate
    let obj = {
      applicableTo :new Date(this.ClaenderMasterModel.applicabelFromDate),
      compId : this.compId,
    
    }
    let url1 = this.baseurl + '/holcompulsory';
    this.httpService.post(url1,obj).subscribe(data => {
        this.allHolidayData = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }

  getObj(object) {
    this.getobject = object.weeklyOffCalenderMasterId;
    this.isedit = true;
    this.saveDirect = true;
    this.editobject(this.getobject);
  }

  masterID: any;
  editobject(getobject) {
    let urledit = this.baseurl + '/weeklyOfMasterById/';
    this.httpService.get(urledit + getobject).subscribe((data: any) => {
      
        var table = $('#CalComp').DataTable();
        this.ClaenderMasterModel.applicabelFromDate = data['result']['applicabelFromDate'];
        this.getallHolidayMasters();
        this.ClaenderMasterModel.calenderName = data['result']['calenderName'];
        this.loconame = this.ClaenderMasterModel.calenderName;
        this.ClaenderMasterModel.locationId = data['result']['locationId'];
       // console.log("locationdata",  this.ClaenderMasterModel.locationId);

        this.selectedFinal = data['result']['finallist'];
        setTimeout(() => {
          this.ClaenderMasterModel.selectedHoliday = data['result']['selectedHolidays'];
        }, 100);
        if (this.selectedFinal.length) {
          this.disButton = false;
        }
        this.masterID = data.result.weeklyOffCalenderMasterId;
      },
      (err: HttpErrorResponse) => {
      });
  }
  ngValue1(event){
    this.selectedLocationobj = parseInt(event.target.value);
   // console.log("selectedlocationfromadmin",  this.selectedLocationobj );
 }
 
  postWeeklyOff(f) {
    this.ClaenderMasterModel.applicabelFromDate = (new Date(this.ClaenderMasterModel.applicabelFromDate)).getTime()
    this.finallist.push(this.FinallistObj);
    this.ClaenderMasterModel.finallist = this.selectedFinal;
    this.ClaenderMasterModel.compId = this.compId;
    this.ClaenderMasterModel.locationId =    this.selectedLocationobj ;
   
    if (this.isedit == false) {
      let obj = this.ClaenderMasterModel;
      this.Calender.postCalenderMaster(obj).subscribe(data => {
          this.getallWeekly(this.selectedlocationids);
          f.reset();
          setTimeout(() => {
            this.selectedFinal = [];
            this.weekSelect = [];
            this.saveDirect = true;
            this.disButton = true;
            this.ClaenderMasterModel.selectedHoliday = [];
            this.selctWeeklyOff = "Select Week Day";
            this.weeklyoff = "Select Weekly Number";
            this.ClaenderMasterModel.applicabelFromDate = new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getDate();
          }, 500);
          this.toster.success('WeeklyOff-Master Information Added Successfully!', 'WeeklyOff-Master');
        },
        (err: HttpErrorResponse) => {
          this.toster.error('An Error Occurred Please Try again later', 'WeeklyOff-Master');
        });
    }else {
      let obj = this.ClaenderMasterModel;
      this.ClaenderMasterModel.weeklyOffCalenderMasterId = this.masterID;
 

      let url1 = this.baseurl + '/WekklyOffCalender';
      this.ClaenderMasterModel.compId = this.compId;
      this.ClaenderMasterModel.locationId = this.selectedLocationobj;
   
      this.httpService.put(url1, obj).subscribe(data => {
          this.toster.success('WeeklyOff-Master Information Updated Successfully!', 'WeeklyOff-Master');
          this.getallWeekly(this.selectedlocationids);
          f.reset();
          setTimeout(() => {
          this.selectedFinal = [];
          this.saveDirect = true;
          this.disButton = true;
          this.weekSelect = [];
          this.ClaenderMasterModel.applicabelFromDate = new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getDate();
          this.ClaenderMasterModel.selectedHoliday = [];
          this.selctWeeklyOff = "Select Week Day";
          this.weeklyoff = "Select Weekly Number";
          }, 500);
          this.isedit = false;
        }, (err: HttpErrorResponse) => {
          this.isedit = false;
          this.toster.error('Server Side Error..!', 'WeeklyOff-Master');
        });
    }
  }
  dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
  }

  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.weeklyOffCalenderMasterId;
    this.dltObjfun(this.dltObje);
  }

  dltObjfun(object) {
    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/weeklyoff/';
      this.ClaenderMasterModel.weeklyOffCalenderMasterId = this.dltObje;
      this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.toster.success('Delete Successfully', 'WeeklyOff-Master');
          this.getallWeekly(this.selectedlocationids);
        }, (err: HttpErrorResponse) => {
          this.toster.error('Already in use can not be deleted');
        });
    }
  }

  onItemSelect(item: any) {
    if (this.selectedHoliday.length == 0) {
      this.selectedHoliday.push(item);
    } else {
      this.selectedHoliday.push(item);
    }
  }

  onSelectAll(items: any) {
    this.selectedHoliday = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedHoliday.length; i++) {
      if (this.selectedHoliday[i].holidayMasterId == item.holidayMasterId) {
        this.selectedHoliday.splice(i, 1)
      }
    }
  }

  onDeSelectAll(item: any) {
    this.selectedHoliday = [];
  }

  setDate(event) {
    this.ClaenderMasterModel.selectedHoliday =[];
    this.ClaenderMasterModel.applicabelFromDate = new Date(event);
    this.getallHolidayMasters();
  }

  reset(){
    this.isedit = false
    this.selctWeeklyOff= "";
    setTimeout(() => {
      this.selectedFinal = [];
      this.weekSelect = [];
      this.saveDirect = true;
      this.disButton = true;
      this.ClaenderMasterModel.selectedHoliday = [];
      this.ClaenderMasterModel.applicabelFromDate = new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getDate();
      this.getallHolidayMasters();
      this.selctWeeklyOff = "Select Week Day";
      this.weeklyoff = "Select Weekly Number";
    }, 500);
  }

  /**START of Weekly off date table - 12Nov  */
  //STRAT OF THE ON CLICK ADD BUTTON OF WEEKLY OFF DAY
  addWeeklyOff(){
    if(this.isWeekDayEditFlag){
      this.selectedFinal[this.indexOfEditWeekDayRecord].weekDay = this.selctWeeklyOff;
      this.selectedFinal[this.indexOfEditWeekDayRecord].weeklyoff = this.weeklyoff;
      this.isWeekDayEditFlag = false;
    }
    else{
      let obj = {
        weeklyOffWeekId: 0,
        weekDay: this.selctWeeklyOff,
        weeklyoff: this.weeklyoff
  
      }
      this.selectedFinal.push(obj);
    }
    this.selctWeeklyOff = "Select Week Day";
    this.weeklyoff = "Select Weekly Number";
  }
  //END OF THE ON CLICK ADD BUTTON OF WEEKLY OFF DAY

  //STRAT OF THE ON CLICK EDIT BUTTON OF WEEKLY OFF DAY
  editWeeklyOff(c, b) {
    this.selctWeeklyOff = c.weekDay;
    this.weeklyoff = c.weeklyoff;
    this.isWeekDayEditFlag = true;
    this.indexOfEditWeekDayRecord = b;
  }
  //END OF THE ON CLICK EDIT BUTTON OF WEEKLY OFF DAY

  //STRAT OF THE ON CLICK DELETE BUTTON OF WEEKLY OFF DAY
  subWeek: any;
  indexx: any
  deleteWeek(data, i) {
    this.subWeek = data;
    this.indexx = i;
  }
  deleteWeekoff() {
    this.selectedFinal.splice(this.indexx, 1);
  }
  //END OF THE ON CLICK DELETE BUTTON OF WEEKLY OFF DAY

  /**END of Weekly off date table - 12Nov  */
}

