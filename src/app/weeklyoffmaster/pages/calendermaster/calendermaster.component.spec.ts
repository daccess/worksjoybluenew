import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendermasterComponent } from './calendermaster.component';

describe('CalendermasterComponent', () => {
  let component: CalendermasterComponent;
  let fixture: ComponentFixture<CalendermasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendermasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendermasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
