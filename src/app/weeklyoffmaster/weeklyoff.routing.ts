import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';


import { WeeklyoffmasterComponent } from 'src/app/weeklyoffmaster/weeklyoffmaster.component';
import { CalendermasterComponent } from 'src/app/weeklyoffmaster/pages/calendermaster/calendermaster.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: WeeklyoffmasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'weeklyoffmaster',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'weeklyoffmaster',
                    component: CalendermasterComponent
                },


            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);