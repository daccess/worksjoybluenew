import { routing } from '../../app/weeklyoffmaster/weeklyoff.routing';
import { NgModule } from '@angular/core';
 
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';

import { WeeklyoffmasterComponent } from 'src/app/weeklyoffmaster/weeklyoffmaster.component';
import { CalendermasterComponent } from 'src/app/weeklyoffmaster/pages/calendermaster/calendermaster.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CalenderMasterService } from '../shared/services/clendermasterservice';


// import { HolidaymComponent } from './pages/holidaym/holidaym.component';


@NgModule({
    declarations: [
        WeeklyoffmasterComponent,
        CalendermasterComponent
    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule
    ],
    providers: [CalenderMasterService]
  })
  export class WleeklyoffModule { 
      constructor(){

      }
  }
  