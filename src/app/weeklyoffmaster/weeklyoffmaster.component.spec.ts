import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyoffmasterComponent } from './weeklyoffmaster.component';

describe('WeeklyoffmasterComponent', () => {
  let component: WeeklyoffmasterComponent;
  let fixture: ComponentFixture<WeeklyoffmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyoffmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyoffmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
