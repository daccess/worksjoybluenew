import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ReportService } from '../shared/services/ReportService';
import { ExitInterviewQuestion } from '../shared/model/ExitInterviewQuestion';
import { MainURL } from '../shared/configurl';
import { dataService } from '../shared/services/dataService';
import { ToastrService } from 'ngx-toastr';
import { ClearenceService } from '../shared/services/clearenceservice';
@Component({
  selector: 'app-exitinterviewreport',
  templateUrl: './exitinterviewreport.component.html',
  styleUrls: ['./exitinterviewreport.component.css']
})
export class ExitinterviewreportComponent implements OnInit {
  exitInterviewQueObj: ExitInterviewQuestion;
  public exitinterviewDataArray = [];
  parsedData: any;
  loggedUser: any;
  compId: any;
  selectedDept: any;
  AssetsPath=MainURL.AssetsPath;
  delegateempId:any;
  //deldeligationOfEmployee url
  deligationOfEmployee_url ='/deligationOfEmployee';
  resignempId:any;
  resigAndnLeavingIdfordeligation:any;
   empIdEmployee: any;
   resignempname:any;
 searchTextSanction: string = "";
  delegateempobj:any=[];
  searchSanctionFlag: boolean = false;
  showbuttonflag: boolean= false;
 
  constructor(private reportService: ReportService,private service: ClearenceService, public toastr: ToastrService,public chRef: ChangeDetectorRef,private dataservice:dataService) {
    this.exitInterviewQueObj = new ExitInterviewQuestion();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.modeflag = false;
    this.compId = this.loggedUser.compId
    let date = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.startDate = new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + new Date().getDate();;
    this.model.endDate = date;
  }

  ngOnInit() {
    this.getAllDepartments();
    this.getAllExitInterviewData();
    this.getAllSanctioningEmployee();
  }
  departmentsData = [];
  getAllDepartments() {
    this.reportService.getAllDepartments(this.loggedUser.compId).subscribe(data => {
      this.departmentsData = data.result;
      this.model.departmentMaster.deptId = this.departmentsData[0].deptId;
    }, err => {
    })
  }
  dataTableFlag: boolean = true;
  AllExitInterviewData = [];
  model = { departmentMaster: { deptId: '' }, startDate: '', endDate: '' };
  fromDateChanged(e){
    this.model.startDate = e;
    this.getAllExitInterviewData();
  }
  endDateChanged(e){
    this.model.endDate = e;
    this.getAllExitInterviewData();
  }
  getAllExitInterviewData() {
    this.dataTableFlag = false;
    this.exitinterviewDataArray = [];
    this.reportService.getAllExitInterviewData(this.model).subscribe(data => {
      this.exitinterviewDataArray = data.result;
      for(var i =0;i<this.exitinterviewDataArray.length;i++){
        if(this.exitinterviewDataArray[i].sanctioningAuthority == true){
          this.showbuttonflag =true;
        }
      }
      this.dataTableFlag = true;
      setTimeout(function () {
        $(function () {
          var table = $('#Business').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
    }, err => {
      this.dataTableFlag = true;
    })
  }
  getExitInterviewList(empData) {
    if (!empData.questionAnswerData) {
      this.reportService.getExitInterviewReportServiceList(empData.empOfficialId).subscribe(data => {
        empData.questionAnswerData = data.result;
        setTimeout(function () {
          $(function () {
            var table = $('#Business').DataTable();
          });
        }, 1000);
      }, err => {
      })
    }
  }
 
  suggestThisEmployeeSanction(item) {
    this.searchTextSanction = item.empId + "-" + item.fullName + " " + item.lastName;
    // this.resigntaion.delegationEmpId = item.empId;
    this.delegateempobj= item;
    for(let i =0;i<this.delegateempobj.length;i++){
      this.delegateempId = this.delegateempobj[i].empId;
    }
    this.searchSanctionFlag = false;
  }

  allSanctionEmployees = [];
  getAllSanctioningEmployee() {
    this.service.getAllSanctioningEmployee().subscribe(data => {
      this.allSanctionEmployees = data.result;
      for (let i = 0; i <  this.allSanctionEmployees.length; i++) {
        this.empIdEmployee =this.allSanctionEmployees[i].empId;
      if(this.resignempname !=this.allSanctionEmployees[i].fullName+" "+ this.allSanctionEmployees[i].lastName){
          this.allSanctionEmployees =this.allSanctionEmployees;
        }
      }
    })
  }

  getSearchSuggestionListSanction(text) {
    if (!text) {
      this.searchSanctionFlag = false;
    }
    else if (text) {
      this.searchSanctionFlag = true;
    }
  }
  deligateData = [];
  getDeligateData(data){
    this.delegateempId =data.empId;
    this.resigAndnLeavingIdfordeligation =data.resigAndnLeavingId;
  }
  deligateEmp(){
   let obj={
      empId:this.empIdEmployee,
      delegationEmpId:this.delegateempId,
      resigAndnLeavingId:this.resigAndnLeavingIdfordeligation
    }
    if(obj != null){
         this.dataservice.createRecord(this.deligationOfEmployee_url,obj).subscribe(res=>{
          if(res.statusCode==200){
            this.toastr.success("Delegation process done successfully");
            this.searchSanctionFlag= true;
          }
      });
    }
  }
}