import { routing } from './exitinterviewreport.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ExitinterviewreportComponent } from './exitinterviewreport.component';
import { ReportService } from '../shared/services/ReportService';
import { SearchsPipe } from './Shared/searchs.pipe';
import { ClearenceService } from '../shared/services/clearenceservice';



@NgModule({
    declarations: [
        ExitinterviewreportComponent,
        SearchsPipe

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: [ReportService,ClearenceService]
  })
  export class  ExitinterviewreportModule { 
      constructor(){

      }
  }
  