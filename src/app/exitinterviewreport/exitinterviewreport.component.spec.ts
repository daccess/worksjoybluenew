import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitinterviewreportComponent } from './exitinterviewreport.component';

describe('ExitinterviewreportComponent', () => {
  let component: ExitinterviewreportComponent;
  let fixture: ComponentFixture<ExitinterviewreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitinterviewreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitinterviewreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
