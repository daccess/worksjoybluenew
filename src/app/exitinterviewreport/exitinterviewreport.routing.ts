import { Routes, RouterModule } from '@angular/router'
import { ExitinterviewreportComponent } from './exitinterviewreport.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ExitinterviewreportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'exitinterviewreport',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'exitinterviewreport',
                    component: ExitinterviewreportComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);