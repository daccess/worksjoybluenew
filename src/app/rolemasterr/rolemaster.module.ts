import { routing } from './rolemaster.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { RolemasterComponent } from './rolemaster.component'
import { QuerySeprationService } from './Shared/QuerySeprationService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    declarations: [
        RolemasterComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     UiSwitchModule,
     NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [QuerySeprationService]
  })
  export class RolemasterModule { 
      constructor(){

      }
  }
  