import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

// import { satutory } from '../models/satutorymodel';
// import { MainURL } from '../../../shared/configurl';
import { from } from 'rxjs';
import { MainURL } from 'src/app/shared/configurl';
//import { satutory } from 'src/app/employee/shared/models/satutorymodel';
import {qerySeprationModal} from '../Shared/QuerySeprationModel'
import {queryModel } from '../Shared/QeryModel'

import { HttpClient } from '@angular/common/http';
// import { applyLeave } from '../model/applyLeave';

@Injectable()
export class QuerySeprationService
 {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http,public httpService: HttpClient) { }

  postRole(Qery : qerySeprationModal){
    let url = this.baseurl + '/createQueryAdminRole';
    var body = JSON.stringify(Qery);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  updateQueryAdmin( queryAdmin : queryModel ){
    let url = this.baseurl + '/updatequeryadmin';
    var body = JSON.stringify(queryAdmin);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,queryAdmin).map(x => x.json());
  }

  PostQueryAdmin( queryAdmin : queryModel ){
    let url = this.baseurl + '/queryadmin' ;
    var body = JSON.stringify(queryAdmin);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  PostQueryAdminInd( queryAdmin : queryModel ){
    let url = this.baseurl + '/createInductionProcessAdmin' ;
    var body = JSON.stringify(queryAdmin);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  //update induction
  updateQueryInduction( queryAdmin : queryModel ){
    let url = this.baseurl + '/updateInductionProcessAdmin';
    var body = JSON.stringify(queryAdmin);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,queryAdmin).map(x => x.json());
  }

  getGrade(id: number) {
    return this.http.get(this.baseurl +'/GradeMaster/Active/'+ id)
    // .subscribe(data=>{
    //   data.json()
    // });
  }

  getLocaction(id: number) {
    return this.http.get(this.baseurl +'/Location/Active/'+ id)
  }
  getDepartment(id: number) {
    return this.http.get(this.baseurl +'/Department/'+ id)
  }
  getPayGroupHead(id: number) {
    return this.http.get(this.baseurl +'/PayGroup/'+ id)
  }
  getDesignation (id: number) {
    return this.http.get(this.baseurl +'/Designation/'+ id)
  }

  getCategory (id: number) {
    return this.http.get(this.baseurl +'/EmployeeType/'+ id)
  }

  getRoleById (id: number) {
    return this.http.get(this.baseurl +'/adminRole/'+ id)
  }
 
  updateRole( queryrole){
    let url = this.baseurl + '/updateQueryAdminRole';
    var body = JSON.stringify(queryrole);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,queryrole).map(x => x.json());
  }
  
}
