import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { RolemasterComponent } from 'src/app/rolemasterr/rolemaster.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: RolemasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'importemployee',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'rolemasterr',
                    component: RolemasterComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);