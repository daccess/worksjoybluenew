import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { QuerySeprationService } from './Shared/QuerySeprationService';
import 'rxjs/add/operator/map';
import { MainURL } from '../../app/shared/configurl';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { qerySeprationModal } from './Shared/QuerySeprationModel'
import { queryModel } from './Shared/QeryModel'
import { RoleMaster } from './Shared/RoleMasterModel';
import { RoleMasterServiceData } from './Shared/RoleMasterService'
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rolemaster',
  templateUrl: './rolemaster.component.html',
  styleUrls: ['./rolemaster.component.css'],
  providers: [RoleMasterServiceData]
})
export class RolemasterComponent implements OnInit {
  dropdownSettingsGrade: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsLocation: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsEmploymentType: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsLeaveEncash: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsDepartment: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsLeaveClubbing: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  loggedUser: any;
  baseurl = MainURL.HostUrl;
  compId: any;
  locationData: any;
  gradeList: any;
  payGroup: any;
  gradeMasterList: any = [];
  locationMasterList = [];
  departmentMasterList: any = [];
  designationMasterList: any = [];
  payGroupMasterList: any = [];
  employeeTypeMasterList: any = [];
  queryAdminWiseEmployementTypeList:any=[];
  empOfficialDataMasterList: any = [];
  flag: boolean;
  flag2: boolean;
  DepartmentData: any;
  dropdownSettingPayGroup: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  Designation: any;
  dropdownSettingsDesignation: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  EmpCat: any;
  dropdownSettingsCatg: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsCateg: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsCateInd: any;
  QyerModel: qerySeprationModal;
  querylist = [];
  departlist: any;
  QyerModelSecond: queryModel;
  deptId: number;
  EmployyUnderDpt = [];
  dropdownSettingsundpt: any;
  SelectedRole: number;
  selectdepartment: number;
  QueryAdminlist = [];
  getobj: any;
  dltmodelFlag: boolean;
  AllRoll: any;
  selectedRoleobj: number;
  RoleMOdel: RoleMaster;
  isedit = false;
  disableuser:boolean =false;
 public selectUndefinedOptionValue: any;
 public selectValue: any;
 public selectdepartmentobj: any;
 selectedCompanyobj: any;
 isRights: boolean = false;
 themeColor : string = "nav-pills-blue";
  empOfficialId: string;
  queryAdminRoleId: string;
  dltObje: any;
  dltObject: any;
  deletRoleList: any;
  disable_assign=false;
  show_induction_update_btn=false;
  constructor(public toastr: ToastrService, 
    public httpService: HttpClient, 
    public chRef: ChangeDetectorRef, 
    private QueryService: QuerySeprationService,
    public RoleService: RoleMasterServiceData,
    public Spinner :NgxSpinnerService,private router : Router) {
    this.RoleMOdel = new RoleMaster();
    this.QyerModel = new qerySeprationModal();
    this.QyerModelSecond = new queryModel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.getAll();
      let themeColor = sessionStorage.getItem('themeColor');
      this.themeColor = themeColor;
    this.ResetInfo();

  }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.RoleMOdel.roleHead = "CompanyHead";
    this.getQueryadminlist();
    this.getDepartment();
    this.getQueryAdminList(this.SelectedRole);
    this.QyerModel = new qerySeprationModal();
    let tabId = sessionStorage.getItem("a");
    if (tabId == 'Query') {
     document.getElementById("pills-profile-tab").click();
     sessionStorage.setItem("a","");
    }
    
    this.dropdownSettingsGrade = {
      singleSelection: false,
      idField: 'gradeId',
      textField: 'gradeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsLocation = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingPayGroup = {
      singleSelection: false,
      idField: 'payGroupId',
      textField: 'payGroupName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };


    this.dropdownSettingsDesignation = {
      singleSelection: false,
      idField: 'descId',
      textField: 'descName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsDepartment = {
      singleSelection: false,
      idField: 'deptId',
      textField: 'deptName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsCateg = {
      singleSelection: false,
      idField: 'empTypeId',
      textField: 'empTypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    this.dropdownSettingsCateInd = {
      singleSelection: false,
      idField: 'empTypeId',
      textField: 'employmentType',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsundpt = {
      singleSelection: false,
      idField: 'empId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true,
      limitSelection:1
    };

    this.QueryService.getGrade(this.compId)
      .subscribe(data => {
        this.gradeList = data.json().result;
      });

    this.QueryService.getLocaction(this.compId)
      .subscribe(data => {
        this.locationData = data.json().result;
      });

    this.QueryService.getDepartment(this.compId)
      .subscribe(data => {
        this.DepartmentData = data.json().result;
      })

    this.QueryService.getPayGroupHead(this.compId).subscribe(data => {
      this.payGroup = data.json().result;
    })
    this.QueryService.getDesignation(this.compId).subscribe(data => {
      this.Designation = data.json().result;
    })
    this.QueryService.getCategory(this.compId).subscribe(data => {
      this.EmpCat = data.json().result;
    })

  }
  onItemSelect(item: any) {
    this.gradeMasterList.push(item);
  }
  onSelectAll(items: any) {
    this.gradeMasterList = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.gradeMasterList.length; i++) {
      if (this.gradeMasterList[i].gradeId == item.gradeId) {
        this.gradeMasterList.splice(i, 1);
      }
    }
  }

  onDeSelectAll(item: any) {
    this.gradeMasterList = [];
  }

  //===========================grade end=============================

  //==========================Location Start=========================

  onItemSelectl(item: any) {
    this.locationMasterList.push(item);

  }
  onSelectAlll(items: any) {
    this.locationMasterList = items;
  }

  OnItemDeSelectl(item: any) {
    for (var i = 0; i < this.locationMasterList.length; i++) {
      if (this.locationMasterList[i].locationId == item.locationId) {
        this.locationMasterList.splice(i, 1);
      }
    }
  }

  onDeSelectAlll(item: any) {
    this.locationMasterList = [];
  }

  //===========================Location End======================= 

  //==========================department Start=========================

  onItemSelectd(item: any) {
    this.departmentMasterList.push(item);
  }
  onSelectAlld(items: any) {
    this.departmentMasterList = items;
  }

  OnItemDeSelectd(item: any) {
    for (var i = 0; i < this.departmentMasterList.length; i++) {
      if (this.departmentMasterList[i].deptId == item.deptId) {
        this.departmentMasterList.splice(i, 1);
      }
    }
  }

  onDeSelectAlld(item: any) {
    this.departmentMasterList = [];
  }

  //===========================department End======================= 
  //==========================Pay Group Start=========================

  onItemSelectp(item: any) {
    this.payGroupMasterList.push(item);
  }
  onSelectAllp(items: any) {
    this.payGroupMasterList = items;
  }

  OnItemDeSelectp(item: any) {
    for (var i = 0; i < this.payGroupMasterList.length; i++) {
      if (this.payGroupMasterList[i].payGroupId == item.payGroupId) {
        this.payGroupMasterList.splice(i, 1);
      }
    }
  }

  onDeSelectAllp(item: any) {
    this.payGroupMasterList = [];
  }

  //===========================Pay Group End======================= 
  //==========================designation Start=========================

  onItemSelectdes(item: any) {
    this.designationMasterList.push(item);
  }
  onSelectAlldes(items: any) {
    this.designationMasterList = items;
  }

  OnItemDeSelectdes(item: any) {
    for (var i = 0; i < this.designationMasterList.length; i++) {
      if (this.designationMasterList[i].descId == item.descId) {
        this.designationMasterList.splice(i, 1);
      }
    }
  }

  onDeSelectAlldes(item: any) {
    this.designationMasterList = [];
  }

  //===========================designation end======================= 

  //==========================Emp Start=========================

  onItemSelectemp(item: any) {
    this.employeeTypeMasterList.push(item);
  }
  //induction select
  onItemSelectIndEmp(item: any) {
    this.queryAdminWiseEmployementTypeList.push(item);
  }
  onSelectAllemp(items: any) {
    // this.selectedItems =[];
    this.employeeTypeMasterList = items;
  }
  //induction emp
  onSelectAllempInd(items: any) {
    this.queryAdminWiseEmployementTypeList = items;
  }

  OnItemDeSelectemp(item: any) {
    for (var i = 0; i < this.employeeTypeMasterList.length; i++) {
      if (this.employeeTypeMasterList[i].descId == item.descId) {
        this.employeeTypeMasterList.splice(i, 1)
      }
    }
  }
  //indction
  OnItemDeSelectempInd(item: any) {
    for (var i = 0; i < this.queryAdminWiseEmployementTypeList.length; i++) {
      if (this.queryAdminWiseEmployementTypeList[i].empTypeName == item.empTypeName) {
        this.queryAdminWiseEmployementTypeList.splice(i, 1)
      }
    }
  }

  onDeSelectAllemp(item: any) {
    this.employeeTypeMasterList = [];
  }
  //induction emp
  onDeSelectAllempInd(item: any) {
    this.queryAdminWiseEmployementTypeList = [];
  }

  //===========================designation end=======================
  //==========================Emp Start=========================

  onItemSelectundpt(item: any) {
    this.empOfficialDataMasterList.push(item);
  }
  onSelectAllundpt(items: any) {
    this.empOfficialDataMasterList = items;
  }

  OnItemDeSelectundpt(item: any) {
    for (var i = 0; i < this.empOfficialDataMasterList.length; i++) {
      if (this.empOfficialDataMasterList[i].empOfficialId == item.empOfficialId) {
        this.empOfficialDataMasterList.splice(i, 1);
      }
    }
  }

  onDeSelectAllundpt(item: any) {
    this.empOfficialDataMasterList = [];
  }

  //===========================designation end======================= 
  roleAdded : boolean = false;
  postQuery() {
    // this.QyerModel.status
    this.QyerModel.compId = this.loggedUser.compId;
    this.QyerModel.roleType = this.roleType;
    console.log(this.QyerModel);
    //test for induction
    if(this.QyerModel.roleType=="induction"){
        if(this.QyerModel.queryAdminRoleName!="Medical" && this.QyerModel.queryAdminRoleName!="Safety"){
            this.toastr.error("You can create Role as Medical and Safety Only..!");
            return false;
        }
    }
    this.Spinner.show();
    this.QueryService.postRole(this.QyerModel)
      .subscribe(data => {
        this.roleAdded = true;
        this.getQueryadminlist();
        this.QyerModel = new qerySeprationModal();
        this.Spinner.hide();
        this.toastr.success(' Role Inserted Successfully!', 'Role-Master');
      },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Role-Master');
        });
  }
  addNewRole(){
    this.isRoleEdit = false;
    this.QyerModel = new qerySeprationModal();
    this.SelectedRole = 0;
    this.QyerModelSecond.departmentMasterList = [];
    this.QyerModelSecond.designationMasterList = [];
    this.QyerModelSecond.employeeTypeMasterList = [];
    this.QyerModelSecond.empOfficialDataMasterList = [];
    this.QyerModelSecond.gradeMasterList = [];
    this.QyerModelSecond.locationMasterList = [];
    this.QyerModelSecond.payGroupMasterList = [];
    this.selectdepartment = 0;
    this.getobj = '';
  }
  getQueryadminlist() {
    this.selectedRoleName = "";
    this.querylist = [];
    let url = this.baseurl + '/queryAdminRoleList/?compId=' + this.loggedUser.compId + '&roleType=' + this.roleType;
    this.httpService.get(url).subscribe(data => {
      this.querylist = data['result'];
      if(this.roleAdded){
        this.SelectedRole = this.querylist[this.querylist.length-1].queryAdminRoleId;
        this.roleSelect(this.SelectedRole);
        this.roleAdded = false;
      }
    })
  }
  getInactiveQueryadminlist() {
    this.selectedRoleName = "";
    this.querylist = [];
    let url = this.baseurl + '/inactiveQueryRoleList/?compId=' + this.loggedUser.compId + '&roleType=' + this.roleType;
    this.httpService.get(url).subscribe(data => {
      this.querylist = data['result'];
    })
  }
  getDepartment() {
    let url = this.baseurl + '/Department/'
    this.httpService.get(url + this.compId).subscribe(data => {
      this.departlist = data['result'];
    })
  }
  DepartObj(event) {
    this.deptId = event.target.value;
    this.EmpList(this.deptId);
  }
  tempAdminEmpList = [];
  EmpList(deptId) {
    let url = this.baseurl + '/empListUnderDept/'
    this.httpService.get(url + deptId).subscribe(data => {
      this.EmployyUnderDpt = data['result'];
      for (let i = 0; i < this.EmployyUnderDpt.length; i++) {
        this.EmployyUnderDpt[i].fullName = this.EmployyUnderDpt[i].empId+"-" + this.EmployyUnderDpt[i].fullName+" "+this.EmployyUnderDpt[i].lastName;
      }
      // for (let i = 0; i < this.EmployyUnderDpt.length; i++) {
      //   for (let j = 0; j < this.QueryAdminlist.length; j++) {
      //     if(this.EmployyUnderDpt[i].empOfficialId == this.QueryAdminlist[j].empOfficialId){
      //       this.EmployyUnderDpt.splice(i,1);
      //     }
      //   }
      // }
    })
  }
  PostQuery() {
    if (this.flag12) {
      this.Spinner.show()
      this.QueryService.updateQueryAdmin(this.QyerModelSecond).subscribe(data => {
          this.Spinner.hide()
          this.chRef.detectChanges();
          this.getQueryAdminList(this.SelectedRole);
          this.flag12 = false;
          this.QyerModelSecond.departmentMasterList = []
          this.QyerModelSecond.designationMasterList = []
          this.QyerModelSecond.employeeTypeMasterList = []
          this.QyerModelSecond.empOfficialDataMasterList = []
          this.QyerModelSecond.gradeMasterList = []
          this.QyerModelSecond.locationMasterList = []
          this.QyerModelSecond.payGroupMasterList = []
          // this.QyerModelSecond.SelectedRole = ''
          this.getobj = '';
          this.toastr.success('Successfully Updated', 'Role-Master');
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Failed To Update', 'Role-Master');
          });
    }
    else {
      this.Spinner.show();
      this.QyerModelSecond.deptId = this.selectdepartment;
      this.QyerModelSecond.queryAdminRoleId = this.SelectedRole;
      this.QueryService.PostQueryAdmin(this.QyerModelSecond).subscribe(data => {
          this.chRef.detectChanges();
          this.getQueryAdminList(this.SelectedRole);
          this.flag12 = false;
          this.QyerModelSecond.roleType = this.roleType;
          this.QyerModelSecond.departmentMasterList = [];
          this.QyerModelSecond.designationMasterList = [];
          this.QyerModelSecond.employeeTypeMasterList = [];
          this.QyerModelSecond.empOfficialDataMasterList = [];
          this.QyerModelSecond.gradeMasterList = [];
          this.QyerModelSecond.locationMasterList = [];
          this.QyerModelSecond.payGroupMasterList = [];
          // this.QyerModelSecond.SelectedRole = '';
          this.getobj = '';
          this.Spinner.hide()
          this.toastr.success('Successfully Added', 'Role-Master');
        },
          (err: any) => {
            this.Spinner.hide()
            if(err.json().result.msg){
              this.toastr.error(err.json().result.msg);
            }
            else{
              this.toastr.error('Failed To Add..!', 'Role-Master');
            }
            
          });

    }
  }
  selectedRoleName : string = "";
  roleSelect(event) {
    this.getQueryAdminList(event);
    this.QyerModelSecond.departmentMasterList = [];
    this.QyerModelSecond.designationMasterList = [];
    this.QyerModelSecond.employeeTypeMasterList = [];
    this.QyerModelSecond.empOfficialDataMasterList = [];
    this.QyerModelSecond.gradeMasterList = [];
    this.QyerModelSecond.locationMasterList = [];
    this.QyerModelSecond.payGroupMasterList = [];
    this.QyerModelSecond.queryAdminWiseEmployementTypeList=[];
    this.QueryAdminlist = [];
    this.selectdepartment = 0;
    this.getobj = '';
    this.getRoleById(event);
    for (let i = 0; i < this.querylist.length; i++) {
      if(this.querylist[i].queryAdminRoleId == event){
        this.selectedRoleName = this.querylist[i].queryAdminRoleName;

      }
    }
  }
  isRoleEdit: boolean = false;
  getRoleById(id) {
    this.QueryService.getRoleById(id).subscribe(data => {
      if (data.json().result) {
        this.QyerModel = data.json().result;
        this.QyerModel.status = data.json().result.status;
      }
      this.isRoleEdit = true;
    }, err => {
    })
  }

  updateRole() {
    //test for induction
    if(this.QyerModel.roleType=="induction"){
      if(this.QyerModel.queryAdminRoleName!="Medical" && this.QyerModel.queryAdminRoleName!="Safety"){
          this.toastr.error("You can create Role as Medical and Safety Only..!");
          return false;
      }
  }
    this.Spinner.show();
    this.QueryService.updateRole(this.QyerModel).subscribe(data => {
      this.isRoleEdit = false;
      this.getQueryadminlist();
      this.Spinner.hide()
      this.QyerModel = new qerySeprationModal();
      
      this.toastr.success("Role Updated Successfully");
    }, err => {
      this.Spinner.hide()
      this.toastr.error("Failed To Update Role");
    })
  }
  getQueryAdminList(role) {
    let url = this.baseurl + '/queryRoleWiseEmpList/' + role;
    this.QueryAdminlist = [];
    this.httpService.get(url).subscribe((data: any) => {
      this.QueryAdminlist = data['result'];
      this.tempAdminEmpList = data['result'];
      this.EmployyUnderDpt = [];
      this.selectdepartment = 0;
    })
  }

  flag12: boolean = false;
  adminRoleWiseEmpId: number;
  getQeryAdminObj(obj) {
    this.getobj = obj.empOfficialId;
    this.adminRoleWiseEmpId = obj.adminRoleWiseEmpId;
    this.QyerModelSecond.adminRoleWiseEmpId = obj.adminRoleWiseEmpId;
    this.getByIdQueryAdmin(obj);
  }
  //induction update
  getQeryAdminObjInd(obj) {
    this.disable_assign=true;
    this.show_induction_update_btn=true;
    this.getobj = obj.empOfficialId;
    this.adminRoleWiseEmpId = obj.adminRoleWiseEmpId;
    this.QyerModelSecond.adminRoleWiseEmpId = obj.adminRoleWiseEmpId;
    this.getByIdQueryAdminInd(obj);
  }
  roleType: string = "query";
  selectRoleType(val) {
    this.roleType = val;
    this.SelectedRole=this.selectValue;
    this.getQueryadminlist();
    //reset data if tab changes
    this.QyerModel = new qerySeprationModal();
    this.QyerModelSecond = new queryModel();
    if(this.roleType=='induction'){
        this.setInductionEmployment();
    }else{
      this.QueryService.getCategory(this.compId).subscribe(data => {
        this.EmpCat = data.json().result;
      })
    }
    //reset previous selections
    this.ResetAllTabs();
  }
  getByIdQueryAdmin(obj) {
    let url = this.baseurl + '/getEmpDataByOfficialId/?empOfficialId=' + obj.empOfficialId + '&queryAdminRoleId=' + obj.queryAdminRoleId;
    this.httpService.get(url).subscribe((data: any) => {
      this.flag12 = true;
      this.QyerModelSecond.empOfficialId = this.getobj;
      this.QyerModelSecond.gradeMasterList = data['result']['gradeList'];
      this.QyerModelSecond.locationMasterList = data['result']['locationList'];
      this.QyerModelSecond.departmentMasterList = data['result']['departmentList'];
      this.QyerModelSecond.designationMasterList = data['result']['designationList'];
      this.QyerModelSecond.payGroupMasterList = data['result']['payGroupList'];
      this.QyerModelSecond.employeeTypeMasterList = data['result']['empTypeList'];
      this.QyerModelSecond.empOfficialDataMasterList = data['result']['empList'];
      for (let i = 0; i < this.QyerModelSecond.empOfficialDataMasterList.length; i++) {
        this.QyerModelSecond.empOfficialDataMasterList[i].fullName = this.QyerModelSecond.empOfficialDataMasterList[i].fullName+" "+this.QyerModelSecond.empOfficialDataMasterList[i].lastName;
      }
      if(data.result.empList){
        this.QyerModelSecond.adminRoleWiseEmpId = data.result.empList[0].adminRoleWiseEmpId;
        this.QyerModelSecond.queryAdminRoleId = data.result.empList[0].queryAdminRoleId;
        this.selectdepartment = data.result.empList[0].deptId;
        this.EmpList(this.selectdepartment);
      }
    }, err => {
    })
  }
  //ind table click
  getByIdQueryAdminInd(obj) {
    let url = this.baseurl + '/getEmpDataForInduction/?empOfficialId=' + obj.empOfficialId + '&queryAdminRoleId=' + obj.queryAdminRoleId;
    this.httpService.get(url).subscribe((data: any) => {
      this.QyerModelSecond.empOfficialId = this.getobj;
      this.QyerModelSecond.locationMasterList = data['result']['locationList'];
      this.QyerModelSecond.queryAdminWiseEmployementTypeList=data['result']['queryAdminWiseEmployementTypeList'];
      this.QyerModelSecond.empOfficialDataMasterList = data['result']['empList'];
      for (let i = 0; i < this.QyerModelSecond.empOfficialDataMasterList.length; i++) {
        // this.QyerModelSecond.empOfficialDataMasterList[i].fullName = this.QyerModelSecond.empOfficialDataMasterList[i].fullName+" "+this.QyerModelSecond.empOfficialDataMasterList[i].lastName;
        this.QyerModelSecond.empOfficialDataMasterList[i].fullName = this.QyerModelSecond.empOfficialDataMasterList[i].empId+"-" + this.QyerModelSecond.empOfficialDataMasterList[i].fullName+" "+this.QyerModelSecond.empOfficialDataMasterList[i].lastName;
      }
      if(data.result.empList){
        this.QyerModelSecond.adminRoleWiseEmpId = data.result.empList[0].adminRoleWiseEmpId;
        this.QyerModelSecond.queryAdminRoleId = data.result.empList[0].queryAdminRoleId;
        this.selectdepartment = data.result.empList[0].deptId;
        this.EmpList(this.selectdepartment);
      }
      if(this.QyerModelSecond.queryAdminWiseEmployementTypeList.length!=0){
        for(let i=0;this.QyerModelSecond.queryAdminWiseEmployementTypeList.length;i++){
            if(this.QyerModelSecond.queryAdminWiseEmployementTypeList[i].employmentType){
              if(this.QyerModelSecond.queryAdminWiseEmployementTypeList[i].employmentType=='On Roll'){
                this.QyerModelSecond.queryAdminWiseEmployementTypeList[i].empTypeId="001";
              }
              if(this.QyerModelSecond.queryAdminWiseEmployementTypeList[i].employmentType=='On Contract'){
                this.QyerModelSecond.queryAdminWiseEmployementTypeList[i].empTypeId="425";
              }
            }
        }
      }
      
    }, err => {
    })
  }
  rollNameFlag : boolean = false;
  roleNameChanged(event){
   this.rollNameFlag = false;
   for (let i = 0; i < this.AllRoll.length; i++) {
      if(this.AllRoll[i].rollName == event){
          this.rollNameFlag = true;
          break;
        }
     }
  }
  getAll() {
    let url = this.baseurl + '/RollMaster/';
    this.httpService.get(url + this.compId).subscribe(
      data => {
        this.AllRoll = data["result"];
      }, (err: HttpErrorResponse) => {
      });

  }


  ngValue(event) {
    this.selectedRoleobj = parseInt(event.target.value);
    this.editobject(this.selectedRoleobj);
  }
  editobject(selectedRoleobj) {
    this.RoleMOdel.rollMasterId = selectedRoleobj;
    let urledit = this.baseurl + '/RollMaster/GetById/';
    this.httpService.get(urledit + selectedRoleobj).subscribe(
      (data: any) => {
        this.RoleMOdel = data['result'];
        this.RoleMOdel.rollMasterId = selectedRoleobj; 
        this.isedit = true;
        this.disableuser = false;
      },
      (err: HttpErrorResponse) => {
         this.isedit = false;
      });

  }

  uiswitch(event) {
    // if(event.target.className == "switch switch-medium checked"){
    //   this.RoleMOdel.status = true;
    // }else{
    //    this.RoleMOdel.status = false
    // }
    this.RoleMOdel.status = !this.RoleMOdel.status;
  }

  submitInfo() {
    this.RoleMOdel.compId = this.compId;
    console.log("***********************************",this.RoleMOdel)
    this.Spinner.show()
    if (this.isedit == false) {
      this.RoleService.postRoleMasterData(this.RoleMOdel)
        .subscribe(data => {
          this.getAll();
           this.ResetInfo();
           this.selectedCompanyobj = "";
           this.Spinner.hide();
          this.toastr.success('Role Inserted Successfully!', 'Role-Master');
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Role-Master');
          });
    } else {
      
      let url1 = this.baseurl + '/RollMaster';
      this.selectedRoleobj = this.RoleMOdel.rollMasterId;
      this.Spinner.show()
      this.httpService.put(url1, this.RoleMOdel)
        .subscribe(data => {
          this.selectedCompanyobj = "";
         this.Spinner.hide();
          this.toastr.success('Role Master Information Updated Successfully!', 'Role-Master');
          this.isedit = false;
           this.ResetInfo();
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Role-Master');
        });
    }
  }

  ResetInfo(){

    this.RoleMOdel = new RoleMaster();
    this.isedit = false;
     
  }

  settingChanged(event){
    if(!event){
      this.RoleMOdel.masters = false;
    }
  }

  companyChanged(event){
    if(!event){
      // this.RoleMOdel.resignAndLeaving = false;
    }
  }

  // ======================== New Roles Logic Starts ===========================

  myUserChanged(event){
     if(!event){
      this.RoleMOdel.profile = false;
      this.RoleMOdel.history = false;
      // this.RoleMOdel.finance = false;
     }
  }

  attendanceUserChanged(event){
    if(!event){
      this.RoleMOdel.yearlyCalender = false;
      this.RoleMOdel.applyLeave = false;
      this.RoleMOdel.arRequest = false;
      this.RoleMOdel.leaveDashboard = false;
      this.RoleMOdel.attendanceReport = false;
     }
  }
  // pmsUserChanged(event){
  //   if(!event){
  //     this.RoleMOdel.pmsDashboard = false;
  //     this.RoleMOdel.goalSettingUser = false;
  //     this.RoleMOdel.selfAppraisalUser = false;
  //     this.RoleMOdel.previousAppraisalUser = false;
  //     this.RoleMOdel.pmsReports = false;
  //    }
  // }

  myTeamOtherChanged(event){
    if(!event){
      this.RoleMOdel.applyLeaveOther = false;
      this.RoleMOdel.leaveDashboardother = false;
      this.RoleMOdel.extraworkRequest = false;
      this.RoleMOdel.allReports = false;
      this.RoleMOdel.advanceLeave = false;
      this.RoleMOdel.leaveBlock = false;
      this.RoleMOdel.uploadShift = false;
      // this.RoleMOdel.selfCertificationDetails = false;
     }
     else{
      this.RoleMOdel.applyLeaveOther = true;
      this.RoleMOdel.leaveDashboardother = true;
      this.RoleMOdel.extraworkRequest = true;
      this.RoleMOdel.allReports = true;
      this.RoleMOdel.advanceLeave = true;
      this.RoleMOdel.leaveBlock = true;
      this.RoleMOdel.uploadShift = true;
      // this.RoleMOdel.selfCertificationDetails = true;
     }
  }
  myTeamOtherOptionsChanged(event){
    let flag : boolean = false;

    flag = !this.RoleMOdel.applyLeaveOther && !this.RoleMOdel.leaveDashboardother && !this.RoleMOdel.extraworkRequest && !this.RoleMOdel.allReports && !this.RoleMOdel.advanceLeave && !this.RoleMOdel.leaveBlock && !this.RoleMOdel.uploadShift;
    if(flag){
      this.RoleMOdel.myOther = false;
     }
  }
  // pmsOtherChanged(event){
  //   if(!event){
  //     this.RoleMOdel.pmsDashboardOther = false;
  //     this.RoleMOdel.goalSettingOther = false;
  //     this.RoleMOdel.goalapprovalsOther = false;
  //     this.RoleMOdel.appriasalsOther = false;
  //     this.RoleMOdel.performanceStatus = false;
  //     this.RoleMOdel.overallReports = false;
  //     this.RoleMOdel.appraisalCalendarConfig = false;
  //     this.RoleMOdel.pmsSettingsOther = false;
  //     this.RoleMOdel.matrixManager = false;
  //    }
  //    else{
  //     this.RoleMOdel.pmsDashboardOther = true;
  //     this.RoleMOdel.goalSettingOther = true;
  //     this.RoleMOdel.goalapprovalsOther = true;
  //     this.RoleMOdel.appriasalsOther = true;
  //     this.RoleMOdel.performanceStatus = true;
  //     this.RoleMOdel.overallReports = true;
  //     this.RoleMOdel.appraisalCalendarConfig = true;
  //     this.RoleMOdel.pmsSettingsOther = true;
  //     this.RoleMOdel.matrixManager = true;
  //    }
  // }
  // pmsOtherOptionsChanged(event){
  //   let flag : boolean = false;

  //   flag =  !this.RoleMOdel.pmsDashboardOther && !this.RoleMOdel.goalSettingOther && !this.RoleMOdel.goalapprovalsOther && !this.RoleMOdel.appriasalsOther && !this.RoleMOdel.performanceStatus && !this.RoleMOdel.overallReports && !this.RoleMOdel.appraisalCalendarConfig && !this.RoleMOdel.pmsSettingsOther && !this.RoleMOdel.matrixManager;
    
  //   if(flag){
  //     this.RoleMOdel.pmsOther = false;
  //    }
  // }
  settingsOtherChanged(event){
    if(!event){
      this.RoleMOdel.masters = false;
      this.RoleMOdel.leaveConfig = false;
      this.RoleMOdel.attenConfig = false;
      // this.RoleMOdel.policies = false;
      // this.RoleMOdel.queryConfig = false;
      this.RoleMOdel.roleMaster = false;
      this.RoleMOdel.customizeDashboard = false;
      // this.RoleMOdel.seperation = false;
      this.RoleMOdel.emailSetting = false;
     }
     else{
      this.RoleMOdel.masters = true;
      this.RoleMOdel.leaveConfig = true;
      this.RoleMOdel.attenConfig = true;
      // this.RoleMOdel.policies = true;
      // this.RoleMOdel.queryConfig = true;
      this.RoleMOdel.roleMaster = true;
      this.RoleMOdel.customizeDashboard = true;
      // this.RoleMOdel.seperation = true;
      this.RoleMOdel.emailSetting = true;
     }
  }
  settingsOtherOptionsChanged(event){
    let flag : boolean = false;

    flag =  !this.RoleMOdel.masters && !this.RoleMOdel.leaveConfig && !this.RoleMOdel.attenConfig  && !this.RoleMOdel.roleMaster && !this.RoleMOdel.customizeDashboard  && !this.RoleMOdel.emailSetting;

    if(flag){
      this.RoleMOdel.setting = false;
     }
  }
  workforceOtherChanged(event){
    if(!event){
      this.RoleMOdel.employee = false;
      // this.RoleMOdel.analytics = false;
      // this.RoleMOdel.employeeVerification = false;
      // this.RoleMOdel.exitEmployeedetail = false;
      this.RoleMOdel.allReports = false;
      this.RoleMOdel.bulkUpload = false;
      // this.RoleMOdel.clearanceDetail = false;
      // this.RoleMOdel.selfCertificationDetails = false;
     }
     else{
      this.RoleMOdel.employee = true;
      // this.RoleMOdel.analytics = true;
      // this.RoleMOdel.employeeVerification = true;
      // this.RoleMOdel.exitEmployeedetail = true;
      this.RoleMOdel.allReports = true;
      this.RoleMOdel.bulkUpload = true;
      // this.RoleMOdel.clearanceDetail = true;
      // this.RoleMOdel.selfCertificationDetails = true;
     }
  }
  workforceOtherOptionsChanged(event){
     let flag : boolean = false;

     flag = !this.RoleMOdel.employee  && !this.RoleMOdel.allReports && !this.RoleMOdel.bulkUpload;
     
      if(flag){
      this.RoleMOdel.workforce = false;
     }
  }
  // ======================== New Roles Logic Ends ===========================



  dltmodelEvent(){
    this.dltmodelFlag=true;
    this.deleteRollMaster(this.dltObje);
     //this.Spinner.show();
  }
  
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje = obj.adminRoleWiseEmpId;
    this.deleteRollMaster(this.dltObje);
  }
  deleteRollMaster(object){
    if (this.dltmodelFlag == true) {
      //this.QyerModel.empOfficialId=this.dltObje
      this.QyerModel.adminRoleWiseEmpId=this.dltObje;
        let url = this.baseurl + '/deleteAdminRoleWiseEmployee/';
        this.httpService.delete(url + this.dltObje).subscribe(data => {
            this.toastr.success('Role master deleted successfully');
            this.getQueryAdminList(this.SelectedRole);
          },(err: HttpErrorResponse) => {
            // this.Spinner.hide();
            this.toastr.error('References is present so you can not delete..!');
         });
        }
      }
      //setInductionEmployment
      setInductionEmployment(){
        this.EmpCat = [
          {
            "employmentType": "On Roll",
            "empTypeId":"001"
          },
          {
            "employmentType": "On Contract",
            "empTypeId":"425"
          }
        ];
      }
      //PostInduction
      PostInduction(){
        if(this.QyerModelSecond.queryAdminWiseEmployementTypeList.length==0){
          this.toastr.error('Please select Employment type !');
          return false;
        }
        if(this.QyerModelSecond.locationMasterList.length==0){
          this.toastr.error('Please select location !');
          return false;
        }
        if(this.selectdepartment){
          this.QyerModelSecond.deptId = this.selectdepartment;
        }else{
          this.toastr.error('Please select Department !');
          return false;
        }
        if(this.QyerModelSecond.empOfficialDataMasterList.length==0){
          this.toastr.error('Please select Employees !');
          return false;
        }
        
       
        this.QyerModelSecond.queryAdminRoleId = this.SelectedRole;
        this.QueryService.PostQueryAdminInd(this.QyerModelSecond).subscribe(data => {
          if(data.statusCode==201){
            this.toastr.success('Role Added successfully');
            this.router.navigateByUrl('/layout/settingpage');
          }
          if(data.result.msg){
            this.toastr.error(data.result.msg);
          }
        })
      }
      //update induction
      updateInduction(){
        if(this.QyerModelSecond.queryAdminWiseEmployementTypeList.length==0){
          this.toastr.error('Please select Employment type !');
          return false;
        }
        if(this.QyerModelSecond.locationMasterList.length==0){
          this.toastr.error('Please select location !');
          return false;
        }
        if(this.selectdepartment){
          this.QyerModelSecond.deptId = this.selectdepartment;
        }else{
          this.toastr.error('Please select Department ! ');
          return false;
        }
        if(this.QyerModelSecond.empOfficialDataMasterList.length==0){
          this.toastr.error('Please select Employees ! ');
          return false;
        }
        this.QyerModelSecond.queryAdminRoleId = this.SelectedRole;
        this.QueryService.updateQueryInduction(this.QyerModelSecond).subscribe(data => {
          if(data.statusCode==201){
            this.toastr.success('Role Updated successfully');
            this.router.navigateByUrl('/layout/settingpage');
          }
          if(data.result.msg){
            this.toastr.error(data.result.msg);
          }
        })
      }
      //Reset All
      ResetAllTabs(){
        this.QyerModelSecond.departmentMasterList = [];
        this.QyerModelSecond.designationMasterList = [];
        this.QyerModelSecond.employeeTypeMasterList = [];
        this.QyerModelSecond.empOfficialDataMasterList = [];
        this.QyerModelSecond.gradeMasterList = [];
        this.QyerModelSecond.locationMasterList = [];
        this.QyerModelSecond.payGroupMasterList = [];
        this.QyerModelSecond.queryAdminWiseEmployementTypeList=[];
        this.QueryAdminlist = [];
        this.selectdepartment = 0;
        this.getobj = '';
      }

      


}