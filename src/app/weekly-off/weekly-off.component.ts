import { Component, OnInit } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn, NgForm } from '@angular/forms';
import { XlsxToJsonService } from '../xlsx-to-json-service';
import { ExcelServiceService } from '../shared/services/excel-service.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { excelimport } from '../shared/model/excelimport.model';
import { MainURL } from '../shared/configurl';
// import { ImportShiftServiceService, ExcelServiceShiftUpload } from './service/import-shift-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelServiceShiftUpload } from '../shiftupload/service/import-shift-service.service';
import {dataService} from '../shared/services/dataService';

declare var require: any;
@Component({
  selector: 'app-weekly-off',
  templateUrl: './weekly-off.component.html',
  styleUrls: ['./weekly-off.component.css']
})
export class WeeklyOffComponent implements OnInit {

  //   constructor() { }

  //   ngOnInit() {
  //   }

  // }

  form: FormGroup = new FormGroup({});
  baseurl = MainURL.HostUrl
  compId: any;

  orders = [
    { title: "Personal" },
    { id: "biometricId", name: "Biometric Id" }

  ];
  excelmodel: excelimport;
  model: any = {};
  finalArray = [];
  fileExtension: any;
  showFileName: any;
  // abbrivationsResDtoList: any;
  loggedUser: any;
  constructor(private formBuilder: FormBuilder, public Spinner: NgxSpinnerService, private excelService: ExcelServiceService,
    public shiftService1: ExcelServiceShiftUpload, private toastr: ToastrService, public httpService: HttpClient, private dataService: dataService) {

    const controls = this.orders.map(c => new FormControl(false));
    this.form = this.formBuilder.group({
      orders: new FormArray(controls, minSelectedCheckboxes(1))
    });
    this.excelmodel = new excelimport();
    this.model.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + 1;
    this.model.toDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();


    this.shoButtonflag = false;
    this.hidebutton = false;
    this.hideShiftTab = true;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
  }
  dateArr: any;
  dateArr1: any = [];
  ngOnInit() {
  }

  selectedFile: File;
  shitDataList: any = [];
  fileName: any;
  arrayList1: any = [];
  fromDate: any;
  toDate: any;
  shoButtonflag: boolean = false;
  hidebutton: boolean = false;
  onFileChanged(event) {
    this.showFileName = event.target.value.replace(/^.*[\\\/]/, '');
    this.fileExtension = event.target.value.replace(/^.*\./, '');
    if (this.fileExtension == 'pdf') {
      this.hidebutton = true;
    }
    else {
      this.hidebutton = false;
    }
    this.selectedFile = event.target.files[0];
    this.fileName = this.selectedFile.name;
    this.shoButtonflag = true
    var fields = this.fileName.split('_');
    this.fromDate = fields[0];
    var demoToDate = fields[1];
    var fields1 = demoToDate.split('.');
    this.toDate = fields1[0];
  }
  list: any = [];
  srNo: any;
  row1: any;
  onChnageFun() {
    this.list = this.form.value.orders
      .map((v, i) => v ? this.orders[i] : null)
      .filter(v => v !== null);
  }

  // getAbbrivations() {
  //   // this.compId=1;
  //     let url = this.baseurl + '/abbrivations/';
  //     this.httpService.get(url + this.compId).subscribe(data => {
  //       this.abbrivationsResDtoList = data["result"];
  //     },
  //     (err: HttpErrorResponse) => {
  //     });
  // };

  alldatademployeelist() {
    // this.compId = 1;
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
      this.shitDataList = data["result"];
    },
      (err: HttpErrorResponse) => {
      });
  }
  maxDate: any;
  mindDate: any;
  startDateSelect(e) {
    this.model.fromDate = e;
    let date = new Date(e).getTime() + 30 * 24 * 60 * 60 * 1000;
    this.maxDate = new Date(new Date(e).getFullYear(), new Date(e).getMonth() + 1, 0);
    let date1 = new Date(new Date(e).getFullYear(), new Date(e).getMonth() + 1, 0);
    this.model.toDate = new Date(date1).getFullYear() + "-0" + (new Date(date1).getMonth() + 1) + "-" + new Date(date1).getDate();
  }
  endDateSelect(e) {
    this.model.toDate = e;
  }
  daysData = [];
  hideShiftTab: boolean;
  submit() {
    this.Spinner.show();
    this.hideShiftTab = false;
    this.dateArr = {};
    this.dateArr1 = [];
    //     // this.getAbbrivations();
    var fromDate = new Date(this.model.fromDate);
    var y = fromDate.getFullYear();
    var m = fromDate.getMonth();
    var startDateOfMonth = new Date(y, m, 1);
    var lastDateOfMonth = new Date(y, m + 1, 0);
    let AttendenceConfiList = '/getWoUploadData/';
    let obj = {
      "empId": this.loggedUser.empId,
      "locationId":this.loggedUser.locationId,
      "roleId": this.loggedUser.rollMasterId,
      "compId": this.compId,
      "fromDate": this.model.fromDate,
      "toDate": this.model.toDate
    }
    this.dataService.createRecord(AttendenceConfiList , obj)
    //this.httpService.post(AttendenceConfiList, obj).
    .subscribe((data: any) => {
      this.shitDataList = data.result.dataList;
      this.daysData = data.result.headerList;
      let tempData1 = new Array();
      let tempObj1: any = {};
      for (let i = 0; i < this.shitDataList.length; i++) {
        tempObj1.srNo = i + 1;
        tempObj1.empId = this.shitDataList[i].empId;
        tempObj1.empName = this.shitDataList[i].fullName + " " + this.shitDataList[i].lastName;
        if (new Date(this.model.fromDate).getDate() <= 1 && new Date(this.model.toDate).getDate() >= 1) {
          tempObj1.one = this.shitDataList[i].one;
        }
        else {
          tempObj1.one = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 2 && new Date(this.model.toDate).getDate() >= 2) {
          tempObj1.two = this.shitDataList[i].two;
        }
        else {
          tempObj1.two = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 3 && new Date(this.model.toDate).getDate() >= 3) {
          tempObj1.three = this.shitDataList[i].three;
        }
        else {
          tempObj1.three = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 4 && new Date(this.model.toDate).getDate() >= 4) {
          tempObj1.four = this.shitDataList[i].four;
        }
        else {
          tempObj1.four = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 5 && new Date(this.model.toDate).getDate() >= 5) {
          tempObj1.five = this.shitDataList[i].five;
        }
        else {
          tempObj1.five = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 6 && new Date(this.model.toDate).getDate() >= 6) {
          tempObj1.six = this.shitDataList[i].six;
        }
        else {
          tempObj1.six = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 7 && new Date(this.model.toDate).getDate() >= 7) {
          tempObj1.seven = this.shitDataList[i].seven;
        }
        else {
          tempObj1.seven = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 8 && new Date(this.model.toDate).getDate() >= 8) {
          tempObj1.eight = this.shitDataList[i].eight;
        }
        else {
          tempObj1.eight = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 9 && new Date(this.model.toDate).getDate() >= 9) {
          tempObj1.nine = this.shitDataList[i].nine;
        }
        else {
          tempObj1.nine = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 10 && new Date(this.model.toDate).getDate() >= 10) {
          tempObj1.ten = this.shitDataList[i].ten;
        }
        else {
          tempObj1.ten = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 11 && new Date(this.model.toDate).getDate() >= 11) {
          tempObj1.eleven = this.shitDataList[i].eleven;
        }
        else {
          tempObj1.eleven = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 12 && new Date(this.model.toDate).getDate() >= 12) {
          tempObj1.twelve = this.shitDataList[i].twelve;
        }
        else {
          tempObj1.twelve = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 13 && new Date(this.model.toDate).getDate() >= 13) {
          tempObj1.thirteen = this.shitDataList[i].thirteen;
        }
        else {
          tempObj1.thirteen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 14 && new Date(this.model.toDate).getDate() >= 14) {
          tempObj1.fourteen = this.shitDataList[i].fourteen;
        }
        else {
          tempObj1.fourteen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 15 && new Date(this.model.toDate).getDate() >= 15) {
          tempObj1.fifteen = this.shitDataList[i].fifteen;
        }
        else {
          tempObj1.fifteen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 16 && new Date(this.model.toDate).getDate() >= 16) {
          tempObj1.sixteen = this.shitDataList[i].sixteen;
        }
        else {
          tempObj1.sixteen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 17 && new Date(this.model.toDate).getDate() >= 17) {
          tempObj1.seventeen = this.shitDataList[i].seventeen;
        }
        else {
          tempObj1.seventeen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 18 && new Date(this.model.toDate).getDate() >= 18) {
          tempObj1.eighteen = this.shitDataList[i].eighteen;
        }
        else {
          tempObj1.eighteen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 19 && new Date(this.model.toDate).getDate() >= 19) {
          tempObj1.nineteen = this.shitDataList[i].nineteen;
        }
        else {
          tempObj1.nineteen = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 20 && new Date(this.model.toDate).getDate() >= 20) {
          tempObj1.twenty = this.shitDataList[i].twenty;
        }
        else {
          tempObj1.twenty = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 21 && new Date(this.model.toDate).getDate() >= 21) {
          tempObj1.twentyone = this.shitDataList[i].twentyone;
        }
        else {
          tempObj1.twentyone = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 22 && new Date(this.model.toDate).getDate() >= 22) {
          tempObj1.twentytwo = this.shitDataList[i].twentytwo;
        }
        else {
          tempObj1.twentytwo = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 23 && new Date(this.model.toDate).getDate() >= 23) {
          tempObj1.twentythree = this.shitDataList[i].twentythree;
        }
        else {
          tempObj1.twentythree = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 24 && new Date(this.model.toDate).getDate() >= 24) {
          tempObj1.twentyfour = this.shitDataList[i].twentyfour;
        }
        else {
          tempObj1.twentyfour = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 25 && new Date(this.model.toDate).getDate() >= 25) {
          tempObj1.twentyfive = this.shitDataList[i].twentyfive;
        }
        else {
          tempObj1.twentyfive = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 26 && new Date(this.model.toDate).getDate() >= 26) {
          tempObj1.twentysix = this.shitDataList[i].twentysix;
        }
        else {
          tempObj1.twentysix = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 27 && new Date(this.model.toDate).getDate() >= 27) {
          tempObj1.twentyseven = this.shitDataList[i].twentyseven;
        }
        else {
          tempObj1.twentyseven = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 28 && new Date(this.model.toDate).getDate() >= 28) {
          tempObj1.twentyeight = this.shitDataList[i].twentyeight;
        }
        else {
          tempObj1.twentyeight = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 29 && new Date(this.model.toDate).getDate() >= 29) {
          tempObj1.twentynine = this.shitDataList[i].twentynine;
        }
        else {
          tempObj1.twentynine = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 30 && new Date(this.model.toDate).getDate() >= 30) {
          tempObj1.thirty = this.shitDataList[i].thirty;
        }
        else {
          tempObj1.thirty = {}
        }
        if (new Date(this.model.fromDate).getDate() <= 31 && new Date(this.model.toDate).getDate() >= 31) {
          tempObj1.thirtyone = this.shitDataList[i].thirtyone;
        }
        else {
          tempObj1.thirtyone = {}
        }
        tempData1.push(tempObj1);
        tempObj1 = {};
      }
      if (this.shitDataList.length != 0) {
        this.shiftService1.exportAsExcelFile(tempData1, this.model.fromDate + "_" + this.model.toDate + "_" + "WeeklyOff Upload" + ".xlsx");
      }
      this.hideShiftTab = true;
      this.Spinner.hide();
    },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
      });
    const numWords = require('num-words');
    this.dateArr = this.getDateArray(new Date(this.model.fromDate), new Date(this.model.toDate));
    this.dateArr1.push("srNo");
    this.dateArr1.push("empId");
    this.dateArr1.push("empName");
    for (let index = 0; index < this.dateArr.length; index++) {
      const element = new Date(this.dateArr[index]);
      this.dateArr1.push(numWords(element.getDate()).replace(/ /g, "").toLowerCase());
    }
    const selectedOrderIds = this.dateArr1;
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('data');
    // Add Header Row
    let headerRow = worksheet.addRow(selectedOrderIds);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });
  }
  // var fromDate = new Date(this.model.fromDate);
  //      var y = fromDate.getFullYear();
  //      var m = fromDate.getMonth();
  // var startDateOfMonth= new Date(y, m, 1);
  // var lastDateOfMonth= new Date(y, m + 1, 0);
  getDaysArray(start, end) {
    for (var arr = [], dt = start; dt <= end; dt.setDate(dt.getDate() + 1)) {
      arr.push(new Date(dt));
    }
    return arr;
  };
  public result: any;
  // shiftUploadReqDtoList="";
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  shiftErrorMessage: any;
  handleFile() {
    this.Spinner.show();
    let file = this.selectedFile;
    this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
      this.excelmodel.shiftUploadReqDtoList = (data['sheets'].data);
      let tempData = this.excelmodel.shiftUploadReqDtoList;
      var toDate = new Date(this.toDate);
      var y = toDate.getFullYear();
      //  var d = toDate.getDate();
      var m = toDate.getMonth();
      var startDateOfMonth = new Date(y, m, 1);
      var d = new Date(y, m + 1, 0).getDate();
      console.log(d);

      for (let i = 0; i < tempData.length; i++) {
        if (tempData[i].one != "WO") {
          tempData[i].one = "";
        }
        if (tempData[i].two != "WO") {
          tempData[i].two = "";
        }
        if (tempData[i].three != "WO") {
          tempData[i].three = "";
        }
        if (tempData[i].four != "WO") {
          tempData[i].four = "";
        }
        if (tempData[i].five != "WO") {
          tempData[i].five = "";
        }
        if (tempData[i].six != "WO") {
          tempData[i].six = "";
        }
        if (tempData[i].seven != "WO") {
          tempData[i].seven = "";
        }
        if (tempData[i].eight != "WO") {
          tempData[i].eight = "";
        }
        if (tempData[i].nine != "WO") {
          tempData[i].nine = "";
        }
        if (tempData[i].ten != "WO") {
          tempData[i].ten = "";
        }
        if (tempData[i].eleven != "WO") {
          tempData[i].eleven = "";
        }
        if (tempData[i].twelve != "WO") {
          tempData[i].twelve = "";
        }
        if (tempData[i].thirteen != "WO") {
          tempData[i].thirteen = "";
        }
        if (tempData[i].fourteen != "WO") {
          tempData[i].fourteen = "";
        }
        if (tempData[i].fifteen != "WO") {
          tempData[i].fifteen = "";
        }
        if (tempData[i].sixteen != "WO") {
          tempData[i].sixteen = "";
        }
        if (tempData[i].seventeen != "WO") {
          tempData[i].seventeen = "";
        }
        if (tempData[i].eighteen != "WO") {
          tempData[i].eighteen = "";
        }
        if (tempData[i].nineteen != "WO") {
          tempData[i].nineteen = "";
        }
        if (tempData[i].twenty != "WO") {
          tempData[i].twenty = "";
        }
        if (tempData[i].twentyone != "WO") {
          tempData[i].twentyone = "";
        }
        if (tempData[i].twentytwo != "WO") {
          tempData[i].twentytwo = "";
        }
        if (tempData[i].twentythree != "WO") {
          tempData[i].twentythree = "";
        }
        if (tempData[i].twentyfour != "WO") {
          tempData[i].twentyfour = "";
        }
        if (tempData[i].twentyfive != "WO") {
          tempData[i].twentyfive = "";
        }
        if (tempData[i].twentysix != "WO") {
          tempData[i].twentysix = "";
        }
        if (tempData[i].twentyseven != "WO") {
          tempData[i].twentyseven = "";
        }
        if (tempData[i].twentyeight != "WO") {
          tempData[i].twentyeight = "";
        }
        if (d > 28 && tempData[i].twentynine != "WO") {
          tempData[i].twentynine = "";
        }
        if (d > 29 && tempData[i].thirty != "WO") {
          tempData[i].thirty = "";
        }
        if (d > 30 && tempData[i].thirtyone != "WO") {
          tempData[i].thirtyone = "";
        }
      }
      this.excelmodel.fromDate = this.fromDate;
      this.excelmodel.toDate = this.toDate;
      this.excelmodel.compId = this.compId;
      if (this.excelmodel.shiftUploadReqDtoList != null) {
        this.excelService.postWeeklyOffExcel(this.excelmodel).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Weeklyoff Upload successfully');
          $("#File1").val('');
          this.shoButtonflag = false;
        }, err => {
          this.Spinner.hide();
          this.shiftErrorMessage = err.json().result.shiftValidateMessage;
          this.toastr.error(this.shiftErrorMessage);
          this.shoButtonflag = false;
        })
      }
      else {
        this.Spinner.hide();
        this.toastr.error('Weeklyoff Upload Failed');
        this.shoButtonflag = false;
      }
    })
  }
  list1 = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
  changeStatus(obj) {
    if (this.list1[obj]) {
      this.list1[obj] = false;
    }
    else {
      this.list1[obj] = true;
    }
  }
  //  var fromDate = new Date(this.model.fromDate);
  //      var y = fromDate.getFullYear();
  //      var m = fromDate.getMonth();
  // var startDateOfMonth= new Date(y, m, 1);
  // var lastDateOfMonth= new Date(y, m + 1, 0);
  getDateArray = function (start, end) {
    var arr = new Array();
    var dt = new Date(start);
    while (dt <= end) {
      arr.push(new Date(dt));
      dt.setDate(dt.getDate() + 1);
    }
    return arr;
  }
  hasProp(o, name) {
    return o.hasOwnProperty(name);
  }
}
function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}

