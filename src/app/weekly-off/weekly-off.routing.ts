import { Routes, RouterModule } from '@angular/router'
import { WeeklyOffComponent } from 'src/app/weekly-off/weekly-off.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: WeeklyOffComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'weekly-off',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'weekly-off',
                    component: WeeklyOffComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);