import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExcelServiceShiftUpload } from '../shiftupload/service/import-shift-service.service';
import { WeeklyOffComponent } from 'src/app/weekly-off/weekly-off.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { routing } from './weekly-off.routing';
@NgModule({
  declarations: [
    WeeklyOffComponent
],
  imports: [
    CommonModule,
  routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     
  ],
 
  providers: [ExcelServiceShiftUpload]
})
export class WeeklyOffModule {
  constructor(){

  }
 }
