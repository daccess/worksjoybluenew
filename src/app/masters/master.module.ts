import { routing } from '../masters/master.routing';
import { NgModule } from '@angular/core';
import { CompanymasterComponent } from './pages/companymaster/companymaster.component';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UiSwitchModule } from 'ngx-ui-switch';
import {MastersComponent} from 'src/app/masters/masters.component';
import {CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { BusinessMasterComponent } from './pages/business-master/business-master.component';
import { FunctionMasterComponent } from './pages/function-master/function-master.component';
import { DepartmentMasterComponent } from './pages/department-master/department-master.component';
import { SubDepartmentMasterComponent } from './pages/sub-department-master/sub-department-master.component';
import { EmployeeTypeMasterComponent } from './pages/employee-type-master/employee-type-master.component';
import { SubEmployeeTypeMasterComponent } from './pages/sub-employee-type-master/sub-employee-type-master.component';
import { PayGroupMasterComponent } from './pages/pay-group-master/pay-group-master.component';
import { GradeMasterComponent } from './pages/grade-master/grade-master.component';
import { DesignationMasterComponent } from './pages/designation-master/designation-master.component';
import { CostCenterMasterComponent } from './pages/cost-center-master/cost-center-master.component';

import { CompanyMasterService } from '../../app/shared/services/companymasterservice'
import { BusinessMasterService } from '../shared/services/businessgroupmasterservice';
import { FunctionMasterService } from '../shared/services/functionmasterservice';
import { subdepartmentMasterService } from '../shared/services/subdepartmentmasterservice';
import { departmentMasterService } from '../shared/services/departmentmasterservice';
import { EmployeeTypeMasterService } from '../shared/services/employee-type-master.service';
import { PayGroupMasterService } from '../shared/services/PayGroupMasterService';
import { SubEmpTypMasterService } from '../shared/services/SubEmpTypService';
import { GradeMasterService } from '../shared/services/GradeMasterService';
import { DesignationMasterService } from '../shared/services/DesignationMasterService';
import { CostCenterMasterService } from '../shared/services/CostCenterMasterService';
import { LocationmasterComponent } from './pages/locationmaster/locationmaster.component';
import { LocationMasterService } from '../shared/services/locationMasterService';

@NgModule({
    declarations: [
     CompanymasterComponent,
     MastersComponent,
     BusinessMasterComponent,
     FunctionMasterComponent,
     DepartmentMasterComponent,
     SubDepartmentMasterComponent,
     EmployeeTypeMasterComponent,
     SubEmployeeTypeMasterComponent,
     PayGroupMasterComponent,
     GradeMasterComponent,
     DesignationMasterComponent,
     CostCenterMasterComponent,
     LocationmasterComponent
    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
    DataTablesModule
    ],
    providers: [CompanyMasterService, 
        BusinessMasterService,
        FunctionMasterService,
        subdepartmentMasterService,
        departmentMasterService,
        EmployeeTypeMasterService,
        PayGroupMasterService,
        SubEmpTypMasterService,
        GradeMasterService,
        DesignationMasterService,
        CostCenterMasterService,
        LocationMasterService]
  })
  export class MasterModule { 
      constructor(){

      }
  }
  