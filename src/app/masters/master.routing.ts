import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { MastersComponent } from 'src/app/masters/masters.component';
import { CompanymasterComponent } from 'src/app/masters/pages/companymaster/companymaster.component';
import { BusinessMasterComponent } from 'src/app/masters/pages/business-master/business-master.component';
import { FunctionMasterComponent } from 'src/app/masters/pages/function-master/function-master.component';
import { DepartmentMasterComponent } from 'src/app/masters/pages/department-master/department-master.component';
import { SubDepartmentMasterComponent } from 'src/app/masters/pages/sub-department-master/sub-department-master.component';
import { EmployeeTypeMasterComponent } from 'src/app/masters/pages/employee-type-master/employee-type-master.component';
import { SubEmployeeTypeMasterComponent } from 'src/app/masters/pages/sub-employee-type-master/sub-employee-type-master.component';
import { PayGroupMasterComponent } from 'src/app/masters/pages/pay-group-master/pay-group-master.component';
import { GradeMasterComponent } from 'src/app/masters/pages/grade-master/grade-master.component';
import { DesignationMasterComponent } from 'src/app/masters/pages/designation-master/designation-master.component';
import { CostCenterMasterComponent } from 'src/app/masters/pages/cost-center-master/cost-center-master.component';
import { LocationmasterComponent } from './pages/locationmaster/locationmaster.component';

const appRoutes: Routes = [
    { 
        path: '', component: MastersComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'companymaster',
                    pathMatch :'full'
                    
                },
                {
                    path:'companymaster',
                    component: CompanymasterComponent
                },
                {
                    path:'businessmaster',
                    component: BusinessMasterComponent
                },
                {
                    path:'functionmaster',
                    component: FunctionMasterComponent
                },
                {
                    path:'departmentmaster',
                    component: DepartmentMasterComponent
                },
                {
                    path:'subdepartmentmaster',
                    component: SubDepartmentMasterComponent
                },
                {
                    path:'emptypemaster',
                    component: EmployeeTypeMasterComponent
                },
                {
                    path:'subemptypemaster',
                    component: SubEmployeeTypeMasterComponent
                },
                {
                    path:'paygroupmaster',
                    component: PayGroupMasterComponent
                },
                {
                    path:'grademaster',
                    component: GradeMasterComponent
                },
                {
                    path:'designationmaster',
                    component: DesignationMasterComponent
                },
                {
                    path:'costcentermaster',
                    component: CostCenterMasterComponent
                },
                {
                    path:'locationmaster',
                    component: LocationmasterComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);