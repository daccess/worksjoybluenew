import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { BusinessMasterService } from '../../../shared/services/businessgroupmasterservice';
import { businessmaster } from '../../../shared/model/businessgroupmastermodel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SessionService } from 'src/app/shared/services/SessionSrvice';
import { NgxSpinnerService } from 'ngx-spinner';

class addcompanym {
  compId : number
};

@Component({
  selector: 'app-business-master',
  templateUrl: './business-master.component.html',
  styleUrls: ['./business-master.component.css'],
  providers: [BusinessMasterService]
})
export class BusinessMasterComponent implements OnInit {

  //model: any = {};
  // data;
  addcompany: addcompanym;
  
  businessmodel: businessmaster;
  selectedCompanyobj: any
  companydata: any;
  public selectUndefinedOptionValue:any;
  baseurl = MainURL.HostUrl;
  compId: any;
  // compdata = [];
  businessgroup: any;
  isedit = false;
  Selectededitobj: any;
  selectedComp:Number;
  updatecompany: any;
  flag  : boolean = false;
  LoginUser:any=[];
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  getcmp: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  businame: string;
  loginModel: {};
  constructor(public httpService: HttpClient, 
    public businessservice: BusinessMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public SessionSer : SessionService,
    public Spinner :NgxSpinnerService) { 

    this.addcompany = new addcompanym()
    this.businessmodel = new businessmaster();

    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
    this.getactivecompany()
  }

  focusFunction(){
    this.flag = true;
  }
  focusOutFunction(){

  }
  input(){
    this.flag = false;
  }
  ngOnInit() {
    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
        this.companydata = data["result"];
      },(err: HttpErrorResponse) => {
      });
  }

  error(){ 
    if (this.businame != this.businessmodel.busiGrupName){
    let obj={
      compId: this.selectedCompanyobj ,
      busiGrupName : this.businessmodel.busiGrupName
    }
    let url = this.baseurl + '/busiMaster';
    this.httpService.post(url,obj).subscribe((data :any) => {
       this.errordata= data["result"];
       this.msg= data.message
       this.toastr.error(this.msg);
       this.errflag = true
      },
      (err: HttpErrorResponse) => {
        this.errflag = false
      });
  }
}
  StatusFlag:boolean=false
  
  onSubmit(f) {
    this.Spinner.show();
    this.StatusFlag=false
    this.businessmodel.status=this.flag1;
    if(this.isedit == false){
    this.businessmodel.compId = this.selectedCompanyobj;
    this.businessservice.postBusinessMaster(this.businessmodel).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.toastr.success('Business-Master Information Inserted Successfully!', 'Business-Master');
      this.getactivecompany()
    },(err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Business-Master');
    });
  }
  else{
    this.errflag = false
    let url1 = this.baseurl + '/BusinessGroup';
    this.businessmodel.compId = this.selectedCompanyobj;
    this.businessmodel.busiGroupId = this.Selectededitobj;
    this.httpService.put(url1,this.businessmodel).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.toastr.success('Business-Master Information Updated Successfully!', 'Business-Master');
      this.isedit=false
      this.getactivecompany()
    },(err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.isedit=false;
      this.toastr.error('Server Side Error..!', 'Business-Master');
    });
  }
  }

  ngValue(event){
    this.selectedCompanyobj = parseInt(event.target.value);
  }

  uiswitch(event){ 
    this.flag1 = !this.flag1;
  }
  dataTableFlag : boolean = true;
  getactivecompany(){
    let url1 = this.baseurl + '/BusiGroup/';
    this.dataTableFlag = false;
    this.httpService.get(url1 + this.compId).subscribe(data => {
       this.businessgroup = data["result"];
       this.dataTableFlag = true;
       setTimeout(function () {
        
         $(function () {
           var table = $('#BusinessTable').DataTable();
               
         });
       }, 1000);
    },
    (err: HttpErrorResponse) => {
    });
  }
 
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.busiGroupId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/business/';
        this.businessmodel.busiGroupId = this.dltObje;
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully', 'Business-Master');
          this.getactivecompany();
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
    }
  }
  getvalue(event){
    
  }
  edit(object){
    this.Selectededitobj = object.busiGroupId;
    this.isedit = true;
    this.StatusFlag=true;
    this.editobject(this.Selectededitobj);
    this.flag1 = object.status;
  }
  flag1 : boolean = false;
  editobject(getobject){
    let urledit = this.baseurl + '/BusinessGroup/';
    this.httpService.get(urledit + getobject).subscribe(data => {
        this.businessmodel.status = data['result']['status'];
        this.businessmodel.busiGrupName = data['result']['busiGrupName'];
        this.businame = this.businessmodel.busiGrupName
        this.businessmodel.busiGroupDesc = data['result']['busiGroupDesc'];
        var comp_id = data['result']['companyMaster']['compId'];
        this.error();
        for(var i=0;i<this.companydata.length;i++){
           if(this.companydata[i].compId == comp_id){
             this.selectedCompanyobj =  comp_id;
           }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}