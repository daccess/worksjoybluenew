import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { EmployeeTypeMasterService } from '../../../shared/services/employee-type-master.service'
import { EmpTypMaster } from '../../../shared/model/EmplyoeeTypeMaster'
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-employee-type-master',
  templateUrl: './employee-type-master.component.html',
  styleUrls: ['./employee-type-master.component.css'],
  providers: [EmployeeTypeMasterService]
})
export class EmployeeTypeMasterComponent implements OnInit {

  EmpTYPModel: EmpTypMaster;
  selectedCompanyobj: any
  companydata: any;
  EmpTypeData: any;
  StatusFlag:boolean=false
  isedit = false;
  baseurl = MainURL.HostUrl;
  compId: any;
  geteditid: any;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errorflag: boolean;
  errflag: boolean;
  public selectUndefinedOptionValue:any;
  // user: String;
  user: string;
  users: any;
  token: any;
  constructor(public httpSer: HttpClient, 
    public EmpTypMasterSer: EmployeeTypeMasterService,
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public Spinner :NgxSpinnerService) {
  
    this.EmpTYPModel = new EmpTypMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.compId =  this.loggedUser.compId;
  }

  // error(){
  //   if(this.isedit == false){
  //   let obj={
  //     compId : this.selectedCompanyobj,
  //     empCatName : this.EmpTYPModel.empCatName
  //   }
  
  //     // The employee category name is unique, proceed with the API call
  //     let obj = {
  //       compId: this.selectedCompanyobj,
  //       empCatName: this.EmpTYPModel.empCatName
  //     };
      
  //     let url = this.baseurl + '/employeeCategory';
  //     this.httpSer.post(url,obj).subscribe((data :any) => {
  //        this.errordata= data["result"];
  //        this.msg= data.message
  //        this.toastr.error(this.msg);
  //        this.errorflag=true
  //       },
  //       (err: HttpErrorResponse) => {
  //         this.errorflag=false
  //       });
  //   }
  // }





  error() {
    if (this.isedit == false) {
      // if (this.isEmployeeCategoryNameUnique(this.EmpTYPModel.empCatName)) {
        let obj = {
          compId: this.selectedCompanyobj,
          empCatName: this.EmpTYPModel.empCatName,
        };
        let url = this.baseurl + '/employeeCategory';
        this.httpSer.post(url, obj).subscribe(
          (data: any) => {
            this.errordata = data['result'];
            console.log('console.log', data);
            this.msg = data.message;
            this.toastr.error(this.msg);
            this.errorflag = true;
          },
          (err: HttpErrorResponse) => {
            this.errorflag = false;
          }
        );
      } else {
        this.toastr.error('Employee Category Name already exists');
      }
    }
  
  

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.getemployee();
    this.getCompany()
  }
getCompany(){

  let url = this.baseurl + '/getAllCompany';
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url,{headers}).subscribe(data => {
      this.companydata = data["result"];
      console.log("this is company data", this.companydata)
    },
    (err: HttpErrorResponse) => {
    });
  }

//   isEmployeeCategoryNameUnique(empCatName: string): boolean {
//     if (this.EmpTypeData) { // Check if this.EmpTypeData is defined
//       if (this.isedit) {
//         // If in edit mode, allow the same name for the current category being edited
//         return this.EmpTypeData.every(
//           (item) => item.empCatName !== empCatName || item.empCatId === this.geteditid
//         );
//       } else {
//         return this.EmpTypeData.every((item) => item.empCatName !== empCatName);
//       }
//     } else {
//       return false; // or handle the case when this.EmpTypeData is undefined
//     }
// }

  
  
  

  onSubmit(f) {
    this.Spinner.show();
    this.EmpTYPModel.status=this.flag1;
    this.StatusFlag=false
     if (this.isedit == false) {
      
      let url = this.baseurl + '/createEmpCategory';
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.EmpTYPModel.compId = this.selectedCompanyobj;
      this.httpSer.post(url,this.EmpTYPModel,{headers}).subscribe(data => {
        this.Spinner.hide();
        f.reset();
        this.formReset()
        this.toastr.success('Employee-Type-Master Information Inserted Successfully!', 'Employee-Type-Master');
        this.getemployee();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
        this.toastr.error('Server Side Error..!', 'Employee-Type-Master');
      });
    }
    else{
      this.Spinner.show();
      this.errorflag=false;
      let url1 = this.baseurl + '/updateEmpCategory';
      const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.EmpTYPModel.empCatId = this.geteditid;
      this.EmpTYPModel.compId = this.selectedCompanyobj;
      this.httpSer.put(url1,this.EmpTYPModel,{headers}).subscribe(data => {
        this.Spinner.hide();
        f.reset();
        this.formReset();
        this.toastr.success('Employee-Type-Master Information Updated Successfully!', 'Employee-Type-Master');
        this.getemployee();
        this.isedit=false;
      },
      (err: HttpErrorResponse) => {
        this.toastr.error('Server Side Error..!', 'Employee-Type-Master');
        this.isedit=false
      });
    }
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
    this.formReset();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.empCatId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deleteEmpCategoryById/';
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpSer.delete(url1 + this.dltObje,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully', );
          this.chRef.detectChanges();
          this.getemployee();
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
      }
    }
  }
  dataTableFlag : boolean = true;
  getemployee(){
    let url1 = this.baseurl + '/getAllEmpCategories';
    this.dataTableFlag = false
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url1,{headers}).subscribe(data => {
      this.EmpTypeData = data["result"];
      this.dataTableFlag = true
      this.chRef.detectChanges(); 
      setTimeout(function () {
        $(function () {
          var table = $('#empTable').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }
  ngValue(event) {
    this.selectedCompanyobj = parseInt(event.target.value);
  }

  edit(object){
    this.geteditid = object.empCatId
    this.editemployee(this.geteditid)
     this.isedit = true;
     this.StatusFlag=true
     this.flag1 = object.status;
  }
  flag1 : boolean = false;
  editemployee(geteditid){
    let urledit = this.baseurl + '/getEmpCategoryById/';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(urledit + geteditid,{headers}).subscribe(data => {
        this.EmpTYPModel.status = data['result']['status'];
        this.EmpTYPModel.empCatName = data['result']['empCatName'];
        this.EmpTYPModel.empCatDesc = data['result']['empCatDesc'];
        this.EmpTYPModel.empNoticePeriod= data['result']['empNoticePeriod']
        var comp_id = data['result']['companyMaster']['compId'];
        this.selectedCompanyobj=data['result'].companyMaster.compId;
        // for(var i=0;i<this.companydata.length;i++){
        //    if(this.companydata[i].compId == comp_id){
        //      this.selectedCompanyobj =  comp_id;
        //    }
        // }
      },
      (err: HttpErrorResponse) => {
      });
  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }

  formReset(){
  this.selectedCompanyobj = this.selectUndefinedOptionValue
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}

