import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTypeMasterComponent } from './employee-type-master.component';

describe('EmployeeTypeMasterComponent', () => {
  let component: EmployeeTypeMasterComponent;
  let fixture: ComponentFixture<EmployeeTypeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTypeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTypeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
