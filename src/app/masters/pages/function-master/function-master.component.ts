import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';

import { FunctionMasterService } from '../../../shared/services/functionmasterservice';
import { functionmodel } from '../../../shared/model/functionmastermodel';
  import { from } from 'rxjs';
  import { ChangeDetectorRef } from '@angular/core';
  import { ToastrService } from 'ngx-toastr';
  import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-function-master',
  templateUrl: './function-master.component.html',
  styleUrls: ['./function-master.component.css'],
  providers: [FunctionMasterService]
})
export class FunctionMasterComponent implements OnInit {

  funct_model: functionmodel;
  baseurl = MainURL.HostUrl;
  companydata: any;
  businessgroupdata: any;
  selectedCompanyobj: any;
  selectedbusinessobj: any;
  compId: any;
  selectedComp:Number;
  functiondatalist: any;
  editcompdata: any;
  isedit = false;
  StatusFlag:boolean=false
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  funname: string;
  public selectUndefinedOptionValue:any;
  constructor( public httpService: HttpClient, 
    public func_Master_service:FunctionMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public Spinner :NgxSpinnerService) { 
    
    this.funct_model= new functionmodel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
    this.functiondatalistshow();
  }

  ngOnInit() {
    this.companyActive();
  }

  onSubmit(f) {
    this.Spinner.show();
    this.StatusFlag=false;
    if(this.isedit == false){
    this.funct_model.busiGroupId = this.selectedbusinessobj;
    this.func_Master_service.postFunctionMaster(this.funct_model).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.resetForm();
      this.toastr.success('Function-Master Information Inserted Successfully!', 'Function-Master');
      this.functiondatalistshow();
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Function-Master');
    });
  }
  else{
    this.Spinner.show();
    this.errflag = false
    let url1 = this.baseurl + '/FunctionUnitMaster';
    this.funct_model.functionUnitId = this.editcompdata;
    this.funct_model.busiGroupId = this.selectedbusinessobj;
    this.httpService.put(url1,this.funct_model).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.resetForm();
      this.toastr.success('Function-Master Information Updated Successfully!', 'Function-Master');
      this.functiondatalistshow();
      this.isedit=false;
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Function-Master');
      this.isedit=false;
    });
  }
}
  dataTableFlag : boolean = true;
  functiondatalistshow(){
    let busiurl = this.baseurl + '/FunctionUnitMaster/';
    this.dataTableFlag = false;
    this.httpService.get(busiurl + this.compId).subscribe(data => {
         this.functiondatalist = data["result"];
         this.dataTableFlag = true;
         this.chRef.detectChanges(); 
         setTimeout(function () {
           $(function () {
             var table = $('#functionTable').DataTable();
           });
         }, 1000);
         this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }
  
companyActive(){
  let url = this.baseurl + '/company/active';
  this.httpService.get(url).subscribe(data => {
    this.companydata = data["result"];
  },
  (err: HttpErrorResponse) => {
  });
}
  ngValuecompany(event){
    this.selectedCompanyobj = parseInt(event.target.value);
    this.businessgroup(this.selectedCompanyobj);
  }

  businessgroup(slectedvalue){
    let url1 = this.baseurl + '/BusinessGroup/Active/';
    this.httpService.get(url1 + slectedvalue).subscribe(data => {
        this.businessgroupdata = data["result"];
    },
    (err: HttpErrorResponse) => {
    });
  }
  ngValuebusiness(event){
    this.selectedbusinessobj = parseInt(event.target.value);
  }
  edit(object){
    this.editcompdata = object.functionUnitId;
    this.isedit = true;
    this.editocompany(this.editcompdata);
    this.StatusFlag=true;
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.reset();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.functionUnitId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/function/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
        this.Spinner.hide();
        this.toastr.success('Delete Successfully', );
        this.functiondatalistshow();
        },(err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error(err.error.message);
          });
      }
  }
  //set company
  editocompany(geteditcompvalue){
    let urledit = this.baseurl + '/FunctionUnitMaster/getId/';
    this.httpService.get(urledit + geteditcompvalue).subscribe(data => {
        this.funct_model.status = data['result']['status'];
        this.funct_model.functionUnitName = data['result']['functionUnitName'];
        this.funct_model.functionUnitDesc = data['result']['functionUnitDesc'];
        this.funname =  this.funct_model.functionUnitName;
        var comp_id = data['result']['companyMaster']['compId'];
        var business_id = data['result']['businessGroupMaster']['busiGroupId'];
        for(var i=0; i<this.companydata.length; i++){
          if(this.companydata[i].compId == comp_id){
            this.selectedCompanyobj =  comp_id;
          }
        }
        let url1 = this.baseurl + '/BusinessGroup/Active/';
        this.httpService.get(url1 + this.selectedCompanyobj).subscribe(data => {
            this.businessgroupdata = data["result"];
            for(var j=0; j<this.businessgroupdata.length; j++){
              if(this.businessgroupdata[j].busiGroupId == business_id){
                this.selectedbusinessobj =  business_id;
              }
            }
          },
          (err: HttpErrorResponse) => {
          });
      },
      (err: HttpErrorResponse) => {
      });
  }
  error(){
    if(this.funname != this.funct_model.functionUnitName){
    let obj={
      busiGroupId : this.selectedbusinessobj,
      functionUnitName : this.funct_model.functionUnitName
      }
      let url = this.baseurl + '/functionUnit';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        });
    }
  }

  //set business data
  uiswitch(event){
     var check = event.target['className'];
    if(event.target.className == "switch switch-medium checked"){
      this.funct_model.status =true;
    }else{
      this.funct_model.status = false
    }
}

  resetForm(){
    setTimeout(() => {
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.selectedbusinessobj = this.selectUndefinedOptionValue;
    }, 100);
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}