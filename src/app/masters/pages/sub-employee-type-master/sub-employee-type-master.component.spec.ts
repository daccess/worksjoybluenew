import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubEmployeeTypeMasterComponent } from './sub-employee-type-master.component';

describe('SubEmployeeTypeMasterComponent', () => {
  let component: SubEmployeeTypeMasterComponent;
  let fixture: ComponentFixture<SubEmployeeTypeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubEmployeeTypeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubEmployeeTypeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
