import { Component, OnInit, Inject } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {SubEmpTypMasterService} from '../../../shared/services/SubEmpTypService';
import { SubEmpTypMaster } from '../../../shared/model/SubEmpTypModel'
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';

import { DOCUMENT } from '@angular/common';
declare var $;
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-sub-employee-type-master',
  templateUrl: './sub-employee-type-master.component.html',
  styleUrls: ['./sub-employee-type-master.component.css'],
  providers: [SubEmpTypMasterService]
})
export class SubEmployeeTypeMasterComponent implements OnInit {
  StatusFlag :boolean=false
  SubEmpTypMast:SubEmpTypMaster;
  selectedEmpTypeObj: any;
  selectedCompanyobj: any;
  Employeedataall:any;
  AllSubEmp:any;
  Compdata:any;
  compId: any;
  baseurl = MainURL.HostUrl;
  subempedit: any;
  isedit = false;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  loconame: String;
  public selectUndefinedOptionValue:any;
  constructor(@Inject(DOCUMENT) document,
  public httpService:HttpClient,
  public SubempTypService:SubEmpTypMasterService, 
  public chRef: ChangeDetectorRef,
  private toastr: ToastrService) { 
    
    this.SubEmpTypMast=new SubEmpTypMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
  }
  ngOnInit() {
   let url = this.baseurl + '/company/active';
   this.httpService.get(url).subscribe(data => {
      this.Compdata = data["result"];
    },
    (err: HttpErrorResponse) => {
    });
    this.getaubemployeedata();
  }
  error(){
    if( this.loconame != this.SubEmpTypMast.subEmpTypeName){
    let obj={
      empTypeId : this.selectedEmpTypeObj,
      subEmpTypeName : this.SubEmpTypMast.subEmpTypeName
      }
      let url = this.baseurl + '/subEmployeTypeMaster';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message;
         this.toastr.error(this.msg);
         this.errflag = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag = false;
        });
    }
  }

  CompValue(event){
     this.selectedCompanyobj = parseInt(event.target.value);
     this.getemployee(this.selectedCompanyobj);
  }
  getemployee(catchelectedvalue){
    let testempactive = this.baseurl + '/EmployeeType/Active/';
    this.httpService.get(testempactive + catchelectedvalue).subscribe(data => {
        this.Employeedataall = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }


  EmpValue(event){
    this.selectedEmpTypeObj = parseInt(event.target.value)
   } 
   dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.reset();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.subEmpTypeId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/subemployeetype/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.toastr.success('Delete Successfully', );
          this.chRef.detectChanges();
          this.getaubemployeedata()
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.toastr.error(err.error.message);
        });
      }
    }
  
  }
  
  
  onSubmit(SubEmpTypMast,f:NgForm){
    this.StatusFlag=false
    this.SubEmpTypMast.empTypeId = this.selectedEmpTypeObj;
    this.SubEmpTypMast.status=this.flag1;
    if(this.isedit == false){
    this.SubEmpTypMast.empTypeId = this.selectedEmpTypeObj;
    this.SubempTypService.postSubEmpTypeMaster(this.SubEmpTypMast).subscribe(data => {
      this.getaubemployeedata()
      this.Employeedataall=[];
      f.reset();
      this.resetForm();
      this.toastr.success('Sub-Employee-Master Information Inserted Successfully!', 'Sub-Employee-Master');
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Sub-Employee-Master');
    });
  }
  else{
    //document.getElementById('switchbtn').style.display="none";
    let updateurl = this.baseurl + '/SubEmployeeType';
    this.SubEmpTypMast.subEmpTypeId = this.subempedit;
    this.SubEmpTypMast.empTypeId = this.selectedEmpTypeObj
    this.httpService.put(updateurl,this.SubEmpTypMast).subscribe(data => {
      this.getaubemployeedata()
      this.Employeedataall=[];
      f.reset();
      this.resetForm();
      this.toastr.success('Sub-Employee-Master Information Updated Successfully!', 'Sub-Employee-Master');
      this.isedit=false
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Sub-Employee-Master');
      this.isedit=false
    });

  }
    }
    dataTableFlag : boolean  = true
  getaubemployeedata(){
    let url1 = this.baseurl + '/SubEmployeeType/';
    this.dataTableFlag = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
          this.AllSubEmp = data["result"];
          this.dataTableFlag = true
          this.chRef.detectChanges(); 
          setTimeout(function () {
          $(function () {
            var table = $('#subempTable').DataTable();
                
          });
        }, 1000);
        
        this.chRef.detectChanges();

        },
        (err: HttpErrorResponse) => {
        });
  }

  edit(object){
    this.subempedit = object.subEmpTypeId;
    this.geteditdatasucemp(this.subempedit);
    this.isedit = true;
    this.StatusFlag=true;
    this.flag1 = object.status;
  }
  flag1 : boolean = false;
  geteditdatasucemp(getselectedvaluesub){
    let test = this.baseurl + '/SubEmployeeCategory/getByID/';
    this.httpService.get(test + getselectedvaluesub).subscribe(data => {
        this.SubEmpTypMast.status = data['result']['status'];
        this.SubEmpTypMast.subEmpTypeName = data['result']['subEmpTypeName'];
        this.SubEmpTypMast.subEmpTypeDesc = data['result']['subEmpTypeDesc'];
        var comp_id = data['result']['companyMaster']['compId'];
        this.loconame = this.SubEmpTypMast.subEmpTypeName
        var emp_id = data['result']['employeeTypeMaster']['empTypeId'];
        for(var i=0;i<this.Compdata.length;i++){
           if(this.Compdata[i].compId == comp_id){
            this.selectedCompanyobj =  comp_id;
           }
        }
        let empactiveurl = this.baseurl + '/EmployeeType/Active/';
        this.httpService.get(empactiveurl + this.selectedCompanyobj).subscribe(data => {
            this.Employeedataall = data["result"];
            for(var j=0;j<this.Employeedataall.length;j++){
               if(this.Employeedataall[j].empTypeId == emp_id){
                 this.selectedEmpTypeObj =  emp_id;
               }
            }

          },
          (err: HttpErrorResponse) => {
          });
    
      },
      (err: HttpErrorResponse) => {
      });

  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }

  resetForm(){
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
    this.selectedEmpTypeObj = this.selectUndefinedOptionValue;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}