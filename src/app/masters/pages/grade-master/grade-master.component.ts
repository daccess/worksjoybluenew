import { Component, OnInit,Inject } from '@angular/core';
import { GradeMaster } from '../../../shared/model/GradeMasterModel';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { GradeMasterService } from 'src/app/shared/services/GradeMasterService';
import { MainURL } from '../../../shared/configurl';
declare var $;
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-grade-master',
  templateUrl: './grade-master.component.html',
  styleUrls: ['./grade-master.component.css'],
  providers: [GradeMasterService]
})
export class GradeMasterComponent implements OnInit {

  GradeMasterModel: GradeMaster;
  selectedCompanyobj: any;
  companydata: any;
  AllGradeMaster: any;
  isedit = false;
  grdaeidedit: any;
  checkedVal:any;
  baseurl = MainURL.HostUrl;
  compId: any;
  selectedval: any;
  checkedSingle: any;
  checkedBoth: boolean;
  checkedNoPunch: boolean;
  StatusFlag:boolean=false
  single: any;
  both: any;
  nopunch: any;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  grdname: any;
  SinglePunch: any;
  public selectUndefinedOptionValue: any;
  constructor(@Inject(DOCUMENT) document,
  public httpService: HttpClient, 
  public GradeMasterSer: GradeMasterService, 
  public chRef: ChangeDetectorRef,
  private toastr: ToastrService,
  public Spinner :NgxSpinnerService) {
    this.GradeMasterModel = new GradeMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
    this.GradeMasterModel.attendnacecopultion = 'BothPunch';
  }
  ngOnInit() {
    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    this.GradeMaster();
  }

  ngValue(event) {
    this.selectedCompanyobj = parseInt(event.target.value);
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.reset();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.gradeId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deletegrade/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully');
          this.chRef.detectChanges();
          this.GradeMaster();
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
      }
    }
  
  }
  
  error(){
    if(this.grdname != this.GradeMasterModel.gradeName){
    let obj={
      compId : this.selectedCompanyobj,
      gradeName : this.GradeMasterModel.gradeName
      }
      let url = this.baseurl + '/gradeMaster';
      this.httpService.post(url,obj).subscribe((data :any) => {
        this.errordata= data["result"];
        this.msg= data.message
        this.toastr.error(this.msg);
        this.errflag = true
      },
      (err: HttpErrorResponse) => {
        this.errflag = false
      });
    }
  }
  edit(object){
    this.grdaeidedit = object.gradeId;
    this.isedit = true;
    this.flag1 = object.status;
    this.editgrademasterfunction(this.grdaeidedit)
    this.StatusFlag=true;
  }

  flag1 : boolean = false;
  editgrademasterfunction(getselectedgrade){
    let urledit = this.baseurl + '/GradeMaster/getById/';
    this.httpService.get(urledit + getselectedgrade).subscribe(data => {
        this.chRef.detectChanges();
        this.GradeMasterModel.status = data['result']['status'];
        this.GradeMasterModel.gradeName = data['result']['gradeName'];
        this.grdname = this.GradeMasterModel.gradeName
        this.GradeMasterModel.gradeDesc = data['result']['gradeDesc'];
        this.GradeMasterModel.gradeLevel=data['result']['gradeLevel']
        this.GradeMasterModel.attendnacecopultion = data['result']['attendnacecopultion'];
        var comp_id = data['result']['companyMaster']['compId'];
        for(var i=0;i<this.companydata.length;i++){
           if(this.companydata[i].compId == comp_id){
             this.selectedCompanyobj =  comp_id;
           }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  dataTableFlag : boolean = true;
  GradeMaster(){
    let url1 = this.baseurl + '/GradeMaster/';
    this.dataTableFlag = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.AllGradeMaster = data["result"];
       this.dataTableFlag = true
        this.chRef.detectChanges(); 
        setTimeout(function () {
          $(function () {
            var table = $('#grademasterTable').DataTable();
          });
        }, 1000);
        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }

onSubmit(f) {
  this.Spinner.show();
  this.StatusFlag=false
  this.GradeMasterModel.status=this.flag1;
  if(this.isedit == false){
      this.GradeMasterModel.compId = this.selectedCompanyobj;
      this.GradeMasterSer.postGradeMaster(this.GradeMasterModel).subscribe(data => {
          this.Spinner.hide();
          this.chRef.detectChanges();
          f.reset();
          this.resetForm();
          this.toastr.success('Grade-Master Information Inserted Successfully!', 'Grade-Master');
          this.GradeMaster()
        },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Function-Master');
        });

      }
      else{
        this.errflag = false
        let url1 = this.baseurl + '/GradeMaster';
        this.GradeMasterModel.gradeId = this.grdaeidedit;
        this.GradeMasterModel.compId = this.selectedCompanyobj;
        this.httpService.put(url1,this.GradeMasterModel).subscribe(data => {
          this.Spinner.hide();
          f.reset();
          this.resetForm();
          this.toastr.success('Grade-Master Information Updated Successfully!', 'Grade-Master');
          this.GradeMaster();
          this.isedit=false
        },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Function-Master');
          this.isedit=false;
        });

      }

  }

  uiswitch(event){
    this.flag1 = !this.flag1;
 
  }

  resetForm(){
    setTimeout(() => {
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.GradeMasterModel.attendnacecopultion = 'SinglePunch';
    }, 100);
   
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}

