import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { subdepartmentmodel } from '../../../shared/model/subdepartmentmastermodel';
import { subdepartmentMasterService } from '../../../shared/services/subdepartmentmasterservice';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sub-department-master',
  templateUrl: './sub-department-master.component.html',
  styleUrls: ['./sub-department-master.component.css'],
  providers: [subdepartmentMasterService]
})
export class SubDepartmentMasterComponent implements OnInit {

  StatusFlag:boolean=false
  baseurl = MainURL.HostUrl;
  companydata: any;
  selectedCompanyobj: any;
  businessgroupdata: any;
  selectedbusinessobj: any;
  functiondata: any;
  selectfunctionobj: any;
  departmentselect: any;
  selectdepartmentobj: any;
  subdeptmodel: subdepartmentmodel;
  subdepartmentlistdata: any;
  compId: any;
  editsubdept: any;
  isedit = false;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errorflag: boolean;
  errflag: boolean;
  subdepname: any;
  public selectUndefinedOptionValue:any;
  constructor(public httpService: HttpClient,public subdeptservice: subdepartmentMasterService,public chRef: ChangeDetectorRef,private toastr: ToastrService) { 
  this.subdeptmodel = new subdepartmentmodel();
  let user = sessionStorage.getItem('loggedUser');
  this.loggedUser = JSON.parse(user);
  this.compId =  this.loggedUser.compId
  this.subdepartmentlist();
}
  ngOnInit() {
    this.companyActive();
  }

  onSubmit(f) {
   this.StatusFlag=false
   this.subdeptmodel.status=this.flag1;
    if(this.isedit ==false){
    this.subdeptmodel.deptId = this.selectdepartmentobj;
    this.subdeptservice.postsubdepartmentMaster(this.subdeptmodel).subscribe(data => {
      f.reset();
      this.resetForm();
      this.toastr.success('Sub-Department-Master Information Inserted Successfully!', 'Sub-Department-Master');
      this.subdepartmentlist();
      this.businessgroupdata=[];
      this.functiondata=[];
      this.departmentselect=[]
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Sub-Department-Master');
    });
  }
  else{
    this.errorflag = false
    let urlsd = this.baseurl + '/SubDepartment';
    this.subdeptmodel.subDeptId = this.editsubdept;
    this.subdeptmodel.deptId = this.selectdepartmentobj;
    this.httpService.put(urlsd,this.subdeptmodel).subscribe(data => {
      f.reset();
      this.resetForm();
      this.toastr.success('Sub-Department-Master Information Updated Successfully!', 'Sub-Department-Master');
      this.subdepartmentlist();
      this.businessgroupdata=[];
      this.functiondata=[];
      this.departmentselect=[]
      this.isedit=false
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Sub-Department-Master');
      this.isedit=false
    });
  }
}

  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.resetForm();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.subDeptId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/subdepartment/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.toastr.success('Delete Successfully', );
          this.chRef.detectChanges();
          this.subdepartmentlist();
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.toastr.error(err.error.message);
        });
      }
    }
  
  }
  dataTableFlag : boolean = true;
  subdepartmentlist(){
    let sundepturl = this.baseurl + '/SubDepartment/';
    this.dataTableFlag = false
    this.httpService.get(sundepturl + this.compId).subscribe(data => {
         this.subdepartmentlistdata = data["result"];
         this.dataTableFlag = true
         this.chRef.detectChanges(); 
        setTimeout(function () {
        $(function () {
          var table = $('#subdepartmentTable').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });

  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }
error(){
  if(this.subdepname != this.subdeptmodel.subDeptName){
  let obj={
    "deptId": this.selectdepartmentobj,
    "subDeptName": this.subdeptmodel.subDeptName
    }
    let url = this.baseurl + '/subDeparmentmaster';
    this.httpService.post(url,obj).subscribe((data :any) => {
       this.errordata= data["result"];
       this.msg= data.message
       this.toastr.error(this.msg);
       this.errorflag = true
      },
      (err: HttpErrorResponse) => {
        this.errorflag = false
      });
  }
}

  companyActive(){
    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }

  ngValuecompany(event){
     this.selectedCompanyobj = parseInt(event.target.value);
     this.Departmentgroup(this.selectedCompanyobj);
  }

  Departmentgroup(selecteddeptvalue){
    let url1 = this.baseurl + '/Department/Active/';
    this.httpService.get(url1 + selecteddeptvalue).subscribe(data => {
        this.departmentselect = data["result"];
    },
    (err: HttpErrorResponse) => {
     
    });
  }

  ngValuedepartmentgroup(event){
    this.selectdepartmentobj = parseInt(event.target.value)
  }

  edit(object){
    this.editsubdept = object.subDeptId;
    this.isedit =true;
    this.subdepartmentedit(this.editsubdept)
    this.StatusFlag=true;
    this.flag1 = object.status;
  }
  flag1 : boolean = false;

  subdepartmentedit(getselectedvalue){
    let urledit = this.baseurl + '/SubDepartment/GetById/';
    this.httpService.get(urledit + getselectedvalue).subscribe(data => {
        this.subdeptmodel.status = data['result']['status'];
        this.subdeptmodel.subDeptName = data['result']['subDeptName'];
        this.subdepname =  this.subdeptmodel.subDeptName ;
        this.subdeptmodel.subDeptDesc = data['result']['subDeptDesc'];
        var comp_id = data['result']['companyMaster']['compId'];
        var dept_id = data['result']['departmentMaster']['deptId'];
        for(var i=0; i<this.companydata.length; i++){
          if(this.companydata[i].compId == comp_id){
            this.selectedCompanyobj =  comp_id;
          }
        }

        let url1 = this.baseurl + '/Department/Active/';
        this.httpService.get(url1 + this.selectedCompanyobj).subscribe(data => {
            this.departmentselect = data["result"];
            for(var l=0; l<this.departmentselect.length; l++){
              if(this.departmentselect[l].deptId == dept_id){
                this.selectdepartmentobj =  dept_id;
              }
            }
          },
          (err: HttpErrorResponse) => {
          });
      
      },(err: HttpErrorResponse) => {
      });
  }


  resetForm(){
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
    this.selectedbusinessobj = this.selectUndefinedOptionValue;
    this.selectfunctionobj = this.selectUndefinedOptionValue;
    this.selectdepartmentobj = this.selectUndefinedOptionValue;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}