import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDepartmentMasterComponent } from './sub-department-master.component';

describe('SubDepartmentMasterComponent', () => {
  let component: SubDepartmentMasterComponent;
  let fixture: ComponentFixture<SubDepartmentMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubDepartmentMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDepartmentMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
