import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {PayGroupMasterService} from '../../../shared/services/PayGroupMasterService'
import{PayGroupMaster} from '../../../shared/model/PayGroupMaster'
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pay-group-master',
  templateUrl: './pay-group-master.component.html',
  styleUrls: ['./pay-group-master.component.css'],
  providers: [PayGroupMasterService]
})
export class PayGroupMasterComponent  {
  PayGroupModel : PayGroupMaster;
  selectedCompanyobj: any
  companydata:any;
  AllPayGroupMastData:any;
  baseurl = MainURL.HostUrl;
  compId: any;
  editpaygroup: any;
  isedit = false;
  StatusFlag:boolean=false
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  loconame: string;
  public selectUndefinedOptionValue:any;
  constructor( public httpService:HttpClient,
    public PayGropSer:PayGroupMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService) { 
    this.PayGroupModel=new PayGroupMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.reset();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.payGroupId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deletepaygroup/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.toastr.success('Delete Successfully', );
          this.chRef.detectChanges();
          this.getpaygroupmaster();
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.toastr.error(err.error.message);
        });
      }
    }
  
  }
  ngOnInit() {

    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
      this.companydata = data["result"];
    },
    (err: HttpErrorResponse) => {
    });
    this.getpaygroupmaster();
  }
  error(){
    if(this.loconame != this.PayGroupModel.payGroupName){
    let obj={
      compId : this.selectedCompanyobj,
      payGroupName : this.PayGroupModel.payGroupName
    }
      let url = this.baseurl + '/payGroupMaster';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message;
         this.toastr.error(this.msg);
        this.errflag = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag = false;
        });
    }
  }

  onSubmit(f) {
    this.StatusFlag=false
    this.PayGroupModel.status=this.flag1;
    if(this.isedit == false){
    this.PayGroupModel.compId = this.selectedCompanyobj;
    this.PayGropSer.postPayGroupMaster(this.PayGroupModel).subscribe(data => {
      f.reset();
      this.resetForm();
      this.toastr.success('Pay-Group-Master Information Inserted Successfully!', 'Pay-Group-Master');
      this.getpaygroupmaster();
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Pay-Group-Master');
    });
  }
  else{
    this.errflag=false
    let updateurl = this.baseurl + '/PayGroup';
    this.PayGroupModel.compId = this.selectedCompanyobj;
    this.PayGroupModel.payGroupId = this.editpaygroup;
    //updtae pay group
    this.httpService.put(updateurl,this.PayGroupModel).subscribe(data => {
      f.reset();
      this.resetForm();
      this.toastr.success('Pay-Group-Master Information Updated Successfully!', 'Pay-Group-Master');
      this.getpaygroupmaster();
      this.isedit=false;
    },
      (err: HttpErrorResponse) => {
        this.toastr.error('Server Side Error..!', 'Pay-Group-Master');
        this.isedit=false;
      });
  }
}
 
  edit(object){
    this.PayGroupModel = object;
    this.flag1 = object.status;
    this.editpaygroup = object.payGroupId;
     this.isedit = true;
     this.editpayroupfunction(this.editpaygroup);
     this.StatusFlag=true;
  }
  flag1 : boolean = false;
  editpayroupfunction(geteditpaygroupvalue){
    let urledit = this.baseurl + '/PayGroup/getById/';
    this.httpService.get(urledit + geteditpaygroupvalue).subscribe(data => {
        this.PayGroupModel.status = data['result']['status'];
        this.PayGroupModel.payGroupName = data['result']['payGroupName'];
        this.PayGroupModel.payGroupDesc = data['result']['payGroupDesc'];
        var comp_id = data['result']['companyMaster']['compId'];
        this.loconame = this.PayGroupModel.payGroupName
        for(var i=0;i<this.companydata.length;i++){
           if(this.companydata[i].compId == comp_id){
             this.selectedCompanyobj =  comp_id;
           }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  dataTableFlag : boolean = true;
  getpaygroupmaster(){
    let url1 = this.baseurl + '/PayGroup/';
    this.dataTableFlag = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.AllPayGroupMastData = data["result"];
        this.dataTableFlag = true
        this.chRef.detectChanges(); 
        setTimeout(function () {
        $(function () {
          var table = $('#paygroupTable').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }

  ngValue(event){
    this.selectedCompanyobj = parseInt(event.target.value)
  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }

  resetForm(){
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}