import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayGroupMasterComponent } from './pay-group-master.component';

describe('PayGroupMasterComponent', () => {
  let component: PayGroupMasterComponent;
  let fixture: ComponentFixture<PayGroupMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayGroupMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayGroupMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
