import { Component, OnInit } from '@angular/core';
import { CostCenterMaster } from '../../../shared/model/CostCenterModel';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { CostCenterMasterService } from 'src/app/shared/services/CostCenterMasterService';
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cost-center-master',
  templateUrl: './cost-center-master.component.html',
  styleUrls: ['./cost-center-master.component.css'],
  providers: [CostCenterMasterService]
})
export class CostCenterMasterComponent implements OnInit {

  CostCenterModel: CostCenterMaster;
  selectedCompanyobj: any;
  companydata: any;
  CostCenterMasterData: any;
  baseurl = MainURL.HostUrl
  compId: any;
  isedit = false;
  iseditdatacost: any;
  StatusFlag: boolean = false
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  costname: string;
  public selectUndefinedOptionValue: any;
  constructor(public httpService: HttpClient,
    public CostCenterMastSer: CostCenterMasterService,
    public ch: ChangeDetectorRef,
    private toastr: ToastrService,
    public Spinner: NgxSpinnerService) {

    this.CostCenterModel = new CostCenterMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
  }
  ngOnInit() {
    let url = this.baseurl + '/company/active';
    this.httpService.get(url).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
      this.costcenterlist()
  }

  onSubmit(f) {
    this.Spinner.show();
    this.StatusFlag = false
    this.CostCenterModel.status = this.flag1;
    if (this.isedit == false) {
      this.CostCenterModel.compId = this.selectedCompanyobj;
      this.CostCenterMastSer.postCostCenterMaster(this.CostCenterModel).subscribe(data => {
          this.Spinner.hide();
          this.ch.detectChanges();
          this.costcenterlist();
          this.ch.detectChanges();
          f.reset();
          this.resetForm();
          this.toastr.success('Cost-Center-Master Information Inserted Successfully!', 'Cost-Center-Master');
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Cost-Center-Master');
          });
    }
    else {
      this.errflag = false
      let urledit = this.baseurl + '/CostCenter';
      this.CostCenterModel.compId = this.selectedCompanyobj;
      this.CostCenterModel.costCenterId = this.iseditdatacost;
      this.httpService.put(urledit, this.CostCenterModel).subscribe(data => {
          this.Spinner.hide();
          this.ch.detectChanges();
          this.ch.detectChanges();
          f.reset();
          this.resetForm();
          this.toastr.success('Cost-Center-Master Information Updated Successfully!', 'Cost-Center-Master');
          this.costcenterlist()
          this.isedit = false
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Cost-Center-Master');
            this.isedit = false
          });

    }

  }

  error() {
    if (this.costname != this.CostCenterModel.costCenterName) {
      let obj = {
        compId: this.selectedCompanyobj,
        costCenterCode: this.CostCenterModel.costCenterCode,
        costCenterName: this.CostCenterModel.costCenterName
      }
      let url = this.baseurl + '/costCenter';
      this.httpService.post(url, obj).subscribe((data: any) => {
          this.errordata = data["result"];
          this.msg = data.message
          this.toastr.error(this.msg);
          this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        });

    }
  }

  ngValue(event) {
    this.selectedCompanyobj = parseInt(event.target.value);
  }

  dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.reset();
    this.Spinner.show();

  }
  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.costCenterId;
    this.dltObjfun(this.dltObje)
  }
  dltObjfun(object) {
    if (this.dltmodelFlag == true) {
      if (this.dltmodelFlag == true) {
        let url1 = this.baseurl + '/costcenter/';
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
            this.Spinner.hide();
            this.toastr.success('Delete Successfully');
            this.ch.detectChanges();
            this.costcenterlist()
            this.ch.detectChanges();
          }, (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error(err.error.message);
          });
      }
    }

  }

  edit(object) {
    this.iseditdatacost = object.costCenterId
    this.isedit = true;
    this.editcostcenter(this.iseditdatacost)
    this.StatusFlag = true
    this.flag1 = object.status;
  }
  flag1: boolean = false;
  editcostcenter(selectededitvalue) {
    let urledit = this.baseurl + '/CostCenter/getById/';
    this.httpService.get(urledit + selectededitvalue).subscribe(data => {
        this.CostCenterModel.status = data['result']['status'];
        this.CostCenterModel.costCenterCode = data['result']['costCenterCode'];
        this.CostCenterModel.costCenterName = data['result']['costCenterName'];
        this.costname = this.CostCenterModel.costCenterName;
        this.CostCenterModel.costCenterDesc = data['result']['costCenterDesc'];
        var comp_id = data['result']['companyMaster']['compId'];
        for (var i = 0; i < this.companydata.length; i++) {
          if (this.companydata[i].compId == comp_id) {
            this.selectedCompanyobj = comp_id;

          }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  dataTableFlag: boolean = true;
  costcenterlist() {
    let url1 = this.baseurl + '/CostCenter/';
    this.dataTableFlag = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.CostCenterMasterData = data["result"];
        this.dataTableFlag = true;
        this.ch.detectChanges();
        setTimeout(function () {
          $(function () {
            var table = $('#costcenterTable').DataTable();
          });
        }, 1000);
        this.ch.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }
  uiswitch(event) {
    this.flag1 = !this.flag1;
  }
  resetForm() {
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}

