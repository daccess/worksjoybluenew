import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostCenterMasterComponent } from './cost-center-master.component';

describe('CostCenterMasterComponent', () => {
  let component: CostCenterMasterComponent;
  let fixture: ComponentFixture<CostCenterMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostCenterMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostCenterMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
