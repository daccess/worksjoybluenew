import { Component, OnInit } from '@angular/core';
import { CompanyMasterService } from '../../../shared/services/companymasterservice';
import { UploadcompanylogoService } from '../../../shared/services/uploadcompanylogo';
import { commaster } from '../../../shared/model/companymastermodel';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from '../../../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-companymaster',
  templateUrl: './companymaster.component.html',
  styleUrls: ['./companymaster.component.css'],
  providers: [CompanyMasterService, UploadcompanylogoService]
})
export class CompanymasterComponent implements OnInit {
  isedit = false;
  company: commaster;
  companydata: any;
  isActive: boolean;
  baseurl = MainURL.HostUrl;
  Aws_flag=MainURL.Aws_flag;
  StatusFlag: boolean = false
  CompanyResponce: any;
  dltmodelFlag: boolean;
  errordata: any;
  getcmp: string;
  msg: any;
  errflag: boolean;
  empPerId: any;
  imageUrlupload: any;
  buttondisable: boolean = true;
  imageUrl: string = "";
  fileToUpload: File = null;
  empDetails: any;
  compId: any;
  imgFlag: boolean
  fileePath: any;
  imghideflag: boolean
  selectedFileName: string = "No File Choosen";
  user: string;
  users: any;
  token: any;
  fromdates: any;
  datehere: string;
  fromDate: any;
  companydatas: any=[];


  constructor(public compmastservice: CompanyMasterService,
    public httpService: HttpClient,
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public companylogoserve: UploadcompanylogoService,
    public Spinner: NgxSpinnerService,
    private dom: DomSanitizer,public http:HttpClient) {
    this.imgFlag = false;
    this.imghideflag = false;
    this.HideButton = false;
    this.company = new commaster();

  
  }

  ngOnInit() {
    
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.companydatalist();
    // this.empDetails = JSON.parse(sessionStorage.getItem('loggedUser'));
    // this.getallCompanyID()
  }
//   getallCompanyID(){
//     const tokens = this.token;
//     const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
//     let url = this.baseurl + '/getAllCompany';
//           this.http.get(url,{headers}).subscribe(data => {
//   })
// }
  HideButton: Boolean = false
  handleFileInput(file: FileList) {
    this.imgFlag = true
    this.Spinner.show();
    this.fileToUpload = file.item(0);
    //Show image preview
    this.selectedFileName = this.fileToUpload.name;
    var reader = new FileReader();
    reader.onload = (event: any) => {
      // this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    this.imghideflag = false
    this.ppostFile(this.fileToUpload).subscribe((data: any) => {
      this.Spinner.hide();
      this.imageUrl = data.result.imageUrl;
      this.filePathToShow = this.dom.bypassSecurityTrustResourceUrl(data.result.imageUrl);
      this.HideButton = true;
      this.buttondisable = false;
      var rest = this.imageUrl.substring(0, this.imageUrl.lastIndexOf("/") + 1);

    }, (err: HttpErrorResponse) => {
    })
  }

  companylogo(files) {
    this.companylogoserve.postcompanylogo(this.fileToUpload).subscribe(data => {
        this.imageUrlupload = data['result']['imageUrl'];
      });
  }
  dataTableFlag: boolean = true;
  companydatalist() {
    
    const tokens = this.token;
   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);

    let url = this.baseurl + '/getAllCompany';
    this.dataTableFlag = false;
    this.httpService.get(url,{headers}).subscribe((data:any)=> {
    
      console.log("this is the data",data)
        this.companydata = data["result"];
        
        // for(let i=0; i<this.companydata.length;i++){
        //   if(this.companydata[i]==0){
        //   this.companydatas.push(this.companydata[i])
        //   this.dataTableFlag = true
        //   }
        // }
        this.dataTableFlag = true
        this.chRef.detectChanges();
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable();
          });
        }, 1000);
        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }
  CompName: string;
  filePathToShow: any;
  error() {
    if (this.company.compName != this.CompName) {
      this.getcmp = this.company.compName
      let url = this.baseurl + '/comMaster/';
      this.httpService.get(url + this.getcmp).subscribe((data: any) => {
          this.errordata = data["result"];
          this.msg = data.message
          this.toastr.error(this.msg);
          this.errflag = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag = false;
        });
    }
  }
  dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);
    f.reset()
    this.selectedFileName = "No File Choosen";
    this.reset()
    this.Spinner.show();
  }
  dltObje(dltObje: any): any {
    throw new Error("Method not implemented.");
  }
  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.compId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object) {
    if (this.dltmodelFlag == true) {
      const tokens = this.token;
   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);

      let url1 = this.baseurl + '/deleteCompanyById/';
      this.httpService.delete(url1 + this.dltObje,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully..!');
          this.companydatalist();
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
    }
  }
  // getDate(e){
  //   debugger
  //   this.fromDate=e.target.value
  //  this.datehere = new Date(this.fromDate).toUTCString();
  //  console.log("fhashdgf",this.datehere)
  // }

  onSubmit(f) {
    
    this.StatusFlag = false
    this.company.status = this.flag;
    this.Spinner.show();
    if (this.isedit == false) {
      this.company.filPath = this.imageUrl;
      const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
let url = this.baseurl + '/createCompany';
      this.http.post(url,this.company,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.selectedFileName = "No File Choosen";
          f.reset();
          this.toastr.success('Company Information Inserted Successfully!', 'Company Info');
          $("#File1").val('');
          this.companydatalist();
          this.HideButton = true
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error("One Company already exist!!");
            this.HideButton = true
          });
    }
    else {
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      let url1 = this.baseurl + '/updateCompany';
      if (this.imgFlag == false) {
        this.company.filPath = this.fileePath
      }
      this.company.filPath = this.imageUrl;
      this.httpService.put(url1, this.company,{headers}).subscribe(data => {
          this.Spinner.hide();
          f.reset();
          this.selectedFileName = "No File Choosen";
          // this.resetCompanyForm();
          this.toastr.success('Company Information Updated Successfully!', 'Company Info Update');
          $("#File1").val('');
          this.companydatalist();
          this.isedit = false;
          this.HideButton = true
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.isedit = false;
            this.toastr.error('Server Side Error..!', 'Company Info Update');
            this.HideButton = true
          });
    }
  }
  flag: boolean = false;
  edit(object) {
    this.isedit = true;
    this.StatusFlag = true
    this.flag = object.status;
    this.imghideflag = true
    this.compId = object.compId
    this.componyGetByID(this.compId)
  }
  componyGetByID(getobject) {
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    let urledit = this.baseurl + '/getCompanyById/';
    this.httpService.get(urledit + getobject,{headers}).subscribe((data: any) => {
        this.company.compName = data.result.compName
        this.company.compAddress = data.result.compAddress
        this.company.contactDetails = data.result.contactDetails
        this.company.appLink = data.result.appLink;
        this.company.compId = data.result.compId;
        this.company.status = data.status
        this.filePathToShow = this.dom.bypassSecurityTrustResourceUrl(data.result.filPath);
        this.fileePath = data.result.filPath;
        // var lastIndex = this.fileePath.lastIndexOf('/');
        // this.selectedFileName = this.fileePath.substr(lastIndex + 1)

      },
      (err: HttpErrorResponse) => {
      });
  }
  uiswitch(event) {
  
  }

  open(c) {
    this.flag = !this.flag;
  }

  ppostFile(fileToUpload: File) {
    let url = this.baseurl + '/uploadFile';
    if(this.Aws_flag!='true'){
      url = this.baseurl + '/fileHandaling';
    }
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    return this.httpService.post(endpoint, formData).map(x => x)
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}
