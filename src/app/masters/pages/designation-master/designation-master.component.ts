
import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {DesignationMasterService} from '../../../shared/services/DesignationMasterService'
import { DesignationMaster } from 'src/app/shared/model/DesignationMasterModel';
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-designation-master',
  templateUrl: './designation-master.component.html',
  styleUrls: ['./designation-master.component.css'],
  providers: [DesignationMasterService]
})
export class DesignationMasterComponent implements OnInit {

  DesignationModel : DesignationMaster;
  selectedCompanyobj: any
  companydata:any;
  StatusFlag:boolean=false

  AllDesignationData:any
  baseurl = MainURL.HostUrl
  compId:any;
  isedit = false;
  editdesignationid: any;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  desname: String;
  public selectUndefinedOptionValue: any;
  user: string;
  users: any;
  token: any;
  constructor( public httpService:HttpClient,
    public DesignationMasterSer:DesignationMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public Spinner :NgxSpinnerService) { 
    this.DesignationModel=new DesignationMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.compId =  this.loggedUser.compId
  }
  ngOnInit() { 
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.Designation();
    this.getCompany()
  }
  getCompany(){
    
    let url = this.baseurl + '/getAllCompany';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.get(url,{headers}).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    }
  onSubmit(f) {
    this.Spinner.show();
    this.DesignationModel.status=this.flag1;
    this.StatusFlag=false;
    if(this.isedit == false){
    this.DesignationModel.compId = this.selectedCompanyobj;
    
    let url1 = this.baseurl + '/createDesignation';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.post(url1,this.DesignationModel,{headers}).subscribe(data => {
      this.Spinner.hide();
      this.Designation();
      f.reset();
      this.resetForm();
      this.toastr.success('Designation-Master Information Inserted Successfully!', 'Designation-Master');
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Designation-Master');
    });
  }
  else{
    this.Spinner.show();
    this.errflag = false
    let url1 = this.baseurl + '/updateDesignation';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.DesignationModel.compId = this.selectedCompanyobj;
    this.DesignationModel.desigId = this.editdesignationid;
    this.httpService.put(url1,this.DesignationModel,{headers}).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.resetForm();
      this.toastr.success('Designation-Master Information Updated Successfully!', 'Designation-Master');
      this.Designation();
      this.isedit=false
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Designation-Master');
      this.isedit=false
    });
  }
}

  error(){
    if (this.desname != this.DesignationModel.desigName){
      let obj={
        compId : this.selectedCompanyobj,
        desigName : this.DesignationModel.desigName
      }
      let url = this.baseurl + '/desiMaster';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        });
    }
  }

  
  edit(object){
    this.isedit = true;
    this.editdesignationid=object.desigId
    this.editdesignation(object.desigId)
    this.StatusFlag=true
    this.flag1 = object.status;
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.resetForm();
    this.reset();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.desigId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deleteDesignationById/';
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpService.delete(url1 + this.dltObje,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully');
          this.chRef.detectChanges();
          this.Designation()
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
      }
    }
  }
  flag1 : boolean = false;
  editdesignation(selectededitvalue){
    let urledit = this.baseurl + '/getDesignationById/';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(urledit + selectededitvalue,{headers}).subscribe(data => {
        this.DesignationModel.status = data['result']['status'];
        this.DesignationModel.desigName = data['result']['desigName'];
        this.desname = this.DesignationModel.desigName;
        this.DesignationModel.desigDesc = data['result']['desigDesc'];
        this.DesignationModel.desigLevel = data['result']['desigLevel'];
        var comp_id = data['result']['companyMaster']['compId'];
        this.selectedCompanyobj=data['result'].companyMaster.compId;
        // for(var i=0;i<this.companydata.length;i++){
        //   if(this.companydata[i].compId == comp_id){
        //   this.selectedCompanyobj =  comp_id;
        //   }
        // }



        
      },
      (err: HttpErrorResponse) => {
      });
    }
  dataTableFlag : boolean = true;
  Designation(){
    let url1 = this.baseurl + '/getAllDesignations';
    this.dataTableFlag = false
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(url1,{headers}).subscribe(data => {
      this.AllDesignationData = data["result"];
      this.dataTableFlag = true
      this.chRef.detectChanges(); 
      setTimeout(function () {
        $(function () {
          var table = $('#designationTable').DataTable();
        
        });
      }, 1000);
      this.chRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
    });
  }

  ngValue(event){
    this.selectedCompanyobj = parseInt(event.target.value)
  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }

  resetForm(){
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}
