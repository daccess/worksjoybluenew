import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {LocationMasterService} from '../../../shared/services/locationMasterService';
import{LocationMaster} from '../../../shared/model/locationMastermodel';
import { MainURL } from '../../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pay-group-master',
  templateUrl: './locationmaster.component.html',
  styleUrls: ['./locationmaster.component.css'],
  providers: [LocationMasterService]
})
export class LocationmasterComponent  {
  StatusFlag:boolean=false
  LocationMasterModel : LocationMaster;
  selectedCompanyobj: any
  companydata:any;
  AllLocationmasterMastData:any;
  baseurl = MainURL.HostUrl;
  compId: any;
  isedit = false;
  iseditvarible: any;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  loconame: String;
 public selectUndefinedOptionValue: any;
  user: string;
  users: any;
  token: any;
  constructor( public httpService:HttpClient,
    public LocationMasterSer:LocationMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public Spinner :NgxSpinnerService) { 
   
    this.LocationMasterModel=new LocationMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.compId =  this.loggedUser.compId;
  }

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.locationget();
    this.getCompany();
  }
  getCompany(){

    let url = this.baseurl + '/getAllCompany';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.get(url,{headers}).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
    this.reset();
    this.Spinner.show();
  }
  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.locationId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deleteLocationById/';
        const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpService.delete(url1 + this.dltObje,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully');
          this.chRef.detectChanges();
          this.locationget();
          this.chRef.detectChanges();
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
      }
    }
  
  }
  onSubmit(f) {
    this.Spinner.show();
    this.StatusFlag=false
    this.LocationMasterModel.status=this.flag1;
    if(this.isedit == false){
      let url = this.baseurl + '/createLocation';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.LocationMasterModel.compId = this.selectedCompanyobj;
    this.httpService.post(url,this.LocationMasterModel,{headers}).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.toastr.success('Loaction-Master Information Inserted Successfully!', 'Location-Master');
      this.locationget();
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Function-Master');
    });
  }
  else{
    //update
    this.errflag = false
    let urledit = this.baseurl + '/updateLocation';
    this.LocationMasterModel.compId = this.selectedCompanyobj;
    this.LocationMasterModel.locationId = this.iseditvarible;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.put(urledit,this.LocationMasterModel,{headers}).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.selectedCompanyobj = this.selectUndefinedOptionValue;
      this.toastr.success('Loaction-Master Information Updated Successfully!', 'Location-Master');
      this.locationget();
      this.isedit=false;
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Function-Master');
      this.isedit=false;
    });
  }
}

  edit(object){
    this.iseditvarible = object.locationId;
     this.isedit = true;
     this.geteditobjct(this.iseditvarible)
     this.StatusFlag=true;
     this.flag1 = object.status;
  }
  flag1 : boolean = false;
  geteditobjct(selectededitvariable){
    
    let urledit = this.baseurl + '/getLocationById/';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(urledit + selectededitvariable,{headers}).subscribe(data => {
      
        this.LocationMasterModel.status = data['result']['status'];
        this.LocationMasterModel.locationName = data['result']['locationName'];
        this.LocationMasterModel.locationDesc = data['result']['locationDesc'];
        this.LocationMasterModel.locationAddress = data['result']['locationAddress'];
        this.LocationMasterModel.locationConPerson = data['result']['locationConPerson'];
        this.LocationMasterModel.locationCode = data['result']['locationCode'];
        this.LocationMasterModel.emailId = data['result']['emailId'];
        this.LocationMasterModel.contactDetails = data['result']['contactDetails'];
        this.selectedCompanyobj=data['result'].companyMaster.compId;
        // var comp_id = data['result']['companyMaster']['compId'];
        // this.loconame = this.LocationMasterModel.locationName
        // for(var i=0;i<this.companydata.length;i++){
        //    if(this.companydata[i].compId == comp_id){
        //      this.selectedCompanyobj =  comp_id;
        //    }
        // }
      },
      (err: HttpErrorResponse) => {
      });
  }

  error(){
    if(this.loconame != this.LocationMasterModel.locationName){
    let obj={
      compId : this.selectedCompanyobj,
      locationCode : this.LocationMasterModel.locationCode,
      locationName: this.LocationMasterModel.locationName
      }
      let url = this.baseurl +  '/locationmaster';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        });
    }
  }

  dataTableFlag : boolean = true;
  locationget(){
  
    let url = this.baseurl + '/getAllLocations/';
   this.dataTableFlag = false
   const tokens = this.token;
   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(url,{headers}).subscribe(data => {
      this.AllLocationmasterMastData = data["result"];
      this.dataTableFlag = true
      this.chRef.detectChanges(); 
      setTimeout(function () {
        $(function () {
          var table = $('#locationTable').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
    });
  }

  ngValue(event){
    this.selectedCompanyobj = parseInt(event.target.value);
  }
  uiswitch(event){
    this.flag1 = !this.flag1;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}