import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { departmentMasterService } from '../../../shared/services/departmentmasterservice';
import { departmentmodel } from '../../../shared/model/departmentmastermodel';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-department-master',
  templateUrl: './department-master.component.html',
  styleUrls: ['./department-master.component.css'],
  providers: [departmentMasterService]

})
export class DepartmentMasterComponent implements OnInit {
  model: any = {};
  baseurl = MainURL.HostUrl;
  companydata: any;
  selectedCompanyobj: any;
  businessgroupdata: any;
  selectedbusinessobj: any;
  functiondata: any;
  selectfunctionobj: any;
  deptmodel: departmentmodel;
  compId: any;
  departmentlistdata: any;
  editdepartment: any;
  isedit = false;
  StatusFlag:boolean=false
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  errordata: any;
  msg: any;
  errorflag: boolean;
  errflag: boolean;
  deptname: any;
  depname: string;
  public selectUndefinedOptionValue:any;
  arr=[];
  comname: any;
  user: string;
  users: any;
  token: any;
  constructor(public httpService: HttpClient,
     public deptservicep: departmentMasterService, 
     public chRef: ChangeDetectorRef,
     private toastr: ToastrService,
     public Spinner :NgxSpinnerService) { 
   
  this.deptmodel = new departmentmodel()
  
  let user = sessionStorage.getItem('loggedUser');
  this.loggedUser = JSON.parse(user);
  // this.compId =  this.loggedUser.compId;
  this.departmentlist();
  }

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    
    this.companyActive();
    this.departmentlist();
  }

  companyActive(){
    const tokens = this.token;
   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    let url = this.baseurl + '/getAllCompany';
    this.httpService.get(url,{headers}).subscribe(data => {
      this.companydata = data["result"]; 
    },
    (err: HttpErrorResponse) => {
    });
  }
  
  ngValuecompany(event){
    this.selectedCompanyobj = parseInt(event.target.value);
    console.log(this.selectedCompanyobj);
    //this.businessgroup(this.selectedCompanyobj);
  }
 
  error(){
    if(this.depname != this.deptmodel.departmentName){
    let obj={
      functionUnitId : this.selectfunctionobj,
      deptName : this.deptmodel.departmentName
      }
      let url = this.baseurl + '/deparmentmaster';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errorflag = true
        },
        (err: HttpErrorResponse) => {
          this.errorflag = false
        });
    }
  }

  // ngValuebusiness(event){
  //   this.selectedbusinessobj = parseInt(event.target.value);
  //   this.functiongroup(this.selectedbusinessobj);
  // }
  // functiongroup(selectbusinessvalue){
  //   let url1 = this.baseurl + '/FunctionUnitMaster/Active/';
  //   this.httpService.get(url1 + selectbusinessvalue).subscribe(data => {
  //       this.functiondata = data["result"];
  //     },
  //     (err: HttpErrorResponse) => {
  //     });
  // }

  // ngValuefunctiongroup(event){
  //   this.selectfunctionobj = parseInt(event.target.value)
  // }

  onSubmit(f) {
    this.Spinner.show();
    this.StatusFlag=false
    this.deptmodel.status=this.flag1;
    if(this.isedit == false){
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      let url = this.baseurl + '/createDepartment';
    this.deptmodel.compId = this.selectedCompanyobj;
    this.httpService.post(url,this.deptmodel,{headers}).subscribe(data => {
      this.Spinner.hide();
      f.reset();
      this.resetForm();
      this.toastr.success('Department-Master Information Inserted Successfully!', 'Department-Master');
       this.departmentlist();
       this.businessgroupdata = [];
       this.functiondata =[];
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Department-Master');
    });
  }
  else{
      this.errorflag = false
      let urld = this.baseurl + '/updateDepartment';
      this.deptmodel.deptId = this.editdepartment;
      this.deptmodel.compId = this.selectedCompanyobj;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.put(urld,this.deptmodel,{headers}).subscribe(data => {
        this.Spinner.hide();
        this.departmentlist();
        this.businessgroupdata = [];
        this.functiondata =[];
        f.reset();
        this.resetForm();
        this.toastr.success('Department-Master Information Updated Successfully!', 'Department-Master');
        this.isedit=false
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
        this.isedit=false;
        this.toastr.error('Server Side Error..!', 'Department-Master');
      });
    }
  }

  uiswitch(event){
    this.flag1 = !this.flag1;
  }
  dataTableFlag : boolean = true;
  departmentlist(){
    let busiurl = this.baseurl + '/getAllDepartments/';
    this.dataTableFlag  = false;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(busiurl,{headers}).subscribe(data => {
        this.departmentlistdata = data["result"];
        this.dataTableFlag  = true;
        this.chRef.detectChanges(); 
          for (let j = 0; j < this.departmentlistdata.length; j++) {
          for (let index = 0; index < this.companydata.length; index++) {
            if(this.companydata[index].compId==this.departmentlistdata[j].compId){
              this.departmentlistdata[j].compName=this.companydata[index].compName;
            }
          }
        };
        setTimeout(function () {
          $(function () {
            var table = $('#departmentTable').DataTable();
          });
        }, 1000);
        this.chRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
    });
  } 
dltmodelEvent(f){
  this.dltmodelFlag=true;
  this.dltObjfun(this.dltObje);
  f.reset();
  this.resetForm();
  this.reset();
  this.Spinner.show();
}
dltObj(obj){
  this.dltmodelFlag=false;
  this.dltObje =obj.deptId;
  this.dltObjfun(this.dltObje);
}
dltObjfun(object){
  if(this.dltmodelFlag==true){
    if(this.dltmodelFlag==true){
      let url1 = this.baseurl + '/deleteDepartmentById/';
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.delete(url1 + this.dltObje,{headers}).subscribe(data => {
        this.Spinner.hide();
        this.toastr.success('Delete Successfully', );
        this.departmentlist();
      },(err: HttpErrorResponse) => {
        this.Spinner.hide();
        this.toastr.error(err.error.message);
      });
    }
  }
}

edit(object){
  this.editdepartment = object.deptId;
  this.isedit = true;
  this.editdepartmentfunction(this.editdepartment)
  this.StatusFlag=true
  this.flag1 = object.status;
}
flag1 : boolean = false;
editdepartmentfunction(selectedvalue){
  let urledit = this.baseurl + '/getDepartmentById/';
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(urledit + selectedvalue,{headers}).subscribe(data => {
        this.deptmodel.status = data['result']['status'];
        this.deptmodel.departmentName = data['result']['departmentName'];
        // this.comname= data['result'].companyMaster.compId;
        // this.selectedCompanyobj =  this.comname;

        this.deptmodel.departmentDescription = data['result']['departmentDescription'];
        this.deptmodel.departmentCode = data['result']['departmentCode'];
        this.selectedCompanyobj=data['result'].companyMaster.compId
        // this.depname = this.deptmodel.departmentName;
        // var comp_id = data['result']['companyMaster']['compId'];
        // var busi_id = data['result']['businessGroupMaster']['busiGroupId'];
        // var func_id = data['result']['functionUnitMaster']['functionUnitId'];
        // for(var i=0; i<this.companydata.length; i++){
        //   if(this.companydata[i].compId == comp_id){
        //     this.selectedCompanyobj =  comp_id;
        //   }
        // }
        // let url1 = this.baseurl + '/BusinessGroup/Active/';
        // this.httpService.get(url1 + this.selectedCompanyobj).subscribe(data => {
        // this.businessgroupdata = data["result"];
        // for(var j=0; j<this.businessgroupdata.length; j++){
        //   if(this.businessgroupdata[j].busiGroupId == busi_id){
        //     this.selectedbusinessobj =  busi_id;
        //   }
        // }
       // let url1 = this.baseurl + '/FunctionUnitMaster/Active/';
        // this.httpService.get(url1 + this.selectedbusinessobj).subscribe(data => {
        //     this.functiondata = data["result"];
        //     for(var k=0; k<this.functiondata.length; k++){
        //       if(this.functiondata[k].functionUnitId == func_id){
        //         this.selectfunctionobj =  func_id;
               
        //       }
        //     }
        //   },
        //   (err: HttpErrorResponse) => {
        //   });

    },
    (err: HttpErrorResponse) => {
    });
}

  resetForm(){
    this.selectedCompanyobj = this.selectUndefinedOptionValue;
    this.selectedbusinessobj = this.selectUndefinedOptionValue;
    this.selectfunctionobj = this.selectUndefinedOptionValue;
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}