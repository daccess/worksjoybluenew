import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftAssignmentreportComponent } from './shift-assignmentreport.component';

describe('ShiftAssignmentreportComponent', () => {
  let component: ShiftAssignmentreportComponent;
  let fixture: ComponentFixture<ShiftAssignmentreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftAssignmentreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftAssignmentreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
