import { Routes, RouterModule } from '@angular/router'

//import { AbsentreportComponent } from './absentreport.component';
import { ShiftAssignmentreportComponent } from './shift-assignmentreport.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ShiftAssignmentreportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'shiftassignmentreports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'shiftassignmentreports',
                    component: ShiftAssignmentreportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);