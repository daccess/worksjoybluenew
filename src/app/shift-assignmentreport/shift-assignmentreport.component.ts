import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
//import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-shift-assignmentreport',
  templateUrl: './shift-assignmentreport.component.html',
  styleUrls: ['./shift-assignmentreport.component.css'],
  providers:[ExcelService]
})
export class ShiftAssignmentreportComponent implements OnInit {
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];

  fromDate: any;
  toDate: any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  constructor(private excelService:ExcelService,private datePipe: DatePipe) { 

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();

    this.AllReportData = sessionStorage.getItem('monthlyreportData')
    this.AllReport = JSON.parse(this.AllReportData);
    this.exceldata = this.AllReport;

    for(let i=0; i<this.exceldata.length; i++){
        for (let j = 0; j < this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList.length; j++) {
          this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j];

          if(j !=0){
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].fullName = "";
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].lastName = "";
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].empId = "";
            this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].deptName = "";
            
            
          }
          this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j].sr = j+1;
          this.exceldata1.push(this.exceldata[i].attendanceReportNameResDtos.attendanceReportResDtoList[j]);
        }
     
    }

  }

  ngOnInit() {
  }

  exportAsXLSX():void {
    let excelData = new Array();
    for (let i = 0; i < this.exceldata1.length; i++) {

      let obj : any= {
        empId: this.exceldata1[i].empId,
        fullName: this.exceldata1[i].fullName.toString().trim()+' '+this.exceldata1[i].lastName.toString().trim(),
        deptName: this.exceldata1[i].deptName.toString().trim(),
        attenDate: this.exceldata1[i].attenDate,
        shiftName: this.exceldata1[i].shiftName,
       }

       excelData.push(obj)
    }


    this.excelService.exportAsExcelFile(excelData, 'shift_assignment_Report');
  }

  public captureScreen()  
  {    

    let item = {
      header:"EmpId",
      // header:"",
      title : "abc"
    }
    let item1 = {
      // header:"Shift",
      header:"Full Name",
      title : "cde"
    }
    let item2 = {
      // header:"Attendance date",
      header:"Dept name",
      title : "efg"
    }
    let item3 = {
      // header:"Attendance date",
      header:"Date",
      title : "jkl"
    }
    let item4 = {
      // header:"In Time",
      header:"shiftname",
      title : "ghi"
    }
    let item5 = {
      // header:"In Time",
      header:"sr",
      title : "ss"
    }
    
    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [item5,item,item1,item2,item3,item4];
    var rows = [];
    doc.addPage();
    var pageCount = doc.internal.getNumberOfPages();
    for(let i = 0; i < pageCount; i++) { 
    doc.setPage(i); 
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.text(580,12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
    }
      // var rowCountModNew = [
      // ["1721079361", "0001", "2100074911", "200", "22112017", "23112017", "51696"],
      // ["1721079362", "0002", "2100074912", "300", "22112017", "23112017", "51691"],
      // ["1721079363", "0003", "2100074913", "400", "22112017", "23112017", "51692"],
      // ["1721079364", "0004", "2100074914", "500", "22112017", "23112017", "51693"]
      // ]

      var rowCountModNew = this.exceldata1;

      rowCountModNew.forEach(element => {
        let obj = [element.sr,element.empId,element.fullName+' '+element.lastName,element.deptName.trim(),element.attenDate,element.shiftName]
      rows.push(obj);
    });

    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14 ,'Company Name :'+ this.compName)

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(34, 44 ,'Abbreviation:- GS : General Shift, SS : Store Shift')
    
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12);
    doc.setFontStyle("Arial")
    doc.text(280, 14,'Shift Assignment Report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(8)
    doc.setFontStyle("Arial")
    doc.text(280, 24 ,'From' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy") + ' ' +' To' + ' : ' + this.datePipe.transform(this.toDate,"dd-MMM-yyyy")
    )

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"))
    // doc.
    doc.autoTable(col, rows,{
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      headerStyles: {
        fillColor: [103, 132, 130],   
    },
    
      styles: {
        halign: 'center'
      },
      theme: 'grid',
      margin: { top: 50, left: 5, right: 5, bottom: 50 }
    });
    doc.save('shift_assignment_report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');
  }  

}
