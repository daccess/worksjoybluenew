import { ContractorLabourMasterListsModule } from './contractor-labour-master-lists.module';

describe('ContractorLabourMasterListsModule', () => {
  let contractorLabourMasterListsModule: ContractorLabourMasterListsModule;

  beforeEach(() => {
    contractorLabourMasterListsModule = new ContractorLabourMasterListsModule();
  });

  it('should create an instance', () => {
    expect(contractorLabourMasterListsModule).toBeTruthy();
  });
});
