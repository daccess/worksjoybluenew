import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {contractorLabourMastersList } from './contractor-labour-master-lists-routing.module';
import { ContractorLabourMasterListsComponent } from './contractor-labour-master-lists.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,

    contractorLabourMastersList,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ContractorLabourMasterListsComponent
  ]
})
export class ContractorLabourMasterListsModule { }
