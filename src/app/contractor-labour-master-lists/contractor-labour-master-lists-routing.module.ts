import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorLabourMasterListsComponent } from './contractor-labour-master-lists.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorLabourMasterListsComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorlabourMasterList',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'contractorlabourMasterList',
                  component: ContractorLabourMasterListsComponent
              }

          ]

  }


]

export const contractorLabourMastersList = RouterModule.forChild(appRoutes);
