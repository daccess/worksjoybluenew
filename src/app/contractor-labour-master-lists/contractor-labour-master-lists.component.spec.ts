import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorLabourMasterListsComponent } from './contractor-labour-master-lists.component';

describe('ContractorLabourMasterListsComponent', () => {
  let component: ContractorLabourMasterListsComponent;
  let fixture: ComponentFixture<ContractorLabourMasterListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorLabourMasterListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorLabourMasterListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
