import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contractor-labour-master-lists',
  templateUrl: './contractor-labour-master-lists.component.html',
  styleUrls: ['./contractor-labour-master-lists.component.css']
})
export class ContractorLabourMasterListsComponent implements OnInit {
  LabourDatas: any;
  baseurl = MainURL.HostUrl;
  empids: any;
  token: any;
  user: string;
  users: any;

  locationdata: any;
  locationId: any;
  labourlistEdit: string;
  labourlistEdit1:string;
  labourIds: any;
  locationIds: any;
  labourperids: any;
  labourId: any;
  manReqStatusIde: any;

  constructor(public httpservice:HttpClient,public router:Router,public Spinner:NgxSpinnerService,public toastr: ToastrService) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId;
    this.locationIds=this.users.roleList.locationId
    // this.getlocationData()
    this.getLabourData();
  }
  // getlocationData(){
  //   let url = this.baseurl + `/getAllLabourMaster?empId=${this.empids}`;
  // const tokens = this.token;
  // const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.get(url,{headers}).subscribe(data => {
    
  //     this.locationdata = data["result"];
  //       this.locationId=this.locationdata[0].locationId
  //       this.getLabourData();
  // },
  //   (err: HttpErrorResponse) => {
  
  //   })
  
  //     }


getLabourData() {
  debugger
  let url = this.baseurl + `/getAllLabourMaster/${this.locationIds}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.get(url, { headers }).subscribe(data => {
    
    this.LabourDatas = data['result']
    setTimeout(function () {

      $(function () {

        var table = $('#emptable').DataTable({

          retrieve: true,
          searching:false

       

        })

        // table
        // .order( [ 1, 'des' ] )
        // .draw();
        // $('.status-dropdown').on('change', function (e) {
        //   var status = $(this).val();
        //   $('.status-dropdown').val(status)
        //   console.log(status)
        //   table.column(6).search(status as AssignType).draw();
        // })


      });
    }, 100);
    this.Spinner.hide();

    // this.count1=this.manpowerAllData.length

  },
    // this.labourData=JSON.stringify(this.LabourData )
    // console.log("dsfasdfdsfsdfsdfsdafasdfsdaf",this.LabourData)

    (err: HttpErrorResponse) => {

    })

}

editLabour(e:any){

  this.labourlistEdit1='true'
 this.labourIds= e.labourMasterId
 this.labourperids=e.labourInfo.labourPerId
  sessionStorage.setItem("labourId1",this.labourIds);
  sessionStorage.setItem("labourId", this.labourperids)
  sessionStorage.setItem("iseditLabour1",'true')
  this.router.navigateByUrl('/layout/editcontractorLabour');


}
gotoDocument(e:any){
  debugger
  this.labourId=e.labourInfo.labourPerId;
  this.manReqStatusIde=e.manReqStatusId;
  sessionStorage.setItem("manRequestIds",this.manReqStatusIde)
  sessionStorage.setItem("LabourPerIdss",this.labourId)
  this.router.navigate(['/layout/originalPass'])
  
}
// deleteLabour(e:any){
  
//   this.labourId=e.labourPerId
//     let url = this.baseurl+`/deleteLabourInfoById/${this.labourId}`;
//     const tokens = this.token;
//     const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
//     this.httpservice.delete(url, { headers }).subscribe(data => {
//       this.toastr.success("Labour delete successfully")
//       this.getLabourData()
//     },(err: HttpErrorResponse) => {
//       this.toastr.error("error while delete labour")
// })

//   }
}


