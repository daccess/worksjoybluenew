import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { Http } from '@angular/http';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { RegularisationService } from '../regularisation/service/regularisation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { MessagingService } from '../shared/services/messaging.service';
import * as moment from 'moment';
import { dataService } from '../shared/services/dataService';


import swal from 'sweetalert';
@Component({
  selector: 'app-new-regularisation',
  templateUrl: './new-regularisation.component.html',
  styleUrls: ['./new-regularisation.component.css']
})
export class NewRegularisationComponent implements OnInit {
  user: string;
  users: any;
  token: any;
  baseurl = MainURL.HostUrl;
  manpowerData: any;
  locationdata: any;
  locationId: any;
  checkedManpowerRequest: boolean = false;
  getmanpowerId: any;
  trueElementsLength: any=0;
  count: any;
  manpowerSingleDetails: any;
  desigName: any;
  empids: any;
  manpowerid: string;
  manpowerData1: any;
  roleNames: any;
  supervisorEmpIds: any;
  router: any;
 constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private regService: RegularisationService,public toastr:ToastrService){
 this.getmanpowerId= sessionStorage.getItem("manPowerId")
 this.manpowerid=sessionStorage.getItem("manpowerids")
 console.log("sdfs",this.getmanpowerId)
 }
  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user);
    this.empids=this.users.roleList.empId
    this.token=this.users.token;
    this.locationId=this.users.roleList.locationId
    this.roleNames=this.users.roleList.roleName;
    this.supervisorEmpIds=this.users.roleList.contrctorEmpId
    this.getlocationData();
    this.getSingleManpowerDetails();

    this.getAllJobDescription()
  } 
  

getlocationData(){
  let url = this.baseurl + '/getAllLocations';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.get(url,{headers}).subscribe(data => {
   
    this.locationdata = data["result"];
      this.locationId=this.locationId
      // this.getallManpowerRequest();
},
  (err: HttpErrorResponse) => {

  })

    }

    getAllJobDescription(){
      let url = this.baseurl + `/getManpowerReqById/${this.manpowerid}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.get(url,{headers}).subscribe(data => {
       
        this.manpowerData1 = data["result"];

          // this.getallManpowerRequest();
    },
      (err: HttpErrorResponse) => {
    
      })
    
        }
    getSingleManpowerDetails(){
      let url = this.baseurl + `/getDesignationWiseLabour?desigId=${this.getmanpowerId}&empId=${this.empids}`;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpservice.get(url,{headers}).subscribe(data => {
          this.manpowerData = data["result"];
          setTimeout(function () {
            $(function () {
              var table;
              $(document).ready(function () {
                table = $("#emptable").DataTable({
                  pageLength: 10,
                  // dom: "lrtip",
                  destroy: true,
                  searching:true,
                  retrieve: true,
                });
    
               
              });
            });
          }, 1000);
    

        },
        (err: HttpErrorResponse) => {
        });

        if(this.roleNames=='Supervisor'){
          let url = this.baseurl + `/getDesignationWiseLabour?desigId=${this.getmanpowerId}&empId=${this.supervisorEmpIds}`;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpservice.get(url,{headers}).subscribe(data => {
          this.manpowerData = data["result"];
          setTimeout(function () {
            $(function () {
              var table;
              $(document).ready(function () {
                table = $("#emptable").DataTable({
                  pageLength: 10,
                  // dom: "lrtip",
                  destroy: true,
                  searching:true,
                  retrieve: true,
                });
    
               
              });
            });
          }, 1000);
    

        },
        (err: HttpErrorResponse) => {
        });
        }
      }
    
    checkAllManpowerRequest() {
    
      if (!this.checkedManpowerRequest) {
        for (let i = 0; i < this.manpowerData.length; i++) {
          this.manpowerData[i].check = true;
          
          const trueElements = this.manpowerData.filter(item => item.check == true);
this.trueElementsLength = trueElements.length;

        
        }
      }
      else {
        for (let i = 0; i < this.manpowerData.length; i++) {
          this.manpowerData[i].check = false;
          const trueElements = this.manpowerData.filter(item => item.check == false);
          this.trueElementsLength =0;
        }
      }
    }
    ManpowerRequestSingle: boolean = false;
  CheckAllManpowerRequestSingle() {
    for (let i = 0; i < this.manpowerData.length; i++) {
      if (this.manpowerData[i].check) {
        this.ManpowerRequestSingle = true;
        const trueElements = this.manpowerData.filter(item => item.check == true);
        this.trueElementsLength = trueElements.length;
        break
      }
      else {
        this.ManpowerRequestSingle = false;
        const trueElements = this.manpowerData.filter(item => item.check == false);
        this.trueElementsLength = trueElements.length-=1;
        if(trueElements.length){
          this.trueElementsLength=0
        }
      }
    }
  }
  approveManPower() {

    let selectedObj: any = new Array();
    for (let i = 0; i < this.manpowerData.length; i++) {

      if (this.manpowerData[i].check) {
        let obj = {

          labourPerId: this.manpowerData[i].labourPerId,
          manPowerReqId:this.manpowerData[i].manPowerReqId,
          locationId:this.manpowerData[i].locationMaster.locationId,
          conMasterId:this.manpowerData[i].contractorMaster.conMasterId

        }
        selectedObj.push(obj);
      }

    }

    this.Spinner.show();

    let obj={
      "manPowerReqId":this.manpowerid,
      "commonIdReqDtos":selectedObj
    }
    let url = this.baseurl + '/updateLabourActiveLHList';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj, { headers }).subscribe(
      data => {
        this.toastr.success('Labour Assign Successfully');
        this.Spinner.hide();
        // this.router.navigate(['/layout/main-manpower-request']);
        // this.router.navigateByUrl('/layout/employeelist/employeelist')
        // this.router.navigateByUrl('/layout/employeelist/mainManpowerRequest')
        setTimeout(() => {
          this.getSingleManpowerDetails();
          // Move the reload inside the success block
          window.location.reload();
        }, 1000);
        this.checkedManpowerRequest = false;
      },
      err => {
        this.Spinner.hide();
        this.toastr.error('Failed To Labour Assign Requisition');
      }
    );
    
  }
  


  }
