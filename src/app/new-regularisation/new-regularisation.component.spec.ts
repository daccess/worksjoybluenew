import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRegularisationComponent } from './new-regularisation.component';

describe('NewRegularisationComponent', () => {
  let component: NewRegularisationComponent;
  let fixture: ComponentFixture<NewRegularisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRegularisationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRegularisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
