import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { RegularisationComponent } from 'src/app/regularisation/regularisation.component';
import { NewRegularisationComponent } from './new-regularisation.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: NewRegularisationComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'newregularisation',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'newregularisation',
                    component: NewRegularisationComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const newrouting = RouterModule.forChild(appRoutes);