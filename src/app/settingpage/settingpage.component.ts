import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settingpage',
  templateUrl: './settingpage.component.html',
  styleUrls: ['./settingpage.component.css']
})
export class SettingpageComponent implements OnInit {
  changeColor : string = "customBlue";
  loggedUser: any = {};
  roleofuser: any;
  constructor(private router:Router) {
    if(sessionStorage.getItem('layOutThemeColor')){
      this.changeColor = sessionStorage.getItem('layOutThemeColor');
    }
    let user = sessionStorage.getItem('loggedUser');
    // this.loggedUser = JSON.parse(user);
    // this.roleofuser = this.loggedUser.mrollMaster.roleName;
   }

  ngOnInit() {
  }
  navigateRoute(){
this.router.navigateByUrl('layout/signatureAuthority')
  }

}
