import { Routes, RouterModule } from '@angular/router'


import { SettingpageComponent } from './settingpage.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: SettingpageComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'importemployee',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'rolemaster',
                    component: SettingpageComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);