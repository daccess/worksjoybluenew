import { routing } from './settingpage.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UiSwitchModule } from 'ngx-ui-switch';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';

import { SettingpageComponent } from './settingpage.component';


@NgModule({
    declarations: [
        SettingpageComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: []
  })
  export class SettingpageModule { 
      constructor(){

      }
  }
  