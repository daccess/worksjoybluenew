import { routing } from './applicationshistory.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { ApplicationshistoryComponent } from './applicationshistory.component';

@NgModule({
    declarations: [
        ApplicationshistoryComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: []
  })
  export class ApplicationshistoryModule { 
      constructor(){

      }
  }
  