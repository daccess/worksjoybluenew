import { Routes, RouterModule } from '@angular/router'
import { ApplicationshistoryComponent } from './applicationshistory.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ApplicationshistoryComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'applicationshistory',
                    pathMatch :'full'
                },
                {
                    path:'applicationshistory',
                    component: ApplicationshistoryComponent
                }
            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);