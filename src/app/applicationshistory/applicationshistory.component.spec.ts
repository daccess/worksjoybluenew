import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationshistoryComponent } from './applicationshistory.component';

describe('ApplicationshistoryComponent', () => {
  let component: ApplicationshistoryComponent;
  let fixture: ComponentFixture<ApplicationshistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationshistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationshistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
