import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ChangePassService } from '../shared/services/ChangPassService'
import { chngePassModel } from '../shared/model/ChangePassModel';
import { from } from 'rxjs';

//import { MainURL } from '../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CdkAccordion } from '@angular/cdk/accordion';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  changePass:chngePassModel
  empPerId: any;
  msg: any;
  UserId: any;
  flag:boolean;
  Password: any;
  Pass: any;
  compId: any;
  empOfficialId: any;
  loggedUser: any;
  constructor(public httpService: HttpClient, 
    public ChangePass: ChangePassService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService) {
      // this.compId =  this.loggedUser.compId
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empOfficialId = this.loggedUser.empOfficialId;
      this.empPerId = this.loggedUser.empPerId
      this.changePass = new chngePassModel();
     }

  ngOnInit() {
   this.flag=false
   this.chRef.detectChanges();

  }

  onSubmit(){
    this.postnewPass()
    this.toastr.success("password chnge successfully");
  }

 
  onBlurMethod(){
    this.changePass.empPerId=this.empPerId;
    this.ChangePass.postchangePass(this.changePass).subscribe(data => {
       this.msg=data['result']['loginMsg']
       this.chRef.detectChanges();
       if(this.msg=='Correct Password'){
         this.flag=true;
         this.chRef.detectChanges();
       }else{
        this.flag=false;
       }
       this.chRef.detectChanges();
       this.UserId=data['result']['userId']
     this.toastr.info(this.msg);
     // this.getactivecompany()
    }
    ,(err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error');
    });
   
  }
 obj(event){
  this.Pass=event.target.value
  }
  postnewPass(){
    this.changePass.userId=this.UserId
    this.changePass.passWord=this.Pass;
    this.ChangePass.postnewPass(this.changePass).subscribe(data => {
    },(err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error');
    });
  }
  eyeFlagc : boolean = false
  seePasswordo() {
    var x: any = document.getElementById("Old");
    if (x.type === "password") {
      this.eyeFlagc = true;
      x.type = "text";
    } else {
      this.eyeFlagc = false;
      x.type = "password";
    }
  }

  eyeFlago : boolean = false
  seePasswordc() {
    var x: any = document.getElementById("newpas");
    if (x.type === "password") {
      this.eyeFlago = true;
      x.type = "text";
    } else {
      this.eyeFlago = false;
      x.type = "password";
    }
  }

  eyeFlagn : boolean = false
  seePasswordn() {
    var x: any = document.getElementById("conpas");
    if (x.type === "password") {
      this.eyeFlagn = true;
      x.type = "text";
    } else {
      this.eyeFlagn = false;
      x.type = "password";
    }
  }

  }
  


