import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';

import { ChangepasswordComponent } from 'src/app/changepassword/changepassword.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ChangepasswordComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'changepassword',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'changepassword',
                    component: ChangepasswordComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);