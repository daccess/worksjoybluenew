import { Component, OnInit ,OnDestroy} from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { departmentMasterService } from '../shared/services/departmentmasterservice';
import { RoleMasterService } from '../shared/services/RoleMasterService';

@Component({
  selector: 'app-editcontractorlabour',
  templateUrl: './editcontractorlabour.component.html',
  styleUrls: ['./editcontractorlabour.component.css']
})
export class EditcontractorlabourComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  empId: any;
  firstName: any;
  middleName: any;
  lastName: any;
  gender: any
  bloodGroup: any
  contactNumber: any
  emergencyContactNo: any
  
  tempAddress: any;
  permanentAddress: any
  maritalStatus: any
  aadharNo: any
  panNumber: any
  voterCardNo: any
  user: string;
  users: any;
  token: any;
  empIds: any;
  imageUrl: string;
  employeeName: any;
  employeelastname: any;
  employeeFullName: string;
  DesignationList: any;
  getallDepartment: any;
  allLocations: any;
  fileToUpload: File = null;
  obj1: any = {};
  obj2: any = {};
  obj4: any = {}
  obj3: any = {};
  obj: any = {};
  aadharImg: any;
  experienceLetter: any;
  panCardImg: any;
  receiptUrl1: any;
  receiptUrl4: any
  policeVerificationDoc: any;
  receiptUrl3: any
  receiptUrl2: any;
  imagevalidation1: any;
  imagevalidation2: string;
  imagevalidation3: string;
  imagevalidation4: string;
  Allassets: any;
  issuedReason: any;
  issuedate: any;
  labourMasterId: any;
  accurrenceDetails:any;
  assetHistories: any[] = [];
  assetsname: any;
  penaltyAmount:any;
  currentAge: number;
  /*new variable here*/
  isChecked:boolean=false;
  pfNumber: any
  esicNumber: any
  wcNumber: any
  dateOfOccurence:any
  policeVerificationDocExpiryDate: any
  aadharUploadDate: any
  panUpdateDate: any
  experienceUpdateDate: any
  previousEmployer: any
  jobTitle: any
  wage: any
  workedStartedDate: any
  workedCompletedDate: any
  jobType: any
  jobDuties: any
  employmentType: any
  toolUsed: any;
  reasonForLeaving: any
  
  emailId: any;
  workHistoryList: any = [];
  dateOfBirth: any;
  deptId: any;
  locationId: any;
  desigId: any;
  workSkill: any;
  contractStartDate:any;
  contractEndDate: any;
  reportingManager: any;
  paymentMode: any;
  busTransport: any;
  conMasterId: any;
  assetId: any;
  roleId: any;
  conEmpId: any;
  userRoleList: any;
  userRoleLists: any;
  labourID: any;
  allLabourDataEdit: any;
  documentMaster:any={};
  getSingleLabourList: any;
  labourUpdate: any;
  receiptUrl: any;
  imagevalidation: string;
  labourID1: any;
  labourUpdate1: string;
  allSalaryDatas: any;
  salaryStructureID: any;
  salaryDetailsData: any;
  salaryData: any;
  salaryName: any;
  totalEarning: any;
  totalDeduction: any;
  netpayeble: any;
  fresher: any;
  TypeOfMissConduct: any;
  MissconductDate: any;
  Details: any;
  MissconductType: any;
  diciplinaryActions: any=[];
  idCounter: any=1;

  constructor(public httpservice: HttpClient, public toastr: ToastrService,public departmentmasterservice:departmentMasterService,public roleservice:RoleMasterService) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token
    this.empIds = this.users.role;
    this.conEmpId=this.users.roleList.empId
    this.employeeName = this.users.roleList.firstName;
    this.employeelastname = this.users.roleList.lastName;
    // this.employeeFullName = this.employeeName + " " + this.employeelastname
    this.getDesignation()
    this.getDepartment();
    this.getAllLocation();
    this.getAllassets()
    this.getLabourID()
    this.getallroleid()
    this.getAllSalaryStructures()
    this.labourID=sessionStorage.getItem("labourId")
    this.labourID1=sessionStorage.getItem("labourId1")
   
    // this.labourUpdate=sessionStorage.getItem("iseditLabour")
    this.labourUpdate1=sessionStorage.getItem("iseditLabour1")
  
    if(this.labourUpdate1=='true'){
      
    this.getLabourById1();
    }
  }
  ngOnDestroy() {

    sessionStorage.removeItem("labourId")
  }
  getDesignation() {
    let url = this.baseurl + '/getAllDesignations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.DesignationList = data['result']



    },
      (err: HttpErrorResponse) => {

      })

  }

  getDepartment() {
    let url = this.baseurl + '/getAllDepartments';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
    
      this.getallDepartment = data['result']



    },
      (err: HttpErrorResponse) => {

      })

  }
  getAllSalaryStructures() {
    let url = this.baseurl + '/getSalaryStructure';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.allSalaryDatas = data['result']



    },
      (err: HttpErrorResponse) => {

      })

  }
  getAllLocation() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.allLocations = data['result']



    },
      (err: HttpErrorResponse) => {

      })

  }
  getAllassets() {

    let url = this.baseurl + '/getAllAssets';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
    
      this.Allassets = data['result']
    },
      (err: HttpErrorResponse) => {

      })

  }
  getLabourID() {
    
    let url = this.baseurl + '/getMaxLabourId';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      console.log("shafds",data)
      
  this.labourMasterId=data;
  this.empId=this.labourMasterId.result.labourEmpId
    },
      (err: HttpErrorResponse) => {

      })

  }
  getallroleid(){
   
    this.roleservice.GetAllRoles1(this.token).subscribe(data=>{
      // this.allRolesData=data['result'];
      console.log("this is the data",data)
      
      this.userRoleLists=data.result;
      
      this.roleId=this.userRoleLists.userRoleList[0].roleId
      console.log("testing",this.roleId)
    
  
  })
}
  getassetsname(e: any) {
  
    this.assetsname = e.target.assetName
  }
  addAssets() {
    
    let obj: any = {
      "issuedReason": this.issuedReason,
      "issuedate": this.issuedate,
      "assetId": this.assetId,
      "assetsName": this.assetsname
    }
    this.assetHistories.push(obj)
    console.log("sdfasdf", this.assetHistories)
    obj = ''
  }

  handleFileInput1(file: FileList) {
  
    this.fileToUpload = file.item(0);
    this.obj1.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.aadharImg = event.target.result.substring(0, 10);;
      this.receiptUrl1 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.aadharImg = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation1 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation1 = "true"
        }
      }, 300);
    }
  }
  handleFileInput2(file: FileList) {
    
    this.fileToUpload = file.item(0);
    this.obj2.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.panCardImg = event.target.result.substring(0, 10);;
      this.receiptUrl2 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.panCardImg = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation2 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation2 = "true"
        }
      }, 300);
    }
  }

  handleFileInput(files: FileList) {
    const maxSizeInBytes = 1 * 1024 * 1024; // 1MB
    var flag1 = 'false';    
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      if (file.size > maxSizeInBytes) {
        console.log(`File ${file.name} exceeds 1MB`);
        // Additional actions, such as displaying an error message, can be performed here
        flag1 = 'false'
      } else {
        console.log(`File ${file.name} is valid`);
        flag1 = 'true'  
        // Further processing, such as uploading the image, can be done here
      }
    }
    this.fileToUpload = files.item(0);
    this.obj.name = files.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
      this.receiptUrl = event.target.result
    }
    reader.readAsDataURL(this.fileToUpload);
    // let flag1 = false;
    if (files) {
      var img = new Image();
      img.src = window.URL.createObjectURL(files[0]);
      setTimeout(() => { 
        if (flag1=='true') {
this.imagevalidation="false"
          this.fileToUpload = files.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.imageUrl = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
        }
        else {
          // this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation="true"
        }
        console.log("this is image validation status",this.imagevalidation)
      }, 300);
    }
    
  }

  handleFileInput4(file: FileList) {
  
    this.fileToUpload = file.item(0);
    this.obj4.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.experienceLetter = event.target.result.substring(0, 10);;
      this.receiptUrl4 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.experienceLetter = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation4 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation4 = "true"
        }
      }, 300);
    }
  }
  AddworkHistoryList() {
    let obje: any = {
      "previousEmployer": this.previousEmployer,
      "jobTitle": this.jobTitle,
      "wage": this.wage,
      "workedStartedDate": this.workedStartedDate,
      "workedCompletedDate": this.workedCompletedDate,
      "jobType": this.jobType,
      "jobDuties": this.jobDuties,
      "employmentType": this.employmentType,
      "toolUsed": this.toolUsed,
      "reasonForLeaving": this.reasonForLeaving,
    }
    this.workHistoryList.push(obje)
    obje = ''
  }
  addDocumenthere(){
  let obj={

    "aadharImg": this.aadharImg,
    "panCardImg": this.panCardImg,
    "policeVerificationDoc": this.policeVerificationDoc,
    "experienceLetter": this.experienceLetter,
    "policeVerificationDocExpiryDate": this.policeVerificationDocExpiryDate,
    // "aadharUploadDate":
    // "panUpdateDate":"2023-06-20",
    // "experienceUpdateDate":"2023-06-20"
  }
  this.documentMaster.push(obj)
  
}

  // saveContractorLabours() {
  
  //   let obj = {
  //     "roleId":this.roleId,
  //     "empId": this.empId,
  //     "firstName": this.firstName,
  //     "middleName": this.middleName,
  //     "lastName": this.lastName,
  //     "gender": this.gender,
  //     "bloodGroup": this.bloodGroup,
  //     "contactNumber": this.contactNumber,
  //     "emergencyContactNo": this.emergencyContactNo,
  //     "emailId": this.emailId,
  //     "tempAddress": this.tempAddress,
  //     "permanentAddress": this.permanentAddress,
  //     "maritalStatus": this.maritalStatus,
  //     "aadharNo": this.aadharNo,
  //     "panNumber": this.panNumber,
  //     "voterCardNo": this.voterCardNo,
  //     "pfNumber": this.pfNumber,
  //     "esicNumber": this.esicNumber,
  //     "wcNumber": this.wcNumber,
  //     "documentMaster": this.documentMaster,
  //     "workHistoryList": this.workHistoryList,
  //     "assetHistories":this.assetHistories,
  //     "dateOfBirth": this.dateOfBirth,
  //     "conEmpId": this.conEmpId,
  //     "deptId": this.deptId,
  //     "locationId": this.locationId,
  //     "desigId": this.desigId,
  //     "workSkill": this.workSkill,
  //     "contractStartDate": this.contractStartDate,
  //     "contractEndDate": this.contractEndDate,
  //     "reportingManager": this.reportingManager,
  //     "paymentMode": this.paymentMode,
  //     "busTransport": this.busTransport,
  //     "labourImage":this.imageUrl,
  //     "age":this.currentAge



  //   }
  //   let url = this.baseurl + '/labourDetailsSave';
  //   const tokens = this.token;
  //   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.post(url,obj, { headers }).subscribe(data => {
  //     this.toastr.success("Contractor's Labour Inserted Successfully")
  //     this.allLocations = data['result']



  //   },
  //     (err: HttpErrorResponse) => {
  //       this.toastr.error("server side Error while add Contractor Labour")

  //     })
  //   }
    
  
  updatecontractor(){
    let obj11 = {
      "roleId":this.roleId,
      "empId": this.empId,
      "firstName": this.firstName,
      "middleName": this.middleName,
      "lastName": this.lastName,
      "gender": this.gender,
      "bloodGroup": this.bloodGroup,
      "contactNumber": this.contactNumber,
      "emergencyContactNo": this.emergencyContactNo,
      "emailId": this.emailId,
      "tempAddress": this.tempAddress,
      "permanentAddress": this.permanentAddress,
      "maritalStatus": this.maritalStatus,
      "aadharNo": this.aadharNo,
      "panNumber": this.panNumber,
      "voterCardNo": this.voterCardNo,
      "pfNumber": this.pfNumber,
      "esicNumber": this.esicNumber,
      "wcNumber": this.wcNumber,
      "documentMaster":this.documentMaster,
      "workHistoryList": this.workHistoryList,
      "assetHistories":this.assetHistories,
      "dateOfBirth": this.dateOfBirth,
      "conEmpId": this.conEmpId,
      "deptId": this.deptId,
      "locationId": this.locationId,
      "desigId": this.desigId,
      "workSkill": this.workSkill,
      "contractStartDate": this.contractStartDate,
      "contractEndDate": this.contractEndDate,
      "reportingManager": this.reportingManager,
      "paymentMode": this.paymentMode,
      "busTransport": this.busTransport,
      // "labourMasterId":this.labourMasterId,
      "labourImage":this.imageUrl,
      "age":this.currentAge,
      "labourPerId":this.labourID,
      "labourMasterId":this.labourID1,
      "salaryStructureID":this.salaryStructureID,
      "fresher":this.fresher,
      "diciplinaryActions":this.diciplinaryActions


    }
    let url = this.baseurl + '/updateLabourDetailsSave';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url,obj11, { headers }).subscribe(data => {
      this.toastr.success("Contractor's Labour updated Successfully")
    
      // this.allLocations = data['result']
  },
      (err: HttpErrorResponse) => {
        this.toastr.error("server side Error while update Contractor Labour")

      })
    }
  editAssets(c, b) {
    this.assetId = c.assetId;
    this.issuedate = c.issuedate;
    this.issuedReason=c.issuedReason;
 
  }
  ediExperience(c, b) {
    
    this.previousEmployer=c.previousEmployer;
    this.jobTitle=c.jobTitle;
    this.wage=c.wage;
  this.workedStartedDate=c.workedStartedDate;
  this.workedCompletedDate=c.workedCompletedDate;
    this.jobType=c.jobType;
    this.jobDuties=c.jobDuties;
    this.employmentType=c.employmentType;
  this.toolUsed=c.toolUsed;
    this.reasonForLeaving=c.reasonForLeaving;
  }
  subwork:any
  indexes:any
  deletework(data, i){
    this.subwork = data;
    this.indexes = i;
  }
deleteworks(){
  this.workHistoryList.splice(this.indexes, 1);

}
  subWeek: any;
  indexx: any
  deleteAset(data, i) {
    this.subWeek = data;
    this.indexx = i;
  }
  
  deleteAssets() {
    this.assetHistories.splice(this.indexx, 1);
  }
  isedit:boolean=false
 
  getLabourById1() {
    // if(this.labourUpdate=='true'){
    //   debugger
    //   this.isedit=true
    // }
    let url = this.baseurl + `/getByIdLabourMaster/${this.labourID1}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
        console.log('Received data:', data); 
      this.isedit=true
      this.getSingleLabourList = data;
      console.log('getSingleLabourList==', this.getSingleLabourList);
      
      this.allLabourDataEdit=this.getSingleLabourList.result;
      this.employeeFullName=this.getSingleLabourList.result.contractorMaster.firstName
      console.log("duplicate==",this.allLabourDataEdit)
      console.log("name is here", this.getSingleLabourList.result.contractorMaster.firstName)
      console.log("name is kskkskskskskks", this.allLabourDataEdit.contractorMaster.firstName)
      
          
    this.roleId=this.roleId
    this.labourMasterId=this.allLabourDataEdit.labourInfo.labourMasterId
    this.assetHistories=this.allLabourDataEdit.assetHistories;
    this.salaryStructureID=this.allLabourDataEdit.salaryStructure.salaryStructureID
    this.getSalaryDetails(this.salaryStructureID)
    this.workHistoryList=this.allLabourDataEdit.labourInfo.workHistoryList;
    this.documentMaster=this.allLabourDataEdit.labourInfo.documentMaster;
      this.empId=this.allLabourDataEdit.empId
       this.firstName=this.allLabourDataEdit.labourInfo.firstName
       this.middleName=this.allLabourDataEdit.labourInfo.middleName;
       this.lastName=this.allLabourDataEdit.labourInfo.lastName;
       this.gender=this.allLabourDataEdit.labourInfo.gender;
       this.bloodGroup=this.allLabourDataEdit.labourInfo.bloodGroup;
       this.contactNumber=this.allLabourDataEdit.labourInfo.contactNumber
       
       this.emergencyContactNo=this.allLabourDataEdit.labourInfo.emergencyContactNo
       this.emailId=this.allLabourDataEdit.labourInfo.emailId
       this.tempAddress=this.allLabourDataEdit.labourInfo.tempAddress;
      this.permanentAddress=this.allLabourDataEdit.labourInfo.permanentAddress;
       this.maritalStatus=this.allLabourDataEdit.labourInfo.maritalStatus;
      this.aadharNo=this.allLabourDataEdit.labourInfo.aadharNo;
    this.panNumber=this.allLabourDataEdit.labourInfo.panNumber;
      this.voterCardNo=this.allLabourDataEdit.labourInfo.voterCardNo;
       this.pfNumber=this.allLabourDataEdit.labourInfo.pfNumber;
      this.esicNumber=this.allLabourDataEdit.labourInfo.esicNumber;
       this.wcNumber=this.allLabourDataEdit.labourInfo.wcNumber;
      this.dateOfBirth=this.allLabourDataEdit.labourInfo.dateOfBirth;
      this.conEmpId=this.allLabourDataEdit.contractorMaster.empId;
      this.deptId=this.allLabourDataEdit.departmentMaster.deptId;
      this.locationId=this.allLabourDataEdit.locationMaster.locationId;
      this.desigId=this.allLabourDataEdit.labourInfo.designationMaster.desigId;
      this.workSkill=this.allLabourDataEdit.workSkill;
      this.contractStartDate=this.allLabourDataEdit.contractStartDate;
      this.contractEndDate=this.allLabourDataEdit.contractEndDate;
      this.reportingManager=this.allLabourDataEdit.reportingManager;
      this.paymentMode=this.allLabourDataEdit.paymentMode;
      this.busTransport=this.allLabourDataEdit.busTransport;
      this.fresher=this.allLabourDataEdit.fresher;
this.employeeFullName=this.allLabourDataEdit.contractorMaster.firstName;
this.diciplinaryActions=this.allLabourDataEdit.diciplinaryActionResDtos

    },
      (err: HttpErrorResponse) => {

      })

  }
  getSalaryDetails(e:any){
   
    this.salaryStructureID=e;
    let url = this.baseurl + `/getSalaryStructure/${this.salaryStructureID}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.salaryDetailsData = data
      this.salaryData=this.salaryDetailsData.result;
      this.salaryName=this.salaryData.nameOfStructure;
      this.totalEarning=this.salaryData.totalEarningAmount;
      this.totalDeduction=this.salaryData.totalDeductionAmount;
      this.netpayeble=this.salaryData.netPayable;



    },
      (err: HttpErrorResponse) => {

      })


  }
  AddDisciplinaryAction(){
    let obj={
      "id":this.idCounter++,
    "TypeOfMissConduct":this.TypeOfMissConduct,
    "MissconductDate":this.MissconductDate,
    "Details":this.Details,
    "MissconductType":this.MissconductType,

    }
    this.diciplinaryActions.push(obj)
    console.log("this is the console data ", this.diciplinaryActions)
    this.idCounter=null;
    this.TypeOfMissConduct=null;
    this.MissconductDate=null;
    this.Details=null;
    this.MissconductType=null
  }



  calculateAge() {
    const birthDate = new Date(this.dateOfBirth);
    const currentDate = new Date();
    
    const yearDiff = currentDate.getFullYear() - birthDate.getFullYear();
    const monthDiff = currentDate.getMonth() - birthDate.getMonth();
    
    // Adjust age if birth month hasn't occurred yet this year
    if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthDate.getDate())) {
      this.currentAge = yearDiff - 1;
    } else {
      this.currentAge = yearDiff;
    }
  }
  deleteRecords(id: number){
    // this.adddataInTable.splice(index, 1);
    const index = this.diciplinaryActions.findIndex(item => item.id === id);
    if (index !== -1) {
      this.diciplinaryActions.splice(index, 1);
    }
  }
  onCheckboxChange() {
    console.log("Checkbox value:", this.isChecked);
    this.fresher=this.isChecked
  }
}