import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditcontractorlabourComponent } from './editcontractorlabour.component';

describe('EditcontractorlabourComponent', () => {
  let component: EditcontractorlabourComponent;
  let fixture: ComponentFixture<EditcontractorlabourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditcontractorlabourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditcontractorlabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
