import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShiftwiseAbsentismComponent } from '../hrdashboard/charts/shiftwise-absentism/shiftwise-absentism.component';
import { CoffchartComponent } from '../hrdashboard/charts/coffchart/coffchart.component';
import { AbsentismchartComponent } from '../hrdashboard/charts/absentismchart/absentismchart.component';
import { Apsentismchart2Component } from '../hrdashboard/charts/apsentismchart2/apsentismchart2.component';
import { SkilledchartComponent } from '../hrdashboard/charts/skilledchart/skilledchart.component';
import { FtechartComponent } from '../hrdashboard/charts/ftechart/ftechart.component';
import { AbsentismtrendchartComponent } from '../hrdashboard/charts/absentismtrendchart/absentismtrendchart.component';
import { ThreedaysabsentismchartComponent } from '../hrdashboard/charts/threedaysabsentismchart/threedaysabsentismchart.component';
import { CoffsupervsorchartComponent } from '../hrdashboard/charts/coffsupervsorchart/coffsupervsorchart.component';
import { ShiftwiseabsentismsupervisorComponent } from '../hrdashboard/charts/shiftwiseabsentismsupervisor/shiftwiseabsentismsupervisor.component';
import { TotalheadcountchartComponent } from '../hrdashboard/charts/totalheadcountchart/totalheadcountchart.component';
import { PlanvsactulachartComponent } from '../hrdashboard/charts/planvsactulachart/planvsactulachart.component';
import { LateEarlyLateChartComponent } from '../hrdashboard/charts/late-early-late-chart/late-early-late-chart.component';
import { ShiftdeviationchartComponent } from '../hrdashboard/charts/shiftdeviationchart/shiftdeviationchart.component';
import { DepartmentwisemanhrsComponent } from '../hrdashboard/charts/departmentwisemanhrs/departmentwisemanhrs.component';
import { OthrsincreaseunplannedComponent } from '../hrdashboard/charts/othrsincreaseunplanned/othrsincreaseunplanned.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
        NgxChartsModule,
        
        NgCircleProgressModule.forRoot({
   
        })
  ],
  declarations: [
    ShiftwiseAbsentismComponent,
        CoffchartComponent,
        AbsentismchartComponent,
        Apsentismchart2Component,
        SkilledchartComponent,
        FtechartComponent,
        AbsentismtrendchartComponent,
        ThreedaysabsentismchartComponent,
        CoffsupervsorchartComponent,
        ShiftwiseabsentismsupervisorComponent,
        TotalheadcountchartComponent,
        PlanvsactulachartComponent,
        LateEarlyLateChartComponent,
        ShiftdeviationchartComponent,
        DepartmentwisemanhrsComponent,
        OthrsincreaseunplannedComponent
  ],
  exports : [
        ShiftwiseAbsentismComponent,
        CoffchartComponent,
        AbsentismchartComponent,
        Apsentismchart2Component,
        SkilledchartComponent,
        FtechartComponent,
        AbsentismtrendchartComponent,
        ThreedaysabsentismchartComponent,
        CoffsupervsorchartComponent,
        ShiftwiseabsentismsupervisorComponent,
        TotalheadcountchartComponent,
        PlanvsactulachartComponent,
        LateEarlyLateChartComponent,
        ShiftdeviationchartComponent,
        DepartmentwisemanhrsComponent,
        OthrsincreaseunplannedComponent,
        
        // ChartsModule,
        // NgxChartsModule,
        
        // NgCircleProgressModule.forRoot({
   
        // })
  ]
})
export class HrdashboardsharedModule { }
