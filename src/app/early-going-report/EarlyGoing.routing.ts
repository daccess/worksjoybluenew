import { Routes, RouterModule } from '@angular/router';
import { EarlyGoingReportComponent } from './early-going-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: EarlyGoingReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'earlygoingreports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'earlygoingreports',
                    component: EarlyGoingReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);