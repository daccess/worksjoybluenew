import { routing } from './employeelist.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule, DatePipe } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { EmployeelistComponent } from './employeelist.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { SearchPipeForEmpleave } from '../leaverequest/SearchPipe/Serach';


@NgModule({
    declarations: [
        EmployeelistComponent,
        // SearchPipeForEmpleave
    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgbModule
    ],
    providers: [DatePipe]
  })
  export class EmployeelistModule { 
      constructor(){

      }
  }
  