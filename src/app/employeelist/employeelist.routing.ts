import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { EmployeelistComponent } from './employeelist.component'


const appRoutes: Routes = [
    { 
             
        path: '', component: EmployeelistComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'employeelist',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'employeelist',
                    component: EmployeelistComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);