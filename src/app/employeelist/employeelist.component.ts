import { Component,OnInit } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import "rxjs/add/operator/map";
import { ChangeDetectorRef } from "@angular/core";
import { MainURL } from "../shared/configurl";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { ExcelService } from "../absentreport/excel.service";
import { NgxSpinnerService } from "ngx-spinner";
import { InfoService } from "../shared/services/infoService";
import { DomSanitizer } from "@angular/platform-browser";
import { ToastrService } from "ngx-toastr";
export type asignColType = "number" | "string";
import {dataService} from '../shared/services/dataService';

@Component({
  selector: "app-employeelist",
  templateUrl: "./employeelist.component.html",
  styleUrls: ["./employeelist.component.css"],
  providers: [ExcelService],
})
export class EmployeelistComponent implements OnInit{
  Inputvalue: any;
  selected_number: any;
  selected: any = [];
  baseurl = MainURL.HostUrl;
  AssetsPath = MainURL.AssetsPath;
  Aws_flag = MainURL.Aws_flag;
  Employeedata = [];
  compId: any;
  returnUrl: string;
  dataEmp: any;
  EmpID: any;
  loggedUser: any;
  excelData: any;
  empActiveListType: string = "active";
  contractorActiveListType: string = "active";
  empOfficialId: any;
  EmployeeImportdata: any;
  employeeListType: string = "employee";
  empPerId: any;
  CONTRACTOR: any;
  lengthOfPriboard: number;
  empSearchSuggestionList = [];
  orgEmpList = [];
  flagPreon: boolean = false;
  dataTableFlag3: boolean;
  dataTableFlagNewPreon: boolean;
  public dataTablepreOn: boolean;
  EmployeedataDepartment = [];
  // public listType : string = "employee";
  listType: any='employee';
  public empdata: boolean;
  priOnBoard = [];
  contractor_data = [];

  received_message: any;
  dtOptions: any = {};
  EmployeedataDepartmentbyid: any[];
  EmployeedataDesignation: any[];
  EmployeedataFirmName:any[]
  EmployeedataDesignationbyid: any[];
  EmployeeCostCenter: any[];
  EmployeeCostCenterById: any[];
  EmployeeTypes: any[];
  EmployeeTypeCenterById: any[];
  EmploymentTypeById: any[];
  EmployeeGrade: any[];
  EmployeeGradeTypeById: any[];
  EmployeeEmailById: any[];
  EmployeebioById: any[];
  EmployeeSearchByContact: any[];
  EmployeeEmailById1: any[];
  EmployeebioById1: any[];
  EmployeeSearchByContact1: any[];
  EmployeedataDepartmentbyid1: any[];
  EmployeedataDesignationbyid1: any[];
  EmployeeCostCenterById1: any[];
  EmployeeTypeCenterById1: any[];
  EmploymentTypeById1: any[];
  EmployeeGradeTypeById1: any[];
  Employeedatas: any;
  AllLocationwisedata: any[];
  rolName: any;
  rollName: any;
  EmployeeLocationwise: any[];
  AllLocationwisedatas: any[];
  EmployeedataDesignationbyid2: any[];
  selectedLocationId: any=0;
  locationids: any=0;
 
  constructor(
    public httpService: HttpClient,
    public chRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    public excelService: ExcelService,
    public Spinner: NgxSpinnerService,
    public InfoService: InfoService,
    private sanitizer: DomSanitizer,
    private toastr: ToastrService,
    private dataService: dataService
  ) {
    
    this.listType= "employee";
   
    this.empdata = true;
    let user = sessionStorage.getItem("loggedUser");
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empPerId = this.loggedUser.empPerId;
    
    this.rollName=this.loggedUser.mrollMaster.roleName

    //set active tabs
    this.Employeedata = this.EmployeedataDepartmentbyid;

    this.setCurrentActiveTab();

    $("#second-dropdown").hide();
    $("#searchhide").hide();

  }

  goToPage(i) {
    let key = i.fullName + " " + i.lastName;
    let id = i.empPerId;
    sessionStorage.setItem("data", key);
    sessionStorage.setItem("id", id);
    if (this.listType == "contractor") {
      this.InfoService.changeMessage("contractor");
    }
    if (this.listType == "employee") {
      this.InfoService.changeMessage("employee");
    }
    this.router.navigateByUrl("/layout/deactivateemployee");
  }
  ngOnInit() {
    // this.getEmployeeData();
    this.returnUrl =
      this.route.snapshot.queryParams["returnUrl"] || "/layout/viewempdetails";
    
      $("#searchhide").hide();
      $("#SearchContractorFirmName").hide()
      $("#Searchcolumwise").hide();
      $("#second-dropdown").hide();
      $("#third-dropdown").hide();
      $("#fourth-dropdown").hide();
      $("#fifth-dropdown").hide();
      $("#sixth-dropdown").hide();
      $("#seven-dropdown").hide();
      $("#eight-dropdown").hide();
      $("#search-button-forbiometricid").hide();
      $("#search-button-forContactNumber").hide();
      $("#search-button-foremail").hide();
      //here bydefault hide all search box and dropdown  box for employee list
  }


  transform(path) {
    let temp = "data:image/png;base64," + path;
    return this.sanitizer.bypassSecurityTrustResourceUrl(temp);
  }
  getEmployeeData() {
    
    // this.Spinner.show();
    this.Employeedata = [];
    if(this.loggedUser.roleHead=='Admin'){
      let url = "/EmployeeMasterOperation/";

      this.dataService.createRecors(url+this.loggedUser.compId).subscribe(
        (data) => {
          this.Employeedatas = data["result"];
          for(let i=0; i<this.Employeedatas.length;i++){
            
         if(this.Employeedatas[i].employmentType=='On Roll'){
           this.Employeedata.push(this.Employeedatas[i])
           this.orgEmpList =  this.Employeedata;
         }
          }
          // this.Employeedata = data["result"];
        
          // this.orgEmpList = data["result"];
          //this.Employeedata=this.EmployeedataDepartment;
  
          this.Spinner.hide();
          $("#second-dropdown").hide();
          $("#searchhide").hide();
          $("#third-dropdown").hide();
          $("#fourth-dropdown").hide();
          $("#fifth-dropdown").hide();
          $("#sixth-dropdown").hide();
          $("#search-button-foremail").hide();
          $("#seven-dropdown").hide();
          $("#eight-dropdown").hide();
          $("#search-button-forbiometricid").hide();
          $("#search-button-forContactNumber").hide();
  
          setTimeout(function () {
            $(function () {
              var table;
  
              $(document).ready(function () {
                table = $("#emptable").DataTable({
                  pageLength: 10,
                  dom: "lrtip",
                  destroy: true,
  
                  retrieve: true,
                });
  
                var column_no = 2;
  
                $("#ddlSearch").on("change", function () {
                  column_no = Number($(this).val());
                  if (
                    column_no == 1 ||
                    column_no == 2 ||
                    column_no == 4 ||
                    column_no == 5
                  ) {
                    $("#searchhide").show();
                    $("#second-dropdown").hide();
                  } else if (
                    column_no == 6 ||
                    column_no == 7 ||
                    column_no == 3 ||
                    column_no == 8 ||
                    column_no == 9 ||
                    column_no == 10
                  ) {
                    $("#searchhide").hide();
                    $("#second-dropdown").show();
                  } else {
                    $("#searchhide").hide();
                    $("#second-dropdown").hide();
                    $("#third-dropdown").hide();
                    $("#fourth-dropdown").hide();
                  }
                });
  
                $("#txtSearch").on("input", function () {
                  if (
                    table.columns([column_no]).search() !== $("#txtSearch").val()
                  ) {
                    table
                      .columns([column_no])
                      .search($("#txtSearch").val())
                      .draw();
                  }
                });
              
              });
            });
          }, 1000);
          this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
         
          $("#second-dropdown").hide();
          $("#searchhide").hide();
          $("#third-dropdown").hide();
          $("#fourth-dropdown").hide();
          $("#fifth-dropdown").hide();
          $("#sixth-dropdown").hide();
          $("#search-button-foremail").hide();
          $("#seven-dropdown").hide();
          $("#eight-dropdown").hide();
          $("#search-button-forbiometricid").hide();
          $("#search-button-forContactNumber").hide();
  
        }
      );
    
    }
    else{
    let url = "/EmployeeMasterOperationLite/";
    let obj = {
      "empId": this.loggedUser.empId,
      "locationId":this.loggedUser.locationId,
      "roleId": this.loggedUser.rollMasterId,
      "compId": this.compId
    }

    // this.httpService.get(url + obj).subscribe(
      this.dataService.createRecord(url , obj).subscribe(
      (data) => {
        this.Employeedata = data["result"];
        this.orgEmpList = data["result"];
        //this.Employeedata=this.EmployeedataDepartment;

        this.Spinner.hide();
        $("#second-dropdown").hide();
        $("#searchhide").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#search-button-foremail").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();

        setTimeout(function () {
          $(function () {
            var table;

            $(document).ready(function () {
              table = $("#emptable").DataTable({
                pageLength: 10,
                dom: "lrtip",
                destroy: true,

                retrieve: true,
              });

              var column_no = 2;

              $("#ddlSearch").on("change", function () {
                column_no = Number($(this).val());
                if (
                  column_no == 1 ||
                  column_no == 2 ||
                  column_no == 4 ||
                  column_no == 5
                ) {
                  $("#searchhide").show();
                  $("#second-dropdown").hide();
                } else if (
                  column_no == 6 ||
                  column_no == 7 ||
                  column_no == 3 ||
                  column_no == 8 ||
                  column_no == 9 ||
                  column_no == 10
                ) {
                  $("#searchhide").hide();
                  $("#second-dropdown").show();
                } else {
                  $("#searchhide").hide();
                  $("#second-dropdown").hide();
                  $("#third-dropdown").hide();
                  $("#fourth-dropdown").hide();
                }
              });

              $("#txtSearch").on("input", function () {
                if (
                  table.columns([column_no]).search() !== $("#txtSearch").val()
                ) {
                  table
                    .columns([column_no])
                    .search($("#txtSearch").val())
                    .draw();
                }
              });
              //  jQuery(document).ready(function($){
              //  // $("#second-dropdown option[value=0]").attr('selected', 'selected');
              //  // $('second-dropdown option[value="0"]').attr("selected");
              //  var options = $('#second-dropdown');

              //  $('#ddlSearch').on('change', function(e){
              //      $('#second-dropdown').append(options);
              //      if($(this).val() != 'Select') {
              //           $('#second-dropdown option[value!=' + $(this).val() +']').remove();
              //      } else {
              //          $('#second-dropdown').val('Select');
              //      }
              //  });
              // })
            });
          });
        }, 1000);
        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
       
        $("#second-dropdown").hide();
        $("#searchhide").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#search-button-foremail").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();

      }
    );
  }
}




  onChange($event: any) {
    this.EmployeedataDepartment = [];
    var url = this.baseurl + "/Department/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeedataDepartment = data["result"];
      //console.log("departmentdata*****",this.EmployeedataDepartment);
      this.selected = ` ${$event.target.value}`;
      if (this.selected == 1) {
        $("#searchhide").show();
        $("#Searchcolumwise").show();
        $("#SearchContractorFirmName").hide()
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      } else if (this.selected == 2) {
        $("#searchhide").show();
        $("#Searchcolumwise").show();
        $("#second-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();

        //  // $('#txtSearch-dropdown').show();
      } else if (this.selected == 3) {
        $("#searchhide").show();
        $("#Searchcolumwise").show();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      }
      else if (this.selected == 17) {
        $("#searchhide").hide();
        $("#Searchcolumwise").hide();
        $("#second-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
        $("#Eight-dropdown").show();
        $("#globalsearch").hide();
        $("#globalsearch1").hide();
       }
       else if (this.selected == 18) {
        $("#searchhide").hide();
        $("#Searchcolumwise").hide();
        $("#second-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
        $("#Eight-dropdowns").show();
        $("#globalsearch").hide();
        $("#globalsearch1").hide();
       }
      else if (this.selected == 14) {
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").show();
        $("#third-dropdown").hide();
        $("#Eight-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      }
      else if (this.selected == 15) {
        
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#SearchContractorFirmName").show();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      }
      else if (this.selected == 4) {
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forContactNumber").show();
        $("#search-button-forbiometricid").hide();
        $("#search-button-foremail").hide();
      } else if (this.selected == 5) {
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").show();
        $("#search-button-foremail").hide();
        $("#search-button-forContactNumber").hide();
      } else if (this.selected == 11) {
        $("#Searchcolumwise").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").show();
        $("#fifth-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      } else if (this.selected == 12) {
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").show();
        $("#seven-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      } else if (this.selected == 13) {
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fifth-dropdown").show();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      } else if (this.selected == 6) {
        $("#Searchcolumwise").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("#search-button-foremail").show();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
      } else if (this.selected == 10) {
        $("#Searchcolumwise").hide();
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").show();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      } else if (this.selected == 9) {
        $("#Searchcolumwise").hide();
        $("#second-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#third-dropdown").show();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#seven-dropdown").hide();
        $("eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        $("#search-button-foremail").hide();
      } else if (
        this.selected == 6 ||
        this.selected == 7 ||
        this.selected == 8
      ) {
        $("#searchhide").hide();
        $("#second-dropdown").hide();
        $("#third-dropdown").show();
        $("#fourth-dropdown").show();
      } else {
        $("#searchhide").hide();
        $("#second-dropdown").hide();
      }
      // $( '#txtSearch' ).on( 'input', function () {
      //   if ( table.columns([this.ariaSort]).search() !== $( '#txtSearch' ).val() ) {
      //     table.columns([this.ariaSort]).search( $( '#txtSearch' ).val() ).draw();
      //   }
    });
    this.EmployeedataDesignation = [];
    var url = this.baseurl + "/Designation/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeedataDesignation = data["result"];
    });
    this.EmployeedataFirmName = [];
    var url = this.baseurl + "/Contractor/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeedataFirmName = data["result"];
    });
    this.EmployeeLocationwise = [];
    var url = this.baseurl + "/Location/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeeLocationwise = data["result"];
      

    });
    this.EmployeeCostCenter = [];
    var url = this.baseurl + "/CostCenter/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeeCostCenter = data["result"];
    });
    this.EmployeeTypes = [];
    var url = this.baseurl + "/EmployeeType/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeeTypes = data["result"];
  
    });

    this.EmployeeGrade = [];
    var url = this.baseurl + "/GradeMaster/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeeGrade = data["result"];
    });

    // var options = $('#second-dropdown option');

    this.selected = ` ${$event.target.value}`;

    // $("#second-dropdown").append(options);
    // if ($(this.selected).val() != "Select") {
    //   $("#second-dropdown option[value!=" + $(this).val() + "]").remove();
    // } else {
    //   $("#second-dropdown").val("Select");
    // }
  }

  getContractorEmployeeData() {
    // this.Spinner.show();
    if(this.loggedUser.roleHead=='Admin'){
    this.contractor_data = [];
    let url = this.baseurl + "/getActiveEmpContractorList/";
  
    this.httpService.get(url + this.compId).subscribe(
      (data) => {
        this.Spinner.hide();
        this.contractor_data = data["result"];
        this.orgEmpList = data["result"];
        this.Spinner.hide();
        $("#second-dropdown").hide();
        $("#searchhide").hide();
        $("#Searchcolumwise").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#search-button-foremail").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        setTimeout(function () {
          $(function () {
            var table;
            $(document).ready(function () {
              table = $("#contractor_table").DataTable({
                pageLength: 10,
                dom: "lrtip",
                retrieve: true,
              });

              var column_no = 2;

              $("#selectedheader").on("change", function () {
                column_no = Number($(this).val());
              });

              $("#Searchcolumwise").on("input", function () {
                if (
                  table.columns([column_no]).search() !==
                  $("#Searchcolumwise").val()
                ) {
                  table
                    .columns([column_no])
                    .search($("#Searchcolumwise").val())
                    .draw();
                }

                //  this.selected_number=column_no;
              });
            });
          });
        }, 1000);
        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
        // $("#hideallcontractordive").hide()
        $("#second-dropdown").hide();
        $("#searchhide").hide();
        $("#Searchcolumwise").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#search-button-foremail").hide();
        $("#seven-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
      }
    );
  }
 else{
    this.contractor_data = [];
    let url = this.baseurl + "/getActiveEmpContractorLite/";
    let obj = {
      "empId": this.loggedUser.empId,
      "locationId":this.loggedUser.locationId,
      "roleId": this.loggedUser.rollMasterId,
      "compId": this.compId
    }
    this.httpService.post(url , obj).subscribe(
      (data) => {
        this.Spinner.hide();
        this.contractor_data = data["result"];
        this.orgEmpList = data["result"];
        this.Spinner.hide();
        $("#second-dropdown").hide();
        $("#searchhide").hide();
        $("#Searchcolumwise").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#SearchContractorFirmName").hide()
        $("#sixth-dropdown").hide();
        $("#search-button-foremail").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
        setTimeout(function () {
          $(function () {
            var table;
            $(document).ready(function () {
              table = $("#contractor_table").DataTable({
                pageLength: 10,
                dom: "lrtip",
                retrieve: true,
              });

              var column_no = 2;

              $("#selectedheader").on("change", function () {
                column_no = Number($(this).val());
              });

              $("#Searchcolumwise").on("input", function () {
                if (
                  table.columns([column_no]).search() !==
                  $("#Searchcolumwise").val()
                ) {
                  table
                    .columns([column_no])
                    .search($("#Searchcolumwise").val())
                    .draw();
                }

                //  this.selected_number=column_no;
              });
            });
          });
        }, 1000);
        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
        // $("#hideallcontractordive").hide()
        $("#second-dropdown").hide();
        $("#searchhide").hide();
        $("#Searchcolumwise").hide();
        $("#third-dropdown").hide();
        $("#fourth-dropdown").hide();
        $("#fifth-dropdown").hide();
        $("#sixth-dropdown").hide();
        $("#search-button-foremail").hide();
        $("#seven-dropdown").hide();
        $("#eight-dropdown").hide();
        $("#search-button-forbiometricid").hide();
        $("#search-button-forContactNumber").hide();
      }
    );
  }
}

  getContractorInactivelist() {
    // this.Spinner.show();

    let url = this.baseurl + "/getInActiveEmpContractorList/";
    this.contractor_data = [];
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.Spinner.hide();
      this.contractor_data = data["result"];
      this.orgEmpList = data["result"];
      this.Spinner.hide();
      setTimeout(function () {
        $(function () {
          var table = $("#contractor_table").DataTable({
            retrieve: true,
            dom: "lrtip",
          });

          var column_no = 2;

          $("#selectedheader").on("change", function () {
            column_no = Number($(this).val());
          });

          $("#Searchcolumwise").on("input", function () {
            if (
              table.columns([column_no]).search() !==
              $("#Searchcolumwise").val()
            ) {
              table
                .columns([column_no])
                .search($("#Searchcolumwise").val() as asignColType)
                .draw();
            }
          });
        });
      }, 1000);
      this.chRef.detectChanges();
    }),
      (err: HttpErrorResponse) => {};
  }
  getEmpDetail(object) {
    this.dataEmp = object.empPerId;
    if (this.listType == "contractor") {
      this.InfoService.changeMessage("contractor");
    }
    if (this.listType == "employee") {
      this.InfoService.changeMessage("employee");
    }
    if (this.listType == "preonboarding") {
      this.InfoService.changeMessage("preonboarding");
    }

    sessionStorage.setItem("EmpData", this.dataEmp);
    sessionStorage.setItem("whoClicked", "HR");
  }

  gotoManegement() {
    this.router.navigateByUrl("/layout/addmanagementemp");
  }
  goto() {
    if (this.listType == "contractor") {
      this.InfoService.changeMessage("contractor");
    }
    if (this.listType == "employee") {
      this.InfoService.changeMessage("employee");
    }
    if (this.dataTablepreOn && !this.empdata) {
      this.router.navigateByUrl("/layout/addcontractoremp");
    } else if (this.empdata) {
      sessionStorage.removeItem("EmpDataDetails");
      this.router.navigateByUrl("/layout/pimshr");
    }
  }
  gotoPreonbording() {
    if (this.listType == "preonboarding") {
      this.InfoService.changeMessage("preonboarding");
    }
    this.router.navigateByUrl("/layout/preonboarding");
  }
  gotothis() {
    sessionStorage.removeItem("EmpDataDetails");
    this.router.navigateByUrl("/layout/importemployee");
  }
  setEmp(item, val) {
    this.dataEmp = item.empPerId;
    sessionStorage.setItem("EmpDataDetails", this.dataEmp);
    sessionStorage.setItem("whoClicked", "HR");
    sessionStorage.setItem("isEditHR", "true");
    sessionStorage.setItem("preonboarding", val);
    if (this.listType == "contractor") {
      this.InfoService.changeMessage("contractor");
    }
    if (this.listType == "employee") {
      this.InfoService.changeMessage("employee");
    }
    if (this.listType == "preonboarding") {
      this.InfoService.changeMessage("preonboarding");
    }
  }
  editPersonal(item) {
    this.dataEmp = item.empPerId;
    sessionStorage.setItem("EmpData", this.dataEmp);
    sessionStorage.setItem("HrLogId", this.empPerId);
    sessionStorage.setItem("whoClicked", "HR");
    sessionStorage.setItem("IsEmployeeEdit", "true");
  }
  importExcel() {
    if(this.loggedUser.roleHead=='HR'){
    let url = this.baseurl + "/EmployeeExportById/";
    if (this.listType == "employee") {
      url = this.baseurl + "/EmployeeExportById/On Roll";
    }
    if (this.listType == "contractor") {
      url = this.baseurl + "/EmployeeExportById/Contractor";
    }

    this.httpService.get(url+'/'+this.loggedUser.locationId).subscribe(
      (data) => {
        this.EmployeeImportdata = data["result"];
        this.excelService.exportAsExcelFile(
          this.EmployeeImportdata,
          this.listType + "-List"
        );
      },
      (err: HttpErrorResponse) => {}
    );
    }
    else if(this.loggedUser.roleHead=='Admin'){
      let url = this.baseurl + "/EmployeeExportById/";
      if (this.listType == "employee") {
        url = this.baseurl + "/EmployeeExportById/On Roll";
      }
      if (this.listType == "contractor") {
        url = this.baseurl + "/EmployeeExportById/Contractor";
      }
  
      this.httpService.get(url+'/'+this.selectedLocationId).subscribe(
        (data) => {
          this.EmployeeImportdata = data["result"];
          this.excelService.exportAsExcelFile(
            this.EmployeeImportdata,
            this.listType + "-List"
          );
        },
        (err: HttpErrorResponse) => {}
      );



  }
}
  employeeListTypeChanged(event) {

    $("#searchhide").hide();
    $("#Searchcolumwise").hide();
    $("#second-dropdown").hide();
    $("#third-dropdown").hide();
    $("#fourth-dropdown").hide();
    $("#fifth-dropdown").hide();
    $("#sixth-dropdown").hide();
    $("#SearchContractorFirmName").hide()
    $("#seven-dropdown").hide();
    $("#eight-dropdown").hide();
    $("#search-button-forbiometricid").hide();
    $("#search-button-forContactNumber").hide();
    $("#search-button-foremail").hide();
    this.listType = event;
    //console.log(event);
    if (event == "contractor") {
      this.dataTablepreOn = true;
      this.empdata = false;
      this.getContractorEmployeeData();
    }

    if (event == "employee") {
      this.empdata = true;
      if (this.empActiveListType == "active") {
        this.empdata = true;
        this.getEmployeeData();
      } else {
        this.empdata = true;
        this.dataTablepreOn = true;
        this.inActiveEmpList();
      }
    }

    if (event == "preonboarding") {
      this.dataTablepreOn = true;
      this.priOnboarding();
    }
  }
  employeeListTypeChangedActiveInactive(event) {
    this.empActiveListType = event;
    if (this.listType == "employee") {
      if (event == "active") {
        this.empdata = true;
        var table1 = $("#emptable").DataTable().destroy();
        this.getEmployeeData();
      } else {
        var table1 = $("#emptable").DataTable().destroy();
        this.dataTablepreOn = false;
        this.empdata = true;
        this.inActiveEmpList();
      }
    }
  }
  contractorListTypeChangedActiveInactive(event) {
    $("#searchhide").hide();
    $("#Searchcolumwise").hide();
    $("#second-dropdown").hide();
    $("#third-dropdown").hide();
    $("#fourth-dropdown").hide();
    $("#fifth-dropdown").hide();
    $("#sixth-dropdown").hide();
    $("#seven-dropdown").hide();
    $("#SearchContractorFirmName").hide()
    $("#eight-dropdown").hide();
    $("#search-button-forbiometricid").hide();
    $("#search-button-forContactNumber").hide();
    $("#search-button-foremail").hide();
    
    this.contractorActiveListType = event;
    if (this.listType == "contractor") {
      if (event == "active") {
        this.empdata = true;
        var table1 = $("#contractor_table").DataTable().destroy();
        this.getContractorEmployeeData();
      } else {
        var table1 = $("#contractor_table").DataTable().destroy();
        this.getContractorInactivelist();
        this.dataTablepreOn = false;
        this.empdata = true;
      }
    }
  }

  inActiveEmpList() {
    let url = this.baseurl + "/employeeInActive/";
    this.Employeedata = [];
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.Employeedata = data["result"];
      this.orgEmpList = data["result"];
      this.Spinner.hide();
      setTimeout(function () {
        $(function () {
          var table;
          $(document).ready(function () {
            table = $("#emptable").DataTable({
              pageLength: 10,
              dom: "lrtip",
              destroy: true,

              retrieve: true,
            });

            var column_no = 2;

            $("#ddlSearch").on("change", function () {
              column_no = Number($(this).val());
            });
            $("#txtSearch").on("input", function () {
              if (
                table.columns([column_no]).search() !== $("#txtSearch").val()
              ) {
                table.columns([column_no]).search($("#txtSearch").val()).draw();
              }
            });
          });
        });
      }, 1000);

      this.chRef.detectChanges();
    }),
      (err: HttpErrorResponse) => {};
  }
  priOnboarding() {
    this.dataTableFlagNewPreon = false;
    let url = this.baseurl + "/EmployeePreOnBoarding/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.dataTableFlagNewPreon = true;
      this.priOnBoard = data["result"];
      this.orgEmpList = data["result"];
      this.lengthOfPriboard = this.priOnBoard.length;
      this.Spinner.hide();
      setTimeout(function () {
        $(function () {
          var table = $("#preonboarding_table").DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
    }),
      (err: HttpErrorResponse) => {};
  }
  generatePass(i) {
    this.empOfficialId = i.empOfficialId;
    sessionStorage.setItem("employeeId", this.empOfficialId);
    this.InfoService.changeMessage("contractor");
    this.router.navigateByUrl("/layout/gatepass");
  }
  //EditContractor
  EditContractor(i) {
    let temp_emp_data = i.empPerId;
    sessionStorage.setItem("a_contractor", temp_emp_data);
    this.InfoService.changeMessage("contractor");
    this.router.navigateByUrl("/layout/update_employee");
  }
  setCurrentActiveTab() {
    //get data
    this.InfoService.currentMessage.subscribe((message) => {
      if (message != "default message") {
        this.received_message = message;
        this.listType = message;
        this.employeeListTypeChanged(message);
      } else {
        this.listType = "employee";
        this.employeeListTypeChanged(this.listType);
      }
    });
  }

  onSelect(val) {
    //console.log("objectdata",val);  
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeedataDepartmentbyid = [];
    let url = this.baseurl + "/EmployeeListSearchByDepartment/";
    this.httpService.get(url + val+'/'+this.loggedUser.locationId).subscribe((data) => {
      this.EmployeedataDepartmentbyid = data["result"];
      this.Employeedata = this.EmployeedataDepartmentbyid;
      //console.log("datadepartment",this.Employeedata );
    });

  }
  onSelectContractFirmName(val) {
    if(this.loggedUser.roleHead=='HR'){
    console.log("objectdata",val);  
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeedataDepartmentbyid = [];
    let url = this.baseurl + "/ContractorListSearchByContractorfirmName/";
    this.httpService.get(url + val+'/'+this.loggedUser.locationId).subscribe((data) => {
      this.EmployeedataDepartmentbyid = data["result"];
      this.contractor_data = this.EmployeedataDepartmentbyid;
      console.log("datadepartment",   this.contractor_data);
    });

  }
  else if(this.loggedUser.roleHead=='Admin'){
   
      console.log("objectdata",val);  
      var table1 = $("#emptable").DataTable().destroy();
      this.EmployeedataDepartmentbyid = [];
      let url = this.baseurl + "/ContractorListSearchByContractorfirmName/";
      this.httpService.get(url + val+'/'+this.locationids).subscribe((data) => {
        this.EmployeedataDepartmentbyid = data["result"];
        this.contractor_data = this.EmployeedataDepartmentbyid;
        console.log("datadepartment",   this.contractor_data);
      });
  
    
  }

}

  onSelect1(val) {
 
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeedataDesignationbyid = [];
  
  
    let url = this.baseurl + "/EmployeeListSearchByDesignation/";
   
    this.httpService.get(url +val+'/'+this.loggedUser.locationId).subscribe((data) => {
      this.EmployeedataDesignationbyid = data["result"];
      this.Employeedata = this.EmployeedataDesignationbyid;
      console.log("empdata",    this.Employeedata);
    });
  }

  onSelect2(val) {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeCostCenterById = [];
    let url = "EmployeeListSearchByCostCenter";
    // this.httpService.get(url + val)
    this.dataService.getRecordByCompLocIds(url, val,this.loggedUser.locationId)
    .subscribe((data) => {
      this.EmployeeCostCenterById = data["result"];
      this.Employeedata = this.EmployeeCostCenterById;
    });
  }
  onSelect3(val){

    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeTypeCenterById = [];
    let url = this.baseurl + "/EmployeeListSearchByEmployeeType/";
    this.httpService.get(url + val+'/'+this.loggedUser.locationId).subscribe((data) => {
      this.EmployeeTypeCenterById = data["result"];

      this.Employeedata = this.EmployeeTypeCenterById;
    console.log("emptyedata**",   this.Employeedata);
    });
  }

  onSelect4(val) {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmploymentTypeById = [];
    let url = this.baseurl + "/EmployeeListSearchByEmploymentType/";
    this.httpService.get(url + val+'/'+this.loggedUser.locationId).subscribe((data) => {
      this.EmploymentTypeById = data["result"];
      this.Employeedata = this.EmploymentTypeById;
    });
  }


  onSelect5(val) {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeGradeTypeById = [];
    let url = "EmployeeListSearchByGrade";
    this.dataService.getRecordByCompLocIds(url, val,this.loggedUser.locationId)
    .subscribe((data) => {
      this.EmployeeGradeTypeById = data["result"];
      this.Employeedata = this.EmployeeGradeTypeById;
    });
  }
  OfficialEmailId() {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeEmailById = [];
    let url = "EmployeeSearchByEmailId";
   // this.httpService.get(url + this.Inputvalue)
    this.dataService.getRecordByCompLocIds(url, this.Inputvalue,this.loggedUser.locationId)
    .subscribe(
      (data) => {
        this.EmployeeEmailById.push(data["result"]);
        console.log(this.EmployeeEmailById)
        this.Employeedata = this.EmployeeEmailById;
      },
      (err) => {
        if (err.status == 406 || err.status == 500) {
          this.toastr.error("this email is not exits");
        }
      }
    );
  }
  SearchByBiometricid() {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeebioById = [];
    let url = this.baseurl + "/EmployeeSearchByBioId/";
    this.httpService.get(url + this.Inputvalue).subscribe(
      (data) => {
        this.EmployeebioById = data["result"];
        this.Employeedata = this.EmployeebioById;
        console.log("empdata", this.Employeedata);
      },
      (err) => {
        if (err.status == 400||err.status == 404||err.status == 500) {
          this.toastr.error("this bioId is not exits");
        }
      }
    );

  }
  SearchByContactNumber() {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeSearchByContact = [];
    let url =  "EmployeeSearchByOfficialContactNumber";
    // this.httpService.get(url + this.Inputvalue)
    this.dataService.getRecordByCompLocIds(url, this.Inputvalue,this.loggedUser.locationId)
    .subscribe(
      (data) => {
        this.EmployeeSearchByContact = data["result"];
        this.Employeedata = this.EmployeeSearchByContact;
      },
      (err) => {
        if (err.status == 500||err.status == 404) {
          this.toastr.error("this contact is not exits");
        }
      }
    );
  }

  OfficialEmailId1() {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeEmailById1 = [];
    let url = "EmployeeSearchByEmailId";
    // this.httpService.get(url + this.Inputvalue)
    this.dataService.getRecordByCompLocIds(url, this.Inputvalue,this.loggedUser.locationId)
    .subscribe(
      (data) => {
        this.EmployeeEmailById1 = data["result"];
        this.contractor_data = this.EmployeeEmailById1;
      },
      (err) => {
        if (err.status == 406 || err.status == 500) {
          this.toastr.error("this email is not exits");
        }
      }
    );
  }
  SearchByBiometricid1() {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeebioById1 = [];
    let url = this.baseurl +"/ContractorSearchByBioId/";
 
    this.httpService.get(url + this.Inputvalue).subscribe(
      (data) => {
        this.EmployeebioById1 = data["result"];
        this.contractor_data = this.EmployeebioById1;
        console.log("biodata",this.contractor_data);
      },
      (err) => {
        if (err.status == 400||err.status == 404 || err.status == 500) {
          this.toastr.error("this bioId is not exits");
        }
      }
    );

  }
  SearchByContactNumber1() {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeSearchByContact1 = [];
    let url =  "EmployeeSearchByOfficialContactNumber";
    // this.httpService.get(url + this.Inputvalue)
    this.dataService.getRecordByCompLocIds(url, this.Inputvalue,this.loggedUser.locationId)
    .subscribe(
      (data) => {
        this.EmployeeSearchByContact1 = data["result"];
        this.contractor_data = this.EmployeeSearchByContact1;
      },
      (err) => {
        if (err.status == 500||err.status == 404) {
          this.toastr.error("this contact is not exits");
        }
      }
    );
  }
  onSelected(val) {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeedataDepartmentbyid1 = [];
    let url = "EmployeeListSearchByDepartment";
    // this.httpService.get(url + val)
    this.dataService.getRecordByCompLocIds(url, val,this.loggedUser.locationId)
    .subscribe((data) => {
      this.EmployeedataDepartmentbyid1 = data["result"];
      this.contractor_data = this.EmployeedataDepartmentbyid1;
    });
  }
  onSelectedC(val) {
    console.log("value",val);
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeedataDesignationbyid1 = [];
    let url = this.baseurl + "/ContractorListSearchByDepartment/";
    this.httpService.get(url +val+'/'+this.loggedUser.locationId).subscribe((data) => {
      this.EmployeedataDesignationbyid1 = data["result"];
      this.contractor_data = this.EmployeedataDesignationbyid1;
      //console.log("contractordata&&&&&",this.contractor_data);
    });

  }
 
  onSelect11(val) {
    
    console.log("valuebydesignation",val);
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeedataDesignationbyid1 = [];
    let url =this.baseurl+"/ContractorSearchByDesignations/";
     this.httpService.get(url + val).subscribe((data) => {
      this.EmployeedataDesignationbyid1 = data["result"];
      this.contractor_data = this.EmployeedataDesignationbyid1;
   
    });

  }
  onSelect22(val) {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeCostCenterById1 = [];
    let url = "EmployeeListSearchByCostCenter";
    // this.httpService.get(url + val)
    this.dataService.getRecordByCompLocIds(url, val,this.loggedUser.locationId)
    .subscribe((data) => {
      this.EmployeeCostCenterById1 = data["result"];
      this.contractor_data = this.EmployeeCostCenterById1;
    });
  }
  
 
  onSelect55(val) {
    var table1 = $("#emptable").DataTable().destroy();
    this.EmployeeGradeTypeById1 = [];
    let url = "EmployeeListSearchByGrade";
    // this.httpService.get(url + val)
    this.dataService.getRecordByCompLocIds(url, val,this.loggedUser.locationId)
    .subscribe((data) => {
      this.EmployeeGradeTypeById1 = data["result"];

      this.contractor_data = this.EmployeeGradeTypeById1;
    });
  }
  onSelect57(val) {
    
    let obj={
      "empId":this.loggedUser.empId,
      "locationId":val,
      "roleId":this.loggedUser.rollMasterId,
      "compId":this.loggedUser.compId,
    }

    
    
    var table1 = $("#emptable").DataTable().destroy();
    this.AllLocationwisedata = [];
    let url = "EmployeeListSearchByLocation";
    // this.httpService.get(url + val)
    this.dataService.getRecordByComp(url,obj)
    .subscribe((data) => {
      this.AllLocationwisedata = data["result"];
      this.Employeedata = this.AllLocationwisedata;
 this.orgEmpList=data["result"];
 
 

     $("#globalsearch").show();
    
 
      this.Employeedata = this.AllLocationwisedata;
    
    });
  }
  onSelect58(val) {
    let obj={
      "empId":this.loggedUser.empId,
      "locationId":val,
      "roleId":this.loggedUser.rollMasterId,
      "compId":this.loggedUser.compId,
    }
    
    var table1 = $("#contractor_table").DataTable().destroy();
    this.AllLocationwisedatas = [];
    let url = "contractorListSearchByLocation";
    // this.httpService.get(url + val)
    this.dataService.getRecordByLocation(url,obj)
    .subscribe((data) => {
      this.AllLocationwisedatas = data["result"];
      this.contractor_data=this.AllLocationwisedatas
      this.orgEmpList=data["result"];
 

    // $("#globalsearch1").show();
    
 
    //   this.contractor_data = this.AllLocationwisedata;
    
    });
  }
  omit_number(event) {
   
    var key;
    key = event.keyCode;  
    
    return ((key > 47 && key < 58) || key == 45 || key == 46);
   
}

}
