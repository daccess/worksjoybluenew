import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontdeskAssetsComponent } from './frontdesk-assets.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: FrontdeskAssetsComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'frontDeskAssets',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'frontDeskAssets',
                  component: FrontdeskAssetsComponent
              }

          ]

  },

  // otherwise redirect to home

]

export const FrontDeskAssets = RouterModule.forChild(appRoutes);