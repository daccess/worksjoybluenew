import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';
// import { debug } from 'util';
// import { error } from 'console';

@Component({
  selector: 'app-frontdesk-assets',
  templateUrl: './frontdesk-assets.component.html',
  styleUrls: ['./frontdesk-assets.component.css']
})
export class FrontdeskAssetsComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  labourIds: string;
  labourmasterId: string;
  // labourDatas: Object;
  labourdatas: any;
  labourData: any;
  empfistname: any;
  lastname: any;
  gender: any;
  dateOfBirth: any;
  bloodGroup: any;
  contactNumber: any;
  emergancyContact: any;
  allAssets: any[]=[];
  assetId: any;
  issuedate: any;
  issuedReason: any;
  labourPerId: any;
  allAssetsData: any;
  assetHistoryId: any;
  labourimg: any;
  assetsHistoryList:any=[];
  assetName: any;
  selectedLevel;
  iseditFlag: string;
  contractorName: any;
  lastnames: any;
  selectedValue: any;
  remark:any
  

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
    this.labourIds=sessionStorage.getItem("LabourPerIds")
    this.labourmasterId=sessionStorage.getItem("manrequestIds")
    this.iseditFlag=sessionStorage.getItem('IseditFlag');
    
   }

  ngOnInit() {
    this.getSafetyEmployee()
    this.getassets();
    this.getlabourperticularData();
  }
  getassets(){
    
    let url = this.baseurl + `/getAllAssets`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
    this.allAssets=data['result']
    })

  }
  getSafetyEmployee(){
    let url = this.baseurl + `/getByIdLabourInfo/${this.labourIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.labourData = data
      this.labourdatas=this.labourData.result;
      this.empfistname=this.labourdatas.firstName
      this.contractorName=this.labourdatas.contractorMaster.firstName
    this.lastnames=this.labourdatas.contractorMaster.lastName
      this.lastname=this.labourdatas.lastName
      this.gender=this.labourdatas.gender
      this.dateOfBirth=this.labourdatas.dateOfBirth
      this.bloodGroup=this.labourdatas.bloodGroup
      this.contactNumber=this.labourdatas.contactNumber
      this.emergancyContact=this.labourdatas.emergencyContactNo
      this.labourPerId=this.labourdatas.labourPerId
      this.labourimg=this.labourdatas.labourImage
      // this.locationId = this.locationdata[0].locationId
    

    },
      (err: HttpErrorResponse) => {

      })

  }
  // getAssets(e:any) {
  //   const selectedValue = e.target.value;
  //     this.assetId = selectedValue;
  // }
  getAssets(event: any) {
    // Access the selected values
    const selectedAsset = this.selectedValue;
  
    // Access specific properties
    this.assetId = selectedAsset.assetId;
    this.assetName = selectedAsset.assetName;
   
    
  
    // Your logic here
  }
  getAssetss(){
    let obj={
      issuedReason:this.issuedReason,
      issuedate:this.issuedate,
      assetId:this.assetId,
      assetName:this.assetName,
      safetyEmpId:this.empids,
    
    }
    this.assetsHistoryList.push(obj);
    this.issuedReason = null;
    this.issuedate = null;
    this.assetId = null;
    this.assetName = null;
    this.empids = null;
  }
  saveassets(){
   
    let obj={
      labourPerId:this.labourPerId,
      remark:this.remark,
      assetHistoryList:this.assetsHistoryList,
      approverId:this.empids,
      manReqStatusId:this.labourmasterId
    }
    console.log(obj);
    let url = this.baseurl + `/saveSafetyLabourdata`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.Spinner.show();
    this.httpservice.post(url,obj, { headers }).subscribe(data => {
    this.toastr.success("labour assets save successfully")
    this.router.navigateByUrl('layout/safetyapproval')
    this.Spinner.hide()
    // this.router.navigateByUrl('layout/safetylabourDoneList/safetylabourDoneList');
  },
  (err: HttpErrorResponse) => {
this.toastr.error("error while add assets")
this.Spinner.hide()
  })
}
saveassets1(){
  let obj={
    labourPerId:this.labourPerId,
    remark:this.remark,
   
    assetHistoryList:this.assetsHistoryList,
  }
  console.log(obj);
  let url = this.baseurl + `/updateAssetHistory`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.post(url,obj, { headers }).subscribe(data => {
    this.iseditFlag=='false'
  this.toastr.success("labour assets update successfully")
  // this.router.navigateByUrl('layout/safetylabourDoneList/safetylabourDoneList');
},
(err: HttpErrorResponse) => {
this.toastr.error("error while update assets")
})
}
getlabourperticularData(){
  let url = this.baseurl + `/getAssetHistory/${this.labourIds}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.get(url, { headers }).subscribe(data => {
  this.allAssetsData=data['result']
  // for(let i=0; i<this.allAssetsData.length; i++){
  //   if(this.allAssetsData[i].assetsMaster){
  //     this.assetsHistoryList.push(this.allAssetsData[i].assetsMaster)
  //   }
  // }
  
 
  
  // Loop through each object in the result array
  const modifiedResult = this.allAssetsData.map((item) => {
    // Check if assetsMaster is present
    if (item.assetsMaster && item.assetsMaster.assetId) {
      // Move assetsMaster.assetId value to assetId and remove assetsMaster property
      item.assetId = item.assetsMaster.assetId;
      delete item.assetsMaster;
    }
  
    return item;
  });
  
  // Update the data object with the modified result array
  // data.result = modifiedResult;
  
  // Log the modified data
  console.log(JSON.stringify(modifiedResult));
  this.assetsHistoryList=modifiedResult
  
  
  
  

})
}
deleteAssets(e){
  this.assetHistoryId=e.assetHistoryId
  let url = this.baseurl + `/deleteAssetHistory/${this.assetHistoryId}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.delete(url, { headers }).subscribe(data => {
  this.toastr.success("delete assets successfully")
  this.getlabourperticularData()

},(err:HttpErrorResponse)=>{
this.toastr.error("error while delete assets of labour")
})
}
goToPrevious(){
  debugger
  document.getElementById('extra-apply-tab').click();
}

}
