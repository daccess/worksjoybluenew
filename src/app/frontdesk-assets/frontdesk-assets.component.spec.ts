import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontdeskAssetsComponent } from './frontdesk-assets.component';

describe('FrontdeskAssetsComponent', () => {
  let component: FrontdeskAssetsComponent;
  let fixture: ComponentFixture<FrontdeskAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontdeskAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontdeskAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
