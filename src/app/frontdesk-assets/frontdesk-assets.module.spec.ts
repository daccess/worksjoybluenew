import { FrontdeskAssetsModule } from './frontdesk-assets.module';

describe('FrontdeskAssetsModule', () => {
  let frontdeskAssetsModule: FrontdeskAssetsModule;

  beforeEach(() => {
    frontdeskAssetsModule = new FrontdeskAssetsModule();
  });

  it('should create an instance', () => {
    expect(frontdeskAssetsModule).toBeTruthy();
  });
});
