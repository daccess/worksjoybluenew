import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontDeskAssets } from './frontdesk-assets-routing.module';
import { FrontdeskAssetsComponent } from './frontdesk-assets.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FrontDeskAssets,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    FrontdeskAssetsComponent
  ]
})
export class FrontdeskAssetsModule { }
