import { Routes, RouterModule } from '@angular/router'
import { ResetpwdforsupervComponent } from './resetpwdforsuperv.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ResetpwdforsupervComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'resetpwdforsuperv',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'resetpwdforsuperv',
                    component: ResetpwdforsupervComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);