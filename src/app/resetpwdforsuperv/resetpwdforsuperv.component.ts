import { Component, OnInit } from '@angular/core';


import { MainURL } from '../shared/configurl';
import { Http } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-resetpwdforsuperv',
  templateUrl: './resetpwdforsuperv.component.html',
  styleUrls: ['./resetpwdforsuperv.component.css']
})
export class ResetpwdforsupervComponent implements OnInit {
  baseurl = MainURL.HostUrl
  loggedUser: any;
  compId: any;
  empOfficialId: any;
  empPerId: any;
  Employeedata: any;
  empFlag: boolean = false;
  searchQuery: any;
  ResetempPerId: any;

  constructor(public toastr: ToastrService, private httpService: Http) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.getAllEmployee();
  }

  ngOnInit() {
  }

  getAllEmployee() {
    let url = this.baseurl + '/employeelistunderempid/';
    this.httpService.get(url + this.loggedUser.empId).subscribe(data => {
        this.Employeedata = data.json().result;
      },
      (err: HttpErrorResponse) => {
      });
  }
  arrowkeyLocation = 0;

  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }
  filterItem(value) {
    if (!value) {
      this.empFlag = false;
    }
    if (value) {
      this.empFlag = true;
    }
  }
  suggestThis(item) {
    this.searchQuery = item.fullName;
    this.empPerId = item.empPerId;
    this.empFlag = false;
  }
  resetPass(f) {
    let url = this.baseurl + '/resetPassword?empPerId=' + this.empPerId;
    let obj = {
      empPerId: this.empPerId
    }
    this.httpService.put(url, obj).subscribe(data => {
        this.toastr.success('Password Reset Successfully')
        f.reset();
      },
      (err: HttpErrorResponse) => {
        this.toastr.error('Server Side Error')
      });
  }
}
