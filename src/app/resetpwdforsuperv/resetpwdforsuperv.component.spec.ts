import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetpwdforsupervComponent } from './resetpwdforsuperv.component';

describe('ResetpwdforsupervComponent', () => {
  let component: ResetpwdforsupervComponent;
  let fixture: ComponentFixture<ResetpwdforsupervComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetpwdforsupervComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetpwdforsupervComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
