import { routing } from './resetpwdforsuperv.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ResetpwdforsupervComponent } from './resetpwdforsuperv.component';

import { SearchPipeForResetSup } from './searchpipe/SearchPipe';

@NgModule({
    declarations: [
        ResetpwdforsupervComponent,
        SearchPipeForResetSup

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    ],
  })
  export class ResetpwdforsupervModule { 
   
      constructor(){

      }

    
  }
  