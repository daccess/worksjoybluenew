export class PreOnBoarding {
    empPerId:number;
    salutation:string;
    fullName:string;
    middleName:string;
    lastName:string;
    employeeStatus:string;
    gender:string;
    dateOfBirth:any;
    age:number;
    personalContactNumber:string;
    personalEmailId:string;
    logEmpOffId:number;
    locationId: any;
    employeementType: string;
    locationname: string;
    emptype:any;
  }