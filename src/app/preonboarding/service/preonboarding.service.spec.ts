import { TestBed, inject } from '@angular/core/testing';

import { PreonboardingService } from './preonboarding.service';

describe('PreonboardingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PreonboardingService]
    });
  });

  it('should be created', inject([PreonboardingService], (service: PreonboardingService) => {
    expect(service).toBeTruthy();
  }));
});
