import { Injectable } from '@angular/core';
import { MainURL } from '../../shared/configurl';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { PreOnBoarding } from '../model/PreOnBoarding';
@Injectable({
  providedIn: 'root'
})
export class PreonboardingService {
  baseurl = MainURL.HostUrl
  
  constructor(private http : Http) { }

  postPreonboarding(preonBoading : PreOnBoarding){
    let url = this.baseurl + '/emppreonboarding';
    var body = JSON.stringify(preonBoading);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,preonBoading,requestOptions).map(x => x.json());
  }
}
