import { Routes, RouterModule } from '@angular/router'

// import { PreonboardingComponent } from 'src/app/preonboarding/preonboarding.component';

import { PreonboardingComponent } from 'src/app/preonboarding/preonboarding.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: PreonboardingComponent,

            children:[
                {
                    path:'preonboarding',
                    component: PreonboardingComponent,
                }

            ]
    },
    {
        path : 'preonboarding',
        component : PreonboardingComponent,
        
    }
 
]

export const routing = RouterModule.forChild(appRoutes);