import { Component, OnInit } from '@angular/core';
import { PreOnBoarding } from './model/PreOnBoarding';
import { PreonboardingService } from './service/preonboarding.service';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';
import { Router } from '@angular/router';
@Component({
  selector: 'app-preonboarding',
  templateUrl: './preonboarding.component.html',
  styleUrls: ['./preonboarding.component.css']

})
export class PreonboardingComponent implements OnInit {
  birthdate:any
  cur:any;
  diff:any;
  age:any;
  maxDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
  preonBoadingModel = new PreOnBoarding();
  preOnBoadingList = [];
  errflag:boolean;
  loggedUser: any;
  selectedCompanyobj: any;
  compId: any;
  baseurl = MainURL.HostUrl
    errordata=[];
  msg: any;
  LocationAllData: any;
  weeklyCalendarId: number;
  selectedLocationobj: any;
  public selectUndefinedOptionValue:any="";
  constructor(private http: PreonboardingService,public toastr: ToastrService,public httpService: HttpClient,public router: Router) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    console.log("sessionnnnn data",this.loggedUser);
    this.preonBoadingModel.dateOfBirth = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    
    this.compId =  this.loggedUser.compId
   
   }

  ngOnInit() {
    this.locationGRoup();
  }
  errorpersonal(event){
    let obj = {
      personalEmailId: this.preonBoadingModel.personalEmailId
    }
    let url = this.baseurl + '/empPerMailIdCheck';
    this.httpService.post(url, obj).subscribe((data: any) => {
      this.errordata = data["result"];
      this.msg = data.message
      this.toastr.error(this.msg);
      this.errflag = true
    },
    (err: HttpErrorResponse) => {
      this.errflag = false
    });
  }

  getdate(event){
    this.preonBoadingModel.dateOfBirth=event.target.value;
    this.calculateage(event.target.value)
  }
  calculateage(getvalue) {
    this.birthdate = new Date(this.preonBoadingModel.dateOfBirth);
    this.cur = new Date();
    this.diff = this.cur - this.birthdate; //This is the difference in milliseconds
    this.age = Math.floor(this.diff / 31557600000); //Divide by 1000*60*60*24*365.25
    this.preonBoadingModel.age=this.age;
    if(this.age <= 18) {
      this.toastr.error('Age cannot be less than 18 years');
      }
  }

  genderChange(){
    if(this.preonBoadingModel.salutation == 'MR'){
      this.preonBoadingModel.gender='Male';
    }
    if(this.preonBoadingModel.salutation == 'Mrs'){
      this.preonBoadingModel.gender='Female';
    }
    if(this.preonBoadingModel.salutation == 'Miss'){
      this.preonBoadingModel.gender='Female';
    }
  }

  AddNew(){
  this.preonBoadingModel.fullName=""
  this.preonBoadingModel.middleName="";
  this.preonBoadingModel.lastName="";
  this.preonBoadingModel.gender="";
  this.preonBoadingModel.dateOfBirth= new Date();
  this.preonBoadingModel.locationId="";
  this.preonBoadingModel.employeementType="";
  this.preonBoadingModel.personalContactNumber="";
  this.preonBoadingModel.personalEmailId="";
 }
  SavePreonboardingInfo(preonBoading) {
    preonBoading.compId = this.compId
    this.http.postPreonboarding(preonBoading).subscribe(data => {
    this.preOnBoadingList=data.result;
    this.toastr.success("Preonboarding Information is Save Successfully");
    this.preonBoadingModel = new PreOnBoarding();
    this.router.navigateByUrl('layout/employeelist/employeelist');
     
    } ,(err: HttpErrorResponse) => {
     this.toastr.error('Server Side Error..!', 'Preonboarding Information is Not Save  Successfully ');
    });
  }
  // .........................location ....................................

  locationGRoup() {
    let url4 = this.baseurl + '/Location/Active/';
    this.httpService.get(url4 + this.compId).subscribe(data => {
        this.LocationAllData = data["result"];
      }, (err: HttpErrorResponse) => {
    });
  }
  Locationdata(event) {
    this.selectedLocationobj = "";
    this.selectedLocationobj = parseInt(event.target.value);
  }
  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
}
