import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { QueryadminComponent } from 'src/app/queryadmin/queryadmin.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: QueryadminComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'queryadmin',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'queryadmin',
                    component: QueryadminComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);