import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryadminComponent } from './queryadmin.component';

describe('QueryadminComponent', () => {
  let component: QueryadminComponent;
  let fixture: ComponentFixture<QueryadminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryadminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
