import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { RegularisationComponent } from 'src/app/regularisation/regularisation.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: RegularisationComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'regularisation',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'regularisation',
                    component: RegularisationComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);