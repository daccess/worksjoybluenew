import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from 'src/app/shared/configurl';
@Injectable({
  providedIn: 'root'
})
export class RegularisationService {
  baseurl = MainURL.HostUrl;
  approveMedicalRequest: any;
  approveSafetyRequest: any;
  constructor(private http: Http) {

  }

  getLeaveList(id) {
    let url = this.baseurl + '/leaverequest/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  getMissPunchRequests(id) {
    // let url = this.baseurl + '/otherinout';
    let url = this.baseurl + '/employeemisspunch/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  getPermissionRequests(id) {
    let url = this.baseurl+'/employeepr/'+id;
    // let url = this.baseurl + '/requestpr';
    // let url = this.baseurl+'/employeeod/38047'
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  getLateEarlyRequests(id) {
    // let url = this.baseurl + '/requestlateearly';
    let url = this.baseurl+'/employeeLateEarly/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  getOnDutyRequests(id) {
    // let url = this.baseurl + '/requestod';
    let url = this.baseurl+'/employeeod/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  getWorkFromHomeRequests(id) {
    // let url = this.baseurl + '/requestwfh';
    let url = this.baseurl + '/employeewfh/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  getCoffRequests(id) {
    // let url = this.baseurl + '/otherinout';
    let url = this.baseurl + '/coffrequestunderemp/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }

  getExtraWorkRequests(id) {
    // let url = this.baseurl + '/otherinout';
    let url = this.baseurl + '/extraworking/'+id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }
  // getLateEarlyRequests(id) {
  //   let url = this.baseurl + '/requestlateearly';
  //   // var body = JSON.stringify(onduty);
  //   var headerOptions = new Headers({ 'Content-Type': 'application/json' });
  //   var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
  //   return this.http.get(url).map(x => x.json());
  // }
  getNotifyAndHanded(id) {
    let url = this.baseurl + '/employeehandednotify/' + id;
    // var body = JSON.stringify(onduty);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.get(url).map(x => x.json());
  }

  rejectleaveRequest(leave){
    let url = this.baseurl + '/rejectOtherLeaveRequestAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  rejectMissPunchRequest(leave){
    let url = this.baseurl + '/rejectOtherMissPunchAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  rejectOnDutyRequest(leave){
    let url = this.baseurl + '/rejectOtherODAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  rejectPermissionRequest(leave){
    let url = this.baseurl + '/rejectOtherPRAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  rejectLateEarlyRequest(leave){
    let url = this.baseurl + '/rejectOtherLateearlyAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  rejectWorkFromHomeRequest(leave){
    let url = this.baseurl + '/rejectOtherWFHAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  rejectExpenseRequest(expense): Observable<any>{
    let url = this.baseurl + '/rejectExpensesData';
    var body = {expenseApproveDtoList:expense};
  
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  rejecExtraWorkRequest(leave){
    let url = this.baseurl + '/rejectExtraWorkingRequest';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  rejectCoffRequest(leave){
    let url = this.baseurl + '/rejectOtherCoffAttendanceData';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  //==========Approve Start ==============
  approveleaveRequest(leave){
    // let url = this.baseurl + '/rejectOtherLeaveRequestAttendanceData';
    let url = this.baseurl + '/ApproveLeaveRequestAttendanceData';
    var body = {leaveRequestAttendanceRejectedReqDtoList:leave,compId : leave.compId};
  
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  approveMissPunchRequest(leave){
    let url = this.baseurl + '/ApproveOtherMissPunchAttendanceData';
    var body = {otherMissPunchAttendanceRejectedReqDtos:leave,compId : leave.compId};
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  approveOnDutyRequest(leave){
    let url = this.baseurl + '/ApproveOtherODAttendanceData';
    var body = {otherODAttendanceRejectedReqDtoList:leave,compId : leave.compId};
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  approvePermissionRequest(leave){
    let url = this.baseurl + '/ApprovedOtherPRAttendanceData';
    var body = {otherPRAttendanceRejectedReqDtoList:leave,compId : leave.compId};
    
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  approveLateEarlyRequest(leave){
    let url = this.baseurl + '/ApproveOtherLateearlyAttendanceData';
    var body = {otherLateEarlyAttendanceRejectedReqDtoList:leave,compId : leave.compId};
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  approveWorkFromHomeRequest(leave){
    let url = this.baseurl + '/ApproveOtherWFHAttendanceData';
    var body = {otherWFHAttendanceRejectedReqDtoList:leave,compId : leave.compId};
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  approveExpenseRequest(expense): Observable<any>{
    let url = this.baseurl + '/approveExpensesData';
    var body = {expenseApproveDtoList:expense};
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  approveCoffRequest(leave){
    let url = this.baseurl + '/ApproveOtherCoffAttendanceData';
    var body = {otherCoffAttendanceRejectedReqDtoList:leave,compId : leave.compId};
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  approveExtraWorkRequest(leave){
    let url = this.baseurl + '/ApproveExtraWorkingRequest';
    var body = {otherExtraWorkingAttendanceRequestDtos:leave,compId : leave.compId};
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  //===========Approve Ends ==============
  getLeaveCancelList(id) {
    let url = this.baseurl + '/leaverequestCancel/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getLateEarlyCancelList(id) {
    let url = this.baseurl + '/employeeLateEarlyCancel/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getMissPunchCancelList(id) {
    let url = this.baseurl + '/employeemisspunchCancel/' + id;
    return this.http.get(url).map(x => x.json());
  }
  getOnDutylList(id){
    let url = this.baseurl + '/employeeodCancel/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getPermissionlList(id){
    let url = this.baseurl + '/employeeprCancel/' + id;
    return this.http.get(url).map(x => x.json());
  }
  getWorkFromCancelList(id){
    let url = this.baseurl + '/employeewfhCancel/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getExtraWorkFromCancelList(id){
    let url = this.baseurl + '/extraworkingcancle/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getCoffCancelList(id){
    let url = this.baseurl + '/coffrequestcancle/' + id;
    return this.http.get(url).map(x => x.json());
  }

  approveLeaveCancel(leave){
    let url = this.baseurl + '/ApproveCancelLeave';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }
  
  approveLTECancel(leave){
    let url = this.baseurl + '/ApproveCancelLateEarly';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  approveMPCancel(leave){
    let url = this.baseurl + '/ApproveCancelMissPunch';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  approveODCancel(leave){
    let url = this.baseurl + '/ApproveCancelOD';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  approvePRCancel(leave){
    let url = this.baseurl + '/ApproveCancelPr';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  approveWFHCancel(leave){
    let url = this.baseurl + '/ApproveCancelWFH';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  approveExtrawork(leave){
    let url = this.baseurl + '/ApproveCancelExtraWorkingRequest';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  approveCoffCancel(leave){
    let url = this.baseurl + '/ApproveCancelCoffRequest';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  rejectLeaveCancel(leave){
    let url = this.baseurl + '/RejectCancelLeave';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }
  rejectLTECancel(leave){
    let url = this.baseurl + '/ApproveRejectLateEarly';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }
  rejectMPCancel(leave){
    let url = this.baseurl + '/ApproveRejectMissPunch';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }
  rejectODCancel(leave){
    let url = this.baseurl + '/ApproveRejectOD';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }
  rejectPRCancel(leave){
    let url = this.baseurl + '/ApproveRejectPr';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }
  rejectWFHCancel(leave){
    let url = this.baseurl + '/ApproveRejectWFH';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }

  rejectExWFHCancel(leave){
    let url = this.baseurl + '/RejectCancleExtraWorking';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }

  rejectCoffCancel(leave){
    let url = this.baseurl + '/RejectCancleCoffRequest';
    var body =leave;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    return this.http.put(url,body).map(x => x.json());
  }

  leaveRejectWithBlock(leave){
    let url = this.baseurl + '/rejectLeaveRequestwithLeaveBlockAttendanceData';
    var body = JSON.stringify(leave);
    console.log("body",body);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  leaveSuggestDates(leave){

    let url = this.baseurl + '/leaveRequestWiseSuggestion';
    var body = JSON.stringify(leave);
    console.log("body",body);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.put(url,leave).map(x => x.json());
  }

  getQueryList(id) {
    let url = this.baseurl + '/query/getEmployeeQueryList/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getQueryById(id) {
    let url = this.baseurl + '/getRaiseQueryById/' + id;
    return this.http.get(url).map(x => x.json());
  }

  approveQuery(query){

    let url = this.baseurl + '/updateRaisedQryAproval';
    var body = JSON.stringify(query);
    console.log("body",body);
    return this.http.put(url,query).map(x => x.json());
  }

  // Resign Starts 
  getResignList(id) {
    let url = this.baseurl + '/employeeClearanceOrHoldListByEmpId/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getResignById(id,empTypeId) {
    // ?empPerId=929&empTypeId=1
    let url = this.baseurl + '/empresignation/?empPerId=' + id+'&empTypeId='+empTypeId;
    return this.http.get(url).map(x => x.json());
  }


  approveResign(query,resignationReason,compId,empPerId){

    let url = this.baseurl + '/approvedOrRejectResignationData';
    var body = {resignationApproveAndRejectReqDtoList:query,resignationReason:resignationReason,compId:compId,empPerId:empPerId};
    // var body = JSON.stringify(query);
    console.log("body",body);
    return this.http.post(url,body).map(x => x.json());
  }

  rejectResign(query,resignationReason,compId){

    let url = this.baseurl + '/RejectResignationData';
    var body = {resignationApproveAndRejectReqDtoList:query,resignationReason:resignationReason,compId:compId};
  
    console.log("body",body);
    return this.http.post(url,body).map(x => x.json());
  }

  expenseListData(id){

    let url = this.baseurl + '/expenseDetailsForApprovals/';
    // var body = {resignationApproveAndRejectReqDtoList:query,resignationReason:resignationReason,compId:compId};
    // var body = JSON.stringify(query);
    console.log("vinodbody",id);
    return this.http.get(url+id).map(x => x.json());
  }

  getexpenceListById(id) {
    let url = this.baseurl + '/expenseById/' + id;
    return this.http.get(url).map(x => x.json());
  }

  getSameDateLeaveData(data){
    let url = this.baseurl + '/leaveRequestList';
    var body = data;
    return this.http.post(url,body).map(x => x.json());
  }

  employeesWithRoles(id){
    // let url = this.baseurl +'/query/getEmployeeList/'+id;
    let url = this.baseurl +'/getEmpListQueryAuthority';
    return this.http.get(url).map(x => x.json());
  }

  sendMail(data){
    // let url = this.baseurl +'/query/getEmployeeList/'+id;
    let url = this.baseurl + '/falseQueryMail';
    var body = JSON.stringify(data);
    return this.http.post(url,body).map(x => x.json());
  }

  
// ......................medical api......................................
getPreEmpMedicalVeri(empId) {
  let url = this.baseurl +'/getPreEmpMedicalVeri/';
  return this.http.get(url + empId).map(x => x.json());
}

postapproveEmpMedical(data)
{
 let url = this.baseurl +'/approveEmpMedical';
 var body = JSON.stringify(data);
 var headerOptions = new Headers({'Content-Type':'application/json'});
 var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
 return this.http.post(url,body,requestOptions).map(x => x.json());
}  

postrejectEmpMedical(data)
{
 let url = this.baseurl +'/rejectEmpMedical';
 var body = JSON.stringify(data);
 var headerOptions = new Headers({'Content-Type':'application/json'});
 var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
 return this.http.post(url,body,requestOptions).map(x => x.json());
}
// ....................../medical api......................................
// ...............................safety api......................................
getPreEmpSafetyVeri(empId) {
  let url = this.baseurl +'/getPreEmpSafetyVeri/';
  return this.http.get(url + empId).map(x => x.json());  
}
postapproveEmpSafety(data){
 let url = this.baseurl +'/approveEmpSafety';
 var body = JSON.stringify(data);
 var headerOptions = new Headers({'Content-Type':'application/json'});
 var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
 return this.http.post(url,body,requestOptions).map(x => x.json());
} 

postrejectEmpSafety(data){
 let url = this.baseurl +'/rejectEmpSafety';
 var body = JSON.stringify(data);
 var headerOptions = new Headers({'Content-Type':'application/json'});
 var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
 return this.http.post(url,body,requestOptions).map(x => x.json());
}
// ...............................safety api......................................
  getTrainingCompletion(empId) {
    let url = this.baseurl +'/empProbationaryAndTraining/';
    return this.http.get(url + empId).map(x => x.json());  
  }
  RejectTraining(data){
    let url = this.baseurl +'/probationAndTrainingRejection';
    var body = JSON.stringify(data);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
   }
   ApproveTraining(data){
    let url = this.baseurl +'/probationAndTrainingApproval';
    var body = JSON.stringify(data);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
   }
}

