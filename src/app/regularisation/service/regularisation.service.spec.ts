import { TestBed, inject } from '@angular/core/testing';

import { RegularisationService } from './regularisation.service';

describe('RegularisationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegularisationService]
    });
  });

  it('should be created', inject([RegularisationService], (service: RegularisationService) => {
    expect(service).toBeTruthy();
  }));
});
