import { routing } from './regularisation.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { RegularisationComponent } from 'src/app/regularisation/regularisation.component';
import { RegularisationService } from 'src/app/regularisation/service/regularisation.service';


@NgModule({
    declarations: [
        RegularisationComponent
        
    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: [RegularisationService]
  })
  export class RegularisationModule { 
      constructor(){

      }
  }
  