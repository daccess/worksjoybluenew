import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { ChangeDetectionStrategy } from '@angular/core';
//import { resignationservice } from '../employee/shared/services/resignationservice';
import { RegularisationService } from './service/regularisation.service';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-regularisation',
  templateUrl: './regularisation.component.html',
  styleUrls: ['./regularisation.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegularisationComponent implements OnInit {
  leaves: any = [];
  flag1: boolean = true;
  physicalmodel: any = { remark: "" }
  checked: boolean = false;
  date = new Date()
  model: any = { frmdate: this.date, todate: "" };
  leaveSuggestionModel : any = {};
  length: number;
  leaveList: any = [];
  monthNames: string[];
  notified = ["sopan", "ruturaj"];
  handed = ["ruturaj", "sopan"];
  selectedLeaves: any = [];
  approveChecks: boolean = false;
  missPunchRequests = [];
  permissionRequests = [];
  lateEarlyRequests = [];
  onDutyRequests = [];
  workFromRequests = [];
  LateEarlyCancelList = [];
  checkedMissPunch: boolean = false;
  checkedOnDuty: boolean = false;
  checkedPermissionRequests: boolean = false;
  checkedLateEarlyRequests: boolean = false;
  checkedWorkFromHomeRequests : boolean = false;
  checkedCoffRequests : boolean = false;
  checkedExtraWorkRequests : boolean = false;
  missPunchLength: number = 0;
  loggedUser: any;
  compId: any;
  empOfficialId: any;
  empPerId: any;
  empId: string;
  missPunchCancelList = [];
  onDutylList = [];
  permissionList = [];
  worFromHomeCAncelList = [];
  extraworFromHomeCAncelList = [];
  LeaveCancelList = [];
  coffRequests = [];
  extraWorkRequests = [];
  cancelLength : number = 0;
  coffCancelList: any;
  constructor(private http: Http, public toastr: ToastrService, private cd: ChangeDetectorRef, private service: CompanyMasterService, private regService: RegularisationService) {

    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId
    this.empPerId = this.loggedUser.empPerId
    this.empId = this.loggedUser.empId;
    for (let i = 0; i < this.leaves.length; i++) {
      this.leaves[i].check = false
    }
    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    this.leaveSuggestionModel.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.leaveSuggestionModel.toDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
  }
  ngOnInit() {
    this.getLeaveList();
    this.getMissPunchRequests();
    this.getPermissionRequests();
    this.getLateEarlyRequests();
    this.getOnDutyRequests();
    this.getWorkFromHomeRequests();
    this.getCoffRequests();
    this.getExtraWorkRequests();
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
    this.getCoffCancelList();
  }
  baseurl = MainURL.HostUrl;
  getLeaveList() {
    this.regService.getLeaveList(this.empId).subscribe(data => {
      this.leaveList = data.result;
      for (let i = 0; i < this.leaveList.length; i++) {
        this.leaveList[i].endDate = new Date(this.leaveList[i].endDate).getDate() + " " + this.monthNames[new Date(this.leaveList[i].endDate).getMonth()] + " " + new Date(this.leaveList[i].endDate).getFullYear();
        this.leaveList[i].startDate = new Date(this.leaveList[i].startDate).getDate() + " " + this.monthNames[new Date(this.leaveList[i].startDate).getMonth()] + " " + new Date(this.leaveList[i].startDate).getFullYear();
        this.leaveList[i].handed = this.handed;
        this.leaveList[i].notified = this.notified;

        this.regService.getNotifyAndHanded(this.leaveList[i].leaveRequestId).subscribe(resp => {
          this.leaveList[i].handed = resp.result.employeeHandedResDtos;
          this.leaveList[i].notified = resp.result.employeeNotifyResDtos;

        })
      }
    })
  }
  getMissPunchRequests() {
    this.regService.getMissPunchRequests(this.empId).subscribe(data => {
      this.missPunchRequests = data.result;

    },
      err => {

      })
  }
  getPermissionRequests() {
    this.regService.getPermissionRequests(this.empId).subscribe(data => {
      this.permissionRequests = data.result;
    },
      err => {

      })
  }
  getLateEarlyRequests() {
    this.regService.getLateEarlyRequests(this.empId).subscribe(data => {
      this.lateEarlyRequests = data.result;

    },
      err => {

      })
  }
  getOnDutyRequests() {
    this.regService.getOnDutyRequests(this.empId).subscribe(data => {
      this.onDutyRequests = data.result;
    },
      err => {

      })
  }
  getWorkFromHomeRequests() {
    this.regService.getWorkFromHomeRequests(this.empId).subscribe(data => {
      this.workFromRequests = data.result;
    },
      err => {

      })
  }

  getCoffRequests() {
    this.regService.getCoffRequests(this.empId).subscribe(data => {
      this.coffRequests = data.result;
    },
      err => {
      })
  }

  getExtraWorkRequests() {
    this.regService.getExtraWorkRequests(this.empId).subscribe(data => {
      this.extraWorkRequests = data.result;
    },err => {

    })
  }
  getLeaveCancelList() {
    this.regService.getLeaveCancelList(this.loggedUser.empId).subscribe(data => {
      this.LeaveCancelList = data.result;
      this.cancelLength = this.cancelLength + this.LeaveCancelList.length;
    }, err => {

    })
  }

  getLateEarlyCancelList() {
    this.regService.getLateEarlyCancelList(this.loggedUser.empId).subscribe(data => {
      this.LateEarlyCancelList = data.result;
      this.cancelLength = this.cancelLength + this.LateEarlyCancelList.length;
    }, err => {

    })
  }

  getMissPunchCancelList() {
    this.regService.getMissPunchCancelList(this.loggedUser.empId).subscribe(data => {
      this.missPunchCancelList = data.result;
      this.cancelLength = this.cancelLength + this.missPunchCancelList.length;
    }, err => {
    })
  }

  getOnDutyCancellList() {
    this.regService.getOnDutylList(this.loggedUser.empId).subscribe(data => {
      this.onDutylList = data.result;
      this.cancelLength = this.cancelLength + this.onDutylList.length;
    }, err => {

    })
  }
  getPermissionCancelList() {
    this.regService.getPermissionlList(this.loggedUser.empId).subscribe(data => {
      this.permissionList = data.result;
      this.cancelLength = this.cancelLength + this.permissionList.length;
    }, err => {

    })
  }

  getWorkFromHomeCancelList() {
    this.regService.getWorkFromCancelList(this.loggedUser.empId).subscribe(data => {
      this.worFromHomeCAncelList = data.result;
      this.cancelLength = this.cancelLength + this.worFromHomeCAncelList.length;
    }, err => {

    })
  }

  
  getExtraWorkFromHomeCancelList() {
    this.regService.getExtraWorkFromCancelList(this.loggedUser.empId).subscribe(data => {
      this.extraworFromHomeCAncelList = data.result;
      this.cancelLength = this.cancelLength + this.extraworFromHomeCAncelList.length;
    }, err => {
    })
  }

  getCoffCancelList() {
    this.regService.getCoffCancelList(this.loggedUser.empId).subscribe(data => {
      this.coffCancelList = data.result;
      this.cancelLength = this.cancelLength + this.coffCancelList.length;
    }, err => {

    })
  }

  checks() {
    for (let i = 0; i < this.leaveList.length; i++) {
      // this.leaveList[i].check = true;
      if (this.leaveList[i].check) {
        this.approveChecks = false;
        break;
      }
      else {
        this.approveChecks = true;
      }
    }
  }
  click() {

    this.flag1 = !this.flag1;

  }
  submit() {

  }
  check(i) {
  
  }
  checkAll() {
    if (!this.checked) {
      for (let i = 0; i < this.leaveList.length; i++) {
        this.leaveList[i].check = true;
        this.approveChecks = true;
      }
    }
    else {
      for (let i = 0; i < this.leaveList.length; i++) {
        this.leaveList[i].check = false;
        this.approveChecks = false;
      }
    }

  }



  loadData() {
    this.leaves = this.service.leavesData;
    this.length = this.leaves.length;
  }
  approve() {
    let selectedObj = new Array();
    for (let i = 0; i < this.leaveList.length; i++) {

      if (this.leaveList[i].check) {
        selectedObj.push(this.leaveList[i])
      }

    }
  }
  checkAllMissPunch() {
    if (!this.checkedMissPunch) {
      for (let i = 0; i < this.missPunchRequests.length; i++) {
        this.missPunchRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.missPunchRequests.length; i++) {
        this.missPunchRequests[i].check = false;
      }
    }
  }
  checksMissPunch() {

  }
  checkAllOnDuty() {
    if (!this.checkedOnDuty) {
      for (let i = 0; i < this.onDutyRequests.length; i++) {
        this.onDutyRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.onDutyRequests.length; i++) {
        this.onDutyRequests[i].check = false;
      }
    }
  }
  checksOnDuty() {

  }
  checkAllPermissionRequests() {
    if (!this.checkedPermissionRequests) {
      for (let i = 0; i < this.permissionRequests.length; i++) {
        this.permissionRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.permissionRequests.length; i++) {
        this.permissionRequests[i].check = false;
      }
    }
  }
  checkAllLateEarlyRequests() {
    if (!this.checkedLateEarlyRequests) {
      for (let i = 0; i < this.lateEarlyRequests.length; i++) {
        this.lateEarlyRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.lateEarlyRequests.length; i++) {
        this.lateEarlyRequests[i].check = false;
      }
    }
  }

  checkAllWorkFromHomeRequests() {
    if (!this.checkedWorkFromHomeRequests) {
      for (let i = 0; i < this.workFromRequests.length; i++) {
        this.workFromRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.workFromRequests.length; i++) {
        this.workFromRequests[i].check = false;
      }
    }
  }

  checkAllCoffRequests() {
    if (!this.checkedCoffRequests) {
      for (let i = 0; i < this.coffRequests.length; i++) {
        this.coffRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.coffRequests.length; i++) {
        this.coffRequests[i].check = false;
      }
    }
  }

  checkAllExtraWorkRequests() {
    if (!this.checkedExtraWorkRequests) {
      for (let i = 0; i < this.extraWorkRequests.length; i++) {
        this.extraWorkRequests[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.extraWorkRequests.length; i++) {
        this.extraWorkRequests[i].check = false;
      }
    }
  }
  //===================Reject Starts ==============
  rejectLeaveRequest(item) {
    this.leaveRequestModel.leaveRequestId = item.leaveRequestId;
    this.leaveRequestModel.approverRemark = item.approverRemark;
    this.leaveRequestModel.approverId = this.loggedUser.empId;
    this.regService.rejectleaveRequest(this.leaveRequestModel).subscribe(data => {
      this.getLeaveList();
      document.getElementById('LeaveCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  missPunchModel: any = {};
  onDutyModel: any = {};
  leaveRequestModel: any = {};
  permisssionRequestModel: any = {};
  lateEarlyModel: any = {};
  workFromHomeModel: any = {};
  extraWorkModel: any = {};
  coffModel: any = {};
  rejectMissPunchRequest(item) {
    this.missPunchModel.requesMissPunchId = item.requesMissPunchId
    this.missPunchModel.approverId = this.loggedUser.empId;
    this.missPunchModel.approverRemark = item.approverRemark;
    this.getMissPunchRequests();
    this.regService.rejectMissPunchRequest(this.missPunchModel).subscribe(data => {
      document.getElementById('missCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  rejectOnDutyRequest(item) {
    this.onDutyModel.requesODId = item.requesODId;
    this.onDutyModel.approverId = this.loggedUser.empId;
    this.onDutyModel.approverRemark = item.approverRemark;
    this.getOnDutyRequests();

    this.regService.rejectOnDutyRequest(this.onDutyModel).subscribe(data => {
      document.getElementById('OdCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  rejectPermissionRequest(item) {
    this.permisssionRequestModel.requestPRId = item.requestPRId;
    this.permisssionRequestModel.approverId = this.loggedUser.empId;
    this.permisssionRequestModel.approverRemark = item.approverRemark;
    this.getPermissionRequests();
    
    this.regService.rejectPermissionRequest(this.permissionRequests).subscribe(data => {
      document.getElementById('PerCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  rejectLateEarlyRequest(item) {
    this.lateEarlyModel.requesLateEarlyId = item.requesLateEarlyId;
    this.lateEarlyModel.approverId = this.loggedUser.empId;
    this.lateEarlyModel.approverRemark = item.approverRemark;
    this.getLateEarlyRequests();
    this.regService.rejectLateEarlyRequest(this.lateEarlyModel).subscribe(data => {
      document.getElementById('LateCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  rejectWorkFromHomeRequest(item) {
    this.workFromHomeModel.requestWFHId = item.requestWFHId;
    this.workFromHomeModel.approverId = this.loggedUser.empId;
    this.workFromHomeModel.approverRemark = item.approverRemark;
    this.getWorkFromHomeRequests();
    this.regService.rejectWorkFromHomeRequest(this.workFromHomeModel).subscribe(data => {
      document.getElementById('WorkCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  
  rejectExtraWorkRequest(item){
    this.extraWorkModel.extraWorkingRequestID = item.extraWorkingRequestID;
    this.extraWorkModel.approverId = this.loggedUser.empId;
    this.extraWorkModel.approverRemark = item.approverRemark;
    this.extraWorkModel.compId = this.compId;
    this.getCoffRequests();
    this.regService.rejecExtraWorkRequest(this.extraWorkModel).subscribe(data => {
      document.getElementById('ExtraCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }

  rejectCoffRequest(item){
    this.coffModel.requesCoffIdId = item.requesCoffIdId;
    this.coffModel.approverId = this.loggedUser.empId;
    this.coffModel.approverRemark = item.approverRemark;
    this.coffModel.compId = this.compId;
    this.getCoffRequests();
    this.regService.rejectCoffRequest(this.coffModel).subscribe(data => {
      document.getElementById('CoffCol').click();
      this.toastr.success('Successfully Rejected!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  //===================Reject Ends ==============


  //===================Approve Start ==============
  approveLeaveRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.leaveList.length; i++) {

      if (this.leaveList[i].check) {

        let obj = {
          leaveRequestId: this.leaveList[i].leaveRequestId,
          approverId: this.loggedUser.empId,
          approverRemark: ""
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approveleaveRequest(selectedObj).subscribe(data => {
      document.getElementById('LeaveCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  approveMissPunchRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.missPunchRequests.length; i++) {

      if (this.missPunchRequests[i].check) {

        let obj = {
          requesMissPunchId: this.missPunchRequests[i].requesMissPunchId,
          approverId: this.loggedUser.empId,
          approverRemark: ""
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approveMissPunchRequest(selectedObj).subscribe(data => {
      document.getElementById('missCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }
  approveOnDutyRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.onDutyRequests.length; i++) {

      if (this.onDutyRequests[i].check) {
        let obj = {
          approverId: this.loggedUser.empId,
          approverRemark: "",
          requesODId: this.onDutyRequests[i].requesODId
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approveOnDutyRequest(selectedObj).subscribe(data => {
      document.getElementById('OdCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }
  approvePermissionRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.permissionRequests.length; i++) {

      if (this.permissionRequests[i].check) {
        let obj = {
          approverId: this.loggedUser.empId,
          approverRemark: "",
          requestPRId: this.permissionRequests[i].requestPRId
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approvePermissionRequest(selectedObj).subscribe(data => {
      document.getElementById('PerCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }
  approveLateEarlyRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.lateEarlyRequests.length; i++) {
      if (this.lateEarlyRequests[i].check) {
        let obj = {
          approverId: this.loggedUser.empId,
          approverRemark: "",
          requesLateEarlyId: this.lateEarlyRequests[i].requesLateEarlyId
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approveLateEarlyRequest(selectedObj).subscribe(data => {
      document.getElementById('LateCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }
  approveWorkFromHomeRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.workFromRequests.length; i++) {

      if (this.workFromRequests[i].check) {
        let obj = {
          approverId: this.loggedUser.empId,
          approverRemark: "",
          requestWFHId: this.workFromRequests[i].requestWFHId
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approveWorkFromHomeRequest(selectedObj).subscribe(data => {
      document.getElementById('WorkCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }


  approveCoffRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.coffRequests.length; i++) {
      if (this.coffRequests[i].check) {
        let obj = {
          approverId: this.loggedUser.empId,
          approverRemark: "",
          requesCoffIdId: this.coffRequests[i].requesCoffIdId
        }
        selectedObj.push(obj)
      }

    }
    this.regService.approveCoffRequest(selectedObj).subscribe(data => {
      document.getElementById('CoffCol').click();
      this.checkedCoffRequests = false;
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }

  approveExtraWorkRequest() {
    let selectedObj = new Array();
    for (let i = 0; i < this.extraWorkRequests.length; i++) {
      if (this.extraWorkRequests[i].check) {
        let obj = {
          approverId: this.loggedUser.empId,
          approverRemark: "",
          extraWorkingRequestID: this.extraWorkRequests[i].extraWorkingRequestID
        }
        selectedObj.push(obj)
      }

    }

    this.regService.approveExtraWorkRequest(selectedObj).subscribe(data => {
      document.getElementById('ExtraCol').click();
      this.checkedExtraWorkRequests = false;
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }
  //====================Approve Ends ==============
  approveLR(item) {
    this.leaveRequestModel.leaveRequestId = item.leaveRequestId;
    this.leaveRequestModel.approverId = this.loggedUser.empId;
    this.leaveRequestModel.approverRemark = item.approverRemark;
    let selectedObj = new Array();
    selectedObj.push(this.leaveRequestModel);
    this.regService.approveleaveRequest(selectedObj).subscribe(data => {
      document.getElementById('LeaveCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');
    })
  }
  approvePR(item) {
    this.permisssionRequestModel.requestPRId = item.requestPRId;
    this.permisssionRequestModel.approverId = this.loggedUser.empId;
    this.permisssionRequestModel.approverRemark = item.approverRemark;
    let selectedObj = new Array();
    selectedObj.push(this.permisssionRequestModel)
    this.regService.approvePermissionRequest(selectedObj).subscribe(data => {
      document.getElementById('PerCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }
  approveLTE(item) {
    this.lateEarlyModel.requesLateEarlyId = item.requesLateEarlyId;
    this.lateEarlyModel.approverId = this.loggedUser.empId;
    this.lateEarlyModel.approverRemark = item.approverRemark;
    let selectedObj = new Array();
    selectedObj.push(this.lateEarlyModel);
    document.getElementById('LateCol').click();
    this.regService.approveLateEarlyRequest(selectedObj).subscribe(data => {
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');
    })
  }

  approveOD(item) {
    this.onDutyModel.requesODId = item.requesODId;
    this.onDutyModel.approverId = this.loggedUser.empId;
    this.onDutyModel.approverRemark = item.approverRemark;
    let selectedObj = new Array();
    selectedObj.push(this.onDutyModel)
    this.regService.approveOnDutyRequest(selectedObj).subscribe(data => {
      document.getElementById('OdCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');
    })
  }

  approveWFH(item) {
    this.workFromHomeModel.requestWFHId = item.requestWFHId;
    this.workFromHomeModel.approverId = this.loggedUser.empId;
    this.workFromHomeModel.approverRemark = item.approverRemark;
    let selectedObj = new Array();
    selectedObj.push(this.workFromHomeModel)
    this.regService.approveWorkFromHomeRequest(selectedObj).subscribe(data => {
      document.getElementById('WorkCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })
  }

  approveMP(item){
    let obj = new Array()
    this.missPunchModel.requesMissPunchId = item.requesMissPunchId;
    this.missPunchModel.approverId = this.loggedUser.empId;
    this.missPunchModel.approverRemark = item.approverRemark;
    obj.push(this.missPunchModel);
    this.regService.approveMissPunchRequest(obj).subscribe(data => {
      document.getElementById('missCol').click();
      this.toastr.success('Successfully Approved!');
      this.getMissPunchRequests();
    }, err => {
      this.toastr.error('Failed');

    })

  }

  approveCR(item){  
    this.coffModel.requesCoffIdId = item.requesCoffIdId;
    this.coffModel.approverId = this.loggedUser.empId;
    this.coffModel.approverRemark = item.approverRemark;
    let obj = new Array();
    obj.push(this.coffModel)
    this.regService.approveCoffRequest(obj).subscribe(data => {
      document.getElementById('CoffCol').click();
      this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');

    })

  }

  approveEWR(item){    
    this.extraWorkModel.extraWorkingRequestID = item.extraWorkingRequestID;
    this.extraWorkModel.approverId = this.loggedUser.empId;
    this.extraWorkModel.approverRemark = item.approverRemark;
    this.regService.approveExtraWorkRequest(this.extraWorkModel).subscribe(data => {
    this.toastr.success('Successfully Approved!');
    }, err => {
      this.toastr.error('Failed');
    })

  }
  approveLeaveCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      leaveApproveRequestId: item.leaveRequestId
    }
    this.regService.approveLeaveCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  approveLTECancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveLateEarlyId: item.requesLateEarlyId
    }
    this.regService.approveLTECancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  approveMPCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveMissPunchId: item.requesMissPunchId

    }
    this.regService.approveMPCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  approveODCancel(item,i) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveODId: item.requesODId

    }
    this.regService.approveODCancel(obj).subscribe(data => {
    this.onDutylList.splice(i,0);
    this.getLeaveCancelList();
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  approvePRCancel(item,i) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApprovePRId: item.requestPRId
    }
    this.regService.approvePRCancel(obj).subscribe(data => {
    this.permissionList.splice(i,0);
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  approveWFHCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveWFHId: item.requestWFHId
    }
    this.regService.approveWFHCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }
  approveExtrawork(item){
    let obj = {
      empOfficialId: this.empOfficialId,
      extraWorkingApproveRequestID: item.extraWorkingRequestID
    }
    this.regService. approveExtrawork(obj).subscribe(data => {
      this.getLeaveCancelList()
      this.getLateEarlyCancelList();
      this.getMissPunchCancelList();
      this.getOnDutyCancellList();
      this.getPermissionCancelList();
      this.getWorkFromHomeCancelList();
      this.getExtraWorkFromHomeCancelList();

      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
    
  }


  approveCoffCancel(item){
    let obj = {
      empOfficialId: this.empOfficialId,
      requesCoffApproveId: item.requesCoffIdId
    }
    this.regService.approveCoffCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Approved")
    }, err => {
      this.toastr.error("Failed");
    })
  }
  //================Approve Cancel Requests ===================

  //================Reject Cancel Requests ===================

  rejectLeaveCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveLeaveRequestId: item.requesLeaveRequestId
    }
    this.regService.rejectLeaveCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  rejectLTECancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveLateEarlyId: item.requesLateEarlyId
    }
    this.regService.rejectLTECancel(obj).subscribe(data => {
      this.getLeaveCancelList()
      this.getLateEarlyCancelList();
      this.getMissPunchCancelList();
      this.getOnDutyCancellList();
      this.getPermissionCancelList();
      this.getWorkFromHomeCancelList();
      this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  rejectMPCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveMissPunchId: item.requesMissPunchId
    }
    this.regService.rejectMPCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  rejectODCancel(item,i) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveODId: item.requesODId
    }
    this.regService.rejectODCancel(obj).subscribe(data => {
   // this.onDutylList.splice(i,0)
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  rejectPRCancel(item,i) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApprovePRId: item.requestPRId

    }
    this.regService.rejectPRCancel(obj).subscribe(data => {
    this.permissionList.splice(i,0);
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected")
    }, err => {
      this.toastr.error("Failed");
    })
  }

  rejectWFHCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requestApproveWFHId: item.requestWFHId
    }
    this.regService.rejectWFHCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected");
    }, err => {
      this.toastr.error("Failed");
    })
  }

  rejectCoffCancel(item) {
    let obj = {
      empOfficialId: this.empOfficialId,
      requesCoffApproveId: item.requesCoffIdId
    }
    this.regService.rejectWFHCancel(obj).subscribe(data => {
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList();
    this.getCoffCancelList();
      document.getElementById('LeaveColCancel').click();
      this.toastr.success("Succesfully Rejected");
    }, err => {
      this.toastr.error("Failed");
    })
  }
  //================Reject Cancel Requests ===================

  // ===============Leave Reject With Block===================
 
  leaveBlockWithReject(item){

    let obj = {
      approverId: this.loggedUser.empId,
      approverRemark: item.approverRemark,
      compId: this.compId, 
      leaveRequestId: item.leaveRequestId
    }
    this.regService.leaveRejectWithBlock(obj).subscribe(data=>{
      document.getElementById('LeaveCol').click();
      this.toastr.success("Succesfully Blocked");
    },err=>{
      this.toastr.error("Failed To Block");
    })
  }

  selectFromDate(e){
  this.leaveSuggestionModel.fromDate = e;
  }

  selectToDate(e){
    this.leaveSuggestionModel.toDate = e;
  }
  item : any = {};
  modalOpen(item){
    this.item = item;
  }
  suggestDate(){
   this.leaveSuggestionModel.empPerId = this.empPerId;
   this.leaveSuggestionModel.approver1Id = this.empId;
   this.leaveSuggestionModel.leaveRequestId = this.item.leaveRequestId;
   this.regService.leaveSuggestDates(this.leaveSuggestionModel).subscribe(data=>{
     document.getElementById('LeaveCol').click();
      this.toastr.success("Succesfully Suggested Dates");
   },err=>{
     this.toastr.error("Failed To Suggested Dates");
   })
  }
  //=================Date Suggestion Ends===================

  refreshCancel(){
    this.getLeaveCancelList()
    this.getLateEarlyCancelList();
    this.getMissPunchCancelList();
    this.getOnDutyCancellList();
    this.getPermissionCancelList();
    this.getWorkFromHomeCancelList();
    this.getExtraWorkFromHomeCancelList()
    this.getCoffCancelList();
  }
}
