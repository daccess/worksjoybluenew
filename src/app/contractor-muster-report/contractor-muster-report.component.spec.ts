import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorMusterReportComponent } from './contractor-muster-report.component';

describe('ContractorMusterReportComponent', () => {
  let component: ContractorMusterReportComponent;
  let fixture: ComponentFixture<ContractorMusterReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorMusterReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorMusterReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
