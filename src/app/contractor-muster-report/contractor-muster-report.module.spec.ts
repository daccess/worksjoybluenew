import { ContractorMusterReportModule } from './contractor-muster-report.module';

describe('ContractorMusterReportModule', () => {
  let contractorMusterReportModule: ContractorMusterReportModule;

  beforeEach(() => {
    contractorMusterReportModule = new ContractorMusterReportModule();
  });

  it('should create an instance', () => {
    expect(contractorMusterReportModule).toBeTruthy();
  });
});
