import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorMusterReportComponent } from './contractor-muster-report.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorMusterReportComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorMusterReport',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorMusterReport',
                  component: ContractorMusterReportComponent
              }

          ]

  }


]
export const routing = RouterModule.forChild(appRoutes);
