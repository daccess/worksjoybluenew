import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './contractor-muster-report-routing.module';
import { ContractorMusterReportComponent } from './contractor-muster-report.component';



@NgModule({
  imports: [
    CommonModule,
    routing
    
  ],
  declarations: [
    ContractorMusterReportComponent
  ]
})
export class ContractorMusterReportModule { }
