import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-hod-approvals',
  templateUrl: './hod-approvals.component.html',
  styleUrls: ['./hod-approvals.component.css']
})
export class HodApprovalsComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  locationId: any;
  compIds: any;
  allmanpowerLabourData: any;
  labourPerId: any;
  LabourRequestListData: any;
  LabourDatas: any;
  approverRemark1: any;

  constructor(private httpservice: HttpClient, public Spinner: NgxSpinnerService, public toastr: ToastrService) { 
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
    this.locationId=this.users.roleList.locationId;
    
    this.compIds=this.users.roleList.compId;
  }

  ngOnInit() {
    this.getAllManpowerRequestApprovedByContractor()
  }
  getAllManpowerRequestApprovedByContractor() {
    let url = this.baseurl + `/getHodDeskLabourList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.allmanpowerLabourData = data["result"];

      // this.count1=this.manpowerAllData.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  ApprovedLabourRequest(e:any){
  
    this.labourPerId=e.requestStatus.labourInfo.labourPerId
    let url = this.baseurl + `/GetLabourActiveById/${this.labourPerId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.LabourRequestListData = data;
      this.LabourDatas=this.LabourRequestListData.result;

      // this.count=this.manpowerDatas.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  approveLabour(item) {
    
    let obj = {
      "labourPerId": item.labourInfo.labourPerId,
      "manReqStatusId": item.manReqStatusId,
      "remark": this.approverRemark1,
      "hodFlag": "Approved",
      "approverId": this.empids,
    };
  
    this.Spinner.show();
    let url = this.baseurl + '/createHODLabourApproved';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    
    this.httpservice.post(url, obj, { headers }).subscribe(
      data => {
        this.Spinner.hide();
        this.toastr.success('Manpower Approved Successfully');
  
        // Reload the page after a short delay
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      },
      err => {
        this.Spinner.hide();
        this.toastr.error('Failed To Approve manpower Request');
      }
    );
  }
  
  rejectLabour(item) {
    let selectedObj5: any = new Array();
    let obj = {
      "labourPerId":item.labourInfo.labourPerId,
      "manReqStatusId": item.manReqStatusId,
      "remark": item.approverRemark,
      "hodFlag": "Rejected",
      "approverId": this.empids,

    }
    selectedObj5.push(obj)
    let obj2 = {
      "commonApprovedReqDtos": selectedObj5
    }
    this.Spinner.show();
    let url = this.baseurl + '/createHODLabourApproved';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.post(url, obj, { headers }).subscribe(data => {
      this.Spinner.hide()
      setTimeout(() => {
        this.getAllManpowerRequestApprovedByContractor();
      }, 1000);

      this.toastr.success('manpower Request Rejected Successfully');
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Reject manpower Request');
    })
  }
}
