import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HodApprovalsComponent } from './hod-approvals.component';

describe('HodApprovalsComponent', () => {
  let component: HodApprovalsComponent;
  let fixture: ComponentFixture<HodApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HodApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HodApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
