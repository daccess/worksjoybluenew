import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit ,Renderer2} from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

import * as htmlToImage from 'html-to-image';
@Component({
  selector: 'app-provisionalpass',
  templateUrl: './provisionalpass.component.html',
  styleUrls: ['./provisionalpass.component.css']
})
export class ProvisionalpassComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  getByDocIds: string;
  getemployeeDetails: any;
  allEmployeedata: any;
  contractorName: any;
  LabourNames: any;
  genders: any;
  dateOfBirths: any;
  bloodGroups: any;
  contactNumbers: any;
  imageOfLabou: any;
  signature: any;
  conCompanyName: any;
  

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router: Router,private renderer: Renderer2) { 
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
  }

  ngOnInit() {
    this.getByDocIds=sessionStorage.getItem('documentIds')
    this.getAllDocumentByIds()
  }
  

  getAllDocumentByIds() {
    let url = this.baseurl + `/getFrontDeskDocumentList/${this.getByDocIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.getemployeeDetails = data;
      this.allEmployeedata=this.getemployeeDetails.result;
      debugger
      this.imageOfLabou=this.allEmployeedata.labourImage
      this.contractorName=this.allEmployeedata.contractorMaster.firstName+this.allEmployeedata.contractorMaster.lastName;
      this.LabourNames=this.allEmployeedata.firstName+this.allEmployeedata.lastName;
      this.genders=this.allEmployeedata.gender;
      this.dateOfBirths=this.allEmployeedata.dateOfBirth;
      this.bloodGroups=this.allEmployeedata.bloodGroup;
      this.contactNumbers=this.allEmployeedata.contactNumber;
      this.conCompanyName=this.allEmployeedata.contractorMaster.conCompanyName;
      this.signature=this.allEmployeedata.contractorMaster.companyMaster.signatureImage
     
  
    },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();

      });
  }

// printProvissionalPass(): void {
//  window.print()
// }
printProvissionalPass(): void {
  // Add a class to the body when printing
  this.renderer.addClass(document.body, 'print-mode');

  // Trigger the print dialog
  window.print();

  // Remove the class after printing to revert to the screen styles
  this.renderer.removeClass(document.body, 'print-mode');
}



// printDiv(id) {
//   var printContents = document.getElementById(id);

//   html2pdf(printContents, {
//     margin: 10,
//     filename: 'your_document.pdf',
//     image: { type: 'jpeg', quality: 0.98 },
//     html2canvas: { scale: 2 },
//     jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
//   });
// }
generateImage(){
 
  htmlToImage.toJpeg(document.getElementById('gatepass') as HTMLElement, { quality: 0.95 })
.then(function (dataUrl) {
var link = document.createElement('a');
link.download = 'Gate-Pass.jpeg';
link.href = dataUrl;
link.click();
});
}


}

