import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvisionalpassComponent } from './provisionalpass.component';

describe('ProvisionalpassComponent', () => {
  let component: ProvisionalpassComponent;
  let fixture: ComponentFixture<ProvisionalpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvisionalpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvisionalpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
