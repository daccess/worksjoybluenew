import { Component, ChangeDetectorRef, OnInit, OnDestroy, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, from } from 'rxjs';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { MainURL } from '../app/shared/configurl';
// import moment from 'moment';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { chatmodeldata } from '../app/shared/model/chatmodel';
import { ChatService } from '../app/shared/services/chatbotservice';
import { SpeechRecognitionService } from './speech-recognition.service';
import { MessagingService } from './shared/services/messaging.service';
import { ConnectionService } from 'ng-connection-service';
import { ToastrService } from 'ngx-toastr';
import { SessionService } from './shared/services/SessionSrvice';
import { AuthService } from './services/auth.service';
declare var google: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ChatService]
})

export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  showSearchButton: boolean;
  chatmodel: chatmodeldata
  baseurl = MainURL.HostUrl;
  subscription: any;
  title = 'Worksjoylite';
  flag: boolean = false;
  buttonFlag: boolean = false;
  question = [{ question: "hi", answer: "hello" }, { question: "how are you", answer: "fine" }];
  //amswer  = ["hello","fine"]
  data = { question: "" }
  originalChat = [];
  userquestion: any;
  useranswer: any;
  questionData = [];
  questionFlag: boolean = false;
  greetings: string;
  message: any;
  url: BehaviorSubject<any> = new BehaviorSubject<any>("");
  loggedUser: any;
  permissionModel: any = {};
  holidayModel: any = {};
  yearSelected: any;
  years = [];
  status = 'ONLINE';
  isConnected = true;
  constructor(public SessionServ: SessionService,
    private http: Http,
    private router: Router,
    private service: CompanyMasterService,
    private cdRef: ChangeDetectorRef,
    private httpClient: HttpClient,
    public chatserve: ChatService,
    private speechRecognitionService: SpeechRecognitionService,
    private messagingService: MessagingService,
    private connectionService: ConnectionService,
    public toster: ToastrService,
    private authService: AuthService) {
    this.chatmodel = new chatmodeldata();
    this.showSearchButton = true;
    this.chatmodel.question = "";
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.status = "ONLINE";
        this.toster.success('Internet Connection Available');
        
      }
      else {
        this.status = "OFFLINE";
        this.toster.error('No Internet Connection Available');
      }
    })
    this.SessionServ.ngOnInit12();
  }
  showChatBot() {
    this.flag = !this.flag;
    if(!this.originalChat.length){
     let user = sessionStorage.getItem('loggedUser');
     let logged = JSON.parse(user);
     this.loggedUser = JSON.parse(user);
     let date = new Date();
     let greetings: string = " ";
     if (date.getHours() < 12) {
       this.greetings = "Good Morning";
     }
     else if (date.getHours() >= 12 && date.getHours() < 18) {
       this.greetings = "Good Afternoon";
     }
     else if (date.getHours() > 18 && date.getHours() < 24) {
       this.greetings = "Good Evening";
     }
     if (this.loggedUser) {
       let obj = {
         questionId: 'greetings',
         question: '',
         answer: this.greetings + " " + this.loggedUser.fullName + " ..!"
       }
       this.originalChat.push(obj);
     }
     }
  }
  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }
  ngOnInit() {
    // let user = sessionStorage.getItem('loggedUser');
    // let logged = JSON.parse(user);
    // if (!logged) {
    //   this.router.navigateByUrl('/');
    // }
    // this.loggedUser = JSON.parse(user);
    // let date = new Date();
    // let greetings: string = " ";
    // if (date.getHours() < 12) {
    //   this.greetings = "Good Morning";
    // }
    // else if (date.getHours() >= 12 && date.getHours() < 18) {
    //   this.greetings = "Good Afternoon";
    // }
    // else if (date.getHours() > 18 && date.getHours() < 24) {
    //   this.greetings = "Good Evening";
    // }
    // if (this.loggedUser) {
    //   let obj = {
    //     questionId: 'greetings',
    //     question: '',
    //     answer: this.greetings + " " + this.loggedUser.fullName + " ..!"
    //   }
    //   this.originalChat.push(obj);
    // }
    // this.subscription = this.service.getEmittedValue().subscribe(item => {
    //   this.buttonFlag = item;
    //   this.cdRef.detectChanges();
    // });
    // this.scrollToBottom();
    // this.getYear();
  }

  ngOnDestroy() {
    this.speechRecognitionService.DestroySpeechObject();
  }
  getYear() {
    var today = new Date();
    this.yearSelected = today.getFullYear();
    for (var i = (this.yearSelected - 100); i <= this.yearSelected; i++) {
      this.years.push(i);
    }
  }

  onSubmit1() {
    let url = this.baseurl + '/chat';
    var body = this.data
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post(url, body, requestOptions).map(x => {
      x.json();
    });
  }

  getQuestionsLikeWise(question) {
    this.getQuestions(question).subscribe(data => {
    })
  }
  getQuestions(question) {
    this.questionData = [];
    let urlforgetQuetion = this.baseurl + '/employeeQuestion?questionRole=' + this.loggedUser.mrollMaster.roleName + '&serchText=' + question;
    return this.http.get(urlforgetQuetion).map(x => {
      if (x.json()) {
        this.questionData = x.json()['result'];
      }
      for (let i = 0; i < this.questionData.length; i++) {
        this.questionData[i].index = i;
      }
    }, err => {
      this.questionData = [];
    });
  }
  onSubmit(chatmodel) {
    let q = chatmodel;
    if (this.questionData.length) {
      for (let i = 0; i < this.questionData.length; i++) {
        if (this.questionData[i].chatBotMasterId == q.chatBotMasterId) {
          let obj;
          if (this.questionData[i].questionType == 'Static') {
            if (this.questionData[i].chatBotMasterId == 1) {
              obj = {
                questionId: this.questionData[i].chatBotMasterId,
                question: this.chatmodel.question,
                answer: "Hi " + this.loggedUser.fullName + " " + this.loggedUser.lastName
              }
            }
            else if (this.questionData[i].chatBotMasterId == 2) {
              obj = {
                questionId: this.questionData[i].chatBotMasterId,
                question: this.chatmodel.question,
                answer: "Hello " + this.loggedUser.fullName + " " + this.loggedUser.lastName
              }
            }
            else {
              obj = {
                questionId: this.questionData[i].chatBotMasterId,
                question: this.chatmodel.question,
                answer: this.questionData[i].expectedAnswer
              }
            }

            this.speakAnswer(obj.answer);

            this.originalChat.push(obj);
            document.getElementById("chatbot").scrollTop;
            this.chatmodel.question = "";
            this.filterItem('');
            break
          }
          else {
            let model = {
              chatBotMasterId: this.questionData[i].chatBotMasterId
            }
            this.loggedUser.chatBotMasterId = this.questionData[i].chatBotMasterId;
            this.chatmodel.question = chatmodel.question;
            this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
                let obj: any = {}
                if (this.questionData[i].chatBotMasterId == 11) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.empDashLeaveBalanceResDtoList
                  }
                  this.leaveBalance = data.result.empDashLeaveBalanceResDtoList;
                }
                else if (this.questionData[i].chatBotMasterId == 34) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.employeeUnderEmployee
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 33) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: "1) " + data.result.firstLevelReportingManager + " , 2) " + data.result.secondLevelReportingManager + " , 3) " + data.result.thirdLevelReportingManager
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 41) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 76) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.empDashLeaveBalanceResDtoList
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 51) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.empDashLeaveBalanceResDtoList
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 44) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    status: true
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 98) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.enchasmentCount
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 99) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.question,
                    list: data.result.enchasmentCount
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 100) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    dates: true
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 94) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.coffChatbotResDto
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 102) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.question
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 1) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: "Hi " + this.loggedUser.fullName + " " + this.loggedUser.lastName
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 24) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.departmentList
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 25) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.functionUnitList
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 27) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns,
                    list: data.result.bussinessGroupList
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 29) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: '',
                    list: data.result.employeeTypeList
                  }
                }
                else if (this.questionData[i].chatBotMasterId == 31) {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: '',
                    list: data.result.locationList
                  }
                }
                else {
                  obj = {
                    questionId: this.questionData[i].chatBotMasterId,
                    question: this.chatmodel.question,
                    answer: data.result.questionAns
                  }
                }


                this.speakAnswer(obj.answer);
                this.originalChat.push(obj);
                document.getElementById("chatbot").scrollTop;
                // this.scrollToBottom();
                this.scrollToBottom();
                this.chatmodel.question = "";
                this.filterItem('');
              },
                (err: HttpErrorResponse) => {
                });

          }

        }
      }
    }
    else {
      let question = this.chatmodel.question;
      let obj = {
        questionId: '',
        question: question,
        answer: 'Please ask another question'
      }
      this.chatmodel.question = '';
      this.originalChat.push(obj);
      let model = {
        answeredOrNot: "no",
        compId: this.loggedUser.compId,
        empOfficialId: this.loggedUser.empOfficialId,
        onDateTime: new Date(),
        question: question,
        questionType: "Dynamic"
      }
      this.chatmodel.question = '';
      this.chatserve.addNewQuestion(model).subscribe(data => {
      }, err => {
      })

    }

  }
  regulariseYes() {
    this.router.navigateByUrl('layout/otherrequests/otherrequests');
  }
  regulariseNo() {

    let obj = {
      questionId: '',

      answer: " have a good day "
    }
    // this.originalChat.push(obj);
  }
  updateScroll() {
    var element = document.getElementById("chatbot");
    element.scrollTop = element.scrollHeight;
  }
  leaveBalance = [];
  getLeaveBalance(data) {
    for (let i = 0; i < this.leaveBalance.length; i++) {
      if (this.leaveBalance[i].leaveCode == data.leaveCode) {
        let obj = {
          questionId: '',
          question: data.leaveCode,
          answer: "Your " + this.leaveBalance[i].leaveName + " Balance is " + this.leaveBalance[i].currentLeaves,
        }
        this.originalChat.push(obj);

      }
    }
  }

  getCarryForward(item, id) {
    this.loggedUser.leaveId = item.leaveId;
    this.loggedUser.chatBotMasterId = id;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: item.leaveCode,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
    })
  }

  selectFromDate(e) {
    this.permissionModel.fromDate = e;
  }

  selectToDate(e) {
    this.permissionModel.toDate = e;
  }

  showPrCount(id) {
    this.loggedUser.fromDate = this.permissionModel.fromDate;
    this.loggedUser.toDate = this.permissionModel.toDate;
    this.loggedUser.chatBotMasterId = id;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: this.permissionModel.fromDate + " - " + this.permissionModel.toDate,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
    })
  }

  selectFromDateHoliday(e) {
    this.holidayModel.fromDate = e;
  }

  selectToDateHoliday(e) {
    this.holidayModel.toDate = e;
  }

  showHolidays(id) {

    this.loggedUser.fromDate = this.holidayModel.fromDate;
    this.loggedUser.toDate = this.holidayModel.toDate;
    this.loggedUser.chatBotMasterId = id;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: id,
        question: this.holidayModel.fromDate + " - " + this.holidayModel.toDate,
        answer: data.result.questionAns,
        list: data.result.holidayNameAndDate,
        dates: false
      }
      this.originalChat.push(obj);
    }, err => {
    })
  }

  selectedYear(e) {
    this.loggedUser.number = e;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: this.yearSelected,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
    })
  }

  getAttritionByDept(data1) {
    this.loggedUser.deptId = data1.deptId;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: data1.deptName,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
      let obj = {
        questionId: '',
        question: data1.deptName,
        answer: "No Data"
      }
    })
  }

  getAttritionByFunc(data1) {
    this.loggedUser.functionUnitId = data1.functionUnitId;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: data1.functionUnitName,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
      let obj = {
        questionId: '',
        question: data1.functionUnitName,
        answer: "No Data"
      }
    })
  }

  getAttritionByBusi(data1) {
    this.loggedUser.busiGroupId = data1.busiGroupId;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: data1.busiGrupName,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
      let obj = {
        questionId: '',
        question: data1.busiGrupName,
        answer: "No Data"
      }
    })
  }

  getAttritionByEmpCate(data1) {
    this.loggedUser.empTypeId = data1.empTypeId;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: data1.empTypeName,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
      let obj = {
        questionId: '',
        question: data1.empTypeName,
        answer: "No Data"
      }
    })
  }

  getAttritionByLoca(data1) {
    this.loggedUser.locationId = data1.locationId;
    this.chatserve.postChatdata(this.loggedUser).subscribe(data => {
      let obj = {
        questionId: '',
        question: data1.locationName,
        answer: data.result.questionAns
      }
      this.originalChat.push(obj);
    }, err => {
      let obj = {
        questionId: '',
        question: data1.locationName,
        answer: "No Data"
      }
    })
  }
  activateSpeechSearchMovie(): void {
    this.showSearchButton = false;

    this.speechRecognitionService.record().subscribe(
        //listener
        (value) => {
          this.chatmodel.question = value;
          this.filterItem(value);
        },
        //errror
        (err) => {
          if (err.error == "no-speech") {
            this.activateSpeechSearchMovie();
          }
        },
        //completion
        () => {
          this.showSearchButton = true;
          this.activateSpeechSearchMovie();
        });
  }

  speakAnswer(string) {
    var utterance = new SpeechSynthesisUtterance();
    utterance.voice = speechSynthesis.getVoices().filter(function (voice) { return voice.name == " "; })[0];
    utterance.text = string;
    utterance.lang = "en-US";
    utterance.volume = 1;
    utterance.rate = 1;
    utterance.pitch = 2;
    speechSynthesis.speak(utterance);
  }

  searchQuestion(event) {
  }
  filterItem(value) {
    this.arrowkeyLocation = 0;
    if (!value) {
      this.questionFlag = false;
      this.questionData = [];
    }
    if (value) {
      this.questionFlag = true;
      this.getQuestionsLikeWise(value);
    }

  }
  suggest(val) {
    this.chatmodel.question = val.question;
    this.onSubmit(val);
  }

  arrowkeyLocation = 0;

  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38:
        this.arrowkeyLocation--;
        break;
      case 40:
        this.arrowkeyLocation++;
        break;
    }
  }

  keyUp(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      if (this.questionData.length) {
        this.chatmodel.question = this.questionData[this.arrowkeyLocation].question;
        this.onSubmit(this.questionData[this.arrowkeyLocation]);
      }
      else {
        this.onSubmit("");
      }

    }
  }
  applyLeave(item) {
    this.router.navigateByUrl('/layout/leaverequest/leaverequest');
    this.filterItem('');
    this.chatmodel.question = "";
    let obj = {
      questionId: '',
      question: "Yes",
      answer: null
    }
    this.originalChat.push(obj);

    item.status = false;


  }
  focusFunction() {
  }

  @HostListener('document:keyup', ['$event'])
  @HostListener('document:click', ['$event'])
  @HostListener('document:wheel', ['$event'])
  @HostListener('document:focus', ['$event'])
  resetTimer() {
    this.authService.notifyUserAction();
  }
}