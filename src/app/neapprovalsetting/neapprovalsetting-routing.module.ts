import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NeapprovalsettingComponent } from './neapprovalsetting.component';

const routes: Routes = [
  { 
             
    path: '', component: NeapprovalsettingComponent,

        children:[
            {
                path:'',
                redirectTo : 'newapproval',
                pathMatch :'full'
                
            },
        
            {
                path:'newapproval',
                component: NeapprovalsettingComponent
            }

        ]

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NeapprovalsettingRoutingModule { }
