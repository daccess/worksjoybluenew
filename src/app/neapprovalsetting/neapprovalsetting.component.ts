import { Component, OnInit } from '@angular/core';

import { event } from 'jquery';
import { dataService } from '../shared/services/dataService';
import * as moment from 'moment'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-neapprovalsetting',
  templateUrl: './neapprovalsetting.component.html',
  styleUrls: ['./neapprovalsetting.component.css']
})
export class NeapprovalsettingComponent implements OnInit {
  themeColor: string = "nav-pills-blue";
  loggedUser: any;
  locationId: any;
  deptId: any;
  allEmpsdatas: any;
  selectedId: any;
  todate:any;
  fromdate:any;
  remarks: any;
  allListdata: any;
  firstAuthorityEmpId: any;
  divertAuthorityEmpId: any;
  empAuthoId: any;
  divertFromDate: any;
  revokeDate:any =new Date();
  divertAuthorityName: any;
  divertauthorityMiddleName: any;
  divertAuthorityLastName: any;
  divertedDate:any
  allListdatas: any;
  today:any
  
  filteredAllData: any;
  
   
  
  constructor(private httpservice:dataService,private toastr: ToastrService,private router:Router) {
 
    let user:any = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user)
    this.locationId=this.loggedUser.locationId;
    this.deptId=this.loggedUser.deptId;
    // this.today = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.today=new Date();
   }

  ngOnInit() {
    this.getAllEmployeedata()
    this.getallapprovallist()
    this.getallEmpDataRecords()
    // this.getalllistOfChangeApproval()
  }

  getAllEmployeedata(){
    let url='EmployeeListSearchByDepartmentIsSanctionAuthorityTrue';

    this.httpservice.getallempOfSectionAuthorityTrue(url,this.deptId,this.locationId).subscribe(data=>{
      console.log(data);
      this.allEmpsdatas=data['result']
    })
  }

  selectedvalue(e){
    this.selectedId=e.target.value
    console.log(this.selectedId)
  }

  selectFromDate(value) {
    this.fromdate = value;
  }
  selectToDate(value) {
    this.todate = value;
  }
  saveToDrivertApplication(f){
    let obj={
      "firstAuthorityEmpId":this.loggedUser.empId,
      "divertFromDate":this.fromdate,
      // "toDate":this.todate,
      "divertAuthorityEmpId":this.selectedId,
      // "Remark":this.remarks,
      "compId":this.loggedUser.compId,
      "revoke":'',

    }
    let url='/EmpOfficialApproverChange'
    console.log(obj)
    this.httpservice.SaveApprovalChange(url,obj).subscribe(data=>{
      this.toastr.success("successfully changed Approval")
    })
  }

  getallapprovallist(){
    
   
     
    let obj={
      "firstAuthorityEmpId":this.loggedUser.empId
    }
    this.httpservice.getallemplist(obj).subscribe(data=>{
      this.allListdata=data['result'];
      console.log("this is the data of real", this.allListdata)
    
       
    })

  }
  getallEmpDataRecords(){
    this.httpservice.getallempRecord(this.loggedUser.empId).subscribe(data=>{
      this.allListdatas=data['result'];
      console.log("this is the data", this.allListdatas)
    })
  }
  // getalllistOfChangeApproval(){
  //   let url='/EmpOfficialApproverChanges';
  //   this.httpservice.getallchangeList(url).subscribe(data=>{
  //   this.allListdata=data.result;
   
  //   })
  // }

  changeApproval(e){
   
    // this.revokeDate= moment(this.revokeDate).format('YYYY/MM/DD')
    var todayDate = new Date().toISOString().slice(0, 10);
console.log(todayDate);
    // this.divertedDate=moment(e.divertFromDate).format('MM/DD/YYYY')
   let obj={
    "firstAuthorityEmpId":this.firstAuthorityEmpId=e.firstAuthorityEmpId,
    "divertAuthorityEmpId":this.divertAuthorityEmpId=e.divertAuthorityEmpId,
    "empAuthoId":this.empAuthoId=e.empAuthoId,
    // "divertFromDate":this.divertFromDate=e.divertFromDate,
    "revoke":todayDate,
    "divertAuthorityName":this.divertAuthorityName=e.divertAuthorityName,
    "divertauthorityMiddleName":this.divertauthorityMiddleName=e.divertauthorityMiddleName,
    "divertAuthorityLastName":this.divertAuthorityLastName=e.divertAuthorityLastName,
      "empId":this.loggedUser.empId,

   }
  let url='/EmpOficialApproverDivertRevoke'
  this.httpservice.changedApprovals(url,obj).subscribe(data=>{
this.toastr.success("Revoke Approval Successfully")

this.router.navigateByUrl('/layout/myteamattendancemenu')
  })

  }
}
