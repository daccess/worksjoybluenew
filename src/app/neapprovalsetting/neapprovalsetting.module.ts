import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NeapprovalsettingComponent } from './neapprovalsetting.component';
import { NeapprovalsettingRoutingModule } from './neapprovalsetting-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    NeapprovalsettingRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [NeapprovalsettingComponent]
})
export class NeapprovalsettingModule { }
