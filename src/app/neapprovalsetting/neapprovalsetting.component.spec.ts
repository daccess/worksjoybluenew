import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeapprovalsettingComponent } from './neapprovalsetting.component';

describe('NeapprovalsettingComponent', () => {
  let component: NeapprovalsettingComponent;
  let fixture: ComponentFixture<NeapprovalsettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeapprovalsettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeapprovalsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
