import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
//import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-leave-report',
  templateUrl: './leave-report.component.html',
  styleUrls: ['./leave-report.component.css'],
  providers: [ExcelService,DatePipe]
})
export class LeaveReportComponent implements OnInit {

  AllReportData: any;
  AllReport: any;
  AllReport1: any;

  srnoCheckbox = true;
  leaveNamecheckbox = true;
  fromdateCheckbox = true;
  todateCheckbox = true;
  noofleavesCheckbox = true;
  typeCheckbox = true;
  halfdayCheckbox = true;
  currentbalaceCheckbox = true

  marked = true;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  exceldata: any;
  exceldata1 = [];

  fromDate: any;
  toDate:  any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  
  constructor(private excelService: ExcelService,private datePipe: DatePipe) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.AllReportData = sessionStorage.getItem('leavereportData')
    this.AllReport = JSON.parse(this.AllReportData);
    this.exceldata = this.AllReport;
    for (let i = 0; i < this.exceldata.length; i++) {
      for (let j = 0; j < this.exceldata[i].leaveRecordDetailResDtos.length; j++) {
        this.exceldata[i].leaveRecordDetailResDtos[j];
        this.exceldata1.push(this.exceldata[i].leaveRecordDetailResDtos[j]);
      }
    }
  }

  ngOnInit() {
  }

  exportAsXLSX(): void {
    let excelData = new Array();
    for (let i = 0; i < this.exceldata1.length; i++) {

      let obj: any = {}

      obj.fullName = this.exceldata1[i].fullName+' '+this.exceldata1[i].lastName;
      obj.empId = this.exceldata1[i].empId;


      if (this.marked1) {
        obj.sr = this.exceldata1[i]+1;
      }
      if (this.marked1) {
        obj.leaveName = this.exceldata1[i].leaveName;
      }
      if (this.marked2) {
        obj.startDate = this.exceldata1[i].startDate;
      }
      if (this.marked3) {
        obj.endDate = this.exceldata1[i].endDate;
      }
      if (this.marked4) {
        obj.currentLeaves = this.exceldata1[i].currentLeaves;
      }
      if (this.marked5) {
        obj.leaveType = this.exceldata1[i].leaveType;
      }
      if (this.marked6) {
        obj.typeOfLeave = this.exceldata1[i].typeOfLeave;
      }
      if (this.marked7) {
        obj.totalLeaves = this.exceldata1[i].totalLeaves;
      }

      excelData.push(obj);
    }

    this.excelService.exportAsExcelFile(excelData, 'Leave_report');
  }

  toggleVisibility(e) {
    this.marked = e.target.checked;
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }

  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }

  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }


  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }

  public captureScreen() {
    let item9 = {
      header: "Sr",
      // header:"",
      title: "Sr"
    }

    let item = {
      header: "Name",
      // header:"",
      title: "xyz"
    }
    let item1 = {
      header: "EmpId",
      // header:"",
      title: "pqr"
    }

    let item2 = {
      header: "Leave Name",
      // header:"",
      title: "abc"
    }
    let item3 = {
      // header:"Shift",
      header: "from date",
      title: "cde"
    }
    let item4 = {
      // header:"Attendance date",
      header: "To date",
      title: "efg"
    }
    let item5 = {
      // header:"In Time",
      header: "leaves",
      title: "ghi"
    }
    let item6 = {
      // header:"In Time",
      header: "type",
      title: "ghi"
    }
    let item7 = {
      // header:"In Time",
      header: "Half day",
      title: "ghi"
    }
    let item8 = {
      // header:"In Time",
      header: "current Balance",
      title: "ghi"
    }

    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [item9,item, item1, item2, item3, item4, item5, item6, item7, item8];
    var rows = [];

    var rowCountModNew = this.exceldata1;

    rowCountModNew.forEach((element, index) => {
      let obj = [index+1,element.fullName+' '+element.lastName, element.empId, element.leaveName, element.startDate, element.endDate, element.currentLeaves, element.leaveType, element.typeOfLeave, element.totalLeaves]
      rows.push(obj);
    });

   

  }

  capturePDF() {
    let item9 = {
      header: "Sr.No",
      title: "ghi"
    }
    let item1 = {
      header: "EmpId",
      title: "rfg"
    }
    let item = {
      header: "Full Name",
      title: "egr"
    }
    
    let item2 = {
      header: "Leave Name",
      title: "abc"
    }
    let item3 = {
      header: "from date",
      title: "cde"
    }
    let item4 = {
      header: "To date",
      title: "efg"
    }
    let item5 = {
      header: "leaves",
      title: "ghi"
    }
    let item6 = {
      header: "type",
      title: "ghi"
    }
    let item7 = {
      header: "Half day",
      title: "ghi"
    }
    let item8 = {
      header: "current Balance",
      title: "ghi"
    }
   

    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [];
    if (this.marked) {
      col.push(item9);
    }
    col.push(item)
    col.push(item1)
   
    
    

    if (this.marked1) {
      col.push(item2);
    }
    if (this.marked2) {
      col.push(item3);
    }
    if (this.marked3) {
      col.push(item4);
    }
    if (this.marked4) {
      col.push(item5);
    }
    if (this.marked5) {
      col.push(item6);
    }
    if (this.marked6) {
      col.push(item7);
    }
    if (this.marked7) {
      col.push(item8);
    }

    var rows = [];
    var rowCountModNew = this.exceldata1;

    rowCountModNew.forEach((element , index) => {
      let obj = []


      if (this.marked) {
        obj.push(index+1);
      }
      obj.push(element.fullName+' '+element.lastName)
      obj.push(element.empId)
      if (this.marked1) {
        obj.push(element.leaveName);
      }
      if (this.marked2) {
        obj.push(element.startDate);
      }
      if (this.marked3) {
        obj.push(element.endDate);
      }
      if (this.marked4) {
        obj.push(element.currentLeaves);
      }
      if (this.marked5) {
        obj.push(element.leaveType);
      }
      if (this.marked6) {
        obj.push(element.typeOfLeave);
      }
      if (this.marked7) {
        obj.push(element.totalLeaves);
      }

      rows.push(obj);
    });

    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14 ,'Company Name :'+ this.compName)

    // doc.setTextColor(0, 0, 0)
    // doc.setFontSize(7)
    // doc.setFontStyle("Arial")
    // doc.text(34, 44 ,'Abbreviation:- GS : General Shift, SS : Store Shift')
    
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12);
    doc.setFontStyle("Arial")
    doc.text(280, 14,'Leave Report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(8)
    doc.setFontStyle("Arial")
    doc.text(280, 24 ,'From' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy") + ' ' +' To' + ' : ' + this.datePipe.transform(this.toDate,"dd-MMM-yyyy")
    )

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"))
    // doc.
    doc.autoTable(col, rows,{
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      headerStyles: {
        fillColor: [103, 132, 130],   
    },
    
      styles: {
        halign: 'center'
      },
      theme: 'grid',
      margin: { top: 50, left: 5, right: 5, bottom: 50 }
    });
    
    doc.save('Leave_Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');
  }

}
