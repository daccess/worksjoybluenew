import { Routes, RouterModule } from '@angular/router'
import { LeaveReportComponent } from './leave-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: LeaveReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leavereports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leavereports',
                    component: LeaveReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);