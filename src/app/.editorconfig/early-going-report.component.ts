import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-early-going-report',
  templateUrl: './early-going-report.component.html',
  styleUrls: ['./early-going-report.component.css'],
  providers:[ExcelService]
})
export class EarlyGoingReportComponent implements OnInit {

  AllReportData: any;
  AllReport: any;

  srnoCheckbox = true;
  attendencecheckbox = true;
  shiftintimeCheckbox = true;
  shiftouttimeCheckbox =  true;
  intimeCheckbox = true;
  outtimeCheckbox = true;
  latedurationChecckbox =  true;

  marked = true;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  exceldata: any;
  exceldata1 =[];


  constructor(private excelService:ExcelService) {

    this.AllReportData = sessionStorage.getItem('EarlyGoingreportData')
    this.AllReport = JSON.parse(this.AllReportData);

    console.log("*****vin Early Report*****",this.AllReport )


    console.log("*****vin*****",this.AllReport )
    this.exceldata = this.AllReport;
    console.log("excel",this.exceldata);

    for(let i=0; i<this.exceldata.length; i++){
      console.log("viioviov",this.exceldata[i].earlyGoingAllRecordResDtoList)
      

      if(this.exceldata[i].earlyGoingAllRecordResDtoList != null){
        for (let j = 0; j < this.exceldata[i].earlyGoingAllRecordResDtoList.length; j++) {
          this.exceldata[i].earlyGoingAllRecordResDtoList[j];
          this.exceldata1.push(this.exceldata[i].earlyGoingAllRecordResDtoList[j]);
        }
      }



     
    }

   }

  ngOnInit() {
  }

  exportAsXLSX():void {
    console.log(this.exceldata1);
    this.excelService.exportAsExcelFile(this.exceldata1, 'earlygoing_Report');
  }
  

  toggleVisibility(e){
    this.marked = e.target.checked;
    console.log("vinod", this.marked)
  }

  toggleVisibility1(e){
    this.marked1 = e.target.checked;
    console.log(this.marked1)
  }
  toggleVisibility2(e){
    this.marked2 = e.target.checked;
    console.log(this.marked2)
  }
  toggleVisibility3(e){
    this.marked3 = e.target.checked;
    console.log(this.marked3)
  }
  toggleVisibility4(e){
    this.marked4 = e.target.checked;
    console.log(this.marked4)
  }
  toggleVisibility5(e){
    this.marked5 = e.target.checked;
    console.log(this.marked5)
  }
  toggleVisibility6(e){
    this.marked6 = e.target.checked;
    console.log(this.marked6)
  }
  public captureScreen()  
  {  
    var data = document.getElementById('content');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('Early_Going_Report.pdf'); // Generated PDF   
    });  
  } 
}
