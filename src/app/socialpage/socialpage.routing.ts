import { Routes, RouterModule } from '@angular/router'


import { SocialpageComponent } from './socialpage.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: SocialpageComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'socialpage',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'socialpage',
                    component: SocialpageComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);