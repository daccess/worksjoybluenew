import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-socialpage',
  templateUrl: './socialpage.component.html',
  styleUrls: ['./socialpage.component.css']
})
export class SocialpageComponent implements OnInit {
  @ViewChild('fullScreen') divRef;

  loggedUser: any;
  firstname: any;
  lastname: any;
  profilepic: any;
  designation: any;

  constructor() { 
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    console.log("sessionnnnn data", this.loggedUser);
    this.profilepic = this.loggedUser.profilePath;
    this.firstname = this.loggedUser.fullName;
    this.lastname = this.loggedUser.lastName;
    this.designation = this.loggedUser.descName;
    console.log("locale lang",localStorage.getItem('locale'))
  }

  ngOnInit() {
    this.divRef = document.documentElement;
  }
  openFullscreen() {
    // Use this.divRef.nativeElement here to request fullscreen
    if (this.divRef.requestFullscreen) {
      this.divRef.requestFullscreen();
    } else if (this.divRef.mozRequestFullScreen) {
      /* Firefox */
      this.divRef.mozRequestFullScreen();
    } else if (this.divRef.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.divRef.webkitRequestFullscreen();
    } else if (this.divRef.msRequestFullscreen) {
      /* IE/Edge */
      this.divRef.msRequestFullscreen();
    }
  }

  
}
