import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialpageComponent } from './socialpage.component';

describe('SocialpageComponent', () => {
  let component: SocialpageComponent;
  let fixture: ComponentFixture<SocialpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
