import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainManpowerRequestComponent } from './main-manpower-request.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: MainManpowerRequestComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'mainManpowerRequest',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'mainManpowerRequest',
                  component: MainManpowerRequestComponent
              }

          ]

  }

  // otherwise redirect to home

]

export const newrouting = RouterModule.forChild(appRoutes);