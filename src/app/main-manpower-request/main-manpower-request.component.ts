import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { Http } from '@angular/http';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { RegularisationService } from '../regularisation/service/regularisation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { MessagingService } from '../shared/services/messaging.service';
import * as moment from 'moment';
import { dataService } from '../shared/services/dataService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-manpower-request',
  templateUrl: './main-manpower-request.component.html',
  styleUrls: ['./main-manpower-request.component.css']
})
export class MainManpowerRequestComponent implements OnInit {
  user: string;
  users: any;
  token: any;
  baseurl = MainURL.HostUrl;
  manpowerData: any;
  locationdata: any;
  locationId: any;
  checkedManpowerRequest: boolean = false;
  count: any;
  manRequestId: any;
  allloactionid: any;
  manpowerRequestId: any;
  empid: any;
  selectedtablelist='Approved'
  allmanpowerLabourData: any;
  allmanpowerLabourData1: any;
  roleNames: any;
  supervisorEmpIds: any;
 constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private regService: RegularisationService,public toastr:ToastrService,private router:Router){

 }
  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.allloactionid=this.users.roleList.locationIdList
    this.empid=this.users.roleList.empId;
    this.locationId=this.users.roleList.locationId;
    this.roleNames=this.users.roleList.roleName;
    this.supervisorEmpIds=this.users.roleList.contrctorEmpId
    this.getlocationData();

// this.getAllLabourData()
this.getAllLabourData1()
    
  } 
  

getlocationData(){
  let url = this.baseurl + '/getAllLocations';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.get(url,{headers}).subscribe(data => {
  
    this.locationdata = data["result"];
      this.locationId=this.locationId;
      this.getallManpowerRequest();
},
  (err: HttpErrorResponse) => {

  })

    }
    // getAllLabourData(){
    //   let url = this.baseurl + `/GetApprovedListContractor?empId=${this.empid}`;
    // const tokens = this.token;
    // const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    //   this.httpservice.get(url,{headers}).subscribe(data => {
      
    //     this.allmanpowerLabourData = data["result"];
    //       // this.locationId=this.locationdata[0].locationId
    //       this.getallManpowerRequest();
    // },
    //   (err: HttpErrorResponse) => {
    
    //   })
    
    //     }
        getAllLabourData1(){
          let url = this.baseurl + `/GetRejectedListContractor?empId=${this.empid}`;
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
          this.httpservice.get(url,{headers}).subscribe(data => {
          
            this.allmanpowerLabourData1 = data["result"];
              // this.locationId=this.locationdata[0].locationId
 
        },
          (err: HttpErrorResponse) => {
        
          })
        
            }
  getallManpowerRequest(){
    let url = this.baseurl + `/getOneContractorMultiLocationList?empId=${this.empid}`;
    const tokens = this.token;

    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.get(url,{headers}).subscribe(data => {
        this.manpowerData = data["result"];
        
        this.count=this.manpowerData.length
      },
      (err: HttpErrorResponse) => {
      });
      if(this.roleNames=='Supervisor'){
        let url = this.baseurl + `/getOneContractorMultiLocationList?empId=${this.supervisorEmpIds}`;
    const tokens = this.token;

    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.get(url,{headers}).subscribe(data => {
        this.manpowerData = data["result"];
        
        this.count=this.manpowerData.length
      },
      (err: HttpErrorResponse) => {
      });
      }
    }
    getrequestDetails(data:any){
    
this.manRequestId=data.designationMaster.desigId;
this.manpowerRequestId=data.empManpowerReqId
sessionStorage.setItem("manPowerId",this.manRequestId)
sessionStorage.setItem("manpowerids",this.manpowerRequestId)
this.router.navigateByUrl("/layout/newregularisation")
    }
   
    // getselectedList(e:any){
    //   if(e=='Approved'){
    //     this.selectedtablelist='Approved'
    //   }
    //   if(e=='Rejected'){
    //     this.selectedtablelist='Rejected'
    //   }

    // }

}
