import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { newrouting } from './main-manpower-request-routing.module';
import { MainManpowerRequestComponent } from './main-manpower-request.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    newrouting,
    FormsModule
  ],
  declarations: [
    MainManpowerRequestComponent
  ]
})
export class MainManpowerRequestModule { }
