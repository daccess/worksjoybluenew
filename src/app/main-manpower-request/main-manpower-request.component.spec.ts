import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainManpowerRequestComponent } from './main-manpower-request.component';

describe('MainManpowerRequestComponent', () => {
  let component: MainManpowerRequestComponent;
  let fixture: ComponentFixture<MainManpowerRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainManpowerRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainManpowerRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
