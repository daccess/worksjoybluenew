import { routing } from './holidaymaster.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
 import { HolidaymasterComponent } from './holidaymaster.component';
import { HolidaymComponent } from './pages/holidaym/holidaym.component';
import { HolidayMasterService } from '../shared/services/holidayMasterService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {SharedModule} from './../shared/shared.module';



@NgModule({
    declarations: [
      HolidaymasterComponent,
      HolidaymComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     SharedModule,
     NgMultiSelectDropDownModule.forRoot()
     
    ],
    providers: [HolidayMasterService]
  })
  export class HolidayModule { 
      constructor(){

      }
  }
  