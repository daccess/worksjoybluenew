import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { HolidaymasterComponent } from 'src/app/holidaymaster/holidaymaster.component';
import { HolidaymComponent } from 'src/app/holidaymaster/pages/holidaym/holidaym.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: HolidaymasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'holidaymaster',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'holidaymaster',
                    component: HolidaymComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);