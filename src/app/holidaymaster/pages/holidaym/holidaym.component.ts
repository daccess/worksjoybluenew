import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { HolidayMaster } from 'src/app/shared/model/holidayMasterModel';
import { HolidayMasterService } from '../../../shared/services/holidayMasterService'
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;

@Component({
  selector: 'app-holidaym',
  templateUrl: './holidaym.component.html',
  styleUrls: ['./holidaym.component.css']
})
export class HolidaymComponent implements OnInit {
  
 dropdownList: any;
  HolidModayMasterModel: HolidayMaster;
  selectedCompanyobj: any
  companydata: any;
  baseurl = MainURL.HostUrl;
  compId: any;
  isedit = false;
  Selectededitobj: any;
  allHolidayData: any;
  geteditid: any;
  allActiveData: any;
  flag: boolean = true;
  flag2: boolean = false;
  id: any
  name: any
  holiyidedit: any;
  checkedfull: boolean;
  checkedHalf: boolean;
  selectedval: string;
  Startval: any;
  toShow: any;
  Endval: any;
  isclear: boolean;
  loggedUser: any;
  errordata: any;
  msg: any;
  dltmodelFlag: boolean;
  dltObje: any;
  loconame: string;
  objDuplicate: string = "";
  myDate: String;
  newmyDate: String;
  year: any;
  years: number;
  date: any;
  applicabelFromDate: any;
  allHolidayList: any;
  holidayTypeName:any;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  absentDay: any = "";
  currentMonth1: any;
  currentDay:any;
  from: any;
  maxDate:any;
  minDate:any;
  todayDate:any;
  holidaytypeflag:boolean=false;
  user: string;
  users: any;
  token: any;
  empids: any;
  holidayMasterId: any;
  holiDayName: any;
  hoilDayDesc: any;
  startDate: any;
  endtDate: any;
  typeOfHalfDay: any;
  holidayType: any;
  typeOfHoliDay: any;
  selectedLocation: any;
  location_id: any;
  
 constructor(public httpService: HttpClient, public Spinner: NgxSpinnerService,private router:Router,private location: Location ,public Holiday: HolidayMasterService, private toastr: ToastrService, public chRef: ChangeDetectorRef, public sanitizer: DomSanitizer,private httpSer:HttpClient) {

    // let user = sessionStorage.getItem('loggedUser');
    // this.loggedUser = JSON.parse(user);
    // this.compId = this.loggedUser.compId;
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token;
    this.empids=this.users.roleList.empId
    this.compId=this.users.roleList.compId;
    this.HolidModayMasterModel = new HolidayMaster();
    this.currentDate =moment();
    this.HolidModayMasterModel.startDate = (new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + new Date().getFullYear();
    this.getallHolidayMasters();
    this.getAllholidayList();
  }
  ngOnInit() {
    let url1 = this.baseurl + '/Location/Active/';
    this.httpService.get(url1 +1).subscribe(data => {
        this.allActiveData = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
      this.selectToDate(this.currentDate);
    this.typeOfHoliDay = 'F';
    this.HolidModayMasterModel.holidayType = "";
    this.HolidModayMasterModel.typeOfHalfDay='F';
    this.HolidModayMasterModel.startDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    this.HolidModayMasterModel.endtDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
 }
  handler(e) {
  }
  onSubmit(f) {
    this.Spinner.show();
    this.HolidModayMasterModel.startDate = (new Date(this.HolidModayMasterModel.startDate)).getTime()
    this.HolidModayMasterModel.endtDate = (new Date(this.HolidModayMasterModel.endtDate)).getTime()
    // this.HolidModayMasterModel.holidayMasterId = this.loggedUser.holidayMasterId
    if (this.isedit == false) {
      this.HolidModayMasterModel.compId = this.compId;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      let obj:any={
        "holidayMasterId": this.holidayMasterId,
        "holiDayName": this.holiDayName,
        "hoilDayDesc": this.hoilDayDesc,
        "startDate": this.startDate,
        "endtDate": this.endtDate,
        
        "typeOfHalfDay": this.typeOfHalfDay,
        "holidayType":this.holidayType,
        "typeOfHoliDay":this.typeOfHoliDay,
        "selectedLocation":this.selectedLocation,
        "compId" :this.compId,
        "year": this.year,
      "location_id":this.location_id
      }
      let url = this.baseurl + '/Holiday';
      this.httpService.post(url,obj,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.HolidModayMasterModel.selectedLocation = [];
          f.reset();
          this.resetForm();
          this.toastr.success('Holiday-Master Information Inserted Successfully!', 'Holiday-Master');
          this.getAllholidayList();
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Holiday-Master');
          this.HolidModayMasterModel.selectedLocation = [];
        });
      this.isclear = false;
      } else {
      this.isedit=true;
      this.holidaytypeflag =true;
      this.HolidModayMasterModel.compId = this.compId;
      if(this.HolidModayMasterModel.typeOfHoliDay =='H'){
         this.valueChange(this.HolidModayMasterModel.typeOfHoliDay);
      }
      let url1 = this.baseurl + '/Holiday';
      this.HolidModayMasterModel.holidayMasterId = this.holiyidedit;

      this.httpService.put(url1, this.HolidModayMasterModel).subscribe(data => {
          this.Spinner.hide();
          f.reset();
          this.resetForm();
          this.toastr.success('Holiday-Master Information updated Successfully!', 'Holiday-Master')
           this.HolidModayMasterModel.selectedLocation = [];
          this.getallHolidayMasters();
          this.getAllholidayList()
          this.isedit = false
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'Holiday-Master');
          this.HolidModayMasterModel.selectedLocation = [];
          this.isedit = false
        });

    }

  }
 dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);
    f.reset()
    this.resetForm();
     this.Spinner.show();

  }
  dltObj(obj){
   this.dltmodelFlag = false
    this.dltObje = obj.holidayMasterId;
    this.dltObjfun(this.dltObje)
  }
  dltObjfun(object) {
     if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/deleteholiday/';
      this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully');
         this.getAllholidayList()
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error(err.error.message);
        });
    }


  }
 iseditDupe: any;
  flagHoli: boolean;
  hiddbut:boolean;
  
  errorDupicateHolidayName(holiDayName) {
    this.flagHoli = false;
    this.hiddbut = false
    if(this.isedit==true){
      if(this.objDuplicate != holiDayName.trim()){
        let url = this.baseurl + '/holidayname?compId=' + this.compId + '&holiDayName=' + holiDayName.trim();
        this.httpService.get(url).subscribe((data: any) => {
          this.errordata = data;
          this.msg = data.message
          this.toastr.error(this.msg);
          this.flagHoli = true
          this.hiddbut = true;
        },
        (err: HttpErrorResponse) => {
          this.flagHoli = false;
          this.hiddbut = false;
         });
      }
      else{
        this.flagHoli = false;
      }
    }
    else{
      this.isedit=false;
      this.flagHoli = false
      this.hiddbut = false;
      let url = this.baseurl + '/holidayname?compId=' + this.compId + '&holiDayName=' + holiDayName.trim();
      this.httpService.get(url).subscribe((data: any) => {
          this.errordata = data;
          this.msg = data.message
          this.toastr.error(this.msg);
          this.flagHoli = true
          this.hiddbut = true;
        },
        (err: HttpErrorResponse) => {
          this.flagHoli = false;
          this.hiddbut = false;
        });
    }
  }
 errorDate(event) {
    let obj = {
      compId: this.compId,
      holiDayName: this.holiDayName,
      holidayMasterId : this.holidayMasterId,
      startDate:this.startDate,
      endtDate:this.endtDate
    }
    this.errflagEndDate = false;
    this.errflagStartDate = false;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    let url = this.baseurl + '/holidaydate';
    if(this.HolidModayMasterModel.startDate){
      this.httpService.post(url, obj,{headers}).subscribe((data: any) => {
          this.errordata = data["result"];
          if(data["result"] && this.errordata.message != "Not found"){
            this.errflagStartDate = true;
            this.errflagEndDate = true;
            this.msg = data.message;
            this.toastr.error(this.msg);
          }
        },
        (err: HttpErrorResponse) => {
          if(err.error.message == "Already Exist"){
            this.errflagStartDate = true;
            this.errflagEndDate = true;
            this.msg = err.error.result.message
            this.toastr.error(this.msg);
          }
          else{
            this.errflagEndDate = false;
            this.errflagStartDate = false;
          } 
        });
    }
    else{
      this.errflagEndDate = false;
      this.errflagStartDate = false
    }
  
  }
 errflagStartDate: boolean;
  Startvalue(event) {
    this.startDate = event;
    this.Startval = event;
    this.Endval = event;
    let startDate = this.startDate;
    this.endtDate = startDate;
    this.selectToDate(this.Startval);
    let year = new Date(this.Startval).getFullYear()
    this.year = year;
    this.errflagStartDate = true;
    // if (new Date(event).getTime() > new Date(this.endtDate).getTime()) {
    //   this.endtDate = event;
    // }
  }
  enable: boolean = false;
  errflagEndDate: boolean;
  Endvalue(event) {
    this.endtDate = event;
    this.Endval = event;
    if (this.Startval <= this.Endval) {
      this.enable = false
    }
    this.errflagEndDate = true;
    
  }
  selectToDate(datenew){
    this.todayDate =moment();
    this.todayDate.format("YYYY-MM-DD");
    this.currentDate =  moment(datenew);
    if(this.currentDate == this.todayDate){
    this.currentMonth =  this.currentDate.format('MM');
      this.currentDay = this.currentDate.format('DD');
      this.currentYear = this.currentDate.format('YYYY');
        if(this.currentMonth < 10)
        this.currentMonth = '0' + this.currentMonth.toString();
        if( this.currentDay < 10)
        this.currentDay = '0' +  this.currentDay.toString();
      this.maxDate = this.currentYear+ '-' + this.currentMonth + '-' +   this.currentDay;
      this.minDate =moment(this.maxDate); 
    }else{
      this.currentMonth =  this.currentDate.format('MM');
      this.currentDay = this.currentDate.format('DD');
      this.currentYear = this.currentDate.format('YYYY');
        if(this.currentMonth < 10)
        this.currentMonth = '0' + this.currentMonth.toString();
        if( this.currentDay < 10)
        this.currentDay = '0' +  this.currentDay.toString();
      this.maxDate = this.currentYear+ '-' + this.currentMonth + '-' +   this.currentDay;
      this.minDate =moment(this.maxDate); 

    }
  }
  edit(object) {
    this.isedit = true;
    this.holiyidedit = object.holidayMasterId;
    this.isedit = true;
    this.editgrademasterfunction(this.holiyidedit)
  }
  editgrademasterfunction(getselectedgrade) {
    let urledit = this.baseurl + '/Holiday/getById/';
    this.httpService.get(urledit + this.holiyidedit).subscribe(data => {
        this.objDuplicate = data['result']['holiDayName'];
        this.HolidModayMasterModel.holiDayName = data['result']['holiDayName'];
        this.HolidModayMasterModel.hoilDayDesc = data['result']['hoilDayDesc'];
        this.HolidModayMasterModel.holidayType = data['result']['holidayType'];
        this.HolidModayMasterModel.startDate = data['result']['startDate'];
        this.HolidModayMasterModel.endtDate = data['result']['endtDate'];
        this.HolidModayMasterModel.typeOfHoliDay = data['result']['typeOfHoliDay'];
        this.HolidModayMasterModel.typeOfHalfDay = data['result']['typeOfHalfDay'];
        this.HolidModayMasterModel.selectedLocation = data['result']['selectedLocation'];
        this.HolidModayMasterModel.holidayMasterId = data['result']['holidayMasterId'];
        this.loconame = this.HolidModayMasterModel.holiDayName;
        if(this.HolidModayMasterModel.holidayType == 'o'){
          this.holidaytypeflag =true;
           }else{
             this.holidaytypeflag =false;
           }
      },
      (err: HttpErrorResponse) => {
      });
  }
  dataTableFlag: boolean = true
  getallHolidayMasters() {
    this.applicabelFromDate = new Date().valueOf();
    this.compId =  this.compId
    let url1 = this.baseurl + '/holcompulsory/';
    this.dataTableFlag = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.allHolidayData = data["result"];
        this.dataTableFlag = true;
        this.chRef.detectChanges();
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable();
        });
        }, 1000);
      },
      (err: HttpErrorResponse) => {
      });
  }
 cancel(){
   this.router.navigate(['/layout/settingpage']);
  }
  holidaytypechange(e){
    if(e == 'o'){
   this.holidaytypeflag =true;
    }else{
      this.holidaytypeflag =false;
    }
  }

  getAllholidayList(){
    let url1 = this.baseurl + '/holidayList/';
    this.httpService.get(url1 + this.compId).subscribe(data => {
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable();
          });
        }, 1000);
        this.allHolidayList = data["result"];
        this.dataTableFlag = true;
      },
      (err: HttpErrorResponse) => {
        this.allHolidayList=[] ;
      });
  }
  valueChange(e){
    this.HolidModayMasterModel.typeOfHoliDay = e;
    if(this.HolidModayMasterModel.typeOfHoliDay == 'H'){
      this.HolidModayMasterModel.typeOfHalfDay='F';
      }
    }
  resetForm() {
     setTimeout(() => {
      this.errflagEndDate = false;
      this.errflagStartDate = false;
      this.HolidModayMasterModel.typeOfHoliDay = 'F'
      this.HolidModayMasterModel.holidayType = ""
      this.HolidModayMasterModel.typeOfHalfDay = 'F';
      this.HolidModayMasterModel.startDate = this.todayDate;
      this.HolidModayMasterModel.endtDate = this.todayDate;
      this.selectToDate(this.HolidModayMasterModel.startDate);
     }, 1000);
  }
  reset(){
    this.isedit = false;
    this.resetForm();
  }
}
