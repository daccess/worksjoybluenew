import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidaymComponent } from './holidaym.component';

describe('HolidaymComponent', () => {
  let component: HolidaymComponent;
  let fixture: ComponentFixture<HolidaymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolidaymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidaymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
