import { routing } from './empAttendenceReport.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { EmpAttendenceReportComponent } from 'src/app/emp-attendence-report/emp-attendence-report.component';


@NgModule({
    declarations: [
        EmpAttendenceReportComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: []
  })
  export class ReportAttendenceviewModule { 
      constructor(){

      }
  }
  