import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpAttendenceReportComponent } from './emp-attendence-report.component';

describe('EmpAttendenceReportComponent', () => {
  let component: EmpAttendenceReportComponent;
  let fixture: ComponentFixture<EmpAttendenceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpAttendenceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpAttendenceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
