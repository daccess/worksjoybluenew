import { Component, OnInit, ElementRef ,ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { attendencereportmodel } from '../emp-attendence-report/models/attendenceModel';
import { attendenceservice } from '../emp-attendence-report/services/attendenceservice';

import { ExcelService } from '../absentreport/excel.service';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as jspdf from 'jspdf'; 
import * as moment from 'moment'
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-emp-attendence-report',
  templateUrl: './emp-attendence-report.component.html',
  styleUrls: ['./emp-attendence-report.component.css'],
  providers: [DatePipe,attendenceservice,ExcelService]
})
export class EmpAttendenceReportComponent implements OnInit {

  myDate: any;
  baseurl = MainURL.HostUrl
  AttendenceReportdata: any;
  attenConId: any;

  toDate: any;
  fromDate: any;
  in_time:any;
  out_time:any;
  attendenceModel: attendencereportmodel;
  loggedUser: any;

  empOfficialId: any
  AllAttendenceData: any;

  //empName: any;
  fullname: any;

  srnoCheckbox = true;
  shiftcheckbox = true;
  attendanceCheckbox = true;
  intimeCheckbox = true;
  outtimeCheckbox = true;
  workingDurationCheckbox = true;
  statusofdayChecckbox =  true;
  statusChecckbox = true;

  marked = true;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8=true;
  marked9 = true;
  lastname: any;
 

  constructor(private datePipe: DatePipe, 
    public httpService: HttpClient,
    private toastr: ToastrService,
    private excelService:ExcelService,
    public attendenceServe: attendenceservice) {
    this.myDate = new Date();
    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MMM-dd');

    this.attendenceModel = new attendencereportmodel();

    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.fullname=this.loggedUser.fullName;
    this.lastname=this.loggedUser.lastName;
   }

  ngOnInit() {
   
    }
   

  toggleVisibility(e){
    this.marked = e.target.checked;
  }

  toggleVisibility1(e){

    this.marked1 = e.target.checked;
   
  }
  
  

  toggleVisibility2(e){
    this.marked2 = e.target.checked;
  }

  toggleVisibility3(e){
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e){
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e){
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e){
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e){
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e){
    this.marked8 = e.target.checked;
  }


  public capturePDF(){
    let item1 = {
      header:"FullName",
      title : "bcd",
      title1 : "xyz"
    }
    
    let item2 = {
      header:"Shift",
      title : "efg"
    }
    let item3 = {
      header:"Attendance Date",
      title : "ghi"
    }
    let item4 = {
      header:"In Time",
      title : "jkl"
    }
    let item5 = {
      header:"Out time",
      title : "mno"
    }
    let item6 = {
      header:"Working Duration",
      title : "pqr"
    }
    let item7 = {
      header:"Status OF the Day",
      title : "stu"
    }

    let item8 = {
      header:"Status",
      title : "vwx"
    }

    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [];

    // if(this.marked1){
    //   col.push(item1);
    // }
    // if(this.marked2){

    // }
    if(this.marked1){
      col.push(item1);
    }
   

    if(this.marked2){
      col.push(item2);
    }
    if(this.marked3){
      col.push(item3);
    }
    if(this.marked4){
      col.push(item4);
    }
    if(this.marked5){
      col.push(item5);
    }
    if(this.marked6){
      col.push(item6);
    }
    if(this.marked7){
      col.push(item7);
    }
    if(this.marked8){
      col.push(item8);
    }
    var rows = [];
    var rowCountModNew = this.AllAttendenceData;

    rowCountModNew.forEach(element => {
      let obj = []

      // obj.push(element.fullName);
      // obj.push(element.empId)
      // if(this.marked1){
      //   obj.push(element.attenDate);
      // }
      // if(element.checkIn){
      //   if(element.checkIn!=null){
      //     let temp_in_time=new Date(element.checkIn);
      //     element.checkIn=moment(temp_in_time).format('HH.mm');
         
      //   }
      // }
    
      // if(element.checkOut){
      //   if(element.checkOut!=null){
      //     let temp_in_time=new Date(element.checkOut);
      //     element.checkOut=moment(temp_in_time).format('HH.mm');
         
      //   }
      // }
  
     
      if(this.marked1){
        obj.push(element.fullName+" "+element.lastName);
      
    }
    

      if(this.marked2){
        obj.push(element.shiftName);
      }
      if(this.marked3){
        obj.push(element.attenDate);
      }
      if(this.marked4){
        obj.push(element.checkIn);
      }
      if(this.marked5){
        obj.push(element.checkOut);
      }
      if(this.marked6){
        obj.push(element.workDuration);
      }
      if(this.marked7){
        obj.push(element.statusOfDay);
      }
      if(this.marked8){
        obj.push(element.attendanceStatus);
      }

    rows.push(obj);
  });
    doc.autoTable(col, rows);
    doc.save('Employee_Attendance_Report.pdf');
  }  
  // public captureScreen()  
  // {  
  //   var data = document.getElementById('content');  
  //   html2canvas(data).then(canvas => {  
  //     // Few necessary setting options  
  //     var imgWidth = 208;   
  //     var pageHeight = 295;    
  //     var imgHeight = canvas.height * imgWidth / canvas.width;  
  //     var heightLeft = imgHeight;  
  
  //     const contentDataURL = canvas.toDataURL('image/png')  
  //     let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
  //     var position = 0;  
  //     pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
  //     pdf.save('Early_Going_Report.pdf'); // Generated PDF   
  //   });  
  // } 
  exportAsXLSX(){
    let excelData = new Array();
   

    for (let i = 0; i < this.AllAttendenceData.length; i++) {

      let obj : any= { }

       obj.fullName = this.AllAttendenceData[i].fullName+ " " + this.AllAttendenceData[i].lastName 
    
       //obj.lastName = this.AllAttendenceData[i].lastName;
      // obj.empId = this.exceldata1[i].empId;
      // let excel = []

    if(this.marked1){
        obj.shiftName = this.AllAttendenceData[i].shiftName
      }
      if(this.marked2){
        obj.attenDate = this.AllAttendenceData[i].attenDate
      }

      if(this.marked3){
        obj.checkIn = this.AllAttendenceData[i].checkIn
      }

      if(this.marked4){
        obj.checkOut = this.AllAttendenceData[i].checkOut
      }

      if(this.marked5){
        obj.workDuration = this.AllAttendenceData[i].workDuration
      }

      if(this.marked6){
        obj.statusOfDay = this.AllAttendenceData[i].statusOfDay
      }

      if(this.marked7){
        obj.attendanceStatus = this.AllAttendenceData[i].attendanceStatus
      }
      excelData.push(obj)
    }
    this.excelService.exportAsExcelFile(excelData, 'Employee_Attendance_Report');
  }

  getinput(){
    this.attendenceModel.empOfficialId = this.empOfficialId;
    this.attendenceModel.fromDate = this.fromDate;
    this.attendenceModel.toDate = this.toDate;
    this.attendenceServe.PostattendenceDetail(this.attendenceModel).subscribe(data => {
      this.AllAttendenceData = data["result"];
       this.toastr.success('Show Attendence Data In Detail!', 'Attendence Data');
       for(let i=0; i<this.AllAttendenceData.length;i++){
               
                if(this.AllAttendenceData[i].checkIn){
                  
                      let temp_in_time=new Date( this.AllAttendenceData[i].checkIn);
                      this.AllAttendenceData[i].checkIn= moment(temp_in_time).format('HH.mm');
                   
       }
      }
      for(let i=0; i<this.AllAttendenceData.length;i++){
               
        if(this.AllAttendenceData[i].checkOut){
          
              let temp_in_time=new Date( this.AllAttendenceData[i].checkOut);
              this.AllAttendenceData[i].checkOut= moment(temp_in_time).format('HH.mm');
           
}
}
      for(let i=0; i<this.AllAttendenceData.length;i++){
        if(this.AllAttendenceData[i].workDuration){
          var num = this.AllAttendenceData[i].workDuration;
          var hours = (num / 60);
          var rhours = Math.floor(hours);
          var minutes = (hours - rhours) * 60;
          var rminutes = Math.round(minutes);
          if(rminutes<10){
            this.AllAttendenceData[i].workDuration = rhours + ":0" + rminutes;
          }
          if(rhours<10){
            this.AllAttendenceData[i].workDuration = "0"+rhours + ":" + rminutes;
          }
          
          this.AllAttendenceData[i].workDuration = rhours + ":" + rminutes;
          this.AllAttendenceData[i].workDuration= rhours + ":" + rminutes;
          }   
          //console.log("this is minute convert to hourse",this.AllAttendenceData[i].workDuration)
        }
      
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Attendence Report');
    });
  }

  getdata(){
    this.attendenceModel.empOfficialId = this.empOfficialId;
    this.attendenceModel.fromDate = this.fromDate;
    this.attendenceModel.toDate = this.toDate;
    this.attendenceServe.PostattendenceDetail(this.attendenceModel).subscribe(data => {
      this.AllAttendenceData = data["result"];
      for(let i=0; i<this.AllAttendenceData.length;i++){
               
        if(this.AllAttendenceData[i].checkIn){
          
              let temp_in_time=new Date( this.AllAttendenceData[i].checkIn);
              this.AllAttendenceData[i].checkIn= moment(temp_in_time).format('HH.mm');
           
}
}
for(let i=0; i<this.AllAttendenceData.length;i++){
       
if(this.AllAttendenceData[i].checkOut){
  
      let temp_in_time=new Date( this.AllAttendenceData[i].checkOut);
      this.AllAttendenceData[i].checkOut= moment(temp_in_time).format('HH.mm');
   
}
}
for(let i=0; i<this.AllAttendenceData.length;i++){
if(this.AllAttendenceData[i].workDuration){
  var num = this.AllAttendenceData[i].workDuration;
  var hours = (num / 60);
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  if(rminutes<10){
    this.AllAttendenceData[i].workDuration = rhours + ":0" + rminutes;
  }
  if(rhours<10){
    this.AllAttendenceData[i].workDuration = "0"+rhours + ":" + rminutes;
  }
  
  this.AllAttendenceData[i].workDuration = rhours + ":" + rminutes;
  this.AllAttendenceData[i].workDuration= rhours + ":" + rminutes;
  }   
  //console.log("this is minute convert to hourse",this.AllAttendenceData[i].workDuration)
}

    },
    (err: HttpErrorResponse) => {
    });

  }

  timeDuration: string;
//   timeConvert(n) {
//     var num = n;
//     var hours = (num / 60);
//     var rhours = Math.floor(hours);
//     var minutes = (hours - rhours) * 60;
//     var rminutes = Math.round(minutes);
//     if(rminutes<10){
//       this.timeDuration = rhours + ":0" + rminutes;
//     }
//     if(rhours<10){
//       this.timeDuration = "0"+rhours + ":" + rminutes;
//     }
    
//     this.timeDuration = rhours + ":" + rminutes;
//     return rhours + ":" + rminutes;
//     }   
 }
