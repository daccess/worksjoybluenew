import { Routes, RouterModule } from '@angular/router'

import { EmpAttendenceReportComponent } from 'src/app/emp-attendence-report/emp-attendence-report.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: EmpAttendenceReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'viewattendencereport',
                    pathMatch :'full'
                    
                },
                {
                    path:'viewattendencereport',
                    component: EmpAttendenceReportComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);