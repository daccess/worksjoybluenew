import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MainURL } from '../../../shared/configurl';
import * as moment from 'moment';
import { dataService } from 'src/app/shared/services/dataService';
import { ToastrService } from 'ngx-toastr';
import { WorkDurationRepoortservice } from "../../services/workDurationReportService";
import { InfoService } from "../../../shared/services/infoService";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { HostListener } from '@angular/core';
import { MultiplePunchesRepoortservice } from '../../services/multiplepunchesreport';
@Component({
  selector: 'app-contractorreports',
  templateUrl: './contractorreports.component.html',
  styleUrls: ['./contractorreports.component.css']
})
export class ContractorportsComponent implements OnInit {
  //Declarations
  baseurl = MainURL.HostUrl;
  themeColor:any;
  compId;
  loggedUser;
  Select:any ='Select Reports';
  model: any = {};
  //contractor
  allContractorList:any;
  contractor_service_list:any;
  allServiceContractorArr=[];
  contractor_data=[];
  customizeReportContractorReqDtos=[];
  ColumnReqDto = []
  workDurationColumn = [];
  RegisterReportsColumns=[];
  Duration_ColumnReqDto=[];
  masterSelected=true;
  selected_report;
  AllDepartments=[];
  departments_url='Department';
  all_contractors=true;
  daysDifference = [];
  getDate = [];
  empOfficialId:any;
  alldepartment: any;
  //register report changes
  allLocoList: any;
  customizeReportLocationReqDtos = [];
  allLocoL: any;
  //emp category
  AllEMp: any;
  customizeReportEmpCategoryReqDtos = [];
  allEmpCat: any;
  //shift
  allShiftList: any;
  customizeReportShiftReqDtos = [];
  AllShiftGrp: boolean;
  customizeReportDepartmentReqDtos = [];
  department_data=[];
  location_data=[];
  category_data=[];
  shift_data=[];
  Date:any;
  report_name:any;
  currentDate: Date;
  maxDate: any;
  mindDate: any;
  minDate: any;
  valueenddate: string;
  dateshowFalg:boolean=true;
  report_wise="customise";
  Employeedata: any;
  dropdownSettings: any;
  selectedEmp = [];
  Multipunch_ColumnReqDto=[];
  multiPulPunchColumn = []
  LocationID: number;
  constructor(public httpService: HttpClient,public chRef: ChangeDetectorRef,private dataservice:dataService,private toastr: ToastrService,public workdurationserve: WorkDurationRepoortservice,private InfoService:InfoService,private router: Router,public Spinner: NgxSpinnerService,public multipleReportServe: MultiplePunchesRepoortservice) { 
    //set theme color
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.LocationID=this.loggedUser.locationId;
    this.report_name = 'Select'
    $(function() {
      var start = moment();
      var end = moment();
      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
      (<any>$('#reportrange')).daterangepicker({
          startDate: start,
          endDate: end,
          maxDate:start,
          autoApply:true,
          opens:'right',
          ranges: {}
      }, cb);
      cb(start, end);
      (<any>$('#reportrange')).on('apply.daterangepicker', function(ev, picker) {
          let start=picker.startDate.format('YYYY-MM-DD');
          let end=picker.endDate.format('YYYY-MM-DD');
          sessionStorage.setItem("selected_start", start);
          sessionStorage.setItem("selected_end", end);
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          $('#reportrange').val('');
      });
  });
  }

  ngOnInit() {
    this.getServicTypeList();
  
    this.workDurationColumn = [
      { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
      { Columnsid: 2, ColumnName: "Name",isSelected:true},
      { Columnsid: 3, ColumnName: "Category",isSelected:true},
      { Columnsid: 4, ColumnName: "Branch",isSelected:true},
      { Columnsid: 5, ColumnName: "Shift",isSelected:true},
      { Columnsid: 6, ColumnName: "Attendance Date",isSelected:true},
      { Columnsid: 7, ColumnName: "In-Time",isSelected:true},
      { Columnsid: 8, ColumnName: "Out-Time",isSelected:true},
      { Columnsid: 9, ColumnName: "Working Duration",isSelected:true},
      { Columnsid: 10, ColumnName: "Status of day",isSelected:true},
      { Columnsid: 11, ColumnName: "Department",isSelected:true},
      { Columnsid: 13, ColumnName: "attendanceStatus",isSelected:true},
      { Columnsid: 4, ColumnName: "Designation",isSelected:true},
    ];
    this.RegisterReportsColumns = [
      { Columnsid: 1, ColumnName: "conFirmName",isSelected:true},
      { Columnsid: 2, ColumnName: "conName",isSelected:true},
      { Columnsid: 3, ColumnName: "conContactNo",isSelected:true},
      { Columnsid: 4, ColumnName: "conEmail",isSelected:true},
      { Columnsid: 5, ColumnName: "conAddress",isSelected:true},
      { Columnsid: 6, ColumnName: "contNameAddress",isSelected:true},
      { Columnsid: 7, ColumnName: "startDate",isSelected:true},
      { Columnsid: 8, ColumnName: "expDate",isSelected:true},
      { Columnsid: 9, ColumnName: "noOfManpwrApproved",isSelected:true},
      { Columnsid: 10, ColumnName: "labLicStartDate",isSelected:true},
      { Columnsid: 11, ColumnName: "labLicExpDate",isSelected:true},
      { Columnsid: 12, ColumnName: "wcNo",isSelected:true},
      { Columnsid: 13, ColumnName: "wcExpDate",isSelected:true},

      { Columnsid: 14, ColumnName: "epfRegNo",isSelected:true},
      { Columnsid: 15, ColumnName: "gstRegNo",isSelected:true},
      { Columnsid: 16, ColumnName: "pfApplicable",isSelected:true},
      { Columnsid: 17, ColumnName: "pfNumber",isSelected:true},
      { Columnsid: 18, ColumnName: "esicApplicble",isSelected:true},
      { Columnsid: 19, ColumnName: "esicfRegNo",isSelected:true},
      { Columnsid: 20, ColumnName: "lwfApplicable",isSelected:true},
      { Columnsid: 21, ColumnName: "lwFNumber",isSelected:true},
      { Columnsid: 22, ColumnName: "tinNo",isSelected:true},
      { Columnsid: 23, ColumnName: "panNo",isSelected:true},
      { Columnsid: 24, ColumnName: "status",isSelected:true},
      { Columnsid: 25, ColumnName: "conSerZTypeName",isSelected:true},
      { Columnsid: 26, ColumnName: "conSerTypeDesc",isSelected:true},
      { Columnsid: 27, ColumnName: "conGovTypeName",isSelected:true},
      { Columnsid: 28, ColumnName: "validityMonths",isSelected:true},
      { Columnsid: 29, ColumnName: "rejoinAllowed",isSelected:true},
      { Columnsid: 30, ColumnName: "extOfValidity",isSelected:true},
    ];

    this.multiPulPunchColumn = [       
      { Columnsid: 1, ColumnName: "Name",isSelected:true},
      { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
      { Columnsid: 3, ColumnName: "Department",isSelected:true},
      { Columnsid: 4, ColumnName: "Designation",isSelected:true},
      { Columnsid: 7, ColumnName: "Branch",isSelected:true},
  ]
    this.model.customizeReportDepartmentReqDtos=[];
    this.model.customizeReportEmpCategoryReqDtos=[];
    this.model.customizeReportLocationReqDtos=[];
    this.model.customizeReportShiftReqDtos=[];
    this.model.customizeReportContractorReqDtos=[];
    this.getContractorEmployess();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'empPerId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 100,
      allowSearchFilter: true
    };
  }
  //days
  periodicDates = [];
  getdays(date1, date2) {
    var isoDateString_start = new Date(date1).toISOString();
    var isoDateString_end = new Date(date2).toISOString();
    let date_start = moment(isoDateString_start).local().format("MM/DD/YYYY");
    let date_end = moment(isoDateString_end).local().format("MM/DD/YYYY");
    // console.log(date_start,+'   '+date_end);
    var startOfWeek = moment(date_start);
    var endOfWeek = moment(date_end);

    var days = [];
    var day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }
    let temp_Dates_array=[];
    for(let i=0;i<days.length;i++){
        let temps=moment(days[i]).format("YYYY-MM-DD");
        let temp= temps.match(/[^\s-]+-?/g);
        let parsed_date=parseInt(temp[2]);
        temp_Dates_array.push(parsed_date)
    }
    // console.log(temp_Dates_array);
    this.daysDifference=temp_Dates_array;
}
  SetSelectedReport(event){
    console.log(event);
    this.selected_report=event;
    if(event=='Contractor Register Report'){
      this.getDepartments();
      this.getallLocations();
      this.getAllEmpCategories();
      this.SelectAllContractors(true);
      this.all_contractors=true;
      this.report_wise="customise";

    
    }
    if(event=='Contractor Presents Report'){
      this.getDepartments();
      this.getallLocations();
      this.getAllEmpCategories();
      this.SelectAllContractors(true);
      this.all_contractors=true;
      this.report_wise="customise";

    
    }
    if(event=='Multiple Punches Report'){
      this.getDepartments();
      this.getallLocations();
      this.getAllEmpCategories();
      this.SelectAllContractors(true);
     // this.all_contractors=true;
      this.report_wise="customise";
    }
    // else{
    //   // this.report_name='Select'
    // }
    this.setDateRange();
  }
  getServicTypeList(){
    let busiurl = this.baseurl + '/CosSerType/Active/';
    this.httpService.get(busiurl + this.compId).subscribe(data => {
        console.log(data);
        let temp_data=data['result'];
        if(temp_data){
            for(let i=0;i<temp_data.length;i++){
                temp_data[i].status=false;
            }
        }
          this.contractor_service_list=temp_data;
    },
    (err: HttpErrorResponse) => {
        
    });
  }
  //getContractors
  getContractors(event,id){
    console.log(event);
    console.log(id);
    if(event){
      //get list
      let busiurl = this.baseurl + '/contractorList/';
      this.httpService.get(busiurl + id).subscribe(data => {
          this.allContractorList = data["result"];
          console.log(this.allContractorList);
          for (let i = 0; i < this.allContractorList.length; i++) {
            this.allServiceContractorArr.push(this.allContractorList[i][0]);
          }
          console.log(this.allServiceContractorArr);
          this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
          
      });
    }else{
      //uncheck select all checkbox
      this.all_contractors=false;
      //get selected contractor service types and fetch their contractors
      this.allServiceContractorArr=[];
      for(let j=0;j<this.contractor_service_list.length;j++){
        if(this.contractor_service_list[j].status){
          this.getContractors(true,this.contractor_service_list[j].conSerTypeId);
        }
      }
    }
  }
  //select all contractors
  SelectAllContractors(event) {
      console.log(event);
      for (let i = 0; i < this.allServiceContractorArr.length; i++) {
          if (event) {
              this.allServiceContractorArr[i].status = true;
          }
          else {
              this.allServiceContractorArr[i].status = false;
              this.all_contractors=false;
          }
      }
  }
 
checkallcontractorserive(){
  let flag = false;
  for(let i = 0; i < this.allServiceContractorArr.length; i++){
      if(this.allServiceContractorArr[i].status == false){
          flag = false;
          break;
      }else{
          flag = true;
      }
  }
  if(flag == true){
      this.all_contractors = true;
  }else{
      this.all_contractors = false;
  }
}
  checkUncheckAll(report_type) {
    console.log(this.masterSelected);
    this.ColumnReqDto=[];
    //work duration report
    if(report_type=='work_duration'){
        for (var i = 0; i < this.workDurationColumn.length; i++) {
            this.workDurationColumn[i].isSelected = this.masterSelected;
          }
          if(this.masterSelected){
              for (let j = 0; j < this.workDurationColumn.length; j++) {
                  let obj ={
                      field: this.workDurationColumn[j].ColumnName,
                      operator: 'String', 
                      value: 'String',
                      id : this.workDurationColumn[j].Columnsid
                  }
                  this.ColumnReqDto.push(obj)
              }
          }
    }
    //register report
    if(report_type=='register_report'){
      for (var i = 0; i < this.RegisterReportsColumns.length; i++) {
          this.RegisterReportsColumns[i].isSelected = this.masterSelected;
        }
        if(this.masterSelected){
            for (let j = 0; j < this.RegisterReportsColumns.length; j++) {
                let obj ={
                    field: this.RegisterReportsColumns[j].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.RegisterReportsColumns[j].Columnsid
                }
                this.ColumnReqDto.push(obj)
            }
        }
    }
    console.log(this.ColumnReqDto);
  }
  //Duration_ColumnReqDto
  OnDurationColumnChange(e){
    this.ColumnReqDto=[];
    console.log(e);
    if(!e){
        this.masterSelected=false;
    }
    this.Duration_ColumnReqDto=[];
    console.log(this.workDurationColumn);
    for(let i=0;i<this.workDurationColumn.length;i++){
        if(this.workDurationColumn[i].isSelected==true){
            let temp ={
                field: this.workDurationColumn[i].ColumnName,
                operator: 'String', 
                value: 'String',
                id : this.workDurationColumn[i].Columnsid
            }
            this.Duration_ColumnReqDto.push(temp);
        }
    }
    this.ColumnReqDto = this.Duration_ColumnReqDto;
    console.log(this.ColumnReqDto);
  }
  //register
  OnRegisterColumnChange(e){
    this.ColumnReqDto=[];
    console.log(e);
    if(!e){
        this.masterSelected=false;
    }
    this.Duration_ColumnReqDto=[];
    console.log(this.RegisterReportsColumns);
    for(let i=0;i<this.RegisterReportsColumns.length;i++){
        if(this.RegisterReportsColumns[i].isSelected==true){
            let temp ={
                field: this.RegisterReportsColumns[i].ColumnName,
                operator: 'String', 
                value: 'String',
                id : this.RegisterReportsColumns[i].Columnsid
            }
            this.Duration_ColumnReqDto.push(temp);
        }
    }
    this.ColumnReqDto = this.Duration_ColumnReqDto;
    console.log(this.ColumnReqDto);
  }
  //on select contractor
  getSelectedContractors(){
    this.contractor_data=[];
    for(let i=0;i<this.allServiceContractorArr.length;i++){
        if(this.allServiceContractorArr[i].status==true){
            let temp={
              "conFirmName": this.allServiceContractorArr[i].conFirmName,
              "contractorId": this.allServiceContractorArr[i].contractorId
            }
            this.contractor_data.push(temp);
        }
    }
    this.model.customizeReportContractorReqDtos = this.contractor_data;
    console.log(this.model.customizeReportContractorReqDtos);
    this.checkallcontractorserive();

  }

  getDepartments(){
    if(this.compId){
      this.dataservice.getRecordsByid(this.departments_url,this.compId).subscribe(res=>{
        if(res.statusCode==200){
          this.AllDepartments=res.result;
          for (let i = 0; i < this.AllDepartments.length; i++) {
            this.AllDepartments[i].department = true;
            this.customizeReportDepartmentReqDtos = this.AllDepartments
            this.alldepartment = true
          }
        }
      })
    }
  }

  //locations
  getallLocations() {
    if(this.loggedUser.roleHead=='Admin'){
    let busiurl = this.baseurl + '/Location/';
    this.httpService.get(busiurl + this.compId).subscribe(data => {
      this.allLocoList = data["result"];
      for (let i = 0; i < this.allLocoList.length; i++) {
          this.allLocoList[i].allLocoListdata = true;
          this.customizeReportLocationReqDtos = this.allLocoList;
          this.allLocoL = true;
      }
      this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
          console.log(err.message);
      });
    }
   else if(this.loggedUser.roleHead=='HR' ||this.loggedUser.roleHead=='Report'){
        let busiurl = this.baseurl + '/LocationWise/';
      this.httpService.get(busiurl + this.loggedUser.locationId).subscribe(data => {
        this.allLocoList = data["result"];
        for (let i = 0; i < this.allLocoList.length; i++) {
            this.allLocoList[i].allLocoListdata = true;
            this.customizeReportLocationReqDtos = this.allLocoList;
            this.allLocoL = true;
        }
        this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
      }

  }
  //get employee categories
  getAllEmpCategories() {
    let busiurl = this.baseurl + '/EmployeeType/';
    this.httpService.get(busiurl + this.compId).subscribe(data => {
      this.AllEMp = data["result"];
      for (let i = 0; i < this.AllEMp.length; i++) {
          this.AllEMp[i].AllEMpCate = true;
          this.customizeReportEmpCategoryReqDtos = this.AllEMp;
          this.allEmpCat = true;
      }
      this.chRef.detectChanges();
    },(err: HttpErrorResponse) => {
        console.log(err.message);
    });
  }
 

  //SelectAllDepartments
  SelectAllDepartments(event) {
    for (let i = 0; i < this.AllDepartments.length; i++) {
        if (event) {
            this.AllDepartments[i].department = true;
        }
        else {
            this.AllDepartments[i].department = false;
        }
    }
    this.valueDepartment();
}
valueDepartment() {
  console.log(this.customizeReportDepartmentReqDtos);
  console.log(this.AllDepartments);
  this.department_data=[];
  for(let i=0;i<this.AllDepartments.length;i++){
      if(this.AllDepartments[i].department==true){
          let temp={
              "deptId": this.AllDepartments[i].deptId
          }
          this.department_data.push(temp);
      }
  }
  this.model.customizeReportDepartmentReqDtos = this.department_data;
  console.log(this.model.customizeReportDepartmentReqDtos);
}

SelectAllLocations(event) {
  console.log(event);
  for (let i = 0; i < this.allLocoList.length; i++) {
      if (event) {
          this.allLocoList[i].allLocoListdata = true;
      }
      else {
          this.allLocoList[i].allLocoListdata = false;
      }
  }
 this.valueLocation();
}

valueLocation() {
  this.location_data=[];
  for(let i=0;i<this.allLocoList.length;i++){
      if(this.allLocoList[i].allLocoListdata==true){
          let temp={
              "locationId": this.allLocoList[i].locationId
          }
          this.location_data.push(temp);
      }
  }
  this.model.customizeReportLocationReqDtos = this.location_data;
  console.log(this.model.customizeReportLocationReqDtos);
}
SelectAllEmpCategories(event) {
  console.log(event);
  for (let i = 0; i < this.AllEMp.length; i++) {
      if (event) {
          this.AllEMp[i].AllEMpCate = true;
      }
      else {
          this.AllEMp[i].AllEMpCate = false;
      }
  }
  this.valueEmpCategoreis();
}

valueEmpCategoreis() {   
  this.category_data=[];
  console.log(this.AllEMp);
  for(let i=0;i<this.AllEMp.length;i++){
      if(this.AllEMp[i].AllEMpCate==true){
          let temp={
              "empTypeId": this.AllEMp[i].empTypeId
          }
          this.category_data.push(temp);
      }
  }
  this.model.customizeReportEmpCategoryReqDtos = this.category_data;
  console.log(this.model.customizeReportEmpCategoryReqDtos);
}
SelectAllShiftGrp(event) {
  for (let i = 0; i < this.allShiftList.length; i++) {
      if (event) {
          this.allShiftList[i].shift = true;
      }
      else {
          this.allShiftList[i].shift = false;
      }
  }
  console.log(this.allShiftList);
}
SelectShiftGrp() {
  this.shift_data=[];
  console.log(this.allShiftList);
  for(let i=0;i<this.allShiftList.length;i++){
      if(this.allShiftList[i].shift==true){
          let temp={
              "shiftMasterId": this.allShiftList[i].shiftMasterId
          }
          this.shift_data.push(temp);
      }
  }
  this.model.customizeReportShiftReqDtos = this.shift_data;
  console.log(this.model.customizeReportShiftReqDtos);
}
//Get All Contractor Employess
getContractorEmployess(){

  if(this.loggedUser.roleHead=='Admin'){
  let url = this.baseurl + '/getActiveEmpContractorList/';
  this.httpService.get(url + this.compId).subscribe(data => {
  this.Employeedata = data["result"];
  for (let index = 0; index < this.Employeedata.length; index++) {
      this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
  }
  },(err: HttpErrorResponse) => {
  console.log(err.message);
  this.Employeedata =[];
  });
}
else if(this.loggedUser.roleHead=='HR'|| this.loggedUser.roleHead=='Report'){
  let url = this.baseurl + '/ContractorMasterOperationLite/';
  let obj = {
    "empId": this.loggedUser.empId,
    "locationId":this.loggedUser.locationId,
    "roleId": this.loggedUser.rollMasterId,
    "compId": this.compId,
    
  }
  this.httpService.post(url,obj).subscribe(data => {
  this.Employeedata = data["result"];
 
  for (let index = 0; index < this.Employeedata.length; index++) {
      this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
  }
  },(err: HttpErrorResponse) => {
  console.log(err.message);
  this.Employeedata =[];
  });

}
}
//setdateRange
setDateRange(){
  let date_range={};
  date_range={};
  //test
  $(function() {
      var start = moment();
      var end = moment();
      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
      (<any>$('#reportrange')).daterangepicker({
          startDate: start,
          endDate: end,
          maxDate:start,
          autoApply:true,
          opens:'right',
          ranges: date_range
      }, cb);
      cb(start, end);
      (<any>$('#reportrange')).on('apply.daterangepicker', function(ev, picker) {
          let start=picker.startDate.format('YYYY-MM-DD');
          let end=picker.endDate.format('YYYY-MM-DD');
          sessionStorage.setItem("selected_start", start);
          sessionStorage.setItem("selected_end", end);
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          $('#reportrange').val('');
      });
  });
}
onItemSelect(item: any) {
  console.log("item", item);
  //get data
  for(let i=0;i<this.Employeedata.length;i++){
      if(this.Employeedata[i].empPerId==item.empPerId){
          let obj={
              "empOfficialId":this.Employeedata[i].empOfficialId,
              "bioId":this.Employeedata[i].bioId,
              "empPerId":this.Employeedata[i].empPerId
          }
          this.selectedEmp.push(obj);   
      }
  }
  console.log(this.selectedEmp);
}
onSelectAll(items: any) {
  console.log(items);
  this.selectedEmp = [];
  for(let j=0;j<items.length;j++){
      for(let i=0;i<this.Employeedata.length;i++){
          if(this.Employeedata[i].empPerId==items[j].empPerId){
              let obj={
                  "empOfficialId":this.Employeedata[i].empOfficialId,
                  "bioId":this.Employeedata[i].bioId,
                  "empPerId":this.Employeedata[i].empPerId
              }
              this.selectedEmp.push(obj);   
          }
      }
  }
}

onItemDeSelect(item: any) {
  for (var i = 0; i < this.selectedEmp.length; i++) {
      if (this.selectedEmp[i].empPerId == item.empPerId) {
          this.selectedEmp.splice(i, 1)
      }
  }
}

onDeSelectAll(item: any) {
  console.log(item);
  this.selectedEmp = [];
}

OnMultipleColumnChange(e){
  this.ColumnReqDto=[];
  if(!e){
      this.masterSelected=false;
  }
  this.Multipunch_ColumnReqDto=[];
  for(let i=0;i<this.multiPulPunchColumn.length;i++){
      if(this.multiPulPunchColumn[i].isSelected==true){
          let temp ={
              field: this.multiPulPunchColumn[i].ColumnName,
              operator: 'String', 
              value: 'String',
              id : this.multiPulPunchColumn[i].Columnsid
          }
          this.Multipunch_ColumnReqDto.push(temp);
      }
  }
  this.ColumnReqDto = this.Multipunch_ColumnReqDto;
}

  onCreateReport(){
    this.model.empOfficialId = this.empOfficialId;
    this.model.fromDate = sessionStorage.getItem('selected_start');
    this.model.toDate = sessionStorage.getItem('selected_end');
    //get selected columns
    if(this.selected_report=='Contractor Duration Report' || this.selected_report=='Muster Report'){
      this.OnDurationColumnChange('');
      this.getSelectedContractors();
      this.getdays(new Date(this.model.fromDate), new Date(this.model.toDate));
      if(this.model.customizeReportContractorReqDtos.length==0){
        this.toastr.error('Please select contractor service type, Contractors and continue', 'Error');
        return false;
      }
    }
    //register report
    if(this.selected_report=='Contractor Register Report'){
      //get all data
      this.valueDepartment();
      this.valueLocation();
      this.valueEmpCategoreis();
      // this.SelectShiftGrp();
      this.OnRegisterColumnChange('');
      // this.getdays(new Date(this.model.fromDate), new Date(this.model.toDate));
      console.log(this.model);
    }
    if(this.selected_report=='Contractor Presents Report'){
      //get all data
      this.valueDepartment();
      this.valueLocation();
      this.valueEmpCategoreis();
      // this.SelectShiftGrp();
      this.OnRegisterColumnChange('');
      // this.getdays(new Date(this.model.fromDate), new Date(this.model.toDate));
      console.log(this.model);
    }
    if(this.selected_report == 'Multiple Punches Report'){
      this.valueDepartment();
      this.valueLocation();
      this.valueEmpCategoreis();
      this.OnMultipleColumnChange('');
    }
    if(this.model.customizeReportDepartmentReqDtos.length==0){
      let temp={
          "deptId": 0,
          "deptName": "string"
        }
        this.model.customizeReportDepartmentReqDtos.push(temp);
    }
    if(this.model.customizeReportEmpCategoryReqDtos.length==0){
        let temp={
            "empTypeId": 0,
            "empTypeName": "string"
          }
          this.model.customizeReportEmpCategoryReqDtos.push(temp);
    }
    if(this.model.customizeReportLocationReqDtos.length==0){
        let temp={
            "locationId": 0,
            "locationName": "string"
          }
          this.model.customizeReportLocationReqDtos.push(temp);
    }
    if(this.model.customizeReportShiftReqDtos.length==0){
        let temp= {
            "shiftMasterId": 0,
            "shiftName": "string"
          }
          this.model.customizeReportShiftReqDtos.push(temp);
    }
    if(this.model.customizeReportContractorReqDtos.length==0){
        let temp= {
            "contractorId": 0,
            "conFirmName": "string"
          }
          this.model.customizeReportContractorReqDtos.push(temp);
    }
    if(this.ColumnReqDto.length==0){
        this.toastr.error('Please select columns and then continue..!', 'Error');
        return false;
    }
    console.log(this.model);
    this.model.customizeReportsConditionReqDtos = this.ColumnReqDto;

    //api call for duration report
    if(this.selected_report=='Contractor Duration Report'){
      let start = sessionStorage.getItem('selected_start');
      let end = sessionStorage.getItem('selected_end');
      this.model.fromDate = new Date(start);
      this.model.toDate = new Date(end);
      let temp_from_val= moment(this.model.fromDate);
      let temp_to_val= moment(this.model.toDate);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      if(diff>31){
          this.toastr.error('You can create this report upto 31 Days', 'Error');
          return false;
      }
      this.Spinner.show();
      this.workdurationserve.workDurationdataForContractor(this.model).subscribe((data: any) => {
          console.log(data);
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem('getTableheadworkduration', JSON.stringify(this.daysDifference));
          sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
          sessionStorage.setItem("fromDate", this.model.fromDate);
          sessionStorage.setItem("toDate", this.model.toDate);
          this.router.navigateByUrl('/layout/contractorworkingDurationreports');
          this.Spinner.hide();
      },
          (err: HttpErrorResponse) => {
              this.Spinner.hide()
              console.log(err.message);
          });
    }
    //api call for contractor register report
    if(this.selected_report=='Contractor Register Report'){
      this.Spinner.show();
      this.workdurationserve.getContractorRegisterData(this.model).subscribe((data: any) => {
          console.log(data);
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem("fromDate", this.model.fromDate);
          sessionStorage.setItem("toDate", this.model.toDate);
          this.router.navigateByUrl('/layout/contractorregisterreports');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
          this.Spinner.hide()
          console.log(err.message);
      });
    }
///Contractor Presents Report////
    if(this.selected_report=='Contractor Presents Report'){
      
      this.getallLocationeeees(this.LocationID);
    }
  
    //muster report
    if(this.selected_report=='Muster Report'){
      this.model.fromDate = new Date(this.model.fromDate);
      this.model.toDate = new Date(this.model.toDate);
      let temp_from_val= moment(this.model.fromDate);
      let temp_to_val= moment(this.model.toDate);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      if(diff>31){
          this.toastr.error('You can create this report upto 31 Days', 'Error');
          return false;
      }
      this.Spinner.show();
      this.workdurationserve.getContractorMusterDataForContractor(this.model).subscribe((data: any) => {
          console.log(data);
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem('getTableheadMuster', JSON.stringify(this.daysDifference));
          sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
          sessionStorage.setItem("fromDate", this.model.fromDate);
          sessionStorage.setItem("toDate", this.model.toDate);
          this.router.navigateByUrl('/layout/contractorMusterReport');
          this.Spinner.hide();
      },
          (err: HttpErrorResponse) => {
              this.Spinner.hide()
              console.log(err.message);
          });
    }
    //Multiple punches report
    if (this.selected_report=='Multiple Punches Report') {
      let start = sessionStorage.getItem('selected_start');
      let end = sessionStorage.getItem('selected_end');
      this.model.fromDate = new Date(start);
      this.model.toDate = new Date(end);
      this.model.reportType = "contractor";
      let temp_from_val= moment(this.model.fromDate);
      let temp_to_val= moment(this.model.toDate);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      if(diff>31){
          this.toastr.error('You can create this report upto 31 Days', 'Error');
          return false;
      }
      this.Spinner.show();
      this.multipleReportServe.multiplepunchesdataForContractor(this.model).subscribe((data: any) => {
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem("fromDate", this.model.fromDate);
          sessionStorage.setItem("toDate", this.model.toDate);
          this.router.navigateByUrl('/layout/contractorMultiplePunchReport');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
          this.Spinner.hide();
          console.log(err.message);
      });
  }
  }     
getallLocationeeees(id) {
  
  this.Spinner.show();
    let busiurl = this.baseurl + '/getTotalPresentContractorReportNewDesign/';
    this.httpService.get(busiurl + id).subscribe((data:any )=> {
      this.InfoService.changeMessage(JSON.stringify(data["result"]));
      console.log(JSON.stringify(data["result"]));
      
            sessionStorage.setItem("fromDate", this.model.fromDate);
            sessionStorage.setItem("toDate", this.model.toDate);
            
            this.router.navigateByUrl('/layout/contractorpresentreport');
            this.Spinner.hide();
  
      this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
          console.log(err.message);
      });
  }

  
  GenerateEmployeeWiseReport(){
    console.log(this.model);
    console.log(this.selectedEmp);
    this.model.fromDate = sessionStorage.getItem('selected_start');
    this.model.toDate = sessionStorage.getItem('selected_end');
    
    this.getdays(new Date(this.model.fromDate), new Date(this.model.toDate));
    if(this.selectedEmp.length==0){
        this.toastr.error('Please Select Employees And Then Continue..!!');
        return false;
    }
    let body={
        "fromDate": sessionStorage.getItem('selected_start'),
        "toDate": sessionStorage.getItem('selected_end'),
        "selectedEmp":this.selectedEmp
    }
    console.log(body);
    //Working Duration Report
    if(this.selected_report=='Contractor Duration Report'){
      let start=sessionStorage.getItem('selected_start');
      let end=sessionStorage.getItem('selected_end');
      let temp_from_val= moment(start);
      let temp_to_val= moment(end);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      if(diff>31){
          this.toastr.error('You can create this report upto 31 Days', 'Error');
          return false;
      }
      this.Spinner.show();
      this.workdurationserve.getWorkDurationEmpWise(body).subscribe((data: any) => {
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem('getTableheadworkduration', JSON.stringify(this.daysDifference));
          sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
          sessionStorage.setItem("fromDate", body.fromDate);
          sessionStorage.setItem("toDate", body.toDate);
          this.router.navigateByUrl('/layout/contractorworkingDurationreports');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
          this.Spinner.hide()
          console.log(err.message);
      });
    }
    //muster report
    if(this.selected_report=='Muster Report'){
      this.model.fromDate = new Date(this.model.fromDate);
      this.model.toDate = new Date(this.model.toDate);
      let temp_from_val= moment(this.model.fromDate);
      let temp_to_val= moment(this.model.toDate);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      if(diff>31){
          this.toastr.error('You can create this report upto 31 Days', 'Error');
          return false;
      }
      this.Spinner.show();
      this.workdurationserve.getEmpWiseContractorMusterReportForContractor(body).subscribe((data: any) => {
          console.log(data);
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem('getTableheadMuster', JSON.stringify(this.daysDifference));
          sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
          sessionStorage.setItem("fromDate", body.fromDate);
          sessionStorage.setItem("toDate", body.toDate);
          this.router.navigateByUrl('/layout/contractorMusterReport');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide()
        console.log(err.message);
      });
    }

     //multiple punches report
     if(this.selected_report=='Multiple Punches Report'){
      this.model.fromDate = new Date(this.model.fromDate);
      this.model.toDate = new Date(this.model.toDate);
      this.model.selectedEmp = this.selectedEmp;
      let temp_from_val= moment(this.model.fromDate);
      let temp_to_val= moment(this.model.toDate);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      if(diff>31){
          this.toastr.error('You can create this report upto 31 Days', 'Error');
          return false;
      }
      this.Spinner.show();
      this.multipleReportServe.multiplepunchesByEmp(this.model).subscribe((data: any) => {
          console.log(data);
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem("fromDate", this.model.fromDate);
          sessionStorage.setItem("toDate", this.model.toDate);
          this.router.navigateByUrl('/layout/contractorMultiplePunchReport');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide()
        console.log(err.message);
      });
    }
  }
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Back button pressed');
    this.router.navigateByUrl('/layout/workforcemenu');
  }

   //reset All selections
   ResetAllSelections(){
    this.model.selectedEmp=[];
    this.SetSelection();
  }
  //SetSelection
  SetSelection(){
      this.model.selectedEmp=[];
      document.getElementById("pills-home-tab").click();
  }
}
