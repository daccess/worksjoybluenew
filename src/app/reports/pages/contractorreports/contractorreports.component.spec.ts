import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkforcereportsComponent } from './workforcereports.component';

describe('WorkforcereportsComponent', () => {
  let component: WorkforcereportsComponent;
  let fixture: ComponentFixture<WorkforcereportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkforcereportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkforcereportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
