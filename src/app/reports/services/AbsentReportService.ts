import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { PostReport } from 'src/app/shared/model/postRepoModel';

@Injectable()
export class AbsentRepoort {

  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }
   postAbsentdata(Absent : PostReport){ 
    let url = this.baseurl + '/absentReportNewDesign';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  postAbsentdataEmpWise(Absent : any){ 
    let url = this.baseurl + '/absentreportlist';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  //payroll report
  postPayrolldata(Absent : any){ 
   let url = this.baseurl + '/PayRollReportForCommon';
   var body = JSON.stringify(Absent);
   var headerOptions = new Headers({'Content-Type':'application/json'});
   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
   return this.http.post(url,body,requestOptions).map(x => x.json());
 }
 

 
  
}