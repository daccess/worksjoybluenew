import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { MainURL } from '../../shared/configurl';
import { PostReport } from 'src/app/shared/model/postRepoModel';

@Injectable({
  providedIn: 'root'
})
export class LeaveReportsService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  /*Leave Taken Reports Start*/
  postLeaveTakendata(leaveReports : PostReport){ 
   let url = this.baseurl + '/leaveTakenreportAsPerNewDesign';
   var body = JSON.stringify(leaveReports);
   var headerOptions = new Headers({'Content-Type':'application/json'});
   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
   return this.http.post(url,body,requestOptions).map(x => x.json());
 }
 getLeaveTakenReportEmpWise(leaveReports : any){ 
  let url = this.baseurl + '/leaveTakenreport';
  var body = JSON.stringify(leaveReports);
  var headerOptions = new Headers({'Content-Type':'application/json'});
  var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  return this.http.post(url,body,requestOptions).map(x => x.json());
}
  /*Leave Taken Reports End*/



   


  /*Leave Register Report Start*/
  postLeaveRegisterdata(leaveReports : PostReport){ 
    let url = this.baseurl + '/getLeaveRegisterReportNewDesign';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getLeaveRegisterReportEmpWise(leaveReports : any){ 
    let url = this.baseurl + '/leaveRegisterReport';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  /*Leave Register Report End*/
 
  
  /*Leave TakenInCount Report Start*/
  postleaveTakenInCountdata(leaveReports : PostReport){ 
    let url = this.baseurl + '/leaveTakenReportInCountAsPerNewDesign';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getleaveTakenInCountReportEmpWise(leaveReports : any){ 
    let url = this.baseurl + '/leaveTakenReportInCount';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  /*Leave TakenInCount Report End*/

  /**START OF THE LEAVE BALANCE REPORT*/
  postLeaveBalance(leaveReports : PostReport){ 
    let url = this.baseurl + '/getEmpLeaveBalanceReport';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getLeaveBalanceReportEmpWise(leaveReports : any){ 
    let url = this.baseurl + '/getEmpWiseLeaveBalanceReport';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  /*END OF THE LEAVE BALANCE REPORT*/
   
   /*Leave carryForward Reports  Start*/
   postLeaveCarryForwarddata(leaveReports : PostReport){ 
    let url = this.baseurl + '/getEmpLeaveCarryForwardReport';
    var body = JSON.stringify(leaveReports);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getLeaveCarryForwardReportEmpWise(leaveReports : any){ 
   let url = this.baseurl + '/getEmpWiseLeaveCarryForwardReport';
   var body = JSON.stringify(leaveReports);
   var headerOptions = new Headers({'Content-Type':'application/json'});
   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
   return this.http.post(url,body,requestOptions).map(x => x.json());
 }
   /*LeavecarryForward Reports End*/
 
 
 

}
