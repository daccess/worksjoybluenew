import { TestBed, inject } from '@angular/core/testing';

import { WorkForceReportService } from './work-force-report.service';

describe('WorkForceReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkForceReportService]
    });
  });

  it('should be created', inject([WorkForceReportService], (service: WorkForceReportService) => {
    expect(service).toBeTruthy();
  }));
});
