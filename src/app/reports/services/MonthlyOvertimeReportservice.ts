import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { PostReport } from 'src/app/shared/model/postRepoModel';

@Injectable()
export class MonthlyOvertimeReposervice {

    baseurl = MainURL.HostUrl
    constructor(private http : Http) { }
    postMonthlyOverTimedata(MonthlyOverTime : PostReport){
     let url = this.baseurl + '/getMonthlyOvertimeReportNewDesign';
     var body = JSON.stringify(MonthlyOverTime);
     var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
     return this.http.post(url,body,requestOptions).map(x => x.json());
   }
   MonthlyOverTimeReportEmpWise(MonthlyOverTime : any){
    let url = this.baseurl + '/otreport';
    var body = JSON.stringify(MonthlyOverTime);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}