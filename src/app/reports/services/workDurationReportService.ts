import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { PostReport } from 'src/app/shared/model/postRepoModel';

@Injectable()
export class WorkDurationRepoortservice {

    baseurl = MainURL.HostUrl
    constructor(private http : Http) { }
    workDurationdata(Absent : PostReport){
     let url = this.baseurl + '/getWorkingDurationReportNewDesign';
     var body = JSON.stringify(Absent);
     var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
     return this.http.post(url,body,requestOptions).map(x => x.json());
   }
   workDurationdataForContractor(Absent : PostReport){
    let url = this.baseurl + '/getWorkingDurationReportNewDesignforContractor';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
   getWorkDurationEmpWise(Absent : any){
    let url = this.baseurl + '/workingduration';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
   //contractor
   getContractorRegisterData(Absent : PostReport){
    let url = this.baseurl + '/getContractorRegisterReport';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
   //contractor Muster Report
   getContractorMusterData(Absent : PostReport){
    let url = this.baseurl + '/getMusterReport';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getContractorMusterDataForContractor(Absent : PostReport){
    let url = this.baseurl + '/getMusterReportForContractor';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  //Emp wise Muster Report
  getEmpWiseContractorMusterReport(Absent : any){
    let url = this.baseurl + '/getEmpWiseMusterReport';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getEmpWiseContractorMusterReportForContractor(Absent : any){
    let url = this.baseurl + '/getEmpWiseMusterReportForContractor';
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  //Emp wise Query Raised Report
  getEmpWiseQueryRaisedReport(Absent : any, url: any ){
    var body = JSON.stringify(Absent);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}