import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { MainURL } from '../../shared/configurl';
import { HeadCountdata } from '../models/headCountModel';
import { from } from 'rxjs';

@Injectable()
export class HeadCountservice {
    baseurl = MainURL.HostUrl;
    constructor(private http: Http) { }
    HeadCountpostdata(headcount: HeadCountdata) {
        let url = this.baseurl + '/headcount';
        var body = JSON.stringify(headcount);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
}
