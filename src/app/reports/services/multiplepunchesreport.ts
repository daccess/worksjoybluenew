import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { PostReport } from 'src/app/shared/model/postRepoModel';

@Injectable()
export class MultiplePunchesRepoortservice {
    baseurl = MainURL.HostUrl
    constructor(private http : Http) { }
    multiplepunchesdata(multiplePunch : PostReport){
        let url = this.baseurl + '/getMultiplePunchesReportNewDesign';
        var body = JSON.stringify(multiplePunch);
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.post(url,body,requestOptions).map(x => x.json());
    }
    multiplepunchesdataForContractor(multiplePunch : PostReport){
        let url = this.baseurl + '/getMultiplePunchesReportNewDesignForContractor';
        var body = JSON.stringify(multiplePunch);
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.post(url,body,requestOptions).map(x => x.json());
    }
    multiplepunchesByEmp(multiplePunch : any){
        let url = this.baseurl + '/multiplepunchesreport';
        var body = JSON.stringify(multiplePunch);
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.post(url,body,requestOptions).map(x => x.json());
    }
}