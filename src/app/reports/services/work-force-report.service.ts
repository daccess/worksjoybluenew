import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { PostReport } from 'src/app/shared/model/postRepoModel';
import { MainURL } from '../../shared/configurl';

@Injectable({
  providedIn: 'root'
})
export class WorkForceReportService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }
  /*Employee Information data Start*/
  postEmployeeInformationdata(employeeInformation : PostReport){ 
    let url = this.baseurl + '/getEmpInformationReport';
    var body = JSON.stringify(employeeInformation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getEmployeeInformationReportEmpWise(employeeInformation : any){ 
    let url = this.baseurl + '/getEmpWiseInformationReport';
    var body = JSON.stringify(employeeInformation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  /*Employee Information data End */

  /*Joining Updates data start */
  postJoiningUpdatesdata(joiningUpdatesData : PostReport){ 
    let url = this.baseurl + '/getEmpJoiningUpdateReport';
    var body = JSON.stringify(joiningUpdatesData);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getEmpWiseJoiningUpdatesReport(joiningUpdatesData : any){ 
    let url = this.baseurl + '/getEmpWiseJoiningUpdateReport';
    var body = JSON.stringify(joiningUpdatesData);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
   /*Joining Updates data End */

    /*Leaving Updates data start */
    postleavingUpdatesdata(leavingUpdatesData : PostReport){ 
    let url = this.baseurl + '/getEmpLeavingUpdateReport';
    var body = JSON.stringify(leavingUpdatesData);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getEmpWiseleavingUpdatesReport(leavingUpdatesData : any){ 
    let url = this.baseurl + '/getEmpWiseLeavingUpdateReport';
    var body = JSON.stringify(leavingUpdatesData);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
   /*Leaving Updates data End */

  /*Confirmation Due data start */
  postconfirmationDuedata(confirmationDuedata : PostReport){ 
  let url = this.baseurl + '/getEmpConfirmationDueReport';
  var body = JSON.stringify(confirmationDuedata);
  var headerOptions = new Headers({'Content-Type':'application/json'});
  var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getEmpWiseconfirmationDueReport(confirmationDuedata : any){ 
    let url = this.baseurl + '/getEmpWiseConfirmationDueReport';
    var body = JSON.stringify(confirmationDuedata);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  /*Confirmation Due data End */
   
  /*Gratuity data start */
  postGratuitydata(gratuitydata : PostReport){ 
    let url = this.baseurl + '/getEmpGratuityReport';
    var body = JSON.stringify(gratuitydata);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
    }
    getEmpWiseGratuityReport(gratuitydata : any){ 
      let url = this.baseurl + '/getEmpWiseGratuityReport';
      var body = JSON.stringify(gratuitydata);
      var headerOptions = new Headers({'Content-Type':'application/json'});
      var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
      return this.http.post(url,body,requestOptions).map(x => x.json());
    }
  /*Gratuity  data End */

   /*Asset data start */
   postAssetdata(gratuitydata : PostReport){ 
    let url = this.baseurl + '/assetsReport';
    var body = JSON.stringify(gratuitydata);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
    }
    getEmpWiseInformationAssetReport(assetdata: any){
      let url = this.baseurl + '/getEmpWiseInformationAssetReport';
      var body = JSON.stringify(assetdata);
      var headerOptions = new Headers({'Content-Type':'application/json'});
      var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
      return this.http.post(url,body,requestOptions).map(x => x.json());
    }
  /*Asset data End */ 

 
}
