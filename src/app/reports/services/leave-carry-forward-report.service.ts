import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { MainURL } from '../../shared/configurl';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { from } from 'rxjs';
import { PostReport } from 'src/app/shared/model/postRepoModel';
@Injectable({
  providedIn: 'root'
})
export class LeaveCarryForwardReportService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }
  postLeaveCarryForwarddata(leavecarryforword : PostReport){
      let url = this.baseurl + '/leaveCaryyForwardReport';
      var body = JSON.stringify(leavecarryforword );
      var headerOptions = new Headers({'Content-Type':'application/json'});
      var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
      return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}
