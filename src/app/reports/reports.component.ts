import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReportModel } from '../shared/model/ReportModel';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { MainURL } from '../shared/configurl';
import { ReportService } from '../shared/services/ReportService';
import { AbsentRepoort } from './services/AbsentReportService';
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PostReport } from '../shared/model/postRepoModel';
import { WorkDurationRepoortservice } from './services/workDurationReportService';
import { HeadCountdata } from './models/headCountModel';
import { HeadCountservice } from './services/HeadCountservice.1';
import { leaveReportService } from './services/LeaveReportservice';
import { LateComingReportService } from './services/lateComingService';
import { EarlyGoingService } from './services/earlyGoingservice';
import { cOffReportservice } from './services/coffServices';
import { MonthlyOvertimeReposervice } from './services/MonthlyOvertimeReportservice';
import { MultiplePunchesRepoortservice } from './services/multiplepunchesreport';
import { leaveRegisterReportService } from './services/leaveRegisterReportservice';
import { leaveTakenReportDaywiseService } from './services/leaveTakenReportDaywise';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { InfoService } from "../shared/services/infoService";
import { WorkForceReportService } from './services/work-force-report.service';
import { LeaveReportsService } from './services/leave-reports.service';
import {dataService} from '../shared/services/dataService';
import { LeaveCarryForwardReportService } from './services/leave-carry-forward-report.service';
@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.css'],
    providers: [
        MultiplePunchesRepoortservice,
        MonthlyOvertimeReposervice,
        ReportService,
        AbsentRepoort,
        WorkDurationRepoortservice,
        HeadCountservice,
        leaveReportService,
        LateComingReportService,
        EarlyGoingService,
        cOffReportservice,
        leaveRegisterReportService,
        LeaveCarryForwardReportService,
        leaveTakenReportDaywiseService,
        WorkForceReportService,
        LeaveReportsService,
        DatePipe
    ]

})
export class ReportsComponent implements OnInit {
    status:any;
    Select: any;
    baseurl = MainURL.HostUrl;
    departmentselect: any;
    loggedUser: any;
    compId: any;
    departmentlistdata: any;
    serviceTypelistdata: any;
    AllGradeData: any;
    AllEMp: any;
    postRole: PostReport;
    RoleModel: ReportModel
    selectdepartmentobj: number;
    Categoryobj: number;
    selectGradeobj: number;
    EmpData: any;
    selectedItems: any
    dropdownSettings: {}
    selectedEmp = [];
    model: any = {};
    empId:any;
    reportName: any;
    openflag = true;
    activeContractorFlag = false;
    fromDateHeadCount: any;
    toDateHeadCount: any;
    bioId: any;
    HeadCountModel: HeadCountdata;
    maxdate: any;
    daysDifference = [];
    getDate = [];
    empOfficialId: any;
    fromDateToDateFlag = false;
    themeColor: string;
    reportTypeVal: string = "attendance";
    departmentList = [];
    TodaysDate: any;
    allSubDepList: any;
    allLocoList: any;
    allEmpTypeList: any;
    allSubEmpTypeList: any;
    allCostCenterList: any;
    allBuiGrpList: any;
    employmentType = [];
    SubEmploymentType = [];
    AbsentColumns = []
    ColumnsForAttend = []
    workDurationColumn = []
    musterReportColumn=[]
    multiPulPunchColumn = []
    overTimeColumns = []
    latecomingColumn =[]
    earlyGoingColumn = []
    coffEntryColumn =[]
    LeaveCol = []
    leaveTakenColumn=[]
    departmentlist: any;
    alldepartment: any
    allDep: any
    allLocoL: any
    allEpType: any
    allEmpCat: any
    allSubEmpCat: any
    allSubEpType: any
    AllContractor: any
    AllServiceTypeContractor: any;
    AllCostCenter: any
    customizeReportDepartmentReqDtos = []
    customizeReportSubDepartmentReqDtos = []
    customizeReportLocationReqDtos = []
    customizeReportEmploymentTypeReqDtos = []
    customizeReportSubEmployTypeReqDtos = []
    customizeReportEmpCategoryReqDtos = []
    customizeReportSubEmpCategoryReqDtos = []
    customizeReportContractorReqDtos = []
    customizeReportBusinessGroupReqDtos = []
    customizeReportGradeReqDtos = []
    customizeReportShiftReqDtos = []
    customizeReportsConditionReqDtos = []
    customizeReportFunctionReqDtos = []
    customizeReportsCostCenterReqDtos = []
    customizeReportDesignationReqDtos = []
    ColumnReqDto = []
    allShiftList: any;
    allGrade: any;
    AllBuiGrp: boolean;
    AllShiftGrp: boolean;
    todayDate: any;
    Conditionobj: any = {};
    slectedOperator: any
    ConditionValue: any;
    opt: any;
    fild: any;
    field = []
    CostCenterSe: any;
    allFunctionList: any;
    AllFun: any;
    allDesignationList: any;
    Alldesi: any;
    fieldOP: any
    selectedDeptId: any;
    masterSelected:boolean;
    checkedList:any;
    department_data=[];
    location_data=[];
    category_data=[];
    shift_data=[];
    Absent_ColumnReqDto=[];
    Attendence_ColumnReqDto=[];
    Duration_ColumnReqDto=[];
    Multipunch_ColumnReqDto=[];
    Latepunch_ColumnReqDto=[];
    Earlypunch_ColumnReqDto=[];
    OverTime_ColumnReqDto=[];
    Coff_ColumnReqDto=[];
    //contractor
    contractor_service_list:any;
    allServiceContractorArr=[];
    roleofuser:any;
    /*WorkForce Reports Start*/
    maxEndDate =  new Date();
    employeeInformationColumn = [];
    EmployeeInformation_ColumnReqDto = [];
    joiningUpdatesColumn = [];
    JoiningUpdates_ColumnReqDto = [];
    leavingUpdatesColumn = [];
    leavingUpdates_ColumnReqDto = [];
    confirmationDueColumn = [];
    confirmationDue_ColumnReqDto = [];
    gratuityColumn = [];
    gratuity_ColumnReqDto = [];
    assetColumn = [];
    asset_ColumnReqDto = [];
    /*WorkForce Reports End*/
    isShow:boolean=false;
    /*Leave Reports Start */
    LeaveTakenInCountColumns = [];
    LeaveTakenColumns = [];
    
    LeaveTaken_ColumnReqDto = [];
    
    LeaveRegisterColumns = [];
    LeaveRegister_ColumnReqDto = [];
    leaveTakenInCount_ColumnReqDto = [];
    leaveBalanceReport: boolean;
    
    currentyear:any;
    availableYears:any = [];
    selectedYear:any;
    LeaveBalanceColumns = [];
    leaveBalance_ColumnReqDto = [];
    LeaveCaryyForwardColumns=[];
    LeaveCaryyForward_ColumnReqDto = [];

    /*Leave Reports End */
    /*Report changes start*/
    selectedstatus:boolean=false;
    maxendDate: string;
    minDate: any;
    valueenddate:any;
    Date:any="option3";
    activeTab:string = 'dateTab';
    /*Report changes end*/
    report_wise='customise';
  
    selected_report: any;
    Employeedata: any;
   
    constructor(
        private router: Router,
        public httpService: HttpClient,
        public absentserve: AbsentRepoort,
        public workdurationserve: WorkDurationRepoortservice,
        public headcountserve: HeadCountservice,
        public leaveReportServe: leaveReportService,
        public latecomingserve: LateComingReportService,
        public earlyGoingserve: EarlyGoingService,
        public workforceService: WorkForceReportService,
        public chRef: ChangeDetectorRef,
        private toastr: ToastrService,
        public RepService: ReportService,
        public coffserve: cOffReportservice,
        public monthlyOverTimeReportserve: MonthlyOvertimeReposervice,
        public multipleReportServe: MultiplePunchesRepoortservice,
        public leaveRegisterServe: leaveRegisterReportService,
        public leaveCarryForwardServe:LeaveCarryForwardReportService ,
        public Spinner: NgxSpinnerService,
        public leaveTakenReportDaywise: leaveTakenReportDaywiseService,
        public InfoService:InfoService,
        public leaveReportService: LeaveReportsService,
        public dataService: dataService) {
        let user = sessionStorage.getItem('loggedUser');
        this.loggedUser = JSON.parse(user);
        this.postRole = new PostReport();
        this.RoleModel = new ReportModel();
        this.compId = this.loggedUser.compId;
        this.empOfficialId = this.loggedUser.empOfficialId;
        
        this.roleofuser = this.loggedUser.mrollMaster.roleName;
        //set todays date as default date
        let start=moment().format('YYYY-MM-DD');
        let end=moment().format('YYYY-MM-DD');
        sessionStorage.setItem("selected_start", start);
        sessionStorage.setItem("selected_end", end);
        this.empId =this.loggedUser.empId;
        this.HeadCountModel = new HeadCountdata();
        this.postRole.empOfficialId = this.empOfficialId;
        let themeColor = sessionStorage.getItem('themeColor');
        this.themeColor = themeColor;
        this.reportName = 'Select';
        this.selectedYear = 'Select';
        this.masterSelected = true;
        this.employmentType = [
            { employmentTypeid: 1, employmentType: "Consultant OR Retainer" },
            { employmentTypeid: 2, employmentType: "On Contact Job" },
            { employmentTypeid: 3, employmentType: "On Contact Labour" },
            { employmentTypeid: 4, employmentType: "On Contact Services" },
            { employmentTypeid: 5, employmentType: "On Roll" }
        ]

        for (let i = 0; i < this.employmentType.length; i++) {

            this.employmentType[i].empTY = true;
            this.customizeReportEmploymentTypeReqDtos = this.employmentType
            this.allEpType = true

        }

        this.SubEmploymentType = [
            { SubEmploymentTypeid: 1, SubEmploymentType: "Confirmed" },
            { SubEmploymentTypeid: 2, SubEmploymentType: "On Probation" },
            { SubEmploymentTypeid: 3, SubEmploymentType: "Temporary" },
            { SubEmploymentTypeid: 4, SubEmploymentType: "Trainee" }
        ];

        for (let i = 0; i < this.SubEmploymentType.length; i++) {
            this.SubEmploymentType[i].SubempTY = true;
            this.customizeReportSubEmployTypeReqDtos = this.SubEmploymentType
            this.allSubEpType = true
        }
        this.Conditionobj = {
            field: '', operator: '', value: ''
        }

        this.AbsentColumns = [

            { Columnsid: 1, ColumnName: "EmpId",isSelected:true },
            { Columnsid: 2, ColumnName: "Name",isSelected:true },
            { Columnsid: 3, ColumnName: "Absent Date",isSelected:true },
            { Columnsid: 4, ColumnName: "Department",isSelected:true },
            // { Columnsid: 5, ColumnName: "Designation",isSelected:true },
            { Columnsid: 3, ColumnName: "Category",isSelected:true},
            // { Columnsid: 7, ColumnName: "DOB",isSelected:false },
            // { Columnsid: 8, ColumnName: "DOJ",isSelected:false },
            { Columnsid: 9, ColumnName: "Branch",isSelected:true},
            { Columnsid: 10, ColumnName: "attendanceStatus",isSelected:true },

        ]
        this.ColumnsForAttend = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Attendance Date",isSelected:true},
            { Columnsid: 4, ColumnName: "In-Time",isSelected:true},
            { Columnsid: 5, ColumnName: "Out-Time",isSelected:true},
            { Columnsid: 6, ColumnName: "Working Duration",isSelected:true},
            { Columnsid: 7, ColumnName: "Status",isSelected:true},
            { Columnsid: 9, ColumnName: "Status Of The Day",isSelected:true},
            { Columnsid: 8, ColumnName: "Department",isSelected:true},
            { Columnsid: 3, ColumnName: "Category",isSelected:true},
            // { Columnsid: 9, ColumnName: "Designation",isSelected:false},
            // { Columnsid: 10, ColumnName: "Age",isSelected:false},
            // { Columnsid: 11, ColumnName: "DOB",isSelected:false},
            // { Columnsid: 12, ColumnName: "DOJ",isSelected:false},
            { Columnsid: 13, ColumnName: "Branch",isSelected:true},
            { Columnsid: 5, ColumnName: "Shift",isSelected:true},
        ]
        this.workDurationColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Category",isSelected:true},
            { Columnsid: 4, ColumnName: "Branch",isSelected:true},
            { Columnsid: 5, ColumnName: "Shift",isSelected:true},
            { Columnsid: 6, ColumnName: "Attendance Date",isSelected:true},
            { Columnsid: 7, ColumnName: "In-Time",isSelected:true},
            { Columnsid: 8, ColumnName: "Out-Time",isSelected:true},
            { Columnsid: 9, ColumnName: "Working Duration",isSelected:true},
            { Columnsid: 10, ColumnName: "Status of day",isSelected:true},
            { Columnsid: 11, ColumnName: "Department",isSelected:true},
            { Columnsid: 13, ColumnName: "attendanceStatus",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
        ]
        this.musterReportColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Category",isSelected:true},
            { Columnsid: 4, ColumnName: "Branch",isSelected:true},
            { Columnsid: 5, ColumnName: "Shift",isSelected:true},
            { Columnsid: 6, ColumnName: "Attendance Date",isSelected:true},
            { Columnsid: 7, ColumnName: "In-Time",isSelected:true},
            { Columnsid: 8, ColumnName: "Out-Time",isSelected:true},
            { Columnsid: 9, ColumnName: "Working Duration",isSelected:true},
            { Columnsid: 10, ColumnName: "Status of day",isSelected:true},
            { Columnsid: 11, ColumnName: "Department",isSelected:true},
            { Columnsid: 13, ColumnName: "attendanceStatus",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
        ]
        this.multiPulPunchColumn = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.overTimeColumns = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.latecomingColumn = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            // { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.earlyGoingColumn = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.coffEntryColumn = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.LeaveCol = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            // { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.leaveTakenColumn = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            // { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 7, ColumnName: "Branch",isSelected:true},
        ]
        this.employeeInformationColumn = [
            { Columnsid: 1, ColumnName: "EmpID",isSelected:true},
            { Columnsid: 2, ColumnName: "BiometricID",isSelected:true},
            { Columnsid: 3, ColumnName: "Employeename",isSelected:true},
            { Columnsid: 4, ColumnName: "MiddldlName",isSelected:true},
            { Columnsid: 5, ColumnName: "LastName",isSelected:true},
            { Columnsid: 6, ColumnName: "DateOfBirth",isSelected:true},
            { Columnsid: 7, ColumnName: "Gender",isSelected:true},
            { Columnsid: 8, ColumnName: "CountryName",isSelected:true},
            { Columnsid: 9, ColumnName: "StateName",isSelected:true},
            { Columnsid: 10, ColumnName: "BloodGroup",isSelected:true},
            { Columnsid: 11, ColumnName: "PermantAddress",isSelected:true},
            { Columnsid: 12, ColumnName: "TempAddress",isSelected:true},
            { Columnsid: 13, ColumnName: "PersonalEmailID",isSelected:true},
            { Columnsid: 14, ColumnName: "MotherTongue",isSelected:true},
            { Columnsid: 15, ColumnName: "MobileNo",isSelected:true},
            { Columnsid: 16, ColumnName: "LicenseNumber",isSelected:true},
            { Columnsid: 17, ColumnName: "PassportNumber",isSelected:true},
            { Columnsid: 18, ColumnName: "AdharCardNo",isSelected:true},
            { Columnsid: 19, ColumnName: "PlaceOfBirth",isSelected:true},
            { Columnsid: 20, ColumnName: "Nationality",isSelected:true},
            { Columnsid: 21, ColumnName: "CompanyName",isSelected:true},
            { Columnsid: 22, ColumnName: "BranchName",isSelected:true},
            { Columnsid: 23, ColumnName: "EmployeeTypeName",isSelected:true},
            { Columnsid: 24, ColumnName: "SubEmployeeTypeName",isSelected:true},
            { Columnsid: 25, ColumnName: "ContractorName",isSelected:true},
            { Columnsid: 26, ColumnName: "DepartmentName",isSelected:true},
            { Columnsid: 27, ColumnName: "SubDepartmentName",isSelected:true},
            { Columnsid: 28, ColumnName: "FunctionName",isSelected:true},
            { Columnsid: 29, ColumnName: "DesignationName",isSelected:true},
            { Columnsid: 30, ColumnName: "OfficialEmailID",isSelected:true},
            { Columnsid: 31, ColumnName: "OfficialMobileNo",isSelected:true},
            { Columnsid: 32, ColumnName: "DateofJoining",isSelected:true},
            { Columnsid: 33, ColumnName: "LoginName",isSelected:true},
            { Columnsid: 34, ColumnName: "Password",isSelected:true},
            { Columnsid: 35, ColumnName: "WeeklyOFF",isSelected:true},
            { Columnsid: 36, ColumnName: "Weekoff",isSelected:true},
            { Columnsid: 37, ColumnName: "ReportingPersonID",isSelected:true},
            { Columnsid: 38, ColumnName: "Iodiff",isSelected:true},
            { Columnsid: 39, ColumnName: "DateOfsepration",isSelected:true},
            { Columnsid: 40, ColumnName: "UANNumber",isSelected:true},
            { Columnsid: 41, ColumnName: "PANNumber",isSelected:true},
            { Columnsid: 42, ColumnName: "ESICNumber",isSelected:true},
        ]
        this.joiningUpdatesColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 5, ColumnName: "Branch",isSelected:true},
        ]
        this.leavingUpdatesColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 5, ColumnName: "Branch",isSelected:true},
        ]
        this.confirmationDueColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 5, ColumnName: "Branch",isSelected:true},
        ]
        this. gratuityColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Designation",isSelected:true},
            { Columnsid: 5, ColumnName: "Branch",isSelected:true},
        ]
        this. assetColumn = [
            { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 2, ColumnName: "Name",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Category",isSelected:true},
            { Columnsid: 5, ColumnName: "issueDate",isSelected:true},
            { Columnsid: 6, ColumnName: "assetName",isSelected:true},
            { Columnsid: 7, ColumnName: "assetDesc",isSelected:true},
            { Columnsid: 8, ColumnName: "serialNo",isSelected:true},
            { Columnsid: 9, ColumnName: "descName",isSelected:true},
            { Columnsid: 10, ColumnName: "bioId",isSelected:true},
        ]
        this.LeaveRegisterColumns =  [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Branch",isSelected:true},
        ]
        this.LeaveTakenColumns =  [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Branch",isSelected:true},
        ]
        this.LeaveTakenInCountColumns = [       
            { Columnsid: 1, ColumnName: "Name",isSelected:true},
            { Columnsid: 2, ColumnName: "EmpId",isSelected:true},
            { Columnsid: 3, ColumnName: "Department",isSelected:true},
            { Columnsid: 4, ColumnName: "Branch",isSelected:true},
        ]
        
        /**START OF THE LEAVE BALANCE REPORT */
        this.LeaveBalanceColumns = [       
            { Columnsid: 1, ColumnName: "empid",isSelected:true},
            { Columnsid: 2, ColumnName: "empname ",isSelected:true},
            { Columnsid: 3, ColumnName: "department",isSelected:true},
            { Columnsid: 4, ColumnName: "designation",isSelected:true},
            { Columnsid: 5, ColumnName: "leavecode",isSelected:true},
            { Columnsid: 6, ColumnName: "balance",isSelected:true},
           
        ]
        this.LeaveCaryyForwardColumns= [       
            { Columnsid: 1, ColumnName: "empid",isSelected:true},
            { Columnsid: 2, ColumnName: "empname ",isSelected:true},
            { Columnsid: 3, ColumnName: "leavecode",isSelected:true},
            { Columnsid: 4, ColumnName: "department",isSelected:true},
            { Columnsid: 5, ColumnName: "caryyforwarded Leaves",isSelected:true},
            { Columnsid: 6, ColumnName: "year",isSelected:true},
        ]
        /**END OF THE LEAVE BALANCE REPORT */
    $(function() {
        var start = moment();
        var end = moment();
        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        (<any>$('#reportrange')).daterangepicker({
            startDate: start,
            endDate: end,
            maxDate:start,
            autoApply:true,
            opens:'right',
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
        (<any>$('#reportrange')).on('apply.daterangepicker', function(ev, picker) {
            let start=picker.startDate.format('YYYY-MM-DD');
            let end=picker.endDate.format('YYYY-MM-DD');
            sessionStorage.setItem("selected_start", start);
            sessionStorage.setItem("selected_end", end);
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
            $('#reportrange').val('');
        });
    });
}

    ngOnInit() {
        this.Departmentgroup();
        // this.GradeData();
        this.AllEmptype();
        // this.allSubDep();
        this.allLoco();
        // this.allSubEmpType();
        // this.allCostCenter();
        this.allBuiss();
        // this.allShift();
        // this.allFunction();
        // this.allDesignation();
        this.getAllEmployee();
        this.selectedItems = []
        this.Date='select_date';
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'empOfficialId',
            textField: 'fullName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 100,
            allowSearchFilter: true
        };
    }
    checkUncheckAll(report_type) {
        this.ColumnReqDto=[];
        if(this.reportTypeVal == 'attendance'){
            //absentReport
            if(report_type=='absentReport'){
                for (var i = 0; i < this.AbsentColumns.length; i++) {
                    this.AbsentColumns[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.AbsentColumns.length; j++) {
                        let obj ={
                            field: this.AbsentColumns[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.AbsentColumns[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            //AttendenceReport
            if(report_type=='AttendenceReport'){
                for (var i = 0; i < this.ColumnsForAttend.length; i++) {
                    this.ColumnsForAttend[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.ColumnsForAttend.length; j++) {
                        let obj ={
                            field: this.ColumnsForAttend[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.ColumnsForAttend[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            //Ar-Request Report
            // if(report_type=='ArRequest'){
            //     for (var i = 0; i < this.ColumnsForAttend.length; i++) {
            //         this.ColumnsForAttend[i].isSelected = this.masterSelected;
            //     }
            //     if(this.masterSelected){
            //         for (let j = 0; j < this.ColumnsForAttend.length; j++) {
            //             let obj ={
            //                 field: this.ColumnsForAttend[j].ColumnName,
            //                 operator: 'String', 
            //                 value: 'String',
            //                 id : this.ColumnsForAttend[j].Columnsid
            //             }
            //             this.ColumnReqDto.push(obj)
            //         }
            //     }
            // }
            //workDurationReport
            if(report_type=='workDurationReport'){
                for (var i = 0; i < this.workDurationColumn.length; i++) {
                    this.workDurationColumn[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.workDurationColumn.length; j++) {
                        let obj ={
                            field: this.workDurationColumn[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.workDurationColumn[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            //muster report
            if(report_type=='musterReport'){
                for (var i = 0; i < this.musterReportColumn.length; i++) {
                    this.musterReportColumn[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.musterReportColumn.length; j++) {
                        let obj ={
                            field: this.musterReportColumn[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.musterReportColumn[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            //multiplePuchReport
            if(report_type=='multiplePuchReport'){
                for (var i = 0; i < this.multiPulPunchColumn.length; i++) {
                    this.multiPulPunchColumn[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.multiPulPunchColumn.length; j++) {
                        let obj ={
                            field: this.multiPulPunchColumn[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.multiPulPunchColumn[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            //overTimeReport
            if(report_type=='overTimeReport' || 'lateComingReport'){
                for (var i = 0; i < this.overTimeColumns.length; i++) {
                    this.overTimeColumns[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.overTimeColumns.length; j++) {
                        let obj ={
                            field: this.overTimeColumns[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.overTimeColumns[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            //earlyGoingReport
            if(report_type=='earlyGoingReport'){
                for (var i = 0; i < this.earlyGoingColumn.length; i++) {
                    this.earlyGoingColumn[i].isSelected = this.masterSelected;
                }
                if(this.masterSelected){
                    for (let j = 0; j < this.earlyGoingColumn.length; j++) {
                        let obj ={
                            field: this.earlyGoingColumn[j].ColumnName,
                            operator: 'String', 
                            value: 'String',
                            id : this.earlyGoingColumn[j].Columnsid
                        }
                        this.ColumnReqDto.push(obj)
                    }
                }
            }
            
        }
        if(this.reportTypeVal == 'workforce'){
            //Employee Information Report
            if(report_type == 'employeeInformationReport'){
                for (var i = 0; i < this.employeeInformationColumn.length; i++) {
                    this.employeeInformationColumn[i].isSelected = this.masterSelected;
                  }
                  if(this.masterSelected){
                      for (let j = 0; j < this.employeeInformationColumn.length; j++) {
                          let obj ={
                              field: this.employeeInformationColumn[j].ColumnName,
                              operator: 'String', 
                              value: 'String',
                              id : this.employeeInformationColumn[j].Columnsid
                          }
                          this.ColumnReqDto.push(obj)
                      }
                  } 
            }
            //Joining Updates Report
            if(report_type == 'joiningUpdatesReport'){
                for (var i = 0; i < this.joiningUpdatesColumn.length; i++) {
                    this.joiningUpdatesColumn[i].isSelected = this.masterSelected;
                  }
                  if(this.masterSelected){
                      for (let j = 0; j < this.joiningUpdatesColumn.length; j++) {
                          let obj ={
                              field: this.joiningUpdatesColumn[j].ColumnName,
                              operator: 'String', 
                              value: 'String',
                              id : this.joiningUpdatesColumn[j].Columnsid
                          }
                          this.ColumnReqDto.push(obj)
                      }
                  } 
            }
             //Leaving Updates Report
             if(report_type == 'leavingUpdatesReport'){
                for (var i = 0; i < this.leavingUpdatesColumn.length; i++) {
                    this.leavingUpdatesColumn[i].isSelected = this.masterSelected;
                  }
                  if(this.masterSelected){
                      for (let j = 0; j < this.leavingUpdatesColumn.length; j++) {
                          let obj ={
                              field: this.leavingUpdatesColumn[j].ColumnName,
                              operator: 'String', 
                              value: 'String',
                              id : this.leavingUpdatesColumn[j].Columnsid
                          }
                          this.ColumnReqDto.push(obj)
                      }
                  } 
            }
            //Confirmation Due Report
              if(report_type == 'confirmationDueReport'){
                for (var i = 0; i < this. confirmationDueColumn.length; i++) {
                    this. confirmationDueColumn[i].isSelected = this.masterSelected;
                  }
                  if(this.masterSelected){
                      for (let j = 0; j < this. confirmationDueColumn.length; j++) {
                          let obj ={
                              field: this. confirmationDueColumn[j].ColumnName,
                              operator: 'String', 
                              value: 'String',
                              id : this. confirmationDueColumn[j].Columnsid
                          }
                          this.ColumnReqDto.push(obj)
                      }
                  } 
            }
             //Gratuity Report
             if(report_type == 'gratuityReport'){
                for (var i = 0; i < this.  gratuityColumn.length; i++) {
                    this.gratuityColumn[i].isSelected = this.masterSelected;
                  }
                  if(this.masterSelected){
                      for (let j = 0; j < this.gratuityColumn.length; j++) {
                          let obj ={
                              field: this.gratuityColumn[j].ColumnName,
                              operator: 'String', 
                              value: 'String',
                              id : this.gratuityColumn[j].Columnsid
                          }
                          this.ColumnReqDto.push(obj)
                      }
                  } 
            }
            //Asset Report Start
             if(report_type == 'assetReport'){
                for (var i = 0; i < this.assetColumn.length; i++) {
                    this.assetColumn[i].isSelected = this.masterSelected;
                  }
                  if(this.masterSelected){
                      for (let j = 0; j < this.assetColumn.length; j++) {
                          let obj ={
                              field: this.assetColumn[j].ColumnName,
                              operator: 'String', 
                              value: 'String',
                              id : this.assetColumn[j].Columnsid
                          }
                          this.ColumnReqDto.push(obj)
                      }
                  } 
            }
        }
         //Leave Register Report
         if(report_type=='leaveRegisterReport'){
            for (var i = 0; i < this.LeaveRegisterColumns.length; i++) {
                this.LeaveRegisterColumns[i].isSelected = this.masterSelected;
              }
              if(this.masterSelected){
                  for (let j = 0; j < this.LeaveRegisterColumns.length; j++) {
                      let obj ={
                          field: this.LeaveRegisterColumns[j].ColumnName,
                          operator: 'String', 
                          value: 'String',
                          id : this.LeaveRegisterColumns[j].Columnsid
                      }
                      this.ColumnReqDto.push(obj)
                  }
              }
        }
        //Leave Taken Report
        if(report_type=='leaveTakenReport'){
            for (var i = 0; i < this.LeaveTakenColumns.length; i++) {
                this.LeaveTakenColumns[i].isSelected = this.masterSelected;
              }
              if(this.masterSelected){
                  for (let j = 0; j < this.LeaveTakenColumns.length; j++) {
                      let obj ={
                          field: this.LeaveTakenColumns[j].ColumnName,
                          operator: 'String', 
                          value: 'String',
                          id : this.LeaveTakenColumns[j].Columnsid
                      }
                      this.ColumnReqDto.push(obj)
                  }
              }
        }
        if(report_type=='leaveCaryyForwardReport'){
            for (var i = 0; i < this.LeaveCaryyForwardColumns.length; i++) {
                this.LeaveCaryyForwardColumns[i].isSelected = this.masterSelected;
              }
              if(this.masterSelected){
                  for (let j = 0; j < this.LeaveCaryyForwardColumns.length; j++) {
                      let obj ={
                          field: this.LeaveCaryyForwardColumns[j].ColumnName,
                          operator: 'String', 
                          value: 'String',
                          id : this.LeaveCaryyForwardColumns[j].Columnsid
                      }
                      this.ColumnReqDto.push(obj)
                  }
              }
        }
        //Leave Taken InCount
        if(report_type=='leaveTakenInCountReport'){
            for (var i = 0; i < this.LeaveTakenInCountColumns.length; i++) {
                this.LeaveTakenInCountColumns[i].isSelected = this.masterSelected;
              }
              if(this.masterSelected){
                  for (let j = 0; j < this.LeaveTakenInCountColumns.length; j++) {
                      let obj ={
                          field: this.LeaveTakenInCountColumns[j].ColumnName,
                          operator: 'String', 
                          value: 'String',
                          id : this.LeaveTakenInCountColumns[j].Columnsid
                      }
                      this.ColumnReqDto.push(obj)
                  }
              }
        }
    }
    absentReport: boolean = false;
    AttendenceReport: boolean = false;
    workDurationReport : boolean = false;
    musterReport : boolean = false;
    multiplePuchReport : boolean = false;
    overTimeReport : boolean = false;
    lateComingReport : boolean = false;
    earlyGoingReport : boolean =false;
    
    leaveReport : boolean = false;
    leaveTakenR : boolean = false;
    employeeInformationReport: boolean = false;
    joiningUpdatesReport: boolean = false;
    leavingUpdatesReport: boolean = false;
    confirmationDueReport: boolean = false;
    gratuityReport: boolean = false;
    assetReport: boolean = false;
    leaveRegisterReport: boolean = false;
    leaveTakenReport: boolean = false;
    leaveTakenReportInCount: boolean = false;
    

    /**START FOR THE LEAVE BALANCE REPORT */
    currentleaveBalanceReport : boolean = false;
    leaveCaryyForwardReport:boolean= false;
    /**END FOR THE LEAVE BALANCE REPORT */

    getDropdown(event) {
      
        this.selected_report=event.target.value;
        this.setDateRange(this.selected_report);
        this.employeeInformationReport=false;
        this.currentleaveBalanceReport = false;
        this.leaveCaryyForwardReport= false;
        this.activeTab='dateTab';
        //test reset
        this.ResetAllSelections();
        this.selectedEmp = [];
        //For Attendance Reports
        if(this.reportTypeVal == "attendance"){
            this.employeeInformationReport = false;
            this.joiningUpdatesReport = false;
            this.leavingUpdatesReport = false;
            this.confirmationDueReport = false;
            this.gratuityReport = false;
            this.assetReport = false;
            this.currentleaveBalanceReport = false;
            this.leaveCaryyForwardReport= false;
            if(event.target.value == 'Head Count Report'){
                this.openflag = false;
            }
            else if(event.target.value == 'Absent Report') {
                this.openflag = true;
                this.fromDateToDateFlag = true;
                this.AttendenceReport = false;
                this.workDurationReport = false;
                this.musterReport=false;
                this.multiplePuchReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.earlyGoingReport = false;
            
                this.leaveReport = false;
                this.leaveTakenR = false;
        
                this.absentReport = true;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if(event.target.value == 'Attendence Report') {
                this.openflag = true;
                this.fromDateToDateFlag = true;
                this.absentReport = false;
                this.workDurationReport = false;
                this.musterReport=false;
                this.multiplePuchReport = false;
                this.lateComingReport = false;
                this.overTimeReport = false;
                this.earlyGoingReport = false;
                
                this.leaveReport = false;
                this.leaveTakenR = false;
                this.AttendenceReport = true;
                
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
           
            else if (event.target.value == 'Working Duration Report') {
                this.openflag = true;
                this.absentReport = false;
                this.AttendenceReport = false;    
                this.multiplePuchReport = false;
                this.lateComingReport = false;
                this.overTimeReport = false;
                this.earlyGoingReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
                this.musterReport=false;
                this.workDurationReport = true;
            
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Muster Report') {
                this.openflag = true;
                this.absentReport = false;
                this.AttendenceReport = false;    
                this.multiplePuchReport = false;
                this.lateComingReport = false;
                this.overTimeReport = false;
                this.earlyGoingReport = false;
                
                this.leaveReport = false;
                this.leaveTakenR = false;
                this.workDurationReport = false;
                this.musterReport=true;
            
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Monthly Overtime Report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.multiplePuchReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.musterReport=false;
                this.earlyGoingReport = false;
                
                this.leaveReport = false;
                this.leaveTakenR = false;
                this.overTimeReport = true;
                
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Multiple Punches Report') {
                this.openflag = true;
                this.fromDateToDateFlag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.earlyGoingReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
                this.multiplePuchReport = true;
                
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            
            else if (event.target.value == 'Shift Assignment Report') {
                this.openflag = true;  
            }
            else if (event.target.value == 'Head Count Budgetory Report') {
                this.openflag = true;
            }
            else if (event.target.value == 'Leave Report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.earlyGoingReport = false;
                this.musterReport=false;
                this.multiplePuchReport = false;
                this.leaveTakenR = false;
            
                this.leaveReport = true; 
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Leave Taken Report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.multiplePuchReport = false;
                this.workDurationReport = false;
                this.earlyGoingReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.lateComingReport = false;
                
                this.leaveTakenR = true;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            
            else if (event.target.value == 'Late Coming Report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.multiplePuchReport = false;
                this.workDurationReport = false;
                this.earlyGoingReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.lateComingReport = true;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Early Going Report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = true;
                this.employeeInformationReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Leave Taken Report Daywise') {
                this.openflag = true; 
            }
        }
        //for arrequest report
        
        //For WorkForce Reports
        if(this.reportTypeVal == "workforce"){
            if(event.target.value == 'Employee Information Report'){
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = false;
                this.employeeInformationReport = true;
                this.joiningUpdatesReport = false;
                this.leavingUpdatesReport = false;
                this.confirmationDueReport = false;
                this.gratuityReport = false;
                this.assetReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            if(event.target.value == 'Joining Updates Report'){
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
                
                this.earlyGoingReport = false;
                this.employeeInformationReport = false;
                this.joiningUpdatesReport = true;
                this.leavingUpdatesReport = false;
                this.confirmationDueReport = false;
                this.gratuityReport = false;
                this.assetReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            if(event.target.value == 'Leaving Updates Report'){
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = false;
                this.employeeInformationReport = false;
                this.joiningUpdatesReport = false;
                this.leavingUpdatesReport = true;
                this.confirmationDueReport = false;
                this.gratuityReport = false;
                this.assetReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            if(event.target.value == 'Confirmation Due Report'){
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = false;
                this.employeeInformationReport = false;
                this.joiningUpdatesReport = false;
                this.leavingUpdatesReport = false;
                this.confirmationDueReport = true;
                this.gratuityReport = false;
                this.assetReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            if(event.target.value == 'Gratuity Eligible Report'){
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = false;
                this.employeeInformationReport = false;
                this.joiningUpdatesReport = false;
                this.leavingUpdatesReport = false;
                this.confirmationDueReport = false;
                this.gratuityReport = true;
                this.assetReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            if(event.target.value == 'Asset Report'){
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = false;
                this.employeeInformationReport = false;
                this.joiningUpdatesReport = false;
                this.leavingUpdatesReport = false;
                this.confirmationDueReport = false;
                this.gratuityReport = false;
                this.assetReport = true;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
        }
        //Leave Reports
        if(this.reportTypeVal == "leave"){
            // if (event.target.value == 'Leave Register Report') {
            //     this.openflag = true;
            //     this.getyears();
            //     this.AttendenceReport = false;
            //     this.absentReport = false;
            //     this.overTimeReport = false;
            //     this.multiplePuchReport = false;
            //     this.workDurationReport = false;
            //     this.earlyGoingReport = false;
            //     this.musterReport=false;
            //     this.leaveReport = false;
            
            //     this.lateComingReport = false;
            //     this.employeeInformationReport = false;
            //     this.leaveRegisterReport = true;
            //     this.leaveTakenReport = false
            //     this.leaveBalanceReport = false;
            //     this.currentleaveBalanceReport = false;
            // }
             if (event.target.value == 'Leave Taken Report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.multiplePuchReport = false;
                this.workDurationReport = false;
                this.earlyGoingReport = false;
                this.musterReport=false;
                this.leaveReport = false;
            
                this.lateComingReport = false;
                this.leaveRegisterReport = false;
                this.employeeInformationReport = false;
                this.leaveTakenReport = true;
                this.leaveBalanceReport = false;
                this.leaveTakenReportInCount = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value == 'Leave Taken Report InCount') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
            
                this.earlyGoingReport = false;
                this.leaveTakenReportInCount = true;
                this.employeeInformationReport = false;
                this.leaveTakenReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport= false;
            }
         
            else if (event.target.value == 'Leave balance report') {
                this.openflag = true;
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
                
                this.earlyGoingReport = false;
                this.leaveTakenReportInCount = false;
                this.employeeInformationReport = false;
                this.leaveTakenReport = false;
                this.currentleaveBalanceReport = true;
                this.leaveCaryyForwardReport= false;
            }
            else if (event.target.value =='Leave Carry Forward Report') {
                this.openflag = true;
                this.getyears();
                this.AttendenceReport = false;
                this.absentReport = false;
                this.overTimeReport = false;
                this.lateComingReport = false;
                this.workDurationReport = false;
                this.multiplePuchReport = false;
                this.musterReport=false;
                this.leaveReport = false;
                this.leaveTakenR = false;
                
                this.earlyGoingReport = false;
                this.leaveTakenReportInCount = false;
                this.employeeInformationReport = false;
                this.leaveTakenReport = false;
                this.currentleaveBalanceReport = false;
                this.leaveCaryyForwardReport = true;
            }
        
        }
        this.SelectAllDep(true);
        this.alldepartment=true;
        this.SelectAllEmpCat(true);
        this.allEmpCat=true;
        this.SelectAllLoco(true);
        this.allLocoL=true;
    } 
    valueDepartment() {
        this.department_data=[];
        for(let i=0;i<this.departmentlistdata.length;i++){
            if(this.departmentlistdata[i].department==true){
                let temp={
                    "deptId": this.departmentlistdata[i].deptId
                }
                  this.alldepartment=false;
                this.department_data.push(temp);
            }
        }
        this.postRole.customizeReportDepartmentReqDtos = this.department_data;
    }
    SelectAllDep(event) {
        for (let i = 0; i < this.departmentlistdata.length; i++) {
            if(event) {
                this.departmentlistdata[i].department = true;
            }
            else {
                this.departmentlistdata[i].department = false;
            }
        }
        this.valueDepartment();
    }
    valueSubDepartment(e, index) {
        if (e) {
            this.customizeReportSubDepartmentReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportSubDepartmentReqDtos.length; i++) {
                if (this.customizeReportSubDepartmentReqDtos[i].subDeptId === index.subDeptId) {
                    this.customizeReportSubDepartmentReqDtos.splice(i, 1)
                    if (!this.customizeReportSubDepartmentReqDtos.length) {
                        let obj = {
                            "subDeptId": 0,
                            "subDeptName": "string"
                        }
                        this.customizeReportSubDepartmentReqDtos.push(obj)
                    }
                }
            }
        }
    }
    SelectAllSubDeplist(event) {
        for (let i = 0; i < this.allSubDepList.length; i++) {
            if (event) {
                this.allSubDepList[i].subDept = true;
                this.customizeReportSubDepartmentReqDtos = this.allSubDepList
                console.log(this.customizeReportSubDepartmentReqDtos);
            }
            else {
                this.allSubDepList[i].subDept = false;
                this.customizeReportSubDepartmentReqDtos = []
                if (!this.customizeReportSubDepartmentReqDtos.length) {

                    let obj = {
                        "subDeptId": 0,
                        "subDeptName": "string"
                    }
                    this.customizeReportSubDepartmentReqDtos.push(obj)
                }
                console.log(this.customizeReportSubDepartmentReqDtos);
            }
        }
    }
    valueLocation() {
        this.location_data=[];
      for(let i=0;i<this.allLocoList.length;i++){
            if(this.allLocoList[i].allLocoListdata==true){
                let temp={
                    "locationId": this.allLocoList[i].locationId
                }
                this.location_data.push(temp);
            }
        }
        this.postRole.customizeReportLocationReqDtos = this.location_data;
    }
    SelectAllLoco(event) {
        for (let i = 0; i < this.allLocoList.length; i++) {
            if (event) {
                this.allLocoList[i].allLocoListdata = true;
            }
            else {
                this.allLocoList[i].allLocoListdata = false;
            }
        }
        this.valueLocation();
    }
    valueEmpType(e, index) {
        if (e) {
            this.customizeReportEmploymentTypeReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportEmploymentTypeReqDtos.length; i++) {
                if (this.customizeReportEmploymentTypeReqDtos[i].employmentTypeid === index.employmentTypeid) {
                    this.customizeReportEmploymentTypeReqDtos.splice(i, 1)
                    if (!this.customizeReportEmploymentTypeReqDtos.length) {
                        let obj = {
                            "employmentTypeid": 0,
                            "employmentType": "string"
                        }
                        this.customizeReportEmploymentTypeReqDtos.push(obj)
                    }
                }
            }
        }
    }
    SelectAllEmpType(event) {
        for (let i = 0; i < this.employmentType.length; i++) {
            if (event) {
                this.employmentType[i].empTY = true;
                this.customizeReportEmploymentTypeReqDtos = this.employmentType;
            }
            else {
                this.employmentType[i].empTY = false;
                this.customizeReportEmploymentTypeReqDtos = []
                if (!this.customizeReportEmploymentTypeReqDtos.length) {
                    let obj = {
                        "employmentTypeid": 0,
                        "employmentType": "string"
                    }
                    this.customizeReportEmploymentTypeReqDtos.push(obj)
                }
            }
        }
    }
    valueSubEmpType(e, index) {
        if (e) {
            this.customizeReportSubEmployTypeReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportSubEmployTypeReqDtos.length; i++) {
                if (this.customizeReportSubEmployTypeReqDtos[i].SubEmploymentTypeid === index.SubEmploymentTypeid) {
                    this.customizeReportSubEmployTypeReqDtos.splice(i, 1)
                    if (!this.customizeReportSubEmployTypeReqDtos) {

                        let obj = {
                            "SubEmploymentTypeid": 0,
                            "SubEmploymentType": "string"
                        }
                        this.customizeReportSubEmployTypeReqDtos.push(obj)
                    }
                }
            }

        }
    }
    SelectAllSubEmpType(event) {
        for (let i = 0; i < this.SubEmploymentType.length; i++) {
            if (event) {
                this.SubEmploymentType[i].SubempTY = true;
                this.customizeReportSubEmployTypeReqDtos = this.SubEmploymentType;
            }
            else {
                this.SubEmploymentType[i].SubempTY = false;
                this.customizeReportSubEmployTypeReqDtos = []
                if (!this.customizeReportSubEmployTypeReqDtos.length) {
                    let obj = {
                        "SubEmploymentTypeid": 0,
                        "SubEmploymentType": "string"
                    }
                    this.customizeReportSubEmployTypeReqDtos.push(obj)
                }
            }
        }
    }
    SelectSubEmpCat(e, index) {
        if (e) {
            this.customizeReportSubEmpCategoryReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportSubEmpCategoryReqDtos.length; i++) {
                if (this.customizeReportSubEmpCategoryReqDtos[i].subEmpTypeId === index.subEmpTypeId) {
                    this.customizeReportSubEmpCategoryReqDtos.splice(i, 1)
                    if (!this.customizeReportSubEmpCategoryReqDtos.length) {
                        let obj = {
                            "subEmpTypeId": 0,
                            "subEmpTypeName": "string"
                        }
                        this.customizeReportSubEmpCategoryReqDtos.push(obj)
                    }
                }
            }
        }
    }
    SelectAllSubEmpCat(event) {
        for (let i = 0; i < this.allSubEmpTypeList.length; i++) {
            if (event) {
                this.allSubEmpTypeList[i].AllSubEMpCate = true;
                this.customizeReportSubEmpCategoryReqDtos = this.allSubEmpTypeList;
            }
            else {
                this.allSubEmpTypeList[i].AllSubEMpCate = false;
                this.customizeReportSubEmpCategoryReqDtos = []
                if (!this.customizeReportSubEmpCategoryReqDtos.length) {
                    let obj = {
                        "subEmpTypeId": 0,
                        "subEmpTypeName": "string"
                    }
                    this.customizeReportSubEmpCategoryReqDtos.push(obj)
                }
            }
        }

    }  
    valueEmpCate() {
        this.category_data=[];
        for(let i=0;i<this.AllEMp.length;i++){
            if(this.AllEMp[i].AllEMpCate==true){
                let temp={
                    "empTypeId": this.AllEMp[i].empTypeId
                }
                this.category_data.push(temp);
            }
        }
        this.postRole.customizeReportEmpCategoryReqDtos = this.category_data;
    }
    SelectAllEmpCat(event) {
        for (let i = 0; i < this.AllEMp.length; i++) {
            if (event) {
                this.AllEMp[i].AllEMpCate = true;
            }
            else {
                this.AllEMp[i].AllEMpCate = false;
            }
        }
        //test
        this.valueEmpCate();
    } 
    valueBuissness(e, index) {
        if (e) {
            this.customizeReportBusinessGroupReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportBusinessGroupReqDtos.length; i++) {
                if (this.customizeReportBusinessGroupReqDtos[i].busiGroupId === index.busiGroupId) {
                    this.customizeReportBusinessGroupReqDtos.splice(i, 1)
                    if (!this.customizeReportBusinessGroupReqDtos.length) {

                        let obj = {
                            "busiGroupId": 0,
                            "busiGrupName": "string"
                        }
                        this.customizeReportBusinessGroupReqDtos.push(obj)
                    }
                }
            }

        }
    }
    SelectAllBuissness(event) {
        for (let i = 0; i < this.allBuiGrpList.length; i++) {
            if (event) {
                this.allBuiGrpList[i].buiGr = true;
                this.customizeReportBusinessGroupReqDtos = this.allBuiGrpList;
            }
            else {
                this.allBuiGrpList[i].buiGr = false;
                this.customizeReportBusinessGroupReqDtos = []
                if (!this.customizeReportBusinessGroupReqDtos.length) {

                    let obj = {
                        "busiGroupId": 0,
                        "busiGrupName": "string"
                    }
                    this.customizeReportBusinessGroupReqDtos.push(obj)
                }
            }
        }

    } 
    valueGrade(e, index) {
        if (e) {
            this.customizeReportGradeReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportGradeReqDtos.length; i++) {
                if (this.customizeReportGradeReqDtos[i].gradeId === index.gradeId) {
                    this.customizeReportGradeReqDtos.splice(i, 1)
                    if (!this.customizeReportGradeReqDtos.length) {

                        let obj = {
                            "gradeId": 0,
                            "gradeName": "string"
                        }
                        this.customizeReportGradeReqDtos.push(obj)
                    }
                }
            }

        }
    }
    SelectallGrade(event) {
        for (let i = 0; i < this.AllGradeData.length; i++) {
            if (event) {
                this.AllGradeData[i].GradeList = true;
                this.customizeReportGradeReqDtos = this.AllGradeData;
            }
            else {
                this.AllGradeData[i].GradeList = false;
                this.customizeReportGradeReqDtos = []
                if (!this.customizeReportGradeReqDtos.length) {

                    let obj = {
                        "gradeId": 0,
                        "gradeName": "string"
                    }
                    this.customizeReportGradeReqDtos.push(obj)
                }
            }
        }

    }
    SelectShiftGrp() {
        this.shift_data=[];
        for(let i=0;i<this.allShiftList.length;i++){
            if(this.allShiftList[i].shift==true){
                let temp={
                    "shiftMasterId": this.allShiftList[i].shiftMasterId
                }
                this.shift_data.push(temp);
            }
        }
        this.postRole.customizeReportShiftReqDtos = this.shift_data;
    }
    SelectAllShiftGrp(event) {
        for (let i = 0; i < this.allShiftList.length; i++) {
            if (event) {
                this.allShiftList[i].shift = true;
            }
            else {
                this.allShiftList[i].shift = false;
            }
        }
        //test
        this.SelectShiftGrp();
    }
    SelectCost(e, index) {
        if (e) {
            this.customizeReportsCostCenterReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportsCostCenterReqDtos.length; i++) {
                if (this.customizeReportsCostCenterReqDtos[i].costCenterId === index.costCenterId) {
                    this.customizeReportsCostCenterReqDtos.splice(i, 1)
                    if (!this.customizeReportsCostCenterReqDtos.length) {

                        let obj = {
                            "costCenterId": 0,
                            "costCenterName": "string"
                        }
                        this.customizeReportsCostCenterReqDtos.push(obj)
                    }
                }
            }

        }
    }
    SelectCostCenter(event) {
        for (let i = 0; i < this.allCostCenterList.length; i++) {
            if (event) {
                this.allCostCenterList[i].CostCenterSe = true;
                this.customizeReportsCostCenterReqDtos = this.allCostCenterList;
            }
            else {
                this.allCostCenterList[i].CostCenterSe = false;
                this.customizeReportsCostCenterReqDtos = []
                if (!this.customizeReportsCostCenterReqDtos.length) {
                    let obj = {
                        "costCenterId": 0,
                        "costCenterName": "string"
                    }
                    this.customizeReportsCostCenterReqDtos.push(obj)
                }
            }
        }

    }
    SelectFun(e, index) {
        if (e) {
            this.customizeReportFunctionReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportFunctionReqDtos.length; i++) {
                if (this.customizeReportFunctionReqDtos[i].functionUnitId === index.functionUnitId) {
                    this.customizeReportFunctionReqDtos.splice(i, 1)
                    if (!this.customizeReportFunctionReqDtos.length) {

                        let obj = {
                            "functionUnitId": 0,
                            "functionUnitName": "string"
                        }
                        this.customizeReportFunctionReqDtos.push(obj)
                    }
                }
            }

        }
    }
    SelectAllFunUnit(event) {
        for (let i = 0; i < this.allFunctionList.length; i++) {
            if (event) {
                this.allFunctionList[i].func = true;
                this.customizeReportFunctionReqDtos = this.allFunctionList
            }
            else {
                this.allFunctionList[i].func = false;
                this.customizeReportFunctionReqDtos = []
                if (!this.customizeReportFunctionReqDtos.length) {
                    let obj = {
                        "functionUnitId": 0,
                        "functionUnitName": "string"
                    }
                    this.customizeReportFunctionReqDtos.push(obj)
                }

            }
        }

    }
    Selectdes(e, index) {
        if (e) {
            this.customizeReportDesignationReqDtos.push(index)
        }
        else {
            for (let i = 0; i < this.customizeReportDesignationReqDtos.length; i++) {
                if (this.customizeReportDesignationReqDtos[i].descId === index.descId) {
                    this.customizeReportDesignationReqDtos.splice(i, 1)
                    if (!this.customizeReportDesignationReqDtos.length) {

                        let obj = {
                            "descId": 0,
                            "functionUnitName": "string"
                        }
                        this.customizeReportDesignationReqDtos.push(obj)
                    }
                }
            }

        }
    }
    SelectAllDesign(event) {
        for (let i = 0; i < this.allDesignationList.length; i++) {
            if (event) {
                this.allDesignationList[i].desig = true;
                this.customizeReportDesignationReqDtos = this.allDesignationList
            }
            else {
                this.allDesignationList[i].desig = false;
                this.customizeReportDesignationReqDtos = []
                if (!this.customizeReportDesignationReqDtos.length) {

                    let obj = {
                        "descId": 0,
                        "descName": "string"
                    }
                    this.customizeReportDesignationReqDtos.push(obj)
                }
            }
        }

    }

    postattendance() {
        // 
        console.log("this.postRole.selectedEmp", this.postRole.selectedEmp);
        let obj = new Array();
        for (let j = 0; j < this.EmpData.length; j++) {
            for (let i = 0; i < this.postRole.selectedEmp.length; i++) {
                console.log("this.postRole.selectedEmp[i]", this.postRole.selectedEmp[i]);


                console.log("this.EmpData", this.EmpData[j])

                if (this.postRole.selectedEmp[i].empOfficialId == this.EmpData[j].empOfficialId) {

                    obj.push(this.EmpData[j]);
                    // break;
                }
            }
        }

        this.postRole.selectedEmp = obj;
        console.log("obj", obj);

        if (this.reportName == 'Attendence Report') {
            this.postRole.fromDate = new Date(this.model.fromDate);
            console.log(this.postRole.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            console.log(this.postRole.fromDate);
            this.Spinner.show()
            this.RepService.postAttendance(this.postRole)
                .subscribe((data: any) => {
                    console.log(data)

                    console.log(data["result"])
                    sessionStorage.setItem('reportData', JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/reportview')
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
        if (this.reportName == 'Muster Report') {
            
            this.postRole.fromDate = new Date(this.model.fromDate);
            console.log(this.postRole.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            console.log(this.postRole.fromDate);
            this.Spinner.show()
            this.workdurationserve.getContractorMusterData(this.postRole)
                .subscribe((data: any) => {
                    console.log(data)

                    console.log(data["result"])
                    sessionStorage.setItem('reportData', JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/musterreports')
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
       
        else if (this.reportName == 'Monthly Attendence Report') {

            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            this.Spinner.show()
            this.RepService.postAttendance(this.postRole)
                .subscribe((data: any) => {
                    console.log(data)

                    console.log(data["result"])
                    sessionStorage.setItem('monthlyreportData', JSON.stringify(data["result"]));
                    sessionStorage.setItem('getDataForTablehead', JSON.stringify(this.daysDifference));
                    sessionStorage.setItem('getDateTablehead', JSON.stringify(this.getDate));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/monthlyattendancereports');
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });

        }
        else if (this.reportName == 'Monthly Overtime Report') {
            console.log("Monthly Overtime Report");
            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            this.Spinner.show()
            this.monthlyOverTimeReportserve.postMonthlyOverTimedata(this.postRole)
                .subscribe((data: any) => {
                    console.log(data)

                    console.log(data["result"])
                    sessionStorage.setItem('monthlyoverTimereportData', JSON.stringify(data["result"]));
                    sessionStorage.setItem('getTablehead', JSON.stringify(this.daysDifference));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/monthlyovertimereports')
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
        else if (this.reportName == 'Multiple Punches Report') {
            console.log("Multiple Punches Report");

            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            this.Spinner.show()
            this.multipleReportServe.multiplepunchesdata(this.postRole)
                .subscribe((data: any) => {

                    console.log(data)
                    console.log(data["result"])
                    sessionStorage.setItem('multiplepunchesreportData', JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/multiplereportspunches');
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
        else if (this.reportName == 'Shift Assignment Report') {

            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            this.Spinner.show()
            this.RepService.postAttendance(this.postRole).subscribe((data: any) => {
                console.log(data)

                console.log(data["result"])
                sessionStorage.setItem('monthlyreportData', JSON.stringify(data["result"]));
                sessionStorage.setItem("fromDate", this.model.fromDate);
                sessionStorage.setItem("toDate", this.model.toDate);
                this.router.navigateByUrl('/layout/shiftassignmentreports')
                this.Spinner.hide()
            },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
        
        }
        else if (this.reportName == 'Head Count Report') {
            console.log("Head Count Report");
          
        }
        // else if(this.reportName == 'Head Count Budgetory Report'){
        //     console.log("Head Count Budgetory Report");

        // }
        else if (this.reportName == 'Leave Report') {

            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            this.Spinner.show()
            this.leaveReportServe.postLeaveReportdata(this.postRole)
                .subscribe((data: any) => {
                    console.log(data)

                    console.log(data["result"])
                    sessionStorage.setItem('leavereportData', JSON.stringify(data["result"]));
                   
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/leavereports')
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
        else if (this.reportName == 'Leave Register Report') {

            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            console.log('aaaaaaaaaaaaaaaaaaa', this.postRole);
            this.Spinner.show()

            this.leaveRegisterServe.postLeaveRegisterReportdata(this.postRole)
                .subscribe((data: any) => {

                    console.log(data)
                    console.log(data["result"])
                    sessionStorage.setItem('leaveRegisterReportData', JSON.stringify(data["result"]));
                    
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/leaveRegisterreports');
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {~
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
        // else if (this.reportName == 'Leave CaryyForward Report') {

        //     this.postRole.fromDate = new Date(this.model.fromDate);
        //     this.postRole.toDate = new Date(this.model.toDate);
        //     console.log('aaaaaaaaaaaaaaaaaaa', this.postRole);
        //     this.Spinner.show()

        //     this.leaveCarryForwardServe.postLeaveCarryForwarddata(this.postRole)
        //         .subscribe((data: any) => {

        //             console.log(data)
        //             console.log(data["result"])
        //             sessionStorage.setItem('leaveCaryyForwardReport ', JSON.stringify(data["result"]));
                    
        //             sessionStorage.setItem("fromDate", this.model.fromDate);
        //             sessionStorage.setItem("toDate", this.model.toDate);
        //             this.router.navigateByUrl('/layout/leaveCaryyForwardReport');
        //             this.Spinner.hide()
        //         },
        //             (err: HttpErrorResponse) => {~
        //                 this.Spinner.hide()
        //                 console.log(err.message);
        //             });
        // }
        else if (this.reportName == 'Leave Taken Report Daywise') {
            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            console.log('leave taken report daywise', this.postRole);
            this.Spinner.show()

            this.leaveTakenReportDaywise.postLeaveTakenReportDaywisedata(this.postRole)
                .subscribe((data: any) => {
                    console.log(data)
                    console.log(data["result"])
                    sessionStorage.setItem('leaveTakenReportDaywise', JSON.stringify(data["result"]));
                  
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/leaveTakenReportDaywise')
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
        else if (this.reportName == 'Leave Taken Report') {
            this.postRole.fromDate = new Date(this.model.fromDate);
            this.postRole.toDate = new Date(this.model.toDate);
            console.log('leave taken report', this.postRole);
            this.Spinner.show()
            this.leaveTakenReportDaywise.postLeaveTakenReportdata(this.postRole)
                .subscribe((data: any) => {

                    console.log(data)
                    console.log(data["result"])
                    sessionStorage.setItem('leaveTakenReport', JSON.stringify(data["result"]));
                   
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/leaveTakenReport');
                    this.Spinner.hide()
                },
                    (err: HttpErrorResponse) => {
                        this.Spinner.hide()
                        console.log(err.message);
                    });
        }
     

        else {
            alert("Please Select Report Name");
        }

    }
  
    Departmentgroup() {
      if(this.roleofuser == "HR"|| this.roleofuser == "Report" || this.roleofuser == "Admin"){
        let busiurl = this.baseurl + '/Department/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
        this.departmentlistdata = data["result"];
        console.log("departdata", this.departmentlistdata);
        for (let i = 0; i < this.departmentlistdata.length; i++) {
            this.departmentlistdata[i].department = true;
            this.customizeReportDepartmentReqDtos = this.departmentlistdata
            this.alldepartment = true;
            }
        this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
        }
        if(this.loggedUser.sanctioningAuthority == true && this.roleofuser != "HR"){
            let busiurl = this.baseurl + '/employeeunderempid/';
            this.httpService.get(busiurl +  this.empId).subscribe(data => {
                if(data != null){
                this.departmentlistdata = data["result"];
                for (let i = 0; i < this.departmentlistdata.length; i++) {
                    this.departmentlistdata[i].department = true;
                    this.customizeReportDepartmentReqDtos = this.departmentlistdata
                    this.alldepartment = true
                }
                this.chRef.detectChanges();
                }else{
                    this.departmentlistdata =[];
                }
            },
            (err: HttpErrorResponse) => {
                console.log(err.message);
            });
        }
    }

    GradeData() {
        let busiurl = this.baseurl + '/GradeMaster/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
            this.AllGradeData = data["result"];
            for (let i = 0; i < this.AllGradeData.length; i++) {
                this.AllGradeData[i].GradeList = true;
                this.customizeReportGradeReqDtos = this.AllGradeData
                this.allGrade = true
            }
            this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }

    AllEmptype() {
        let busiurl = this.baseurl + '/EmployeeType/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
        this.AllEMp = data["result"];
        for (let i = 0; i < this.AllEMp.length; i++) {
            this.AllEMp[i].AllEMpCate = true;
            this.customizeReportEmpCategoryReqDtos = this.AllEMp
            this.allEmpCat = true
        }
        this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });

    }
    allSubDep() {
        let busiurl = this.baseurl + '/SubDepartment/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
        this.allSubDepList = data["result"];
        for (let i = 0; i < this.allSubDepList.length; i++) {
            this.allSubDepList[i].subDept = true;
            this.customizeReportSubDepartmentReqDtos = this.allSubDepList
            this.allDep = true
        }
        this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    allLoco() {
        if(this.loggedUser.roleHead=='HR' ||this.loggedUser.roleHead=='Report'){
        let busiurl = this.baseurl + '/LocationWise/';
        this.httpService.get(busiurl + this.loggedUser.locationId).subscribe(data => {
            this.allLocoList = data["result"];
            for (let i = 0; i < this.allLocoList.length; i++) {
                this.allLocoList[i].allLocoListdata = true;
                this.customizeReportLocationReqDtos = this.allLocoList;
                this.allLocoL = true;
            }
            this.chRef.detectChanges();
        },
        
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
   
   else if(this.loggedUser.roleHead=='Admin'){
        let busiurl = this.baseurl + '/Location/Active/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
            this.allLocoList = data["result"];
            for (let i = 0; i < this.allLocoList.length; i++) {
                this.allLocoList[i].allLocoListdata = true;
                this.customizeReportLocationReqDtos = this.allLocoList;
                this.allLocoL = true;
            }
            this.chRef.detectChanges();
        },
        
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }

    }
    allSubEmpType() {
        let busiurl = this.baseurl + '/SubEmployeeType/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
            this.allSubEmpTypeList = data["result"];
            for (let i = 0; i < this.allSubEmpTypeList.length; i++) {
                this.allSubEmpTypeList[i].AllSubEMpCate = true;
                this.customizeReportSubEmpCategoryReqDtos = this.allSubEmpTypeList
                this.allSubEmpCat = true
            }
            this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    allCostCenter() {
        let busiurl = this.baseurl + '/CostCenter/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
            this.allCostCenterList = data["result"];
            for (let i = 0; i < this.allCostCenterList.length; i++) {
                this.allCostCenterList[i].CostCenterSe = true;
                this.customizeReportsCostCenterReqDtos = this.allCostCenterList
                this.AllCostCenter = true
            }
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    allShift() {
        let url = this.baseurl + '/ShiftMaster/';
        this.httpService.get(url + this.compId).subscribe(data => {
            this.allShiftList = data["result"];
            for (let i = 0; i < this.allShiftList.length; i++) {
                this.customizeReportShiftReqDtos = this.allShiftList
            }
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    allFunction() {
        let url = this.baseurl + '/FunctionUnitMaster/';
        this.httpService.get(url + this.compId).subscribe(data => {
            this.allFunctionList = data["result"];
            for (let i = 0; i < this.allFunctionList.length; i++) {
                this.allFunctionList[i].func = true;
                this.customizeReportFunctionReqDtos = this.allFunctionList
                this.AllFun = true
            }
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    allBuiss() {
        let busiurl = this.baseurl + '/BusiGroup/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
            this.allBuiGrpList = data["result"];
            for (let i = 0; i < this.allBuiGrpList.length; i++) {
                this.allBuiGrpList[i].buiGr = true;
                this.customizeReportBusinessGroupReqDtos = this.allBuiGrpList
                console.log(this.customizeReportBusinessGroupReqDtos);
                this.AllBuiGrp = true
            }
            this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    allDesignation() {
        let busiurl = this.baseurl + '/Designation/';
        this.httpService.get(busiurl + this.compId).subscribe(data => {
            this.allDesignationList = data["result"];
            for (let i = 0; i < this.allDesignationList.length; i++) {
                this.allDesignationList[i].desig = true;
                this.customizeReportDesignationReqDtos = this.allDesignationList
                this.Alldesi = true
            }
            this.chRef.detectChanges();
        },
        (err: HttpErrorResponse) => {
            console.log(err.message);
        });
    }
    flag: boolean = false;
    periodicDates = [];
    getdays() {
        let date1 = sessionStorage.getItem('selected_start');
        let date2= sessionStorage.getItem('selected_end');
        var isoDateString_start = new Date(date1).toISOString();
        var isoDateString_end = new Date(date2).toISOString();
        let date_start = moment(isoDateString_start).local().format("MM/DD/YYYY");
        let date_end = moment(isoDateString_end).local().format("MM/DD/YYYY");
        var startOfWeek = moment(date_start);
        var endOfWeek = moment(date_end);
        var days = [];
        var day = startOfWeek;
        while (day <= endOfWeek) {
            days.push(day.toDate());
            day = day.clone().add(1, 'd');
        }
        let temp_Dates_array=[];
        for(let i=0;i<days.length;i++){
            let temps=moment(days[i]).format("YYYY-MM-DD");
            let temp= temps.match(/[^\s-]+-?/g);
            let parsed_date=parseInt(temp[2]);
            temp_Dates_array.push(parsed_date)
        }
        console.log(temp_Dates_array);
        this.daysDifference=temp_Dates_array;
    }
    onItemSelect(item: any) {
        console.log("item", item);
        //get data
        for(let i=0;i<this.Employeedata.length;i++){
            if(this.Employeedata[i].empOfficialId==item.empOfficialId){
                let obj={
                    "empOfficialId":item.empOfficialId,
                    "bioId":this.Employeedata[i].bioId,
                    "empPerId":this.Employeedata[i].empPerId
                }
                this.selectedEmp.push(obj);   
            }
        }
    }
    onSelectAll(items: any) {
        console.log(items);
        this.selectedEmp = [];
        for(let j=0;j<items.length;j++){
            for(let i=0;i<this.Employeedata.length;i++){
                if(this.Employeedata[i].empOfficialId==items[j].empOfficialId){
                    let obj={
                        "empOfficialId":items[j].empOfficialId,
                        "bioId":this.Employeedata[i].bioId,
                        "empPerId":this.Employeedata[i].empPerId
                    }
                    this.selectedEmp.push(obj);   
                }
            }
        }
    }
    onItemDeSelect(item: any) {
        for (var i = 0; i < this.selectedEmp.length; i++) {
            if (this.selectedEmp[i].empOfficialId == item.empOfficialId) {
                this.selectedEmp.splice(i, 1)
            }
        }
    }
    onDeSelectAll(item: any) {
        console.log(item);
        this.selectedEmp = [];
    }
    reportType(val) {
        if(this.loggedUser.roleHead=='Report' && this.reportTypeVal == 'workforce'){
            let url = this.baseurl + '/EmployeeAttendanceProcess/';
      this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
        this.Employeedata = data["result"];
        for (let index = 0; index < this.Employeedata.length; index++) {
            this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
        }
        })
    }
        this.employeeInformationReport= false;
        this.reportTypeVal = val;
        this.reportName = 'Select';
        this.activeTab = 'dateTab';
        this.SelectAllDep(true);
        this.alldepartment=true;
        this.SelectAllEmpCat(true);
        this.allEmpCat=true;
        this.SelectAllLoco(true);
        this.allLocoL=true;
        this.setDateRange('');
        this.ResetAllSelections();
        
        if(this.loggedUser.roleHead=='Admin' && this.reportTypeVal == 'workforce'){
            let url = '/EmployeeMasterOperation/';
            this.dataService.createRecors(url +this.loggedUser.compId)
            //   this.httpService.get(url + this.compId)
            .subscribe(data => {
                this.Employeedata = data["result"];
                for (let index = 0; index < this.Employeedata.length; index++) {
                    this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
                }
            },
            (err: HttpErrorResponse) => {
                console.log(err.message);
                this.Employeedata =[];
            });
        }
        else if(this.loggedUser.roleHead=='HR' && this.reportTypeVal=='workforce'){
            let url = this.baseurl + '/EmployeeAttendanceProcess/';
      this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
        this.Employeedata = data["result"];
        for (let index = 0; index < this.Employeedata.length; index++) {
            this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
        }
        })
    }

  
    }
    postAbsent() {
        let curr_year = new Date().getFullYear();
        this.model.fromDate = sessionStorage.getItem('selected_start');
        this.model.toDate = sessionStorage.getItem('selected_end');
        if(this.reportName == 'Leave Register Report'){
            if(this.selectedYear == "Select"){
              this.model.fromDate = curr_year + "-01"+"-01";
              this.model.toDate = curr_year + "-12"+"-31";
            }
            else{
                this.model.fromDate = this.selectedYear + "-01"+"-01";
                this.model.toDate = this.selectedYear + "-12"+"-31";
            }
            
        }
        if(this.reportName == 'Leave Carry Forward Report'){
            if(this.selectedYear == "Select"){
              this.model.fromDate = curr_year + "-01"+"-01";
              this.model.toDate = curr_year + "-12"+"-31";
            }
            else{
                this.model.fromDate = this.selectedYear + "-01"+"-01";
                this.model.toDate = this.selectedYear + "-12"+"-31";
            }
            
        }
        this.postRole.fromDate = this.model.fromDate;
        this.postRole.toDate = this.model.toDate;
        this.valueDepartment();
        this.getdays();
    
        
        if(this.absentReport){
            this.OnAbsentColumnChange('');
        }
        if(this.AttendenceReport){
            this.OnAttendenceColumnChange('');
        }
        if(this.workDurationReport){
            this.OnDurationColumnChange('');
        }
        if(this.musterReport){
            this.OnmusterReportColumnChanges('');
        }
        if(this.multiplePuchReport){
            this.OnMultipleColumnChange('');
        }
        if(this.lateComingReport){
            this.OnLateColumnChange('');
        }
        if(this.earlyGoingReport){
            this.OnEarlyColumnChange('');
        }
        if(this.overTimeReport){
            this.OnOverTimeColumnChange('');
        }
        
        if(this.employeeInformationReport){
            this.OnEmployeeInformationColumnChange('');
        }
        if(this.joiningUpdatesReport){
            this.OnJoiningUpdatesColumnChange('');
        }
        if(this.leavingUpdatesReport){
            this.OnLeavingUpdatesColumnChange('');
        }
        if(this.confirmationDueReport){
            this.OnConfirmationDueColumnChange('');
        }
        if(this.gratuityReport){
            this.OnGratuityColumnChange('');
        }
        if(this.assetReport){
            this.OnAssetColumnChange('');
        }
        if(this.leaveRegisterReport){
            this.OnLeaveRegisterColumnChange('');
        }
        if(this.leaveTakenReport){
            this.OnLeaveTakenColumnChange('');
        }
        if(this.leaveTakenReportInCount){
            this.OnleaveTakenInCountColumnChange('');
        }
        if(this.currentleaveBalanceReport){
            this.OnleaveBalanceColumnChange('');
        }
        if(this.leaveCaryyForwardReport){
            this. OnCaryyForwardColumnChange('');
        }
        this.valueLocation();
        this.valueEmpCate();
        //if user not selects any filter
        if(this.postRole.customizeReportDepartmentReqDtos.length==0 && this.postRole.customizeReportEmpCategoryReqDtos.length==0 && this.postRole.customizeReportLocationReqDtos.length==0 && this.postRole.customizeReportShiftReqDtos.length==0){
            this.toastr.error('Please select filters and then continue..!', 'Error');
            return false;
        }
        if(this.postRole.customizeReportDepartmentReqDtos.length==0){
            let temp={
                "deptId": 0,
                "deptName": "string"
              }
              this.postRole.customizeReportDepartmentReqDtos.push(temp);
        }
        console.log(this.postRole.customizeReportEmpCategoryReqDtos);
        if(this.postRole.customizeReportEmpCategoryReqDtos.length==0){
            let temp={
                "empTypeId": 0,
                "empTypeName": "string"
              }
              this.postRole.customizeReportEmpCategoryReqDtos.push(temp);
        }
        if(this.postRole.customizeReportLocationReqDtos.length==0){
            let temp={
                "locationId": 0,
                "locationName": "string"
              }
              this.postRole.customizeReportLocationReqDtos.push(temp);
        }
        if(this.postRole.customizeReportShiftReqDtos.length==0){
            let temp= {
                "shiftMasterId": 0,
                "shiftName": "string"
              }
              this.postRole.customizeReportShiftReqDtos.push(temp);
        }
        if(this.postRole.customizeReportContractorReqDtos.length==0){
            let temp= {
                "contractorId": 0,
                "conFirmName": "string"
              }
              this.postRole.customizeReportContractorReqDtos.push(temp);
        }
        if(this.ColumnReqDto.length==0){
            this.toastr.error('Please select columns and then continue..!', 'Error');
            return false;
        }
        if(!this.customizeReportsConditionReqDtos.length){
            this.postRole.customizeReportsConditionReqDtos = this.ColumnReqDto
        }
        else{
        this.postRole.customizeReportsConditionReqDtos = this.customizeReportsConditionReqDtos
        }

        if(this.postRole.customizeReportsConditionReqDtos.length==0){
            let temp= {
                "field": "string",
                "operator": "string",
                "value": "string"
              }
              this.postRole.customizeReportsConditionReqDtos.push(temp);
        }
        if(this.postRole.customizeReportContractorReqDtos.length==0){
            let temp={
                "contractorId": 0,
                "conFirmName": "string"
              }
              this.postRole.customizeReportContractorReqDtos.push(temp);
        }
        if(this.reportTypeVal == 'attendance'){
            //absentReport
            if (this.absentReport){
            this.Spinner.show()
            this.absentserve.postAbsentdata(this.postRole).subscribe((data: any) => {
                console.log("", data)
                console.log(data["result"]);
                this.InfoService.changeMessage(JSON.stringify(data["result"]));
                sessionStorage.setItem("fromDate", this.model.fromDate);
                sessionStorage.setItem("toDate", this.model.toDate);
                sessionStorage.setItem("customizeOrEmp","customize");
                this.router.navigate(['layout/absentreports']);
                this.Spinner.hide()
            },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
            }
            //AttendenceReport
            if (this.AttendenceReport){
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                
                this.Spinner.show();
                this.RepService.postAttendance(this.postRole).subscribe((data: any) => {
                        this.InfoService.changeMessage(JSON.stringify(data["result"]));
                        sessionStorage.setItem("fromDate", this.model.fromDate);
                        sessionStorage.setItem("toDate", this.model.toDate);
                        sessionStorage.setItem("customizeOrEmp","customize");
                        this.router.navigateByUrl('layout/reportview');
                        this.Spinner.hide();
                },(err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
               
            } 
            else if (this.workDurationReport) {
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                let temp_from_val= moment(this.model.fromDate);
                let temp_to_val= moment(this.model.toDate);
                let diff=temp_to_val.diff(temp_from_val, 'days')+1;
                if(diff>31){
                    this.toastr.error('You can create this report upto 31 Days', 'Error');
                    return false;
                }
                this.Spinner.show();
                this.workdurationserve.workDurationdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem('getTableheadworkduration', JSON.stringify(this.daysDifference));
                    sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/workingDurationreports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            else if (this.musterReport) {
                
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                let temp_from_val= moment(this.model.fromDate);
                let temp_to_val= moment(this.model.toDate);
                let diff=temp_to_val.diff(temp_from_val, 'days')+1;
                if(diff>31){
                    this.toastr.error('You can create this report upto 31 Days', 'Error');
                    return false;
                }
                this.Spinner.show();
                this.workdurationserve.getContractorMusterData(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem('getTableheadMuster', JSON.stringify(this.daysDifference));
                    sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/musterreports');
                    this.Spinner.hide();
                  
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            else if (this.multiplePuchReport) {
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                this.Spinner.show();
                this.multipleReportServe.multiplepunchesdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/multiplereportspunches');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            else if (this.overTimeReport) {
                let start=sessionStorage.getItem('selected_start');
                let end=sessionStorage.getItem('selected_end');
                let temp_from_val= moment(start);
                let temp_to_val= moment(end);
                let diff=temp_to_val.diff(temp_from_val, 'days')+1;
                if(diff>31){
                    this.toastr.error('You can create this report upto 31 Days', 'Error');
                    return false;
                }
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                this.Spinner.show();
                this.monthlyOverTimeReportserve.postMonthlyOverTimedata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem('getTablehead', JSON.stringify(this.daysDifference));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/monthlyovertimereports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            else if (this.lateComingReport) {
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                this.Spinner.show();
                this.latecomingserve.postLateComingdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/latecomingreports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
            }
            else if (this.earlyGoingReport) {
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                this.Spinner.show();
                this.earlyGoingserve.postEarlyReportdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/earlygoingreports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
    
            }
            
            else if (this.leaveReport) {
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                this.Spinner.show();
                this.leaveReportServe.postLeaveReportdata(this.postRole).subscribe((data: any) => {
                    sessionStorage.setItem('leavereportData', JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/leavereports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            else if(this.leaveTakenR){
                this.postRole.fromDate = new Date(this.model.fromDate);
                this.postRole.toDate = new Date(this.model.toDate);
                this.Spinner.show();
                this.leaveTakenReportDaywise.postLeaveTakenReportdata(this.postRole).subscribe((data: any) => {
                    sessionStorage.setItem('leaveTakenReport', JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    this.router.navigateByUrl('/layout/leaveTakenReport');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
         
        }
        if(this.reportTypeVal == 'workforce'){
            if(this.employeeInformationReport){
                this.Spinner.show();
                this.workforceService.postEmployeeInformationdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", 'employeeInformationReport');
                    this.router.navigateByUrl('layout/workForceReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            if(this.joiningUpdatesReport){
                this.Spinner.show();
                this.workforceService.postJoiningUpdatesdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", 'joiningUpdatesReport');
                    this.router.navigateByUrl('layout/workForceReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            if(this.leavingUpdatesReport){
                this.Spinner.show();
                this.workforceService.postleavingUpdatesdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", 'leavingUpdatesReport');
                    this.router.navigateByUrl('layout/workForceReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            if(this.confirmationDueReport){
                this.Spinner.show();
                this.workforceService.postconfirmationDuedata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", 'confirmationDueReport');
                    this.router.navigateByUrl('layout/workForceReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            if(this.gratuityReport){
                this.Spinner.show();
                this.workforceService.postGratuitydata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", 'gratuityReport');
                    this.router.navigateByUrl('layout/workForceReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            if(this.assetReport){
                this.Spinner.show();
                this.workforceService.postAssetdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", 'assetReport');
                    this.router.navigateByUrl('layout/workForceReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
        }
        if(this.reportTypeVal == 'leave'){
            if (this.leaveRegisterReport){
                this.Spinner.show();
                this.leaveReportService.postLeaveRegisterdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", "LeaveRegisterReport");
                    this.router.navigateByUrl('layout/leaveReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
            }
            if (this.leaveTakenReport){
                this.Spinner.show();
                this.leaveReportService.postLeaveTakendata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", "LeaveTakenReport");
                    this.router.navigateByUrl('layout/leaveReports')
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide();
                    console.log(err.message);
                });
            }
            if (this.leaveTakenReportInCount){
                this.Spinner.show();
                this.leaveReportService.postleaveTakenInCountdata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    sessionStorage.setItem("reportName", "leaveTakenReportInCount");
                    this.router.navigateByUrl('layout/leaveReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
            }
            /**START OF THE LEAVE BALANCE REPORT */
            if (this.currentleaveBalanceReport){
                sessionStorage.setItem("reportName", "LeaveBalanceReport");
                this.router.navigateByUrl('layout/leaveReports');
                this.Spinner.show();
                this.leaveReportService.postLeaveBalance(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    
                    this.router.navigateByUrl('layout/leaveReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
            }   
            /**END OF THE LEAVE BALANCE REPORT */    
            if (this.leaveCaryyForwardReport){
                
                
                sessionStorage.setItem("reportName", "leaveCaryyForwardReport");
                // this.router.navigateByUrl('layout/leaveReports');
                this.Spinner.show();
                this.leaveReportService.postLeaveCarryForwarddata(this.postRole).subscribe((data: any) => {
                    this.InfoService.changeMessage(JSON.stringify(data["result"]));
                    sessionStorage.setItem("fromDate", this.model.fromDate);
                    sessionStorage.setItem("toDate", this.model.toDate);
                    
                    this.router.navigateByUrl('layout/leaveReports');
                    this.Spinner.hide();
                },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
            } 
        }
    }
    Operator(event) {
        console.log(event);
        this.opt = event
    }
    selectedField(event) {
        console.log(event);
        this.fild = event
    }
    //absent columns
    OnAbsentColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.Absent_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.AbsentColumns.length;i++){
            if(this.AbsentColumns[i].isSelected==true){
                let temp ={
                    field: this.AbsentColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.AbsentColumns[i].Columnsid
                }
                this.Absent_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Absent_ColumnReqDto;
    }
    //Attendence_ColumnReqDto
    OnAttendenceColumnChange(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.Attendence_ColumnReqDto=[];
        for(let i=0;i<this.ColumnsForAttend.length;i++){
            if(this.ColumnsForAttend[i].isSelected==true){
                let temp ={
                    field: this.ColumnsForAttend[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.ColumnsForAttend[i].Columnsid
                }
                this.Attendence_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Attendence_ColumnReqDto;
    }
    //Duration_ColumnReqDto
    OnDurationColumnChange(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.Duration_ColumnReqDto=[];
        for(let i=0;i<this.workDurationColumn.length;i++){
            if(this.workDurationColumn[i].isSelected==true){
                let temp ={
                    field: this.workDurationColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.workDurationColumn[i].Columnsid
                }
                this.Duration_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Duration_ColumnReqDto;
    }
    OnMultipleColumnChange(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.Multipunch_ColumnReqDto=[];
        for(let i=0;i<this.multiPulPunchColumn.length;i++){
            if(this.multiPulPunchColumn[i].isSelected==true){
                let temp ={
                    field: this.multiPulPunchColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.multiPulPunchColumn[i].Columnsid
                }
                this.Multipunch_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Multipunch_ColumnReqDto;
    }
    OnmusterReportColumnChanges(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.Multipunch_ColumnReqDto=[];
        for(let i=0;i<this.musterReportColumn.length;i++){
            if(this.musterReportColumn[i].isSelected==true){
                let temp ={
                    field: this.musterReportColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.musterReportColumn[i].Columnsid
                }
                this.Multipunch_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Multipunch_ColumnReqDto;
    }
    //OnLateColumnChange
    OnLateColumnChange(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.Latepunch_ColumnReqDto=[];
        for(let i=0;i<this.overTimeColumns.length;i++){
            if(this.overTimeColumns[i].isSelected==true){
                let temp ={
                    field: this.overTimeColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.overTimeColumns[i].Columnsid
                }
                this.Latepunch_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Latepunch_ColumnReqDto;
    }
    //Earlypunch_ColumnReqDto
    OnEarlyColumnChange(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.Earlypunch_ColumnReqDto=[];
        for(let i=0;i<this.earlyGoingColumn.length;i++){
            if(this.earlyGoingColumn[i].isSelected==true){
                let temp ={
                    field: this.earlyGoingColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.earlyGoingColumn[i].Columnsid
                }
                this.Earlypunch_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.Earlypunch_ColumnReqDto;
    }
    OnOverTimeColumnChange(e){
        this.ColumnReqDto=[];
        if(!e){
            this.masterSelected=false;
        }
        this.OverTime_ColumnReqDto=[];
        for(let i=0;i<this.overTimeColumns.length;i++){
            if(this.overTimeColumns[i].isSelected==true){
                let temp ={
                    field: this.overTimeColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.overTimeColumns[i].Columnsid
                }
                this.OverTime_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.OverTime_ColumnReqDto;
    }
    
     OnEmployeeInformationColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.EmployeeInformation_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.employeeInformationColumn.length;i++){
            if(this.employeeInformationColumn[i].isSelected==true){
                let temp ={
                    field: this.employeeInformationColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.employeeInformationColumn[i].Columnsid
                }
                this.EmployeeInformation_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.EmployeeInformation_ColumnReqDto;
    }
    OnJoiningUpdatesColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.JoiningUpdates_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.joiningUpdatesColumn.length;i++){
            if(this.joiningUpdatesColumn[i].isSelected==true){
                let temp ={
                    field: this.joiningUpdatesColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.joiningUpdatesColumn[i].Columnsid
                }
                this.JoiningUpdates_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.JoiningUpdates_ColumnReqDto;
    }
    OnLeavingUpdatesColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.leavingUpdates_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.leavingUpdatesColumn.length;i++){
            if(this.leavingUpdatesColumn[i].isSelected==true){
                let temp ={
                    field: this.leavingUpdatesColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.leavingUpdatesColumn[i].Columnsid
                }
                this.leavingUpdates_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.leavingUpdates_ColumnReqDto;
    }
    OnConfirmationDueColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }   
        this.confirmationDue_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.confirmationDueColumn.length;i++){
            if(this.confirmationDueColumn[i].isSelected==true){
                let temp ={
                    field: this.confirmationDueColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.confirmationDueColumn[i].Columnsid
                }
                this.confirmationDue_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.confirmationDue_ColumnReqDto;
    }
    OnGratuityColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }   
        this.gratuity_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.gratuityColumn.length;i++){
            if(this.gratuityColumn[i].isSelected==true){
                let temp ={
                    field: this.gratuityColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.gratuityColumn[i].Columnsid
                }
                this.gratuity_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.gratuity_ColumnReqDto;
    }
    OnAssetColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }   
        this.asset_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.assetColumn.length;i++){
            if(this.assetColumn[i].isSelected==true){
                let temp ={
                    field: this.assetColumn[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.assetColumn[i].Columnsid
                }
                this.asset_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.asset_ColumnReqDto;
    }
    OnLeaveRegisterColumnChange(e){  
        if(!e){
            this.masterSelected=false;
        }      
        this.LeaveRegister_ColumnReqDto=[];
        this.ColumnReqDto=[];
        console.log(this.LeaveRegisterColumns);
        for(let i=0;i<this.LeaveRegisterColumns.length;i++){
            if(this.LeaveRegisterColumns[i].isSelected==true){
                let temp ={
                    field: this.LeaveRegisterColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.LeaveRegisterColumns[i].Columnsid
                }
                this.LeaveRegister_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.LeaveRegister_ColumnReqDto;
    }
    OnLeaveTakenColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.LeaveTaken_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.LeaveTakenColumns.length;i++){
            if(this.LeaveTakenColumns[i].isSelected==true){
                let temp ={
                    field: this.LeaveTakenColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.LeaveTakenColumns[i].Columnsid
                }
                this.LeaveTaken_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.LeaveTaken_ColumnReqDto;
    }
 
  
    OnleaveTakenInCountColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.leaveTakenInCount_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.LeaveTakenInCountColumns.length;i++){
            if(this.LeaveTakenInCountColumns[i].isSelected==true){
                let temp ={
                    field: this.LeaveTakenInCountColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.LeaveTakenInCountColumns[i].Columnsid
                }
                this.leaveTakenInCount_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.leaveTakenInCount_ColumnReqDto;
    }

    /**START OF THE LEAVE BALANCE REPORT */
    OnleaveBalanceColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this.leaveBalance_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.LeaveBalanceColumns.length;i++){
            if(this.LeaveBalanceColumns[i].isSelected==true){
                let temp ={
                    field: this.LeaveBalanceColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.LeaveBalanceColumns[i].Columnsid
                }
                this.leaveBalance_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.leaveBalance_ColumnReqDto;
    }
    OnCaryyForwardColumnChange(e){
        if(!e){
            this.masterSelected=false;
        }
        this. LeaveCaryyForward_ColumnReqDto=[];
        this.ColumnReqDto=[];
        for(let i=0;i<this.LeaveCaryyForwardColumns.length;i++){
            if(this.LeaveCaryyForwardColumns[i].isSelected==true){
                let temp ={
                    field: this.LeaveCaryyForwardColumns[i].ColumnName,
                    operator: 'String', 
                    value: 'String',
                    id : this.LeaveCaryyForwardColumns[i].Columnsid
                }
                this.LeaveCaryyForward_ColumnReqDto.push(temp);
            }
        }
        this.ColumnReqDto = this.LeaveCaryyForward_ColumnReqDto;
    }
    /**END OF THE LEAVE BALANCE REPORT */

    tabchange(){
        this.activeTab = "filterTab";
    }
    //get all employees
    getAllEmployee() {
       
        // let url = this.baseurl + '/EmployeeMasterOperation/';
        if(this.loggedUser.roleHead=='Admin'){
        

                //these ois new code 
                
               
                    let url = '/EmployeeMasterOperation/';
                    // let obj = {
                    //     "empId": this.loggedUser.empId,
                    //     "locationId":this.loggedUser.locationId,
                    //     "roleId": this.loggedUser.rollMasterId,
                    //     "compId": this.compId,
                    //     "fromDate": this.model.fromDate,
                    //     "toDate": this.model.toDate
                    //   }
                      this.dataService.createRecors(url +this.loggedUser.compId)
                    //   this.httpService.get(url + this.compId)
                    .subscribe(data => {
                        this.Employeedata = data["result"];
                        for (let index = 0; index < this.Employeedata.length; index++) {
                            this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
                        }
                    },
                    (err: HttpErrorResponse) => {
                        console.log(err.message);
                        this.Employeedata =[];
                    });
                }
    

    else if(this.loggedUser.roleHead=='HR'|| this.loggedUser.roleHead=='Report'){
      
            let url = '/EmployeeMasterOperationLite/';
            let obj = {
                "empId": this.loggedUser.empId,
                "locationId":this.loggedUser.locationId,
                "roleId": this.loggedUser.rollMasterId,
                "compId": this.compId,
                "fromDate": this.model.fromDate,
                "toDate": this.model.toDate
              }
              this.dataService.createRecord(url , obj)
            //   this.httpService.get(url + this.compId)
            .subscribe(data => {
                this.Employeedata = data["result"];
                for (let index = 0; index < this.Employeedata.length; index++) {
                    this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
                }
            },
            (err: HttpErrorResponse) => {
                console.log(err.message);
                this.Employeedata =[];
            });
        }
    
      
}

    //setdateRange
    setDateRange(report_name){
        let date_range={};
        if(report_name=='Working Duration Report' || report_name=='Monthly Overtime Report' || report_name=='Muster Report' ){
            date_range={};
        }else{
            date_range={
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }
        //test
        $(function() {
            var start = moment();
            var end = moment();
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            (<any>$('#reportrange')).daterangepicker({
                startDate: start,
                endDate: end,
                maxDate:start,
                autoApply:true,
                opens:'right',
                ranges: date_range
            }, cb);
            cb(start, end);
            (<any>$('#reportrange')).on('apply.daterangepicker', function(ev, picker) {
                let start=picker.startDate.format('YYYY-MM-DD');
                let end=picker.endDate.format('YYYY-MM-DD');
                sessionStorage.setItem("selected_start", start);
                sessionStorage.setItem("selected_end", end);
            });
            $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
                $('#reportrange').val('');
            });
        });
    }
    getyears(){
      
        this.currentyear =new Date().getFullYear();
        let min :any;
        let max:any;
        min = this.currentyear - 10;
        max = this.currentyear + 10;
        var x = document.getElementById("years");
        for(var i=min; i<=max; i++){
        this.availableYears.push({"id":i});
        }

    }   
    GenerateEmployeeWiseReport(){
        this.getdays();
        if(this.selectedEmp.length==0){
            this.toastr.error('Please Select Employees And Then Continue..!!');
            return false;
        }
        let body={
            "fromDate": sessionStorage.getItem('selected_start'),
            "toDate": sessionStorage.getItem('selected_end'),
            "selectedEmp":this.selectedEmp
        }
        //absent report
        if (this.absentReport){
            this.Spinner.show()
            this.absentserve.postAbsentdataEmpWise(body).subscribe((data: any) => {
                this.InfoService.changeMessage(JSON.stringify(data["result"]));
                sessionStorage.setItem("fromDate",body.fromDate);
                sessionStorage.setItem("toDate", body.toDate);
                sessionStorage.setItem("customizeOrEmp","employee");
                this.router.navigate(['layout/absentreports']);
                this.Spinner.hide();
            },
                (err: HttpErrorResponse) => {
                    this.Spinner.hide()
                    console.log(err.message);
                });
        }
        //attendance report
        if (this.AttendenceReport){
            this.Spinner.show();
            this.RepService.postAttendanceEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("customizeOrEmp","employee");
            this.router.navigateByUrl('layout/reportview');
            this.Spinner.hide();
            },(err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        } 
        //Working Duration Report
        if (this.workDurationReport) {
            let start=sessionStorage.getItem('selected_start');
            let end=sessionStorage.getItem('selected_end');
            let temp_from_val= moment(start);
            let temp_to_val= moment(end);
            let diff=temp_to_val.diff(temp_from_val, 'days')+1;
            if(diff>31){
                this.toastr.error('You can create this report upto 31 Days', 'Error');
                return false;
            }
            this.Spinner.show();
            this.workdurationserve.getWorkDurationEmpWise(body).subscribe((data: any) => {
                this.InfoService.changeMessage(JSON.stringify(data["result"]));
                sessionStorage.setItem('getTableheadworkduration', JSON.stringify(this.daysDifference));
                sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
                sessionStorage.setItem("fromDate", body.fromDate);
                sessionStorage.setItem("toDate", body.toDate);
                this.router.navigateByUrl('/layout/workingDurationreports');
                this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide()
                console.log(err.message);
            });

        }
        if (this.musterReport) {
        
            let start=sessionStorage.getItem('selected_start');
            let end=sessionStorage.getItem('selected_end');
            let temp_from_val= moment(start);
            let temp_to_val= moment(end);
            let diff=temp_to_val.diff(temp_from_val, 'days')+1;
            if(diff>31){
                this.toastr.error('You can create this report upto 31 Days', 'Error');
                return false;
            }
            this.Spinner.show();
            this.workdurationserve.getEmpWiseContractorMusterReport(body).subscribe((data: any) => {
                this.InfoService.changeMessage(JSON.stringify(data["result"]));
                sessionStorage.setItem('getTableheadMuster', JSON.stringify(this.daysDifference));
                sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
                sessionStorage.setItem("fromDate", body.fromDate);
                sessionStorage.setItem("toDate", body.toDate);
                this.router.navigateByUrl('/layout/musterreports');
                this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide()
                console.log(err.message);
            });

        }
        //Multiple Punches Report
        if (this.multiplePuchReport) {
            this.Spinner.show();
            this.multipleReportServe.multiplepunchesByEmp(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            this.router.navigateByUrl('/layout/multiplereportspunches');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //late coming report
        if (this.lateComingReport) {
            this.Spinner.show();
            this.latecomingserve.LateComingReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            this.router.navigateByUrl('/layout/latecomingreports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Early Going report
        if (this.earlyGoingReport) {
            this.Spinner.show();
            this.earlyGoingserve.EarlyReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            this.router.navigateByUrl('/layout/earlygoingreports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Monthly overtime report
        if (this.overTimeReport) {
            let start=sessionStorage.getItem('selected_start');
            let end=sessionStorage.getItem('selected_end');
            let temp_from_val= moment(start);
            let temp_to_val= moment(end);
            let diff=temp_to_val.diff(temp_from_val, 'days')+1;
            if(diff>31){
                this.toastr.error('You can create this report upto 31 Days', 'Error');
                return false;
            }
            this.Spinner.show();
            this.monthlyOverTimeReportserve.MonthlyOverTimeReportEmpWise(body).subscribe((data: any) => {
                this.InfoService.changeMessage(JSON.stringify(data["result"]));
                sessionStorage.setItem('getTablehead', JSON.stringify(this.daysDifference));
                sessionStorage.setItem("fromDate", body.fromDate);
                sessionStorage.setItem("toDate", body.toDate);
                this.router.navigateByUrl('/layout/monthlyovertimereports');
                this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        
        //Leave Register Report
        if (this.leaveRegisterReport){
            let curr_year = new Date().getFullYear();
            if(this.selectedYear == "Select"){
                this.model.fromDate = curr_year + "-01"+"-01";
                this.model.toDate = curr_year + "-12"+"-31";
              }
            else
            {
                body.fromDate = this.selectedYear + "-01"+"-01";
                body.toDate = this.selectedYear + "-12"+"-31";
            }
           
            this.Spinner.show();
            this.leaveReportService.getLeaveRegisterReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", "LeaveRegisterReportEmp");
            this.router.navigateByUrl('layout/leaveReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide()
                console.log(err.message);
            });
        }
        //Leave Taken Report 
        if (this.leaveTakenReport){
            this.Spinner.show();
            this.leaveReportService.getLeaveTakenReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", "LeaveTakenReport");
            this.router.navigateByUrl('layout/leaveReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Leave Taken Incount Report
        if (this.leaveTakenReportInCount){
        this.Spinner.show();
        this.leaveReportService.getleaveTakenInCountReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", "leaveTakenReportInCount");
            this.router.navigateByUrl('layout/leaveReports');
            this.Spinner.hide();
        },
        (err: HttpErrorResponse) => {
            this.Spinner.hide();
            console.log(err.message);
        });
        }

        /**START OF THE LEAVE BALANCE REPORT */
         if (this.currentleaveBalanceReport){
            this.Spinner.show();
            this.leaveReportService.getLeaveBalanceReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", "LeaveBalanceReport");
            this.router.navigateByUrl('layout/leaveReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        /**END OF THE LEAVE BALANCE REPORT */
        //Leave Carry Forward Report
        if (this.leaveCaryyForwardReport){
            let curr_year = new Date().getFullYear();
            if(this.selectedYear == "Select"){
                this.model.fromDate = curr_year + "-01"+"-01";
                this.model.toDate = curr_year + "-12"+"-31";
              }
            else
            {
                body.fromDate = this.selectedYear + "-01"+"-01";
                body.toDate = this.selectedYear + "-12"+"-31";
            }
           
            this.Spinner.show();
            this.leaveReportService. getLeaveCarryForwardReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", "leaveCaryyForwardReport");
            this.router.navigateByUrl('layout/leaveReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide()
                console.log(err.message);
            });
        }

        //Employee Information Report
        if (this.employeeInformationReport){
            this.Spinner.show();
            this.workforceService.getEmployeeInformationReportEmpWise(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", 'employeeInformationReport');
            this.router.navigateByUrl('layout/workForceReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Employee Joining Updates
        if (this.joiningUpdatesReport){
            this.Spinner.show();
            this.workforceService.getEmpWiseJoiningUpdatesReport(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", 'joiningUpdatesReport');
            this.router.navigateByUrl('layout/workForceReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Employee Leaving Updates 
        if (this.leavingUpdatesReport){
            this.Spinner.show();
            this.workforceService.getEmpWiseleavingUpdatesReport(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", 'leavingUpdatesReport');
            this.router.navigateByUrl('layout/workForceReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Confirmation Due Report
        if (this.confirmationDueReport){
            this.Spinner.show();
            this.workforceService.getEmpWiseconfirmationDueReport(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", 'confirmationDueReport');
            this.router.navigateByUrl('layout/workForceReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Gratuity Report
        if (this.gratuityReport){
            this.Spinner.show();
            this.workforceService.getEmpWiseGratuityReport(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate",body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", 'gratuityReport');
            this.router.navigateByUrl('layout/workForceReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
        //Asset Report
        if (this.assetReport){
            this.Spinner.show();
            this.workforceService.getEmpWiseInformationAssetReport(body).subscribe((data: any) => {
            this.InfoService.changeMessage(JSON.stringify(data["result"]));
            sessionStorage.setItem("fromDate", body.fromDate);
            sessionStorage.setItem("toDate", body.toDate);
            sessionStorage.setItem("reportName", 'assetReport');
            this.router.navigateByUrl('layout/workForceReports');
            this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
                this.Spinner.hide();
                console.log(err.message);
            });
        }
    }
    //reset All selections
    ResetAllSelections(){
        this.model.selectedEmp=[];
        this.SetSelection();
    }
    //SetSelection
    SetSelection(){
        this.model.selectedEmp=[];
        document.getElementById("pills-home-tab").click();
    }
    SelectedRadioButton(e){
        this.status=e.target.value;
        console.log(this.status)
     
    }
    // SelectedLeaveRadioButton(e){
    //   this.statusOfLeave=e.target.value;
    //   console.log(this.statusOfLeave);
      
    // }
}