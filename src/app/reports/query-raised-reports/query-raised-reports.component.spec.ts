import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryRaisedReportsComponent } from './query-raised-reports.component';

describe('QueryRaisedReportsComponent', () => {
  let component: QueryRaisedReportsComponent;
  let fixture: ComponentFixture<QueryRaisedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryRaisedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryRaisedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
