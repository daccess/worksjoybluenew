import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, HostListener, OnInit } from '@angular/core';
import { Router, UrlTree } from '@angular/router';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from 'src/app/shared/configurl';
import { PostReport } from 'src/app/shared/model/postRepoModel';
import { URL } from 'url';
import { dataService } from '../../shared/services/dataService';
import { InfoService } from "../../shared/services/infoService";
import { WorkDurationRepoortservice } from '../services/workDurationReportService';

@Component({
  selector: 'app-query-raised-reports',
  templateUrl: './query-raised-reports.component.html',
  styleUrls: ['./query-raised-reports.component.css']
})
export class QueryRaisedReportsComponent implements OnInit {
  themeColor:any;
  allCategoryList = [];
  selected_report;
  visitorCategoryList = [];
  allcategory:any;
  model: any = {};
  maxDate: any;
  mindDate: any;
  daysDifference = [];
  loggedUser: any;
  category_data=[];
  report_wise='customise';
  report_name: string;
  baseurl = MainURL.HostUrl;
  compId: any;
  Employeedata: any;
  dropdownSettings: any;
  selectedEmp= [];
  departmentlistdata = [];
  customizeReportDepartmentReqDtos = []
  customizeReportSubDepartmentReqDtos = []
  customizeReportLocationReqDtos = []
  customizeReportEmploymentTypeReqDtos = []
  customizeReportEmpCategoryReqDtos = []
  customizeReportContractorReqDtos = []
  customizeReportBusinessGroupReqDtos = []
  customizeReportGradeReqDtos = []
  customizeReportShiftReqDtos = []
  customizeReportsConditionReqDtos = []
  customizeReportFunctionReqDtos = []
  customizeReportsCostCenterReqDtos = []
  customizeReportDesignationReqDtos = []
  ColumnReqDto = [];
  alldepartment: any;
  roleofuser: any;
  empId: any;
  AllEMp: any;
  allEmpCat: boolean;
  allLocoList: any;
  allLocoL: boolean;
  getDate = [];
  postRole: PostReport;
  department_data = [];
  location_data: any[];
  masterSelected: boolean;
  query_ColumnReqDto: any[];
  queryColumn: any;
  reportName: any;
  
  constructor(private dataService: dataService,
    private InfoService: InfoService,
    private router: Router,
    private Spinner: NgxSpinnerService,
    public httpService: HttpClient,
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService,
    public workdurationserve: WorkDurationRepoortservice) {
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    this.report_name = 'Select';
    this.masterSelected = true;
    this.queryColumn = [
        { Columnsid: 1, ColumnName: "EmpId",isSelected:true},
        { Columnsid: 2, ColumnName: "Name",isSelected:true},
        { Columnsid: 3, ColumnName: "Qyery Details",isSelected:true},
        { Columnsid: 4, ColumnName: "Category",isSelected:true},
        { Columnsid: 5, ColumnName: "Sub-Category",isSelected:true},
        { Columnsid: 6, ColumnName: "raised On date",isSelected:true},
        { Columnsid: 7, ColumnName: "status",isSelected:true},
        { Columnsid: 8, ColumnName: "Remark By",isSelected:true},
        { Columnsid: 9, ColumnName: "Remarks",isSelected:true},
    ]
    $(function() {
      var start = moment();
      var end = moment();
      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
      (<any>$('#reportrange')).daterangepicker({
          startDate: start,
          endDate: end,
          maxDate:start,
          autoApply:true,
          opens:'right',
          ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, cb);
      cb(start, end);
      (<any>$('#reportrange')).on('apply.daterangepicker', function(ev, picker) {
          let start=picker.startDate.format('YYYY-MM-DD');
          let end=picker.endDate.format('YYYY-MM-DD');
          sessionStorage.setItem("selected_start", start);
          sessionStorage.setItem("selected_end", end);
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          $('#reportrange').val('');
      });
    });
   }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.roleofuser = this.loggedUser.mrollMaster.roleName;
    this.empId =this.loggedUser.empId;
    this.Departmentgroup();
    this.AllEmptype();
    this.getAllEmployee();
    this.allLoco();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'empOfficialId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 100,
      allowSearchFilter: true
  };
 
  }

  SetSelectedReport(event){
    console.log(event);
    this.selected_report=event;
    this.SelectAllDep(true);
    this.alldepartment=true;
    this.SelectAllEmpCat(true);
    this.allEmpCat=true;
    this.SelectAllLoco(true);
    this.allLocoL=true;
  }
  //get all employees
  getAllEmployee() {
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
    this.Employeedata = data["result"];
    for (let index = 0; index < this.Employeedata.length; index++) {
        this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
    }
    },(err: HttpErrorResponse) => {
    console.log(err.message);
    this.Employeedata =[];
    });
  }

onItemSelect(item: any) {
  console.log("item", item);
  //get data
  for(let i=0;i<this.Employeedata.length;i++){
      if(this.Employeedata[i].empOfficialId==item.empOfficialId){
          let obj={
              "empOfficialId":item.empOfficialId,
              "bioId":this.Employeedata[i].bioId,
              "empPerId":this.Employeedata[i].empPerId
          }
          this.selectedEmp.push(obj);   
      }
  }
}
 
onSelectAll(items: any) {
  console.log(items);
  this.selectedEmp = [];
  for(let j=0;j<items.length;j++){
      for(let i=0;i<this.Employeedata.length;i++){
          if(this.Employeedata[i].empOfficialId==items[j].empOfficialId){
              let obj={
                  "empOfficialId":items[j].empOfficialId,
                  "bioId":this.Employeedata[i].bioId,
                  "empPerId":this.Employeedata[i].empPerId
              }
              this.selectedEmp.push(obj);   
          }
      }
  }
}

onItemDeSelect(item: any) {
  for (var i = 0; i < this.selectedEmp.length; i++) {
      if (this.selectedEmp[i].empOfficialId == item.empOfficialId) {
          this.selectedEmp.splice(i, 1)
      }
  }
}
onDeSelectAll(item: any) {
  console.log(item);
  this.selectedEmp = [];
}


  getdays() {
    let date1 = sessionStorage.getItem('selected_start');
    let date2= sessionStorage.getItem('selected_end');
    var isoDateString_start = new Date(date1).toISOString();
    var isoDateString_end = new Date(date2).toISOString();
    let date_start = moment(isoDateString_start).local().format("MM/DD/YYYY");
    let date_end = moment(isoDateString_end).local().format("MM/DD/YYYY");
    var startOfWeek = moment(date_start);
    var endOfWeek = moment(date_end);
    var days = [];
    var day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }
    let temp_Dates_array=[];
    for(let i=0;i<days.length;i++){
        let temps=moment(days[i]).format("YYYY-MM-DD");
        let temp= temps.match(/[^\s-]+-?/g);
        let parsed_date=parseInt(temp[2]);
        temp_Dates_array.push(parsed_date)
    }
    this.daysDifference=temp_Dates_array;
  }

  Departmentgroup() {
    if(this.roleofuser == "HR"){
      let busiurl = this.baseurl + '/Department/';
      this.httpService.get(busiurl + this.compId).subscribe(data => {
      this.departmentlistdata = data["result"];
      for (let i = 0; i < this.departmentlistdata.length; i++) {
          this.departmentlistdata[i].department = true;
          this.customizeReportDepartmentReqDtos = this.departmentlistdata
          this.alldepartment = true;
          }
      this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
          console.log(err.message);
      });
      }
      if(this.loggedUser.sanctioningAuthority == true && this.roleofuser != "HR"){
          let busiurl = this.baseurl + '/employeeunderempid/';
          this.httpService.get(busiurl +  this.empId).subscribe(data => {
              if(data != null){
              this.departmentlistdata = data["result"];
              for (let i = 0; i < this.departmentlistdata.length; i++) {
                  this.departmentlistdata[i].department = true;
                  this.customizeReportDepartmentReqDtos = this.departmentlistdata
                  this.alldepartment = true
              }
              this.chRef.detectChanges();
              }else{
                  this.departmentlistdata =[];
              }
          },
          (err: HttpErrorResponse) => {
              console.log(err.message);
          });
      }
  }

  AllEmptype() {
    let busiurl = this.baseurl + '/EmployeeType/';
    this.httpService.get(busiurl + this.compId).subscribe(data => {
    this.AllEMp = data["result"];
    for (let i = 0; i < this.AllEMp.length; i++) {
        this.AllEMp[i].AllEMpCate = true;
        this.customizeReportEmpCategoryReqDtos = this.AllEMp
        this.allEmpCat = true
    }
    this.chRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
        console.log(err.message);
    });

  }

  allLoco() {
    let busiurl = this.baseurl + '/Location/';
    this.httpService.get(busiurl + this.compId).subscribe(data => {
        this.allLocoList = data["result"];
        for (let i = 0; i < this.allLocoList.length; i++) {
            this.allLocoList[i].allLocoListdata = true;
            this.customizeReportLocationReqDtos = this.allLocoList;
            this.allLocoL = true;
        }
        this.chRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
        console.log(err.message);
    });
  }

  valueDepartment() {
    this.department_data=[];
    for(let i=0;i<this.departmentlistdata.length;i++){
        if(this.departmentlistdata[i].department==true){
            let temp={
                "deptId": this.departmentlistdata[i].deptId
            }
              this.alldepartment=false;
            this.department_data.push(temp);
        }
    }
    this.model.customizeReportDepartmentReqDtos = this.department_data;
 }
 SelectAllDep(event) {
    for (let i = 0; i < this.departmentlistdata.length; i++) {
        if(event) {
            this.departmentlistdata[i].department = true;
        }
        else {
            this.departmentlistdata[i].department = false;
        }
    }
    this.valueDepartment();
}

valueLocation() {
  this.location_data=[];
for(let i=0;i<this.allLocoList.length;i++){
      if(this.allLocoList[i].allLocoListdata==true){
          let temp={
              "locationId": this.allLocoList[i].locationId
          }
          this.location_data.push(temp);
      }
  }
  this.model.customizeReportLocationReqDtos = this.location_data;
}
SelectAllLoco(event) {
  for (let i = 0; i < this.allLocoList.length; i++) {
      if (event) {
          this.allLocoList[i].allLocoListdata = true;
      }
      else {
          this.allLocoList[i].allLocoListdata = false;
      }
  }
  this.valueLocation();
}

valueEmpCate() {
  this.category_data=[];
  for(let i=0;i<this.AllEMp.length;i++){
      if(this.AllEMp[i].AllEMpCate==true){
          let temp={
              "empTypeId": this.AllEMp[i].empTypeId
          }
          this.category_data.push(temp);
      }
  }
  this.model.customizeReportEmpCategoryReqDtos = this.category_data;
}
SelectAllEmpCat(event) {
  for (let i = 0; i < this.AllEMp.length; i++) {
      if (event) {
          this.AllEMp[i].AllEMpCate = true;
      }
      else {
          this.AllEMp[i].AllEMpCate = false;
      }
  }
  //test
  this.valueEmpCate();
} 

OnQueryRaisedColumnChange(e){
  if(!e){
      this.masterSelected=false;
  }   
  this.query_ColumnReqDto=[];
  this.ColumnReqDto=[];
  for(let i=0;i<this.queryColumn.length;i++){
      if(this.queryColumn[i].isSelected==true){
          let temp ={
              field: this.queryColumn[i].ColumnName,
              operator: 'String', 
              value: 'String',
              id : this.queryColumn[i].Columnsid
          }
          this.query_ColumnReqDto.push(temp);
      }
  }
  this.ColumnReqDto = this.query_ColumnReqDto;
}
 

  onCreateReport(){
    let curr_year = new Date().getFullYear();
    this.model.fromDate = sessionStorage.getItem('selected_start');
    this.model.toDate = sessionStorage.getItem('selected_end');
    console.log("***********",this.model.fromDate);
   
    this.valueDepartment();
    this.getdays();
  
    this.OnQueryRaisedColumnChange('');
    this.valueLocation();
    this.valueEmpCate();
    //if user not selects any filter
    if(this.model.customizeReportDepartmentReqDtos.length==0 && this.model.customizeReportEmpCategoryReqDtos.length==0 && this.model.customizeReportLocationReqDtos.length==0 && this.model.customizeReportShiftReqDtos.length==0){
        this.toastr.error('Please select filters and then continue..!', 'Error');
        return false;
    }
    if(this.model.customizeReportDepartmentReqDtos.length==0){
        let temp={
            "deptId": 0,
            "deptName": "string"
          }
          this.model.customizeReportDepartmentReqDtos.push(temp);
    }
    console.log(this.model.customizeReportEmpCategoryReqDtos);
    if(this.model.customizeReportEmpCategoryReqDtos.length==0){
        let temp={
            "empTypeId": 0,
            "empTypeName": "string"
          }
          this.model.customizeReportEmpCategoryReqDtos.push(temp);
    }
    if(this.model.customizeReportLocationReqDtos.length==0){
        let temp={
            "locationId": 0,
            "locationName": "string"
          }
          this.model.customizeReportLocationReqDtos.push(temp);
    }
    
   
    if(this.ColumnReqDto.length==0){
        this.toastr.error('Please select columns and then continue..!', 'Error');
        return false;
    }
    if(!this.customizeReportsConditionReqDtos.length){
        this.model.customizeReportsConditionReqDtos = this.ColumnReqDto
    }
    else{
    this.model.customizeReportsConditionReqDtos = this.customizeReportsConditionReqDtos
    }

    if(this.model.customizeReportsConditionReqDtos.length==0){
        let temp= {
            "field": "string",
            "operator": "string",
            "value": "string"
          }
          this.model.customizeReportsConditionReqDtos.push(temp);
    }

    
      this.Spinner.show();
      let url = this.baseurl + '/getQueryReport';
      this.workdurationserve.getEmpWiseQueryRaisedReport(this.model, url).subscribe((data: any) => {
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem("fromDate", this.model.fromDate);
          sessionStorage.setItem("toDate", this.model.toDate);
          sessionStorage.setItem("reportName", 'assetReport');
          this.router.navigateByUrl('/layout/queryRaisedReports');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
          this.Spinner.hide();
          console.log(err.message);
      });
  
  }

  GenerateEmployeeWiseReport(){
    console.log(this.model);
    console.log(this.selectedEmp);
    this.model.fromDate = sessionStorage.getItem('selected_start');
    this.model.toDate = sessionStorage.getItem('selected_end');
    //this.getdays(new Date(this.model.fromDate), new Date(this.model.toDate));
    if(this.selectedEmp.length==0){
        this.toastr.error('Please Select Employees And Then Continue..!!');
        return false;
    }
    let body={
        "fromDate": sessionStorage.getItem('selected_start'),
        "toDate": sessionStorage.getItem('selected_end'),
        "selectedEmp":this.selectedEmp
    }
    console.log(body);
      let start=sessionStorage.getItem('selected_start');
      let end=sessionStorage.getItem('selected_end');
      let temp_from_val= moment(start);
      let temp_to_val= moment(end);
      let diff=temp_to_val.diff(temp_from_val, 'days')+1;
      
      this.Spinner.show();
      let url = this.baseurl + '/getEmpWiseQueryRaiseReport'; 
      this.workdurationserve.getEmpWiseQueryRaisedReport(body, url).subscribe((data: any) => {
          this.InfoService.changeMessage(JSON.stringify(data["result"]));
          sessionStorage.setItem('getTableheadQueryRaised', JSON.stringify(this.daysDifference));
          sessionStorage.setItem('getDateTable', JSON.stringify(this.getDate));
          sessionStorage.setItem("fromDate", body.fromDate);
          sessionStorage.setItem("toDate", body.toDate);
          this.router.navigateByUrl('/layout/queryRaisedReports');
          this.Spinner.hide();
      },
      (err: HttpErrorResponse) => {
          this.Spinner.hide()
          console.log(err.message);
      });
   
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Back button pressed');
    this.router.navigateByUrl('/layout/workforcemenu');
  }
}
