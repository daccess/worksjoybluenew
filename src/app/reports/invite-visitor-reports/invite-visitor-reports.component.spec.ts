import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteVisitorReportsComponent } from './invite-visitor-reports.component';

describe('InviteVisitorReportsComponent', () => {
  let component: InviteVisitorReportsComponent;
  let fixture: ComponentFixture<InviteVisitorReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteVisitorReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteVisitorReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
