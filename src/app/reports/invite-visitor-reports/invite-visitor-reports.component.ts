import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { dataService } from '../../shared/services/dataService';
import { InfoService } from "../../shared/services/infoService";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MainURL } from 'src/app/shared/configurl';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-invite-visitor-reports',
  templateUrl: './invite-visitor-reports.component.html',
  styleUrls: ['./invite-visitor-reports.component.css']
})
export class InviteVisitorReportsComponent implements OnInit {
  themeColor:any;
  allCategoryList = [];
  selected_report;
  visitorCategoryList = [];
  allcategory:any;
  model: any = {};
  maxDate: any;
  mindDate: any;
  daysDifference = [];
  loggedUser: any;
  category_data=[];
  report_wise='customise';
  report_name: string;
  baseurl = MainURL.HostUrl;
  compId: any;
  Employeedata: any;
  dropdownSettings: any;
  selectedEmp= [];
  constructor(private dataService: dataService,private InfoService: InfoService,private router: Router,private Spinner: NgxSpinnerService,public httpService: HttpClient) {
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    this.report_name = 'Select';
    $(function() {
      var start = moment();
      var end = moment();
      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
      (<any>$('#reportrange')).daterangepicker({
          startDate: start,
          endDate: end,
          maxDate:start,
          autoApply:true,
          opens:'right',
          ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, cb);
      cb(start, end);
      (<any>$('#reportrange')).on('apply.daterangepicker', function(ev, picker) {
          let start=picker.startDate.format('YYYY-MM-DD');
          let end=picker.endDate.format('YYYY-MM-DD');
          sessionStorage.setItem("selected_start", start);
          sessionStorage.setItem("selected_end", end);
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          $('#reportrange').val('');
      });
  });
   }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.getVisitorCategory();
    this.getAllEmployee();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'empPerId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 100,
      allowSearchFilter: true
  };
  }

  SetSelectedReport(event){
    console.log(event);
    this.selected_report=event;
    this.visitorCategoryList = [];
    this.getVisitorCategory();
  }
  //get all employees
  getAllEmployee() {
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
    this.Employeedata = data["result"];
    for (let index = 0; index < this.Employeedata.length; index++) {
        this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
    }
    },(err: HttpErrorResponse) => {
    console.log(err.message);
    this.Employeedata =[];
    });
  }
  onItemSelect(item: any) {
    console.log("item", item);
    //get data
    for(let i=0;i<this.Employeedata.length;i++){
        if(this.Employeedata[i].empPerId==item.empPerId){
            let obj={
                "empPerId":item.empPerId,
            }
            this.selectedEmp.push(obj);   
        }
    }
}
  onSelectAll(items: any) {
    console.log(items);
    this.selectedEmp = [];
    for(let j=0;j<items.length;j++){
        for(let i=0;i<this.Employeedata.length;i++){
            if(this.Employeedata[i].empPerId==items[j].empPerId){
                let obj={
                    "empPerId":items[j].empPerId,
                }
                this.selectedEmp.push(obj);   
            }
        }
    }
}
onItemDeSelect(item: any) {
  for (var i = 0; i < this.selectedEmp.length; i++) {
      if (this.selectedEmp[i].empPerId == item.empPerId) {
          this.selectedEmp.splice(i, 1)
      }
  }
}

onDeSelectAll(item: any) {
  console.log(item);
  this.selectedEmp = [];
}

  getVisitorCategory(){
    let url = "/api/Appointment/GetVisitorCategory";
    this.dataService.getInviteVisitorsRecordsNoFilter(url)
    .subscribe(data => {
      console.log("Visitors category list",data);
      for(let i = 0; i < data.length; i++){
        if(data[i].categoryId  != 0){
          this.visitorCategoryList.push(data[i]);
        }
      }
      console.log("Visitors category list",this.visitorCategoryList);
      for(let i = 0; i < this.visitorCategoryList.length; i++){
        this.visitorCategoryList[i].allCatListdata = true;
        this.allcategory = true;
      }
    });
  }

  SelectAllCategories(event) {
    console.log(event);
    for (let i = 0; i < this.visitorCategoryList.length; i++) {
        if (event) {
            this.visitorCategoryList[i].allCatListdata = true;
        }
        else {
            this.visitorCategoryList[i].allCatListdata = false;
        }
    }
    console.log(this.visitorCategoryList);
  }

  getdays() {
    let date1 = sessionStorage.getItem('selected_start');
    let date2= sessionStorage.getItem('selected_end');
    var isoDateString_start = new Date(date1).toISOString();
    var isoDateString_end = new Date(date2).toISOString();
    let date_start = moment(isoDateString_start).local().format("MM/DD/YYYY");
    let date_end = moment(isoDateString_end).local().format("MM/DD/YYYY");
    var startOfWeek = moment(date_start);
    var endOfWeek = moment(date_end);
    var days = [];
    var day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }
    let temp_Dates_array=[];
    for(let i=0;i<days.length;i++){
        let temps=moment(days[i]).format("YYYY-MM-DD");
        let temp= temps.match(/[^\s-]+-?/g);
        let parsed_date=parseInt(temp[2]);
        temp_Dates_array.push(parsed_date)
    }
    this.daysDifference=temp_Dates_array;
  }

  SelectAllVisitorCategories(event) {
    console.log(event);
    for (let i = 0; i < this.visitorCategoryList.length; i++) {
        if (event) {
            this.visitorCategoryList[i].allCatListdata = true;
        }
        else {
            this.visitorCategoryList[i].allCatListdata = false;
        }
    }
    console.log(this.visitorCategoryList);
  }

  valueCategory() {
    this.model.VisitorCategoryId = "";
    for(let i=0;i<this.visitorCategoryList.length;i++){
        if(this.visitorCategoryList[i].allCatListdata==true){ 
          this.model.VisitorCategoryId = this.model.VisitorCategoryId + "," + this.visitorCategoryList[i].categoryId;
        }
    }
    this.model.VisitorCategoryId = this.model.VisitorCategoryId.substring(this.model.VisitorCategoryId.indexOf(',')+1);
    console.log(this.model.VisitorCategoryId);
  }

  onCreateReport(){
    this.getdays();
    console.log(this.report_wise);
    let temp_arr=[];
    if(this.report_wise=="employee_wise"){
      for(let i=0;i<this.selectedEmp.length;i++){
        temp_arr.push(this.selectedEmp[i].empPerId);
      }
    }else{
      for(let j=0;j<this.Employeedata.length;j++){
        temp_arr.push(this.Employeedata[j].empPerId);
      }
    }
    let val=temp_arr.join();
    console.log(val);
    this.model.EmployeeId =val;
    this.model.UserId = this.loggedUser.empPerId;
    let start = sessionStorage.getItem('selected_start');
    let end = sessionStorage.getItem('selected_end');
    this.model.fromDate = moment(start).format("YYYY-MM-DDTHH:mm:ss");
    this.model.toDate  = moment(end).format("YYYY-MM-DDTHH:mm:ss");
    this.valueCategory();
    if(this.model.VisitorCategoryId == ""){
       this.model.VisitorCategoryId = "0";
    }
    if(this.selected_report=='Periodic Pass Validity Report'){
      let url = "/api/Appointment/GetPeriodicPassValidityReportData"; 
      this.Spinner.show();
      this.postData(url, this.selected_report);
    }

    if(this.selected_report == 'Visitor Appointment Report'){
      let url = "/api/Appointment/GetAppointmentReportData";
      this.Spinner.show();
      this.postData(url, this.selected_report);
    }

    if(this.selected_report == "Visitor Visited Report"){
      let url = "/api/Appointment/GetVisitorVisitedReportData";
      this.Spinner.show();
      this.postData(url, this.selected_report);
    }

    if(this.selected_report == "Periodic Pass IN-OUT Report"){
      let url = "/api/Appointment/GetPeriodicVisitorInOutReportData";
      this.Spinner.show();
      this.postData(url, this.selected_report);
    }

  }

  postData(url, reportName){
    this.dataService.createInviteVisitorsRecord(url, this.model)
    .subscribe(data => {
        this.InfoService.changeMessage(data);
        sessionStorage.setItem("fromDate", this.model.fromDate);
        sessionStorage.setItem("toDate", this.model.toDate);
        sessionStorage.setItem("reportName",reportName);
        this.router.navigateByUrl('/layout/inviteVisitorReports');
        this.Spinner.hide();
    });
  }
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Back button pressed');
    this.router.navigateByUrl('/layout/workforcemenu');
  }
}
