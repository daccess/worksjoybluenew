import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ReportsComponent } from 'src/app/reports/reports.component';
import { ContractorportsComponent } from './pages/contractorreports/contractorreports.component';
import { InviteVisitorReportsComponent } from './invite-visitor-reports/invite-visitor-reports.component';
import { QueryRaisedReportsComponent } from './query-raised-reports/query-raised-reports.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ReportsComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'reports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'reports',
                    component: ReportsComponent
                },
                {
                    path:'contractor-reports',
                    component: ContractorportsComponent
                },
                {
                    path:'inviteVisitor-reports',
                    component: InviteVisitorReportsComponent
                },
                {
                    path:'query-raised-reports',
                    component: QueryRaisedReportsComponent
                }


            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);