import { routing } from './reports.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule, DatePipe } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ReportsComponent } from 'src/app/reports/reports.component';
import { ReportService } from '../shared/services/ReportService';
import { AbsentRepoort } from './services/AbsentReportService'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { from } from 'rxjs';
import { WorkDurationRepoortservice } from './services/workDurationReportService';
import { HeadCountservice } from './services/HeadCountservice.1';
import { leaveReportService } from './services/LeaveReportservice';
import { LateComingReportService } from './services/lateComingService';
import { EarlyGoingService } from './services/earlyGoingservice';
import { cOffReportservice } from './services/coffServices';
import { MonthlyOvertimeReposervice } from './services/MonthlyOvertimeReportservice';
import { MultiplePunchesRepoortservice } from './services/multiplepunchesreport';
import { leaveRegisterReportService } from './services/leaveRegisterReportservice';
import { ContractorportsComponent } from './pages/contractorreports/contractorreports.component';
import { WorkForceReportService } from './services/work-force-report.service';
import { InviteVisitorReportsComponent } from './invite-visitor-reports/invite-visitor-reports.component';
import { QueryRaisedReportsComponent } from './query-raised-reports/query-raised-reports.component';

@NgModule({
    declarations: [
        ReportsComponent,
        ContractorportsComponent,
        InviteVisitorReportsComponent,
        QueryRaisedReportsComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule
    ],
    providers: [ReportService,
        AbsentRepoort,
        WorkDurationRepoortservice,
        HeadCountservice,
        leaveReportService, 
        LateComingReportService, 
        EarlyGoingService,
        cOffReportservice,
        MonthlyOvertimeReposervice,
        MultiplePunchesRepoortservice,
        leaveRegisterReportService,
        WorkForceReportService,
        DatePipe
    ]
  })
  export class ReportsModule { 
      constructor(){

      }
  }
  