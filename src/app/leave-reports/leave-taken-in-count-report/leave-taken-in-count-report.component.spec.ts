import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveTakenInCountReportComponent } from './leave-taken-in-count-report.component';

describe('LeaveTakenInCountReportComponent', () => {
  let component: LeaveTakenInCountReportComponent;
  let fixture: ComponentFixture<LeaveTakenInCountReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveTakenInCountReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveTakenInCountReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
