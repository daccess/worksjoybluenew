import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-leave-taken-in-count-report',
  templateUrl: './leave-taken-in-count-report.component.html',
  styleUrls: ['./leave-taken-in-count-report.component.css'],
  providers: [ExcelService, DatePipe]
})
export class LeaveTakenInCountReportComponent implements OnInit {

  marked1 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;
  marked10 = true;
  marked11 = true;
  marked12 = true;
  

  srNoCheckbox = true
  employeeNameCheckbox = true;
  leaveNameCheckbox = true;
  leaveCodeCheckbox = true;
  startDateCheckbox = true;
  endDateCheckbox = true;
  leaveCountCheckbox = true;
  leaveTypeCheckbox = true;
  typeOfLeaveCheckbox = true;
  plannedUnplannedCheckbox = true;
  leaveResonCheckbox = true;
 
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  reportData =[];
  leaveSubHeaders = [];
  modifiedArr = [];
  report_time: any;
  table_length: any;
  
  
  constructor(private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router){
      this.Init();
  }

  ngOnInit() {
  }

  Init(){
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });

    /*Start of Dynamic subheaders list in table*/
    for(let i=0;i<this.AllReport.length;i++){
      if(this.AllReport[i].reportEmpNameAndIdResDto != null){
        if(this.AllReport[i].leaveTakenReportInCountResDtos.length!=0){
          for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtos.length; j++){
              if(j != this.AllReport[i].leaveTakenReportInCountResDtos[j][0]){
                  this.leaveSubHeaders.push(this.AllReport[i].leaveTakenReportInCountResDtos[j][0]);
            }
          }
        }
      }
    }

    this.leaveSubHeaders = this.leaveSubHeaders.filter(leavename => (leavename != undefined));

    this.leaveSubHeaders =this.leaveSubHeaders.filter( function( item, index, inputArray ) {
      return inputArray.indexOf(item) == index;
    });
    this.leaveSubHeaders.sort(function(a, b){
      var nameC=a.toLowerCase();
      var nameD=b.toLowerCase();
      if (nameC < nameD) //sort string ascending
          return -1 
      if (nameC > nameD)
          return 1
      return 0 //default return value (no sorting)
    })
    this.table_length=this.leaveSubHeaders.length + 6;
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
  /*End of Dynamic headers list in table*/

  /*Start of Dynamic data list in table*/
    for(let i = 0; i < this.AllReport.length; i++){
      let temp1 = [];
      let temp2 = [];
      let totalCount = 0;
      let totalCurrentLeaves = 0;
      let employeeName = "";
      let empId = 0;
      let total = 0;
      if(this.AllReport[i].leaveTakenReportInCountResDtos.length!=0){
      if(this.AllReport[i].reportEmpNameAndIdResDto != null){
        empId = this.AllReport[i].reportEmpNameAndIdResDto.empId;
          employeeName = this.AllReport[i].reportEmpNameAndIdResDto.fullName + " " + this.AllReport[i].reportEmpNameAndIdResDto.lastName;
        if(this.AllReport[i].leaveTakenReportInCountResDtos.length!=0){
         
          let leavesArr = [];
          for(let k = 0; k < this.AllReport[i].leaveTakenReportInCountResDtos.length; k++){
              leavesArr.push(this.AllReport[i].leaveTakenReportInCountResDtos[k][0]);
          }
          var filterdata = this.leaveSubHeaders.filter((unmatchLeaves) => !leavesArr.includes(unmatchLeaves));
          let arrLength = this.AllReport[i].leaveTakenReportInCountResDtos.length;

          for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtos.length; j++){
            let leavesDataArr = [];
            let coff = 0;
            if(this.AllReport[i].leaveTakenReportInCountResDtos[j][0]){
              total = total + this.AllReport[i].leaveTakenReportInCountResDtos[j][2];
            }
             
           
            if(j == this.AllReport[i].leaveTakenReportInCountResDtos.length-1){
              if(!this.AllReport[i].leaveTakenReportInCountResDtos[j][0]){
                coff = this.AllReport[i].leaveTakenReportInCountResDtos[j].noOfCoff; 
              }
            }

            if(this.AllReport[i].leaveTakenReportInCountResDtos[j][0]){
            
            if(filterdata.length == 0){
              temp1.push({
                leavename: this.AllReport[i].leaveTakenReportInCountResDtos[j][0],
                leavetype: this.AllReport[i].leaveTakenReportInCountResDtos[j][1],
                count: this.AllReport[i].leaveTakenReportInCountResDtos[j][2],
              });
            }
            else{
              if(j == 0){
                for(let k = 0; k < filterdata.length; k++){
                  temp1.push({
                    leavename: filterdata[k],
                    leavetype: "Paid",
                    count: 0
                  });
                }
              }
              temp1.push({
                leavename: this.AllReport[i].leaveTakenReportInCountResDtos[j][0],
                leavetype: this.AllReport[i].leaveTakenReportInCountResDtos[j][1],
                count: this.AllReport[i].leaveTakenReportInCountResDtos[j][2]
              });
            }
          }

            if(j == this.AllReport[i].leaveTakenReportInCountResDtos.length-1){
              if(arrLength == 1){
                if(!this.AllReport[i].leaveTakenReportInCountResDtos[j][0]){
                  for(let k = 0; k < filterdata.length; k++){
                    temp1.push({
                      leavename: filterdata[k],
                      leavetype: "Paid",
                      count: 0
                    });
                  }
                }
              }
              total = total + coff;
              temp2.push(coff);
              temp2.push(total);
            }
                    
            if(this.AllReport[i].leaveTakenReportInCountResDtos[j][0]){
              temp1.sort(function(a, b){
                var nameA=a.leavename.toLowerCase();
                var nameB=b.leavename.toLowerCase();
                if (nameA < nameB) //sort string ascending
                    return -1 
                if (nameA > nameB)
                    return 1
                return 0 //default return value (no sorting)
            });
            }
          }
        }
        else{
          for(let i = 0; i < this.leaveSubHeaders.length; i++){
            temp1.push({
              leavename: this.leaveSubHeaders[i],
              leavetype: "Paid",
              count:0,
            });
          }
          temp2.push(0);
          temp2.push(0);
        }  
      
        this.reportData.push({
          empId: empId,
          employeeName : employeeName,
          leavesdata: temp1,
          coffAndTot: temp2
          });
      }
    }
  
    /*End of Dynamic data list in table*/

    this.exceldata = this.reportData;
    this.exceldata1=this.reportData;
    }
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
  toggleVisibility10(e) {
    this.marked10 = e.target.checked;
  }
  toggleVisibility11(e) {
    this.marked11 = e.target.checked;
  }
  toggleVisibility12(e) {
    this.marked12 = e.target.checked;
  }

  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Leave Taken InCount Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#leaveTaken_InCount',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'right'},
          3: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        // doc.setTextColor(0, 0, 0);
        // doc.setFontSize(7);
        // doc.setFontStyle("Arial");
        // doc.text(13, 15, 'Abbreviation:-  WD: Working Day');
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Leave Taken InCount Report'+this.report_time+'.pdf');  
  }

}
