import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveTakenReportComponent } from './leave-taken-report.component';

describe('LeaveTakenReportComponent', () => {
  let component: LeaveTakenReportComponent;
  let fixture: ComponentFixture<LeaveTakenReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveTakenReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveTakenReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
