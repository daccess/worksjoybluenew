import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';


@Component({
  selector: 'app-leave-taken-report',
  templateUrl: './leave-taken-report.component.html',
  styleUrls: ['./leave-taken-report.component.css'],
  providers: [ExcelService, DatePipe]
})
export class LeaveTakenReportComponent implements OnInit {

  marked1 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;
  marked10 = true;
  marked11 = true;
  marked12 = true;
  marked13 = true;
  

  srNoCheckbox = true;
  employeeNameCheckbox = true;
  leaveNameCheckbox = true;
  leaveCodeCheckbox = true;
  startDateCheckbox = true;
  endDateCheckbox = true;
  leaveCountCheckbox = true;
  leaveTypeCheckbox = true;
  typeOfLeaveCheckbox = true;
  // plannedUnplannedCheckbox = true;
  leaveResonCheckbox = true;
  empIdCheckbox = true;
 
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  reportData =[];
  report_time: any;
  table_length: any;
  
  constructor(private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });
    var k = 0;
    for(let i = 0; i < this.AllReport.length;i++){
      var value = this.AllReport[i];
      for(let j = 0; j < this.AllReport[i].length; j++){
         this.reportData[k] = this.AllReport[i][j];
         k++;   
      }
   }

    this.exceldata = this.reportData;
    this.exceldata1=this.reportData;
    this.table_length = 10;
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
  }

  ngOnInit() {
  }

  // exportAsXLSX(): void {
  //   let excelData = new Array();
  
  //   for (let i = 0; i < this.exceldata1.length; i++) {
  //     let obj: any = {}
  //     if (this.marked1) {
  //       obj.SrNo = i+1;
  //     }
  //     if (this.marked13) {
  //       obj.EmpId = this.exceldata1[i].empId;
  //     }
  //     if (this.marked3) {
  //       obj.EmployeeName = this.exceldata1[i].fullName + this.exceldata1[i].lastName;
  //     }
  //     if (this.marked4) {
  //       obj.LeaveName = this.exceldata1[i].leaveName;
  //     }
  //     if (this.marked5) {
  //       obj.LeaveCode = this.exceldata1[i].leaveCode;
  //     }
  //     if (this.marked6) {
  //       let temp=this.exceldata1[i].startDate;
  //       obj.StartDate = temp;
  //     }
  //     if (this.marked7) {
  //       let temp=this.exceldata1[i].endDate;
  //       obj.EndDate = temp;
  //     }
  //     if (this.marked8) {
  //       obj.NumberOfLeavesTaken = this.exceldata1[i].leaveCount;
  //     }
  //     // if (this.marked9) {
  //     //   obj.LeaveType = this.exceldata1[i].leaveType;
  //     // }
  //     if (this.marked9) {
  //       obj.LeaveType = this.exceldata1[i].typeOfLeave;
  //     }
  //     // if (this.marked11) {
  //     //   obj.PlannedOrUnplanned = "PlannedOrUnplanned";
  //     // }
  //     if (this.marked12) {
  //       obj.Reason = this.exceldata1[i].leaveReson;
  //     }
  //     excelData.push(obj)
  //   }
  //   this.excelService.exportAsExcelFile(excelData, 'leaveTakenReport');
  // }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
  toggleVisibility10(e) {
    this.marked10 = e.target.checked;
  }
  toggleVisibility11(e) {
    this.marked11 = e.target.checked;
  }
  toggleVisibility12(e) {
    this.marked12 = e.target.checked;
  }
  toggleVisibility13(e) {
    this.marked13 = e.target.checked;
  }

  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Leave Taken Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#my-table',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'right'},
          3: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        // doc.setTextColor(0, 0, 0);
        // doc.setFontSize(7);
        // doc.setFontStyle("Arial");
        // doc.text(13, 15, 'Abbreviation:-  WD: Working Day');
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Leave Taken Report'+this.report_time+'.pdf');  
  }
 

  // /*Function to generate Pdf */
  // capturePdf() {

  //   let item1 = {
  //     header: "S/N",
  //     title: "s/N"
  //   }

  //   let item13 = {
  //     header: "EmpId",
  //     title: "empid"
  //   }

  //   let item3 = {
  //     header: "EmployeeName",
  //     title: "employee Name"
  //   }

  //   let item4 = {
  //     header: "LeaveName",
  //     title: "leaveName"
  //   }

  //   let item5 = {
  //     header: "LeaveCode",
  //     title: "leaveCode"
  //   }

  //   let item6 = {
  //     header: "FromDate",
  //     title: "fromDate"
  //   }

  //   let item7 = {
  //     header: "ToDate",
  //     title: "toDate"
  //   }

  //   let item8 = {
  //     header: "NumberOfLeavesTaken",
  //     title: "number Of Leaves"
  //   }

  //   let item9 = {
  //     header: "LeaveType",
  //     title: "leaveType"
  //   }

  //   // let item10 = {
  //   //   header: "Half Day Type",
  //   //   title: "half Day Type"
  //   // }

  //   // let item11 = {
  //   //   header: "Planned/ Unplanned",
  //   //   title: "planned/ Unplanned"
  //   // }

  //   let item12 = {
  //     header: "Reason",
  //     title: "reason"
  //   }

  //   var doc = new jsPDF('l', 'mm', 'a4');
  //   var col = [];

  //   if (this.marked1) {
  //     col.push(item1);
  //   }
  //   if (this.marked13) {
  //     col.push(item13);
  //   }
  //   if (this.marked3) {
  //     col.push(item3);
  //   }
  //   if (this.marked4) {
  //     col.push(item4);
  //   }
  //   if (this.marked5) {
  //     col.push(item5);
  //   }
  //   if (this.marked6) {
  //     col.push(item6);
  //   }
  //   if (this.marked7) {
  //     col.push(item7);
  //   }
  //   if (this.marked8) {
  //     col.push(item8);
  //   }
  //   if (this.marked9) {
  //     col.push(item9);
  //   }
  //   // if (this.marked10) {
  //   //   col.push(item10);
  //   // }
  //   // if (this.marked11) {
  //   //   col.push(item11);
  //   // }
  //   if (this.marked12) {
  //     col.push(item12);
  //   }
   
  //   var rows = [];
  //   var counter = 0;
  //   var rowCountModNew = this.exceldata1;
  //   rowCountModNew.forEach(element => {
  //     counter = counter + 1;
  //     let obj = []
  //     if (this.marked1) {
  //       obj.push(counter);
  //     }
  //     if (this.marked13) {
  //       obj.push(element.empId);
  //     }
     
  //     if (this.marked3) {
  //      if (element.fullName == null && element.lastName == null) {
  //           obj.push('');
  //          }else{
  //              obj.push(element.fullName+ ' '+element.lastName);
  //          }
  //     }
  //     if (this.marked4) {
  //       obj.push(element.leaveName);
  //     }
  //     if (this.marked5) {
  //       obj.push(element.leaveCode);
  //     }
  //     if (this.marked6) {
  //       obj.push(element.startDate);
  //     }
  //     if (this.marked7) {
  //       obj.push(element.endDate);
  //     }
  //     if (this.marked8) {
  //       obj.push(element.leaveCount);
  //     }
  //     if (this.marked9) {
  //       obj.push(element.typeOfLeave);
       
  //     }
  //     // if (this.marked10) {
  //     //   obj.push(element.typeOfLeave);
  //     // }
  //     // if (this.marked11) {
  //     //   obj.push(element.plannedUnplanned);
  //     // }
  //     if (this.marked12) {
  //       obj.push(element.leaveReson);
  //     }
  //     rows.push(obj);
  //   });

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(22, 14, 'Company Name :' + this.compName)

  //   // doc.setTextColor(0, 0, 0)
  //   // doc.setFontSize(7)
  //   // doc.setFontStyle("Arial")
  //   // doc.text(30, 44, 'Abbreviation:-  WD: Working Day , WO : Weekly Off , HO : Holiday , WOH : Week Off And Holiday')

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(120, 20, 'Leave Taken Report')

  //   if(this.fromDate == this.toDate){
  //     doc.setTextColor(40, 80, 139)
  //     doc.setFontSize(9)
  //     doc.setFontStyle("Arial")
  //     doc.text(120, 26, 'For date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") 
  //     )
  //   }
  //   else{
  //     doc.setTextColor(40, 80, 139)
  //     doc.setFontSize(9)
  //     doc.setFontStyle("Arial")
  //     doc.text(110, 26, 'From date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") + ' ' + 'To date' + ' : ' + this.datePipe.transform(this.toDate, "dd-MMM-yyyy")
  //     )
  
  //   }

  //   doc.setTextColor(0, 0, 0)
  //   doc.setFontSize(9)
  //   doc.setFontStyle("Arial")
  //   doc.text(212, 48, 'Report date' + ' : ' + this.datePipe.transform(this.todaysDate, "dd-MMM-yyyy hh:mm:ss a"))

  //   doc.autoTable(col, rows, {
  //     columnStyles: {
  //       1: {halign:'right'},
  //       2: {halign:'left'},
  //       3: {halign:'left'},
  //       4: {halign:'left'},
  //       7: {halign:'right'},
  //       8: {halign:'left'},
  //       9: {halign:'left'},
  //     },
  //     tableLineColor: [189, 195, 199],
  //     tableLineWidth: 0.5,
  //     tableWidth: 'wrap',
  //     headerStyles: {
  //       fillColor: [103, 132, 130],
  //     },
  //     styles: {
  //       halign: 'center',
  //       cellPadding: 0.5, 
  //       fontSize: 8
  //     },
  //     theme: 'grid',
  //     pageBreak:'avoid',
  //     margin: { top: 60,bottom: 50 }
  //   });

  //   // doc.autoTable({html:"#my-table"});

  //   var pageCount = doc.internal.getNumberOfPages();
  //   for (let i = 0; i < pageCount; i++) {
  //     doc.setPage(i);
  //     doc.setTextColor(48, 80, 139)
  //     doc.setFontSize(10)
  //     doc.text(250, 12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
  //   }

  //   doc.save('LeaveTakenReport' + this.datePipe.transform(this.todaysDate, "dd-MM-yyyy hh:mm:ss") + '.pdf');

  // }
}
