import { routing } from '../leave-reports/leave-reports.routing';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { LeaveTakenReportComponent } from './leave-taken-report/leave-taken-report.component';
import { LeaveReportsComponent } from './leave-reports/leave-reports.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LeaveRegisterComponent } from './leave-register/leave-register.component';
import { LeaveTakenInCountReportComponent } from './leave-taken-in-count-report/leave-taken-in-count-report.component';
import { LeaveBalanceReportComponent } from './leave-balance-report/leave-balance-report.component';
import { LeaveCarryforwardReportComponent } from './leave-carryforward-report/leave-carryforward-report.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule
    ],
  providers: [
      DatePipe
    ],
  declarations: [
    LeaveTakenReportComponent, 
    LeaveReportsComponent, LeaveRegisterComponent, LeaveTakenInCountReportComponent, LeaveBalanceReportComponent, LeaveCarryforwardReportComponent]
})
export class LeaveReportsModule { }
