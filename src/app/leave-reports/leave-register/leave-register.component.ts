import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';
@Component({
  selector: 'app-leave-register',
  templateUrl: './leave-register.component.html',
  styleUrls: ['./leave-register.component.css'],
  providers: [ExcelService, DatePipe]
})
export class LeaveRegisterComponent implements OnInit {

  @Input('reportName') reportNm: string;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  
  srNoCheckbox = true
  employeeNameCheckbox = true;
  casualLeaveCheckbox = true;
  privlegeLeaveCheckbox = true;
  sickLeavesCheckbox = true;
  compensatoryOffCheckbox = true;
  totalCheckbox = true;

  casualFlag = true;
  sickFlag = true;
  privFlag = true;
  comFlag = true;
  totalFlag = true;
 
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  user_data = [];
  temp_array=[];
  temp = [];
  leaveHeaders = [];
  leaveSubHeaders = [];
  leaveCode = [];
  leaveSubHeadersModify = [];
  report_time: string;
  table_length: any;
  constructor(private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });
  
  }

  ngOnInit() {
    console.log("**********&&&&",this.reportNm);
    /*Start of Dynamic headers list in table*/
    console.log(this.AllReport);
    let lek = this.AllReport.length;
    console.log(lek);
    for(let i=0;i<this.AllReport.length;i++){
      if(this.AllReport[i].reportEmpNameAndIdResDto != null){
        if(this.reportNm == "LeaveRegisterReport"){
        if(this.AllReport[i].leaveTakenReportInCountResDtoList.length!=0){
          for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtoList.length; j++){
            if(this.AllReport[i].leaveTakenReportInCountResDtoList[j].leaveName != null){
              this.leaveHeaders.push(this.AllReport[i].leaveTakenReportInCountResDtoList[j].leaveName);
            }
          }
        }
        }
        else{
          if(this.AllReport[i].leaveTakenReportInCountResDtos.length!=0){
            for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtos.length; j++){
              if(this.AllReport[i].leaveTakenReportInCountResDtos[j].leaveName != null){
                this.leaveHeaders.push(this.AllReport[i].leaveTakenReportInCountResDtos[j].leaveName);
              }
            }
          }
  
        }
      }
    }
     this.leaveHeaders =this.leaveHeaders.filter( function( item, index, inputArray ) {
      return inputArray.indexOf(item) == index;
    });
    this.leaveHeaders.sort(function(a, b){
      var nameC=a.toLowerCase();
      var nameD=b.toLowerCase();
      if (nameC < nameD) //sort string ascending
          return -1 
      if (nameC > nameD)
          return 1
      return 0 //default return value (no sorting)
   })
   this.table_length = (this.leaveHeaders.length*3) + 4;
    /*End of Dynamic headers list in table*/
  
    /*For Dynamic Subheaders list in table*/
    for(let j = 0; j < this.leaveHeaders.length; j++){
      if(this.leaveHeaders[j] != null){
        this.leaveSubHeaders.push("OB");
        this.leaveSubHeaders.push("U");
        this.leaveSubHeaders.push("CB");
      }
    }
    /*End of Dynamic Subheaders list in table*/
  
    /*Start of Leave code array*/
    for(let i=0;i<this.AllReport.length;i++){
      if(this.AllReport[i].reportEmpNameAndIdResDto != null){
        if(this.reportNm == "LeaveRegisterReport"){
          if(this.AllReport[i].leaveNameReportListResDtoList.length!=0){
            for(let j = 0; j < this.AllReport[i].leaveNameReportListResDtoList.length; j++){
              for( let k = 0; k < this.leaveHeaders.length; k++){
                if(this.AllReport[i].leaveNameReportListResDtoList[j].leaveName == this.leaveHeaders[k]){
                  this.leaveCode.push(this.AllReport[i].leaveNameReportListResDtoList[j].leaveCode);
                }
              }
            }
          }
        }
        else{
          if(this.AllReport[i].leaveTakenReportInCountResDtos.length!=0){
            for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtos.length; j++){
              for( let k = 0; k < this.leaveHeaders.length; k++){
                if(this.AllReport[i].leaveTakenReportInCountResDtos[j].leaveName == this.leaveHeaders[k]){
                  this.leaveCode.push(this.AllReport[i].leaveTakenReportInCountResDtos[j].leaveCode);
                }
              }
            }
          }
        }
      }
    }
    this.leaveCode =this.leaveCode.filter( function( item, index, inputArray ) {
      return inputArray.indexOf(item) == index;
    });
    this.leaveCode.sort(function(a, b){
      var nameC=a.toLowerCase();
      var nameD=b.toLowerCase();
      if (nameC < nameD) //sort string ascending
          return -1 
      if (nameC > nameD)
          return 1
      return 0 //default return value (no sorting)
   })
    /*End of Leave code array*/
  
    /*Start of Dynamic data list in table*/
    for(let i = 0; i < this.AllReport.length; i++){
      let temp1 = [];
      let temp2 = [];
      let totalUsedLeaves = 0;
      let totalCurrentLeaves = 0;
      if(this.AllReport[i].reportEmpNameAndIdResDto != null){
        if(this.reportNm == "LeaveRegisterReport"){
          if(this.AllReport[i].leaveTakenReportInCountResDtoList.length!=0){
            let leavesArr = [];
            for(let k = 0; k < this.AllReport[i].leaveTakenReportInCountResDtoList.length; k++){
              if(this.AllReport[i].leaveTakenReportInCountResDtoList[k].leaveName != null){
                leavesArr.push(this.AllReport[i].leaveTakenReportInCountResDtoList[k].leaveName);
              }
              else{
                leavesArr.push(this.leaveHeaders);
              }
            }
            var filterdata = this.leaveHeaders.filter((unmatchLeaves) => !leavesArr.includes(unmatchLeaves));
            for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtoList.length; j++){
              if(this.AllReport[i].leaveTakenReportInCountResDtoList[j].leaveName != null){
                if(this.AllReport[i].leaveTakenReportInCountResDtoList[j].currentLeaves == null){
                  this.AllReport[i].leaveTakenReportInCountResDtoList[j].currentLeaves = 0;
                }
                if(this.AllReport[i].leaveTakenReportInCountResDtoList[j].usedLeaves == null){
                  this.AllReport[i].leaveTakenReportInCountResDtoList[j].usedLeaves = 0;
                }
                if(this.AllReport[i].leaveTakenReportInCountResDtoList[j].openingBalance == null){
                  this.AllReport[i].leaveTakenReportInCountResDtoList[j].openingBalance = 0;
                }
                if(filterdata.length == 0){
                  temp1.push(this.AllReport[i].leaveTakenReportInCountResDtoList[j]);
                }
                else{
                  if(j == 0){
                      for(let k = 0; k < filterdata.length; k++){
                        temp1.push({
                          currentLeaves: 0,
                          leaveCount: null,
                          leaveName: filterdata[k],
                          usedLeaves: 0,
                          openingBalance:0
                        });
                      }
                    }
                   
                  temp1.push(this.AllReport[i].leaveTakenReportInCountResDtoList[j]);
                 }
               }
            let res = this.AllReport[i].leaveTakenReportInCountResDtoList.map(x => Object.keys(x)[1]);
            // if(j == this.AllReport[i].leaveTakenReportInCountResDtoList.length - 2){
            //   if(res.includes("noOfCoff")){
            //     totalUsedLeaves = totalUsedLeaves; 
            //     totalCurrentLeaves = totalCurrentLeaves; 
            //   }
            //   else{
            //     totalUsedLeaves = totalUsedLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].usedLeaves;
            //     totalCurrentLeaves = totalCurrentLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].currentLeaves;
            //   }
            // }
            // else{
            //   totalUsedLeaves = totalUsedLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].usedLeaves;
            //   totalCurrentLeaves = totalCurrentLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].currentLeaves;
            // }
          
            
            // if(j == this.AllReport[i].leaveTakenReportInCountResDtoList.length - 2){
            //   if(res.includes("noOfCoff")){
            //     temp2.push(this.AllReport[i].leaveTakenReportInCountResDtoList[j]);
            //     temp2.push({
            //       totalUsedLeaves: totalUsedLeaves,
            //       totalCurrentLeaves: totalCurrentLeaves
            //     });
            //   }
            //   else{
            //       temp2.push(
            //         {
            //           noOfCoff: 0,
            //         }
            //       );
            //       temp2.push({
            //         totalUsedLeaves: totalUsedLeaves,
            //         totalCurrentLeaves: totalCurrentLeaves
            //       });
            //   }
            // }
    
            if(j == this.AllReport[i].leaveTakenReportInCountResDtoList.length - 2){
              if(res.includes("noOfCoff")){
                temp2.push(this.AllReport[i].leaveTakenReportInCountResDtoList[j]);
              }
              else{
                  temp2.push(
                    {
                      noOfCoff: 0,
                    }
                  );
              }
            }
    
          }
          }
        }
        else{
          if(this.AllReport[i].leaveTakenReportInCountResDtos.length!=0){
            let leavesArr = [];
            for(let k = 0; k < this.AllReport[i].leaveTakenReportInCountResDtos.length; k++){
              if(this.AllReport[i].leaveTakenReportInCountResDtos[k].leaveName != null){
                leavesArr.push(this.AllReport[i].leaveTakenReportInCountResDtos[k].leaveName);
              }
              else{
                leavesArr.push(this.leaveHeaders);
              }
            }
            var filterdata = this.leaveHeaders.filter((unmatchLeaves) => !leavesArr.includes(unmatchLeaves));
            for(let j = 0; j < this.AllReport[i].leaveTakenReportInCountResDtos.length; j++){
              if(this.AllReport[i].leaveTakenReportInCountResDtos[j].leaveName != null){
                if(this.AllReport[i].leaveTakenReportInCountResDtos[j].currentLeaves == null){
                  this.AllReport[i].leaveTakenReportInCountResDtos[j].currentLeaves = 0;
                }
                if(this.AllReport[i].leaveTakenReportInCountResDtos[j].usedLeaves == null){
                  this.AllReport[i].leaveTakenReportInCountResDtos[j].usedLeaves = 0;
                }
                if(this.AllReport[i].leaveTakenReportInCountResDtos[j].openingBalance == null){
                  this.AllReport[i].leaveTakenReportInCountResDtos[j].openingBalance = 0;
                }
                if(filterdata.length == 0){
                  temp1.push(this.AllReport[i].leaveTakenReportInCountResDtos[j]);
                }
                else{
                  if(j == 0){
                      for(let k = 0; k < filterdata.length; k++){
                        temp1.push({
                          currentLeaves: 0,
                          leaveCount: null,
                          leaveName: filterdata[k],
                          usedLeaves: 0,
                          openingBalance:0
                        });
                      }
                    }
                   
                  temp1.push(this.AllReport[i].leaveTakenReportInCountResDtos[j]);
                 }
               }
            let res = this.AllReport[i].leaveTakenReportInCountResDtos.map(x => Object.keys(x)[1]);
            // if(j == this.AllReport[i].leaveTakenReportInCountResDtoList.length - 2){
            //   if(res.includes("noOfCoff")){
            //     totalUsedLeaves = totalUsedLeaves; 
            //     totalCurrentLeaves = totalCurrentLeaves; 
            //   }
            //   else{
            //     totalUsedLeaves = totalUsedLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].usedLeaves;
            //     totalCurrentLeaves = totalCurrentLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].currentLeaves;
            //   }
            // }
            // else{
            //   totalUsedLeaves = totalUsedLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].usedLeaves;
            //   totalCurrentLeaves = totalCurrentLeaves + this.AllReport[i].leaveTakenReportInCountResDtoList[j].currentLeaves;
            // }
          
            
            // if(j == this.AllReport[i].leaveTakenReportInCountResDtoList.length - 2){
            //   if(res.includes("noOfCoff")){
            //     temp2.push(this.AllReport[i].leaveTakenReportInCountResDtoList[j]);
            //     temp2.push({
            //       totalUsedLeaves: totalUsedLeaves,
            //       totalCurrentLeaves: totalCurrentLeaves
            //     });
            //   }
            //   else{
            //       temp2.push(
            //         {
            //           noOfCoff: 0,
            //         }
            //       );
            //       temp2.push({
            //         totalUsedLeaves: totalUsedLeaves,
            //         totalCurrentLeaves: totalCurrentLeaves
            //       });
            //   }
            // }
    
            if(j == this.AllReport[i].leaveTakenReportInCountResDtos.length - 2){
              if(res.includes("noOfCoff")){
                temp2.push(this.AllReport[i].leaveTakenReportInCountResDtos[j]);
              }
              else{
                  temp2.push(
                    {
                      noOfCoff: 0,
                    }
                  );
              }
            }
    
          }
          } 
        }
        
      temp1.sort(function(a, b){
        var nameA=a.leaveName.toLowerCase();
        var nameB=b.leaveName.toLowerCase();
        if (nameA < nameB) //sort string ascending
            return -1 
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    for(let k = 0; k < temp2.length; k++){
      temp1.push(temp2[k]);
    }
    var fullname = this.AllReport[i].reportEmpNameAndIdResDto.fullName;
    var lastName = this.AllReport[i].reportEmpNameAndIdResDto.lastName;
      
    this.temp.push(
        {
          "empId":this.AllReport[i].reportEmpNameAndIdResDto.empId,
          "fullName":fullname,
          "lastName":lastName,
          leavedata:temp1
        }
      );
    }
  }
    /*End of Dynamic data list in table*/
    this.exceldata = this.temp;
    this.exceldata1 = this.temp;
  }

  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Leave-Register-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#leave-Register',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'}
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(0, 0, 0);
        doc.setFontSize(7);
        doc.setFontStyle("Arial");
        doc.text(13, 15, 'Abbreviation:-  OB : Opening Balance, U : Used, CB : Current Balance');
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Leave-Register-Report'+this.report_time+'.pdf');  
  }

  // exportAsXLSX(): void {
  //   let excelData = [];
  //   let temp2:any = [];
  //   var temp = [];
  //   let leavenames = [];
  //   let leaveSubnames = [];

  //   for(let i = 0; i < this.exceldata1.length; i++){
  //     temp = [];
  //     for(let j = 0; j < this.exceldata1[i].leavedata.length; j++){
  //       if(this.exceldata1[i].leavedata[j].leaveName != null){
  //         temp.push(this.exceldata1[i].leavedata[j].usedLeaves);
  //         temp.push(this.exceldata1[i].leavedata[j].currentLeaves);
  //       }
  //       if(j == this.exceldata1[i].leavedata.length-2){
  //         temp.push(this.exceldata1[i].leavedata[j].noOfCoff);
  //       }
  //       if(j == this.exceldata1[i].leavedata.length-1){
  //         temp.push(this.exceldata1[i].leavedata[j].totalUsedLeaves);
  //         temp.push(this.exceldata1[i].leavedata[j].totalCurrentLeaves);
  //       }
  //     }
  //     temp2.push(
  //       {
  //         "empId":this.AllReport[i].reportEmpNameAndIdResDto.fullName,
  //         "fullName":this.exceldata1[i].fullName+ " " + this.exceldata1[i].lastName,
  //         leavedata:temp
  //       }
  //     );
  //   }
  //   leavenames.push("EmpId");
  //   leavenames.push("Employee");
  //   for(let k = 0; k < this.leaveHeaders.length; k++){
  //     leavenames.push(this.leaveHeaders[k]);
  //   }
  //   leavenames.push("Compensatory Off");
  //   leavenames.push("Total");

  //   leaveSubnames.push("EmpId");
  //   leaveSubnames.push("Employee");
  //   for(let k = 0; k < this.leaveHeaders.length; k++){
  //     leaveSubnames.push(this.leaveHeaders[k] +" "+ "Claim");
  //     leaveSubnames.push(this.leaveHeaders[k] + " "+"OutStanding Balance");
  //   }
  //   leaveSubnames.push("Compensatory Off");
  //   leaveSubnames.push("Total Claim");
  //   leaveSubnames.push("Total OutStanding Balance");

  //   for(let i = 0; i < temp2.length; i++){
  //     let obj: any = {};
  //     let k = 1;
  //     let l = 2;
  //     let key = "";
  //     let len = temp2[i].leavedata.length-3;
  //     obj["Employee"] = temp2[i].fullName;
  //     for(let j = 0; j < temp2[i].leavedata.length; j++){
  //       if(j != len){
  //         if(l%2 == 0){
  //           key = leavenames[k] + " "+ "Claim";
  //           obj[key] = temp2[i].leavedata[j];
  //         }
  //         else{
  //           key = leavenames[k] + " "+ "OutStanding Balance";
  //           obj[key] = temp2[i].leavedata[j];
  //           k++;
  //         }
  //         l++;
  //       }
  //       if(j == len){
  //         key = leavenames[k];
  //         obj[key] = temp2[i].leavedata[j];
  //         k++;
  //       }
  //     }
  //     excelData.push(obj);
      
  //   }
  //   this.excelService.exportAsExcelFileMultiHeader(excelData, 'leaveRegisterReport', leaveSubnames);
  // }

 
  /*Start of Function to generate Pdf */

  // headRows(){
  //   let col = [];
  //   col.push("EmpId");
  //   col.push("Employee");
  //   for(let i = 0; i < this.leaveCode.length; i++){ 
  //     col.push("            "+this.leaveCode[i]);
  //     col.push("   ");
  //   }
  //   col.push("COff");

  //   col.push("            "+"Total");
  //   return col;
  // }

  // bodyRows(){
  //   let rows = [];
  //   for(let i = 0; i < this.temp.length; i++){
  //     let body = [];
  //     let empId = this.temp[i].empId;
  //     body.push(empId);
  //     let employee = this.temp[i].fullName + " " + this.temp[i].lastName;
  //     body.push(employee);
  //     for(let j = 0; j < this.temp[i].leavedata.length; j++){
  //       if(j <  this.temp[i].leavedata.length-2){
  //         body.push(this.temp[i].leavedata[j].usedLeaves);
  //         body.push(this.temp[i].leavedata[j].currentLeaves);
  //       }
  //       if(j == this.temp[i].leavedata.length-2){
  //         body.push(this.temp[i].leavedata[j].noOfCoff);
  //       }
  //       if(j ==  this.temp[i].leavedata.length-1){
  //         body.push(this.temp[i].leavedata[j].totalUsedLeaves);
  //         body.push(this.temp[i].leavedata[j].totalCurrentLeaves);
  //       }
  //      }
  //      rows.push(body);
  //     }
  //   return rows;
  // }
  /*End of Function to generate Pdf */

  /*Start of Function to generate pdf */
  // capturePdf() {
  //   var doc = new jsPDF('l', 'mm', 'a4');
    
   
  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(22, 14, 'Company Name :' + this.compName)

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(120, 20, 'Leave Register Report')

  //   if(this.fromDate == this.toDate){
  //     doc.setTextColor(40, 80, 139)
  //     doc.setFontSize(9)
  //     doc.setFontStyle("Arial")
  //     doc.text(120, 26, 'For date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") 
  //     )
  //   }
  //   else{
  //     doc.setTextColor(40, 80, 139)
  //     doc.setFontSize(9)
  //     doc.setFontStyle("Arial")
  //     doc.text(110, 26, 'From date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") + ' ' + 'To date' + ' : ' + this.datePipe.transform(this.toDate, "dd-MMM-yyyy")
  //     )
  
  //   }

  //   doc.setTextColor(0, 0, 0)
  //   doc.setFontSize(9)
  //   doc.setFontStyle("Arial")
  //   doc.text(212, 48, 'Report date' + ' : ' + this.datePipe.transform(this.todaysDate, "dd-MMM-yyyy hh:mm:ss a"))
  //   var subheaders = [];
  //   subheaders.push("");
  //   subheaders.push("");
  //   for(let k = 0; k < this.leaveSubHeaders.length; k++){
  //     this.leaveSubHeadersModify.push(this.leaveSubHeaders[k]);
  //   }
  //   for(let k = 0; k < this.leaveSubHeadersModify.length; k++){
  //     if(this.leaveSubHeadersModify[k] == "OutStanding Balance"){
  //       this.leaveSubHeadersModify[k] = "OB";
  //     }
  //     subheaders.push(this.leaveSubHeadersModify[k]);
  //   }
  //   subheaders.push("");
  //   subheaders.push("Claim");
  //   subheaders.push("OB");
  //   doc.autoTable({
  //     head:[this.headRows(),subheaders], 
  //     body:this.bodyRows(),
  //     tableLineColor:[189, 195, 199],
  //     tableLineWidth: 0.5,
  //     tableWidth: 'wrap',
  //     headerStyles: {
  //       fillColor: [103, 132, 130],
  //     },
  //     styles: {
  //       halign: 'center',
  //       // styles: { fillColor: "#010808" },
  //       // overflowColumns: 'linebreak',
  //       // minCellWidth:1
  //       cellPadding: 0.9, 
  //       fontSize: 9
  //     },
  //     columnStyles: {
  //       colspan:1, 
  //     },
  //     theme: 'grid',
  //     pageBreak:'avoid',
  //     margin: { top: 60,bottom: 50 }
  //   });

    
  //   var pageCount = doc.internal.getNumberOfPages();
  //   for (let i = 0; i < pageCount; i++) {
  //     doc.setPage(i);
  //     doc.setTextColor(48, 80, 139)
  //     doc.setFontSize(10)
  //     doc.text(250, 12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
  //   }
  //   doc.save('LeaveRegisterReport' + this.datePipe.transform(this.todaysDate, "dd-MM-yyyy hh:mm:ss") + '.pdf');
  // }
  /*End of Function to generate pdf */
}
