import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveCarryforwardReportComponent } from './leave-carryforward-report.component';

describe('LeaveCarryforwardReportComponent', () => {
  let component: LeaveCarryforwardReportComponent;
  let fixture: ComponentFixture<LeaveCarryforwardReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveCarryforwardReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveCarryforwardReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
