import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
import { Workbook } from 'exceljs';
@Injectable({
  providedIn: 'root'
})
export class ExcelService {
  title: string;

  constructor( private datePipe :  DatePipe) { }

  public exportAsExcelFileMultiHeader(json: any[], excelFileName: string, headersArr: any[]): void {
    var title = "";
    
    if(excelFileName == "leaveTakenInCountReport"){
      title = 'Leave Taken In Count Report';
    }
    else{
      title = 'Leave Register Report';
    }
    // const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json,{skipHeader: true});
    // const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    var worksheet = XLSX.utils.json_to_sheet(json, {header:headersArr, skipHeader: false});
   // const worksheet: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(json);
    // const merge = [
    //   { s: { r: 0, c: 1 }, e: { r: 0, c: 2 } },{ s: { r: 0, c: 3 }, e: { r: 0, c: 4 } },
    // ];
    // worksheet["!merges"] = merge;
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);

    let titleRow = worksheet.addRow(title);
    titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, underline: 'double', bold: true }
    worksheet.addRow([titleRow]);
    // let subTitleRow = worksheet.addRow(['Date : ' + this.datePipe.transform(new Date(), 'medium')])
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    //var reportName = sessionStorage.getItem('reportName');
    if(excelFileName == 'leaveTakenReport'){
      this.title = 'Leave Taken Report';
    } 
    if(excelFileName == 'leaveTakenInCountReport'){
      this.title = 'Leave Taken In Count Report';
    }
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);

    let titleRow = worksheet.addRow(this.title);
    titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, underline: 'double', bold: true }
    worksheet.addRow([titleRow]);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + this.datePipe.transform(new Date(),"ddMMyyyy hh:mm") + EXCEL_EXTENSION);
  }
}
