import { Routes, RouterModule } from '@angular/router'

import { LeaveReportsComponent } from './leave-reports/leave-reports.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: LeaveReportsComponent ,

            children:[
                {
                    path:'',
                    redirectTo : 'leaveReports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaveReports',
                    component: LeaveReportsComponent
                }

            ]

    }
]

export const routing = RouterModule.forChild(appRoutes);