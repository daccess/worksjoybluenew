import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-leave-reports',
  templateUrl: './leave-reports.component.html',
  styleUrls: ['./leave-reports.component.css']
})
export class LeaveReportsComponent implements OnInit {
  reportName = sessionStorage.getItem('reportName');
  
  constructor() {
  }

  ngOnInit() {
  }

}
