import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave-balance-report',
  templateUrl: './leave-balance-report.component.html',
  styleUrls: ['./leave-balance-report.component.css'],
  providers: [ExcelService, DatePipe]
})
export class LeaveBalanceReportComponent implements OnInit {

  marked_empId = true;
  marked_empName = true;
  marked_designation = true;
  marked_department = true;
  marked_leaveCode = true;
  marked_balance = true;

  srNoCheckbox = true;
  employeeNameCheckbox = true;
  leaveCodeCheckbox = true;
  designationCheckbox = true;
  departmentCheckbox = true;
  balanceCheckbox = true;
  empIdCheckbox = true;
 
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  reportData =[];
  report_time: any;
  table_length: any;
  
  constructor(private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });
    var k = 0;
    for(let i = 0; i < this.AllReport.length;i++){
      for(let j = 0; j < this.AllReport[i].length; j++){
         this.reportData[k] = this.AllReport[i][j];
         k++;   
      }
   }

  
    this.exceldata = this.reportData;
    this.exceldata1=this.reportData;
    this.table_length = 10;
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
  }

  ngOnInit() {
  }

  toggleVisibility_EmpId(e) {
    this.marked_empId = e.target.checked;
  }
  
  toggleVisibility_EmpName(e) {
    this.marked_empName = e.target.checked;
  }

  toggleVisibility_Designation(e) {
    this.marked_designation = e.target.checked;
  }

  toggleVisibility_Department(e) {
    this.marked_department = e.target.checked;
  }

  toggleVisibility_Balance(e) {
    this.marked_balance = e.target.checked;
  }

  toggleVisibility_LeaveCode(e){
    this.marked_balance = e.target.checked;
  }

  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Leave Balance Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#my-table',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'center'},
          6: {halign:'center'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Leave Balance Report'+this.report_time+'.pdf');  
  }
 
}
