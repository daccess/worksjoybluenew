import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { postextraworkData } from '../extraworkreq/models/postextrawork';
import { postextraworkservice } from '../extraworkreq/services/postExtraworkservice';
import { ToastrService } from 'ngx-toastr';
import { getlistData } from './models/getlistModel';
import { getlistextraWorkservice } from './services/getlistservice';
import { ChangeDetectorRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CalendarService } from '../shared/services/calendar.service';
import { CoffReq } from '../otherrequests/shared/models/coffReqModel'


@Component({
  selector: 'app-extraworkreq',
  templateUrl: './extraworkreq.component.html',
  styleUrls: ['./extraworkreq.component.css'],
  providers:[postextraworkservice,getlistextraWorkservice]
})
export class ExtraworkreqComponent implements OnInit {
  isShow: boolean = true;
  EmployeedataLeave: any = [];
  baseurl = MainURL.HostUrl;
  loggedUser: any;
  compId: any;
  allEmpdata: any;
  empId: any;
  selectedItems: any;
  dropdownSettings: {};
  selectedEmp = [];
  Shiftmastergroupdata: any;
  EmpData: any;
  shiftData: any;
  selectedshiftobj: any;
  selectedgetshiftobj: any;
  selectedempobj: any;
  today: any;
  getDataForEdit: any;
  postExtraworkData: postextraworkData;
  hrRequestToWorkId: any;
  AlldataUpdate: any;
  isEdit = false;
  empOfficialId: any;
  daysInThisMonth: any = [];
  daysInLastMonth: any = [];
  daysInNextMonth: any = [];
  from: any;
  Selectund: any
  to: any;
  leaveDays = [];
  searchText: string = "";
  model: any = { frmdate: "", todate: "", leavereson: "", handedover: [], empnotified: [], type: "", typeofleave: "Fullday" };
  HolidayData = [];
  locationFlag = false;
  public selectUndefinedOptionValue: any;
  getModel:getlistData;
  alldata: any;
  selecteddateData: any;
  dltmodelFlag: boolean;
  dltObje: any;
  attendanceData: any = [];
  calAttendance: any = [];
  maxDateForApply: any;
  slectecdoption: any;
  todaysDate: any;
  FullDaydateData: any;
  getfullDayDate: any;
  HalfDaydateData: any;
  getHlafDayDate: any;
  monthNames: any;
  coffModel: CoffReq;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  absentDay: any = "17"
  currentMonth1: any;
  selectedempPerId: any;
  hideCalender: boolean = false;
  leaveConfigList: any = [];
  getEmpOfficialId: any;
  personalData: any;
  EmployeeApproverdata: any = [];
  firstLevel: any;
  secondLevel: any;
  thirdLevel: any;
  roleofUser:any;
  date: any = new Date();
   themeColor : string = "nav-pills-blue";
  constructor(public httpService: HttpClient, 
    public extraworkservice : postextraworkservice,
    private toastr: ToastrService,
    private chRef: ChangeDetectorRef,
    public getlistservice: getlistextraWorkservice,
    public Spinner: NgxSpinnerService,
    private calendarService : CalendarService) { 
    this.selecteddateData = 'All';
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.empId = this.loggedUser.empId
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.roleofUser = this.loggedUser.mrollMaster.roleName;
    this.today=new Date();
    this.getModel = new getlistData();
    this.postExtraworkData = new postextraworkData();
    this.postExtraworkData.workingDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.getlist();
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
  }

  ngOnInit() {
    this.AllEmpCategory();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.selectShift();
    this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    let obj: any = new Object();
    obj.filePath = this.model.filePath;
    this.Holidays();
    this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.maxDateForApply = new Date().getFullYear() + "-" + 12 + "-" + 31;
    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    this.selectedItems = []
    this.dropdownSettings = {
        singleSelection: false,
        idField: 'empPerId',
        textField: 'fullName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 100,
        allowSearchFilter: true
    };
  }
  selectEmp(e){
    this.selectedempPerId = e;
   for (let i = 0; i < this.EmpData.length; i++) {
       if(this.EmpData[i].empPerId == this.selectedempPerId){
          this.getEmpOfficialId = this.EmpData[i].empOfficialId;
        this.getAttendanceOfMonth(new Date().getMonth() + 1, new Date().getFullYear());
       }
    }
    this.hideCalender = true;
  }
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    }else {
      this.currentDate = 999;
    }
    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    let obj;
    for (let i = 0; i < thisNumOfDays; i++) {
      let item;
      let flag = false;
      for (let j = 0; j < this.attendanceData.length; j++) {
        if (!this.attendanceData[j].checkIn) {
          this.attendanceData[j].checkIn = new Date(this.attendanceData[j].checkIn).setHours(0, 0, 0);
        }
        if (!this.attendanceData[j].checkOut) {
          this.attendanceData[j].checkOut = new Date(this.attendanceData[j].checkOut).setHours(0, 0, 0);
        }
        if (new Date(this.attendanceData[j].attenDate).getDate() == i + 1) {
          flag = true;
          if (this.attendanceData[j].statusOfDay == 'WD') {
            if (this.attendanceData[j].attendanceStatus == 'P' || this.attendanceData[j].attendanceStatus == 'VP') {
              if (this.attendanceData[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: this.attendanceData[j].leaveStatus,
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: this.attendanceData[j].coffStatus + " Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "On Duty"
                }
              }
              else if (this.attendanceData[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Work From Home"
                }
              }

              else {
                let checkIn = "";
                let checkOut = "";
                if (this.attendanceData[j].checkIn) {
                  checkIn = new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes();
                }
                if (this.attendanceData[j].checkOut) {
                  checkOut = new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration);
                }
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: 'In ' + checkIn,
                  checkOut: ' Out ' + checkOut
                }
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF' && !this.attendanceData[j].wfhStatus) {
              item = {
                day: i + 1,
                date: this.attendanceData[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF' && this.attendanceData[j].wfhStatus) {
              item = {
                day: i + 1,
                status: "WFH",
                date: this.attendanceData[j].attenDate,
                checkIn: "Work From Home"
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.attendanceData[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Leave",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Work From Home"
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.attendanceData[j].attenDate
                }
              }

            }


          }
          else if (!this.attendanceData[j].statusOfDay) {

            if (this.attendanceData[j].attendanceStatus == 'P') {

              if (this.attendanceData[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "On Duty"
                }
              }



              else {
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF') {
              item = {
                day: i + 1,
                date: this.attendanceData[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.attendanceData[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Leave",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.attendanceData[j].attenDate
                }
              }

            }


          }
          else {
            if (this.attendanceData[j].statusOfDay == 'HO') {
              let holiDayName;
              for (let m = 0; m < this.HolidayData.length; m++) {

                if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                  holiDayName = this.HolidayData[m].holiDayName;
                }

              }
              item = {
                day: i + 1,
                status: "HO",
                checkIn: holiDayName
              }
            } else {
              item = {
                day: i + 1,
                status: "WO",
                checkIn: "Week Off"
              }
            }

          }

          this.daysInThisMonth.push(item);
        }
      }
      if (!flag) {

        let holiDayName;
        let holidayFlag: boolean = false;
        if (this.HolidayData) {
          for (let m = 0; m < this.HolidayData.length; m++) {
            holidayFlag = false;
            if (new Date(new Date(this.HolidayData[m].startDate).getFullYear(), new Date(this.HolidayData[m].startDate).getMonth(), new Date(this.HolidayData[m].startDate).getDate(), 0, 0, 0, 0).getTime() <= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime() && new Date(new Date(this.HolidayData[m].endtDate).getFullYear(), new Date(this.HolidayData[m].endtDate).getMonth(), new Date(this.HolidayData[m].endtDate).getDate(), 0, 0, 0, 0).getTime() >= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime()) {
              holiDayName = this.HolidayData[m].holiDayName;
              holidayFlag = true;
              break;
            }
          }
        }

        if (holidayFlag) {
          item = {
            day: i + 1,
            status: "HO",
            checkIn: holiDayName
          }
        }
        else {
          item = {
            day: i + 1,
            status: "Ot"
          }
        }
        this.daysInThisMonth.push(item);

      }

    }
    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    let nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
    for (let i = 0; i < (6 - lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i + 1);
    }
  }
  getAttendanceOfMonth(month, year) {
    let obj = {
      "empOfficialId": this.getEmpOfficialId,
      "month": month,
      "year": year
    }
    this.getlistservice.monthlyAttendance(obj).subscribe(data => {
      this.attendanceData = data.result;
      this.getDaysOfMonth();
    }, (err => {
      this.getDaysOfMonth();
    })
    )
  }
  
  getDataForExtraWork(hrRequestToWorkId){
  this.isEdit = true;
    let urledit = this.baseurl + '/hrRequestToWorkById/';
    this.httpService.get(urledit + hrRequestToWorkId).subscribe(data => {
        this.AlldataUpdate = data['result'];
        this.postExtraworkData.workingDate = this.AlldataUpdate.workingDate;
        this.postExtraworkData.reason = this.AlldataUpdate.reason;
        this.selectedshiftobj =  this.AlldataUpdate.shiftMasterId;
        this.postExtraworkData.shiftMasterId = this.selectedshiftobj;
        this.selectedempobj = this.AlldataUpdate.empPerId;
        this.postExtraworkData.empPerId = this.selectedempobj;
      },
      (err: HttpErrorResponse) => {
      });
  }
  AllEmpCategory() {
    let busiurl = this.baseurl + '/employeelistunderempid/';
    if(this.roleofUser == "HR"){
        this.httpService.get(busiurl + this.empId).subscribe(data => {
                this.allEmpdata = data["result"];
                this.EmpData = this.allEmpdata;
                for (let index = 0; index < this.EmpData .length; index++) {
                  this.EmpData[index].fullName = this.EmpData[index].empId+"-"+this.EmpData[index].fullName;
                  
            }
        },
        (err: HttpErrorResponse) => {
        });
      }
      if(this.loggedUser.sanctioningAuthority == true && this.roleofUser != "HR"){
        let url = this.baseurl + '/fistLevelEmployeeByEmpId';
        let model =  {
          empId : this.loggedUser.empId
        } 
        this.getlistservice.getEmployesUnderEmpId(model).subscribe(data=>{
          if(data != []){
            this.EmpData = data.result.employeePerInfoList;
            for (let index = 0; index < this.EmpData .length; index++) {
              this.EmpData[index].fullName = this.EmpData[index].empId+"-"+this.EmpData[index].fullName+" "+this.EmpData[index].lastName; 
            }
          }
          else{
            this.EmpData = [];
          }  
        },err=>{
        })
      }
}

Holidays() {
  let url = this.baseurl + '/Holiday?empOfficialId='+this.loggedUser.empOfficialId+'&year='+new Date().getFullYear();
  this.httpService.get(url + this.loggedUser.empOfficialId).subscribe(data => {
      this.HolidayData = data["result"];
    },
    (err: HttpErrorResponse) => {
     
    });
}

empDatadata(event){
  this.selectedempobj = parseInt(event.target.value);
  this.postExtraworkData.empPerId = this.selectedempobj;
}
shiftdata(event){
  this.selectedshiftobj = parseInt(event.target.value);
}

selectShift(){
 
  let url = this.baseurl + '/ShiftMaster/ByActive/';
  this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
      this.Shiftmastergroupdata = data;
      this.shiftData = this.Shiftmastergroupdata.result;
      //console.log("shiftdata", this.shiftData);
    },(err: HttpErrorResponse) => {
    });
}

onItemSelect(item: any) {

 const id = item.empPerId;
  if (this.selectedEmp.length == 0) {
    this.selectedEmp.push(item)
     this.selectEmp(id);
  }else{
    for (var i = 0; i < this.selectedEmp.length; i++) {
      if (this.selectedEmp[i].empOfficialId == item.empOfficialId) {

      } else {
          this.selectedEmp.push(item)
      }
    }
    this.hideCalender=false;
  }
}

onSelectAll(items: any) {
  this.selectedEmp = items;
  this.hideCalender=false;
}
OnItemDeSelect(item: any) {
  for (var i = 0; i < this.selectedEmp.length; i++) {
    if(this.selectedEmp.length == 0){
        this.hideCalender=true;
      if (this.selectedEmp[i].empOfficialId == item.empOfficialId) {
          this.selectedEmp.splice(i, 1);
      }
    }else{
        this.hideCalender=false;
        if (this.selectedEmp[i].empOfficialId == item.empOfficialId) {
          this.selectedEmp.splice(i, 1);
      }

    }
  }
}

onDeSelectAll(item: any) {
  this.selectedEmp = [];
}

workingDate(event){
  this.postExtraworkData.workingDate = event;
}

onSubmit(f){
this.Spinner.show();
let shiftName = "";
for (let i = 0; i < this.shiftData.length; i++) {
  if(this.shiftData[i].shiftMasterId == this.selectedshiftobj){
      shiftName = this.shiftData[i].shiftName;
  }
}
this.postExtraworkData.compId = this.loggedUser.compId;
  if(this.isEdit == false){
    this.postExtraworkData.shiftMasterId = this.selectedshiftobj;
    this.postExtraworkData.shiftName = shiftName;
    this.postExtraworkData.empId = this.empId;
    this.postExtraworkData.empOfficialId = this.empOfficialId;
    this.extraworkservice.Postextraworkmethod(this.postExtraworkData).subscribe(data => {
      this.Spinner.hide();
      if(data.statusCode == "201"){
        this.toastr.success('Extra Work Request Sent Successfully!', 'Extra-Working Request');
        f.reset();
      this.postExtraworkData = new postextraworkData();
      this.resetForm();
      this.selectedshiftobj = null;
      }else{
        this.toastr.success(data.message, 'Extra Work Request');
      }
    },(err) => {
      this.Spinner.hide();
      this.toastr.error(err.json().result.message);
    });
  }
  else{
    let url1 = this.baseurl + '/updatehrRequestToWork';
    this.postExtraworkData.shiftMasterId = this.selectedshiftobj;
    this.postExtraworkData.empId = this.empId;
    this.postExtraworkData.empOfficialId = this.empOfficialId;
    this.postExtraworkData.hrRequestToWorkId = this.hrRequestToWorkId;
    this.postExtraworkData.shiftName = shiftName;
    this.httpService.put(url1,this.postExtraworkData).subscribe((data : any) => {
    this.Spinner.hide();
    f.reset();
    this.postExtraworkData = new postextraworkData();
    this.resetForm();
    if(data.message == "Update Sucess"){
      this.toastr.success('Extra Working Request Updated Successfully!', 'Extra Working Request');
    }
    else{
      this.toastr.success(data.message, 'Extra Working Request');
    }
     this.selectedshiftobj = null;
     this.isEdit = false; 
     sessionStorage.removeItem("editdata");
    },(err) => {
      this.Spinner.hide();
      this.toastr.error(err.json().message, 'Extra-Work');
    });
  }
}

selectClickCategory() {
  if(this.postExtraworkData.hrRequestToWorkReqDtoList.length == 0){
    this.locationFlag = true;
  }
  else{
    this.locationFlag = false;
  }
} 

resetForm(){
  this.getlist();
  setTimeout(() => {
    this.postExtraworkData.workingDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.selectedshiftobj = this.selectUndefinedOptionValue;
  }, 100);
}

templistData = [];
workedDatesArray = [];

dataTableFlag : boolean = true;
getlist(){
  this.getModel.workingDate = this.today;
  this.getModel.empId = this.empId;
  this.dataTableFlag = false;
  this.getlistservice.getextraworkmethod(this.getModel).subscribe(data => {
    this.alldata = new Array();
    this.alldata = data.result;
    this.dataTableFlag = true;
    setTimeout(function () {
      $(function () {
        var table = $('#BusinessTable').DataTable();  
      });
    }, 1000);
    this.templistData = data.result;
    let temp = new Array();
    temp = data.result;
    this.workedDatesArray = new Array();
    this.workedDatesArray = temp;
    for (let i = 0; i < this.workedDatesArray.length; i++) {
      for (let j = 0; j < this.workedDatesArray.length; j++) {
        if(new Date(this.workedDatesArray[i].workingDate).getTime() == new Date(this.workedDatesArray[j].workingDate).getTime()){
          this.workedDatesArray.splice(i,0);
        }
      }
    }
  },
  (err: HttpErrorResponse) => {
  });
}

getdatedata(event){
  this.dataTableFlag = false;
  if(event == "All"){
    this.getlist();
  }
  else{
  let obj = new Array();
  this.alldata = new Array();
  for (let i = 0; i < this.templistData.length; i++) {
    if(new Date(this.templistData[i].workingDate).getTime() == new Date(event).getTime()){
      obj.push(this.templistData[i]); 
    }
  }
  this.chRef.detectChanges();
  this.alldata = obj;
  this.dataTableFlag = true;
  setTimeout(function () { 
    $(function (){
      var table = $('#BusinessTable').DataTable();  
    });
  },1000);
 }
}
dltmodelEvent(){
  this.dltmodelFlag=true;
  this.dltObjfun(this.dltObje);
  this.Spinner.show()
}
dltObj(data){
  this.dltmodelFlag=false;
  this.dltObje =data.hrRequestToWorkId;
  this.dltObjfun(this.dltObje);
}

dltObjfun(object){
  if(this.dltmodelFlag==true){
    if(this.dltmodelFlag==true){
      let url1 = this.baseurl + '/deleteHrRequestToWork/';
      this.postExtraworkData.hrRequestToWorkId = this.dltObje;
      this.httpService.delete(url1 + this.dltObje).subscribe(data => {
       this.Spinner.hide();
       this.toastr.success("Deleted SuccessFully ..!");
        this.getlist();
      },(err: HttpErrorResponse) => {
        this.Spinner.hide();
      });
    }
  }
}

edit(data){
  this.hrRequestToWorkId = data.hrRequestToWorkId;
  this.getDataForExtraWork(this.hrRequestToWorkId);
  document.getElementById('extra-apply-tab').click();
}
timeConvert(n) {
  var num = n;
  var hours = (num / 60);
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return rhours + ":" + rminutes;
}
goToLastMonth() {
  this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
  this.attendanceData = []
   this.getDaysOfMonth();
  this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
}
goToNextMonth() {
  this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
  this.getDaysOfMonth();
  this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
}

show() {
  this.model.frmdate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
}
selectDate(day) {
  this.currentDate = day;
  if (this.from == 'from') {
    this.model.frmdate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
  } else if (this.from == 'to') {
    this.model.todate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
  }
}
}