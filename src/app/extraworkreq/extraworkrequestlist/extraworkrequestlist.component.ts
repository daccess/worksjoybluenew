import { Component, OnInit } from '@angular/core';
import { MainURL } from '../../shared/configurl';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { getlistData } from '../models/getlistModel';
import { getlistextraWorkservice } from '../services/getlistservice';
  import { from } from 'rxjs';
  import { Router } from "@angular/router";
  import { postextraworkData } from '../models/postextrawork';

@Component({
  selector: 'app-extraworkrequestlist',
  templateUrl: './extraworkrequestlist.component.html',
  styleUrls: ['./extraworkrequestlist.component.css'],
  providers:[getlistextraWorkservice]
})
export class ExtraworkrequestlistComponent implements OnInit {
  today: any;
  tosortDate: any;
  baseurl = MainURL.HostUrl;

  loggedUser: any;
  empId: any;

  getModel:getlistData;
  alldata: any;

  dltmodelFlag: boolean;
  dltObje: any;
  selecteddateData: any;

  postExtraworkData: postextraworkData;

  constructor(public httpService: HttpClient,public getlistservice: getlistextraWorkservice, public router: Router) {
    this.postExtraworkData = new postextraworkData();
    this.getModel = new getlistData();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.empId = this.loggedUser.empId;
    this.today=new Date();
    this.getModel.workingDate = this.today;
    this.getlist();
  }
  ngOnInit() {
    this.selecteddateData = 'All';
  }
  getdatedata(event){
    if(event == "All"){
    this.getlist();
    }else{
    let obj = new Array();
    for (let i = 0; i < this.templistData.length; i++) {
      if(new Date(this.templistData[i].workingDate).getTime() == new Date(event).getTime()){
        obj.push(this.templistData[i]);  
      }
    }
    this.alldata = obj;
  }
}

  templistData = [];
  workedDatesArray = [];
  getlist(){
    this.getModel.empId = this.empId;
    this.getlistservice.getextraworkmethod(this.getModel).subscribe(data => {
      this.alldata = new Array();
      this.alldata = data.result;
      this.templistData = data.result;
      let temp = new Array();
      temp = data.result;
      this.workedDatesArray = new Array();
      this.workedDatesArray = temp;
      for (let i = 0; i < this.workedDatesArray.length; i++) {
        for (let j = 0; j < this.workedDatesArray.length; j++) {
          if(new Date(this.workedDatesArray[i].workingDate).getTime() == new Date(this.workedDatesArray[j].workingDate).getTime()){
            this.workedDatesArray.splice(i,0);
          }
        }
      }
    },
    (err: HttpErrorResponse) => {

    });
  }
  edit(data){
    sessionStorage.setItem('editdata', JSON.stringify(data));
    this.router.navigateByUrl('/layout/extraworkreq');
  }
  dltmodelEvent(){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
  }
  dltObj(data){
    this.dltmodelFlag=false;
    this.dltObje =data.hrRequestToWorkId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deleteHrRequestToWork/';
        this.postExtraworkData.hrRequestToWorkId = this.dltObje;
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.getlist();
        },(err: HttpErrorResponse) => {
         
        });
      }
    }

  }
  
}