import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraworkrequestlistComponent } from './extraworkrequestlist.component';

describe('ExtraworkrequestlistComponent', () => {
  let component: ExtraworkrequestlistComponent;
  let fixture: ComponentFixture<ExtraworkrequestlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtraworkrequestlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraworkrequestlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
