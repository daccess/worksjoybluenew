import { routing } from './extraworkreq.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ExtraworkreqComponent } from 'src/app/extraworkreq/extraworkreq.component';
import { ExtraworkrequestlistComponent } from './extraworkrequestlist/extraworkrequestlist.component';
import { postextraworkservice } from '../extraworkreq/services/postExtraworkservice';
import { getlistextraWorkservice } from './services/getlistservice'
    import { from } from 'rxjs';

@NgModule({
    declarations: [
        ExtraworkreqComponent,
        ExtraworkrequestlistComponent

    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [postextraworkservice, getlistextraWorkservice]
  })
  export class ExtraworkreqModule { 
      constructor(){

      }
      
  }
  