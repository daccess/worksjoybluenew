import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ExtraworkreqComponent } from 'src/app/extraworkreq/extraworkreq.component';
import { ExtraworkrequestlistComponent } from 'src/app/extraworkreq/extraworkrequestlist/extraworkrequestlist.component';
    import { from } from 'rxjs';


const appRoutes: Routes = [
    { 
             
        path: '', component: ExtraworkreqComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'extraworkreq',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'extraworkreq',
                    component: ExtraworkreqComponent
                }

            ]

    },
    {
        path : 'extraworkrequestlist',
        component : ExtraworkrequestlistComponent,
        
    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);