import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraworkreqComponent } from './extraworkreq.component';

describe('ExtraworkreqComponent', () => {
  let component: ExtraworkreqComponent;
  let fixture: ComponentFixture<ExtraworkreqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtraworkreqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraworkreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
