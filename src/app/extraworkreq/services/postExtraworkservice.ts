import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { from } from 'rxjs';
import { MainURL } from 'src/app/shared/configurl';
import { postextraworkData } from '../models/postextrawork';

@Injectable()
export class postextraworkservice {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  Postextraworkmethod(extrawork : postextraworkData){
    let url = this.baseurl + '/hrRequestToWork';
    var body = JSON.stringify(extrawork);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}