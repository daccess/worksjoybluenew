import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { from } from 'rxjs';
import { MainURL } from 'src/app/shared/configurl';
import { getlistData } from '../models/getlistModel';

@Injectable()
export class getlistextraWorkservice {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  getextraworkmethod(getdata : getlistData){
    let url = this.baseurl + '/employeelistUnderStatus';
    var body = JSON.stringify(getdata);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getLeaveConfigurationList(empPerId){
    let url = this.baseurl + '/getLeaveData/'+empPerId;
    // var body = JSON.stringify(model);
    return this.http.get(url);
  }
  monthlyAttendance(empOfficialId){
    let url = this.baseurl + '/callender';
    var body = JSON.stringify(empOfficialId);
    console.log("body",body);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getEmployesUnderEmpId(model){
    let url = this.baseurl + '/fistLevelEmployeeByEmpId';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}