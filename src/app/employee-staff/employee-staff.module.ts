import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeStaffComponent } from './employee-staff.component';
import { staffRouting1 } from './employee-staff-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    staffRouting1,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
  EmployeeStaffComponent
  ]
})
export class EmployeeStaffModule { }
