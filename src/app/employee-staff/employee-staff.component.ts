import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-employee-staff',
  templateUrl: './employee-staff.component.html',
  styleUrls: ['./employee-staff.component.css']
})
export class EmployeeStaffComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  employeelistEdit: string;
  employeeIds: any;
  user: string;
  users: any;
  token: any;
  employeeData: any;
  empmasterIds: any;
  searchByLabour:any;
  selectUndefinedOptionValue:any;
  searchByColumn:any;
  searchLabour:any;

  constructor(private router:Router,private httpservice:HttpClient) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token
    this.getLabourData();
  }

  getLabourData() {
    let url = this.baseurl + '/getEmployeeMaster';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.employeeData = data['result']
      setTimeout(function () {

        $(function () {

          var table = $('#emptable').DataTable({

            retrieve: true,
            searching:false

         

          })

          // table
          // .order( [ 1, 'des' ] )
          // .draw();
          // $('.status-dropdown').on('change', function (e) {
          //   var status = $(this).val();
          //   $('.status-dropdown').val(status)
          //   console.log(status)
          //   table.column(6).search(status as AssignType).draw();
          // })


        });
      }, 100);
    
      // this.labourData=JSON.stringify(this.LabourData )
      // console.log("dsfasdfdsfsdfsdfsdafasdfsdaf",this.LabourData)
    },
      (err: HttpErrorResponse) => {

      })

  }
  
  editLabour(e:any){
    this.employeelistEdit='true'
   this.empmasterIds= e.empMasterId
   
    sessionStorage.setItem("empmasterId",this.empmasterIds)
    sessionStorage.setItem("iseditLabour",'true')
    this.router.navigateByUrl('/layout/addempStaff');


  }



  filterData() {
    if (this.searchByLabour && this.searchByColumn) {
      // Convert the search term to lowercase for case-insensitive search
      const searchLower = this.searchByColumn.toLowerCase();
  
      this.employeeData = this.employeeData.filter((item) => {
        if (this.searchByLabour === 'Name' && item.firstName && item.lastName) {
          const fullName = `${item.firstName} ${item.lastName}`;
          if (fullName.toLowerCase().includes(searchLower)) {
            return true;
          }
        } else if (this.searchByLabour === 'Contact Number' && item.mobileNo && item.mobileNo.includes(this.searchByColumn)) {
          return true;
        } else if (this.searchByLabour === 'Adhar Number' && item.bioId && item.bioId.toLowerCase() === searchLower) {
          return true;
        } else if (this.searchByLabour === 'Contractor Service' && item.empCategoryMaster && item.empCategoryMaster.empCatName.toLowerCase().includes(searchLower)) {
          return true;
        } else if (this.searchByLabour === 'Contractor Name' && item.companyMaster && item.companyMaster.compName.toLowerCase().includes(searchLower)) {
          return true;
        }
        return false;
      });
    } else {
      this.getLabourData();
    }
  }
  
  
}
