import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeStaffComponent } from './employee-staff.component';

describe('EmployeeStaffComponent', () => {
  let component: EmployeeStaffComponent;
  let fixture: ComponentFixture<EmployeeStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
