import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeStaffComponent } from './employee-staff.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: EmployeeStaffComponent,
  
            children:[
                {
                    path:'',
                    redirectTo : 'emloyeestaffList',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'emloyeestaffList',
                    component: EmployeeStaffComponent
                }
  
            ]
  
    }
  
  
  ]
  
  export const staffRouting1 = RouterModule.forChild(appRoutes);