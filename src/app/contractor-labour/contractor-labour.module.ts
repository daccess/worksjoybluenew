import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { contractorRouting } from './contractor-labour-routing.module';
import { ContractorLabourComponent } from './contractor-labour.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    contractorRouting,
    FormsModule
  ],
  declarations: [
    ContractorLabourComponent
  ]
})
export class ContractorLabourModule { }
