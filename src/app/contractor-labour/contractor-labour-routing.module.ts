import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorLabourComponent } from './contractor-labour.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorLabourComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorLabour',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorLabour',
                  component: ContractorLabourComponent
              }

          ]

  }


]
export const contractorRouting = RouterModule.forChild(appRoutes);
