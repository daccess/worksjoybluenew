import { ContractorLabourModule } from './contractor-labour.module';

describe('ContractorLabourModule', () => {
  let contractorLabourModule: ContractorLabourModule;

  beforeEach(() => {
    contractorLabourModule = new ContractorLabourModule();
  });

  it('should create an instance', () => {
    expect(contractorLabourModule).toBeTruthy();
  });
});
