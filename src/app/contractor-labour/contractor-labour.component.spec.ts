import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorLabourComponent } from './contractor-labour.component';

describe('ContractorLabourComponent', () => {
  let component: ContractorLabourComponent;
  let fixture: ComponentFixture<ContractorLabourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorLabourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorLabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
