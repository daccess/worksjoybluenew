import { Component, OnInit, NgZone, ElementRef, } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { LoginService } from '../../app/shared/services/loginservice';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Logindata } from '../shared/model/loginmodel';
import { SessionService } from '../shared/services/SessionSrvice';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { chngePassModel } from '../shared/model/ChangePassModel';
import { ChangePassService } from '../shared/services/ChangPassService'
import { NgxSpinnerService } from 'ngx-spinner';
import { LayOutService } from '../shared/model/layOutService';
import { MessagingService } from '../shared/services/messaging.service';
import { Http } from '@angular/http';
import { MainURL } from '../shared/configurl';
import { visitDslNode } from '@angular/animations/browser/src/util';
import { DomSanitizer } from '@angular/platform-browser';
interface JQuery {
  modal(command: 'show' | 'hide' | 'toggle'): this;
}
// declare var ImageCompressor: any;
// const compressor = new ImageCompressor();
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  loginresponse: any;
  loginmodel: Logindata;
  retrievedObject: any;
  getvalue: any;
  rollmasterData: any = []
  name: string;
  userName:any;
  loggedUser: any;
  form: FormGroup;
  fullname: any;
  password:any;
  flag: boolean;
  changePass: chngePassModel
  msg: any;
  routerarry: RouterNavigation[];
  UserArry: RouterNavigationUser[];
  UserId: any;
  Pass: any;
  empPerId: any=1;
  model: any = {}
  searchQuery: any;
  empFlag: boolean;
  loginFlag: boolean;
  suggestionList = [];
  profilepic: any;
  CoffList: any = [];
  errflag3:boolean;
  // extraWorkList = [];
  searchText: string = "";
  lastName: any;
  fileToUpload: File;
  imageUrl: any;
  filePath: any;
  baseurl = MainURL.HostUrl
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  home: any;
  resignAndLeaving: any;
  attendanceDashboard: any;
  pimsHr: any;
  masters: any;
  company: any;
  leaveConfiguration: any;
  attendanceConfiguration: any;
  policies: any;
  seperation: any;
  emailSetting: any;
  queryConfiguration: any;
  roleMaster: any;
  customizedDashboard: any;
  employeeList: any;
  leaveBlock: any;
  extraworkingRequest: any;
  reports: any;
  queryAdmin: any;
  leaveRequest: any;
  arRequest: any;
  shiftUpload: any;
  employeeHistory: any;
  approvals: any;
  raiseQuery: any;
  finance: any;
  yearlyCalendar: any;
  socialPlace: any;
  clearancePage: any;
  clearanceDetails: any;
  clearancePageHr: any;
  exitinterview:any;
  serchText: any;
  empPerIds: any = null;
  changeColor: string = "customBlue";
  bulkUpload: any;
  setting: any;
  empPerID: any;
  toWish: any;
  bdayData = [];
  BadyMsg: any;
  FromWish: any;
  birthdayWishId: any;
  PassWord: any;
  QueryApprovedlist = [];
  modalhide= false;
  array = [10, 20, 30];
  adminLoginFlag: boolean = false;
  adminUserName: string;
  roleofuser: any;
  selectempid: any;
  
  Employeedata: any;
  message: any;
  pFlag: boolean = false;
  obje: any={};
  empimage: any;
  receiptUrl: any;
  locationname: any;
  locationdata: any;
  selectedLocation:any=0;
  user: string;
  users: any;
  RoleNames: any;
  empid: any;
  token: any;
  empids: any;
  rolename: any;
  empImageUrl: any;
  companyNames: any;
  manpowerrequestFlag: any;
  username: any;
  passward: any;
  loginUserStatusFlag: string;
  userFlag: any;
  contractorAcceptFlag:any
  oldPassword: any;
  showPassword = false;
  showPassword1 = false;

  constructor(private messagingService: MessagingService,
    private http: Http, private router: Router,
    public Spinner: NgxSpinnerService,
    private layOutService: LayOutService,
    public ChangePass: ChangePassService,
    public chRef: ChangeDetectorRef,
    private service: CompanyMasterService,
    public loginservice: LoginService,
    public SessionService: SessionService,

    public toastr: ToastrService,
    public httpService: HttpClient,
    private zone: NgZone,
    
    private elementRef: ElementRef,private _sanitizer: DomSanitizer,private httpservice:HttpClient) {
      this.user= sessionStorage.getItem('loggeduser')
      
      // this.username=sessionStorage.getItem('userName')
      //  this.passward= sessionStorage.getItem('passward')
      this.users=JSON.parse(this.user);
     this.fullname= this.users.roleList.firstName
     this.lastName=this.users.roleList.lastName
     this.RoleNames=this.users.roleList.roleName
     this.empid=this.users.roleList.EmpId
     this.companyNames=this.users.roleList.CompanyName;
    //  this.userFlag=this.users.roleList.passFlag;
     debugger
    //  if(this.userFlag==false){

     
    // //  this.openpopModel()
    //  }
     
     
    // let user = sessionStorage.getItem('loggedUser');
    // this.loggedUser = JSON.parse(user);
    // this.name = this.loggedUser.mrollMaster.roleName;
    // this.fullname = this.loggedUser.fullName;
    // this.lastName = this.loggedUser.lastName;
    // this.empPerId = this.loggedUser.empPerId
    // this.rollmasterData = this.loggedUser.mrollMaster;
    // this.loginFlag = this.loggedUser.loginFlag;
    // this.adminUserName = this.loggedUser.username;
    // this.roleofuser=this.loggedUser.roleHead
   
   
    // if(this.loggedUser.username == 'Admin' && this.loggedUser.roleName == 'Admin'){
    //   this.adminLoginFlag = true;
    // }
    // else{
    //   this.adminLoginFlag = false;
    // }
    // if(this.adminLoginFlag){
    //   this.hrMode = true;
    //   this.router.navigateByUrl('/layout/settingpage');
    // }

    //this.loginFlag = true;

    // if(this.Aws_flag=='true'){
    //   this.profilepic = this.loggedUser.profilePath;
    // }
    // else{
    //   this.profilepic=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.loggedUser.profilePath);
    // }
    // this.showImageFlag = true;
    // this.changePass = new chngePassModel();
    // this.home = this.rollmasterData.home;
    // this.resignAndLeaving = this.loggedUser.exitEnterview;
    // this.attendanceDashboard = this.rollmasterData.attendanceDashboard;
    // this.pimsHr = this.rollmasterData.pimsHr;
    // this.masters = this.rollmasterData.masters;
    // this.company = this.rollmasterData.company;
    // this.leaveConfiguration = this.rollmasterData.leaveConfiguration
    // this.attendanceConfiguration = this.rollmasterData.attendanceConfiguration
    // this.policies = this.rollmasterData.policies
    // this.seperation = this.rollmasterData.seperation
    // this.emailSetting = this.rollmasterData.emailSetting
    // this.queryConfiguration = this.rollmasterData.queryConfiguration
    // this.roleMaster = this.rollmasterData.roleMaster
    // this.customizedDashboard = this.rollmasterData.customizedDashboard
    // this.employeeList = this.rollmasterData.employeeList
    // this.leaveBlock = this.rollmasterData.leaveBlock
    // this.extraworkingRequest = this.rollmasterData.extraworkingRequest
    // this.reports = this.rollmasterData.reports
    // this.queryAdmin = this.rollmasterData.queryAdmin
    // this.leaveRequest = this.rollmasterData.leaveRequest
    // this.arRequest = this.rollmasterData.arRequest
    // this.shiftUpload = this.rollmasterData.shiftUpload
    // this.employeeHistory = this.rollmasterData.employeeHistory
    // this.approvals = this.rollmasterData.approvals
    // this.raiseQuery = this.rollmasterData.raiseQuery
    // this.finance = this.rollmasterData.finance
    // this.yearlyCalendar = this.rollmasterData.yearlyCalendar
    // this.socialPlace = this.rollmasterData.socialPlace
    // this.clearancePage = this.rollmasterData.clearancePage
    // this.clearancePageHr = this.rollmasterData.clearancePageHr
    // this.clearanceDetails = this.rollmasterData.clearanceDetails
    // this.bulkUpload = this.rollmasterData.bulkUpload
    // this.setting = this.rollmasterData.setting
    // this.exitinterview = this.roleMaster.exitinterview
    //===================== vinod end =====================
    //  this.getBellAlerts();
    //  this.getlocationname();
    // this.getSuggestionList();
    // this.getBdayNotification();

    // this.onAudioPlay();

    if (sessionStorage.getItem('layOutThemeColor')) {
      this.changeColor = sessionStorage.getItem('layOutThemeColor');
    }

    // sessionStorage.setItem('themeColor','nav-pills-blue');

    let hrMode = sessionStorage.getItem('hrMode');
    this.hrMode = JSON.parse(hrMode);
    if(this.adminLoginFlag){
      this.hrMode = true;
      this.model.status = true;
    }
    else{
      this.model.status = this.hrMode;
    }
    
  }
  leaveList = [];
  permissionRequestList = [];
  lateEarlyList = [];
  missPunchList = [];
  onDutyList = [];
  workFromHomeList = [];
  extraWorkList = [];
  hrExtraWorkList = [];
  leaveBlockList = [];
  dateBlockList = [];

  alertLength: number = 0;

  getSuggestionList() {
    this.layOutService.getSuggestionList(this.empPerId).subscribe(data => {
      if (data.result) {
        this.suggestionList = data.result;
      }
    })
  }
  
  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }
  togglePasswordVisibility1() {
    this.showPassword1 = !this.showPassword1;
  }



  // getBellAlerts() {

  //   let loggedUser = sessionStorage.getItem("loggedUser");
  //   if (loggedUser=="null") {

  //     clearInterval(this.interval_id);
  //   }
  //   this.layOutService.getBellAlerts(this.empPerId).subscribe(data => {
  //     this.leaveList = [];
  //     this.permissionRequestList = [];
  //     this.lateEarlyList = [];
  //     this.missPunchList = [];
  //     this.onDutyList = [];
  //     this.workFromHomeList = [];
  //     this.QueryApprovedlist=[];
  //    if (data.result.leaveRequestApproveRes) {
  //       this.leaveList = data.result.leaveRequestApproveRes;
  //     }
  //     if (data.result.requestPRApproveRes) {
  //       this.permissionRequestList = data.result.requestPRApproveRes;
  //     }
  //     if (data.result.requestLateEarlyApproveRes) {
  //       this.lateEarlyList = data.result.requestLateEarlyApproveRes;
  //     }
  //     if (data.result.requestMissPunchApproveRes) {
  //       this.missPunchList = data.result.requestMissPunchApproveRes;
  //     }
  //     if (data.result.requestODApproveRes) {
  //       this.onDutyList = data.result.requestODApproveRes;
   
  //     }
  //     if (data.result.requestWFHApproveRes) {
  //       this.workFromHomeList = data.result.requestWFHApproveRes;
  //     }
  //     if (data.result.extraWorkingReqResDtos) {
  //       this.extraWorkList = data.result.extraWorkingReqResDtos;
      
  //     }
  //     if (data.result.hrRequestToWork) {
  //       this.hrExtraWorkList = data.result.hrRequestToWork;
  //     }
  //     if (data.result.leaveBlock) {
  //       this.leaveBlockList = data.result.leaveBlock;
  //     }

  //     if (data.result.dateBlockList) {
  //       this.dateBlockList = data.result.dateBlockList;
  //     }
  //     if (data.result.coffRequestApproveRes) {
  //       this.CoffList = data.result.coffRequestApproveRes;
  //     }
  //     if(data.result.raiseQueryApproveList){
  //       this.QueryApprovedlist = data.result.raiseQueryApproveList;
  //      }
  //   }, err => {
  //   })
  // }
  getlocationname(){
  


    let url = this.baseurl + '/Location/getById/'+this.loggedUser.locationId
    this.httpService.get(url).subscribe(data => {
        this.locationdata = data["result"]["locationName"];
      //  console.log("locationdatainlayout",this.locationdata);
        
    },
        (err: HttpErrorResponse) => {
        });  
        
  }
  clearAll() {
    let obj = {
      empPerId: this.loggedUser.empPerId
    };
    this.layOutService.clearAll(this.loggedUser.empPerId).subscribe(data => {
      // this.getBellAlerts();
      this.getSuggestionList();
      this.toastr.success('All Notifications Cleared Successfully');
      // this.getBellAlerts();
    }, err => {
      this.toastr.error('Failed To Clear All Notifications');
    })
  }

  updateLeave(item) {
    let obj = {
      leaveApproveRequestId: item.leaveApproveRequestId
    };
    this.layOutService.updateLeave(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully');
    }, err => {
      this.toastr.error('Failed To Delete Notification');

    })
  }

  updateQueryApproved(item){
    let obj = {
      raiseQueryMasterId: item.raiseQueryMasterId
    }
    this.layOutService.updateQuery(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully');
    }, err => {
      this.toastr.error('Failed To Delete Notification');
    })

  }
  updateMissPunch(item) {
    let obj = {
      requestApproveMissPunchId: item.requestApproveMissPunchId
    }
    this.layOutService.updateMissPunch(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully');
    }, err => {
      this.toastr.error('Failed To Delete Notification');

    })
  }
  updateLateEarly(item) {
    let obj = {
      requestApproveLateEarlyId: item.requestApproveLateEarlyId
    };
    this.layOutService.updateLateEarly(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }
  updateOnDuty(item) {
    let obj = {
      requestApproveODId: item.requestApproveODId
    };
    this.layOutService.updateOnDuty(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }
  updatePermission(item) {
    let obj = {
      requestApprovePRId: item.requestApprovePRId
    };
    this.layOutService.updatePermissionRequest(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }
  updateWorkFromHome(item) {
    let obj = {
      requestApproveWFHId: item.WFHId
    };
    this.layOutService.updateWorkFromHome(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }
  updateExtraWorkRequest(item) {
    let obj = {
      extraWorkingApproveRequestID: item.extraWorkingApproveRequestID
    };
    this.layOutService.updateExtraWorkRequest(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }

  updateExtraWorkRequestEmployee(item) {
    let obj = {
      hrRequestToWorkId: item.hrRequestToWorkId
    };
    this.layOutService.updateExtraWorkRequest(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }

  updateLeaveBlock(item) {
    let obj = {
      blockId: item.blockId
    };
    this.layOutService.updateLeaveBlock(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }

  updateDateBlock(item){
    let obj = {
      blockId: item.blockId,
      empPerId : this.loggedUser.empPerId
    };
    this.layOutService.updateDateBlock(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }

  updateHrExtraWorkRequest(item) {
    let obj = {
      extraWorkingRequestID: item.extraWorkingRequestID
    };
    this.layOutService.updateExtraWorkRequestHr(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }

  updateCoff(item) {
    let obj = {
      requesCoffApproveId: item.requesCoffApproveId
    };
    this.layOutService.updateCoff(obj).subscribe(data => {
      // this.getBellAlerts();
      this.toastr.success('Notificatation Deleted Successfully')
    }, err => {
      this.toastr.error('Failed To Delete Notification')

    })
  }

  actionExtraWork() {
    sessionStorage.setItem('from', 'layout');
    sessionStorage.setItem('length', '1');
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');

  }

  actionDateSuggestion(item) {
    sessionStorage.setItem('from', 'layout');
    sessionStorage.setItem('data', item);
    // sessionStorage.setItem('length','1');
    this.router.navigateByUrl('/layout/leaverequest/leaverequest');
  }
  actionHrExtraWork(item) {
    sessionStorage.setItem('from', 'layout');
    sessionStorage.setItem('data', item);
    // sessionStorage.setItem('length','1');
    this.router.navigateByUrl('/layout/leaverequest/leaverequest');
  }
  hrMode: boolean = false;
  selectMode(e) {
    this.hrMode = e;
    sessionStorage.setItem('hrMode', JSON.stringify(this.hrMode));
    if(this.adminLoginFlag){
      this.hrMode = true;
      this.router.navigateByUrl('/layout/settingpage');
    }
    else{
      if (!e) 
      {
        if (this.loggedUser.employeeStatus) 
        {
          this.router.navigateByUrl('/layout/emppims/empdetails?q=personal');
        }
        else 
        {
          this.router.navigateByUrl('/layout/home');
        }
      }
      else 
      {
        this.router.navigateByUrl('/layout/hrdashboard');
      }
    }
    
  }

  focusOut(event) {
    this.empFlag = false;
    this.searchQuery = '';

  }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId
    this.rolename=this.users.roleList.roleName;
    this.manpowerrequestFlag=this.users.roleList.isManpowerRequestAuth
    this.empImageUrl=this.users.roleList.empImageUrl
    this.userFlag=this.users.roleList.passFlag;
    this.contractorAcceptFlag=this.users.roleList.contractorFlag
    // this.loginUser()
    // this.getAllPendReq();
    // this.service.hide();
    // this.flag = false
    // this.chRef.detectChanges();
    // this.getAllEmp();
    // this.UserArry = new Array()

    // this.UserArry = [
    //   { "linkName": "Home", "link": "/layout/home" },
    //   { "linkName": " Leave Request", "link": "/layout/leaverequest/leaverequest" },
    //   { "linkName": "Attendance Regularisation Request", "link": "/layout/otherrequests" },
    //   { "linkName": " Extra Working Request", "link": "/layout/extraworkreq" },
    //   { "linkName": " Yearly Calendar", "link": "/layout/yearlycalendar" },
    //   { "linkName": "View Attendence Report", "link": "/layout/attendencereportview" },
      
    // ]

    













    // this.getBellAlerts();
   
      this.interval_id = setInterval(() => {
        let loggedUser = sessionStorage.getItem("loggedUser");
        if(loggedUser!=="null"){
        // this.getBellAlerts();
        }
      },90000); 
    // this.getBdayNotification();
  }
  callOnce(){
    this.userFlag=true
  }
  calltwo(){
    this.contractorAcceptFlag=true
  }
//   loginUser(){
//     let obj={
//       userName:this.username,
//       password:this.passward
//     }
//     this.loginservice.postloginservice(obj).subscribe(data => {
      
//       this.Spinner.hide();
//       this.loginUserStatusFlag=JSON.stringify(data);
//       console.log("sdkjhfkadshfhsadhfsadhf",this.loginUserStatusFlag)

//   })
// }
  interval_id: any;
 
 
  onSubmit(f) {
   
    this.postnewPass(f)



  }
  changePasswrd() {
    setTimeout(() => {
      document.getElementById("openModalButton12").click();
    }, 10);

  }
 


  onBlurMethod() {
    this.changePass.empPerId = this.empPerId;
    this.ChangePass.postchangePass(this.changePass).subscribe(data => {
        this.msg = data['result']['loginMsg']
        this.chRef.detectChanges();
        if (this.msg == 'Correct Password') {
          this.flag = true;
          this.chRef.detectChanges();
        } else {
          this.flag = false;
        }
        this.chRef.detectChanges();
        this.UserId = data['result']['userId'];
      }, (err: HttpErrorResponse) => {
          this.toastr.error('Server Side Error');
        });

  }
  resetform(f) {
    f.reset();
     //this.empPerIds = null;
  }
  obj(event) {
    this.Pass = event.target.value
  }
  postnewPass(f) {
    // if(this.changePass.passWord==null || (this.PassWord==null && this.changePass.pas==null)){
    //   this.toastr.error("please fill required field")
     
    // }else{
  this.changePass.userId = this.UserId
  this.changePass.passWord = this.Pass;
  this.ChangePass.postnewPass(this.changePass).subscribe(data => {
       f.reset();
      this.changePass = new chngePassModel();
      // this.PassWord
      // this.changePass.pas = '';
      // this.modalhide=true
      this.toastr.success("password changed successfully");
     
     
    }, (err: HttpErrorResponse) => {
        this.toastr.error('Server Side Error');
    });


} 
resetpassword() {
  setTimeout(() => {
    document.getElementById("openModalButton13").click();
  }, 10);

}

  Profile() {
    debugger
    sessionStorage.setItem('whoClicked', 'EMP');
    sessionStorage.setItem('empids',this.empid)
    this.router.navigateByUrl("/layout/emppimsview");
  }

  clearout() {
    this.Spinner.show();
    sessionStorage.clear();
    this.Spinner.hide();
    this.router.navigateByUrl("#")
  }

  clearout1() {
    this.Spinner.show();
    sessionStorage.clear();
    this.router.navigateByUrl('#')
    this.Spinner.hide();
  }
  logOut() {
    this.messagingService.deleteToken(this.loggedUser.empPerId).subscribe(data => {
    }, err => {
    })
    this.Spinner.show();
    sessionStorage.clear();
    this.toastr.info('Log Out Successfully...')

  }

  transform(value: any, args?: any): any {

    if (!value) return null;
    if (!args) return value;

    args = args.toLowerCase();

    return value.filter(function (item) {
      return JSON.stringify(item).toLowerCase().includes(args);
    });
  }
  arrowkeyLocation = 0;

  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }

  filterItem(value) {
    this.getSearchOPtionLink();
    if (!value) {
      this.empFlag = false;
      // this.questionData = [];
    }
    if (value) {
      // this.questionData = temp;
      this.empFlag = true;

    }
  }

  searchEmpPerId: any;
  suggestThis(item) {
    this.searchQuery = item.link;
    this.router.navigateByUrl(item.link);
    this.searchQuery = null
    this.empFlag = false;
    // this.ResetPass.empPerId=item.empPerId;
    // this.getuser();
  }
  goto(event) {
  }
  empSearchSuggestionList = [];
  getSearchSuggestionList(text) {
    if (!text) {
      this.empSearchSuggestionList = [];
    }
    if (text) {
      this.layOutService.getSearchSuggestionList(text,this.loggedUser.locationId).subscribe(data => {
        this.empSearchSuggestionList = data.result;
      }, err => {
      })

    }
  }
  showImageFlag: boolean = true;
  employeeData = { profilePath:<any> "", salutation: "", fullName: "", lastName: "", descName: "", empId: "", deptName: "", officialEmailId: "", officialContactNumber: "" };
  suggestThisEmployee(item) {
    this.layOutService.suggestThisEmployee(item.empOfficialId).subscribe(data => {
      this.showImageFlag = false;
      this.searchText = "";
      this.employeeData = data.result[0];
      if(this.Aws_flag=='true'){
        this.employeeData.profilePath = item.profilePath;
      }else{
        this.employeeData.profilePath=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + item.profilePath);
      }
      this.empSearchSuggestionList = []
    }, err => {

    })
  }

  closeModal() {
    this.showImageFlag = true;
    this.searchText = "";
    this.empSearchSuggestionList = [];
    this.employeeData = Object();
    // this.employeeData.fullName = this.loggedUser.fullName + " " + this.loggedUser.lastName;
    // this.employeeData.descName = this.loggedUser.descName;
    // this.employeeData.empId = this.loggedUser.empId;
    // this.employeeData.deptName = this.loggedUser.deptName;
    // this.employeeData.officialEmailId = this.loggedUser.officialEmailId;
    // this.employeeData.officialContactNumber = this.loggedUser.officialContactNumber;
  }
  uploadFile(fileLoader) {
    fileLoader.click();
  }
  handleFileInput(file: FileList) {
    // this.Spinner.show()
    this.fileToUpload = file.item(0);
    this.obje.name = file.item(0).name; 
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.empimage = event.target.result;
      this.receiptUrl=event.target.result
     //  this.Spinner.hide();
   
      
    }
    reader.readAsDataURL(this.fileToUpload);
   
       // window.URL = window.URL || window.webkitURL;
       let flag1 = false;
       if (file) {
         var img = new Image();
   
         img.src = window.URL.createObjectURL(file[0]);
   
         img.onload = function () {
           // var flag = false;
           var width = img.naturalWidth,
             height = img.naturalHeight;
   
           window.URL.revokeObjectURL(img.src);
   
           if (width < 2000 && height < 2000) {
             flag1 = true;
           }
           else {
             flag1 = false;
           }
         };
   
         setTimeout(() => {
         
           if (flag1) {
   
             this.fileToUpload = file.item(0);
             var reader = new FileReader();
             reader.onload = (event: any) => {
               this.imageUrl = event.target.result;
             }
             reader.readAsDataURL(this.fileToUpload);
             this.postprofileimage(this.fileToUpload)
           }
           else {
             this.toastr.error("Please upload image upto 2 MB")
           }
         }, 300);
       }
     }
   
     fileToupload() {
   
     }
     postprofileimage(files) {
   this.ppostFile(this.fileToUpload)
      //  this.ppostFile(this.fileToUpload).subscribe((data: any) => {
      //    if(this.Aws_flag=='true'){
      //      this.profilepic = JSON.parse(data._body).result.profilePath;
      //    }
      //    else{
      //      this.profilepic = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + JSON.parse(data._body).result.imageBase64);
      //      this.UpdateLoginImage(JSON.parse(data._body).result.filPath);
      //    }
      //    this.loggedUser.profilePath = this.profilepic;
      //    // sessionStorage.setItem('loggedUser', this.loggedUser);
      //    ss
      //    this.toastr.success('Profile Picture Updated Successfully')
   
      //    sessionStorage.setItem("propic", "P")
      //  })
     }
     //set updated image on login
     UpdateLoginImage(filPath){
       let url = this.baseurl + '/updateUserProfilePic';
       let body={
         "userProfilePath":filPath,
         "empPerId":this.empPerId
       }
       this.httpService.post(url,body).subscribe((data:any) => {
         this.profilepic = JSON.parse(data).result.empImageUrl;
         
       },
       (err: HttpErrorResponse) => {
       });
     }
  
     ppostFile(fileToUpload: File) {
      if(this.rolename=='Contractor'){
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
       let url = this.baseurl + '/updateContactorProfileImage';
       if(this.Aws_flag!='true'){
         url=this.baseurl + '/fileHandaling';
       }
       const endpoint = url;
       // const formData: FormData = new FormData();
       // formData.append('uploadfile', fileToUpload, this.empimage);
       // formData.append('empPerId', this.empPerId);
       let obj={
         "empImageUrl":this.empimage,
         "empId":this.empids
       }
      //  return this.httpService.post(endpoint, obj,{headers}).map(x => x);
      this.httpservice.post(url,obj, { headers }).subscribe(data => {

        this.toastr.success('Profile Picture Updated Successfully')
    
       })
      
     }
     else{
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    
      let url = this.baseurl + '/updateEmployeeProfileImage';
      if(this.Aws_flag!='true'){
        url=this.baseurl + '/fileHandaling';
      }
      const endpoint = url;
      // const formData: FormData = new FormData();
      // formData.append('uploadfile', fileToUpload, this.empimage);
      // formData.append('empPerId', this.empPerId);
      
      let obj={
        "empImageUrl":this.empimage,
        "empId":this.empids
      }
     
      // return this.httpservice.post(endpoint, obj,{headers}).map(x => x);;
      this.httpservice.post(url,obj, { headers }).subscribe(data => {

        this.toastr.success('Profile Picture Updated Successfully')
    
       })
      
     }
    }
   

  getSearchOPtionLink() {
    let url = this.baseurl + '/searchBar?role=' + this.name + '&serchText=' + this.searchQuery;
    this.httpService.get(url).subscribe(data => {
      if (data) {
        this.routerarry = data['result'];
      }
    },
    (err: HttpErrorResponse) => {
    });
  }
  BdayFlag: boolean = false
  // getBdayNotification() {
  //   this.toWish = this.loggedUser.fullName + " " + this.loggedUser.lastName;
  //   this.empPerID = this.loggedUser.empPerId;
  //   let url = this.baseurl + '/birtdaywishes/'
  //   this.httpService.get(url + this.empPerId).subscribe(data => {
  //       if (data) {
  //         this.bdayData = data['result'];
  //         this.BadyMsg = this.bdayData[0].birthdayWish;
  //         this.FromWish = this.bdayData[0].employeeName;
  //         this.BdayFlag = true;
  //         this.alertLength = this.suggestionList.length + this.leaveList.length + this.permissionRequestList.length + this.lateEarlyList.length + this.missPunchList.length + this.onDutyList.length + this.workFromHomeList.length
  //           + this.extraWorkList.length + this.hrExtraWorkList.length + this.leaveBlockList.length + this.dateBlockList.length + this.CoffList.length + this.bdayData.length+this.QueryApprovedlist.length;

  //       }
  //       else {
  //         this.alertLength = this.suggestionList.length + this.leaveList.length + this.permissionRequestList.length + this.lateEarlyList.length + this.missPunchList.length + this.onDutyList.length + this.workFromHomeList.length
  //           + this.extraWorkList.length + this.hrExtraWorkList.length + this.leaveBlockList.length + this.dateBlockList.length + this.CoffList.length +this.QueryApprovedlist.length;
  //       }
  //       if (parseInt(sessionStorage.getItem("notlength")) < this.alertLength) {
  //         this.playAudio();
  //       }
  //       sessionStorage.setItem("notlength", this.alertLength.toString())
  //     },
  //     (err: HttpErrorResponse) => {
  //       this.alertLength = this.suggestionList.length + this.leaveList.length + this.permissionRequestList.length + this.lateEarlyList.length + this.missPunchList.length + this.onDutyList.length + this.workFromHomeList.length
  //         + this.extraWorkList.length + this.hrExtraWorkList.length + this.leaveBlockList.length + this.dateBlockList.length + this.CoffList.length + this.bdayData.length + this.QueryApprovedlist.length;
  //       if (parseInt(sessionStorage.getItem("notlength")) < this.alertLength) {
  //         this.playAudio();
  //       }
  //       sessionStorage.setItem("notlength", this.alertLength.toString())
  //     }
  //   );

  // }
  updateBday(i) {
    let obj = {
      birthdayWishId: i.birthdayWishId
    }
    let url = this.baseurl + '/clearBirthdayWish';

    this.httpService.put(url, obj).subscribe(data => {
      // this.getBdayNotification();
    },
    (err: HttpErrorResponse) => {
    });
  }


  setColor(data, themeClass) {
    //  this.changeColor=data;
    sessionStorage.setItem('layOutThemeColor', data);
    sessionStorage.setItem('themeColor', themeClass);
    location.reload();
  }


  goToHome() {
    // this.router.navigateByUrl('/layout/home');
    // this.router.navigateByUrl('/layout/emppims/empdetails?q=personal');
    if (this.loggedUser.employeeStatus) {
      this.router.navigateByUrl('/layout/emppims/empdetails?q=personal');
    }
    else {
      this.router.navigateByUrl('/layout/home');
    }
  }
  menuOpenClose: boolean = true;
  myFunction() {
    var x = document.getElementById("mySidepanel");
    if (x.style.width === "250px") {
      x.style.width = "60px";
      this.menuOpenClose = true;
    } else {
      x.style.width = "250px";
      this.menuOpenClose = false;
    }

  }
  myFunctionHideMenu() {
    var x = document.getElementById("mySidepanel");
    if (x.style.width === "250px") {
      x.style.width = "60px";
      this.menuOpenClose = true;
    }
  }

  playAudio() {
    let audio = new Audio();
    audio.src = "../assets/definite.mp3";
    audio.load();
    audio.play();
  }

  downloadAPK() {
    // window.open('https://i.diawi.com/2Ca1Rd','_blank');
    // https://drive.google.com/file/d/1vnv3khgEWflFTDlVdGpDx8_9jEZzz7Mm/view?usp=sharing
    // window.open("https://s3.amazonaws.com/techarena.doghouse/demo/1582706904453-app-debug.apk",'_blank');
    window.open("https://drive.google.com/file/d/13Q9H0E8U77XB8KvJSEu15XDCjuZZqCc9/view?usp=sharing", '_blank');
  }

  eyeFlagc: boolean = false
  seePasswordo() {
    var x: any = document.getElementById("Old");
    if (x.type === "password") {
      this.eyeFlagc = true;
      x.type = "text";
    } else {
      this.eyeFlagc = false;
      x.type = "password";
    }
  }

  eyeFlago: boolean = false
  seePasswordc() {
    var x: any = document.getElementById("newpas");
    if (x.type === "password") {
      this.eyeFlago = true;
      x.type = "text";
    } else {
      this.eyeFlago = false;
      x.type = "password";
    }
  }

  eyeFlagn: boolean = false
  seePasswordn() {
    var x: any = document.getElementById("conpas");
    if (x.type === "password") {
      this.eyeFlagn = true;
      x.type = "text";
    } else {
      this.eyeFlagn = false;
      x.type = "password";
    }
  }

  // this.playAudio();

  allApprovalRequest: number = 0;
  // getAllPendReq() {
  //   let url = this.baseurl + '/getPendingRegularisation?empId=' + this.loggedUser.empId;
  //   this.http.get(url).subscribe(
  //     data => {
  //       let allData = data.json()["result"][0];
  //       if (allData.totalCount) {
  //         this.allApprovalRequest = allData.totalCount;
  //        // console.log("allaprroverwq&&",this.allApprovalRequest );
  //       }
  //     },
  //     (err: HttpErrorResponse) => {
  //       this.allApprovalRequest = 0;
  //     });

  // }
  getAllEmp() {
  if(this.loggedUser.roleHead=='HR'){

  
    let url = this.baseurl + '/getEmployeeForPassword/';
    this.httpService.get(url + this.loggedUser.compId+"/"+this.loggedUser.locationId).subscribe(data => {
        this.Employeedata = data["result"];
       // console.log("this is the empall data"+this.employeeData)
        // for (let index = 0; index < this.Employeedata.length; index++) {
        //     this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ; 
        //     console.log("this is the empall data"+this.employeeData)
        // }
    },
    (err: HttpErrorResponse) => {
        console.log(err.message);
        this.Employeedata =[];
    });
}
  
  else if(this.loggedUser.roleHead=='Admin'){
    let url = this.baseurl + '/getEmployeeForPassword/';
    this.httpService.get(url + this.loggedUser.compId+"/"+this.selectedLocation).subscribe(data => {
        this.Employeedata = data["result"];
       // console.log("this is the empall data"+this.employeeData)
        // for (let index = 0; index < this.Employeedata.length; index++) {
        //     this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ; 
        //     console.log("this is the empall data"+this.employeeData)
        // }
    },
    (err: HttpErrorResponse) => {
        console.log(err.message);
        this.Employeedata =[];
    });
}
  }
  changePassward(){
   debugger
    let obj={
      userName:this.userName,
      password:this.password,
      oldPassword:this.oldPassword
    }
   
      let url = this.baseurl + '/ChnageUserPassword';
      const tokens = this.token;
      this.Spinner.show()
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
          this.httpService.post(url,obj,{headers}).subscribe(data => {
      this.toastr.success("passward change successfullhy")
      this.userFlag=true;
      this.Spinner.hide()
     
        },(err: HttpErrorResponse) => {
      this.toastr.error("server side error")
      this.Spinner.hide()
            })
      
      
    
  }
  checkPassward(){
    
     let obj={
       userName:this.userName,
      
      password:this.oldPassword
     }
    
       let url = this.baseurl + '/CheckOldUserPassword';
       const tokens = this.token;
 
       const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
           this.httpService.post(url,obj,{headers}).subscribe(data => {
       
       this.errflag3=true;
     
      
         },
         (err: HttpErrorResponse) => {
          this.errflag3 = false
        });
     
   }
  
  
  // openpopModel(){
    
  //   document.getElementById('passChange').click();
    
  // }


  selectEmp(e){
    let empperid = e;
    let str = empperid.split('-');
    let selectedempPerIdNo = Number(str[1]);
    //console.log("******************",selectedempPerIdNo);
    for (let i = 0; i < this.Employeedata.length; i++) {
       if(this.Employeedata[i].empId == selectedempPerIdNo ){
        this.selectempid = this.Employeedata[i].userId; 
       }
    }
  }
  
  Resetpassword(){

    let obj={
      //passWord :this.ForgoePassw.passWord,
      "userId": this.selectempid,
      "passWord":this.password
    }
    //console.log("this is the selceted"+obj.passWord+obj.userId)
    let url1 = this.baseurl + '/ChnageUserPassword';
    this.httpService.post(url1,obj).subscribe((data: any) => {
        this.message = data['message'];
        this.empPerIds = null;
        this.toastr.success(this.message);
      //
      this.modalhide=true
      this.router.navigateByUrl("#")
      
      }, (err: HttpErrorResponse) => {
       
      });
     
    
    }
  
  compare(a : any, b : any) {
    var chunkRgx = /(_+)|([0-9]+)|([^0-9_]+)/g;
    var ax = [], bx = [];
    a =  a.toString();
    b =  b.toString();
    a.replace(chunkRgx, function(_, $1, $2, $3) {
        ax.push([$1 || "0", $2 || Infinity, $3 || ""])
    });
    b.replace(chunkRgx, function(_, $1, $2, $3) {
        bx.push([$1 || "0", $2 || Infinity, $3 || ""])
    });
    while(ax.length && bx.length) {
        var an = ax.shift();
        var bn = bx.shift();
        var nn = an[0].localeCompare(bn[0]) || 
                 (an[1] - bn[1]) || 
                 an[2].localeCompare(bn[2]);
        if(nn) return nn;
    }
    
    return ax.length - bx.length;
  }

  test = ["img10.png", "img1.png", "img101.png", "img101a.png", "abc10.jpg", "abc10",
    "abc2.jpg", "20.jpg", "20", "abc", "abc2", "",10,8];

  displayOutput() {
    this.test.sort((a: any, b: any) =>this.compare(a,b))
  }
}



class RouterNavigation {
  public linkName: string;
  public link: string;
}
class RouterNavigationUser {
  public linkName: string;
  public link: string;
}
