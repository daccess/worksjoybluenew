import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeUpdateComponent } from './employee-update.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import { FilePickerModule } from  'ngx-awesome-uploader';
import { FilterfirmnamePipe } from './filterfirmname.pipe';


const routes: Routes = [
  {
    path: '',
    component: EmployeeUpdateComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedModule,
    FilePickerModule
  ],
  declarations: [EmployeeUpdateComponent ,FilterfirmnamePipe]
})
export class EmployeeupdateModule { }
