import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash'; 

@Pipe({
  name: 'filterfirmname',
  pure: false,
})
export class FilterfirmnamePipe implements PipeTransform {

  transform(value: any): any{
    
    if(value!== undefined && value!== null){
        return _.uniqBy(value,  'conFirmName');
    }
    return value;
}

}
