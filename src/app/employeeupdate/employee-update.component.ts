import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { dataService } from '../shared/services/dataService';
import { FilePreviewModel } from 'ngx-awesome-uploader';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from "@angular/router";
import { InfoService } from "../shared/services/infoService";
import { MainURL } from '../shared/configurl';
import { DomSanitizer } from '@angular/platform-browser';
import { debounce } from 'rxjs-compat/operator/debounce';
@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css']
})
export class EmployeeUpdateComponent implements OnInit {
  //Declarations
  baseurl = MainURL.HostUrl;
  Aws_flag=MainURL.Aws_flag;
  userForm: FormGroup;
  employee_id:any;
  employee_data:any;
  view_url='employeecontractorbyId';
  loggedUser;
  compId;
  sanctioning_authorities_url='employeeListWhoHasAuthority';
  contractor_supervisor_url='contractorEmpListWhoHasSupervisor';
  authorities:any[]=[];
  contractor_supervisors=[];
  roles_url='RollMaster';
  roles:any[]=[];
  DepartmentAllData: any;
  dept_data_url='/Department/Active/';
  SubDepartmentData:any;
  sub_dept_url='SubDepartment/Active';
  LocationAllData: any;
  location_data_url='Location/Active';
  DesignationAllData:any;
  designation_url='Designation/Active';
  contractNamesData:any;
  contractServiceData: any;
  contractor_service_url='CosSerType/Active';
  shift_group_url='ShiftMaster/ByActive';
  WeeklyOffcalendarData: any;
  calendar_url='weeklyOfMasterActive';
  imageURL;
  panURL;
  AadharURL;
  EduURL;
  update_url='/updateempcontractor';
  Shiftmastergroupdata: any;
  Shiftgroupdata: any;
  //shift_group='ShiftGroup/Active';
  adapter = new dataService(this.http);
  received_message:any;
  selectedEmpTypeobj: number;
  selectedSubEmpTypeobj: number;
  empTypeAllData: any;
  subempTypeAllData: any;
  fileToUpload: File = null;
  obje: any={};
  empimageURl: any;
  img: any;
  newImageFlag:boolean=false;
  receiptUrl: any;
  isNextDisabled=true
  selectedshiftgroupobj: any;
  selectedshiftmastergroupobj: any;
  selectedfirmnameobj: any;
  selectedcontractorserviceobj: any;
  selectedWeeklyGroupobj: any;
  selectedLocationId: any=0;
  selectedLocationobj: any;
  weeklyCalendarId: any;
  temp:any;
  locoid: any;
  constructor(private dataservice:dataService,private userFB: FormBuilder,public httpService: HttpClient, private ref:ChangeDetectorRef,private toastr: ToastrService,private http : HttpClient,private router: Router,private InfoService:  InfoService,private _sanitizer: DomSanitizer) { 
   
}

  ngOnInit() {
     //get data
     this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.received_message = message;
        }
      });
    //set return data
    this.InfoService.changeMessage(this.received_message);
    let employee_details = sessionStorage.getItem('a_contractor');
    this.employee_id=employee_details;
    //get company details
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    //initilise form
    this.userForm = this.userFB.group({
			salutation: [''],
      fullName: ['', Validators.required],
     
      lastName: ['', Validators.required],
      bioId: ['',Validators.required],
      gender: [''],
      dateOfBirth: ['', Validators.required],
      bloodGroup: [''],
      middleName: [''],
     
      buildingNoCurrent: [''],
      socityOrLaneCurrent: [''],
      areaCurrent: [''],
      cityorTahshilCurrent: [''],
      stateCurrent: [''],
      pincodeCurrent: [''],
      nameOfPerson: [''],
      emrContactNumber: [''],
      //permanent address
      buildingNoPermant: [''],
      socityOrLanePermant: [''],
      areaPermant: [''],
      cityOrTalukaPermant: [''],
      statePermant: [''],
      pinCodePermant: [''],
      //edu qualifications
      highestEduction: [''],
      currentyStudying: [''],
      currentStudyDetails: [''],
      subOfSpecialisation: [''],
      //official
      
      empId: ['',Validators.required],
      skillWisePerDaySalary: [''],
      deptId: ['',Validators.required],
      subDeptId: [''],
      locationId: ['',Validators.required],
      empTypeId:['',Validators.required],
      subEmpTypeId:['',Validators.required],
      //designation
      descId: ['',Validators.required],
      conSerTypeId: ['',Validators.required],
      contractorId: ['',Validators.required],
      validStartDate:['',Validators.required],
      validEndDate:['',Validators.required],
      skillCatagory:[''],
      //role
      rollMasterId:['',Validators.required],
      // secondLevelReportingManager:[''],
     
      //attendence settings
      shiftType:['',Validators.required],
      shiftMasterId:['',Validators.required],
      shiftGroupId:['',Validators.required],
      weeklyOffCalenderMasterId:['',Validators.required],
      firstLevelReportingManager:[''],
      empimageURl:[''],
      //govt data
      panNo:['',Validators.required],
      adharNo:['',Validators.required],
    
      esicNumber:['',Validators.required],
      universalAccNo:['',Validators.required, Validators.maxLength(12)],
      pfNumber:['',Validators.required, Validators.maxLength(16)],
      empDocMasterList: [],
    });

    this.userForm.valueChanges.subscribe((v) => {
      this.isNextDisabled = !this.userForm.valid;
 });
    //set data to form
    if(this.employee_id){
      this.dataservice.getRecordsByid(this.view_url,this.employee_id).subscribe(res=>{
        if(res.result){
          this.employee_data=res.result;
          console.log("employeeaddeddataalready",  this.employee_data);
          this.locoid=  this.employee_data.locationId;
          console.log("locoid",this.locoid);
          this.img=this.employee_data.empimageURl;
    
          this.userForm= this.userFB.group({
            
            salutation: new FormControl(this.employee_data.salutation, []),
            fullName: new FormControl(this.employee_data.fullName, [Validators.required]),
            middleName: new FormControl(this.employee_data.middleName, []),
            lastName: new FormControl(this.employee_data.lastName, [Validators.required]),
            dateOfBirth: new FormControl(moment(new Date(this.employee_data.dateOfBirth)).format('YYYY-MM-DD'), [Validators.required]),
            gender: new FormControl(this.employee_data.gender, []),
            bloodGroup: new FormControl(this.employee_data.bloodGroup, []),
            buildingNoCurrent: new FormControl(this.employee_data.buildingNoCurrent, []),
            socityOrLaneCurrent: new FormControl(this.employee_data.socityOrLaneCurrent, []),
            areaCurrent: new FormControl(this.employee_data.areaCurrent, []),
            cityorTahshilCurrent: new FormControl(this.employee_data.cityorTahshilCurrent, []),
            stateCurrent: new FormControl(this.employee_data.stateCurrent, []),
            pincodeCurrent: new FormControl(this.employee_data.pincodeCurrent, []),
            nameOfPerson: new FormControl(this.employee_data.nameOfPerson, []),
            emrContactNumber: new FormControl(this.employee_data.emrContactNumber, []),
            buildingNoPermant: new FormControl(this.employee_data.buildingNoPermant, []),
            socityOrLanePermant: new FormControl(this.employee_data.socityOrLanePermant, []),
            areaPermant: new FormControl(this.employee_data.areaPermant, []),
            cityOrTalukaPermant: new FormControl(this.employee_data.cityOrTalukaPermant, []),
            statePermant: new FormControl(this.employee_data.statePermant, []),
            pinCodePermant: new FormControl(this.employee_data.pinCodePermant, []),
            highestEduction: new FormControl(this.employee_data.highestEduction, []),
            currentyStudying: new FormControl((this.employee_data.currentyStudying), []),
            currentStudyDetails: new FormControl(this.employee_data.currentStudyDetails, []),
            subOfSpecialisation: new FormControl(this.employee_data.subOfSpecialisation, []),
            bioId: new FormControl(this.employee_data.bioId, [Validators.required]),
            empId: new FormControl(this.employee_data.empId, [Validators.required]),
            skillWisePerDaySalary: new FormControl(this.employee_data.skillWisePerDaySalary, []),
            deptId: new FormControl(this.employee_data.deptId, [Validators.required]),
            //------------------------
            locationId: new FormControl(  this.locoid, [Validators.required]),
            descId: new FormControl(this.employee_data.descId, [Validators.required]),
            conSerTypeId: new FormControl(this.employee_data.conSerTypeId, [Validators.required]),
            contractorId: new FormControl( [Validators.required]),
            validStartDate: new FormControl(moment(new Date(this.employee_data.validStartDate)).format('YYYY-MM-DD'), [Validators.required]),
            validEndDate: new FormControl(moment(new Date(this.employee_data.validEndDate)).format('YYYY-MM-DD'), [Validators.required]),
            empTypeId: new FormControl(this.employee_data.empTypeId,[Validators.required]),
            subEmpTypeId: new FormControl((this.employee_data.subEmpTypeId).toString(),[Validators.required]),
            skillCatagory: new FormControl(this.employee_data.skillCatagory, []),
            rollMasterId: new FormControl(this.employee_data.rollMasterId, [Validators.required]),
            secondLevelReportingManager: new FormControl(this.employee_data.secondLevelReportingManager, []),
            firstLevelReportingManager: new FormControl(this.employee_data.firstLevelReportingManager, []),
            shiftType: new FormControl(this.employee_data.shiftType, [Validators.required]),
            shiftMasterId: new FormControl(this.employee_data.shiftMasterId, [Validators.required]),
            shiftGroupId: new FormControl(this.employee_data.shiftGroupId, [Validators.required]),
            weeklyOffCalenderMasterId: new FormControl(this.employee_data.weeklyOffCalenderMasterId, [Validators.required]),
            panNo: new FormControl(this.employee_data.panNo, [Validators.required]),
            adharNo: new FormControl(this.employee_data.adharNo, [Validators.required]),
           
            esicNumber: new FormControl(this.employee_data.esicNumber, [Validators.required]),
            universalAccNo: new FormControl(this.employee_data.universalAccNo, [Validators.required, Validators.maxLength(12)]),
            pfNumber: new FormControl(this.employee_data.pfNumber, [Validators.required, Validators.maxLength(16)]),
            empimageURl: new FormControl(this.employee_data.empimageURl, []),
            empDocMasterList: new FormControl([]),
            
          })
          //test
          this.userForm.controls.subEmpTypeId.setValue(this.employee_data.subEmpTypeId);
          if(this.employee_data.empDocMasterList){
            for(let i=0;i<this.employee_data.empDocMasterList.length;i++){
              if(this.employee_data.empDocMasterList[i].docName=="eduCertificate"){
                let temp={
                  "docName" : "eduCertificate",
                  "docType" : this.employee_data.empDocMasterList[i].docType,
                  "docUrl"  :this.employee_data.empDocMasterList[i].docUrl
                }
                this.userForm.value.empDocMasterList[2]=temp;
                if(this.Aws_flag!='true'){
                  this.EduURL=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.employee_data.empDocMasterList[i].base64DocUrl);
                }else{
                  this.EduURL=this.employee_data.empDocMasterList[i].docUrl;
                }
              }
              if(this.employee_data.empDocMasterList[i].docName=="aadharcard"){
                let temp={
                  "docName" : "aadharcard",
                  "docType" : this.employee_data.empDocMasterList[i].docType,
                  "docUrl"  :this.employee_data.empDocMasterList[i].docUrl
                }
                this.userForm.value.empDocMasterList[1]=temp;
                if(this.Aws_flag!='true'){
                  this.AadharURL=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.employee_data.empDocMasterList[i].base64DocUrl);
                }else{
                  this.AadharURL=this.employee_data.empDocMasterList[i].docUrl;
                }
              }
              if(this.employee_data.empDocMasterList[i].docName=="pancard"){
                let temp={
                  "docName" : "pancard",
                  "docType" : this.employee_data.empDocMasterList[i].docType,
                  "docUrl"  :this.employee_data.empDocMasterList[i].docUrl
                }
                this.userForm.value.empDocMasterList[0]=temp;
                if(this.Aws_flag!='true'){
                  this.panURL=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.employee_data.empDocMasterList[i].base64DocUrl);
                }else{
                  this.panURL=this.employee_data.empDocMasterList[i].docUrl;
                }
              }
            }
          }
          this.userForm.controls.deptId.setValue(this.employee_data.deptId);
          this.userForm.controls.conSerTypeId.setValue(this.employee_data.conSerTypeId);
          this.userForm.controls.contractorId.setValue(this.employee_data.contractorId);
          this.ref.markForCheck();
          //
          this.getRoles();
          this.getAuthorities();
          this.getDepartments();
          this.getAuthorities();
         this.locationGRoup(  this.selectedLocationobj);
          this.getDesignations();
          this.contractorSerType();
          this.weeklyoffdata();
          this.getshiftGroup();
          this.shiftmastergroup();
           this.empType();
          this.subEmpType(this.employee_data.empTypeId)
          this.getshiftGroup();
          this.shiftmastergroup();
          this.getcontractNamesData(); 
          
          if(this.employee_data.currentyStudying){
            this.userForm.controls.currentyStudying.setValue(this.employee_data.currentyStudying.toString());
            let dept_id=this.userForm.get('currentyStudying').value;
          }
        } 
      })
    }
  }
  
  //get roles
  getRoles(){
    if(this.compId){
      this.dataservice.getRecordsByid(this.roles_url,this.compId).subscribe(res=>{
          if(res.statusCode==200){
            this.roles=res.result;
          }
      });
    }
  }
  getAuthorities(){
    if(this.loggedUser.roleHead=='HR'){
    let url = this.baseurl + '/employeeListWhoHasAuthority/';
    this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
      this.authorities=data["result"];
      console.log("authdatacontreactor$$",this.authorities);
    }, (err: HttpErrorResponse) => {
    });
  }
  else if(this.loggedUser.roleHead=='Admin'){
    let url = this.baseurl + '/employeeListWhoHasAuthority/';
    this.httpService.get(url + this.compId+'/'+this.locoid).subscribe(data => {
      this.authorities=data["result"];
      console.log("authdatacontreactor$$",this.authorities);
    }, (err: HttpErrorResponse) => {
    });
  
  }
  }
  getContractorSupervisor(contractorId){
    if(contractorId){
      this.dataservice.getRecordsByid(this.contractor_supervisor_url,contractorId).subscribe(res=>{
        if(res.statusCode==200){
          this.contractor_supervisors=res.result;
        }
      })
    }
  }
  getDepartments(){
    if(this.compId){
      this.dataservice.getRecordsByid(this.dept_data_url,this.compId).subscribe(res=>{
        if(res.statusCode==200){
          this.DepartmentAllData=res.result;
        }
      })
    }
  }
  getSubDepartments(){
    let dept_id=this.userForm.get('deptId').value;
    if(dept_id){
      this.dataservice.getRecordsByid(this.sub_dept_url,dept_id).subscribe(res=>{
        if(res.statusCode==200){
          this.SubDepartmentData=res.result;
        }
      })
    }
  }
  locationGRoup(e) {
  
    // this.selectedLocationId=e;
    if(this.loggedUser.roleHead=='HR'){
      let url4 = this.baseurl + '/LocationWise/';
      this.httpService.get(url4 + this.loggedUser.locationId).subscribe(
      data => {
      this.LocationAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
    }
    else if(this.loggedUser.roleHead=='Admin'){
  let url4 = this.baseurl + '/Location/Active/';
  this.httpService.get(url4 +this.compId).subscribe(
  data => {
  this.LocationAllData = data["result"];
  console.log("adminlocation****",this.LocationAllData);
  }, (err: HttpErrorResponse) => {
  }
  );
  }
  }
  Locationdata(event) {
   
    this.selectedLocationobj = parseInt(event.target.value);
    console.log("locationid",this.selectedLocationobj);
    this.locoid = this.selectedLocationobj;
 
    console.log("-----===== slectedlocationobjinupadte",this.locoid);
    this.weeklyCalendarId = parseInt(event.target.value);
   
    
    sessionStorage.setItem("weeklyCalendarId",this.weeklyCalendarId)
    // this.contractorSerTypevalue(this.selectedcontractorserviceobj)
    this.getcontractNamesData(); 
    this.getshiftGroup();
    this.weeklyoffdata();
    this.getAuthorities();
    this.shiftmastergroup();
     this.contractNamesData={};
   
   
  }
  getDesignations(){
    if(this.compId){
      this.dataservice.getRecordsByid(this.designation_url,this.compId).subscribe(res=>{
        if(res.statusCode==200){
          this.DesignationAllData=res.result;
        }
      })
    }
  }
  contractorfirmnamedata(event){
    this.selectedfirmnameobj=parseInt(event.target.value);
    console.log("firmnamebind",  this.selectedfirmnameobj);
  }
  
  contractorSerType() {
    let url = this.baseurl + '/CosSerType/Active/';
    this.httpService.get(url + this.compId).subscribe(
    (data :  any) => {
        this.contractServiceData = data["result"];
        console.log("contractor service type",  this.contractServiceData );
      }, (err: HttpErrorResponse) => {
      }
    );
  }

  getshiftGroup() {
    if(this.loggedUser.roleHead=='HR'){
      let url = this.baseurl + '/ShiftGroup/Active/';
      this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(
      data => {
      this.Shiftgroupdata = data["result"];
      console.log("shiftdata***",this.Shiftgroupdata);
      }, (err: HttpErrorResponse) => {
        

      });
        }
        else if(this.loggedUser.roleHead=='Admin'){
          let url = this.baseurl + '/ShiftGroup/Active/';
      this.httpService.get(url + this.compId+'/'+ this.locoid).subscribe(
      data => {
      this.Shiftgroupdata = data["result"];
      console.log("shiftdata***",this.Shiftgroupdata);
      }, (err: HttpErrorResponse) => {
       
      });
      
      
        }
      
    }
    shiftmastergroup() {

      if(this.loggedUser.roleHead=='HR'){
        let url = this.baseurl + '/ShiftMaster/ByActive/';
        this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
            this.Shiftmastergroupdata = data["result"];
            console.log(" Shiftgroupdata", this.Shiftgroupdata)
          }, (err: HttpErrorResponse) => {
            
    
          });
          
        
        }    
        else if(this.loggedUser.roleHead=='Admin'){
          console.log("locationid",this.locoid);
           
            let url = this.baseurl + '/ShiftMaster/ByActive/';
            this.httpService.get(url + this.compId+'/'+this.locoid).subscribe(data => {
                this.Shiftmastergroupdata = data["result"];
               console.log(" Shiftgroupdatagenral", this.Shiftgroupdata)
       }, (err: HttpErrorResponse) => {
            
    
          });
       
        }
    }
    shiftgroupmastervalue(event) {
    this.selectedshiftmastergroupobj = parseInt(event.target.value)
    }
      contractorSerTypevalue(event){
        


        this.selectedcontractorserviceobj= parseInt(event.target.value);
        this.getcontractNamesData();
        console.log("consorid***",this.selectedcontractorserviceobj)
        
      }
      getcontractNamesData() {
   
    {   
      if(this.loggedUser.roleHead=='HR'){
        let consertypeId=this.userForm.get('conSerTypeId').value;
         let url = this.baseurl + '/Contractor/';
         this.httpService.get(url +consertypeId +'/'+ this.loggedUser.locationId+'/'+this.compId).subscribe(
          (data :  any) => {
          this.contractNamesData = data["result"];
          this.userForm.controls['contractorId'].setValue(this.employee_data.contractorId)
          console.log("firmnames",this.contractNamesData )
          }, (err: HttpErrorResponse) => {
            this.contractNamesData='';
          }
          );
      
      }
    else if(this.loggedUser.roleHead=='Admin'){
 
    
     
        let consertypeId=this.userForm.get('conSerTypeId').value;
         let url = this.baseurl + '/Contractor/';
         this.httpService.get(url +consertypeId +'/'+this.locoid+'/'+this.compId).subscribe(
          (data :  any) => {
          this.contractNamesData = data["result"];
          this.userForm.controls['contractorId'].setValue(this.employee_data.contractorId)
          console.log("firmnames",this.contractNamesData )
          }, (err: HttpErrorResponse) => {
            this.contractNamesData='';
          }
          );
      
      }

    }

    }
      
  
    weeklyoffdata() {
      if(this.loggedUser.roleHead=='HR'){
        let compId = this.compId;
        this.WeeklyOffcalendarData = [];
        let url1 = this.baseurl + '/weeklyOfMasterActive/';
        this.httpService.get(url1 + compId+'/'+this.loggedUser.locationId).subscribe(
        data => {
        this.WeeklyOffcalendarData = data["result"];
        }, (err: HttpErrorResponse) => {
        });
          }
          else if(this.loggedUser.roleHead=='Admin'){
            let compId = this.compId;
        this.WeeklyOffcalendarData = [];
        let url1 = this.baseurl + '/weeklyOfMasterActive/';
        console.log("weeklyoffdataid", this.locoid);
        this.httpService.get(url1 + compId+'/'+ this.locoid).subscribe(
        data => {
        this.WeeklyOffcalendarData = data["result"];
        }, (err: HttpErrorResponse) => {
        });
        
        
          }
      }
//pan image
onPanFileAdded(file: FilePreviewModel){
  const img_type=file.file.type;
  this.dataservice.uploadFile(file).subscribe(data => {
    if(data.result){
      let temp={
        "docName" : "pancard",
        "docType" : img_type,
        "docUrl"  : data['result'].imageUrl
      }
      this.userForm.value.empDocMasterList[0]=temp;
      this.panURL=data.result.imageUrl;
    }
  })
}
//onPanRemoveSuccess
onPanRemoveSuccess(file : FilePreviewModel){
    this.userForm.value.empDocMasterList[0]='';
    this.panURL='';
}
onAadharRemoveSuccess(file : FilePreviewModel){
  // this.userForm.value.empDocMasterList[1]=null; 
  this.userForm.value.empDocMasterList[1]='';
  this.AadharURL='';
}
onEduFileAdded(file : FilePreviewModel){
  const img_type=file.file.type;
  this.dataservice.uploadFile(file).subscribe(data => {
    if(data.result){
      let temp={
        "docName" : "eduCertificate",
        "docType" : img_type,
        "docUrl"  : data['result'].imageUrl
      }
      this.userForm.value.empDocMasterList[2]=temp;
      this.EduURL=data.result.imageUrl;
    }
  })
}
onEduRemoveSuccess(file : FilePreviewModel){
  // this.userForm.value.empDocMasterList[2]=null; 
  this.userForm.value.empDocMasterList[2]='';
  this.EduURL='';
}
setBirthDate(event){
  
  let temp=event.target.value;
  this.userForm.controls.dateOfBirth.setValue(temp);
}
handleFileInputs(file: FileList) {
  // this.Spinner.show()
  this.fileToUpload = file.item(0);
  this.obje.name = file.item(0).name; 
  //Show image preview
  var reader = new FileReader();
  reader.onload = (event: any) => {
    this.employee_data.empimageURl= this.empimageURl = event.target.result;
    this.newImageFlag=true
    this.receiptUrl=event.target.result
   //  this.Spinner.hide();

    
  }
  reader.readAsDataURL(this.fileToUpload);
}

onUpdateContractor(){
  

  this.userForm.value.secondLevelReportingManager=0;
  this.userForm.value.empPerId=this.employee_id;
  this.userForm.value.offerLetterAndJoiningSatutoryDataId=this.employee_data.offerLetterAndJoiningSatutoryDataId;
  this.userForm.value.govDocInfoId=this.employee_data.govDocInfoId;
  this.userForm.value.atdSettingId=this.employee_data.atdSettingId;
  this.userForm.value.empOfficialId=this.employee_data.empOfficialId;
  this.userForm.value.empAddressId=this.employee_data.empAddressId;
  this.userForm.value.eduQualificationId=this.employee_data.eduQualificationId;
  this.userForm.value.empPhysicalInfoid=this.employee_data.empPhysicalInfoid;
  this.userForm.value.empimageURl=this.employee_data.empimageURl;
  this.userForm.value.empEmergencyContactPersonDetailsId=this.employee_data.empEmergencyContactPersonDetailsId;
  this.userForm.value.selectedLocationobj=this.locoid;
  console.log("slectedlocationobjinupadte",this.locoid);
    console.log("updatedvalues",this.userForm.value);
    this.dataservice.updateRecords(this.update_url,this.userForm.value).subscribe(res=>{
      if(res.statusCode==200){
        this.toastr.success("Contractor Updated Sucessfully !");
        setTimeout(() => {
           this.router.navigateByUrl('/layout/employeelist/employeelist')
           .then(()=>{
            window.location.reload();
           });
       }, 1000);
      }
  },(err) => {
    this.toastr.error('Failed to Update Employee...');
  });
  
 
 
}
//Cancel
Cancel(){
  this.router.navigateByUrl('/layout/employeelist/employeelist')
  .then(()=>{
    window.location.reload();
  });
}

empType() {
  let urlempType = this.baseurl + '/EmployeeType/Active/';
  this.httpService.get(urlempType + this.compId).subscribe(data => {
      this.empTypeAllData = data["result"];
      console.log("empdata",   this.empTypeAllData );
    }, (err: HttpErrorResponse) => {
    });
}
selectEmptype(emptypeId) {
  
  this.selectedEmpTypeobj = emptypeId;
  this.userForm.value.empTypeId= this.selectedEmpTypeobj;
  this.subEmpType(this.selectedEmpTypeobj)
}
subEmpType(selectedValue) {
  let urlsubempType = this.baseurl + '/SubEmployeeType/Active/';
  this.httpService.get(urlsubempType + selectedValue).subscribe(data => {
      this.subempTypeAllData = data["result"];
    }, (err: HttpErrorResponse) => {
    });
}

selectsubEmptype(event) {
  this.selectedSubEmpTypeobj = parseInt(event.target.value);
  this.userForm.value.subEmpTypeId=this.selectedSubEmpTypeobj;
 
}

  
}