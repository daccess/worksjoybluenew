import { Routes, RouterModule } from '@angular/router'
import { AttendancemenuComponent } from './attendancemenu.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: AttendancemenuComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'attendancemenu',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'attendancemenu',
                    component: AttendancemenuComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);