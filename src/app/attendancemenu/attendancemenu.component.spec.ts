import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendancemenuComponent } from './attendancemenu.component';

describe('AttendancemenuComponent', () => {
  let component: AttendancemenuComponent;
  let fixture: ComponentFixture<AttendancemenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendancemenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendancemenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
