import { routing } from './attendancemenu.routing';
import { NgModule } from '@angular/core';
import { AttendancemenuComponent } from './attendancemenu.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        AttendancemenuComponent
    ],
    imports: [
    routing,
    CommonModule
    ],
    providers: []
  })
  export class  AttendancemenuModule { 
      constructor(){

      }
  }
  