import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attendancemenu',
  templateUrl: './attendancemenu.component.html',
  styleUrls: ['./attendancemenu.component.css']
})
export class AttendancemenuComponent implements OnInit {
  loggedUser: any = {};

  constructor() { }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
  }

}
