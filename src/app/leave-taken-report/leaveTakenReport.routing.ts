import { Routes, RouterModule } from '@angular/router';
import { LeaveTakenReportComponent } from './leave-taken-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: LeaveTakenReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leaveTakenReport',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaveTakenReport',
                    component: LeaveTakenReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);