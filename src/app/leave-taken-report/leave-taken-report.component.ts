import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-leave-taken-report',
  templateUrl: './leave-taken-report.component.html',
  styleUrls: ['./leave-taken-report.component.css'],
  providers : [DatePipe]
})
export class LeaveTakenReportComponent implements OnInit {
  fromDate: any;
  toDate:  any;
  AllReportData: any;
  AllReport = [];
  totalLeaveList = [];
  dummyHeaders= [];
  paidList: any;
  leaveName: any;
  leaveType: any;
  count: any;

  causelLeave:any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;

  constructor(private datePipe: DatePipe) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    this.AllReportData = sessionStorage.getItem('leaveTakenReport');
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.AllReport = JSON.parse(this.AllReportData);
    this.totalLeaveList = this.AllReport[0].leaveList;
    this.totalLeaveList = this.AllReport[0].leaveList;
  if(this.totalLeaveList){
      for (let i = 0; i < (this.totalLeaveList.length * 3); i++) {
        if(i%3 == 0){
          this.dummyHeaders.push("");
          this.dummyHeaders.push("");
          this.dummyHeaders.push("");
        } 
        else{
          this.dummyHeaders.push("");
          this.dummyHeaders.push("");
          this.dummyHeaders.push("");

        }
    }
   
  }

    for (let i = 0; i < this.AllReport.length; i++) {
      let obj = new Array();
      let data = {
        leaveName : "",
        leaveType : "",           
        count:""
       };
       let data1 = {
        leaveName : "",
        leaveType : "",           
        count:"",
      };
    }
    for (let index = 0; index < this.AllReport.length; index++) {
      this.paidList = this.AllReport[index].leaveTakenReportInCountResDtos;
      for (let index = 0; index < this.paidList.length*2; index++) {
        this.leaveName = this.paidList[0];
        this.leaveType= this.paidList[1];
        this.count = this.paidList[2];
      }
      
    }
   
  } 
  ngOnInit() {
  }
}
