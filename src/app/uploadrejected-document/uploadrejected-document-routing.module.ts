import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadrejectedDocumentComponent } from './uploadrejected-document.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: UploadrejectedDocumentComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'uploadDocumet',
                  pathMatch :'full'
                  
              },
              {
                  path:'uploadDocumet',
                  component: UploadrejectedDocumentComponent
              }

          ]

  }


]
export const uploadRouting= RouterModule.forChild(appRoutes);
