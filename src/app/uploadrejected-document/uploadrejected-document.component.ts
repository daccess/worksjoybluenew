import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { RoleMasterService } from '../shared/services/RoleMasterService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-uploadrejected-document',
  templateUrl: './uploadrejected-document.component.html',
  styleUrls: ['./uploadrejected-document.component.css']
})
export class UploadrejectedDocumentComponent implements OnInit {
  
  themeColor: string = "nav-pills-blue";
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  Alllabours: any;
  empid: any;
  docIds:any;
  roleNames: any;
  supervisorEmpIds: any;
  constructor(private httpSer:HttpClient,public Spinner :NgxSpinnerService,private toastr:ToastrService,private roleservice:RoleMasterService,private router:Router) { }

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token;
    this.empid=this.users.roleList.empId;
    this.roleNames=this.users.roleList.roleName;
    this.supervisorEmpIds=this.users.roleList.contrctorEmpId
    this.getAllDocumentLabourHistory()
  }
  getAllDocumentLabourHistory() {

    let url = this.baseurl + `/getLabourDocumentRejectList?empId=${this.empid}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url, { headers }).subscribe(data => {
      this.Alllabours= data['result']
      setTimeout(function () {

        $(function () {

          var table = $('#documentHistorylist').DataTable({

            retrieve: true,
            searching:false

         

          })

        });
      }, 100);
    
    },
      (err: HttpErrorResponse) => {

      })
      if(this.roleNames=='Supervisor'){
        let url = this.baseurl + `/getLabourDocumentRejectList?empId=${this.supervisorEmpIds}`;
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpSer.get(url, { headers }).subscribe(data => {
          this.Alllabours= data['result']
          setTimeout(function () {
    
            $(function () {
    
              var table = $('#documentHistorylist').DataTable({
    
                retrieve: true,
                searching:false
    
             
    
              })
    
            });
          }, 100);
        
        },
          (err: HttpErrorResponse) => {
    
          })
      }

  }
  gotoDocumentUploadSection(e:any){
this.docIds=e.docId;
sessionStorage.setItem('docId',this.docIds)
this.router.navigateByUrl('/layout/uploadDocument');
  }

}
