import { UploadrejectedDocumentModule } from './uploadrejected-document.module';

describe('UploadrejectedDocumentModule', () => {
  let uploadrejectedDocumentModule: UploadrejectedDocumentModule;

  beforeEach(() => {
    uploadrejectedDocumentModule = new UploadrejectedDocumentModule();
  });

  it('should create an instance', () => {
    expect(uploadrejectedDocumentModule).toBeTruthy();
  });
});
