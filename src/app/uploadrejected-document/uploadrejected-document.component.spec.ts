import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadrejectedDocumentComponent } from './uploadrejected-document.component';

describe('UploadrejectedDocumentComponent', () => {
  let component: UploadrejectedDocumentComponent;
  let fixture: ComponentFixture<UploadrejectedDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadrejectedDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadrejectedDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
