import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { uploadRouting } from './uploadrejected-document-routing.module';
import { UploadrejectedDocumentComponent } from './uploadrejected-document.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    uploadRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    UploadrejectedDocumentComponent
  ]
})
export class UploadrejectedDocumentModule { }
