import { Routes, RouterModule } from '@angular/router'
import { GatepassComponent } from './gatepass.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: GatepassComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'gatepass',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'gatepass',
                    component: GatepassComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);