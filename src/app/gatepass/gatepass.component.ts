import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { SessionService } from 'src/app/shared/services/SessionSrvice';
import { NgxSpinnerService } from 'ngx-spinner';
import { GatepassList } from '../shared/model/gatepass';
import { InfoService } from "../shared/services/infoService";
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { DomSanitizer} from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HtmlParser } from '@angular/compiler';
import * as htmlToImage from 'html-to-image';

@Component({
  selector: 'app-gatepass',
  templateUrl: './gatepass.component.html',
  styleUrls: ['./gatepass.component.css']
})
export class GatepassComponent implements OnInit {
public QrCodeNumber=null;
  gatepassModel: GatepassList
  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  gateData: any;
  empPerId: any;
  loggedUser: any = {};

  fullName: any;
  temp_profilePath: any;
  profilePath: any;
  empGatePassId: any;
  conFirmName: any;
  personalContactNumber: any;
  emrContactNumber: any;
  bloodGroup: any;
  pfNumber: any;
  esicNumber: any;
  validStartDate: any;
  validEndDate: any;
  vendorCode: any;
  empOfficialId: number;
  fileName: string;
  filePreview: string;
  base64Image: string;
  fatheName:any;
  deptName:any;
  received_message:any;
  profile_img;
  employee_id: any;
  empdata: any;
  width:any;
  imageurls: any;
 
  constructor(private sanitizer:DomSanitizer,public httpService: HttpClient,public chRef: ChangeDetectorRef,public SessionSer: SessionService,public Spinner: NgxSpinnerService,private InfoService:  InfoService, private router: Router) {
    this.gatepassModel = new GatepassList()
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.empOfficialId = parseInt (sessionStorage.getItem('a'));
  }

  ngOnInit() {
    this.getPassData(this.empOfficialId);
    //get data
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.received_message = message;
        }
      });
    //set return data
    
    this.InfoService.changeMessage(this.received_message);
    let employee_details = sessionStorage.getItem('employeeId');
    this.employee_id=employee_details;
console.log(this.employee_id)
    this.getpassdatabyId();
  }
getpassdatabyId(){
  
  let url=this.baseurl+'/getContractorByOfficialId/';
  this.httpService.get(url+this.employee_id).subscribe(data=>{
    this.empdata=data['result'];
    
     this.QrCodeNumber=this.empdata.qrCode
     this.imageurls=this.empdata.empimageURl
console.log("this is the sessiondata",this.empdata)
  },(err: HttpErrorResponse) => {
  })
}
  getPassData(empOfficial) {
    let url1 = this.baseurl + '/gatepassofcontractor/';
    this.httpService.get(url1+empOfficial).subscribe((data: any) => {
        this.gateData = data.result;
        this.fullName = this.gateData.fullName;
        if(this.Aws_flag!='true'){
          this.profilePath=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.gateData.profilePath);
        }else{
          this.profilePath = this.gateData.profilePath;
        }
       
        this.empGatePassId = this.gateData.empGatePassId;
        this.conFirmName = this.gateData.conFirmName;
        this.personalContactNumber = this.gateData.personalContactNumber;
        this.emrContactNumber = this.gateData.emrContactNumber;
        this.bloodGroup = this.gateData.bloodGroup;
        this.pfNumber = this.gateData.pfNumber;
        this.esicNumber = this.gateData.esicNumber;
        this.validStartDate = this.gateData.validStartDate;
        this.validEndDate = this.gateData.validEndDate;
        this.vendorCode = this.gateData.vendorCode;
        this.fatheName=this.gateData.fatheName;
        this.deptName=this.gateData.deptName;
      },
      (err: HttpErrorResponse) => {
      }
    );
  }
  toDataURL(url, callback) {
    let xhRequest = new XMLHttpRequest();
    xhRequest.onload = function () {
      let reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhRequest.response);
    };
    xhRequest.open('GET', url);
    xhRequest.responseType = 'blob';
    xhRequest.send();
  }
 
  htmltoPDF() { 
    // this.getPDF();
   
    // html2canvas(document.querySelector("#gatepass"),{ allowTaint: true,useCORS: true}).then(canvas => {
    //     const contentDataURL = canvas.toDataURL('image/png');
    //     let pdf = new jsPDF('p', 'cm', 'a4'); //Generates PDF in portrait mode
    //     var imgWidth = 229;
    //     var imgHeight = 147;
    //     pdf.addImage(contentDataURL, 'PNG', 0, 0, imgWidth, imgHeight);  
    //     pdf.save('gate Pass.pdf'); 

    // });
     
      var printContents = document.getElementById('gatepass').innerHTML;
      var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
      
     
      
      
    }
    htmltoPDFs(el){
      var restorepage = $('body').html();
      var printcontent = $('#' + el).clone();
      $('body').empty().html(printcontent as any);
      window.print();
      $('body').html(restorepage);
      }
      generateImage(){
        // var node:any = document.getElementById('gatepass');
        // htmlToImage.toPng(node)
        //   .then(function (dataUrl) {
        //     var img = new Image();
        //     img.src = dataUrl;
        //     document.body.appendChild(img);
        //   })
        //   .catch(function (error) {
        //     console.error('oops, something went wrong!', error);
        //   });
        htmlToImage.toJpeg(document.getElementById('gatepass') as HTMLElement, { quality: 0.95 })
  .then(function (dataUrl) {
    var link = document.createElement('a');
    link.download = 'Gate-Pass.jpeg';
    link.href = dataUrl;
    link.click();
  });
}
      
      //  this.router.navigateByUrl('layout/gatepass/gatepass');
      
  
  
  // getPDFs(){
  //   html2canvas(document.getElementById('testdiv')).then(function(canvas){
  //     var wid: number
  //     var hgt: number
  //     var img = canvas.toDataURL("image/png", wid = canvas.width, );
  //     var hratio = hgt/wid
  //     var doc = new jsPDF('p','pt','a4');
  //     var width = doc.internal.pageSize.width;    
  //     var height = width * hratio
  //     doc.addImage(img,'JPEG',20,20, width, height);
  //     doc.save('Test.pdf');
  // });
  // }
  Cancel(){
    //window.history.back();
      setTimeout(() => {
      this.router.navigateByUrl('/layout/employeelist/employeelist')
      .then(()=>{
      window.location.reload();
      });
   }, 100);
  }
  }
