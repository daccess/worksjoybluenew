import { routing } from './gatepass.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import{CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { GatepassComponent } from './gatepass.component';


@NgModule({
    declarations: [
        GatepassComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: []
  })
  export class  GatepassModule { 
      constructor(){

      }
  }
  