import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { labourListRouting } from './contractor-labour-list-routing.module';
import { ContractorLabourListComponent } from './contractor-labour-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    labourListRouting,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ContractorLabourListComponent
  ]
})
export class ContractorLabourListModule { }
