import { ContractorLabourListModule } from './contractor-labour-list.module';

describe('ContractorLabourListModule', () => {
  let contractorLabourListModule: ContractorLabourListModule;

  beforeEach(() => {
    contractorLabourListModule = new ContractorLabourListModule();
  });

  it('should create an instance', () => {
    expect(contractorLabourListModule).toBeTruthy();
  });
});
