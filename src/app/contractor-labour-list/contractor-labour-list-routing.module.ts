import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorLabourListComponent } from './contractor-labour-list.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorLabourListComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorLabourList',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorLabourList',
                  component: ContractorLabourListComponent
              }

          ]

  }


]
export const labourListRouting = RouterModule.forChild(appRoutes);
