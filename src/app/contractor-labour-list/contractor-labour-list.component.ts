import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-contractor-labour-list',
  templateUrl: './contractor-labour-list.component.html',
  styleUrls: ['./contractor-labour-list.component.css']
})
export class ContractorLabourListComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  LabourData: any;
  labourData: any;
  image:any;
  labourIds: any;
  labourlistEdit: string;
  locationdata: any;
  locationId: any;
  empids: any;
  searchLabour:any;
  selectUndefinedOptionValue:any;
  searchByLabour:any;
  searchByColumn:any;
  labourId: any;
  roleNames: string;
  supervisorEmpIds: any;

  constructor(public httpservice:HttpClient,public router:Router,public Spinner:NgxSpinnerService,public toastr: ToastrService) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId
    this.roleNames=this.users.roleList.roleName;
    this.supervisorEmpIds=this.users.roleList.contrctorEmpId
    this.getLabourData();
  
  }
  // getlocationData(){
  //   let url = this.baseurl + '/getAllLocations';
  // const tokens = this.token;
  // const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.get(url,{headers}).subscribe(data => {
    
  //     this.locationdata = data["result"];
  //       this.locationId=this.locationdata[0].locationId
  //       this.getLabourData();
  // },
  //   (err: HttpErrorResponse) => {
  
  //   })
  
  //     }

  getLabourData() {
    
    let url = this.baseurl+`/getLabourInfo?empId=${this.empids}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.LabourData = data['result']
      setTimeout(function () {

        $(function () {

          var table = $('#emptable').DataTable({

            retrieve: true,
            searching:false

         

          })

          // table
          // .order( [ 1, 'des' ] )
          // .draw();
          // $('.status-dropdown').on('change', function (e) {
          //   var status = $(this).val();
          //   $('.status-dropdown').val(status)
          //   console.log(status)
          //   table.column(6).search(status as AssignType).draw();
          // })


        });
      }, 100);
      this.Spinner.hide();
  
      // this.count1=this.manpowerAllData.length

    },
      // this.labourData=JSON.stringify(this.LabourData )
      // console.log("dsfasdfdsfsdfsdfsdafasdfsdaf",this.LabourData)
  
      (err: HttpErrorResponse) => {

      })
if(this.roleNames=='Supervisor'){
  let url = this.baseurl+`/getLabourInfo?empId=${this.supervisorEmpIds}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.get(url, { headers }).subscribe(data => {
    
    this.LabourData = data['result']
    setTimeout(function () {

      $(function () {

        var table = $('#emptable').DataTable({

          retrieve: true,
          searching:false

        })

       
      });
    }, 100);
    this.Spinner.hide();
  },
    (err: HttpErrorResponse) => {

    })
}
  }
  
  editLabour(e:any){
    
    this.labourlistEdit='true'
   this.labourIds= e.labourPerId
   
    sessionStorage.setItem("labourId",this.labourIds)
    sessionStorage.setItem("iseditLabour",'true')
    this.router.navigateByUrl('/layout/contractorLabourAdd');


  }
  deleteLabour(e:any){
    
    this.labourId=e.labourPerId
      let url = this.baseurl+`/deleteLabourInfoById/${this.labourId}`;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.delete(url, { headers }).subscribe(data => {
        this.toastr.success("Labour delete successfully")
        this.getLabourData()
      },(err: HttpErrorResponse) => {
        this.toastr.error("error while delete labour")
  })
  
    }
  }
