import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorLabourListComponent } from './contractor-labour-list.component';

describe('ContractorLabourListComponent', () => {
  let component: ContractorLabourListComponent;
  let fixture: ComponentFixture<ContractorLabourListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorLabourListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorLabourListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
