import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { safetylabourrouting } from './safety-labour-done-list-routing.module';
import { SafetyLabourDoneListComponent } from './safety-labour-done-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    safetylabourrouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SafetyLabourDoneListComponent
  ]
})
export class SafetyLabourDoneListModule { }
