import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SafetyLabourDoneListComponent } from './safety-labour-done-list.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: SafetyLabourDoneListComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'safetylabourDoneList',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'safetylabourDoneList',
                  component: SafetyLabourDoneListComponent
              }

          ]

  }


]

export const safetylabourrouting = RouterModule.forChild(appRoutes);
