import { SafetyLabourDoneListModule } from './safety-labour-done-list.module';

describe('SafetyLabourDoneListModule', () => {
  let safetyLabourDoneListModule: SafetyLabourDoneListModule;

  beforeEach(() => {
    safetyLabourDoneListModule = new SafetyLabourDoneListModule();
  });

  it('should create an instance', () => {
    expect(safetyLabourDoneListModule).toBeTruthy();
  });
});
