import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-safety-labour-done-list',
  templateUrl: './safety-labour-done-list.component.html',
  styleUrls: ['./safety-labour-done-list.component.css']
})
export class SafetyLabourDoneListComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  allmanpowerLabourDatas: any;
  locationId: any;
  locationdata: any;
  labourId: any;
  manReqStatusIde: any;
  documentids: any;
  manReqStatusIds: any;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) { 
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId;
    this.locationId=this.users.roleList.locationId
  }

  ngOnInit() {
    this.getlocationData()
   
  }
  getlocationData() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.locationdata = data["result"];
      this.locationId = this.locationId;
      
      this.getallLabourForFrontDesk()
    },
      (err: HttpErrorResponse) => {

      })

  }
  getallLabourForFrontDesk() {
    let url = this.baseurl + `/getFrontDeskPassLabourList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.allmanpowerLabourDatas = data["result"];
      setTimeout(function () {

        $(function () {

          var table = $('#emptable').DataTable({

            retrieve: true,
            searching:false

         

          })

          // table
          // .order( [ 1, 'des' ] )
          // .draw();
          // $('.status-dropdown').on('change', function (e) {
          //   var status = $(this).val();
          //   $('.status-dropdown').val(status)
          //   console.log(status)
          //   table.column(6).search(status as AssignType).draw();
          // })


        });
      }, 100);
      this.Spinner.hide();
  
      // this.count1=this.manpowerAllData.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  // gotoDocument1(e:any){
    
  //   // this.documentids=e.requestStatus.labourInfo.labourPerId;
  //   // this.manReqStatusIds=e.requestStatus.manReqStatusId
  //   // sessionStorage.setItem("documentIds",this.documentids)
  //   // sessionStorage.setItem("manReqStatusId",this.manReqStatusIds)
  //   this.labourId=e.requestStatus.labourInfo.labourPerId;
  //   this.manReqStatusIde=e.requestStatus.manReqStatusId;
  //   sessionStorage.setItem("manrequestIds",this.manReqStatusIde)
  //   sessionStorage.setItem("LabourPerIds",this.labourId)
  //   sessionStorage.setItem('IseditFlag', 'true')
  //   this.router.navigate(['/layout/frontDeskAssets'])
    
  // }
  // gotoDocument(e:any){
  //   this.labourId=e.requestStatus.labourInfo.labourPerId;
  //   this.manReqStatusIde=e.requestStatus.manReqStatusId;
  //   sessionStorage.setItem("manRequestIds",this.manReqStatusIde)
  //   sessionStorage.setItem("LabourPerIdss",this.labourId)
  //   this.router.navigate(['/layout/originalPass'])
    
  // }

}
