import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyLabourDoneListComponent } from './safety-labour-done-list.component';

describe('SafetyLabourDoneListComponent', () => {
  let component: SafetyLabourDoneListComponent;
  let fixture: ComponentFixture<SafetyLabourDoneListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyLabourDoneListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyLabourDoneListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
