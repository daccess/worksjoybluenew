import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InfoService } from "../shared/services/infoService";
import * as jsPDF from 'jspdf';
import * as moment from 'moment';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-musterrerport',
  templateUrl: './musterrerport.component.html',
  styleUrls: ['./musterrerport.component.css'],
  providers:[DatePipe]
})
export class MusterrerportComponent implements OnInit {
  fromDate: string;
  toDate: string;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  AllReport: any;
  report_time: any;
  tablehead: any;
  table_length: any;
  muster_data: any[];

  constructor(private InfoService:InfoService,private router: Router,private datePipe: DatePipe) {
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    let getTableHead = sessionStorage.getItem('getTableheadMuster');
    this.tablehead = JSON.parse(getTableHead);
    this.table_length=this.tablehead.length+6;
    
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.AllReport = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
   }
  temp_array=[];
  ngOnInit() {
    for(let i=0;i<this.AllReport.length;i++){
      if(this.AllReport[i].musterReportDataDetailsResDtoList && this.AllReport[i].reportEmpNameAndIdResDto){
          this.temp_array.push(this.AllReport[i]);
      }
    }
    //empty
    for(let i=0; i<this.temp_array.length; i++){
      if(this.temp_array[i].musterReportDataDetailsResDtoList && this.temp_array[i].musterReportDataDetailsResDtoList.length!=0){
         //test if empty
         if(this.temp_array[i].musterReportDataDetailsResDtoList[0].attenDate){
          let temp= this.temp_array[i].musterReportDataDetailsResDtoList[0].attenDate.match(/[^\s-]+-?/g);
          let parsed_date=parseInt(temp[2]);
          let temp_start_date=this.fromDate.match(/[^\s-]+-?/g);
          let parsed_start_date=parseInt(temp_start_date[2]);
          if(parsed_date!=parsed_start_date){
            for(let k=1;k<parsed_date;k++){
                let temp_date={
                  "attenDate": "",
                  "attendanceId":"",
                  "attendanceStatus": "",
                  "checkIn": "",
                  "checkOut": "",
                  "coffStatus": "",
                  "empOfficialId": "",
                  "lateEarlyStatus": "",
                  "leaveStatus": "",
                  "odStatus": "",
                  "shiftName": "",
                  "statusOfDay": "",
                  "wfhStatus": "",
                  "workDuration": ""
              }
              // this.setemptydata();
              this.temp_array[i].musterReportDataDetailsResDtoList.unshift(temp_date);
            }
            
          }
        }
     }
    }
    this.muster_data=this.temp_array;
    console.log(this.muster_data);
  }
  exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename='Muster-Report'+moment().format('DD-MM-YYYY h:mm:ss a');
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a3');
    doc.autoTable({
        html: '#employee_muster',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(0, 0, 0);
        doc.setFontSize(7);
        doc.setFontStyle("Arial");
        doc.text(13, 15, 'Abbreviation:-  HF : Half day, WO: Weekly Off, HO: Holiday , CL : Casual Leave , SL : Sick Leave , PL : Previlege Leave , OPF : Out Punch Forgot ');
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(270, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      doc.save('Muster-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a')+'.pdf');
    // doc.save('Muster-Report'+this.report_time+'.pdf');  
  }
}
