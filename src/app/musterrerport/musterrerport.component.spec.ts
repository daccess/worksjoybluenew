import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusterrerportComponent } from './musterrerport.component';

describe('MusterrerportComponent', () => {
  let component: MusterrerportComponent;
  let fixture: ComponentFixture<MusterrerportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusterrerportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusterrerportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
