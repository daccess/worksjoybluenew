import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MusterrerportComponent } from './musterrerport.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path: '', component: MusterrerportComponent,
    children:[
      {
        path:'',
        redirectTo : 'musterreports',
        pathMatch :'full'
        
      }]
  }
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  providers: [DatePipe],
  declarations: [MusterrerportComponent]
})
export class MusterrerportModule { }
