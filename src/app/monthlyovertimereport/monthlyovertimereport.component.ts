import { Component, OnInit } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
@Component({
  selector: 'app-monthlyovertimereport',
  templateUrl: './monthlyovertimereport.component.html',
  styleUrls: ['./monthlyovertimereport.component.css'],
})
export class MonthlyovertimereportComponent implements OnInit {
  AllReportData:any;
  AllMonthlyReport: any;
  head=false;
  tablehead =[];
  fromDate: any;
  toDate: any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  temp_array: any[];
  report_data: any[];
  report_time: string;
  table_length: number;
  constructor(private datePipe: DatePipe,private InfoService:InfoService,private router: Router) { 
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
     this.todaysDate = new Date();
     this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
     this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.AllMonthlyReport = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    let getTableHead = sessionStorage.getItem('getTablehead');
    this.tablehead = JSON.parse(getTableHead);
    this.table_length=this.tablehead.length+6;
    //modify response
    if(this.AllMonthlyReport){
      this.temp_array=[];
      for(let i=0;i<this.AllMonthlyReport.length;i++){
        if(this.AllMonthlyReport[i].otReportResDtoList.length!=0){
          this.temp_array.push(this.AllMonthlyReport[i]);
        }
      }
    }
    //empty
    for(let i=0; i<this.temp_array.length; i++){
      if(this.temp_array[i].otReportResDtoList){
         //test if empty
         if(this.temp_array[i].otReportResDtoList[0].attenDate){
          let temp= this.temp_array[i].otReportResDtoList[0].attenDate.match(/[^\s-]+-?/g);
          let parsed_date=parseInt(temp[2]);
          let temp_start_date=this.fromDate.match(/[^\s-]+-?/g);
          let parsed_start_date=parseInt(temp_start_date[2]);
          if(parsed_date!=parsed_start_date){
            for(let k=1;k<parsed_date;k++){
                let temp_date={
                  "attenDate": "",
                  "empId":"",
                  "fullName": "",
                  "lastName": "",
                  "deptName": "",
                  "descName": "",
                  "otduration": ""
              }
              this.temp_array[i].otReportResDtoList.unshift(temp_date);
            }
            
          }
        }
        //CalculateTotalOT
        this.CalculateTotalOT(this.temp_array[i].otReportResDtoList);
     }
    }
    this.setemptydata();
    this.report_data=this.temp_array;
  }

  ngOnInit() {
    console.log(this.report_data);
  }
  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Monthly-OverTime-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('l', 'mm', 'a4');
    doc.autoTable({
        html: '#absent_employees',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(0, 0, 0);
        doc.setFontSize(7);
        doc.setFontStyle("Arial");
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(270, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Monthly-OverTime-Report'+this.report_time+'.pdf');  
  }
  getTimeFromMins(mins) {
    if(mins){
      return Math.floor(mins / 60) + ':' + mins % 60;
    }
  }
  setemptydata(){
    for(let i=0;i<this.temp_array.length;i++){
      if(this.temp_array[i].otReportResDtoList.length!=0){
        for(let j=0;j<this.temp_array[i].otReportResDtoList.length;j++){
          if(this.temp_array[i].otReportResDtoList[j].fullName!="" && this.temp_array[i].otReportResDtoList[j].lastName!=""){
            this.temp_array[i].otReportResDtoList[0].fullName=this.temp_array[i].otReportResDtoList[j].fullName;
            this.temp_array[i].otReportResDtoList[0].empId=this.temp_array[i].otReportResDtoList[j].empId;
            this.temp_array[i].otReportResDtoList[0].lastName=this.temp_array[i].otReportResDtoList[j].lastName;
            this.temp_array[i].otReportResDtoList[0].deptName=this.temp_array[i].otReportResDtoList[j].deptName;
            this.temp_array[i].otReportResDtoList[0].descName=this.temp_array[i].otReportResDtoList[j].descName;
          }
        }
      }
    }
  }
  //calculate totalOT
  CalculateTotalOT(element){
    console.log(element);
    let total_ot=0;
    if(element){
      for(let i=0;i<element.length;i++){
        if(element[i].otduration){
            total_ot+=element[i].otduration;
        }
      }
    }
   element.total_ot=total_ot;
   for(let k=0;k<element.length;k++){
     if(element.total_ot){
         element[k].total_ot=element.total_ot;
     }
   }
  }
}
