import { Routes, RouterModule } from '@angular/router'

import { MonthlyovertimereportComponent } from './monthlyovertimereport.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: MonthlyovertimereportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'monthlyovertimereports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'monthlyovertimereports',
                    component: MonthlyovertimereportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);