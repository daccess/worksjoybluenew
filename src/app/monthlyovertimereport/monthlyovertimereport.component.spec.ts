import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyovertimereportComponent } from './monthlyovertimereport.component';

describe('MonthlyovertimereportComponent', () => {
  let component: MonthlyovertimereportComponent;
  let fixture: ComponentFixture<MonthlyovertimereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyovertimereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyovertimereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
