import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn, NgForm } from '@angular/forms';
import { ExcelServiceService } from '../shared/services/excel-service.service';
import { ToastrService } from 'ngx-toastr';
import { excelimport } from '../shared/model/excelimport.model';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'ts-xlsx';
import { employeeexcelimport } from '../shared/model/employeeexclimport';
// import * as df from 'moment';
import * as moment from 'moment';
// import { XlsxToJsonService } from '../xlsx-to-json-service';
@Component({
  selector: 'app-importemployee',
  templateUrl: './importemployee.component.html',
  styleUrls: ['./importemployee.component.css']
})
export class ImportemployeeComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  arrayBuffer: any;
  employeeexcelimport: employeeexcelimport;
  model: any = {};
  finalArray = [];
  myVar: boolean;
  FileName: any;
  FileeName: any;
  selectedFile: File;
  showFileFlag:boolean = false;
  ErrorMessage: any;
  masterData: any;
  moment:any;
  constructor(private formBuilder: FormBuilder, private excelService: ExcelServiceService,
     private toastr: ToastrService, public Spinner: NgxSpinnerService) {
    this.employeeexcelimport = new employeeexcelimport();

    this.form = this.formBuilder.group({

    });
    this.myVar = true;
  }

  ngOnInit() {

  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    this.FileName = event.target.value.replace(/^.*[\\\/]/, '');
    this.showFileFlag = true;

  }
  handleFile() {
    
    const file = this.selectedFile;
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, { type: 'binary', cellDates: true});
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      /* this code for removing rows in excel */
      const range = XLSX.utils.decode_range(workbook.Sheets[first_sheet_name]['!ref']);
     range.s.r = 3; // 0 == XLSX.utils.decode_col("A")
      range.e.c = 39; // 6 == XLSX.utils.decode_col("G")
     const new_range = XLSX.utils.encode_range(range.s, range.e);
      const excelInJSON: any = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name], {raw: true, range: new_range });

      /*Excel data send in object format */
          if (first_sheet_name === 'Employee Master') {

            excelInJSON.filter(someobject => someobject.dateOfBirth)
            .forEach(someobject => someobject.dateOfBirth = moment(someobject.dateOfBirth).format('YYYY/MM/DD'))
            excelInJSON.filter(someobject => someobject.dateOfJoining)
            .forEach(someobject => someobject.dateOfJoining = moment(someobject.dateOfJoining).format('YYYY/MM/DD'))

            excelInJSON.filter(someobject => someobject.adharNumber)
            .forEach(someobject => someobject.adharNumber = String(someobject.adharNumber));
            excelInJSON.filter(someobject => someobject.panNumber)
            .forEach(someobject => someobject.panNumber = String(someobject.panNumber));
            excelInJSON.filter(someobject => someobject.officialContactNumber)
            .forEach(someobject => someobject.officialContactNumber = String(someobject.officialContactNumber));
            excelInJSON.filter(someobject => someobject.employeeId)
            .forEach(someobject => someobject.employeeId = String(someobject.employeeId));
            excelInJSON.filter(someobject => someobject.firstLevelReportingManager)
            .forEach(someobject => someobject.firstLevelReportingManager = String(someobject.firstLevelReportingManager));

      this.employeeexcelimport.importExcelMastersList = excelInJSON;
      console.log(this.employeeexcelimport.importExcelMastersList);
      if (this.employeeexcelimport.importExcelMastersList != null) {
        this.Spinner.show();
        this.excelService.postExcels(this.employeeexcelimport)
          .subscribe(data => {
            this.Spinner.hide();
            this.toastr.success('Excel Import Succcessfully');
            this.showFileFlag = false;
            $('#File1').val('');
        },
         err => {
          this.Spinner.hide();
          this.ErrorMessage = err.json().result;
          this.toastr.error(this.ErrorMessage);
          this.showFileFlag = false;
        }
        );
        } else {
        this.Spinner.hide();
        this.toastr.error('Excel uploded Faild');
        this.showFileFlag = false;
      }
          } else {
            // excelInJSON.filter(someobject => someobject.dateOfBirth)
            // .forEach(someobject => someobject.dateOfBirth = moment(someobject.dateOfBirth).format('YYYY/MM/DD'));
            excelInJSON.filter(someobject => someobject.validStartDate)
            .forEach(someobject => someobject.validStartDate = moment(someobject.validStartDate).format('YYYY/MM/DD'));
            excelInJSON.filter(someobject => someobject.validEndDate)
            .forEach(someobject => someobject.validEndDate = moment(someobject.validEndDate).format('YYYY/MM/DD'));
            excelInJSON.filter(someobject => someobject.pfNumber)
            .forEach(someobject => someobject.pfNumber = String(someobject.pfNumber));
            excelInJSON.filter(someobject => someobject.esicNumber)
            .forEach(someobject => someobject.esicNumber = String(someobject.esicNumber));
            this.employeeexcelimport.importExcelMastersList = excelInJSON;
            console.log(this.employeeexcelimport.importExcelMastersList);
            if (this.employeeexcelimport.importExcelMastersList != null) {
              this.Spinner.show();
              this.excelService.postContractExcels(this.employeeexcelimport)
                .subscribe(data => {
                  this.Spinner.hide();
                  this.toastr.success('Excel Import Succcessfully');
                  this.showFileFlag = false;
                  $('#File1').val('');
              },
               err => {
                this.Spinner.hide();
                this.ErrorMessage = err.json().result;
                this.toastr.error(this.ErrorMessage);
                this.showFileFlag = false;
              }
              );
            }
          }
      //   else {
      //   this.Spinner.hide();
      //   this.toastr.error('Shifts uploded Faild');
      //   this.showFileFlag = false;
      // }
    };
  }
}
