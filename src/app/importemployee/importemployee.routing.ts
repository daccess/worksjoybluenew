import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ImportemployeeComponent } from 'src/app/importemployee/importemployee.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ImportemployeeComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'importemployee',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'importemployee',
                    component: ImportemployeeComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);