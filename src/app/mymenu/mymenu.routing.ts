import { Routes, RouterModule } from '@angular/router'
import { MymenuComponent } from './mymenu.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: MymenuComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'mymenu',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'mymenu',
                    component: MymenuComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);