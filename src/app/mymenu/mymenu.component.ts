import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mymenu',
  templateUrl: './mymenu.component.html',
  styleUrls: ['./mymenu.component.css']
})
export class MymenuComponent implements OnInit {
  loggedUser: any = {};

  constructor(private router: Router) { }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
  }

  Profile() {
    sessionStorage.setItem('whoClicked', 'EMP');
    this.router.navigateByUrl("/layout/emppimsview");
  }
}
