import { routing } from './mymenu.routing';
import { NgModule } from '@angular/core';
import { MymenuComponent } from './mymenu.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        MymenuComponent

    
    ],
    imports: [
    routing,
    CommonModule
    ],
    providers: []
  })
  export class  MymenuModule { 
      constructor(){

      }
  }
  