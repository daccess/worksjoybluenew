import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { OptionalholidayMasterComponent } from './optionalholiday-master.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
  path: '',
  component: OptionalholidayMasterComponent
  }
  ]


@NgModule({
  imports: [
    CommonModule,
    
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],

  declarations: [OptionalholidayMasterComponent]
})
export class OptinalholidayModule { }
