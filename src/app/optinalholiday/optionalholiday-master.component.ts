import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { dataService } from '../shared/services/dataService';

@Component({
  selector: 'app-optionalholiday-master',
  templateUrl: './optionalholiday-master.component.html',
  styleUrls: ['./optionalholiday-master.component.css']
})
export class OptionalholidayMasterComponent implements OnInit {

  themeColor: string = "nav-pills-blue";
  title:'holidayForm';
  holidayForm: FormGroup;
  statusForm: FormGroup;
  list_optional_holiday_url='getOptionalHolidayList';
  save_optional_holiday_url ='/requestForOptionalHoliday';
 
 edit_optionalholidayby_id_url ='editOptionalHoliday';
 view_optional_holiday_all_url ='/viewOptionalHolidayStatusList';
 update_url='/updateReqForOptionalHoliday';
 delete_url= '/deleteOptionalHoliday';
  loggedUser;
  compId;
  empPerId: any;
  approverId:any;
  optionalholidaylist:any[]=[];
  alloptionalholidaylist:any[]=[];
  optionalHolidayData:any[] =[];
  empId:any;
  statusType:any;
 isEdit =false;
  optional_holiday_id:any;
  modeflag: boolean;
  deleteItem: any;
  default: string = 'Pending';
  origindataholiday:any[]=[];
  submitted = false;
  dltmodelFlag: boolean;
  dltObje: any;
  resetflag:boolean =false;
  
    
  constructor(private formBuilder:FormBuilder,private dataservice:dataService,public Spinner: NgxSpinnerService,private toastr: ToastrService,private http : HttpClient) { 
    this.holidayForm = this.formBuilder.group({
      holidayMasterId:new FormControl('',[Validators.required]),
       desc : new FormControl(null, [Validators.required]),
     })
     this.statusForm = this.formBuilder.group({
       statusType:new FormControl(['' ,Validators.required])
     })
     this.statusForm.controls['statusType'].setValue(this.default, {onlySelf: true});
  }

  ngOnInit() {
     //get company details
     let user = sessionStorage.getItem('loggedUser');
     this.loggedUser = JSON.parse(user);
     this.compId = this.loggedUser.compId;
     this.empPerId = this.loggedUser.empPerId;
     this.empId = this.loggedUser.empId;
    this.approverId = this.loggedUser.firstLevelReportingManager;
  
    this.getOptionalholidaylist();
    this.getAllOptionalholidaylistdata();
  }
  

 getOptionalholidaylist(){
    if(this.compId){
      this.dataservice.getRecordsByid(this.list_optional_holiday_url,this.compId).subscribe(res=>{
          if(res.statusCode==200){
            this.optionalholidaylist=res.result;
          }
      });
    }

  }

  statusTypeChanged(e) {
    this.default = e;
    let obj = this.alloptionalholidaylist.filter(m => m.approverOrRejectStatus == this.default);  
     this.alloptionalholidaylist = obj; 
    this.getAllOptionalholidaylistdata();
   
  }

  
 

  getAllOptionalholidaylistdata(){
    let obj = {
      approverOrRejectStatus:this.default,
      empPerId: this.empPerId,
     }
     if(obj != null){
     
      this.dataservice.createRecord(this.view_optional_holiday_all_url,obj).subscribe(res=>{
          if(res.statusCode==200){
            this.alloptionalholidaylist=res.result;
            }else{
            this.alloptionalholidaylist = []=[];
          }
      });
    } 

  }
 
saveoptionalholiday(){
  this.submitted = true;
  this.resetflag =false;
  if(this.isEdit == true){
      let obj = {
        optionalHolId:this.optional_holiday_id,
        holidayMasterId: this.holidayForm.controls["holidayMasterId"].value,
        approverId:this.approverId,
        empPerId: this.empPerId,
        approverOrRejectStatus:"Pending",
        resonForPermission : this.holidayForm.controls["desc"].value,
       }
       if (this.holidayForm.valid) {
        this.Spinner.show();
      this.dataservice.updateRecords(this.update_url,obj).subscribe(res=>{
       if(res.statusCode==201){
          this.toastr.success("Optional Holiday Updated Sucessfully !");
          this.Spinner.hide();
          this.getAllOptionalholidaylistdata();
          this.reset();
          this.holidayForm.markAsPristine();
          this.holidayForm.markAsUntouched();
          this.holidayForm.updateValueAndValidity();
        }
       },err => {
        this.toastr.error('Optional Holiday not be Updated ');
        this.Spinner.hide();
       })
      }
      }else{  
    
    let obj = {
    holidayMasterId: this.holidayForm.controls["holidayMasterId"].value,
    approverId:this.approverId,
    empPerId: this.empPerId,
    resonForPermission : this.holidayForm.controls["desc"].value,
   }
   
 if(obj != null){
  if (this.holidayForm.valid) {
    this.Spinner.show();
    this.dataservice.createRecord(this.save_optional_holiday_url,obj).subscribe(res=>{
   if(res.statusCode==201){
      this.toastr.success("Optional Holiday Added Sucessfully !");
      this.reset();
       this.getAllOptionalholidaylistdata();
        document.getElementById('pills-profile-tab').click(); 
        this.Spinner.hide();
    }else{
      this.toastr.error('Only One Optional Holiday Allowed to Apply');
    }
   },err => {
        this.toastr.error('Only One Optional Holiday Allowed to Apply');
     this.Spinner.hide();
    })
}
}
    }

}

editOptionlholiday(editdata){
this.optional_holiday_id =editdata.optionalHolId;
if(editdata != null){
  this.dataservice.getRecordsByid(this.edit_optionalholidayby_id_url,this.optional_holiday_id).subscribe(res=>{
      if(res.statusCode==200){
       this.optionalHolidayData=res.result;
       this.isEdit= true;
       for(let i=0;i<this.optionalHolidayData.length;i++){
         this.optional_holiday_id = this.optionalHolidayData[i].optionalHolId;
       }
        document.getElementById('pills-home-tab').click();
        this.holidayForm.controls.holidayMasterId.setValue(editdata.holidayMasterId);
        this.holidayForm.controls.desc.setValue(editdata.resonForPermission);
       
       }
  });
}



}

reset(){
  if(!this.holidayForm.valid){
  this.resetflag =true;
  }else{
    this.resetflag =false;
  }
  this.holidayForm.reset();
 this.holidayForm.controls.holidayMasterId.clearValidators();
 this.holidayForm.controls.desc.clearValidators();
this.holidayForm.markAsPristine();
this.holidayForm.markAsUntouched();
this.holidayForm.updateValueAndValidity();

  
}
cancel(){
  document.getElementById('pills-profile-tab').click(); 
  this.holidayForm.markAsPristine();
  this.holidayForm.markAsUntouched();
  this.holidayForm.updateValueAndValidity();
}

get f() { return this.holidayForm.controls; }


dltObj(obj) {

  this.dltmodelFlag = false
  this.dltObje = obj;
  this.deleteOptionalday(this.dltObje)
}
dltmodelEvent() {
  this.dltmodelFlag = true;
this.deleteOptionalday(this.dltObje);
  this.getAllOptionalholidaylistdata();
  
}

deleteOptionalday(deletedata){
  this.optional_holiday_id =deletedata.optionalHolId;
  if(deletedata != null){
    if (this.dltmodelFlag == true) {
      this.Spinner.show();
    this.dataservice.deleteRecord(this.delete_url,this.optional_holiday_id).subscribe(res=>{
        if(res.statusCode==200){
         this.toastr.success("Optional Holiday deleted successfully");
        this.alloptionalholidaylist = []=[];
         this.Spinner.hide();
         }
         
       },err => {
       this.toastr.error("Failed to delete Optional Holiday");
      
       this.Spinner.hide();
     
     })
    }
   
    }
    

}


}


