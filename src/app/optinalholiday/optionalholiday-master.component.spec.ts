import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionalholidayMasterComponent } from './optionalholiday-master.component';

describe('OptionalholidayMasterComponent', () => {
  let component: OptionalholidayMasterComponent;
  let fixture: ComponentFixture<OptionalholidayMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionalholidayMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalholidayMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
