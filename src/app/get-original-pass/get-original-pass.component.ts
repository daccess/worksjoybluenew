import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as htmlToImage from 'html-to-image';

@Component({
  selector: 'app-get-original-pass',
  templateUrl: './get-original-pass.component.html',
  styleUrls: ['./get-original-pass.component.css']
})
export class GetOriginalPassComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  getByDocIds: string;
  getemployeeDetails: any;
  allEmployeedata: any;
  manrequesStatusId: string;
  labourIds: string;
  labourmasterId: string;
  labourDataOfImage: any;
  manReqStatusId:any;
  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router: Router) {
    debugger
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
   this.labourIds=sessionStorage.getItem("LabourPerIdss")
    this.labourmasterId=sessionStorage.getItem("manRequestIds")
   }

  ngOnInit() {
    this.getPassData()
  }

  // convert to image and download
  // generateImage(){
  //   var node:any = document.getElementById('image-section');
  //   htmlToImage.toPng(node)
  //     .then(function (dataUrl) {
  //       // var img = new Image();
  //       // img.src = dataUrl;
  //       // document.body.appendChild(img);
  //     var link = document.createElement('a');
  //     link.href = dataUrl;
  //     link.download = 'card-image.png';
  //     link.click();
  //     })
  //     .catch(function (error) {
  //       console.error('error getting download', error);
  //     });

  //     const url = `${this.baseurl}/finalPassDone/${this.labourmasterId}`;
  //     const headers = new HttpHeaders({
  //       'Authorization': `Bearer ${this.token}`
  //     });
  //     this.httpservice.put(url, {}, { headers }).subscribe(
  //       (data) => {
  //         console.log("data =>", data);
  //       },
  //       (err) => {
  //         console.log("error =>", err);
  //         this.Spinner.hide();
  //       }
  //     );
  // }
 
updatePassInfo(){
  debugger
  let url = this.baseurl + `/finalPassDone?labourMasterId=${this.labourmasterId}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpservice.get(url, { headers }).subscribe(data => {
    this.router.navigateByUrl('/layout/LabourList')
    this.generateImage()
  },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();

    });
  }
  generateImage() {
    // Add a delay to ensure the styles are applied before capturing the image
    setTimeout(() => {
      htmlToImage.toJpeg(document.getElementById('image-section') as HTMLElement, { quality: 0.95 })
        .then(function (dataUrl) {
          var link = document.createElement('a');
          link.download = 'Gate-Pass.jpeg';
          link.href = dataUrl;
  
          // Inline styles here
          link.style.color = 'red';
          link.style.textDecoration = 'underline';
  
          document.body.appendChild(link); // Append the link to the DOM
          link.click();
          document.body.removeChild(link); // Remove the link from the DOM after clicking
        });
    }, 1000); // Adjust the delay as needed
  }
  


  getPassData() {
    // let url = this.baseurl + `/getFrontDeskDocumentList/${this.manrequesStatusId}`;
    let url = this.baseurl + `/getByIdLabourInfo/${this.labourIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.getemployeeDetails = data;
      this.allEmployeedata=this.getemployeeDetails.result
      this.labourDataOfImage=this.allEmployeedata.labourImage
  
    },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();

      });
    }


}
