import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetOriginalPassComponent } from './get-original-pass.component';

describe('GetOriginalPassComponent', () => {
  let component: GetOriginalPassComponent;
  let fixture: ComponentFixture<GetOriginalPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetOriginalPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetOriginalPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
