import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontDeskApprovalComponent } from './front-desk-approval.component';

describe('FrontDeskApprovalComponent', () => {
  let component: FrontDeskApprovalComponent;
  let fixture: ComponentFixture<FrontDeskApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontDeskApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontDeskApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
