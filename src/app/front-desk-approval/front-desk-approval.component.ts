import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-front-desk-approval',
  templateUrl: './front-desk-approval.component.html',
  styleUrls: ['./front-desk-approval.component.css']
})
export class FrontDeskApprovalComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  locationdata: any;
  locationId: any;
  manpowerData: any;
  allmanpowerLabourData: any;
  documentids: any;
  labourmasterids: any;
  allLabourDocumentHistory: any;
  manReqStatusIds: any;
  selectUndefinedOptionValue:any;
  searchLabour:any;
  searchByColumn:any;
  searchByLabour:any;
  searchByDocument:any;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId;
    this.locationId=this.users.roleList.locationId
   }

  ngOnInit() {
    this.getlocationData()
  }
  getlocationData() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.locationdata = data["result"];
      this.locationId = this.locationId;
      this.getAllManpowerRequestApprovedByContractor();

    },
      (err: HttpErrorResponse) => {

      })

  }
  getAllManpowerRequestApprovedByContractor() {
    let url = this.baseurl + `/getFrontDeskLabourList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.allmanpowerLabourData = data["result"];
      setTimeout(function () {

        $(function () {

          var table = $('#emptable').DataTable({

            retrieve: true,
            searching:false

         

          })

          // table
          // .order( [ 1, 'des' ] )
          // .draw();
          // $('.status-dropdown').on('change', function (e) {
          //   var status = $(this).val();
          //   $('.status-dropdown').val(status)
          //   console.log(status)
          //   table.column(6).search(status as AssignType).draw();
          // })


        });
      }, 100);
      this.Spinner.hide();
  
      // this.count1=this.manpowerAllData.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  contractorchecked2:boolean=false;
  // checks2() {
  //   for (let i = 0; i < this.allmanpowerLabourData.length; i++) {
  //     // this.leaveList[i].check = true;
  //     if (this.allmanpowerLabourData[i].check) {
  //       this.contractorchecked2 = true;
  //       break;
  //     }
  //     else {
  //       this.contractorchecked2 = false;
  //     }
  //   }
  // }
  // approveLabour(item) {
  //   // let selectedObj4: any = new Array();
  //   let obj = {
  //     "labourMasterId": item.labourMasterId,
  //     "remark": item.approverRemark,
  //     "deskFlag": "Approved",
  //     "approverId": this.empids
  //   }
  //   // selectedObj4.push(obj)
  //   // let obj2 = {
  //   //   "commonApprovedReqDtos": selectedObj4
  //   // }

  //   this.Spinner.show();
  //   let url = this.baseurl + '/createFrontDeskLabourApproved';
  //   const tokens = this.token;
  //   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.post(url, obj, { headers }).subscribe(data => {

  //     //document.getElementById('LeaveCol').click();
  //     this.getAllManpowerRequestApprovedByContractor();
  //     this.Spinner.hide();
  //     setTimeout(() => {
  //       this.getAllManpowerRequestApprovedByContractor();
  //     }, 1000);

  //     this.toastr.success('Manpower Request Approved Successfully');
  //   }, err => {
  //     this.Spinner.hide()
  //     this.toastr.error('Failed To Approve manpower Request');
  //   })
  // }
  // rejectLabour(item) {
  //   let selectedObj5: any = new Array();
  //   let obj = {
  //     "labourMasterId": item.labourMasterId,
  //     "remark": item.approverRemark,
  //     "deskFlag": "Rejected",
  //     "approverId": this.empids,
  //   }
  //   selectedObj5.push(obj)
  //   let obj2 = {
  //     "commonApprovedReqDtos": selectedObj5
  //   }
  //   this.Spinner.show();
  //   let url = this.baseurl + '/createFrontDeskLabourApproved';
  //   const tokens = this.token;
  //   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.post(url, obj, { headers }).subscribe(data => {
  //     this.Spinner.hide()
  //     setTimeout(() => {
  //       this.getAllManpowerRequestApprovedByContractor();
  //     }, 1000);

  //     this.toastr.success('manpower Request Rejected Successfully');
  //   }, err => {
  //     this.Spinner.hide()
  //     this.toastr.error('Failed To Reject manpower Request');
  //   })
  // }
  gotoDocument(e:any){
    
    this.documentids=e.requestStatus.labourInfo.labourPerId;
    this.manReqStatusIds=e.requestStatus.manReqStatusId
    sessionStorage.setItem("documentIds",this.documentids)
    sessionStorage.setItem("manReqStatusId",this.manReqStatusIds)
    this.router.navigate(['/layout/FrontApproveReject'])
    
  }
  gotoDocument1(e:any){
    
    // this.documentids=e.requestStatus.labourInfo.labourPerId;
    // this.manReqStatusIds=e.requestStatus.manReqStatusId
    // sessionStorage.setItem("documentIds",this.documentids)
    // sessionStorage.setItem("manReqStatusId",this.manReqStatusIds)
    this.router.navigate(['/layout/frontDeskAssets'])
    
  }
  getlabourid(e:any){
this.labourmasterids=e.requestStatus.labourInfo.labourPerId;
let url = this.baseurl + `/getDocumentHistory/${this.labourmasterids}`;
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpservice.get(url, { headers }).subscribe(data => {
  this.allLabourDocumentHistory = data["result"];
console.log("this is the data",this.allLabourDocumentHistory)
  })
}
}
