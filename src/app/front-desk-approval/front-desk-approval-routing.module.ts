import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontDeskApprovalComponent } from './front-desk-approval.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: FrontDeskApprovalComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'frontdeskApproval',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'frontdeskApproval',
                  component: FrontDeskApprovalComponent
              }

          ]

  },

  // otherwise redirect to home

]

export const FrontDeskRouting = RouterModule.forChild(appRoutes);