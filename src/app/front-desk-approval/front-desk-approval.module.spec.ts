import { FrontDeskApprovalModule } from './front-desk-approval.module';

describe('FrontDeskApprovalModule', () => {
  let frontDeskApprovalModule: FrontDeskApprovalModule;

  beforeEach(() => {
    frontDeskApprovalModule = new FrontDeskApprovalModule();
  });

  it('should create an instance', () => {
    expect(frontDeskApprovalModule).toBeTruthy();
  });
});
