import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontDeskRouting } from './front-desk-approval-routing.module';
import { FrontDeskApprovalComponent } from './front-desk-approval.component';
import {  FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FrontDeskRouting,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    FrontDeskApprovalComponent
  ]
})
export class FrontDeskApprovalModule { }
