import { routing } from './customizedashboard.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { CustomizedashboardComponent } from './customizedashboard.component';
import { ShiftwiseAbsentismComponent } from '../hrdashboard/charts/shiftwise-absentism/shiftwise-absentism.component';
import { CoffchartComponent } from '../hrdashboard/charts/coffchart/coffchart.component';
import { AbsentismchartComponent } from '../hrdashboard/charts/absentismchart/absentismchart.component';
import { Apsentismchart2Component } from '../hrdashboard/charts/apsentismchart2/apsentismchart2.component';
import { SkilledchartComponent } from '../hrdashboard/charts/skilledchart/skilledchart.component';
import { FtechartComponent } from '../hrdashboard/charts/ftechart/ftechart.component';
import { AbsentismtrendchartComponent } from '../hrdashboard/charts/absentismtrendchart/absentismtrendchart.component';
import { ThreedaysabsentismchartComponent } from '../hrdashboard/charts/threedaysabsentismchart/threedaysabsentismchart.component';
import { CoffsupervsorchartComponent } from '../hrdashboard/charts/coffsupervsorchart/coffsupervsorchart.component';
import { ShiftwiseabsentismsupervisorComponent } from '../hrdashboard/charts/shiftwiseabsentismsupervisor/shiftwiseabsentismsupervisor.component';
import { TotalheadcountchartComponent } from '../hrdashboard/charts/totalheadcountchart/totalheadcountchart.component';
import { PlanvsactulachartComponent } from '../hrdashboard/charts/planvsactulachart/planvsactulachart.component';
import { LateEarlyLateChartComponent } from '../hrdashboard/charts/late-early-late-chart/late-early-late-chart.component';
import { ShiftdeviationchartComponent } from '../hrdashboard/charts/shiftdeviationchart/shiftdeviationchart.component';
import { DepartmentwisemanhrsComponent } from '../hrdashboard/charts/departmentwisemanhrs/departmentwisemanhrs.component';
import { OthrsincreaseunplannedComponent } from '../hrdashboard/charts/othrsincreaseunplanned/othrsincreaseunplanned.component';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { HrdashboardsharedModule } from '../hrdashboardshared/hrdashboardshared.module';


@NgModule({
    declarations: [
        CustomizedashboardComponent,
        // ShiftwiseAbsentismComponent,
        // CoffchartComponent,
        // AbsentismchartComponent,
        // Apsentismchart2Component,
        // SkilledchartComponent,
        // FtechartComponent,
        // AbsentismtrendchartComponent,
        // ThreedaysabsentismchartComponent,
        // CoffsupervsorchartComponent,
        // ShiftwiseabsentismsupervisorComponent,
        // TotalheadcountchartComponent,
        // PlanvsactulachartComponent,
        // LateEarlyLateChartComponent,
        // ShiftdeviationchartComponent,
        // DepartmentwisemanhrsComponent,
        // OthrsincreaseunplannedComponent
    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     ChartsModule,
     NgxChartsModule,
     HrdashboardsharedModule,
     NgCircleProgressModule.forRoot({
   
    })

    ],
    
    providers: [],
 
  })
  export class  CustomizedashboardModule { 
      constructor(){

      }
      
  }
  