import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { CustomizedashboardComponent } from './customizedashboard.component';



const appRoutes: Routes = [
    { 
             
        path: '', component: CustomizedashboardComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'customizedashboard',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'customizedashboard',
                    component: CustomizedashboardComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);