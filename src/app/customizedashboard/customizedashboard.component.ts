import { OnInit, Component } from "@angular/core";
import { MainURL } from "../shared/configurl";
import { Http } from "@angular/http";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

// import { Component, OnInit } from 'src/app/customizedashboard/node_modules/@angular/core';

@Component({
  selector: 'app-customizedashboard',
  templateUrl: './customizedashboard.component.html',
  styleUrls: ['./customizedashboard.component.css']
})
export class CustomizedashboardComponent implements OnInit {
  baseurl = MainURL.HostUrl;

  totalHeadcount: boolean = false;
  allRequests: boolean = false;
  shiftDeviationHabitualAbsentism: boolean = false;
  shiftwiseAbsentism: boolean = false;
  coffDetails: boolean = false;
  absentism: boolean = false;
  gratuityEntitlement: boolean = false;
  lateSitting: boolean = false;
  skiledUnskilled: boolean = false;
  fteVsTemp: boolean = false;
  gender: boolean = false;
  allowances: boolean = false;
  absentismTrend: boolean = false;
  absentismTrendContinuously: boolean = false;
  coffDetailsSupervisor: boolean = false;
  shiftwiseAbsentismSupervisor: boolean = false;
  esicApplicable: boolean = false;
  shiftDeviation: boolean = false;
  lateSittingOtList: boolean = false;
  OtHoursIncrease: boolean = false;
  DepartmentWiseManHoursCalculation: boolean = false;
  shiftwiseAttendance : boolean = false;
  loggedUser: any;
  AllRoll = [];

  role: any;

  test: any=''
  constructor(private http: Http, private toastr: ToastrService, public Spinner :NgxSpinnerService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
  }

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    let url = this.baseurl + '/RollMaster/';
    this.http.get(url + this.loggedUser.compId).subscribe(data => {
        this.AllRoll = data.json()["result"];
      }, (err: HttpErrorResponse) => {
      }
    );

  }
  model: any = {};
  
  onSubmit() {
    let obj = {
      reportHRDashboardMasterList: [],
      rollMasterId: this.role
    }
    if (this.totalHeadcount) {
      let item = {
        reportHrId: 1
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.allRequests) {
      let item = {
        reportHrId: 2
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.shiftDeviationHabitualAbsentism) {
      let item = {
        reportHrId: 3
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.shiftwiseAbsentism) {
      let item = {
        reportHrId: 4
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.coffDetails) {
      let item = {
        reportHrId: 5
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.absentism) {
      let item = {
        reportHrId: 6
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.gratuityEntitlement) {
      let item = {
        reportHrId: 7
      }
      obj.reportHRDashboardMasterList.push(item)
    }

    if (this.lateSitting) {
      let item = {
        reportHrId: 8
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.skiledUnskilled) {
      let item = {
        reportHrId: 9
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.fteVsTemp) {
      let item = {
        reportHrId: 10
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.gender) {
      let item = {
        reportHrId: 11
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.allowances) {
      let item = {
        reportHrId: 12
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.absentismTrend) {
      let item = {
        reportHrId: 13
      }
      obj.reportHRDashboardMasterList.push(item)
    }

    if (this.absentismTrendContinuously) {
      let item = {
        reportHrId: 14
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.coffDetailsSupervisor) {
      let item = {
        reportHrId: 15
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.shiftwiseAbsentismSupervisor) {
      let item = {
        reportHrId: 16
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.esicApplicable) {
      let item = {
        reportHrId: 17
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.shiftDeviation) {
      let item = {
        reportHrId: 18
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.lateSittingOtList) {
      let item = {
        reportHrId: 19
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.OtHoursIncrease) {
      let item = {
        reportHrId: 20
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.DepartmentWiseManHoursCalculation) {
      let item = {
        reportHrId: 21
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    if (this.shiftwiseAttendance) {
      let item = {
        reportHrId: 22
      }
      obj.reportHRDashboardMasterList.push(item)
    }
    var body = JSON.stringify(obj);
    if (this.isEdit) {
      this.Spinner.show()
      let url = this.baseurl + '/updatecustomizedhrdashboard/';
      this.http.put(url, obj).subscribe(data => {
          this.isEdit = false;
          this.Spinner.hide();
          this.toastr.success("Updated SuccessFully");
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error("Failed To Update");
        });
    }
    else {
      this.Spinner.show();
      let url = this.baseurl + '/customizedhrdashboard/';
      this.http.post(url, obj).subscribe(data => {
          this.isEdit = false;
          this.Spinner.hide();
          this.toastr.success("Added SuccessFully");
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error("Failed To Add");
        });
    }
  }
  isEdit: boolean = false;
  selectRole(e) {
        this.totalHeadcount = false;
        this.allRequests = false;
        this.shiftDeviationHabitualAbsentism = false;
        this.shiftwiseAbsentism = false;
        this.coffDetails = false;
        this.absentism = false;
        this.gratuityEntitlement = false;
        this.lateSitting = false;
        this.skiledUnskilled = false;
        this.fteVsTemp = false;
        this.gender = false;
        this.allowances = false;
        this.absentismTrend = false;
        this.absentismTrendContinuously = false;
        this.coffDetailsSupervisor = false;
        this.shiftwiseAbsentismSupervisor = false;
        this.esicApplicable = false;
        this.shiftDeviation = false;
        this.lateSittingOtList = false;
        this.OtHoursIncrease = false;
        this.DepartmentWiseManHoursCalculation = false;
        this.shiftwiseAttendance = false;
    let url = this.baseurl + '/getcustomerhrdashboard/';
    this.http.get(url + e).subscribe(data => {
        let allData = data.json().result;
        if (allData) {
          this.isEdit = true;
        }
        for (let i = 0; i < allData.length; i++) {
          if (allData[i].reportHrId == 1) {
            this.totalHeadcount = true;
          }
          if (allData[i].reportHrId == 2) {
            this.allRequests = true;
          }
          if (allData[i].reportHrId == 3) {
            this.shiftDeviationHabitualAbsentism = true;
          }
          if (allData[i].reportHrId == 4) {
            this.shiftwiseAbsentism = true;
          }
          if (allData[i].reportHrId == 5) {
            this.coffDetails = true;
          }
          if (allData[i].reportHrId == 6) {
            this.absentism = true;
          }
          if (allData[i].reportHrId == 7) {
            this.gratuityEntitlement = true;
          }
          if (allData[i].reportHrId == 8) {
            this.lateSitting = true;
          }
          if (allData[i].reportHrId == 9) {
            this.skiledUnskilled = true;
          }
          if (allData[i].reportHrId == 10) {
            this.fteVsTemp = true;
          }
          if (allData[i].reportHrId == 11) {
            this.gender = true;
          }
          if (allData[i].reportHrId == 12) {
            this.allowances = true;
          }
          if (allData[i].reportHrId == 13) {
            this.absentismTrend = true;
          }
          if (allData[i].reportHrId == 14) {
            this.absentismTrendContinuously = true;
          }
          if (allData[i].reportHrId == 15) {
            this.coffDetailsSupervisor = true;
          }
          if (allData[i].reportHrId == 16) {
            this.shiftwiseAbsentismSupervisor = true;
          }
          if (allData[i].reportHrId == 17) {
            this.esicApplicable = true;
          }
          if (allData[i].reportHrId == 18) {
            this.shiftDeviation = true;
          }
          if (allData[i].reportHrId == 19) {
            this.lateSittingOtList = true;
          }
          if (allData[i].reportHrId == 20) {
            this.OtHoursIncrease = true;
          }
          if (allData[i].reportHrId == 21) {
            this.DepartmentWiseManHoursCalculation = true;
          }
          if (allData[i].reportHrId == 22) {
            this.shiftwiseAttendance = true;
          }
        }
      }, (err: HttpErrorResponse) => {
        this.isEdit = false;
        this.totalHeadcount = false;
        this.allRequests = false;
        this.shiftDeviationHabitualAbsentism = false;
        this.shiftwiseAbsentism = false;
        this.coffDetails = false;
        this.absentism = false;
        this.gratuityEntitlement = false;
        this.lateSitting = false;
        this.skiledUnskilled = false;
        this.fteVsTemp = false;
        this.gender = false;
        this.allowances = false;
        this.absentismTrend = false;
        this.absentismTrendContinuously = false;
        this.coffDetailsSupervisor = false;
        this.shiftwiseAbsentismSupervisor = false;
        this.esicApplicable = false;
        this.shiftDeviation = false;
        this.lateSittingOtList = false;
        this.OtHoursIncrease = false;
        this.DepartmentWiseManHoursCalculation = false;
        this.shiftwiseAttendance = false;
      });
  }
}
