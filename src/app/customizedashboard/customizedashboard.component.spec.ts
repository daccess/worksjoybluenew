// import { async, ComponentFixture, TestBed } from 'src/app/customizedashboard/node_modules/@angular/core/testing';

import { CustomizedashboardComponent } from './customizedashboard.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';

describe('CustomizedashboardComponent', () => {
  let component: CustomizedashboardComponent;
  let fixture: ComponentFixture<CustomizedashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizedashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizedashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
