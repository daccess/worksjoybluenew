import { routing } from './deactivateemployee.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import{CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DeactivateemployeeComponent } from './deactivateemployee.component';
import { resignationservice } from '../resignationpage/shared/services/resignationservice';
import { SearchesPipe } from './searchpipe/search.pipe';
@NgModule({
    declarations: [
        DeactivateemployeeComponent,
        SearchesPipe
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule
    ],
    providers: [resignationservice]
  })
  export class DeactivateemployeeModule { 
      constructor(){

      } 
     
  }
  