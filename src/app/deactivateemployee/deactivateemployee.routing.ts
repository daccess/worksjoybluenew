import { Routes, RouterModule } from '@angular/router';
import { DeactivateemployeeComponent } from './deactivateemployee.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: DeactivateemployeeComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'deactivateemployee',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'deactivateemployee',
                    component: DeactivateemployeeComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);