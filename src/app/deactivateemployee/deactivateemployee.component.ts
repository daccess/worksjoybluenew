import { Component, ChangeDetectorRef,OnInit } from '@angular/core';
import { LayOutService } from '../shared/model/layOutService';
import { resignationmodeldata } from '../resignationpage/shared/Models/resignationmodel';
import { resignationservice } from '../resignationpage/shared/services/resignationservice';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-deactivateemployee',
  templateUrl: './deactivateemployee.component.html',
  styleUrls: ['./deactivateemployee.component.css']
})
export class DeactivateemployeeComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  themeColor: string = "nav-pills-blue";
  noOfManpower:any;
  locationMasterId:any;
  desigMasterId:any;
  deptMasterId:any;
  jobDiscription:any;
  startDate:any;
  endDate:any;
  expectedDateLabour:any;
  fresher:any;
  experience:any;
  skillCategory:any;
  education:any;
  gender:any;
  remark:any;
  iseditflag:boolean=false;
  experiences:boolean=true
  // ManPowerRequestForm:any = {
  //   noOfManpower:'',
  //   locationMasterId:'',
  //   desigMasterId:',',
  //   deptMasterId:'',
  //   jobDiscription:'',
  //   startDate:'',
  //   endDate:'',
  //   expectedDateLabour:'',
  //   fresher:'',
  //   experience:'',
  //   skillCategory:'',
  //   education:'',
  //   gender:'',
  //   remark:'',
  //  };
  UrlDownload:any
  AllLocations: any;
  user: string;
  users: any;
  token: any;
  AllDesignation: any;
  GetAllDepartment: any;
  AllListOfManPower: any;
  getManpowerReqByIds: any;
  AllmanPowerData: any;
  empManpowerReqId: any;
  getDeleteManpowerReqByIds: any;
  empids: any;
  Experiences: any;
  hideExperienceFlag:boolean=false

  constructor(
    public Spinner: NgxSpinnerService, 
    public resignationservice: resignationservice,
 
    public sanitizer: DomSanitizer,
    public toastr: ToastrService,private httpSer:HttpClient,private chRef:ChangeDetectorRef) {

  }


  ngOnInit() {
  
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token;
    this.empids=this.users.roleList.empId
    this.getAllLocation()
    this.getAllDesignation()
    this.getAllDepartment()
    this.getAllManpowerRequest()
  }

  searchFlag: boolean = false;

  employeeData = [];
  isSanctioningAuthority: boolean = false;
 

  dltmodelFlag: boolean;
  indexDelete: number = 0;
  dltObje: any;
  dltObj(i) {
    this.dltmodelFlag = true;
    this.indexDelete = i;
    // obj.reset();

    //this.dltObje = obj.empPerId;
    //this.dltObjfun(this.dltObje);
  }
  
  getAllLocation(){
    let url = this.baseurl + '/getAllLocations';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url,{headers}).subscribe(data => {
    
      this.AllLocations = data["result"];
        
  },
    (err: HttpErrorResponse) => {

    })
  }
  getAllDesignation(){
    let url = this.baseurl + '/getAllDesignations';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url,{headers}).subscribe(data => {
    
      this.AllDesignation = data["result"];
        
  },
    (err: HttpErrorResponse) => {

    })
  }
  getAllDepartment(){
    let url = this.baseurl + '/getAllDepartments';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url,{headers}).subscribe(data => {
    
      this.GetAllDepartment = data["result"];
        
  },
    (err: HttpErrorResponse) => {

    })
  }

  submitManPowerRequest(){

let obj={
  "noOfManpower":this.noOfManpower,
  "locationMasterId":this.locationMasterId,
  "desigMasterId":this.desigMasterId,
  "deptMasterId":this.deptMasterId,
  "jobDiscription":this.jobDiscription,
  "startDate":this.startDate,
  "endDate":this.endDate,
  "expectedDateLabour":this.expectedDateLabour,
  "fresher":this.fresher,
  "experience":this.experience,
  "skillCategory":this.skillCategory,
  "education":this.education,
  "gender":this.gender,
  "remark":this.remark,
  "empManpowerReqId":this.empManpowerReqId,
  "empId":this.empids,
  "Experiences":this.Experiences,
}
if(this.iseditflag==false){
let url = this.baseurl + '/createManpowerReq';
const tokens = this.token;
this.Spinner.show()
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.post(url,obj,{headers}).subscribe(data => {
this.toastr.success("Manpower Request Inserted Successfully")
this.Spinner.hide()
this.getAllManpowerRequest();
      document.getElementById('extra-list-tab').click();

  })
}
else if(this.iseditflag==true){
  let url = this.baseurl + '/updateManpowerReq';
const tokens = this.token;
this.Spinner.show()
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.put(url,obj,{headers}).subscribe(data => {
      this.iseditflag==false
this.toastr.success("Manpower Request updated Successfully")
this.Spinner.hide()
this.getAllManpowerRequest();
      document.getElementById('extra-list-tab').click();

  })
}
}
getAllManpowerRequest(){
  
  let url = this.baseurl + `/getManpowerReq/${this.users.roleList.roleId}`;
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url,{headers}).subscribe(data => {
      this.AllListOfManPower = data["result"];
      this.chRef.detectChanges();
    
      // this.dataTableFlag = true;
    
      setTimeout(function () {

        $(function () {

          var table = $('#ManpowerRequestTable').DataTable({

            retrieve: true,

            // "columnDefs": [
            //   {
            //     "targets": [9],
            //     "visible": true
            //   }
            // ]

          })

          // table
          // .order( [ 1, 'des' ] )
          // .draw();
          // $('.status-dropdown').on('change', function (e) {
          //   var status = $(this).val();
          //   $('.status-dropdown').val(status)
          //   console.log(status)
          //   table.column(9).search(status as AssignType).draw();
          // })


        });
      }, 100);
      this.Spinner.hide();
    },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
  
      });
  }
  getManpowerReqById(e:any){
   this.iseditflag=true
    this.getManpowerReqByIds=e.empManpowerReqId
    let url = this.baseurl + `/getManpowerReqById/${this.getManpowerReqByIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpSer.get(url,{headers}).subscribe(data => {
          document.getElementById('extra-apply-tab').click();
          this.AllmanPowerData = data["result"];
          this.empManpowerReqId=this.AllmanPowerData.empManpowerReqId
          this.noOfManpower=this.AllmanPowerData.noOfManpower;
          this.locationMasterId=this.AllmanPowerData.locationMaster.locationId;
          this.desigMasterId=this.AllmanPowerData.designationMaster.desigId;
          this.deptMasterId=this.AllmanPowerData.departmentMaster.deptId;
          this.jobDiscription=this.AllmanPowerData.jobDiscription;
          this.startDate=this.AllmanPowerData.startDate;
          this.endDate=this.AllmanPowerData.endDate;
          this.expectedDateLabour=this.AllmanPowerData.expectedDateLabour;
        
          this.fresher=this.AllmanPowerData.fresher;
        if(this.fresher==true){
          this.fresher='true'
        }
        if(this.fresher==false){
          this.fresher='false'
        }
          this.experience=this.AllmanPowerData.experience;
          if(this.experience==true){
            this.experience='true'
          }
          if(this.experience==false){
            this.experience='false'
          }
          this.skillCategory=this.AllmanPowerData.skillCategory;
          this.education=this.AllmanPowerData.education;
          this.gender=this.AllmanPowerData.gender;
          this.remark=this.AllmanPowerData.remark;
          this.Experiences=this.AllmanPowerData.Experiences;
          console.log("dshaf",this.AllmanPowerData)
  })
}
deletemanpowerRequest(e:any){
  confirm("Are you sure you want to delete this Manpower Request?")
  this.getDeleteManpowerReqByIds=e.empManpowerReqId
  let url = this.baseurl + `/deleteManpowerReqById/${this.getDeleteManpowerReqByIds}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpSer.delete(url,{headers}).subscribe(data => {
        this.toastr.success("Manpower Request delete Successfully")
        this.getAllManpowerRequest();
      })
}

// Add a function to handle the validation
validateNoOfManpower() {
  if (this.noOfManpower === 0) {
    this.noOfManpower = null; // Clear the value or set it to another appropriate value
  }
} 
numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;

}

}

