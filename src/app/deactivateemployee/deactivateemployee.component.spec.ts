import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeactivateemployeeComponent } from './deactivateemployee.component';

describe('DeactivateemployeeComponent', () => {
  let component: DeactivateemployeeComponent;
  let fixture: ComponentFixture<DeactivateemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeactivateemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeactivateemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
