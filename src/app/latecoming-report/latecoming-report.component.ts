import { Component, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
import * as moment from 'moment';
@Component({
  selector: 'app-latecoming-report',
  templateUrl: './latecoming-report.component.html',
  styleUrls: ['./latecoming-report.component.css'],
})
export class LatecomingReportComponent implements OnInit {

  AllReportData: any;
  AllReport: any;

  srnoCheckbox = true;
  attendencecheckbox = true;
  shiftintimeCheckbox = true;
  shiftouttimeCheckbox =  true;
  intimeCheckbox = true;
  outtimeCheckbox = true;
  latedurationChecckbox =  true;

  marked = true;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  exceldata: any;
  
  exceldata1 =[];
  exceldata2 =[];

  fromDate: any;
  toDate: any;
  compName: any;
  loggedUser: any;
  todaysDate: Date;
  report_time: string;
  isexcel=true;
  late_data: any;
  in_time: any;
  out_time: any;
  constructor(private datePipe : DatePipe,private InfoService:InfoService,private router: Router) { 

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
     this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.AllReport = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    if( this.AllReport[0].list){
      this.late_data = Array.from(this.AllReport[0].list.reduce((m, {
        empId, fullName,lastName,attenDate,deptName,descName,inTime,endTime,checkIn1,checkOut1,duration}) => 
        m.set(empId, [...(m.get(empId) || []), {"empId":empId,"fullName":fullName,"lastName":lastName,"attenDate":attenDate,"deptName":deptName,"descName": descName,"inTime":inTime,"endTime":endTime,"checkIn1":checkIn1, "checkOut1":checkOut1,"duration":duration}]), new Map), ([empno, empname]) => ({
          empno, "emp_details":empname
        }));
     }else{
      this.late_data = Array.from(this.AllReport[0].reduce((m, {
        empId, fullName,lastName,attenDate,deptName,descName,inTime,endTime,checkIn1,checkOut1,duration}) => 
        m.set(empId, [...(m.get(empId) || []), {"empId":empId,"fullName":fullName,"lastName":lastName,"attenDate":attenDate,"deptName":deptName,"descName": descName,"inTime":inTime,"endTime":endTime,"checkIn1":checkIn1, "checkOut1":checkOut1,"duration":duration}]), new Map), ([empno, empname]) => ({
          empno, "emp_details":empname
        }));
    }



    // let k=1;
    // for(let i=0; i<this.AllReport.length; i++){
    //   if(this.AllReport[i].list != null){
    //     for (let j = 0; j < this.AllReport[i].list.length; j++) {
    //       this.AllReport[i].list[j].Sr=k++;;
    //     }
    //   }
      
    // }
  }

  ngOnInit() {
  }
  toggleVisibility(e){
    this.marked = e.target.checked;
  }

  toggleVisibility1(e){
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e){
    this.marked2 = e.target.checked;
  }
  toggleVisibility3(e){
    this.marked3 = e.target.checked;
  }
  toggleVisibility4(e){
    this.marked4 = e.target.checked;
  }
  toggleVisibility5(e){
    this.marked5 = e.target.checked;
  }
  toggleVisibility6(e){
    this.marked6 = e.target.checked;
  }
  exportAsXLSX(tableID, filename = ''){
    this.isexcel=true;
    setTimeout(function(){
      this.todaysDate = new Date();
      var downloadLink;
      var dataType = 'application/vnd.ms-excel';
      var tableSelect = document.getElementById(tableID);
      var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
      
      // Specify file name
      filename='Late-Coming-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a');
      filename = filename?filename+'.xls':'excel_data.xls';
      
      // Create download link element
      downloadLink = document.createElement("a");
      
      document.body.appendChild(downloadLink);
      
      if(navigator.msSaveOrOpenBlob){
          var blob = new Blob(['\ufeff', tableHTML], {
              type: dataType
          });
          navigator.msSaveOrOpenBlob( blob, filename);
      }else{
          // Create a link to the file
          downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
          // Setting the file name
          downloadLink.download = filename;
          //triggering the function
          downloadLink.click();
      }
    },3000)
  }
  capturePdf() {
     this.isexcel=false;
     setTimeout(function(){
      var doc = new jsPDF('p', 'mm', 'a4');
      let user = sessionStorage.getItem('loggedUser');
     this.loggedUser = JSON.parse(user);
     this.compName = this.loggedUser.compName;
     this.fromDate = sessionStorage.getItem("fromDate");
     this.toDate = sessionStorage.getItem("toDate");
      doc.autoTable({
          html: '#late_employees',
          columnStyles: {
            1: {halign:'center'},
            2: {halign:'left'},
            3: {halign:'left'},
            4: {halign:'left'},
            5: {halign:'left'},
          },
          tableLineColor: [190, 191, 191],
          tableLineWidth: 0.75,
          headStyles : {
            fillColor: [103, 132, 130],
          },
          styles: {
            halign: 'center',
            cellPadding: 0.5, fontSize: 6
          },
          theme: 'grid',
          pageBreak:'avoid',
          margin: { top: 20,  bottom: 20 }
        });
        var pageCount = doc.internal.getNumberOfPages();
        for (let i = 0; i < pageCount; i++) {
          doc.setPage(i);
          doc.setTextColor(0, 0, 0);
          doc.setFontSize(9);
          doc.setFontStyle("Arial");
          doc.setTextColor(48, 80, 139);
          doc.text(80, 7, ""+this.compName);
          doc.text(90, 11, 'Late Coming Report');
          doc.setFontSize(7);
          doc.text(80, 15 ,'From Date' +' : '+moment(this.fromDate).format("DD/MMM/YYYY") + ' ' +' To ' + ' : ' + moment(this.toDate).format("DD/MMM/YYYY"));
          doc.setFontSize(7);
          doc.text(150, 15, "Report Date:"+moment().format('DD-MM-YYYY h:mm:ss a'));
          doc.text(180, 7, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
        }
      doc.save('Late-Coming-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a')+'.pdf');
    },3000)  
  }
  getTimeFromMins(mins) {
    if(mins){
      return Math.floor(mins / 60) + ':' + mins % 60;
    }
  }

  getInTime(timestamp){
    if(timestamp!=null){
      let temp_out_time= timestamp.split(":");
      this.in_time = temp_out_time[0]+":"+temp_out_time[1];
      return this.in_time;
    }
  }

  getOutTime(timestamp){
    if(timestamp!=null){
      let temp_out_time= timestamp.split(":");
      this.out_time = temp_out_time[0]+":"+temp_out_time[1];
      return this.out_time;
    }
   
  }
}
