import { Routes, RouterModule } from '@angular/router';
import { LatecomingReportComponent } from './latecoming-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: LatecomingReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'latecomingreports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'latecomingreports',
                    component: LatecomingReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);