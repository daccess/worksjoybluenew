import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatecomingReportComponent } from './latecoming-report.component';

describe('LatecomingReportComponent', () => {
  let component: LatecomingReportComponent;
  let fixture: ComponentFixture<LatecomingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatecomingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatecomingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
