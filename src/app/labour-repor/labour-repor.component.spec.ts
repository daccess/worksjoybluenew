import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabourReporComponent } from './labour-repor.component';

describe('LabourReporComponent', () => {
  let component: LabourReporComponent;
  let fixture: ComponentFixture<LabourReporComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabourReporComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabourReporComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
