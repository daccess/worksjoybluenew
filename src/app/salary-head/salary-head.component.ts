import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, NgZone, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { RoleMasterService } from '../shared/services/RoleMasterService';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-salary-head',
  templateUrl: './salary-head.component.html',
  styleUrls: ['./salary-head.component.css']
})
export class SalaryHeadComponent implements OnInit {
  themeColor: string = "nav-pills-blue";
  baseurl = MainURL.HostUrl;
  Type: any;
  salaryHead: any;
  dataPushed: boolean = false;
  isEditFlag:boolean=false;
  adddataInTable: any=[];
  salaryHeadName: any;
  salaryType: any;
  salaryHeadForm: any;
  user: string;
  users: any;
  token: any;
  allheadData: any;
  singleSalaryHead: any;
  getByuId: any;
  salaryHeadIds: any;
  idCounter: number = 1;
  salaryDatas: any;
  allheadDatas: any;

  constructor(private httpSer:HttpClient,public Spinner :NgxSpinnerService,private toastr:ToastrService,private roleservice:RoleMasterService,private zone: NgZone) { }

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token;
    this.getsalaryHead();
  }
  addSalaryhead(){
    
    let obj:any={
      "id":this.idCounter++,
      "salaryHeadName":this.salaryHeadName,
      "salaryType":this.salaryType
    }
    this.adddataInTable.push(obj);
    this.dataPushed = true;
    console.log("tdfasd",this.adddataInTable)
    // obj.salaryHeadName='',
    // obj.salaryType=''
    obj='';
   this.salaryHeadName='';
   this.salaryType='';
  }
  deleteRoleHeads(id: number){
    // this.adddataInTable.splice(index, 1);
    const index = this.adddataInTable.findIndex(item => item.id === id);
    if (index !== -1) {
      this.adddataInTable.splice(index, 1);
    }
  }

  submitSalaryHead(){
   
    if(this.isEditFlag==false){
      this.addSalaryhead()
var obj={
  "salaryHeadList":this.adddataInTable,
}
// this.myObject.push(obj)
console.log(obj)
  let url = this.baseurl + '/createSalaryHead';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.Spinner.show()
this.httpSer.post(url,obj,{headers}).subscribe(data => {
  
  this.Spinner.hide();

  this.toastr.success('salary head added Successfully');
  this.resetForm();
  
},
(err: HttpErrorResponse) => {
  this.Spinner.hide();
  this.toastr.error('Server Side Error..!');
});
    }
    else if(this.isEditFlag==true){
    
    let obj:any={
      // "salaryHeadList":this.adddataInTable,
      "salaryHeadName":this.salaryHeadName,
      "salaryType":this.salaryType,
      "salaryHeadId":this.getByuId,
    }
    // this.myObject.push(obj)
    console.log(obj)
      let url = this.baseurl + '/UpdateSalaryHead';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.Spinner.show()
    this.httpSer.put(url,obj,{headers}).subscribe(data => {
      this.isEditFlag=false
      this.Spinner.hide();
    
      this.toastr.success('salary head update Successfully');
      
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!');
    });
        
    }
}


getsalaryHead(){

let url = this.baseurl + '/getAllSalaryHead';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpSer.get(url,{headers}).subscribe(data => {
this.allheadData=data['result']
})
}
deleteRequest(e:any){
this.salaryHeadIds=e.salaryHeadId
  let url = this.baseurl + `/deleteSalaryHead/${this.salaryHeadIds}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpSer.delete(url,{headers}).subscribe(data => {
  this.toastr.success("salaryHead delete successfully")
  this.getsalaryHead();
  })
  }
  // Add this method to your component
refreshList() {
  this.getsalaryHead();
}

  resetForm() {
    // Reset form fields and any other necessary variables
    this.salaryHeadName = '';
    this.salaryType = '';
    this.adddataInTable = [];
    this.dataPushed = false;
    // Add any other form reset logic here
  
    // If you want to clear validation errors as well, you can reset the form state
    if (this.salaryHeadForm) {
      this.salaryHeadForm.resetForm();
    }
  }

// getEditSingleRecord(e:any){
//   document.getElementById('extra-apply-tab').click();
//   if(e.salaryHeadId){
//     this.isEditFlag=true
//   this.getByuId=e.salaryHeadId
//   let url = this.baseurl + `/getAllSalaryHead/${this.getByuId}`;
// const tokens = this.token;
// const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
// this.httpSer.get(url,{headers}).subscribe(data => {

// this.allheadData=data;
// // this.singleSalaryHead=this.allheadData.result;
// this.salaryHeadName=this.allheadData.result.salaryHeadName;
// this.salaryType=this.allheadData.result.salaryType;
// })
//   }


// }

getEditSingleRecord(e: any) {
  document.getElementById('extra-apply-tab').click();
  if (e.salaryHeadId) {
    this.isEditFlag = true;
    this.getByuId = e.salaryHeadId;
    let url = this.baseurl + `/getAllSalaryHead/${this.getByuId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url, { headers }).subscribe(data => {
      this.allheadDatas = data;
    this.salaryDatas=  this.allheadDatas.result;
      // If allheadData is an object and not an array, consider updating the template accordingly.
      // For example, if you are using *ngFor, you might need to iterate over Object.keys(allheadData) or similar.
      this.salaryHeadName = this.salaryDatas.salaryHeadName;
      this.salaryType = this.salaryDatas.salaryType;
    });
  }
}

}
