import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgcFloatButtonModule } from 'ngc-float-button';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { ExcelServiceService } from './shared/services/excel-service.service';
import { LoginService } from './shared/services/loginservice';
import { CompanyMasterService } from './shared/services/companymasterservice';
import { SpeechRecognitionService } from './speech-recognition.service';
import { ChatService } from '../app/shared/services/chatbotservice';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material';
import { routing } from '../app/Routing/routing';
import { AppComponent } from './app.component';
import { LoginComponent } from '../app/login/login.component';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SearchPipe } from './search.pipe';
import { SessionService } from './shared/services/SessionSrvice';
import { HomeComponent } from './home/home.component';
import { EmpdashboardComponent } from './empdashboard/empdashboard.component';
import { EmpDashSerivcService } from './shared/services/EmpDashboardService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ChangePassService } from './shared/services/ChangPassService';
import { SearchPipeForEmpDash } from './empdashboard/SearchPipe/Serach';
import { SearchPipeForlayout } from './layout/SearchPipe/SerachPipe';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ShContextMenuModule } from 'context-menu-angular6';
import { LayOutService } from './shared/model/layOutService';
import { BarChartComponent } from './empdashboard/Chart/bar-chart/bar-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { MessagingService } from './shared/services/messaging.service';
import { AuthGuard } from './guards/auth-guard.service';
import { RoleGuard } from './guards/role-guard.service';
import { DeviceMasterService } from './shared/services/deviceMasterService';
import { ReportopenComponent } from './reportopen/reportopen.component';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { RouteReuseStrategy } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CalendarService } from './shared/services/calendar.service';
import { SharedModule } from './shared/shared.module';
import { ExcelService } from './absentreport/excel.service';
import { EmpconsumedleaveschartComponent } from './empdashboard/Chart/empconsumedleaveschart/empconsumedleaveschart.component';

import { RoleMasterService } from './shared/services/RoleMasterService';
import { departmentMasterService } from './shared/services/departmentmasterservice';
import { EmpleaveavgdaywiseComponent } from './empdashboard/Chart/empleaveavgdaywise/empleaveavgdaywise.component';
import { EmpleaveavgmonthwiseComponent } from './empdashboard/Chart/empleaveavgmonthwise/empleaveavgmonthwise.component';
import { UploadDocumentComponent } from './upload-document/upload-document.component';
import { ProvisionalpassComponent } from './provisionalpass/provisionalpass.component';
import { SalaryHeadComponent } from './salary-head/salary-head.component';
import { SafetyRemarkComponent } from './safety-remark/safety-remark.component';
import { EditcontractorlabourComponent } from './editcontractorlabour/editcontractorlabour.component';
import { GetOriginalPassComponent } from './get-original-pass/get-original-pass.component';
import { LabourApproveRejectListComponent } from './labour-approve-reject-list/labour-approve-reject-list.component';
import { HrDashboardComponent } from './hr-dashboard/hr-dashboard.component';
import { HodDashboardComponent } from './hod-dashboard/hod-dashboard.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ContractorDashboardComponent } from './contractor-dashboard/contractor-dashboard.component';
import { SupervisorDashboardComponent } from './supervisor-dashboard/supervisor-dashboard.component';
import { LabourReporComponent } from './labour-repor/labour-repor.component';
import { AllReportsComponent } from './all-reports/all-reports.component';
import { SignaturePadComponent } from './signature-pad/signature-pad.component';
import { HodApprovalsComponent } from './hod-approvals/hod-approvals.component';
import { SupervisorLabourListComponent } from './supervisor-labour-list/supervisor-labour-list.component';

 @NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    SearchPipe,
    HomeComponent,
    EmpdashboardComponent,
    SearchPipeForEmpDash,
    SearchPipeForlayout,
    BarChartComponent,
    ReportopenComponent,
    EmpconsumedleaveschartComponent, 
    EmpleaveavgdaywiseComponent,
    EmpleaveavgmonthwiseComponent,
    UploadDocumentComponent,
    ProvisionalpassComponent,
    SalaryHeadComponent,
    SafetyRemarkComponent,
    EditcontractorlabourComponent,
    GetOriginalPassComponent,
    LabourApproveRejectListComponent,
    HrDashboardComponent,
    HodDashboardComponent,
    AdminDashboardComponent,
    ContractorDashboardComponent,
    SupervisorDashboardComponent,
    LabourReporComponent,
    AllReportsComponent,
    SignaturePadComponent,
    HodApprovalsComponent,
    SupervisorLabourListComponent,



  ],
  imports: [
  
    OwlDateTimeModule, 
   OwlNativeDateTimeModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    routing,
    BrowserAnimationsModule,
    MatToolbarModule, 
    MatSidenavModule,
    
    NgcFloatButtonModule,
    ToastrModule.forRoot( {    
      preventDuplicates: true,
      autoDismiss: true
    }),
    
    NgMultiSelectDropDownModule.forRoot(),
    NgxSpinnerModule,
    ShContextMenuModule,
  ChartsModule,
    NgxChartsModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgbModule,
    SharedModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },MessagingService, AsyncPipe,EmpDashSerivcService,XlsxToJsonService,ExcelServiceService,
    ChangePassService,LayOutService, LoginService,CompanyMasterService,SpeechRecognitionService,
    ChatService,SessionService,AuthGuard, RoleGuard,DeviceMasterService,CalendarService,DatePipe,ExcelService,RoleMasterService,departmentMasterService],
  bootstrap: [AppComponent],

})
export class AppModule { }
