import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DeviceMasterService } from 'src/app/shared/services/deviceMasterService';
import { Assetsmaster } from 'src/app/shared/model/Assetsmaster';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { MainURL } from 'src/app/shared/configurl';
import { ToastrService } from 'ngx-toastr';
// import { url } from 'inspector';

@Component({
  selector: 'app-addasset',
  templateUrl: './addasset.component.html',
  styleUrls: ['./addasset.component.css']
})
export class AddassetComponent implements OnInit {
  public assetsMasterArray: any;
  baseurl = MainURL.HostUrl;
  public assetsMaster: Assetsmaster; 
  
  isEdit : boolean = false;
  loggedUser: any;
  locationData = [];
  dropdownSettings: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  constructor(private devicemasterService: DeviceMasterService, public httpService: HttpClient,
    public toastr: ToastrService,  public chRef: ChangeDetectorRef) {
    this.assetsMaster = new Assetsmaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
  }
  ngOnInit() {
    this.assetsMaster.assetCategory = "";
    this.assetsMaster.status = "";
    this.assetsMaster.location = "";
    let date = new Date();
    this.assetsMaster.purchasedOn = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
    this.getAssetsmasterList();
    this.getAllLocations();

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }

  dataTableFlag: boolean = true;
  getAssetsmasterList() {
    this.dataTableFlag = false;
    this.devicemasterService.getAssertMasterServiceList().subscribe(data => {
      this.assetsMasterArray = data.json().result;
      this.dataTableFlag = true;
      setTimeout(function () {
        $(function () {
          var table = $('#CompTable').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();      
    }, err => { 
      this.dataTableFlag = true
    })
  }
  dateChanged(event){
   this.assetsMaster.purchasedOn = event;
  }
  onSubmit(f) {
    this.assetsMaster.locationId = parseInt(this.assetsMaster.location);
    if(this.isEdit){
      this.updateAsset(f);
    }
    else{
      this.devicemasterService.postServices(this.assetsMaster).subscribe(data => {
        this.getAssetsmasterList();
        f.reset();
        this.resetForm();
        this.isEdit = false;
        this.assetsMaster=new Assetsmaster();         
        this.toastr.success('Asset Added Successfully');
      }, err => {
        this.toastr.error('Failed To Add Asset');
      })
    }
    
  }

  updateAsset(f) {
    this.devicemasterService.putServices(this.assetsMaster).subscribe(data => {
      this.getAssetsmasterList();
      f.reset();
      this.resetForm();
      this.isEdit = false;
      this.assetsMaster=new Assetsmaster();
      this.toastr.success('Asset Updated Successfully');
    },err=>{
      this.toastr.error('Failed To Updated Asset');
    })
  }

  resetForm(){
    setTimeout(() => {
      this.assetsMaster.assetCategory = "";
      this.assetsMaster.status = "";
      this.assetsMaster.location = "";
      let date = new Date();
      this.assetsMaster.purchasedOn = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
    }, 100);
  }

  getAssetMasterById(id) {
    this.devicemasterService.getServicesById(id.assetId).subscribe(data => {
      this.isEdit = true;
      if (data.json().result) {
        this.assetsMaster = data.json().result[0];
        let date = new Date(data.json().result[0].purchasedOn);
        this.assetsMaster.purchasedOn = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
      }
    }, err => {

    })

  }

  assetId : any;
  delete(item) {
    this.dataToDelete = item;
    this.assetId = item.assetId;
  }
  dataToDelete ={assetId:""};
  deleteRecord(f){
    this.devicemasterService.deleteServices(this.dataToDelete.assetId).subscribe(data => {
      this.getAssetsmasterList();
      f.reset()
      this.resetForm
      this.toastr.success('Delete Successfully');
    }, err => {
      this.toastr.error('Failed  Delete Asset');
    })
  }
  getAllLocations(){
    this.devicemasterService.getLocations(this.loggedUser.compId).subscribe(data=>{
      this.locationData=data.json().result;
    },err=>{  
    })
  }
  reset(){
    this.isEdit = false
    this.resetForm()
    //this.StatusFlag= false;
  }
}
