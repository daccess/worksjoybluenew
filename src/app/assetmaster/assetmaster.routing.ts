import { Routes, RouterModule } from '@angular/router'
import { AssetmasterComponent } from './assetmaster.component';
import { AddassetComponent } from './pages/addasset/addasset.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: AssetmasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'assetmaster',
                    pathMatch :'full'
                    
                },
                {
                    path:'assetmaster',
                    component: AddassetComponent
                }

            ]

    }
]

export const routing = RouterModule.forChild(appRoutes);