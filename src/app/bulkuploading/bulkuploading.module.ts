import { routing } from './bulkuploading.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { BulkuploadingComponent } from './bulkuploading.component';
import { BulkUploadService } from '../shared/services/BulkUploadService';

@NgModule({
    declarations: [
        BulkuploadingComponent
    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule
    ],
    providers: [BulkUploadService]
  })
  export class BulkuploadingModule { 
      constructor(){

      }
  }
  