import { Routes, RouterModule } from '@angular/router'
import { BulkuploadingComponent } from './bulkuploading.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: BulkuploadingComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'bulkuploading',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'bulkuploading',
                    component: BulkuploadingComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);