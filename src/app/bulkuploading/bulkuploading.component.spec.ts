import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkuploadingComponent } from './bulkuploading.component';

describe('BulkuploadingComponent', () => {
  let component: BulkuploadingComponent;
  let fixture: ComponentFixture<BulkuploadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkuploadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkuploadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
