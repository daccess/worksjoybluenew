import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { XlsxToJsonService } from '../xlsx-to-json-service';
import { ToastrService } from 'ngx-toastr';
import { BulkUpload } from '../shared/model/BulkUploadModel';
import { BulkUploadService } from '../shared/services/BulkUploadService';
import { NgxSpinnerService } from 'ngx-spinner';
import { formattedError } from '@angular/compiler';
import * as moment from 'moment';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { departmentmodel } from '../shared/model/departmentmastermodel';
import { HttpErrorResponse } from '@angular/common/http';
import { hidden } from '@angular-devkit/core/src/terminal';
// import { HttpErrorResponse } from '@angular/common/http';




@Component({
  selector: 'app-bulkuploading',
  templateUrl: './bulkuploading.component.html',
  styleUrls: ['./bulkuploading.component.css']
})
export class BulkuploadingComponent implements OnInit {
  deptmodal: departmentmodel;
  bulkUpload: BulkUpload;
  fileNamee: any;
  documentdata: any = [];
  leaveErrorMessage: any;
  mesg: any;
  fileNames: any;
    masterData = [];
  mesage: any;
  loggedUser: any;
  constructor(private cdRef: ChangeDetectorRef, private formBuilder: FormBuilder,
    private toastr: ToastrService, private bulkUploadService: BulkUploadService, public Spinner: NgxSpinnerService) {
    this.bulkUpload = new BulkUpload();
    this.deptmodal = new departmentmodel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
  }
  commonList: any = [];
  leaveBalList: any = [];
  holidayList: any = [];
  leaveBalDeductionList: any = [];
  organisationMasterList: any = [];
  companyList: any = [];
  lateList: any = [];
  earlyList: any = [];
  commonList_EG = [];
  date: any;
  fileName_EG: any;
  exceldata1 = [];
  datarow = [];
  datarow_EG =[];
  datarow1 = [];
  

  ngOnInit() {
    this.buttFlag = false;
    this.getallDocument();
    const controls = this.orders.map(c => new FormControl(false));
    this.form = this.formBuilder.group({
      orders: new FormArray(controls, minSelectedCheckboxes(1))
    });
  }
  showfile: boolean = false;
  selectedFile: File;
  buttFlag: boolean = false;
  onFileChanged(event) {
    this.fileNamee = event.target.value.replace(/^.*[\\\/]/, '');
    this.showfile = true;
    this.selectedFile = event.target.files[0];
    this.buttFlag = true;
  }

  uploadType: string = 'leavebalance';
  fileName: any;
  leaveBalListFun() {
    this.uploadType = 'leavebalance';
    this.leaveBalList = [];
    this.leaveBalList = [
    { title: "Current Leaves", name: "currentLeaves" },
    { title: "Opening Balance", name: "openingBalance" },
    // { title: "Total Leaves", name: "totalLeaves" },
    { title: "Used Leaves", name: "usedLeaves" },
    { title: "Emp Id", name: "empId" },
    { title: "Pattern Name", name: "patternName"}];

    this.datarow1 = [
      {title:"5", name:"currentLeaves"},
      {title:"6", name:"openingBalance"},
      // { title: "6", name: "totalLeaves" },
      { title: "1", name: "usedLeaves" },
      { title: "509", name: "empId" },
      { title: "CL", name: "patternName"}]

    this.commonList = [];
    this.commonList = this.leaveBalList;
    this.fileName = 'leaveBalance';

  }

  holidayListFun() {
    this.uploadType = 'holiday';
    this.holidayList = [];
    this.holidayList = [{ title: 'Holiday Name', name: 'holiDayName' },
    { title: 'Type Of HoliDay', name: 'typeOfHoliDay' },
    { title: 'HoilDay Desc', name: 'hoilDayDesc' },
    { title: 'Start Date', name: 'startDate' },
    { title: 'End Date', name: 'endtDate' },
    { title: 'Type Of Half Day', name: 'typeOfHalfDay' },
    { title: 'Holiday Type', name: 'holidayType' },
    { title: 'Location Name', name: 'locationName' }];
    this.commonList = [];
    this.commonList = this.holidayList;
    this.fileName = 'holiday';
  }

  // leaveBalDeductionListFun() {
  //   this.uploadType = 'leavebalancededuction';
  //   this.leaveBalDeductionList = [];
  //   this.leaveBalDeductionList = [{ title: 'Employee Id', name: 'empId' },
  //   { title: 'From Date', name: 'fromDate' },
  //   { title: 'To Date', name: 'toDate' },
  //   { title: 'Leave Type', name: 'leaveType' },
  //   { title: 'Request Type', name: 'requestType' }];
  //   this.commonList = [];
  //   this.commonList = this.leaveBalDeductionList;
  //   this.fileName = 'leaveBalDeduction';
  // }

  // EmpBulkDocuments() {
  //   this.uploadType = 'bulkdocuments';
  // }

  EmpBulkLateEarly() {
    this.uploadType = 'bulklateearly';

    this.lateList = [];
    this.earlyList = [];
    this.lateList = [{ title: 'Employee Id', name: 'empId' },
    { title: 'Employee Name', name: 'empName' },
    { title: 'Date', name: 'lateEarlyDate' },
    ];
    this.earlyList = [{ title: 'Employee Id', name: 'empId' },
    { title: 'Employee Name', name: 'empName' },
    { title: 'Date', name: 'lateEarlyDate' },
    ];
    this.datarow = [{ title: '2', name: 'empId' },
    { title: 'Naramata Pawar', name: 'empName' },
    { title: '2020-12-03', name: 'lateDate' }, ];
    this.datarow_EG = [{ title: '2', name: 'empId' },
    { title: 'Naramata Pawar', name: 'empName' },
    { title: '2020-12-03', name: 'EarlyDate' }, ];
    this.commonList = [];
    this.commonList_EG = [];
    this.commonList = this.lateList;
    this.commonList_EG = this.earlyList;
    this.fileName = 'date_LT';
    this.fileName_EG = 'date_EG';
  }

  submit_LT() {
    const selectedOrderIds = this.commonList
      .map((v, i) => v ? this.commonList[i].name : null)
      .filter(v => v !== null);

    const selectdataIds = this.datarow.map((v, i) => v ? this.datarow[i].title : null)
      .filter(v => v !== null);


    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Sheet1');

    // Add Header Row
    const headerRow = worksheet.addRow(selectedOrderIds);
    const dataRow = worksheet.addRow(selectdataIds);

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });

    dataRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });
    // Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data: ArrayBuffer) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      // fs.saveAs(blob, 'EmployeeDataExport.xlsx');
      fs.saveAs(blob, this.fileName + '.xlsx');

    });

  }
  submit_EG() {
    const selectedOrderIds = this.commonList_EG
      .map((v, i) => v ? this.commonList_EG[i].name : null)
      .filter(v => v !== null);
    const selectdataIdsEG = this.datarow_EG.map((v, i) => v ? this.datarow_EG[i].title : null)
      .filter(v => v !== null);

    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Sheet1');

    // Add Header Row
    const headerRow = worksheet.addRow(selectedOrderIds);
    const dataRow = worksheet.addRow(selectdataIdsEG);

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });
    dataRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });




    // Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data: ArrayBuffer) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      // fs.saveAs(blob, 'EmployeeDataExport.xlsx');
      fs.saveAs(blob, this.fileName_EG + '.xlsx');

    });

  }

  submit() {
    const selectedOrderIds = this.commonList
      .map((v, i) => v ? this.commonList[i].name : null)
      .filter(v => v !== null);

    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Sheet1');

    // Add Header Row
    const headerRow = worksheet.addRow(selectedOrderIds);

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });


    // Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data: ArrayBuffer) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      // fs.saveAs(blob, 'EmployeeDataExport.xlsx');
      fs.saveAs(blob, this.fileName + '_' + new Date().getFullYear() + '.xlsx');

    });
  }

  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  // bulkUpload : any;
  handleFile() {
    let string1 = this.selectedFile.name;
    let fileNameLateEarly = this.fileNameLateEarly;
    string1 = string1.split('_')[0];
    if (string1 == 'holiday') {
      this.Spinner.show();
      const file = this.selectedFile;
      this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
        this.bulkUpload.importHolidayMasterList = (data['sheets'].Sheet1);

        if (this.bulkUpload.importHolidayMasterList != null) {
          this.bulkUpload.compId = 1;
          this.bulkUploadService.postHolidayExcel(this.bulkUpload).subscribe(data => {
            if (data.status == 200) {
              this.toastr.success('Excel Import Succcessfully');
              this.Spinner.hide();
              this.showfile = false;
              $('#File1').val('');
              this.buttFlag = false;
            } else {
              this.toastr.success('Excel Fail To Import');
            }
          });
        } else {
          this.toastr.error('Excel Fail To Import');
          this.Spinner.hide();

        }
      });

    }
    if (string1 == 'leaveBalance') {
      this.Spinner.show();
      const file = this.selectedFile;

      this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
        this.bulkUpload.importLeaveBalanceList = (data['sheets'].Sheet1);
        if (this.bulkUpload.importLeaveBalanceList != null) {
          this.bulkUpload.compId = 1;
          this.bulkUploadService.postLeaveBalExcel(this.bulkUpload)
            .subscribe(data => {
            
              this.mesage=data.message
              if(this.mesage=='Leave uploaded Successfully!!'){
              this.toastr.success('Excel Import Succcessfully');
              this.Spinner.hide();
              $("#File1").val('');
              this.buttFlag = false
              this.showfile = false
              }
              else if(this.mesage=='Employee Id  not exist!!'){
                this.toastr.error('Employee Id  not exist!!')
                this.Spinner.hide();
              }
              else if(this.mesage=='Pattern name does not exist!!'){
                this.toastr.error(this.mesage);
                this.Spinner.hide();
              }
              else if(data.statusCode==404){
                this.toastr.error('error to  upload Leave!!')
                this.Spinner.hide();
              }
            },(err: HttpErrorResponse) => {
              this.toastr.error('Employee Id  not exist!!');
              this.Spinner.hide();
            });
          
        }
        
      
        else {
          this.toastr.error('Excel Fail To Import');
          this.Spinner.hide();
        }


      });

    }
    if (string1 == 'leaveBalDeduction') {
      this.Spinner.show();
      const file = this.selectedFile;
      this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
        this.bulkUpload.importLeaveBalDeductionList = (data['sheets'].Sheet1);
        if (this.bulkUpload.importLeaveBalDeductionList != null) {
          this.bulkUpload.compId = 1;
          this.bulkUploadService.postLeaveBalDeductionExcel(this.bulkUpload)
            .subscribe(data => {
              this.toastr.success('Excel Import Succcessfully');
              this.Spinner.hide();
              $('#File1').val('');
              this.showfile = false;
              this.buttFlag = false;
            });
        } else {
          this.toastr.error('Excel Fail To Import');
          this.Spinner.hide();
        }
      });

    }




  }

  getallDocument() {
    this.bulkUploadService.getallDocument().subscribe(data => {
      this.documentdata = data['result'];
    }, err => {
    });
  }

  fileTitle: string = "Select Documents";
  notUploadedDocsEmp: any = [{}];
  tempObj = [];
  selectedDcsForUpload: any = [];
  bulkDocFlag: boolean = false;
  docolderName: string = "";
  docsUpload(event) {

    this.selectedDcsForUpload = event;
    if (event.length > 0) {
      this.bulkDocFlag = true;
      const lastIndexST = event[0].webkitRelativePath.split('/');
      this.docolderName = lastIndexST[0];
    }
    this.fileTitle = event.length + 'Documents Selected';
  }
  async fileSelected() {
    const event = this.selectedDcsForUpload;

    let obj = new Array();
    this.Spinner.show();
    for (let i = 0; i < event.length; i++) {

      this.bulkUploadService.postFile(event[i]).then((data: any) => {
        data = data.json();
        const lastIndexST = event[i].webkitRelativePath.split('/');
        const empId = lastIndexST[1].split('-')[0].trim();
        const item = {
          docName: event[i].name,
          docType: event[i].name,
          empPerId: empId,
          docUrl: data.result.imageUrl,
          documentMasterId: 0
        };
        for (let j = 0; j < this.documentdata.length; j++) {
          if (this.documentdata[j].documentName == event[i].name.split('.')[0]) {
            item.documentMasterId = this.documentdata[j].documentMasterId;
          }
        }

        this.tempObj.push(item);
        if (i == event.length - 1) {
          setTimeout(() => {
            const docs = {
              empDocMasterList: this.tempObj
            };
            this.notUploadedDocsEmp = [{}];
            this.bulkUploadService.postBulkDocuments(docs).subscribe(data => {
              this.Spinner.hide();
              this.bulkDocFlag = false;
              this.selectedDcsForUpload = [];
              this.docolderName = '';

              this.toastr.success('documents saved successfully');
            }, err => {
              this.Spinner.hide();
              this.notUploadedDocsEmp = JSON.parse(JSON.stringify(err.json())).result;
              if (this.notUploadedDocsEmp[0].message) {
                this.toastr.error('Can not upload documents , please try again');
              } else {
                let objData = new Array();
                objData = this.notUploadedDocsEmp;
                this.toastr.error('some documents are not uploaded , please check list');

              }
              setTimeout(() => {
                document.getElementById('openModalButton1').click();
              }, 100);
            });
          }, 25000);

        }
      }, err => {
        this.notUploadedDocsEmp = [{}];
        this.Spinner.hide();
        document.getElementById('openModalButton1').click();
      });

    }
  }
  fileNameLateEarly: string;
  showfileLateEarly: boolean = false;
  selectedFileLateEarly: any;
  buttFlagLateEarly: boolean = false;

  onLAteEarlyFileChanged(event) {

    this.fileNameLateEarly = event.target.value.replace(/^.*[\\\/]/, '');
    this.showfileLateEarly = true;
    this.selectedFileLateEarly = event.target.files[0];
    this.buttFlagLateEarly = true;
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }
  uploadBulkLateEarly(filename) {
   
    const temp = filename.split('.');
    const obj = {
      fileName: temp[0],
      lateAndEarlyUploadingList: [
        {
          empId: '',
          empName: '',
          lateEarlyDate: ''
        }
      ]
    };
    const file = this.selectedFileLateEarly;
    this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
      this.exceldata1 = (data['sheets'].Sheet1);
      if (this.exceldata1 == null) {
        this.toastr.error('Excel file is empty');
        this.fileNameLateEarly = '';
        this.Spinner.hide();
      } else {

      }
      obj.lateAndEarlyUploadingList = this.exceldata1;
      for (let i = 0; i < obj.lateAndEarlyUploadingList.length; i++) {
        obj.lateAndEarlyUploadingList[i].lateEarlyDate = moment(obj.lateAndEarlyUploadingList[i].lateEarlyDate).format("YYYY-MM-DD");
      }
      if (obj.lateAndEarlyUploadingList != null) {
        this.Spinner.show();
        this.bulkUploadService.postBulkLateEarly(obj).subscribe(data => {
          if (data.statusCode == 201) {
            this.toastr.success('Excel Imported Succcessfully');
            this.fileNameLateEarly = '';
            this.Spinner.hide();
            $('#File3').val('');
          }
        }, 
        err => {
          this.Spinner.hide();
          // this.toastr.error('Excel Uploaded Failed');
          if (err.json()) {
            this.leaveErrorMessage = err.json().result;
            if (this.leaveErrorMessage['message'] != null) {
              this.mesg = this.leaveErrorMessage['message'];
              this.toastr.error(this.mesg);
              this.fileNameLateEarly = '';
              $('#File3').val('');
              this.Spinner.hide();
            }
          } else {
            this.toastr.success('Excel Uploaded Failed Succcessfully');
          }

        });


      }
    });
  }
  form: FormGroup = new FormGroup({});
  orders = [];
  list: any = [];
  showorganisationFile: boolean = false;
  masterType: string;
  masterFileName: string;
  onChnageFun() {
    this.form.value.orders[0] = false;
    this.organisationMasterList = this.form.value.orders
      .map((v, i) => v ? this.orders[i] : null)
      .filter(v => v !== null);
  }
  masterTypeClicked(type) {
    this.orders = [];
    this.organisationMasterList = [];
    // if (type == 'bussinessmaster') {
    //   this.masterType = 'Bussiness Group Master';
    //   this.masterFileName = 'bussinessmaster';
    //   this.orders = [{ title: 'Bussiness Group Master' },
    //   // { id: 'companyName', name: 'Company Name' , field : 'required' },
    //   { id: 'bussinessGroupName', name: 'Bussiness Group Name', field: 'required' },
    //   { id: 'bussinessGroupNameDescription', name: 'Bussiness Group Name Description' }];
    //   this.form.value.orders = [false, true, false];
    // }
    // if (type == 'functionmaster') {
    //   this.masterType = 'Function Unit Master';
    //   this.masterFileName = 'functionmaster';
    //   this.orders = [{ title: ' Function Unit Master' },
    //   // { id: 'companyName', name: 'Company Name' , field : 'required' },
    //   { id: 'selectBussinessGroup', name: 'Bussiness Group', field: 'required' },
    //   { id: 'functionName', name: 'Function Name', field: 'required' },
    //   { id: 'functionNameDescription', name: 'Function Name Description' }];
    //   this.form.value.orders = [false, true, true, false];
    // }
    if (type == 'departmentmaster') {
      this.masterType = 'Department Master';
      this.masterFileName = 'departmentmaster';
      this.orders = [{ title: 'Department Master' },
      { id: 'companyName', name: 'Company Name' , field : 'required' },
      // { id: 'selectBussinessGroup', name: 'Bussiness Group', field: 'required' },
      // { id: 'selectFunctionUnit', name: 'Function Unit', field: 'required' },
      { id: 'departmentCode', name: 'Department Code', field: 'required' },
      { id: 'departmentName', name: 'Department Name', field: 'required' },
      { id: 'departmentDescription', name: 'Department Description' }];
      this.form.value.orders = [false, true, true, true, false];

    }
    if (type == 'subdepartmentmaster') {
      this.masterType = 'Sub Department Master';
      this.masterFileName = 'subdepartmentmaster';
      this.orders = [{ title: 'Sub Department Master' },
      { id: 'companyName', name: 'Company Name' , field : 'required' },
      // { id: 'selectBussinessGroup', name: ' Bussiness Group', field: 'required' },
      // { id: 'selectFunctionUnit', name: ' Function Unit', field: 'required' },
      { id: 'selectDepartment', name: ' Department', field: 'required' },
      { id: 'subDepartmentName', name: ' Sub Department Name', field: 'required' },
      { id: 'subDepartmentDescription', name: 'Sub-Department Description' }];
      this.form.value.orders = [false, true, true, true, true, false];
    }
    if (type == 'employeecategerymaster') {
      this.masterType = 'Employee Category Master';
      this.masterFileName = 'employeecategorymaster';
      this.orders = [{ title: 'Employee Category Master' },
      // { id: 'companyName', name: 'Company Name' , field : 'required' },
      { id: 'empCategoryName', name: 'Employee Categery Name', field: 'required' },
      { id: 'employeeCategoryDescription', name: 'Employee Category Description' },
      { id: 'noticePeriod', name: 'Notice Period In Days', field: 'required' }];
      this.form.value.orders = [false, true, false, true];
    }
    if (type == 'subemployeecategerymaster') {
      this.masterType = 'Sub Employee Categoery Master';
      this.masterFileName = 'subemployeecategerymaster';
      this.orders = [{ title: 'Sub Employee Category Master' },
      // { id: 'companyName', name: 'Company Name' , field : 'required' },
      { id: 'parentEmpCategery', name: 'Parent Employee Categery', field: 'required' },
      { id: 'subEmpCategeryName', name: 'Sub Employee Categery Name', field: 'required' },
      { id: 'subemployeeCategoryDescription', name: 'Sub-Employee Category Description' }];
      this.form.value.orders = [false, true, true, false];

    }
    // if (type == 'paygroupmaster') {
    //   this.masterType = 'Pay Group Master';
    //   this.masterFileName = 'paygroupmaster';
    //   this.orders = [{ title: 'Pay Group Master' },
    //   // { id: 'companyName', name: 'Company Name' , field : 'required' },
    //   { id: 'payGroupName', name: 'Pay Group Name', field: 'required' },
    //   { id: 'payGroupDescription', name: 'Pay Group Description' }];
    //   this.form.value.orders = [false, true, false];
    // }
    // if (type == 'grademaster') {
    //   this.masterType = 'Grade Master';
    //   this.masterFileName = 'grademaster';
    //   this.orders = [{ title: 'Grade Master' },
    //   // { id: 'companyName', name: 'Company Name' , field : 'required' },
    //   { id: 'gradeName', name: 'Grade Name', field: 'required' },
    //   { id: 'gradeDescription', name: 'Grade Description' },
    //   { id: 'gradeLevel', name: 'Grade Level' },
    //   { id: 'attendanceConsiderPrasentIf', name: 'Attendance Consider Present If ', field: 'required' }];
    //   this.form.value.orders = [false, true, false, false, true];
    // }
    if (type == 'designationmaster') {
      this.masterType = 'Designation Master';
      this.masterFileName = 'designationmaster';
      this.orders = [{ title: 'Designation Master' },
      // { id: 'companyName', name: 'Company Name' , field : 'required' },
      { id: 'designationName', name: 'Designation Name', field: 'required' },
      { id: 'designationDescription', name: 'Designation Description' },
      { id: 'designationLevel', name: 'Designation Level' }];
      this.form.value.orders = [false, true, false, false];
    }
    // if (type == 'costcentermaster') {
    //   this.masterType = 'Cost Center Master';
    //   this.masterFileName = 'costcentermaster';
    //   this.orders = [{ title: 'Cost Center Master' },
    //   // { id: 'companyName', name: 'Company Name' , field : 'required' },
    //   { id: 'costCenterCode', name: 'Cost Center Code', field: 'required' },
    //   { id: 'costCenterName', name: 'Cost Center Name', field: 'required' },
    //   { id: 'costCenterDescription', name: 'Cost Center Description' }];
    //   this.form.value.orders = [false, true, true, false];
    // }
    if (type == 'locationmaster') {
      this.masterType = 'Location Master';
      this.masterFileName = 'locationmaster';
      this.orders = [{ title: 'Location Master' },
      // { id: 'companyName', name: 'Company Name' , field : 'required' },
      { id: 'locationCode', name: 'Location Code', field: 'required' },
      { id: 'locationName', name: 'Location Name', field: 'required' },
      { id: 'locationAddress', name: 'Location Address' },
      { id: 'locationContactPerson', name: 'Location Contact Person' },
      { id: 'locationContactDetails', name: 'Location Contact Details' },
      { id: 'locationContactEmailId', name: 'Location Contact Email Id' }];
      this.form.value.orders = [false, true, true, false, false, false, false];

    }
    this.onChnageFun();
    const controls = this.orders.map(c => new FormControl(true));
    this.form = this.formBuilder.group({
      orders: new FormArray(controls, minSelectedCheckboxes(1))
    });

  }



  OrganisationMasterListFun() {
    this.uploadType = 'organisationmaster';
    if (this.masterType == 'department master') {

    }
  }

  submitMaster() {
    const fileName = this.masterFileName + '.xlsx';
    const selectedOrderIds = this.form.value.orders
      .map((v, i) => v ? this.orders[i].id : null)
      .filter(v => v !== null);
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Sheet1');

    // Add Header Row
    const headerRow = worksheet.addRow(selectedOrderIds);

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });


    // Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data: ArrayBuffer) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, fileName);

    });
  }
  fileNameOrganisation: boolean = false
  selectFile: File;
  orgaFlag: boolean = false

  onOrganisationMasterChanged(event) {
    this.masterFileName = event.target.value.replace(/^.*[\\\/]/, '');
    this.fileNameOrganisation = true;
    this.selectFile = event.target.files[0];
    this.orgaFlag = true;
    this.selectFile = event.target.files[0];
    console.log(this.selectFile);

  }

  uploadOrganisation() {
    const file = this.selectFile;
    this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
      this.masterData = (data['sheets'].Sheet1);

      // if (this.masterType == 'Bussiness Group Master') {
      //   const myObj = {
      //     bussinessGroupMasterBulkUploadReqDtoList: this.masterData
      //   };
      //   this.bulkUploadService.postBussiGroupMaster(myObj).subscribe(data => {
      //     if (data.statusCode = 201) {
      //       this.toastr.success('Excel Import Succcessfully');
      //       this.Spinner.hide();
      //     } else {
      //       this.toastr.error('Excel Fail To Import');
      //       this.Spinner.hide();
      //     }
      //   }, err => {
      //     this.Spinner.hide();
      //     if (err.json().result) {
      //       this.toastr.error(err.json().result);

      //     }
      //   });
      // } else if (this.masterType == 'Function Unit Master') {
      //   const myObj = {
      //     functionUnitMasterBulkUploadReqDtoList: this.masterData
      //   };
      //   this.bulkUploadService.postFuncUnitMaster(myObj).subscribe(data => {
      //     if (data.statusCode = 201) {
      //       this.toastr.success('Excel Import Succcessfully');
      //       this.Spinner.hide();
      //     } else if (data.statusCode = 204) {
      //       this.toastr.error(data.result);
      //       this.Spinner.hide();
      //     }
      //   }, err => {
      //     this.Spinner.hide();
      //     if (err.json().result) {
      //       this.toastr.error(err.json().result);

      //     }
      //   });
      // } else
       if (this.masterType == 'Department Master') {
        const myObj = {
          departmentMasterBulkUploadReqDtoList: this.masterData
        };
        this.Spinner.show()
        this.bulkUploadService.postDepartmentMaster(myObj).subscribe(data => {
          if (data.statusCode = 201) {
            this.toastr.success('Excel Import Succcessfully');
            this.Spinner.hide();
          }

          //  else {
          //   this.toastr.error(message);
          //   console.log("reeeeoee",da.body.result);
          //   this.Spinner.hide();
          //  }
        }, err => {
          this.Spinner.hide();
          if (err.json().result) {
            this.toastr.error(err.json().result);

          }
        });
      } else if (this.masterType == 'Sub Department Master') {
        const myObj = {
          subDepartmentMasterBulkUploadReqDtoList: this.masterData
        };
        this.Spinner.show();
        this.bulkUploadService.postSubDeptMaster(myObj).subscribe(data => {
          if (data.statusCode = 201) {
            this.toastr.success('Excel Import Succcessfully');
            this.Spinner.hide();
          } else {
            this.toastr.error('Excel Fail To Import');
            this.Spinner.hide();
          }
        }, err => {
          this.Spinner.hide();
          if (err.json().result) {
            this.toastr.error(err.json().result);
            // console.log("err.json().result", err.json().result);

          }
        });
      } else if (this.masterType == 'Employee Category Master') {
        const myObj = {
          employeeCategoryMasterBulkUploadReqDtoList: this.masterData,
          "compId":this.loggedUser.compId,

        };
      
        this.Spinner.show();
        this.bulkUploadService.postEmployeeCategoryMaster(myObj).subscribe(data => {
          if (data.statusCode = 201) {
            this.toastr.success('Excel Import Succcessfully');
            this.Spinner.hide();
          } else {
            this.toastr.error('Excel Fail To Import');
            this.Spinner.hide();
          }
        }, err => {
          this.Spinner.hide();
          if (err.json().result) {
            this.toastr.error(err.json().result);
            // console.log("err.json().result", err.json().result);

          }
        });
      } else if (this.masterType == 'Sub Employee Categoery Master') {
        const myObj = {
          subEmployeeCategoryMasterBulkUploadReqDtoList: this.masterData,
          "compId":this.loggedUser.compId,
        };
        this.Spinner.show();
        this.bulkUploadService.postSubEmpCategoryMaster(myObj).subscribe(data => {
          if (data.statusCode = 201) {
            this.toastr.success('Excel Import Succcessfully');
            this.Spinner.hide();
          } else {
            this.toastr.error('Excel Fail To Import');
            this.Spinner.hide();
          }
        }, err => {
          this.Spinner.hide();
          if (err.json().result) {
            this.toastr.error(err.json().result);
            // console.log("err.json().result", err.json().result);

          }
        });
      // } else if (this.masterType == 'Pay Group Master') {
      //   const myObj = {
      //     payGroupMasterBulkUploadReqDtoList: this.masterData
      //   };
      //   this.bulkUploadService.postPayGroupMaster(myObj).subscribe(data => {
      //     if (data.statusCode = 201) {
      //       this.toastr.success('Excel Import Succcessfully');
      //       this.Spinner.hide();
      //     } else {
      //       this.toastr.error('Excel Fail To Import');
      //       this.Spinner.hide();
      //     }
      //   }, err => {
      //     this.Spinner.hide();
      //     if (err.json().result) {
      //       this.toastr.error(err.json().result);
      //       // console.log("err.json().result", err.json().result);

      //     }
      //   });
      // } else if (this.masterType == 'Grade Master') {
      //   const myObj = {
      //     gradeMasterBulkUploadReqDtoList: this.masterData
      //   };
      //   this.bulkUploadService.postGradeMaster(myObj).subscribe(data => {
      //     if (data.statusCode = 201) {
      //       this.toastr.success('Excel Import Succcessfully');
      //       this.Spinner.hide();
      //     } else {
      //       this.toastr.error('Excel Fail To Import');
      //       this.Spinner.hide();
      //     }
      //   }, err => {
      //     this.Spinner.hide();
      //     if (err.json().result) {
      //       this.toastr.error(err.json().result);
      //       // console.log("err.json().result", err.json().result);

      //     }
      //   });
      } else if (this.masterType == 'Designation Master') {
        const myObj = {
          designationBulkUploadReqDtoList: this.masterData,
          "compId":this.loggedUser.compId,
        };
        this.Spinner.show();
        this.bulkUploadService.postDesigMaster(myObj).subscribe(data => {
          if (data.statusCode = 201) {
            this.toastr.success('Excel Import Succcessfully');
            this.Spinner.hide();
          } else {
            this.toastr.error('Excel Fail To Import');
            this.Spinner.hide();
          }
        }, err => {
          this.Spinner.hide();
          if (err.json().result) {
            this.toastr.error(err.json().result);
            // console.log("err.json().result", err.json().result);

          }
        });
      // } else if (this.masterType == 'Cost Center Master') {
      //   const myObj = {
      //     costCenterBulkUploadReqDtoList: this.masterData
      //   };
      //   this.bulkUploadService.postCostCenterMaster(myObj).subscribe(data => {
      //     if (data.statusCode == 201) {
      //       this.toastr.success('Excel Import Succcessfully');
      //       this.Spinner.hide();
      //     } else {
      //       this.toastr.error('Excel Fail To Import');
      //       this.Spinner.hide();
      //     }
      //   }, err => {
      //     this.Spinner.hide();
      //     if (err.json().result) {
      //       this.toastr.error(err.json().result);
      //       // console.log("err.json().result", err.json().result);

      //     }
      //   });
      } else if (this.masterType == 'Location Master') {
        const myObj = {
          locationMasterBulkUploadReqDtoList: this.masterData,
          "compId":this.loggedUser.compId,
        };
        this.Spinner.show();
        this.bulkUploadService.postLocationMaster(myObj).subscribe(data => {
          if (data.statusCode == 201) {
            this.toastr.success('Excel Import Succcessfully');
            this.Spinner.hide();
          } else {
            this.toastr.error('Excel Fail To Import');
            this.Spinner.hide();
          }
        }, err => {
          this.Spinner.hide();
          if (err.json().result) {
            this.toastr.error(err.json().result);
            // console.log("err.json().result", err.json().result);

          }
        });
      }
    }

    );
  }
}
function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
