import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyRemarkComponent } from './safety-remark.component';

describe('SafetyRemarkComponent', () => {
  let component: SafetyRemarkComponent;
  let fixture: ComponentFixture<SafetyRemarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyRemarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyRemarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
