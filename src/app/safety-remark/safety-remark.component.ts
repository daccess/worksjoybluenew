import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-safety-remark',
  templateUrl: './safety-remark.component.html',
  styleUrls: ['./safety-remark.component.css']
})
export class SafetyRemarkComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  labourIds: any;
  user: string;
  users: any;
  token: any;
  empids: any;
  labourData: any;
  empfistname: any;
  labourdatas: any;
  lastname: any;
  gender: any;
  dateOfBirth: any;
  bloodGroup: any;
  contactNumber: any;
  emergancyContact: any;
  isChecked: boolean = false;
  remark: any;
  labourmasterId: string;
  labourImages: any;
  option1: boolean = false;
option2: boolean = false;
  safetyApproveReject: string;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
    this.labourIds=sessionStorage.getItem("LabourPerIds")
    this.labourmasterId=sessionStorage.getItem("manrequestIds")
   }

  ngOnInit() {
    this.getSafetyEmployee();
  }
  getSafetyEmployee(){
    let url = this.baseurl + `/getByIdLabourInfo/${this.labourIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.labourData = data
      this.labourdatas=this.labourData.result;
      this.empfistname=this.labourdatas.firstName
      this.lastname=this.labourdatas.lastName
      this.gender=this.labourdatas.gender
      this.dateOfBirth=this.labourdatas.dateOfBirth
      this.bloodGroup=this.labourdatas.bloodGroup
      this.contactNumber=this.labourdatas.contactNumber
      this.emergancyContact=this.labourdatas.emergencyContactNo
      this.labourImages=this.labourdatas.labourImage
      // this.locationId = this.locationdata[0].locationId
    

    },
      (err: HttpErrorResponse) => {

      })

  }
  checkboxChanged() {
    console.log('Checkbox value:', this.isChecked);
  }
  onCheckboxChange(selectedOption: string) {
    if (selectedOption === 'option1' && this.option1) {
      this.option2 = false;
    } else if (selectedOption === 'option2' && this.option2) {
      this.option1 = false;
    }
    // console.log("this is the selected option",  this.option2 )
  if(this.option1==true){
    this.safetyApproveReject='Approve'
    console.log(this.safetyApproveReject)
  }
  else if(this.option2==true){
    this.safetyApproveReject='Reject'
    console.log(this.safetyApproveReject)
  }
  }
  saveRemark(){
    let obj={
      remark:this.remark,
      safetyFlag:"Approved",
      approverId:this.empids,
      labourPerId:this.labourIds,
      manReqStatusId:this.labourmasterId,
      safetyApproveReject:"Approve"
    }
    if(this.isChecked==false){
this.toastr.error('please check Induction program')
    }
    else{
    
      let url = this.baseurl + '/saveSafetyLabourdata';
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.post(url,obj, { headers }).subscribe(data => {
        this.toastr.success("approve successfully")
  
  this.router.navigateByUrl('layout/safetyapproval/safetyapproval');
      },
        (err: HttpErrorResponse) => {
          this.toastr.error("error  while approve")
  
        })
  
    }
  }
  Reject(){
    let obj={
      remark:this.remark,
      safetyFlag:"Approved",
      approverId:this.empids,
      labourPerId:this.labourIds,
      manReqStatusId:this.labourmasterId,
      safetyApproveReject:"Reject"
    }
    if(this.isChecked==false){
this.toastr.error('please check Induction program')
    }
    else{
    
      let url = this.baseurl + '/saveSafetyLabourdata';
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.post(url,obj, { headers }).subscribe(data => {
        this.toastr.success("approve successfully")
  
  this.router.navigateByUrl('layout/safetyapproval/safetyapproval');
      },
        (err: HttpErrorResponse) => {
          this.toastr.error("error  while approve")
  
        })
  
    }
  }
  }

