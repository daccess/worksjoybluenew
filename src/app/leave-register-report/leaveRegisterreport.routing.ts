import { Routes, RouterModule } from '@angular/router';
import { LeaveRegisterReportComponent } from './leave-register-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: LeaveRegisterReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leaveRegisterreports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaveRegisterreports',
                    component: LeaveRegisterReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);