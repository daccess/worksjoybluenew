import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveRegisterReportComponent } from './leave-register-report.component';

describe('LeaveRegisterReportComponent', () => {
  let component: LeaveRegisterReportComponent;
  let fixture: ComponentFixture<LeaveRegisterReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveRegisterReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveRegisterReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
