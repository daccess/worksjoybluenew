import { Component, OnInit } from '@angular/core';
import { BrowserJsonp } from '@angular/http/src/backends/browser_jsonp';
// import { start } from 'repl';

@Component({
  selector: 'app-leave-register-report',
  templateUrl: './leave-register-report.component.html',
  styleUrls: ['./leave-register-report.component.css']
})
export class LeaveRegisterReportComponent implements OnInit {
  

  AllReportData: any;
  AllRegisterReportData: any;
  exceldata:any;
  exceldata1:any;
  reportEmpNameAndIdResDto:any;
  length:any;
  marked=true;
  dataToShow = [];
  totalLeaveList = [];
  dummyHeaders = [];
  dummayCoff=[];
  fromDate: any;
  toDate: any;


  test=[];
  constructor() { 
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    this.AllReportData = sessionStorage.getItem('leaveRegisterReportData')
    this.AllRegisterReportData = JSON.parse(this.AllReportData);
    this.totalLeaveList = this.AllRegisterReportData[0].leaveList;
    if(this.totalLeaveList){
      for (let i = 0; i < (this.totalLeaveList.length * 2); i++) {
        if(i%2 == 0){
          this.dummyHeaders.push("Claim");
        } 
        else{
          this.dummyHeaders.push("Outstanding");
        }
    }
    }
  for (let i = 0; i < this.AllRegisterReportData.length; i++) {
    let obj = new Array();
    let data = {
      currentLeaves : "",
      leaveCount : "",           
      leaveName : "",
      usedLeaves: "",
      claimOutstanding :""
    };

    let data1 = {
      currentLeaves : "",
      leaveCount : "",           
      leaveName : "",
      usedLeaves: "",
      claimOutstanding :""
    };
    for (let j = 0; j < this.AllRegisterReportData[i].leaveList.length; j++) {
      let flag : boolean = false;
      for (let k = 0; k < this.AllRegisterReportData[i].leaveTakenReportInCountResDtos.length; k++) {
        if(this.AllRegisterReportData[i].leaveList[j].leaveName == this.AllRegisterReportData[i].leaveTakenReportInCountResDtos[k].leaveName){
          flag = true;
          data = this.AllRegisterReportData[i].leaveTakenReportInCountResDtos[k];
          data1 = this.AllRegisterReportData[i].leaveTakenReportInCountResDtos[k];
          break;
        }
      }
      if(flag){
         data.claimOutstanding = data.usedLeaves;
        obj.push(data);
        data1.claimOutstanding = data1.currentLeaves;
        obj.push(data1);
        flag = false;

      }
      else{
        obj.push({});
        obj.push({});
        flag = false;
      }
      
    }
    this.AllRegisterReportData[i].leaveTakenReportInCountResDtos = obj;
  }
}

  ngOnInit() {

  }
}


