import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-salary-structure',
  templateUrl: './salary-structure.component.html',
  styleUrls: ['./salary-structure.component.css']
})
export class SalaryStructureComponent implements OnInit {
  baseurl = MainURL.HostUrl;

  themeColor: string = "nav-pills-blue";
  addOnArray:any = [];
  addOnArray1:any=[];
  earningHead: string;
  earningAmount: string;
  deductionHead: string;
  deductionAmount: string;
  AllTableColumnAmount: number;
  totalAmmount: number;

  deductionArray = [];
  netPayableAmount = 0
  totalAmmountOne: number;
  AllTableColumnAmountOne: any;
  user: string;
  users: any;
  token: any;
  earningHeadData: any;
  dedctionHeadData: any;
  salaryHeadId: any;
  nameOfStructure: any;
  getsalaryHeadData: any;
  salaryStructureID: any;
  SalaryStructureData: any;
  salaryDeductionId: any;
  salaryEditData: any;
  buttonFlag:boolean=false
  // netPayableAmount=0

  constructor(private httpSer:HttpClient,public Spinner :NgxSpinnerService,private toastr:ToastrService) { }


  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token
    this.getEarningHeads();
    this.getDeductionHead();
    this.getSalaryStructureList();

  }

//   calculateBasicSalary() {
//     let obj = {
      
//       "salaryHeadId": this.salaryHeadId,
//       "earningAmount": this.earningAmount

//     }
// for(let i=0; this.earningHeadData.length; i++){
//   if(this.earningHeadData[i].salaryHeadId==obj.salaryHeadId){
//     let obj1 = {
//       "earningname":this.earningHeadData[i].salaryHeadName,
//       "salaryHeadId":this.earningHeadData[i].salaryHeadId,
//       "earningAmount": this.earningAmount

//     }
//     this.addOnArray.push(obj1)
//     console.log("this is real object",obj1)
//   }
// }
//     console.log("this is the expense", this.addOnArray)





  

//     // this.earningHead = '';
//     // this.earningAmount = ''


//     // this.AllTableColumnAmount=(this.addOnArray.reduce((n, {amount}) => n + amount, 0));
//     // this.AllTableColumnAmount=this.addOnArray.map(t => t.amount).reduce((acc, value) => acc + value, 0);
//     // this.AllTableColumnAmount= this.addOnArray.reduce((amount, v) => amount += parseInt(v.value), 0)

//     let sum = 0;
//     for (let i = 0; i < this.addOnArray.length; i++) {
//       debugger
//       sum += Number(this.addOnArray[i].earningAmount)
//     }

//     this.AllTableColumnAmount = sum
//     this.totalAmmount = this.AllTableColumnAmount;



//   }
calculateBasicSalary() {
  let obj = {
    "salaryHeadId": this.salaryHeadId,
    "amount": this.earningAmount
  };

  for (let i = 0; i < this.earningHeadData.length; i++) {
    if (this.earningHeadData[i].salaryHeadId == obj.salaryHeadId) {
      let obj1 = {
        "salaryHeadName": this.earningHeadData[i].salaryHeadName,
        "salaryHeadId": this.earningHeadData[i].salaryHeadId,
        "amount": this.earningAmount
      }
      this.addOnArray.push(obj1);
      console.log("this is real object", obj1);
    }
  }

  // Sum the 'earningAmount' properties in addOnArray
  this.AllTableColumnAmount = this.addOnArray.reduce((total, item) => total + Number(item.amount), 0);

  this.totalAmmount = this.AllTableColumnAmount;
}

  calculateBasicSalaryOne() {

    let deductionObj = {
      "salaryDeductionId": this.salaryDeductionId,
      "deductionAmount": this.deductionAmount
    };
    for (let i = 0; i < this.dedctionHeadData.length; i++) {
      if (this.dedctionHeadData[i].salaryDeductionId == deductionObj.salaryDeductionId) {
        let obj2 = {
          "salaryHeadName": this.dedctionHeadData[i].salaryHeadName,
          "salaryDeductionId": this.dedctionHeadData[i].salaryDeductionId,
          "amount": this.deductionAmount
        }
        this.deductionArray.push(obj2);
        console.log("this is real object", obj2);
      }
    }
  

   

    // for deductoin array loop
    let deductionSum = 0;
    for (let i = 0; i < this.deductionArray.length; i++) {
      deductionSum += Number(this.deductionArray[i].amount)

    }
    this.AllTableColumnAmountOne = deductionSum
    this.totalAmmountOne = this.AllTableColumnAmountOne;
    this.netPayableAmount = this.totalAmmount - this.totalAmmountOne;

  }
  getEarningHeads(){
  let url = this.baseurl + '/getEarningSalaryHead';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpSer.get(url,{headers}).subscribe(data => {
 this.earningHeadData=data['result']
},
(err: HttpErrorResponse) => {
});
}
getDeductionHead(){
  let url = this.baseurl + '/getDeductionSalaryHead';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpSer.get(url,{headers}).subscribe(data => {
 this.dedctionHeadData=data['result']
},
(err: HttpErrorResponse) => {
});
}

onsubmitSalaryStructure(){
  this.buttonFlag=false
  let obj={
    "nameOfStructure":this.nameOfStructure,
    "totalEarningAmount":this.totalAmmount,
    "totalDeductionAmount":this.totalAmmountOne,
    "netPayable":this.netPayableAmount,
    "salaryEarningList":this.addOnArray,
    "salaryDeductionList":this.deductionArray
  }
  let url = this.baseurl + '/saveSalaryStructure';
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpSer.post(url,obj,{headers}).subscribe(data => {
   this.toastr.success("salary structure save successfully")
   document.getElementById('extra-list-tab').click();
   this.getSalaryStructureList();
  },
  (err: HttpErrorResponse) => {
    this.toastr.error("server side error")
  });
}
getSalaryStructureList(){
  let url = this.baseurl + '/getSalaryStructure';
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpSer.get(url,{headers}).subscribe(data => {
 this.getsalaryHeadData=data['result']
 setTimeout(function () {

  $(function () {

    var table = $('#salaryTable').DataTable({

      retrieve: true,
      searching:true

   

    })
  });
}, 100);
},
(err: HttpErrorResponse) => {
});
}
getByIdSalaryStucture(id:any){
  this.buttonFlag=true;
this.salaryStructureID=id.salaryStructureID
  document.getElementById('extra-apply-tab').click();
  let url = this.baseurl + `/getSalaryStructure/${this.salaryStructureID}`;
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpSer.get(url,{headers}).subscribe(data => {
 this.SalaryStructureData=data
 this.salaryEditData=this.SalaryStructureData.result;
 this.nameOfStructure=this.salaryEditData.nameOfStructure;
 this.addOnArray=this.salaryEditData.salaryEarningList;
 this.deductionArray=this.salaryEditData.salaryDeductionList
 this.netPayableAmount=this.salaryEditData.netPayable;
 this.totalAmmount=this.salaryEditData.totalEarningAmount;
 this.totalAmmountOne=this.salaryEditData.totalDeductionAmount;

},
(err: HttpErrorResponse) => {
});

}
UpdateSalaryStuctures(){
  let obj={
    "salaryStructureID":this.salaryStructureID,
    "nameOfStructure":this.nameOfStructure,
    "totalEarningAmount":this.totalAmmount,
    "totalDeductionAmount":this.totalAmmountOne,
    "netPayable":this.netPayableAmount,
    "salaryEarningList":this.addOnArray,
    "salaryDeductionList":this.deductionArray,
  
  }
  let url = this.baseurl + '/updateSalaryStructure';
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  this.httpSer.put(url,obj,{headers}).subscribe(data => {
   this.toastr.success("salary structure updated successfully")
  this.buttonFlag=false
   document.getElementById('extra-list-tab').click();
   this.getSalaryStructureList();
  },
  (err: HttpErrorResponse) => {
    this.toastr.error("server side error while update structure")
  });
}

deletesalaryStructure(id:any){
 
this.salaryStructureID=id.salaryStructureID
  let url = this.baseurl + `/deleteSalaryStructure/${this.salaryStructureID}`;
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
this.httpSer.delete(url,{headers}).subscribe(data => {
 
this.toastr.success("salary structure deleted successfully")
this.getSalaryStructureList();
},
// (err: HttpErrorResponse) => {
//   this.toastr.error("server side error while delete salary structure")
// });

err => {
  console.log(err); // Log the error to the console to check its content
  if (err && err.error.result && err.error.result.msg) {
    this.toastr.error(err.error.result.msg); // Access the error message from the error object
  } else {
    this.toastr.error("server side error while delete salary structure"); // Provide a fallback message if the expected structure is not present
  }
})
}
}



