import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { salaryRouting } from './salary-structure-routing.module';
import { SalaryStructureComponent } from './salary-structure.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    salaryRouting,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    SalaryStructureComponent
  ]
})
export class SalaryStructureModule { }
