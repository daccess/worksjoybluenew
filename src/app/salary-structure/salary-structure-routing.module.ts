import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaryStructureComponent } from './salary-structure.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: SalaryStructureComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'salaryStructure',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'salaryStructure',
                  component: SalaryStructureComponent
              }

          ]

  }


]

export const salaryRouting = RouterModule.forChild(appRoutes);