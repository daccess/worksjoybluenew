import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from '../../app/login/login.component';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { LeaveblockComponent } from 'src/app/leaveblock/leaveblock.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: LeaveblockComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leaveblock',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaveblock',
                    component: LeaveblockComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);