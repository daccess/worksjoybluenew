import { TestBed, inject } from '@angular/core/testing';

import { LeaveblockserviceService } from './leaveblockservice.service';

describe('LeaveblockserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeaveblockserviceService]
    });
  });

  it('should be created', inject([LeaveblockserviceService], (service: LeaveblockserviceService) => {
    expect(service).toBeTruthy();
  }));
});
