import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

// import { satutory } from '../models/satutorymodel';
// import { MainURL } from '../../../shared/configurl';
import { from } from 'rxjs';
import { MainURL } from 'src/app/shared/configurl';
//import { satutory } from 'src/app/employee/shared/models/satutorymodel';
@Injectable({
  providedIn: 'root'
})
export class LeaveblockserviceService {

  baseurl = MainURL.HostUrl;
  empOfficialId : string = ""
  constructor(private http : Http) { }

  postleaveBlock(leave){
    let url = this.baseurl + '/blockMaster';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getLeaveblockedList(compId){
    let url = this.baseurl + '/blockMasterList/'+compId;
    // var body = JSON.stringify(model);
    return this.http.get(url)
  }

  monthlyAttendance(empOfficialId){
    let url = this.baseurl + '/callender';
    var body = JSON.stringify(empOfficialId);
    console.log("body",body);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  updateBlock(leave){
    let url = this.baseurl + '/updateBlockMaster';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,leave).map(x => x.json());
  }
}
