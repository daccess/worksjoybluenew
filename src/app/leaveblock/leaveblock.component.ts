import { Component, OnInit } from '@angular/core';
import { leaveBlockModel } from './shared/leaveBlockModel';
import { MainURL } from '../shared/configurl';
import { Http } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { LeaveblockserviceService } from './shared/leaveblockservice.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-leaveblock',
  templateUrl: './leaveblock.component.html',
  styleUrls: ['./leaveblock.component.css']
})
export class LeaveblockComponent implements OnInit {
  compId: any;
  empOfficialId: any;
  empPerId: any;
  loggedUser: any;
  model: any = { employeePerInfoList: [] };
  dropdownSettings: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  Employeedata: any = [];
  curDate: any;
  setlistdata: any = [];
  blockMasterReqDtoList = [];
  monthNames = [];
  todaysDate : any;
  dropdownSet: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  themeColor: string;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  absentDay: any = "17"
  currentMonth1: any;
  selectedempPerId: any;
  hideCalender: boolean = false;
  date: any = new Date();
  attendanceData: any = [];
  daysInThisMonth: any = [];
  daysInLastMonth: any = [];
  daysInNextMonth: any = [];
  from: any;
  HolidayData = [];
  getEmpOfficialId: any;
  constructor(public toastr: ToastrService, private httpService: Http,   public Spinner: NgxSpinnerService, private service: LeaveblockserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.empPerId = this.loggedUser.empPerId;
    this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.toDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
  }

  ngOnInit() {
    this.setlist()
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'empPerId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSet = {
      singleSelection: false,
      idField: 'fromDate',
      textField: 'fromDate',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true
    };
    this.getAllEmployee();
    this.getBlockedList();
  }
  blockedList: any = [];

  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ":" + rminutes;
  }
  getBlockedList() {
    this.service.getLeaveblockedList(this.compId).subscribe(data => {
      this.blockedList = data.json().result;
      for (let i = 0; i < this.blockedList.length; i++) {
        for (let index = 0; index < this.blockedList[i].blockMasterResDtoList.length; index++) {
          let date = new Date(this.blockedList[i].blockMasterResDtoList[index].fromDate)
          let fromDate = date.getDate() + "-" + this.monthNames[(date.getMonth())] + "-" + date.getFullYear();
          this.blockedList[i].blockMasterResDtoList[index].fromDate = fromDate;
        }
      }
    }, err => {
    })
  }
  //===========select employee start===========
  baseurl = MainURL.HostUrl
  getAllEmployee() {
    let url = this.baseurl + '/employeelistunderempid/';
    this.httpService.get(url + this.loggedUser.empId).subscribe(data => {
      this.Employeedata = data.json().result;
      for (let index = 0; index < this.Employeedata.length; index++) {
        this.Employeedata[index].fullName = this.Employeedata[index].empId+"-"+this.Employeedata[index].fullName;
      }
    },
    (err: HttpErrorResponse) => {
    });
  }
  setlist() {
    let url = this.baseurl + '/blockMasterList/';
    this.httpService.get(url + this.loggedUser.compId).subscribe(data => {
        this.setlistdata = data.json().result;
        for (let i = 0; i < this.setlistdata.length; i++) {
          for (let index = 0; index < this.setlistdata[i].blockMasterResDtoList.length; index++) {
            let date = new Date(this.setlistdata[i].blockMasterResDtoList[index].fromDate)
            let fromDate = date.getFullYear() + "-" + this.monthNames[date.getMonth()] + "-" + date.getDate();
            this.setlistdata[i].blockMasterResDtoList[index].fromDate = fromDate;
          }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }


  fromDateChange(e) {
    this.model.fromDate = e;
    if(new Date(this.model.toDate).getTime() < new Date(this.model.fromDate).getTime()){
      this.model.toDate = e;
    }
  }

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }
    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    let obj;
    for (let i = 0; i < thisNumOfDays; i++) {
      let item;
      let flag = false;
      for (let j = 0; j < this.attendanceData.length; j++) {
        if (!this.attendanceData[j].checkIn) {
          this.attendanceData[j].checkIn = new Date(this.attendanceData[j].checkIn).setHours(0, 0, 0);
        }
        if (!this.attendanceData[j].checkOut) {
          this.attendanceData[j].checkOut = new Date(this.attendanceData[j].checkOut).setHours(0, 0, 0);
        }
        if (new Date(this.attendanceData[j].attenDate).getDate() == i + 1) {
          flag = true;
          if (this.attendanceData[j].statusOfDay == 'WD') {

            if (this.attendanceData[j].attendanceStatus == 'P' || this.attendanceData[j].attendanceStatus == 'VP') {

              if (this.attendanceData[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: this.attendanceData[j].leaveStatus,
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: this.attendanceData[j].coffStatus + " Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "On Duty"
                }
              }
              else if (this.attendanceData[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Work From Home"
                }
              }

              else {
                let checkIn = "";
                let checkOut = "";
                if (this.attendanceData[j].checkIn) {
                  checkIn = new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes();
                }
                if (this.attendanceData[j].checkOut) {
                  checkOut = new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration);
                }
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: 'In ' + checkIn,
                  checkOut: ' Out ' + checkOut
                }
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF' && !this.attendanceData[j].wfhStatus) {
              item = {
                day: i + 1,
                date: this.attendanceData[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF' && this.attendanceData[j].wfhStatus) {
              item = {
                day: i + 1,
                status: "WFH",
                date: this.attendanceData[j].attenDate,
                checkIn: "Work From Home"
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.attendanceData[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Leave",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Work From Home"
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.attendanceData[j].attenDate
                }
              }

            }


          }
          else if (!this.attendanceData[j].statusOfDay) {

            if (this.attendanceData[j].attendanceStatus == 'P') {

              if (this.attendanceData[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "On Duty"
                }
              }



              else {
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF') {
              item = {
                day: i + 1,
                date: this.attendanceData[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.attendanceData[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Leave",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.attendanceData[j].attenDate
                }
              }

            }


          }
          else {
            if (this.attendanceData[j].statusOfDay == 'HO') {
              let holiDayName;
              for (let m = 0; m < this.HolidayData.length; m++) {

                if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                  holiDayName = this.HolidayData[m].holiDayName;
                }

              }
              item = {
                day: i + 1,
                status: "HO",
                checkIn: holiDayName
              }
            } else {
              item = {
                day: i + 1,
                status: "WO",
                checkIn: "Week Off"
              }
            }

          }

          this.daysInThisMonth.push(item);
        }
      }
      if (!flag) {

        let holiDayName;
        let holidayFlag: boolean = false;
        if (this.HolidayData) {
          for (let m = 0; m < this.HolidayData.length; m++) {
            holidayFlag = false;
            if (new Date(new Date(this.HolidayData[m].startDate).getFullYear(), new Date(this.HolidayData[m].startDate).getMonth(), new Date(this.HolidayData[m].startDate).getDate(), 0, 0, 0, 0).getTime() <= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime() && new Date(new Date(this.HolidayData[m].endtDate).getFullYear(), new Date(this.HolidayData[m].endtDate).getMonth(), new Date(this.HolidayData[m].endtDate).getDate(), 0, 0, 0, 0).getTime() >= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime()) {
              holiDayName = this.HolidayData[m].holiDayName;
              holidayFlag = true;
              break;
            }
          }
        }

        if (holidayFlag) {
          item = {
            day: i + 1,
            status: "HO",
            checkIn: holiDayName
          }
        }
        else {
          item = {
            day: i + 1,
            status: "Ot"
          }
        }
        this.daysInThisMonth.push(item);

      }

    }
    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    let nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
    for (let i = 0; i < (6 - lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i + 1);
    }
    // let totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    // if (totalDays < 36) {
    //   for (let i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
    //     this.daysInNextMonth.push(i);
    //   }
    // }

  }


  

  getAttendanceOfMonth(month, year) {
    
    let obj = {
      "empOfficialId": this.getEmpOfficialId,
      "month": month,
      "year": year
    }
    this.service.monthlyAttendance(obj).subscribe(data => {
      this.attendanceData = data.result;
      this.getDaysOfMonth();
    }, (err => {
      this.getDaysOfMonth();
    })
    )
  }
  


  toDateChange(e) {
    this.model.toDate = e;
  }
  selectedEmployee: any = [];
  onItemSelect(item: any) {
    const id = item.empPerId;
    if (this.selectedEmployee.length == 0) {
      this.selectedEmployee.push(item);
      this.selectEmp(id);
      this.hideCalender=true;
    } else {

      this.selectedEmployee.push(item);
      this.hideCalender=false;

    }
  }
  onSelectAll(items: any) {
    this.selectedEmployee = items;
    this.hideCalender=false;
  }
  selectEmp(e){
    this.selectedempPerId = e;
   for (let i = 0; i < this.Employeedata.length; i++) {
       if(this.Employeedata[i].empPerId == this.selectedempPerId){
        this.getEmpOfficialId = this.Employeedata[i].empOfficialId;
        this.getAttendanceOfMonth(new Date().getMonth() + 1, new Date().getFullYear());
       }
    }
    this.hideCalender = true;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedEmployee.length; i++) {
      if (this.selectedEmployee[i].locationId == item.locationId) {
        this.selectedEmployee.splice(i, 1)
      
      }
    }
    if (this.selectedEmployee.length > 0) {
      this.hideCalender=false;
    }
  }

  onDeSelectAll(item: any) {
    this.selectedEmployee = [];
  }
  //===========select employee end=============
  onItemSelec(item: any) {
    if (this.blockMasterReqDtoList.length == 0) {
      this.blockMasterReqDtoList.push({ "fromDate": item });
    } else {
      this.blockMasterReqDtoList.push({ "fromDate": item })
    }
  }

  tempList: any = [];
  onSelectAl(items: any) {
    for (let i = 0; i < items.length; i++) {
      this.tempList.push({ "fromDate": items[i] });
      this.blockMasterReqDtoList = this.tempList;
    }
    this.blockMasterReqDtoList.push({ "fromDate": items });
  }

  OnItemDeSelec(item: any) {
    for (var i = 0; i < this.blockMasterReqDtoList.length; i++) {
      if (this.blockMasterReqDtoList[i].locationId == item.locationId) {
        this.blockMasterReqDtoList.splice(i, 1)
        
      }
    }
  }

  onDeSelectAl(item: any) {
    this.blockMasterReqDtoList = [];
    this.hideCalender=false;
  }
  onSubmit() {
    this.Spinner.show()
    this.service.postleaveBlock(this.model).subscribe(data => {
      this.getBlockedList();
      this.model.employeePerInfoList = [];
      this.model.reason = '';
      this.model.fromDate = new Date();
      this.model.toDate = new Date();
      this.toastr.success("Leave date blocked successfully");
      this.Spinner.hide()
      this.setlist()
    }, err => {
      this.Spinner.hide();
      this.toastr.error("Failed To Blocked")
    })
  }
  unblock(item) {
    for (let i = 0; i < this.blockMasterReqDtoList.length; i++) {
      this.blockMasterReqDtoList[i].fromDate = new Date(this.blockMasterReqDtoList[i].fromDate);
    }
    let dates = new Array();
    for (let i = 0; i < item.data.length; i++) {
      let obj = {
        fromDate : new Date(item.data[i]).getTime()
      }
       dates.push(obj);
    }
    let obj = {
      blockId: item.blockId,
      empPerId: item.empPerId,
      fromDate: item.fromDate,
      toDate: item.fromDate,
      blockMasterReqDtoList: dates
      // reason : '',
      // status : ''
    }
    this.Spinner.show()
    this.service.updateBlock(obj).subscribe(data => {
      this.Spinner.hide()
      this.toastr.success("Successfully Unblocked");
      item.blockMasterResDtoList = [];
      this.setlist();
    }, err => {
      this.Spinner.hide()
      this.toastr.error("Failed To Unblock")
    })
  }
  unblockNone(){
    this.toastr.error("please select date");
  }
  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.attendanceData = []
    this.getDaysOfMonth();
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }

  show() {
    this.model.frmdate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
  }
  selectDate(day) {
    this.currentDate = day;
    if (this.from == 'from') {
      this.model.frmdate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
    } else if (this.from == 'to') {
      this.model.todate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
    }
  }
}