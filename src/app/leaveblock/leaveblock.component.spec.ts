import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveblockComponent } from './leaveblock.component';

describe('LeaveblockComponent', () => {
  let component: LeaveblockComponent;
  let fixture: ComponentFixture<LeaveblockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveblockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
