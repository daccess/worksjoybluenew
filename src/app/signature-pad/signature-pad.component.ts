import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import SignaturePad from 'signature_pad';
import { MainURL } from '../shared/configurl';
declare const GlobalCompositeOperation: string;

@Component({
  selector: 'app-signature-pad',
  templateUrl: './signature-pad.component.html',
  styleUrls: ['./signature-pad.component.css']
})
export class SignaturePadComponent implements OnInit {
   GlobalCompositeOperation: string;

  baseurl = MainURL.HostUrl;
  signPad: any;
  @ViewChild('signPadCanvas', {static: false} as any) signaturePadElement:any;
  signImage: string | null = null;

  loggedUser: any;
  compId: any;
  user: string;
  users: any;
  token: any;
  empIds: any;
  companydata: any;
  
  
  constructor(public httpService: HttpClient,public Spinner: NgxSpinnerService,private toastr: ToastrService,private router: Router) { 
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.empIds=this.users.roleList.empId
    this.compId=this.users.roleList.compId
  }
  

  ngOnInit() {
    this.getCompany()
  }
  
  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    this.signPad = new SignaturePad(this.signaturePadElement.nativeElement);
    console.log('SignaturePad initialized:', this.signPad);
  }
  
  /*It's work in devices*/
  startSignPadDrawing(event: Event) {
    console.log(event);
  }
  /*It's work in devices*/
  movedFinger(event: Event) {
  }
  /*Undo last step from the signature*/
  undoSign() {
    const data = this.signPad.toData();
    if (data) {
      data.pop(); // remove the last step
      this.signPad.fromData(data);
    }
  }
  /*Clean whole the signature*/
  clearSignPad() {
    console.log('clearSignPad method called');
    this.signPad.clear();
    console.log('SignaturePad cleared');
  }
  
  /*Here you can save the signature as a Image*/
  saveSignPad() {
    const base64ImageData = this.signPad.toDataURL();
    this.signImage = base64ImageData;
    console.log("bdfjahfh", this.signImage);
    this.saveSignature();
  }
  





  
  saveSignature(){
    let obj={
      compId:this.compId,
      signatureImage:this.signImage
      
    }

    let url = this.baseurl + '/saveAuthoritySignature';
    const tokens = this.token;
    this.Spinner.show();
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.put(url,obj,{headers}).subscribe(data => {
        this.toastr.success("signature uploaded successfully")
        this.Spinner.hide()
        // this.contractorServiceType=data['result']
       
    },
      (err: HttpErrorResponse) => {
    this.toastr.error("server side error")
    this.Spinner.hide()
      })
    //Here you can save your signature image using your API call.
  }



  getCompany() {
    let url = this.baseurl + '/getAllCompany';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  
    this.httpService.get(url, { headers }).subscribe(
      data => {
        this.companydata = data["result"];
        console.log('Company Data:', this.companydata);
  
        // Extract signatureImage from the first object
        const signatureImage = this.companydata.length > 0 ? this.companydata[0].signatureImage : null;
  
        // Set the signImage variable
        this.signImage = signatureImage;
  
        console.log('Signature Image:', this.signImage);
      },
      (err: HttpErrorResponse) => {
        console.error('Error fetching company data:', err);
      }
    );
  }
  
  
  
}

