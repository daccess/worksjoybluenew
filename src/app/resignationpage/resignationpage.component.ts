import { Component, OnInit } from '@angular/core';
import { resignationservice } from './shared/services/resignationservice';
import { ToastrService } from 'ngx-toastr';
import { resignationmodeldata } from './shared/Models/resignationmodel';
import { HttpErrorResponse } from '@angular/common/http';
import { ResignationpageModule } from './resignationpage.module';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-resignationpage',
  templateUrl: './resignationpage.component.html',
  styleUrls: ['./resignationpage.component.css'],
  providers: [resignationservice]
})
export class ResignationpageComponent implements OnInit {
  eligiblerehired: any;
  resignationstatus: any;

  resigntaion: resignationmodeldata;
  empPerId: any;
  submitButtonFlag: boolean = false;
  resignationDetails: any;
  compId: any;
  resignationReasonList: any;
  empDetails: any;
  disable: boolean;
  ResignationleavingId: any;
  displayDate: any
  buttonDisabled: boolean;
  fileName: string;
  approvelsStatus: any;
  currentDate: Date;
  constructor(public resignationservice: resignationservice,public Spinner :NgxSpinnerService, public toastr: ToastrService) {

  }

  ngOnInit() {
    this.resigntaion = new resignationmodeldata()
    let data = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.empDetails = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.empPerId = data.empPerId;
    this.compId = data.compId;
    this.resigntaion.resignationReason1 = "";
    this.getResignationReasonList();
    this.getResignationData();
    let day = new Date();
    this.currentDate = new Date ();
  }

  onSubmit() {
    this.resigntaion.lastDayOfWorkAsPerTheCompanyPolicy = new Date(this.resigntaion.lastDayOfWorkAsPerTheCompanyPolicy)
    this.resigntaion.empPerId = this.empPerId;
    // this.resigntaion.empOff = this.empPerId;
    this.resigntaion.eligibleForRehire = this.eligiblerehired;
    this.resigntaion.resignationStatus = this.resignationstatus;
    this.resigntaion.resignOrTerminationBy = 'User'
    this.resigntaion.approver1Id = this.empDetails.firstLevelReportingManager;
    this.resigntaion.approver2Id = this.empDetails.secondLevelReportingManager;
    this.resigntaion.approver3Id = this.empDetails.thirdLevelReportingManager;
    this.resigntaion.firstLevelReportingManager = this.empDetails.firstLevelReportingManager;
    this.resigntaion.compId = this.empDetails.compId;
    this.resigntaion.empId = this.empDetails.empId;
    this.resigntaion.resignationReason = {
      resigReasonId: this.resigntaion.resignationReason1
    }

    let date = new Date(this.displayDate);
    this.resigntaion.relievingDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    this.Spinner.show();
    this.resignationservice.postresigfnation(this.resigntaion).subscribe(data => {
        this.resignationDetails = data['result']['resigAndnLeavingId'];
        sessionStorage.setItem('resigAndnLeavingId', this.resignationDetails);
        this.Spinner.hide();
        this.toastr.success('Resignation Applied Successfully!');
        this.buttonDisabled = true;
        this.getResignationData();
        this.shoImg = false;
        this.disable = true;
      },
      (err: HttpErrorResponse) => {
        this.Spinner.hide();
        this.toastr.error('Server Side Error..!', 'Resignation');
      
      });
  }

  filesToUpload: File;
  imageUrl: any;
  shoImg : boolean = false 
  handleFilesInput(file: FileList) {
    this.disable = true
    this.filesToUpload = file.item(0);
    this.fileName = file.item(0).name;
    this.shoImg = true;
    this.Spinner.show()
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.filesToUpload);
    this.postFile(this.filesToUpload)
  }

  postFile(files) {
    this.resignationservice.postFile(this.filesToUpload).subscribe((data: any) => {
        this.resigntaion.filePath = JSON.parse(data._body).result.imageUrl;
        files.value = null;
        this.Spinner.hide();
        this.toastr.success('Document Uploaded Successfully!', 'Resignation');
        this.disable = false;
      },
      (err : HttpErrorResponse)=>{
        this.Spinner.hide();
      });
  }
  getResignationData() {
    this.resignationservice.getResignation(this.empPerId, this.empDetails.empTypeId).subscribe(data => {
      let data1 = (data);
      this.approvelsStatus =   data1.approvelsStatus;
      this.resigntaion = data1;
      let d = new Date(data.lastDayOfWorkAsPerTheCompanyPolicy);
      this.resigntaion.lastDayOfWorkAsPerTheCompanyPolicy = data.lastDayOfWorkAsPerTheCompanyPolicy
      this.displayDate = d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate();
      if (data.resignationReason1.resigReasonId) {
        this.resigntaion.resignationReason1 = data.resignationReason1.resigReasonId;
      }
      else {
        this.resigntaion.resignationReason1 = "";
      }
      this.ResignationleavingId = data1.resigAndnLeavingId;
      if (this.resigntaion.relievingDate) {
      }
      else {
        let day = new Date()
        this.resigntaion.relievingDate = day.getFullYear() + "/" + (day.getMonth() + 1) + "/" + day.getDate();
      }
      this.getStatuss(data1.resigAndnLeavingId);
    })
  }

  getStatuss(item) {
    this.resignationservice.getStatus(item).subscribe(data => {
      if (data.result.submitButtonFlag) {
        this.submitButtonFlag = data.result.submitButtonFlag;
      }
    }, (err: HttpErrorResponse) => {
      let data: any = err
      let test = JSON.parse(data._body);
      if (test.result) {
        this.submitButtonFlag = test.result.submitButtonFlag;
      }
      else {
        this.submitButtonFlag = false
      }
    });
  }
  downloadFile(data) {
    window.open(data, '_blank');
  }
  getResignationReasonList() {
    this.resignationservice.getResignationReasonList(this.compId).subscribe(data => {
      this.resignationReasonList = data.result
    });
  }

  setDate(event) {
    this.resigntaion.relievingDate = event;
  }
  cancelResignasation(f) {
    this.Spinner.show();
    this.resignationservice.deleteResignation(this.ResignationleavingId).subscribe(data => {
    f.reset();
    this.fileName=null;
    this.toastr.success('Delete Successfully');
    this.buttonDisabled = false;
      $("#File1").val('');
      this.getResignationData();
      location.reload();
      this.Spinner.hide();
    }, (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!');
    })
  }
}
