import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ResignationpageComponent } from './resignationpage.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ResignationpageComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'resignationpage',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'resignationpage',
                    component: ResignationpageComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);