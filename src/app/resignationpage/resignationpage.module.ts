import { routing } from './resignationpage.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ResignationpageComponent } from './resignationpage.component';

import { resignationservice } from './shared/services/resignationservice';
import { MainURL } from '../shared/configurl';

@NgModule({
    declarations: [
        ResignationpageComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    ],
    providers: [resignationservice]
  })
  export class ResignationpageModule { 
   
      constructor(){

      }

    
  }
  