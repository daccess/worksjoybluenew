import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResignationpageComponent } from './resignationpage.component';

describe('ResignationpageComponent', () => {
  let component: ResignationpageComponent;
  let fixture: ComponentFixture<ResignationpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResignationpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResignationpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
