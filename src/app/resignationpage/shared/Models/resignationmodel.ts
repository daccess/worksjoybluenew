export class resignationmodeldata{
    resigAndnLeavingId: number;
    dateOfResignation: string;
    reasonForResignation: string;
    wantToServeNoticePeriod: boolean;
    noticePeriodInDays: number;
    resignationStatus: boolean;
    lastDayOfWorkAsPerTheResignationLetter: Date;
    lastDayOfWorkAsPerTheCompanyPolicy: any;
    lastDayOfWorkAsPerTheDiscussionWithEmployee: Date;
    relievingDate: string;
    terminationReason: string;
    eligibleForRehire: boolean;
    reasonForNotRehire: string;
    empPerId: number;
    resignationNote:string
    filePath:any;
    resignationReason:any;
    resignationReason1 : any;
    approver1Id:any;
    approver2Id:any;
    approver3Id:any;
    displayDate : String;
    displaydateOfResignation: String;
    resignOrTerminationBy : string
    requestedRelievingDate : Date
    requestedRelievingReson : String
  empId: any;
  delegationEmpId: any;
  firstLevelReportingManager: any;
  compId: any;
}