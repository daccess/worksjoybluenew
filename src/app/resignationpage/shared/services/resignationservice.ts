import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { resignationmodeldata } from '../Models/resignationmodel';
import { MainURL } from '../../../shared/configurl';
import { from } from 'rxjs';

@Injectable()
export class resignationservice {
 
  baseurl = MainURL.HostUrl;
  Aws_flag=MainURL.Aws_flag;
  constructor(private http : Http) { }

  postresigfnation(resignation : resignationmodeldata){
    let url = this.baseurl + '/empresignation';
    var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  postFile(fileToUpload: File) {
    let url = this.baseurl + '/uploadFile';
    if(this.Aws_flag!='true'){
      url = this.baseurl + '/fileHandaling';
    }
    
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint, formData).map(x => x);;
    }

  getResignation(empId : any,empTypeId){
    let url = this.baseurl+'/empresignation?empPerId='+empId+'&empTypeId='+empTypeId;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  getResignationReasonList(compId){
    
    let url = this.baseurl+'/seperationSetting/getResignationReasonList/'+compId;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  getStatus(resigAndnLeavingId){
    // resigAndnLeavingId:resigAndnLeavingId
    
    let url = this.baseurl+'/checkFistLevelApproveOrNot';
    var body = {
      resigAndnLeavingId:resigAndnLeavingId
    };
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  deleteResignation(resigAndnLeavingId){
    let urldelete = this.baseurl+'/empresignation/'+ resigAndnLeavingId;
    // var body = JSON.stringify(resignation);
    // var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.delete(urldelete).map(x => x.json());
  }

  employeesignationByHR(body){
    // resigAndnLeavingId:resigAndnLeavingId
    
    let url = this.baseurl+'/deactivationEmpDetails';
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getDeactivatedEmployess(){
    let url = this.baseurl+'/empTerminationList';
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  getAllSanctioningEmployee(){
    let url = this.baseurl+'/allSanctioningEmployee';
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  getDeactivatedEmployeeById(id){
    let url = this.baseurl+'/empTermination/';
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url+id).map(x => x.json());
  }
}