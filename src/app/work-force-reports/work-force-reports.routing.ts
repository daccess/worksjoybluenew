import { Routes, RouterModule } from '@angular/router'

import { EmployeeInformationReportComponent } from './employee-information-report/employee-information-report.component';
import { WorkForceReportsComponent } from './work-force-reports/work-force-reports.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: WorkForceReportsComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'workForceReports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'workForceReports',
                    component: WorkForceReportsComponent
                },

                // {
                //     path:'employeeInformationReports',
                //     component:EmployeeInformationReportComponent
                // }

            ]

    }
]

export const routing = RouterModule.forChild(appRoutes);