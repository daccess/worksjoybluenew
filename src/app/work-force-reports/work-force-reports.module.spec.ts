import { WorkForceReportsModule } from './work-force-reports.module';

describe('WorkForceReportsModule', () => {
  let workForceReportsModule: WorkForceReportsModule;

  beforeEach(() => {
    workForceReportsModule = new WorkForceReportsModule();
  });

  it('should create an instance', () => {
    expect(workForceReportsModule).toBeTruthy();
  });
});
