import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from 'src/app/absentreport/excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-information-report',
  templateUrl: './employee-information-report.component.html',
  styleUrls: ['./employee-information-report.component.css'],
  providers: [ExcelService]
})
export class EmployeeInformationReportComponent implements OnInit {

  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;
  marked10 = true;
  marked11 = true;
  marked12 = true;
  marked13 = true;
  marked14 = true;
  marked15 = true;
  marked16 = true;
  marked17 = true;
  marked18 = true;
  marked19 = true;
  marked20 = true;
  marked21 = true;
  marked22 = true;
  marked23 = true;
  marked9514=true;
  marked28 = true;
  marked29 = true;

  
  srNoCheckbox = true
  empIdcheckbox = true;
  biometricIDCheckbox = true;
 
  
  employeeNameCheckbox = true;
  employeeContractNameCheckbox=true;
  genderCheckbox = true;
  dateOfBirthCheckbox = true;
  nationalityCheckbox = true;
  pANNumberChecckbox = true;
  ESICNoCheckbox= true;
  PFNoCheckbox= true;
  adharCardNoCheckbox = true;
  uANNumberCheckbox = true;
  locationCheckbox = true;
  employeeRoleCheckbox = true;
  departmentNameChecckbox = true;

  subDepartmentNameCheckbox = true;
  designationCheckbox = true;
  employeeTypeCheckbox = true;
  subEmployeeTypeChecckbox = true;
  shiftTypeCheckbox = true;

  dateofJoiningCheckbox = true;
  personalEmailIDCheckbox = true;
  officialEmailIDCheckbox = true;
  contractorNameCheckbox = true;
  contractorServiceNameCheckbox = true;
 
  statusDay = true;
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  user_data=[];
  report_time: string;

  constructor(
    private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router) 
    {
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    //this.AllReportData = sessionStorage.getItem('employeeInformationReportData');
    //this.AllReport = JSON.parse(this.AllReportData);

    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });
    console.log("EmployeeInfo All report", this.AllReport);
    this.exceldata = this.AllReport;

    for(let i=0;i<this.AllReport.length;i++){
      if(this.AllReport[i].length!=0){
      for(let j=0;j<this.AllReport[i].length;j++){
         this.user_data.push(this.AllReport[i][j]);
      }
    }
  }
  console.log(this.user_data);
  this.exceldata1=this.AllReport;
  console.log("Data for excel is",this.exceldata1);
  }

  ngOnInit() {
  }


  // exportAsXLSX(): void {
  //   let excelData = new Array();
  //   for (let i = 0; i < this.exceldata1.length; i++) {
  //     let obj: any = {}
  //     if (this.marked1) {
  //       obj.Sr = i+1;
  //     }
  //     if(this.marked2){
  //       obj.EmpId = this.exceldata1[i].empId;
  //     }
  //     if(this.marked3){
  //       obj.BioId = this.exceldata1[i].bioId;
  //     }
  //     if(this.marked4){
  //       let employeeName;
  //       if(this.exceldata1[i].middleName == null){
  //         employeeName = this.exceldata1[i].fullName + " " +this.exceldata1[i].lastName;
  //       }
  //       else{
  //         employeeName = this.exceldata1[i].fullName + " " +this.exceldata1[i].middleName+ " "+ this.exceldata1[i].lastName;
  //       }
        
  //       obj.EmployeeName = employeeName;
  //     }
  //     if(this.marked5){
  //       obj.Gender= this.exceldata1[i].gender;
  //     }
  //     if(this.marked6){
  //       let temp=moment(this.exceldata1[i].dateOfBirth).format("DD/MM/YYYY");
  //       obj.DateOfBirth = temp;
  //     }
  //     if(this.marked7){
  //       obj.Nationality= this.exceldata1[i].nationality;
  //     }
  //     if(this.marked21){
  //       obj.OfficialEmailId = this.exceldata1[i].officialEmailId;
  //     }
  //     if(this.marked8){
  //       obj.PanNo = this.exceldata1[i].panNo;
  //     }
  //     if(this.marked9){
  //       obj.AdharNo= this.exceldata1[i].adharNo;
  //     }

  //     if(this.marked10){
  //       obj.UANNo = this.exceldata1[i].universalAccNo;
  //     }
     
  //     if(this.marked11){
  //       obj.LocationName= this.exceldata1[i].locationName;
  //     }
  //     if(this.marked12){
  //       obj.EmployeeRole= this.exceldata1[i].roleName;
  //     }
     
  //     if(this.marked13){
  //       obj.Department= this.exceldata1[i].deptName;
  //     }
  //     if(this.marked14){
  //       obj.SubDepartmentName= this.exceldata1[i].subDeptName;
  //     }
  //     if(this.marked15){
  //       obj.Designation= this.exceldata1[i].descName;
  //     }
  //     if(this.marked16){
  //       obj.EmploymentType = this.exceldata1[i].employmentType;
  //     }
  //     if(this.marked17){
  //       obj.SubEmploymentType = this.exceldata1[i].subEmploymentType;
  //     }
  //     if(this.marked18){
  //       obj.ShiftType = this.exceldata1[i].shiftType;
  //     }
  //     if(this.marked19){
  //       if(this.exceldata1[i].empJoiningDetails != null){
  //       let temp=moment(this.exceldata1[i].empJoiningDetails.dateOfJoining).format("DD/MM/YYYY");
  //       obj.DateofJoining = temp;
  //       }
  //       else{
  //         obj.DateofJoining = "";
  //       }
  //     }
  //     // if(this.marked20){
  //     //   obj.PersonalEmailId = this.exceldata1[i].epersonalEmailId;
  //     // }
     
  //     // if(this.marked22){
  //     //   obj.ContractorName = this.exceldata1[i].conName;
  //     // }
  //     // if(this.marked23){
  //     //   obj.ContractorServiceName= this.exceldata1[i].conSerZTypeName;
  //     // }
 
  //     excelData.push(obj)
  //   }
  //   this.excelService.exportAsExcelFile(excelData, 'EmployeeInformationReport');
  // }

  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Employee-Information-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }
  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }
  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }
  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }
  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
  toggleVisibility10(e) {
    this.marked10 = e.target.checked;
  }

  toggleVisibility11(e) {
    this.marked11 = e.target.checked;
  }
  toggleVisibility12(e) {
    this.marked12 = e.target.checked;
  }
  toggleVisibility13(e) {
    this.marked13 = e.target.checked;
  }
  toggleVisibility14(e) {
    this.marked14 = e.target.checked;
  }
  toggleVisibility15(e) {
    this.marked15 = e.target.checked;
  }
  toggleVisibility16(e) {
    this.marked16 = e.target.checked;
  }
  toggleVisibility17(e) {
    this.marked17 = e.target.checked;
  }
  toggleVisibility18(e) {
    this.marked18 = e.target.checked;
  }
  toggleVisibility19(e) {
    this.marked19 = e.target.checked;
  }
  toggleVisibility20(e) {
    this.marked20 = e.target.checked;
  }

  toggleVisibility21(e) {
    this.marked21 = e.target.checked;
  }
  toggleVisibility35(e){
    this.marked9514=e.target.checked;
  }
  toggleVisibility22(e) {
    this.marked22 = e.target.checked;
  }
  toggleVisibility23(e) {
    this.marked23 = e.target.checked;
  }
  toggleVisibility28(e) {
    this.marked28 = e.target.checked;
  }
  toggleVisibility29(e) {
    this.marked29 = e.target.checked;
  }

  capturePdf() {
    var doc = new jsPDF('l', 'mm', 'a4');
    doc.autoTable({
        html: '#emp_info',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
          6: {halign:'center'},
          7: {halign:'left'},
          8: {halign:'left'},
          9: {halign:'left'},
          10: {halign:'left'},
          11: {halign:'center'},
          12: {halign:'left'},
          13: {halign:'left'},
          14: {halign:'left'},
          15: {halign:'left'},
          16: {halign:'center'},
          17: {halign:'left'},
          18: {halign:'left'},
          19: {halign:'left'},
          20: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.3, fontSize: 4
        },
        // didParseCell: function(cell, data) {
        //   if (cell.row.index === 3 && cell.row.section=="head") {
        //     cell.cell.styles.halign = 'right';
        //   }
        // },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Employee-Information-Report'+this.report_time+'.pdf');  
  }
 
  // capturePdf() {

  //   let item1 = {
  //     header: "SrNo",
  //     title: "srNo"
  //   }

  //   let item2 = {
  //     header: "EmpID",
  //     title: "empID"
  //   }

  //   let item3 = {
  //     header: "BiometricId",
  //     title: "biometricId"
  //   }
  //   let item4 = {
  //     header: "EmployeeName",
  //     title: "employeename"
  //   }
  //   let item5 = {
  //     header: "Gender",
  //     title: "gender"
  //   }
  //   let item6 = {
  //     header: "DateOfBirth",
  //     title: "dateOfBirth"
  //   }
    
  //   let item7 = {
  //     header: "Nationality",
  //     title: "nationality"
  //   }

  //   let item21 = {
  //     header: "OfficialEmailID",
  //     title: "officialEmailID"
  //   }

  //   let item8 = {
  //     header: "PAN Number",
  //     title: "PANNumber"
  //   }

  //   let item9 = {
  //     header: "AdharCardNo",
  //     title: "adharCardNo"
  //   }
  //   let item10 = {
  //     header: "UAN Number",
  //     title: "uAN Number"
  //   }

  //   let item11 = {
  //     header: "Location",
  //     title: "location"
  //   }
  //   let item12 = {
  //     header: "EmployeeRole",
  //     title: "employeeRole"
  //   }
  //   let item13 = {
  //     header: "Department",
  //     title: "department"
  //   }
  //   let item14 = {
  //     header: "SubDepartment",
  //     title: "subDepartment"
  //   }
   
  //   let item15 = {
  //     header: "Designation",
  //     title: "designation"
  //   }
    
  //   let item16 = {
  //     header: "EmploymentType",
  //     title: "employmenttype"
  //   }

  //   let item17 = {
  //     header: "SubEmploymentType",
  //     title: "subEmploymentType"
  //   }

  //   let item18 = {
  //     header: "ShiftType",
  //     title: "shifttype"
  //   }
  //   let item19 = {
  //     header: "DateofJoining",
  //     title: "dateofJoining"
  //   }
  //   // let item20 = {
  //   //   header: "PersonalEmailID",
  //   //   title: "personalEmailID"
  //   // }
   
  //   // let item22 = {
  //   //   header: "ContractorName",
  //   //   title: "contractorname"
  //   // }
  //   // let item23 = {
  //   //   header: "ContractorServiceName",
  //   //   title: "contractorServiceName"
  //   // }
 

  //   var doc = new jsPDF('l', 'mm', 'a4');
  //   //doc.addPage();
  //   var pageCount = doc.internal.getNumberOfPages();
  //   for (let i = 0; i < pageCount; i++) {
  //     doc.setPage(i);
  //     doc.setTextColor(48, 80, 139)
  //     //doc.setFontSize(10)
  //     doc.text(250, 12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
  //   }
  //   var col = [];

  //   if (this.marked1) {
  //     col.push(item1);
  //   }
  //   if (this.marked2) {
  //     col.push(item2);
  //   }
  //   if (this.marked3) {
  //     col.push(item3);
  //   }
  //   if (this.marked4) {
  //     col.push(item4);
  //   }
  //   if (this.marked5) {
  //     col.push(item5);
  //   }
  //   if (this.marked6) {
  //     col.push(item6);
  //   }
  //   if (this.marked7) {
  //     col.push(item7);
  //   }
  //   if (this.marked21) {
  //     col.push(item21);
  //   }
  //   if (this.marked8) {
  //     col.push(item8);
  //   }
  //   if (this.marked9) {
  //     col.push(item9);
  //   }
  //   if (this.marked10) {
  //     col.push(item10);
  //   }
  
  //   if (this.marked11) {
  //     col.push(item11);
  //   }
  //   if (this.marked12) {
  //     col.push(item12);
  //   }
  //   if (this.marked13) {
  //     col.push(item13);
  //   }
  //   if (this.marked14) {
  //     col.push(item14);
  //   }
  //   if (this.marked15) {
  //     col.push(item15);
  //   }
   
  //   if (this.marked16) {
  //     col.push(item16);
  //   }
  //   if (this.marked17) {
  //     col.push(item17);
  //   }
  //   if (this.marked18) {
  //     col.push(item18);
  //   }
  //   if (this.marked19) {
  //     col.push(item19);
  //   }
  //   // if (this.marked20) {
  //   //   col.push(item20);
  //   // }

   
  //   // if (this.marked22) {
  //   //   col.push(item22);
  //   // }
  //   // if (this.marked23) {
  //   //   col.push(item23);
  //   // }
   

  //   var rows = [];
  //   var counter = 0;
  //   var rowCountModNew = this.exceldata1;
  //   console.log("pdf generation data is",rowCountModNew);
  //   rowCountModNew.forEach(element => {
  //     counter = counter + 1;
  //     let obj = []
  //     if (this.marked1) {
  //       obj.push(counter);
  //     }
  //     if(this.marked2){
  //       obj.push(element.empId);
  //     }
  //     if (this.marked3) {
  //       obj.push(element.bioId);
  //     }
  //     if(this.marked4){
  //       let employeeName;
  //       if(element.middleName == null){
  //         employeeName = element.fullName + " " + element.lastName;
  //       }
  //       else{
  //         employeeName = element.fullName + " " +element.middleName+ " "+ element.lastName;
  //       }
        
  //       if(employeeName  == null){
  //         obj.push('');
  //       }
  //       else{
  //         obj.push(employeeName);
  //       }
  //     }
     
  //     if (this.marked5) {
  //       obj.push(element.gender);
  //     }
  //     if(this.marked6){
  //       obj.push(element.dateOfBirth);
  //     }
  //     if (this.marked7) {
  //       obj.push(element.nationality);
  //     }
  //     if (this.marked21) {
  //       obj.push(element.officialEmailId);
  //     }
  //     if (this.marked8) {
  //       obj.push(element.panNo);
  //     }


  //     if (this.marked9) {
  //       obj.push(element.adharNo);
  //     }
     
  //     if (this.marked10) {
  //       obj.push(element.universalAccNo);
  //     }
     
  //     if(this.marked11){
  //       obj.push(element.locationName);
  //     }
  //     if (this.marked12) {
  //       obj.push(element.roleName);
  //     }
  //     if (this.marked13) {
  //       obj.push(element.deptName);
  //     }
  //     if (this.marked14) {
  //       obj.push(element.subDeptName);
  //     }
  //     if (this.marked15) {
  //       obj.push(element.descName);
  //     }

  //     if (this.marked16) {
  //       obj.push(element.employmentType);
  //     }
  //     if(this.marked17){
  //       obj.push(element.subEmploymentType);
  //     }
  //     if (this.marked18) {
  //       obj.push(element.shiftType);
  //     }
  //     if (this.marked19) {
  //       obj.push(element.empJoiningDetails.dateOfJoining);
  //     }
  //     // if(this.marked20){
  //     //   obj.push(element.epersonalEmailId);
  //     // }
    
  //     // if (this.marked22) {
  //     //   obj.push(element.conName);
  //     // }
  //     // if (this.marked23) {
  //     //   obj.push(element.conSerZTypeName);
  //     // }
  //     rows.push(obj);
  //   });

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(22, 14, 'Company Name :' + this.compName)

  //   // doc.setTextColor(0, 0, 0)
  //   // doc.setFontSize(7)
  //   // doc.setFontStyle("Arial")
  //   // doc.text(30, 44, 'Abbreviation:-  WD: Working Day , WO : Weekly Off , HO : Holiday , WOH : Week Off And Holiday')

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(120, 20, 'Employee Information Report')

  //   // doc.setTextColor(40, 80, 139)
  //   // doc.setFontSize(7)
  //   // doc.setFontStyle("Arial")
  //   // doc.text(263, 24, 'From date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") + ' ' + 'To date' + ' : ' + this.datePipe.transform(this.toDate, "dd-MMM-yyyy")
  //   // )

  //   doc.setTextColor(0, 0, 0)
  //   doc.setFontSize(9)
  //   doc.setFontStyle("Arial")
  //   doc.text(212, 48, 'Report date' + ' : ' + this.datePipe.transform(this.todaysDate, "dd-MMM-yyyy hh:mm:ss a"));
  //   console.log("rows", rows);

  //   doc.autoTable(col, rows ,{
  //     tableLineColor: [189, 195, 199],
  //     tableLineWidth: 0.5,
  //     columnStyles: {
  //       1: {halign:'right'},
  //       2: {halign:'right'},
  //       3: {halign:'left'},
  //       4: {halign:'left'},
  //       6: {halign:'left'},
  //       9: {halign:'left'},
  //       10: {halign:'left'},
  //       11: {halign:'left'},
  //       12: {halign:'left'},
  //       13: {halign:'left'},
  //       14: {halign:'left'},
  //       15: {halign:'left'},
  //       16: {halign:'left'},
  //       17: {halign:'left'},
  //       18: {halign:'left'},
  //     },
  //     headerStyles: {
  //       fillColor: [103, 132, 130],
  //     },
  //     styles: {
  //       halign: 'center',
  //       cellPadding: 0.7, 
  //       fontSize: 5
  //     },
  //     theme: 'grid',
  //     margin: { top: 60, bottom: 50 },
  //   });
  //   doc.save('EmployeeInformationReport' + this.datePipe.transform(this.todaysDate, "dd-MM-yyyy hh:mm:ss") + '.pdf');
  // }
}
