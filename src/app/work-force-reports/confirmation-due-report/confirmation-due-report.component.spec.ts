import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationDueReportComponent } from './confirmation-due-report.component';

describe('ConfirmationDueReportComponent', () => {
  let component: ConfirmationDueReportComponent;
  let fixture: ComponentFixture<ConfirmationDueReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationDueReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationDueReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
