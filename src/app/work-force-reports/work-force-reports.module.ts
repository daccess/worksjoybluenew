import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EmployeeInformationReportComponent } from './employee-information-report/employee-information-report.component';
import { WorkForceReportsComponent } from './work-force-reports/work-force-reports.component';
import { routing } from './work-force-reports.routing';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ExcelService } from '../work-force-reports/excel.service';
import { JoiningUpdatesReportComponent } from './joining-updates-report/joining-updates-report.component';
import { LeavingUpdatesReportComponent } from './leaving-updates-report/leaving-updates-report.component';
import { ConfirmationDueReportComponent } from './confirmation-due-report/confirmation-due-report.component';
import { GratuityReportComponent } from './gratuity-report/gratuity-report.component';
import { AssetReportComponent } from './asset-report/asset-report.component';


@NgModule({
  imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [
    EmployeeInformationReportComponent, 
    WorkForceReportsComponent, JoiningUpdatesReportComponent, LeavingUpdatesReportComponent, ConfirmationDueReportComponent, GratuityReportComponent, AssetReportComponent
  ],
  providers: [
    ExcelService,
    DatePipe
  ]
})
export class WorkForceReportsModule { }
