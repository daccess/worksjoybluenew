import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoiningUpdatesReportComponent } from './joining-updates-report.component';

describe('JoiningUpdatesReportComponent', () => {
  let component: JoiningUpdatesReportComponent;
  let fixture: ComponentFixture<JoiningUpdatesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoiningUpdatesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoiningUpdatesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
