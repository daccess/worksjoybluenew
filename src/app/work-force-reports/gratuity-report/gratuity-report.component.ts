import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';


@Component({
  selector: 'app-gratuity-report',
  templateUrl: './gratuity-report.component.html',
  styleUrls: ['./gratuity-report.component.css']
})
export class GratuityReportComponent implements OnInit {

  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;

 

  srNoCheckbox = true
  empCodeCheckbox = true;
  employeeNameCheckbox = true;
  biometricIdCheckbox = true;
  departmentCheckbox = true;
  designationCheckbox = true;
  subCategoryCheckbox = true;
  joiningDateCheckbox = true;
  locationCheckbox = true;


  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  user_data=[];
  report_time: string;
  
  constructor(private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });
    console.log("Gratuity Report", this.AllReport);
    this.exceldata = this.AllReport;
    this.exceldata1=this.AllReport;
  }

  ngOnInit() {
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
 

  /*Function to generate Pdf */
  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Gratuity-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#gratuity_id',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Gratuity-Report'+this.report_time+'.pdf');  
  }
 
}
