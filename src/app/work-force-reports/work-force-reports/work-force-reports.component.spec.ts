import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkForceReportsComponent } from './work-force-reports.component';

describe('WorkForceReportsComponent', () => {
  let component: WorkForceReportsComponent;
  let fixture: ComponentFixture<WorkForceReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkForceReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkForceReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
