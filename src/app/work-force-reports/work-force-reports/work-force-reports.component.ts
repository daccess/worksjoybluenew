import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work-force-reports',
  templateUrl: './work-force-reports.component.html',
  styleUrls: ['./work-force-reports.component.css']
})
export class WorkForceReportsComponent implements OnInit {
  reportName: string;
  constructor() {
    this.reportName = sessionStorage.getItem('reportName');
   }

  ngOnInit() {
  }

}
