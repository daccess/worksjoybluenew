import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
import { DatePipe } from '@angular/common';
import { Workbook } from 'exceljs';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {
  title: string;

  constructor( private datePipe :  DatePipe) { }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    //var reportName = sessionStorage.getItem('reportName');
    if(excelFileName == 'employeeInformationReport'){
      this.title = 'Employee Information Report';
    }
    if(excelFileName == 'joiningUpdatesReport'){
      this.title = 'Joining Updates Report';
    }
    if(excelFileName == 'LeavingUpdatesReport'){
      this.title = 'Leaving Updates Report';
    }
    if(excelFileName == 'ConfirmationDueReport'){
      this.title = 'Confirmation Due Report';
    }
    if(excelFileName == 'GratuityReport'){
      this.title = 'Gratuity Report';
    }
    if(excelFileName == 'AssetReport'){
      this.title = 'Asset Report';
    }
   
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
   
    //console.log('worksheet',worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);

    let titleRow = worksheet.addRow(this.title);
    titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, underline: 'double', bold: true }
    worksheet.addRow([titleRow]);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + this.datePipe.transform(new Date(),"ddMMyyyy hh:mm") + EXCEL_EXTENSION);
  }

}
