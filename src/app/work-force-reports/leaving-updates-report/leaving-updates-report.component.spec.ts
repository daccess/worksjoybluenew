import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavingUpdatesReportComponent } from './leaving-updates-report.component';

describe('LeavingUpdatesReportComponent', () => {
  let component: LeavingUpdatesReportComponent;
  let fixture: ComponentFixture<LeavingUpdatesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavingUpdatesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavingUpdatesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
