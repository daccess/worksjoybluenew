import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../excel.service';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leaving-updates-report',
  templateUrl: './leaving-updates-report.component.html',
  styleUrls: ['./leaving-updates-report.component.css']
})
export class LeavingUpdatesReportComponent implements OnInit {

 
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;
  marked10 = true;
  marked11 = true;
  marked12 = true;

  srNoCheckbox = true
  empCodeCheckbox = true;
  biometricIdCheckbox = true;
  employeeNameCheckbox = true;
  functionUnitCheckbox = true;
  departmentCheckbox = true;
  designationCheckbox = true;
  locationCheckbox = true;
  dateOfJoiningCheckbox = true;
  typeCheckbox = true;
  subCategoryCheckbox = true;
  dateOfLeavingCheckbox = true;
  reasonOfLeavingCheckbox = true;
  
  AllReportData: any;
  AllReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  user_data=[];
  report_time: string;
  
  constructor(private excelService: ExcelService, 
    public toastr: ToastrService, 
    private datePipe: DatePipe,
    private InfoService: InfoService,
    private router: Router) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
      this.AllReport = JSON.parse(message);
      }
      else{
      this.router.navigateByUrl('layout/reports/reports');
      }
    });
    console.log("Leaving Updates Report", this.AllReport);
    this.exceldata = this.AllReport;
    this.exceldata1=this.AllReport;
  }

  ngOnInit() {
  }

  // exportAsXLSX(): void {
  //   let excelData = new Array();
  
  //   for (let i = 0; i < this.exceldata1.length; i++) {
  //     let obj: any = {}
  //     if (this.marked1) {
  //       obj.SrNo = i+1;
  //     }

  //     if (this.marked2) {
  //       obj.EmpId = this.exceldata1[i].empId;
  //     }
  //     if (this.marked3) {
  //       obj.BiometricId = this.exceldata1[i].bioId;
  //     }

  //     if (this.marked4) {
        
  //       obj.EmployeeName = this.exceldata1[i].fullName + this.exceldata1[i].lastName;
  //     }

  //     if (this.marked5) {
  //       obj.Department = this.exceldata1[i].deptName;
  //     }

  //     if (this.marked6) {
  //       obj.Designation = this.exceldata1[i].descName;
  //     }

  //     if (this.marked7) {
  //       obj.Location = this.exceldata1[i].locationName;
  //     }

  //     if (this.marked8) {
  //       let temp=moment(this.exceldata1[i].joiningDate).format("DD/MM/YYYY");
  //       obj.DateOfJoining = temp;
  //     }
  //     if (this.marked9) {
  //       obj.EmployeeCategory = this.exceldata1[i].empTypeName;
  //     }

  //     if (this.marked10) {
  //       obj.EmployeeSubcategory = this.exceldata1[i].subEmpTypeName;
  //     }
    
  //     if (this.marked11) {
  //       let temp=moment(this.exceldata1[i].relievingDate).format("DD/MM/YYYY");
  //       obj.DateofLeaving = temp;
  //     }
  //     if (this.marked12) {
  //       obj.ReasonofLeaving = this.exceldata1[i].requestedRelievingReson;
  //     }
  //     excelData.push(obj)
  //   }
  //   console.log("data for excel**************", excelData);
  //   this.excelService.exportAsExcelFile(excelData, 'leavingUpdatesReport');
  // }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
  toggleVisibility10(e) {
    this.marked10 = e.target.checked;
  }
  toggleVisibility11(e) {
    this.marked11 = e.target.checked;
  }
  toggleVisibility12(e) {
    this.marked11 = e.target.checked;
  }

  /*Function to generate Pdf */
  // capturePdf() {

  //   let item1 = {
  //     header: "S/N",
  //     title: "s/N"
  //   }

  //   let item2 = {
  //     header: "EmpId",
  //     title: "emp Code"
  //   }

  //   let item3 = {
  //     header: "BiometricId",
  //     title: "biometricId"
  //   }

  //   let item4 = {
  //     header: "EmployeeName",
  //     title: "employee Name"
  //   }

  //   let item5 = {
  //     header: "Department",
  //     title: "department"
  //   }

  //   let item6 = {
  //     header: "Designation",
  //     title: "designation"
  //   }

  //   let item7 = {
  //     header: "Location",
  //     title: "location"
  //   }

  //   let item8 = {
  //     header: "DateOfJoining",
  //     title: "dateOfJoining"
  //   }

  //   let item9 = {
  //     header: "EmployeeCategory",
  //     title: "employeecategory"
  //   }

  //   let item10 = {
  //     header: "EmployeeSubCategory",
  //     title: "employeeSubcategory"
  //   }

  //   let item11 = {
  //     header: "DateOfLeaving",
  //     title: "date of Leaving"
  //   }

  //   let item12 = {
  //     header: "ReasonOfLeaving",
  //     title: "reason of Leaving"
  //   }

  //   var doc = new jsPDF('l', 'mm', 'a4');
  //   //doc.addPage();
  //   var pageCount = doc.internal.getNumberOfPages();
  //   for (let i = 0; i < pageCount; i++) {
  //     doc.setPage(i);
  //     doc.setTextColor(48, 80, 139)
  //     //doc.setFontSize(8)
  //     doc.text(250, 12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
  //   }
  //   var col = [];

  //   if (this.marked1) {
  //     col.push(item1);
  //   }
  //   if (this.marked2) {
  //     col.push(item2);
  //   }
  //   if (this.marked3) {
  //     col.push(item3);
  //   }
  //   if (this.marked4) {
  //     col.push(item4);
  //   }
  //   if (this.marked5) {
  //     col.push(item5);
  //   }
  //   if (this.marked6) {
  //     col.push(item6);
  //   }
  //   if (this.marked7) {
  //     col.push(item7);
  //   }
  //   if (this.marked8) {
  //     col.push(item8);
  //   }
  //   if (this.marked9) {
  //     col.push(item9);
  //   }
  //   if (this.marked10) {
  //     col.push(item10);
  //   }
  //   if (this.marked11) {
  //     col.push(item11);
  //   }
  //   if (this.marked12) {
  //     col.push(item12);
  //   }
    
  //   var rows = [];
  //   var counter = 0;
  //   var rowCountModNew = this.exceldata1;
  //   rowCountModNew.forEach(element => {
  //     counter = counter+1;
  //     let obj = []
  //     if (this.marked1) {
  //       obj.push(counter);
  //     }
  //     if (this.marked2) {
  //       obj.push(element.empId);
  //     }
  //     if (this.marked3) {
  //       obj.push(element.bioId);
  //     }
  //     if (this.marked4) {
  //      if (element.fullName == null && element.lastName == null) {
  //           obj.push('');
  //          }else{
  //              obj.push(element.fullName+ ' '+element.lastName);
  //          }
  //     }
  //     if (this.marked5) {
  //       obj.push(element.deptName);
  //     }
  //     if (this.marked6) {
  //       obj.push(element.descName);
  //     }
  //     if (this.marked7) {
  //       obj.push(element.locationName);
  //     }
  //     if (this.marked8) {
  //       obj.push(element.dateOfJoining);
  //     }
  //     if (this.marked9) {
  //       obj.push(element.empTypeName);
  //     }
  //     if (this.marked10) {
  //       obj.push(element.subEmpTypeName);
  //     }
  //     if (this.marked11) {
  //       obj.push(element.relievingDate);
  //     }
  //     if (this.marked12) {
  //       obj.push(element.requestedRelievingReson);
  //     }
     
  //     rows.push(obj);
  //   });

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(22, 14, 'Company Name :' + this.compName)

  //   // doc.setTextColor(0, 0, 0)
  //   // doc.setFontSize(7)
  //   // doc.setFontStyle("Arial")
  //   // doc.text(30, 44, 'Abbreviation:-  WD: Working Day , WO : Weekly Off , HO : Holiday , WOH : Week Off And Holiday')

  //   doc.setTextColor(48, 80, 139)
  //   doc.setFontSize(12)
  //   doc.setFontStyle("Arial")
  //   doc.text(120, 20, 'Leaving Updates Report')

  //   if(this.fromDate == this.toDate){
  //     doc.setTextColor(40, 80, 139)
  //     doc.setFontSize(9)
  //     doc.setFontStyle("Arial")
  //     doc.text(120, 26, 'For date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") 
  //     )
  //   }
  //   else{
  //     doc.setTextColor(40, 80, 139)
  //     doc.setFontSize(9)
  //     doc.setFontStyle("Arial")
  //     doc.text(110, 26, 'From date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") + ' ' + 'To date' + ' : ' + this.datePipe.transform(this.toDate, "dd-MMM-yyyy")
  //     )
  
  //   }

  //   doc.setTextColor(0, 0, 0)
  //   doc.setFontSize(9)
  //   doc.setFontStyle("Arial")
  //   doc.text(212, 48, 'Report date' + ' : ' + this.datePipe.transform(this.todaysDate, "dd-MMM-yyyy hh:mm:ss a"))
  //   console.log("rows", rows);

  //   doc.autoTable(col, rows, {
  //     columnStyles: {
  //       1: {halign:'right'},
  //       2: {halign:'right'},
  //       3: {halign:'left'},
  //       4: {halign:'left'},
  //       5: {halign:'left'},
  //       6: {halign:'left'},
  //       8: {halign:'left'},
  //       9: {halign:'left'},
  //       10: {halign:'left'},
  //       11: {halign:'left'},
  //     },
  //     tableLineColor: [189, 195, 199],
  //     tableLineWidth: 0.5,
  //     tableWidth: 'wrap',
  //     headerStyles: {
  //       fillColor: [103, 132, 130],
  //     },
  //     styles: {
  //       halign: 'center',
  //       cellPadding: 0.9, 
  //       fontSize: 7
  //     },
  //     theme: 'grid',
  //     pageBreak:'avoid',
  //     margin: { top: 60, bottom: 50 }
  //   });
  //   doc.save('LeavingUpdatesReport' + this.datePipe.transform(this.todaysDate, "dd-MM-yyyy hh:mm:ss") + '.pdf');

  // }

  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Leaving-Updates-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#leaving-updates',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'right'},
          3: {halign:'right'},
          4: {halign:'left'},
          5: {halign:'left'},
          6: {halign:'left'},
          7: {halign:'left'},
          8: {halign:'left'},
          9: {halign:'left'},
          10: {halign:'left'},
          11: {halign:'left'},
          12: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Leaving-Updates-Report'+this.report_time+'.pdf');  
  }

}
