import { Routes, RouterModule } from '@angular/router'
import { MailsettingComponent } from './mailsetting.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: MailsettingComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'mailsetting',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'mailsetting',
                    component: MailsettingComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);