import { Component, OnInit } from '@angular/core';
import { mailSetting } from '../shared/model/mailSetting';
import { MailSettingService } from '../shared/services/mailSettingService';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-mailsetting',
  templateUrl: './mailsetting.component.html',
  styleUrls: ['./mailsetting.component.css']
})
export class MailsettingComponent implements OnInit {

  mailSettingModel:mailSetting
  empDetails: any;
  emailList:any;
  isEdit:boolean = false
  constructor(private mailSettingService:MailSettingService,public toastr: ToastrService) { }

  ngOnInit() {
    this.mailSettingModel =new mailSetting();
    this.empDetails = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.getConfigList()
    this.mailSettingModel.ssl = true
    this.mailSettingModel.status = false
  }

  save(){
    
  this.mailSettingModel.empOfficialId= this.empDetails.empOfficialId;
  this.mailSettingModel.compId = this.empDetails.compId;
  
  if(this.isEdit){
    this.mailSettingService.updateConfiguration(this.mailSettingModel).subscribe(data=>{
      this.toastr.success('Email Configuration Updated Successfully!');
      // this.mailSettingModel = new mailSetting();
      this.isEdit = false
      this.getConfigList();

    }, err => {
      this.toastr.error('Email Configuration Update Failed');
    })
  }else{
    this.mailSettingService.emailConfiguration(this.mailSettingModel,).subscribe(data=>{
      this.mailSettingModel = new mailSetting();
      this.getConfigList();
      this.toastr.success('Email Configuration Inserted Successfully!');
    }, err => {
      this.toastr.error('Email Configuration Insert Failed');
    })
  }
   
  }

  getConfigList(){
    this.mailSettingService.getEmailList(this.empDetails.compId).subscribe(data=>{
      this.emailList = data.result
      this.mailSettingModel
      this.mailSettingModel=data.result
    })
  }

  getEmailById(item){
    this.mailSettingService.getEmailbyid(item).subscribe(data=>{
      this.mailSettingModel = data.result;
      this.isEdit= true
    })
  }
  
  deleteEmailById(item){
    this.mailSettingService.deleteEmailbyid(item).subscribe(data=>{
   this.getConfigList();
      this.toastr.success('Email Configuration Deleted Successfully!')
    }, err => {
      this.toastr.error('Email Configuration Delete Failed');
    })
  }
  
  statusChange(event,data){
    
    if(data.status==false){
   
      
      this.mailSettingService.statusChangecall(this.empDetails.compId,data.emailConfigId).subscribe(data=>{
        this.getConfigList();
      })
    }
  }
}
