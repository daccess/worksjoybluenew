import { routing } from './multiplereportspunches.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import{CommonModule, DatePipe } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
//import { MonthlyovertimereportComponent } from './monthlyovertimereport.component';
import { MultipleReportPunchesComponent } from './multiple-report-punches.component';


@NgModule({
    declarations: [
        MultipleReportPunchesComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule
    ],
    providers: [DatePipe]
  })
  export class MultipleReportsPunchesModule { 
      constructor(){

      }
  }
  