import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
//import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
@Component({
  selector: 'app-multiple-report-punches',
  templateUrl: './multiple-report-punches.component.html',
  styleUrls: ['./multiple-report-punches.component.css'],
  providers:[ExcelService]
})
export class MultipleReportPunchesComponent implements OnInit {
  AllReportData: any;
  AllMultipleReport: any;
  exceldata: any;
  exceldata1 = [];
  fromDate: any;
  toDate:any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  chkTime:any;
  punches_data:any;
  report_time: string;
  constructor(private datePipe: DatePipe,private excelService:ExcelService,private InfoService:InfoService,private router: Router) { 
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.AllMultipleReport = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    this.exceldata = this.AllMultipleReport;
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
  }

  ngOnInit() {
    //generate required array
    let k=0;
    for(let i=0;i<this.exceldata.length;i++){
      if(this.exceldata[i].multiplePunchesReportResDtoList){
        this.exceldata[i].punches_data=[];
          this.punches_data = Array.from(this.exceldata[i].multiplePunchesReportResDtoList.reduce((m, {attenDate, chkTime}) => 
            m.set(attenDate, [...(m.get(attenDate) || []), chkTime]), new Map), ([attenDate, chkTime]) => ({attenDate, chkTime}));

          this.exceldata[i].punches_data=this.punches_data;
      }
      //set Sr.No
      if(this.exceldata[i].punches_data){
        for(let j=0;j<this.exceldata[i].punches_data.length;j++){
          k++;
          this.exceldata[i].punches_data[j].Sr=k;
        }
      }
    }
  }
  getCheckInTime(timestamp){
    if(timestamp!=null){
      let temp_in_time=new Date(timestamp);
      this.chkTime=moment(temp_in_time).format('HH.mm');
      return this.chkTime+',';
    }
    
  }
  getPunches(data,index){
      for(let i=0;i<data.length;i++){
        if(i==index){
            let temp_array=[];
            for(let j=0;j<data[i].chkTime.length;j++){
              let current_punch=this.getCheckInTime(data[i].chkTime[j]);
              temp_array.push(current_punch);
            }
            var result = temp_array.join();
            return result;
        }
      }
  }
  exportAsXLSX():void {
    let excelData = new Array();
    for (let i = 0; i < this.exceldata.length; i++) {
      if(this.exceldata[i].multiplePunchesReportResDtoList!=null){
        for(let j=0;j<this.exceldata[i].punches_data.length;j++){
          let obj : any= {
            "Sr.No": i+1,
            "Employee ID": this.exceldata[i].multiplePunchesNameAndIdResDto.empId,
            "Designation": this.exceldata[i].multiplePunchesNameAndIdResDto.descName,
            "Employee Name":this.exceldata[i].multiplePunchesNameAndIdResDto.fullName.toUpperCase()+'  '+this.exceldata[i].multiplePunchesNameAndIdResDto.lastName.toUpperCase(),
            "Attendence Date": this.exceldata[i].punches_data[j].attenDate,
            "Punches": this.getPunches(this.exceldata[i].punches_data,j),
           }
           excelData.push(obj);
        }
      } 
    }
    this.excelService.exportPunchExcelFile(excelData, 'multiple_punches_Report',this.fromDate,this.toDate);
  }
  //excel
  exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    filename='Multiple-Punches-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('l', 'mm', 'a4');
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
     doc.autoTable({
         html: '#Punches',
         columnStyles: {
           1: {halign:'center'},
           2: {halign:'left'},
           3: {halign:'left'},
           4: {halign:'left'},
           5: {halign:'left'},
           7: {halign:'left'},
         },
         tableLineColor: [190, 191, 191],
         tableLineWidth: 0.75,
         headStyles : {
           fillColor: [103, 132, 130],
         },
         styles: {
           halign: 'center',
           cellPadding: 0.5, fontSize: 6
         },
         theme: 'grid',
         pageBreak:'avoid',
         margin: { top: 20,  bottom: 20 }
       });
       var pageCount = doc.internal.getNumberOfPages();
       for (let i = 0; i < pageCount; i++) {
         doc.setPage(i);
         doc.setTextColor(0, 0, 0);
         doc.setFontSize(9);
         doc.setFontStyle("Arial");
         doc.setTextColor(48, 80, 139);
         doc.setFontSize(7);
         doc.text(270, 7, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
       }
     doc.save('Multiple-Punches-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a')+'.pdf'); 
 }

}
