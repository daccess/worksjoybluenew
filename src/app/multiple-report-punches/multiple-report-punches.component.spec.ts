import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleReportPunchesComponent } from './multiple-report-punches.component';

describe('MultipleReportPunchesComponent', () => {
  let component: MultipleReportPunchesComponent;
  let fixture: ComponentFixture<MultipleReportPunchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleReportPunchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleReportPunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
