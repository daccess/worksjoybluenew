import { Routes, RouterModule } from '@angular/router'
//import { MonthlyovertimereportComponent } from './monthlyovertimereport.component';
import { MultipleReportPunchesComponent } from './multiple-report-punches.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: MultipleReportPunchesComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'multiplereportspunches',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'multiplereportspunches',
                    component: MultipleReportPunchesComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);