import { routing } from './document-master.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { DeviceMasterService } from '../shared/services/deviceMasterService';
import { DocumentMasterComponent } from './document-master.component';
import { DocumentsettingComponent } from './pages/documentsetting/documentsetting.component';


@NgModule({
    declarations: [
        DocumentMasterComponent,
        DocumentsettingComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule.forRoot()
     
    ],
    providers: [DeviceMasterService]
  })
  export class DocumentMasterModule { 
      constructor(){

      }
  }
  