import { Routes, RouterModule } from '@angular/router'

import { DocumentMasterComponent } from './document-master.component';
import { DocumentsettingComponent } from './pages/documentsetting/documentsetting.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: DocumentMasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'documentmaster',
                    pathMatch :'full'
                    
                },
                {
                    path:'documentmaster',
                    component: DocumentsettingComponent
                }

            ]

    }
]

export const routing = RouterModule.forChild(appRoutes);