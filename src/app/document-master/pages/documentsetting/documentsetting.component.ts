import { Component, OnInit } from '@angular/core';
import { DocumentMaster} from '../../modal/documents'
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../../../shared/configurl';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-documentsetting',
  templateUrl: './documentsetting.component.html',
  styleUrls: ['./documentsetting.component.css']
})
export class DocumentsettingComponent implements OnInit {
documentModal : DocumentMaster
  loggedUser: any;
  compId: any;
  baseurl = MainURL.HostUrl;
  documentdata: any;
  Selectededitobj: any;
  isedit = false;
  dltmodelFlag: boolean;
  dltObje: any;
  documentName: string;
  errordata: any;
  msg: any;
  errflag: boolean;
  constructor(public httpService: HttpClient,private toastr: ToastrService,public Spinner :NgxSpinnerService) { 
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId;
    this.documentModal = new DocumentMaster();
    this.getallDocument();
  }
  ngOnInit() {
    this.documentModal.mandatory = false;
  }
  uiswitch(event){ 
    this.flag1 = !this.flag1;
  }
  StatusFlag:boolean=false
  onSubmit(f) {
    this.StatusFlag=false
    this.Spinner.show();
    this.documentModal.status=this.flag1;
    this.documentModal.compId = this.compId
    if(this.isedit == false){
        let url = this.baseurl + '/createDocument';
        this.httpService.post(url,this.documentModal).subscribe(data => {
        this.Spinner.hide()
        this.toastr.success('document-Master Information Inserted Successfully!', 'document-Master');
        this.getallDocument();
        f.reset();
        setTimeout(() => {
          this.documentModal.mandatory = false;
        }, 100);
      },
      (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'document-Master');
      });
    }
    else{
      this.errflag = false
      let url = this.baseurl + '/updateDocument';
      this.httpService.put(url,this.documentModal).subscribe(data => {   
      this.Spinner.hide();
      this.toastr.success('document-Master Information Updated Successfully!', 'document-Master');
      this.getallDocument();
      f.reset();
      setTimeout(() => {
        this.documentModal.mandatory = false;
      }, 100);
      this.isedit = false
        },(err: HttpErrorResponse) => {
          this.Spinner.hide()
          this.toastr.error('Server Side Error..!', 'document-Master');
        });
    }
  }
  dltmodelEvent(f){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    f.reset();
    this.reset();
    setTimeout(() => {
      this.documentModal.mandatory = false;
    }, 100);
    this.Spinner.show();
  }

  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje =obj.documentMasterId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object){
    if(this.dltmodelFlag==true){
      if(this.dltmodelFlag==true){
        let url1 = this.baseurl + '/deleteDocument/';
        this.documentModal.documentMasterId = this.dltObje;
        this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Delete Successfully', 'document-Master');
          this.getallDocument();
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Server Side Error..!', 'document-Master');
        });
      }
    }

  }
  error(){ 
    if (this.documentName!= this.documentModal.documentName){
    this.compId = this.loggedUser.compId
    let url = this.baseurl + '/docMaster?compId='+this.compId+'&documentName='+this.documentModal.documentName;
    this.httpService.get(url).subscribe((data :any) => {
       this.errordata= data["result"];
       this.msg= data.message;
       this.toastr.error(this.msg);
       this.errflag = true;
      },
      (err: HttpErrorResponse) => {
        this.errflag = false;
      });
  }
}

  edit(object){
    this.Selectededitobj = object.documentMasterId;
    this.isedit = true;
    this.StatusFlag=true;
    this.editobject(this.Selectededitobj);
    this.flag1 = object.status;
  }
  flag1 : boolean = false;
  editobject(getobject){
      let urledit = this.baseurl + '/getDocumentById/';
      this.httpService.get(urledit + getobject).subscribe(data => {
      this.documentModal.documentName = data ['result'].documentName
      this.documentModal.mandatory = data ['result'].mandatory
      this.documentModal.documentMasterId = data ['result'].documentMasterId
      this.documentModal.status = data ['result'].status
      this.documentName = this.documentModal.documentName
    },
    (err: HttpErrorResponse) => {
    });
}
  dataTableFlag : boolean = true;
  getallDocument(){
    let url = this.baseurl + '/documentByList';
    this.dataTableFlag = false;
    this.httpService.get(url).subscribe(data => {
        this.documentdata = data["result"];
        this.dataTableFlag = true;
         setTimeout(function () {
           $(function () {
             var table = $('#CompTable').DataTable();      
           });
         }, 1000);
      },(err: HttpErrorResponse) => {
    });
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
}
