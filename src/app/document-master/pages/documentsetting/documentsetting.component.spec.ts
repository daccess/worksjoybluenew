import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsettingComponent } from './documentsetting.component';

describe('DocumentsettingComponent', () => {
  let component: DocumentsettingComponent;
  let fixture: ComponentFixture<DocumentsettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
