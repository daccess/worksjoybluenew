import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontDeskDocumentRouting } from './frontdesk-document-history-list-routing.module';
import { FrontdeskDocumentHistoryListComponent } from './frontdesk-document-history-list.component';

@NgModule({
  imports: [
    CommonModule,
    FrontDeskDocumentRouting
  ],
  declarations: [
    FrontdeskDocumentHistoryListComponent
  ]
})
export class FrontdeskDocumentHistoryListModule { }
