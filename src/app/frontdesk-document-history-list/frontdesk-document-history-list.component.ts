import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-frontdesk-document-history-list',
  templateUrl: './frontdesk-document-history-list.component.html',
  styleUrls: ['./frontdesk-document-history-list.component.css']
})
export class FrontdeskDocumentHistoryListComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  AlllabourData: any;
  empid: any;

  constructor(private router:Router,private httpservice:HttpClient) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token
    this.empid=this.users.roleList.empId
    this.getAllDocumentLabourHistory()
  }

  

    getAllDocumentLabourHistory() {
      let url = this.baseurl + `/getApprovedFrontDeskLabourList/${this.empid}`;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.get(url, { headers }).subscribe(data => {
        
        this.AlllabourData= data['result']
        setTimeout(function () {
  
          $(function () {
  
            var table = $('#emptable').DataTable({
  
              retrieve: true,
              searching:false
  
           
  
            })
  
          });
        }, 100);
      
      },
        (err: HttpErrorResponse) => {
  
        })
  
    }
   
  }

