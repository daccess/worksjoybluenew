import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontdeskDocumentHistoryListComponent } from './frontdesk-document-history-list.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: FrontdeskDocumentHistoryListComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'frontdeskdocumenthistorylist',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'frontdeskdocumenthistorylist',
                  component: FrontdeskDocumentHistoryListComponent
              }

          ]

  },

  // otherwise redirect to home

]

export const FrontDeskDocumentRouting = RouterModule.forChild(appRoutes);