import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontdeskDocumentHistoryListComponent } from './frontdesk-document-history-list.component';

describe('FrontdeskDocumentHistoryListComponent', () => {
  let component: FrontdeskDocumentHistoryListComponent;
  let fixture: ComponentFixture<FrontdeskDocumentHistoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontdeskDocumentHistoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontdeskDocumentHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
