import { FrontdeskDocumentHistoryListModule } from './frontdesk-document-history-list.module';

describe('FrontdeskDocumentHistoryListModule', () => {
  let frontdeskDocumentHistoryListModule: FrontdeskDocumentHistoryListModule;

  beforeEach(() => {
    frontdeskDocumentHistoryListModule = new FrontdeskDocumentHistoryListModule();
  });

  it('should create an instance', () => {
    expect(frontdeskDocumentHistoryListModule).toBeTruthy();
  });
});
