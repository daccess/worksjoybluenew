import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { staffRouting } from './add-emp-staff-routing.module';
import { AddEmpStaffComponent } from './add-emp-staff.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    staffRouting,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    AddEmpStaffComponent
  ]
})
export class AddEmpStaffModule { }
