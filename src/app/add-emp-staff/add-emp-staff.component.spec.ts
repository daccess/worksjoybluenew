import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEmpStaffComponent } from './add-emp-staff.component';

describe('AddEmpStaffComponent', () => {
  let component: AddEmpStaffComponent;
  let fixture: ComponentFixture<AddEmpStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEmpStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEmpStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
