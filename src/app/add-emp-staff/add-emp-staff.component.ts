import { Component, OnDestroy, OnInit } from "@angular/core";
import { MainURL } from "../shared/configurl";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { RoleMasterService } from "../shared/services/RoleMasterService";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { ClmsGetApiService } from "../clms-get-api.service";

@Component({
  selector: "app-add-emp-staff",
  templateUrl: "./add-emp-staff.component.html",
  styleUrls: ["./add-emp-staff.component.css"],
})
export class AddEmpStaffComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  themeColor: string = "nav-pills-blue";
  user: string;
  users: any;
  token: any;
  getallDepartment: any;
  DesignationList: any;
  companydata: any;
  firstName: any;
  lastName: any;
  gender: any;
  roleId: any;
  selectUndefinedOptionValue: any;
  // empStatus:boolean=false;
  public empStatus: boolean = true;
  emailId: any;
  alldataofDropDown: any;
  IsEditeFlag: boolean = false;
  compId: any;
  deptId: any;
  mobileNo: any;
  dateOfBirth: any;
  EmployeeEmpId: any;
  employeemasterId: any;
  desigId: any;
  locationMastersLis: any;
  EmpTypeData: any;
  empCatId: any;
  locationId: any;
  empMasterId: string;
  allEmpDataId: any;
  allEmpDatas: any;
  employeeEdit: string;
  minDate: any;
  maxDate: any;
  employeeData: any;
  employeelistEdit: string;
  empmasterIds: any;
  searchByLabour: any;
  searchByColumn: any;
  buttonFlag:boolean=false
  constructor(
    private httpservice: HttpClient,
    private roleservice: RoleMasterService,
    private toastr: ToastrService,
    private router: Router,
    private getApiservice: ClmsGetApiService
  ) {
    this.user = sessionStorage.getItem("loggeduser");
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empMasterId = sessionStorage.getItem("empmasterId");

    // this.employeeEdit = sessionStorage.getItem("iseditLabour");
    // if (this.employeeEdit == "true") {
    //   this.getemployeeById();
    // }
    // Calculate the maximum and minimum allowed dates
    const currentDate = new Date();
    currentDate.setFullYear(currentDate.getFullYear() - 18);
    this.maxDate = currentDate.toISOString().split("T")[0];

    const minDate = new Date("1960-01-01");
    this.minDate = minDate.toISOString().split("T")[0];
  }

  ngOnInit() {
    this.getApiservice.setAuthToken(this.token);
    this.getDepartment();
    this.getDesignation();
    this.companyActive();
    this.getallRollesOfUser();
    this.getLabourData();
    this.getContractorLocation();
    this.getemployee();

    
  }
  ngOnDestroy() {
    sessionStorage.removeItem("empmasterId");
  }
  // ngOnDestroy(): void {
  //   this.empMasterId=''
  // }

  // dateOfBirth: Date;
  // age: number;

  validateDateOfBirth() {
    if (this.dateOfBirth < this.minDate || this.dateOfBirth > this.maxDate) {
      return {
        dateOfBirthInvalid: true,
      };
    }

    return null;
  }

  isValidMobileNumber: boolean = true;
  hasDuplicateDigits: boolean = false;

  checkInput(event: any) {
    const inputValue = event.target.value;
    this.isValidMobileNumber = this.validateMobileNumber(inputValue);
    this.hasDuplicateDigits = this.containsDuplicateDigits(inputValue, 8);
  }

  validateMobileNumber(inputValue: string): boolean {
    // Check if the input contains exactly 10 digits (0-9) without letters or special symbols
    return /^\d{10}$/.test(inputValue);
  }

  containsDuplicateDigits(
    inputValue: string,
    maxAllowedDuplicates: number
  ): boolean {
    const digitCounts = new Map<number, number>();

    for (let i = 0; i < inputValue.length; i++) {
      const digit = parseInt(inputValue[i]);

      if (digitCounts.has(digit)) {
        digitCounts.set(digit, digitCounts.get(digit) + 1);

        if (digitCounts.get(digit) > maxAllowedDuplicates) {
          return true; // More than the allowed number of duplicates
        }
      } else {
        digitCounts.set(digit, 1);
      }
    }

    return false;
  }

  // getLabourID() {

  //   let url = this.baseurl + '/getMaxEmployeeId';
  //   const tokens = this.token;
  //   const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.get(url, { headers }).subscribe(data => {
  //     console.log("shafds",data)
  // this.employeemasterId=data;
  // this.EmployeeEmpId=this.employeemasterId.result.EmployeeEmpId
  //   },
  //     (err: HttpErrorResponse) => {

  //     })

  // }
  getDepartment() {
    // let url = this.baseurl + '/getAllDepartments';
    // const tokens = this.token;
    // const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    // this.httpservice.get(url, { headers }).subscribe((data:any) => {

    //

    this.getApiservice.getAllDepartments().subscribe(
      (response: any) => {
        // Handle the successful response here

        this.getallDepartment = response["result"];
      },
      (error) => {
        // Handle errors here
        console.error("Error fetching departments:", error);
      }
    );
  }
  getDesignation() {
    this.getApiservice.getAllDesignations().subscribe(
      (response: any) => {
        // Handle the successful response here

        this.DesignationList = response["result"];
      },
      (error) => {
        // Handle errors here
        console.error("Error fetching Designation:", error);
      }
    );
  }

  companyActive() {
    this.getApiservice.getAllCompanyData().subscribe(
      (response: any) => {
        // Handle the successful response here

        this.companydata = response["result"];
      },
      (error) => {
        // Handle errors here
        console.error("Error fetching Comapany:", error);
      }
    );
  }
  getContractorLocation() {
    //   let url = this.baseurl + '/getAllLocations';
    // const tokens = this.token;
    // const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    //   this.httpservice.get(url,{headers}).subscribe((data:any) => {
    //     debugger
    //     this.locationMastersLis=data['result']

    // },
    //   (err: HttpErrorResponse) => {

    //   })
    this.getApiservice.getContractorLocationName().subscribe(
      (response: any) => {
        // Handle the successful response here

        this.locationMastersLis = response["result"];
      },
      (error) => {
        // Handle errors here
        console.error("Error fetching location:", error);
      }
    );
  }
  getemployee() {
    this.getApiservice.GetAllEmployees().subscribe(
      (response: any) => {
        // Handle the successful response here

        this.EmpTypeData = response["result"];
      },
      (error) => {
        // Handle errors here
        console.error("Error fetching employee:", error);
      }
    );
  }
  getRoleids(e: any) {
    this.roleId = e.target.value;
  }
  changevalue(event) {
    if (event.target.checked) {
        this.empStatus = true;
    } else {
        this.empStatus = false;
    }
    console.log(this.empStatus);
}
  resetForm() {
    // Set default or initial values for all form controls
    this.EmployeeEmpId = '';
    this.firstName = '';
    this.lastName = '';
    this.gender = '';
    this.empStatus = false;
    this.emailId = '';
    this.compId = ''; 
    this.deptId = ''; 
    this.mobileNo = '';
    this.dateOfBirth = null; 
    this.roleId = ''; 
    this.locationId = ''; 
    this.empCatId = ''; 
  }
  
  getallRollesOfUser() {
    this.roleservice.GetAllRoless(this.token).subscribe((data: any) => {
      this.alldataofDropDown = data.result;
    });
  }
  isedit: boolean = false;
  getemployeeById(e:any) {
   
    this.empMasterId=e.empMasterId
    this.IsEditeFlag = true;
    let url = this.baseurl + `/getEmployeeMasterById/${this.empMasterId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(

      (data: any) => {
        this.isedit = true;
        this.allEmpDataId = data;
        this.allEmpDatas = this.allEmpDataId.result;
        this.EmployeeEmpId = this.allEmpDatas.empId;
        this.firstName = this.allEmpDatas.firstName;
        this.lastName = this.allEmpDatas.lastName;
        this.dateOfBirth = this.allEmpDatas.dateOfBirth;
        this.gender = this.allEmpDatas.gender;
        this.deptId = this.allEmpDatas.departmentMaster.deptId;
        this.desigId = this.allEmpDatas.designationMaster.desigId;
        this.compId = this.allEmpDatas.companyMaster.compId;
        this.emailId = this.allEmpDatas.emailId;
        this.mobileNo = this.allEmpDatas.mobileNo;
        this.roleId = this.allEmpDatas.user.roles.roleId;
        this.locationId = this.allEmpDatas.locationMaster.locationId;

        this.empCatId = this.allEmpDatas.empCategoryMaster.empCatId;
        document.getElementById("extra-apply-tab").click();
      },
      (err: HttpErrorResponse) => {}
    );
  }

  onSubmitdata() {
    this.buttonFlag=false
      const tokens = this.token;
      const headers = new HttpHeaders().set(
        "Authorization",
        `Bearer ${tokens}`
      );
      let obj = {
        firstName: this.firstName,
        lastName: this.lastName,
        gender: this.gender,
        empStatus: this.empStatus,
        emailId: this.emailId,
        roleId: this.roleId,
        compId: this.compId,
        deptId: this.deptId,
        mobileNo: this.mobileNo,
        desigId: this.desigId,
        dateOfBirth: this.dateOfBirth,
        empId: this.EmployeeEmpId,
        empCatId: this.empCatId,
        locationId: this.locationId,
      };
      let url = this.baseurl + "/createEmployeeMaster";
      this.httpservice.post(url, obj, { headers }).subscribe(
        (data) => {
          this.toastr.success("Staff Employee Added Successfully");
          // this.router.navigateByUrl("layout/emloyeestaffList");
          document.getElementById("extra-list-tab").click();
          // this.IsEditeFlag=false
          this.getLabourData() 
            
        },

        (err: HttpErrorResponse) => {
          this.toastr.error("error while add staff Employee ");
        }
      );
    
  }
  onUpdate() {
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    let obj = {
      empMasterId: this.allEmpDatas.empMasterId,
      firstName: this.firstName,
      lastName: this.lastName,
      gender: this.gender,
      empStatus: this.empStatus,
      emailId: this.emailId,
      roleId: this.roleId,
      compId: this.compId,
      deptId: this.deptId,
      mobileNo: this.mobileNo,
      desigId: this.desigId,
      dateOfBirth: this.dateOfBirth,
      empId: this.EmployeeEmpId,
      empCatId: this.empCatId,
      locationId: this.locationId,
    };
    let url = this.baseurl + "/updateEmployeeMaster";
    this.httpservice.put(url, obj, { headers }).subscribe(
      (data) => {
        this.toastr.success("staff Employee Update Successfully");
      
        this.buttonFlag=false
        document.getElementById('extra-list-tab').click();
         this.getLabourData()
      },
      (err: HttpErrorResponse) => {
        this.toastr.error("error while Update staff Employee ");
      }
    );
  }

  //  list added code

  getLabourData() {
    let url = this.baseurl + "/getEmployeeMaster";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(
      (data) => {
        this.employeeData = data["result"];
        setTimeout(function () {

          $(function () {
  
            var table = $('#emptable').DataTable({
  
              retrieve: true,
             
              searching:true
            })
  
          });
        }, 100);

        // this.labourData=JSON.stringify(this.LabourData )
        // console.log("dsfasdfdsfsdfsdfsdafasdfsdaf",this.LabourData)
      },
      (err: HttpErrorResponse) => {}
    );
  }

  // editLabour(item: any) {
  //   debugger
  //   this.buttonFlag = true;
  //   this.employeelistEdit = "true";
  //   document.getElementById("extra-apply-tab").click();
  //   this.getLabourData()
    
  //   sessionStorage.setItem("empmasterId", item.empMasterId);
  //   sessionStorage.setItem("iseditLabour", "true");
  
  
    
  // }
  
  

  filterData() {
    if (this.searchByLabour && this.searchByColumn) {
      // Convert the search term to lowercase for case-insensitive search
      const searchLower = this.searchByColumn.toLowerCase();

      this.employeeData = this.employeeData.filter((item) => {
        if (this.searchByLabour === "Name" && item.firstName && item.lastName) {
          const fullName = `${item.firstName} ${item.lastName}`;
          if (fullName.toLowerCase().includes(searchLower)) {
            return true;
          }
        } else if (
          this.searchByLabour === "Contact Number" &&
          item.mobileNo &&
          item.mobileNo.includes(this.searchByColumn)
        ) {
          return true;
        } else if (
          this.searchByLabour === "Adhar Number" &&
          item.bioId &&
          item.bioId.toLowerCase() === searchLower
        ) {
          return true;
        } else if (
          this.searchByLabour === "Contractor Service" &&
          item.empCategoryMaster &&
          item.empCategoryMaster.empCatName.toLowerCase().includes(searchLower)
        ) {
          return true;
        } else if (
          this.searchByLabour === "Contractor Name" &&
          item.companyMaster &&
          item.companyMaster.compName.toLowerCase().includes(searchLower)
        ) {
          return true;
        }
        return false;
      });
    } else {
      this.getLabourData();
    }
  }
}
