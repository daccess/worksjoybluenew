import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmpStaffComponent } from './add-emp-staff.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: AddEmpStaffComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'addempStaff',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'addempStaff',
                  component: AddEmpStaffComponent
              }

          ]

  }


]

export const staffRouting = RouterModule.forChild(appRoutes);