import { Component, OnInit } from '@angular/core';
import { ClearenceService } from '../shared/services/clearenceservice';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-clearancepage',
  templateUrl: './clearancepage.component.html',
  styleUrls: ['./clearancepage.component.css']
})
export class ClearancepageComponent implements OnInit {
  loggedUser: any;
  clearenceList = [];
  holdData: any = {}
  remark: any;
  public long: string;
  constructor(private service: ClearenceService, private toastr: ToastrService, public Spinner: NgxSpinnerService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
  }

  ngOnInit() {
    this.getClearenceList();
  }
  dataTableFlag : boolean = false; 
  getClearenceList() {
    let obj = {
      "empOfficialId": this.loggedUser.empOfficialId
    }
    this.dataTableFlag = true;
    this.service.getClearenceListNew(this.loggedUser.empOfficialId).subscribe(data => {
      this.clearenceList = data.result;
      this.dataTableFlag = true;
       setTimeout(function () {
        $(function () {
          var table = $('#emptable').DataTable();
        });
      }, 100);
    
    }, err => {
    })
  }
  
  clear(item) {
    let obj = {
      sepClrId: item.sepClrId
    }
    this.Spinner.show();
    this.service.clearApprove(obj).subscribe(data => {
      this.clearenceList = [];
      this.getClearenceList();
      this.Spinner.hide();
      this.toastr.success('Clearance Done Successfully..!', 'Resignation');
    }, (err) => {
      this.Spinner.hide();
      this.toastr.error('Error While Clearance..!', 'Resignation');
    })
  }

  hold(item) {
    let obj = {
      sepClrId: item.sepClrId,
      clrReason: this.remark
    }
    this.Spinner.show()
    this.service.holdClearance(obj).subscribe(data => {
      this.clearenceList = [];
      this.Spinner.hide()
      this.toastr.success('Clearance on Hold..!', 'Resignation');
      this.getClearenceList();
    }, (err) => {
      this.Spinner.hide();
      this.toastr.error('Error While Clearance Hold..!', 'Resignation');
    })
  }
  setData(item) {
    this.holdData = item
  }
}


