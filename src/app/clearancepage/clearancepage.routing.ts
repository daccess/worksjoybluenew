import { Routes, RouterModule } from '@angular/router'

import { ClearancepageComponent } from './clearancepage.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ClearancepageComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'clearancepage',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'clearancepage',
                    component: ClearancepageComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);