import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearancepageComponent } from './clearancepage.component';

describe('ClearancepageComponent', () => {
  let component: ClearancepageComponent;
  let fixture: ComponentFixture<ClearancepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearancepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearancepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
