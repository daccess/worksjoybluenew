import { routing } from './clearancepage.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ClearancepageComponent } from './clearancepage.component';
import { ClearenceService } from '../shared/services/clearenceservice';


@NgModule({
    declarations: [
        ClearancepageComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule
    ],
    providers: [ClearenceService]
  })
  export class ClearancepageModule { 
      constructor(){

      }
  }
  