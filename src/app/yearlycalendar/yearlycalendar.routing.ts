import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { YearlycalendarComponent } from 'src/app/yearlycalendar/yearlycalendar.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: YearlycalendarComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'yearlycalendar',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'yearlycalendar',
                    component: YearlycalendarComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);