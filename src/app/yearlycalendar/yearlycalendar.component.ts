import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { YearlycalendarService } from './shared/service/yearlycalendar.service';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LayOutService } from '../shared/model/layOutService';
import { CalendarService } from '../shared/services/calendar.service';
@Component({
  selector: 'app-yearlycalendar',
  templateUrl: './yearlycalendar.component.html',
  styleUrls: ['./yearlycalendar.component.css']
})
export class YearlycalendarComponent implements OnInit {


  yearlyCalender: string;

  top: 400;
  left: 500;
  currentDay: any;
  currentMonth: any;
  currentYear: any;
  date: any;
  januaryDays: any = [];
  februaryDays: any = [];
  marchDays: any = [];
  aprilDays: any = [];
  mayDays: any = [];
  juneDays: any = [];
  julyDays: any = [];
  augustDays: any = [];
  septemberDays: any = [];
  octoberDays: any = [];
  novemberDays: any = [];
  decemberDays: any = [];
  contextFlag: boolean = false;
  selectedDate: string = "";
  contextFlag1: boolean = false;
  monthNames = [];
  attendanceData: any = [];
  empOfficialId: any;
  loggedUser: any;
  compId: any;
  empPerId: any;
  baseurl = MainURL.HostUrl;
  HolidayData = [];
  shiftName: any;

  januaryDaysShift: any = [];
  februaryDaysShift: any = [];
  marchDaysShift: any = [];
  aprilDaysShift: any = [];
  mayDaysShift: any = [];
  juneDaysShift: any = [];
  julyDaysShift: any = [];
  augustDaysShift: any = [];
  septemberDaysShift: any = [];
  octoberDaysShift: any = [];
  novemberDaysShift: any = [];
  decemberDaysShift: any = [];
  roleName: any;


  constructor(private layOutService : LayOutService,private router: Router, private service: YearlycalendarService, private http: HttpClient,private calendarService : CalendarService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.empPerId = this.loggedUser.empPerId
    this.roleName = this.loggedUser.mrollMaster.roleName
    this.date = new Date();
    this.currentDay = this.date.getDate();
    this.currentMonth = this.date.getMonth() + 1;
    this.currentYear = this.date.getFullYear();
    this.Holidays();
    this.getAttendanceOfYear(this.currentYear);
    this.monthNames = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    this.getAllDays();
    this.getAllDaysShift();

  }
  getAttendanceOfYear(year) {
    let body = {
      "empOfficialId": this.empOfficialId,
      "year": year
    }
    this.attendanceData = [];
    this.service.yearlyAttendance(body).subscribe(data => {
      this.attendanceData = data.result;
      for (let j = 0; j < this.attendanceData.length; j++) {
        if(!this.attendanceData[j].checkIn){
          this.attendanceData[j].checkIn = new Date(this.attendanceData[j].checkIn).setHours(0,0,0);
        }
        if(!this.attendanceData[j].checkOut){
          this.attendanceData[j].checkOut = new Date(this.attendanceData[j].checkOut).setHours(0,0,0);
        }
      }
      this.getAllDays();
    }, (err => {
      this.getAllDays();
    }))
  }




  ngOnInit() {
    var d = new Date();
    var getTot = this.getLastDayOfMonth(d.getFullYear(), d.getMonth()); //Get total days in a month
    var sat = new Array();   //Declaring array for inserting Saturdays
    var sun = new Array();   //Declaring array for inserting Sundays
    var getTot = this.getLastDayOfMonthShift(d.getFullYear(), d.getMonth()); //Get total days in a month

    for (var i = 1; i <= getTot; i++) {    //looping through days in month
      var newDate = new Date(d.getFullYear(), d.getMonth(), i)
      if (newDate.getDay() == 0) {   //if Sunday
        sun.push(i);
      }
      if (newDate.getDay() == 6) {   //if Saturday
        sat.push(i);
      }
    }
  }


  // ================YearlyCalender================
flagSerch:boolean;
  isShow: boolean = true;
  yearlyCalenderData(event) {
    if (event == 'yearCalnder') {
      this.isShow = true;
      this.getAttendanceOfYear(this.currentYear);
    } else if (event == 'ShiftyearCalnder') {
      this.isShow = false;
      this.getAttendanceOfYearShift(this.currentYear);
    }
  }
  //======================holidays======================
  Holidays() {
    let url = this.baseurl + '/Holiday?empOfficialId='+this.loggedUser.empOfficialId+'&year='+new Date().getFullYear();
    this.service.getHolidays(url).subscribe(data => {
      this.HolidayData = data.result;
    })
  }
  //======================holidays======================
  increaseYear() {
    this.currentYear++;
    this.getAttendanceOfYear(this.currentYear);
    this.getAttendanceOfYearShift(this.currentYear)
  }
  decreaseYear() {
    this.currentYear--;
    this.getAttendanceOfYear(this.currentYear);
    this.getAttendanceOfYearShift(this.currentYear)
  }
  // =================NEW SHIFT CALENDAR STARTS=========================

  shiftAttendeceDate: any = [];
  getAttendanceOfYearShift(year) {
    this.shiftAttendeceDate = [];
    let url = this.baseurl + '/empRoaster/?empOfficialId=' + this.empOfficialId + '&year=' + year;
    this.shiftAttendeceDate = [];
    this.http.get(url).subscribe(data => {
      this.shiftAttendeceDate = data['result'];
      this.getAllDaysShift();
    },(err => {
      this.shiftAttendeceDate = [];
      this.getAllDaysShift();
    }))
  }

  // =================NEW SHIFT CALENDAR ENDS=========================

  getAllDays() {
    this.januaryDays = [];
    this.februaryDays = [];
    this.marchDays = [];
    this.aprilDays = [];
    this.mayDays = [];
    this.juneDays = [];
    this.julyDays = [];
    this.augustDays = [];
    this.septemberDays = [];
    this.octoberDays = [];
    this.novemberDays = [];
    this.decemberDays = [];

    let serivceJanuaryDays =  this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 1) , 1 , this.attendanceData , this.HolidayData);
    this.januaryDays = serivceJanuaryDays.daysInThisMonth;
    
    let serivceFebruaryDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 2) , 2 , this.attendanceData , this.HolidayData);
    this.februaryDays = serivceFebruaryDays.daysInThisMonth;
   
    let serivceMarchDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 3) , 3 , this.attendanceData , this.HolidayData);
    this.marchDays = serivceMarchDays.daysInThisMonth;

    let serivceAprilDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 4) , 4 , this.attendanceData , this.HolidayData);
    this.aprilDays = serivceAprilDays.daysInThisMonth;

    let serivceMayDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 5) , 5 , this.attendanceData , this.HolidayData);
    this.mayDays = serivceMayDays.daysInThisMonth;

    let serivceJuneDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 6) , 6 , this.attendanceData , this.HolidayData);
    this.juneDays = serivceJuneDays.daysInThisMonth;

    let serivceJulyDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 7) , 7 , this.attendanceData , this.HolidayData);
    this.julyDays = serivceJulyDays.daysInThisMonth;

    let serivceAugustDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 8) , 8 , this.attendanceData , this.HolidayData);
    this.augustDays = serivceAugustDays.daysInThisMonth;

    let serivceSeptemberDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 9) , 9 , this.attendanceData , this.HolidayData);
    this.septemberDays = serivceSeptemberDays.daysInThisMonth;

    let serivceOctoberDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 10) , 10 , this.attendanceData , this.HolidayData);
    this.octoberDays = serivceOctoberDays.daysInThisMonth;

    let serivceNovemberDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 11) , 11 , this.attendanceData , this.HolidayData);
    this.novemberDays = serivceNovemberDays.daysInThisMonth;

    let serivceDecemberDays = this.calendarService.getYearlyCalendarAttendance(this.getLastDayOfMonth(this.currentYear, 12) , 12 , this.attendanceData , this.HolidayData);
    this.decemberDays = serivceDecemberDays.daysInThisMonth;
  }


  // =============================Shift  Yealry calender=============

  getAllDaysShift() {
    this.januaryDaysShift = [];
    this.februaryDaysShift = [];
    this.marchDaysShift = [];
    this.aprilDaysShift = [];
    this.mayDaysShift = [];
    this.juneDaysShift = [];
    this.julyDaysShift = [];
    this.augustDaysShift = [];
    this.septemberDaysShift = [];
    this.octoberDaysShift = [];
    this.novemberDaysShift = [];
    this.decemberDaysShift = [];

    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 1); i++) {


      let item;
      item = {
        day: i
      }
      let flag = false;

      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 1) {
          flag = true;

          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }

          this.januaryDaysShift.push(item)

        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.januaryDaysShift.push(hObj);
      }
    }



    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 2); i++) {


      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 2) {
          flag = true;
          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.februaryDaysShift.push(item);
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.februaryDaysShift.push(hObj);
      }
    }
    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 3); i++) {
      let item;
      item = {
        day: i
      }
      let flag = false;

      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 3) {
          flag = true;
          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.marchDaysShift.push(item);
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.marchDaysShift.push(hObj);
      }
    }
    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 4); i++) {
      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 4) {
          flag = true;
          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.aprilDaysShift.push(item);
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.aprilDaysShift.push(hObj);
      }
    }
    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 5); i++) {
      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 5) {
          flag = true;
          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.mayDaysShift.push(item);
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.mayDaysShift.push(hObj);
      }
    }
    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 6); i++) {
      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 6) {
          flag = true;


          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.juneDaysShift.push(item)
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.juneDaysShift.push(hObj);
      }
    }

    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 7); i++) {
      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 7) {
          flag = true;


          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }


          this.julyDaysShift.push(item)


        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.julyDaysShift.push(hObj);
      }
    }



    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 8); i++) {


      let item;
      item = {
        day: i
      }
      let flag = false;




      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 8) {
          flag = true;


          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }


          this.augustDaysShift.push(item)


        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.augustDaysShift.push(hObj);
      }
    }




    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 9); i++) {


      let item;
      item = {
        day: i
      }
      let flag = false;




      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 9) {
          flag = true;


          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }


          this.septemberDaysShift.push(item)


        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.septemberDaysShift.push(hObj);
      }
    }



    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 10); i++) {


      let item;
      item = {
        day: i
      }
      let flag = false;




      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 10) {
          flag = true;


          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.octoberDaysShift.push(item)
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.octoberDaysShift.push(hObj);
      }
    }



    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 11); i++) {
      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 11) {
          flag = true;


          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }


          this.novemberDaysShift.push(item)

        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.novemberDaysShift.push(hObj);
      }
    }


    for (let i = 1; i <= this.getLastDayOfMonthShift(this.currentYear, 12); i++) {


      let item;
      item = {
        day: i
      }
      let flag = false;
      for (let j = 0; j < this.shiftAttendeceDate.length; j++) {

        if (new Date(this.shiftAttendeceDate[j].roasterDate).getDate() == i && new Date(this.shiftAttendeceDate[j].roasterDate).getMonth() + 1 == 12) {
          flag = true;
          item = {
            day: i,
            shiftName: this.shiftAttendeceDate[j].shiftName,
            shiftCode: this.shiftAttendeceDate[j].shiftCode,
            date: this.shiftAttendeceDate[j].roasterDate,
            shiftColor: this.shiftAttendeceDate[j].shiftColor,
            status: "yes"
          }
          this.decemberDaysShift.push(item);
        }
      }

      if (!flag) {
        let hObj = {
          day: i,
          status: "no"
        }
        this.decemberDaysShift.push(hObj);
      }
    }
  }


  getLastDayOfMonthShift(year, month) {
    return new Date(year, month, 0).getDate();
  }



  getLastDayOfMonth(year, month) {
    return new Date(year, month, 0).getDate();
  }
  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ":" + rminutes;
  }
  selectedDay: string = "";
  selectedMonth: string = "";
  requestDate: string = "";
  getSelected(day1, month1) {
    this.selectedDay = day1;
    this.selectedMonth = month1;
    this.selectedDate = this.selectedDay + " " + this.monthNames[this.selectedMonth] + " " + this.currentYear;
    this.requestDate = this.currentYear + "-" + this.selectedMonth + "-" + this.selectedDay;
  }
  rightClick(e1, month1, day1, attenDate) {
    this.getSelected(day1, month1);
    this.contextFlag1 = !this.contextFlag1;
    if (!this.contextFlag) {
      this.contextFlag = true;
      var top = e1.clientX - 80;
      var left = e1.clientY + 90;
      $("#context-menu").css({
        display: "block",
        top: top,
        left: left
      })
    }

    $('.table').on('contextmenu', function (e) {
      var top = e.pageY + 120;
      var left = e.pageX - 90;
      $("#context-menu").css({
        display: "block",
        top: top,
        left: left
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu
    }).on("click", function () {
      $("#context-menu").removeClass("show").hide();
    });

    $("#context-menu a").on("click", function () {
      $(this).parent().removeClass("show").hide();
    });

    return false;
  }
  applyLeave(val) {
    if (val == 'LR') {

      sessionStorage.setItem('requestedDate', this.requestDate);
      sessionStorage.setItem('from', 'yearly');
      this.router.navigateByUrl('layout/leaverequest/leaverequest');
    }
    else {
      sessionStorage.setItem('from', 'yearly');
      sessionStorage.setItem('requestedDate', this.requestDate);
      sessionStorage.setItem('requestedType', val);
      this.router.navigateByUrl('/layout/otherrequests/otherrequests');

    }

  }
  empSearchSuggestionList = [];
  searchFlag : boolean = false;
  getSearchSuggestionList(text){
    this.searchFlag = true;
    if(!text){
      this.empSearchSuggestionList = [];
    }
    if(text){
      this.layOutService.getSearchSuggestionList(text,this.loggedUser.locationId).subscribe(data=>{
        this.empSearchSuggestionList = data.result;
      },err=>{
        this.empSearchSuggestionList = [];
      })
     
   }
  }


  employeeData = [];
  searchText : string = "";
  suggestThisEmployee(item){
      this.loggedUser = item;
      this.searchText = item.fullName+" "+item.lastName;
      this.empSearchSuggestionList = [];
      this.empOfficialId = item.empOfficialId;
     this.getAttendanceOfYear(this.currentYear);
     this.getAttendanceOfYearShift(this.currentYear);
  }
}