import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearlycalendarComponent } from './yearlycalendar.component';

describe('YearlycalendarComponent', () => {
  let component: YearlycalendarComponent;
  let fixture: ComponentFixture<YearlycalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearlycalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearlycalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
