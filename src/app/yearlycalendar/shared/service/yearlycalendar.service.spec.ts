import { TestBed, inject } from '@angular/core/testing';

import { YearlycalendarService } from './yearlycalendar.service';

describe('YearlycalendarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YearlycalendarService]
    });
  });

  it('should be created', inject([YearlycalendarService], (service: YearlycalendarService) => {
    expect(service).toBeTruthy();
  }));
});
