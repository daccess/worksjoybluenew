import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from 'src/app/shared/configurl';
@Injectable({
  providedIn: 'root'
})
export class YearlycalendarService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { 

  }

  yearlyAttendance(bodycontent){
    let url = this.baseurl + '/callenderdata';
    var body = JSON.stringify(bodycontent);
    //console.log("body",body);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  yearlyAttendanceshifts(bodycontent){
    let url = this.baseurl + '/getroasterDataById';
    var body = JSON.stringify(bodycontent);
   // console.log("body",body);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getHolidays(url){
    // let url = this.baseurl + '/HoliDays/'+id;
    return this.http.get(url).map(x => x.json());
  }

  
}
