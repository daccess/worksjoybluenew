import { routing } from './yearlycalendar.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { YearlycalendarComponent } from './yearlycalendar.component'
import { YearlycalendarService } from './shared/service/yearlycalendar.service';
import {ShContextMenuModule} from 'context-menu-angular6'

@NgModule({
    declarations: [
        YearlycalendarComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     ShContextMenuModule
    ],
    providers: [YearlycalendarService]
  })
  export class YearlycalendarModule { 
      constructor(){

      }
  }
  