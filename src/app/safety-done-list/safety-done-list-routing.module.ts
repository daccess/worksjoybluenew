import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SafetyDoneListComponent } from './safety-done-list.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: SafetyDoneListComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'safetydoneList',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'safetydoneList',
                  component: SafetyDoneListComponent
              }

          ]

  }


]

export const safetyDoneListRouting = RouterModule.forChild(appRoutes);
