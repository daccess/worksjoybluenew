import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-safety-done-list',
  templateUrl: './safety-done-list.component.html',
  styleUrls: ['./safety-done-list.component.css']
})
export class SafetyDoneListComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  safetyLabourDatas: any;
  labourId: any;
  manReqStatusIde: any;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
   }

  ngOnInit() {
    this.getSafetyEmployee();
  }
  getSafetyEmployee(){
    let url = this.baseurl + `/getSafetyLabourdata/${this.empids}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.safetyLabourDatas = data["result"];
      // this.locationId = this.locationdata[0].locationId
    

    },
      (err: HttpErrorResponse) => {

      })

  }
  gotToAssetss(e:any){
    
    // this.documentids=e.requestStatus.labourInfo.labourPerId;
    // this.manReqStatusIds=e.requestStatus.manReqStatusId
    // sessionStorage.setItem("documentIds",this.documentids)
    // sessionStorage.setItem("manReqStatusId",this.manReqStatusIds)
    this.labourId=e.requestStatus.labourInfo.labourPerId;
    this.manReqStatusIde=e.requestStatus.manReqStatusId;
    sessionStorage.setItem("manrequestIds",this.manReqStatusIde)
    sessionStorage.setItem("LabourPerIds",this.labourId)
    sessionStorage.setItem('IseditFlag', 'true')
    this.router.navigate(['/layout/frontDeskAssets'])
    
  }
}
