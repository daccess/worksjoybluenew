import { SafetyDoneListModule } from './safety-done-list.module';

describe('SafetyDoneListModule', () => {
  let safetyDoneListModule: SafetyDoneListModule;

  beforeEach(() => {
    safetyDoneListModule = new SafetyDoneListModule();
  });

  it('should create an instance', () => {
    expect(safetyDoneListModule).toBeTruthy();
  });
});
