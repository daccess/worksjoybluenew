import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {safetyDoneListRouting } from './safety-done-list-routing.module';
import { SafetyDoneListComponent } from './safety-done-list.component';

@NgModule({
  imports: [
    CommonModule,
    safetyDoneListRouting
  ],
  declarations: [
    SafetyDoneListComponent
  ]
})
export class SafetyDoneListModule { }
