import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyDoneListComponent } from './safety-done-list.component';

describe('SafetyDoneListComponent', () => {
  let component: SafetyDoneListComponent;
  let fixture: ComponentFixture<SafetyDoneListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyDoneListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyDoneListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
