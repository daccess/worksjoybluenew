import { Routes, RouterModule } from '@angular/router'
import { TeammonthlycalendarComponent } from './teammonthlycalendar.component';



const appRoutes: Routes = [
    { 
             
        path: '', component: TeammonthlycalendarComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'teammonthlycalendar',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'teammonthlycalendar',
                    component: TeammonthlycalendarComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);