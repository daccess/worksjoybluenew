import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeammonthlycalendarComponent } from './teammonthlycalendar.component';

describe('TeammonthlycalendarComponent', () => {
  let component: TeammonthlycalendarComponent;
  let fixture: ComponentFixture<TeammonthlycalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeammonthlycalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeammonthlycalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
