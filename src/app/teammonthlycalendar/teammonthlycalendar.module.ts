import { routing } from './teammonthlycalendar.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import{CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { TeammonthlycalendarComponent } from './teammonthlycalendar.component';
import { TooltipModule } from 'ng2-tooltip-directive';


@NgModule({
    declarations: [
        TeammonthlycalendarComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    TooltipModule 
    ],
  
    providers: [],
    
  })
  export class TeammonthlycalendarModule { 
      constructor(){

      }
  }
  