import { Routes, RouterModule } from '@angular/router'

import { EmphistoryComponent } from './emphistory.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: EmphistoryComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'emphistory',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'emphistory',
                    component: EmphistoryComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);