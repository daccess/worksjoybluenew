import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../shared/configurl';
@Component({
  selector: 'app-emphistory',
  templateUrl: './emphistory.component.html',
  styleUrls: ['./emphistory.component.css','./emphistory.component.scss']
})
export class EmphistoryComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  loggedUser: any;
  empHistoryData = [];
  constructor(public httpService: HttpClient) { }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getEmpHistorty();
  }
  
  getEmpHistorty(){
    let url = this.baseurl + '/employeehistory/';
    this.httpService.get(url + this.loggedUser.empOfficialId).subscribe((data : any) => {
        this.empHistoryData = data.result;
      },
      (err: HttpErrorResponse) => {
      });
  }
}
