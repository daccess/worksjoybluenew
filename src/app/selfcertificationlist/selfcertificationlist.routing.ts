import { Routes, RouterModule } from '@angular/router'


import { SelfcertificationlistComponent } from './selfcertificationlist.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: SelfcertificationlistComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'selfcertificationlist',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'selfcertificationlist',
                    component: SelfcertificationlistComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);