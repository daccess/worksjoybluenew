import { Component, OnInit } from '@angular/core';
import { selfcertificationservice } from './services/selfcertification.service'
import { from } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from './../shared/configurl';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { selfCertification } from './models/selfCertificationModel';
import { selfcertificationservicestatus } from './services/selfcertificationstaus.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { log } from 'util'; 
@Component({
  selector: 'app-selfcertificationlist',
  templateUrl: './selfcertificationlist.component.html',
  styleUrls: ['./selfcertificationlist.component.css'],
  providers: [selfcertificationservice,selfcertificationservicestatus]
})

export class SelfcertificationlistComponent implements OnInit {
  loggedUser: any;
  compId: any;
  empOfficialId: any;
  maxDate: any;
  mindDate: any;
  fromDate: any;
  toDate: any;

  date: Date = new Date();
  monthNames: any;
  currentMonth: any;
  currentYear: any;

  selfCertification: boolean = true;
  selfCertificationPending: boolean = false;
  selfCertificationData: any = [];

  baseurl = MainURL.HostUrl;
  alldevice: any;
  payrollMonth: any;
  dataTableFlag: boolean = true
  empPerId: any;
  payRollMonth: any;
  payrollM: any;
  check: any;

  selCertiPendingData: any =[];
  monthStartDate: any;
  monthEndDate: any;

  selfModel: selfCertification;
  checkOpen = false;
  checkAll: any;
  selfCertificationStatus: any;
  Employeedataall: any;
  selectedEmpTypeObj: any;
  empTypeId: any;
  temFlag: boolean;
  lastDateMonth: Date;
  startDateMonth: Date;
  errorMessage: any;
  toastr: any;
  curY: string;
  curM: any;
  emtype : any
  constructor(public selfcertserve: selfcertificationservice,
    public httpService: HttpClient,
    public chRef: ChangeDetectorRef,
    public Spinner: NgxSpinnerService,
    public selfCertStatus: selfcertificationservicestatus) {

      this.selfModel = new selfCertification();

    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();

    var currentMo = this.date.getMonth()+1;
    
    this.curM  = currentMo
    console.log("aaaaaaaaaaaaaaaaaaaaaaaaa",this.curM);
   
    this.Hflag = true
this.emtype = ''
    //this.payrollMonth = this.currentMonth+'-'+this.currentYear;

    var date = new Date();
    var lastDateMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var startDateMonth = new Date(new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + 1);
    this.getMonthDays(startDateMonth, lastDateMonth);

    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    console.log("sessionnnnn data", this.loggedUser);
    this.compId = this.loggedUser.compId;
    this.empPerId = this.loggedUser.empPerId;
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.getemployeeType();

    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    this.payrollM = m+'-'+y;



    this.getSelfcertificationStatus();
this.temFlag = true

    }

  ngOnInit() {
    this.getMonthLockData();
  
    // this.getalldevice();
    // this.getSelfCertificationPendingData();
  }


  


  getMonthLockData(){
    this.selfcertserve.getMonthLockData(this.empPerId,this.payrollM).subscribe(data => {
      console.log("getselfCertification Month Lock Data", data);
if(data != null){
      this.selfCertificationStatus = data.result;
      this.monthStartDate = data.result.startDate;
      this.monthEndDate = data.result.endDate;
      this.payRollMonth = data.result.payRollMonth;
   
}
      this.getSelfCertication();
      this.selfcertificationPayPeriodMonth();
     
    }, err => {
      console.log(err);

    });
  }

  getSelfCertication() {
    this.dataTableFlag = false;
    this.selfcertserve.getSelfCertification(this.compId,this.empTypeId,this.payRollMonth).subscribe(data => {
      console.log("getselfCertificationData", data);
      this.selfCertificationData = data.result.selfcertificationList;
      console.log(this.selfCertificationData);

      this.dataTableFlag = true
      setTimeout(function () {
        $(function () {
          var table = $('#CompTable').DataTable();

        });
      }, 1000);
      this.chRef.detectChanges();

    }, err => {
      console.log(err);

    });
  }
  Hflag : boolean = false
  goToLastMonth() {
  
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    // this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
    this.lastDateMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
    this.startDateMonth = new Date(this.date.getFullYear() + "/" + (this.date.getMonth() + 1) + "/" + 1);
   // this.currentDateMonth = new Date(this.date.getFullYear() + "/" + (this.date.getMonth() + 1) + "/" + 1);
     this.getMonthDays(this.startDateMonth, this.lastDateMonth);
     this.getSelfcertificationStatus()
    this.selfcertificationPayPeriodMonth();
      this.selectedMonthYear();
    
  }

  flag:string
  
  functionDatedisblenextMonth(){
 if(this.flag=="true"){
  this.goToNextMonth();

    }
  }
   
  goToNextMonth() {
  
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    var lastDateMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
    console.log(lastDateMonth,"next month");  
    var startDateMonth = new Date(this.date.getFullYear() + "/" + (this.date.getMonth() + 1) + "/" + 1);
     this.getMonthDays(startDateMonth, lastDateMonth);
    this.getSelfcertificationStatus()
    this.selfcertificationPayPeriodMonth();
     this.selectedMonthYear(); 
  }

  getMonthDays(start, end) {
    console.log("datess", start + ' ' + end)
    this.fromDate = new Date(start).getDate() + "/" + (new Date(start).getMonth() + 1) + "/" + new Date(start).getFullYear();
    this.toDate = new Date(end).getDate() + "/" + (new Date(end).getMonth() + 1) + "/" + new Date(end).getFullYear();
    console.log("datess2", this.fromDate + ' ' + this.toDate);
    this.monthStartDate =  new Date(start).getFullYear()+"-" + (new Date(start).getMonth() + 1)+"-"+new Date(start).getDate();
    this.monthEndDate =  new Date(end).getFullYear()+"-" + (new Date(end).getMonth() + 1)+"-"+new Date(end).getDate();
    console.log("vinod datess2", this.monthStartDate + ' ' + this.monthEndDate);
  }

  selectedMonthYear() {
    this.currentMonth = this.monthNames[this.date.getMonth()];
    var currentM = this.date.getMonth()+1;
    this.currentYear = this.date.getFullYear();
    this.payRollMonth = currentM +'-'+this.currentYear;
    console.log(this.payRollMonth)
    this.payrollM = this.payRollMonth;
    console.log(this.curM); 
    if( currentM  == this.curM){
      this.Hflag = true
      console.log(this.Hflag);
    }
    else{
      this.Hflag = false
      console.log(this.Hflag);
    }
    this.getSelfcertificationData();
    this.getMonthLockData();
    // this.getSelfCertificationPendingData();
  }

  getSelfLpayperiod :any =[];
   selfcertificationPayPeriodMonth(){
  
    this.selfcertserve.getSelfCertificationPayPeriod(this.empTypeId, this.payrollM ).subscribe(data =>{
      console.log("ssss  sandip", data);
      if(data != null)
      {
        this.getSelfLpayperiod = data.result;
        //  this.getMonthDays();
         console.log(this.getSelfLpayperiod,"sant");
         this.monthStartDate = data.result.startDate
         this.monthEndDate  = data.result.endDate
      }
   
    }, err => {
      console.log(err);
    });

  }



  getSelfcertificationData() {
    this.checkOpen = false;
    this.dataTableFlag = false;
    // alert("data");
    this.selfcertserve.getSelfCertification(this.compId,this.empTypeId,this.payRollMonth).subscribe(data => {
      console.log("getselfCertificationData", data);
      this.selfCertificationData = data.result.selfcertificationList;
      console.log(this.selfCertificationData);

      this.dataTableFlag = true
      setTimeout(function () {
        $(function () {
          var table = $('#CompTable').DataTable();

        });
      }, 1000);
      this.chRef.detectChanges();

    }, err => {
      console.log(err);
    });
  }
  getSelfcertificationStatus() {
    // alert("vinod")
    
    this.selfCertStatus.getSelfCertificationstatus(this.empTypeId,this.monthEndDate,this.monthStartDate).subscribe((data: any)=> {
      if (data) {
        this.selfCertificationStatus = data.result;
        console.log("getselfCertificationData status",JSON.parse(data._body));
        this.selfCertificationStatus =JSON.parse (data. _body).result
        console.log("Self status", this.selfCertificationStatus );
      }
      // this.selfCertificationStatus = true;    
    }, (err : any)=> {
      console.log("getselfCertificationData Erorrr",JSON.parse(err. _body));
      this.selfCertificationStatus =JSON.parse (err. _body).result
      console.log("status" ,this.selfCertificationStatus );
      
    });
  }
  
  getSelfCertificationPendingData() {
    // alert("data");
    this.checkOpen = true;
    this.dataTableFlag = false
    this.selfcertserve.getSelfCertificationPending(this.empTypeId,this.payRollMonth).subscribe(data => {
      console.log("getselfCertificationPendingData", data.result);

      // this.selfCertificationData = data.result;
      this.selfCertificationData = data.result.selfcertificationList;
      console.log(this.selfCertificationData);
      console.log(data.result.count);

      var myArrayNew = this.selfCertificationData.filter(function (el) {
        return el != null && el != "";
      });

      console.log(myArrayNew);
      this.selfCertificationData = myArrayNew;

      this.dataTableFlag = true
      setTimeout(function () {
        $(function () {
          var table = $('#CompTable').DataTable();

        });
      }, 1000);
      this.chRef.detectChanges();
      this.getSelfcertificationStatus() 
      // this.getSelfcertificationStatus();

    }, err => {
      console.log(err);
    });
  }

  postSelfCrtifiedData(){
this.Spinner.show()
    for (let i = 0; i < this.selfCertificationData.length; i++) {
        if(this.selfCertificationData[i].check){
          console.log(this.selfCertificationData[i].empOfficialId);
          let obj = {
            empOfficialId : this.selfCertificationData[i].empOfficialId,
            compId : this.compId,
            fromDate: this.monthStartDate,
            toDate: this.monthEndDate,
            payrollMonth: this.payRollMonth,
            selfcertificationStatus: true
          }
          this.selCertiPendingData.push(obj);
        }
    }
    console.log(this.selCertiPendingData);
    this.selfModel.selfCertificationReqDtos = this.selCertiPendingData;
   
    console.log(this.selfModel);

    this.selfcertserve.postSelfCertificationData(this.selfModel).subscribe(data => {
      console.log("Post Self Certification data", data);
      this.toastr.seccuss("Post Self Certification data");
this.Spinner.hide()
    }, err => {
      console.log(err);
      this.Spinner.hide()
    });
  }

  selfCertificationReqDtos:any
  payStart:any
  payEnd:any;
  checkAllData(event){
  console.log(event);
  console.log(this.checkAll)
  
    console.log(this.checkAll);
    if (this.checkAll) {
      // this.leaveChecked = false;
      for (let i = 0; i < this.selfCertificationData.length; i++) {
        this.selfCertificationData[i].check = true;
        // this.approveChecks = true;
      }
    }
    else {
      for (let i = 0; i < this.selfCertificationData.length; i++) {
        this.selfCertificationData[i].check = false;
        // this.approveChecks = false;
      }
    }
}


getemployeeType() {
  let testempactive = this.baseurl + '/selfcertification/employeeType';
  this.httpService.get(testempactive).subscribe(
    data => {
      console.log("Emp Res", data)
      this.Employeedataall = data["result"];
      console.log("parent emp ID vinod")
      console.log(this.Employeedataall);

    },
    (err: HttpErrorResponse) => {
      console.log(err.message);
    });
}

EmpValue(event){
  console.log("Emp",event)
  console.log("Event value",event.target.value)
   this.selectedEmpTypeObj = parseInt(event.target.value);
   console.log(this.selectedEmpTypeObj);
   this.empTypeId = this.selectedEmpTypeObj;
   this.getSelfCertication();
   this.getMonthLockData();
   this.getSelfCertificationPendingData(); 
 } 
 

}
