import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfcertificationlistComponent } from './selfcertificationlist.component';

describe('SelfcertificationlistComponent', () => {
  let component: SelfcertificationlistComponent;
  let fixture: ComponentFixture<SelfcertificationlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfcertificationlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfcertificationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
