import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../shared/configurl';
import { from } from 'rxjs';

@Injectable()
export class selfcertificationservicestatus {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  getSelfCertificationstatus(empTypeId : any,lastDateMonth : any, startDateMonth : any){
    let url = this.baseurl+'/selfcertification/selfcertificationStatus?empTypeId='+empTypeId+'&endDate='+lastDateMonth +'&startDate='+startDateMonth;
    // var body = JSON.stringify(resignation);
  //  var headerOptions = new Headers({'Content-Type':'application/json'});
  //  var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url);
  }

}