import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../shared/configurl';
import { from } from 'rxjs';

@Injectable()
export class selfcertificationservice {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  getSelfCertification(compId : any,empTypeId: any,payrollMonth: any){
    
    let url = this.baseurl+'/selfcertification/selfCertificationList?compId='+compId+'&empTypeId='+empTypeId+'&payrollMonth='+payrollMonth;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  getSelfCertificationPayPeriod(empTypeId:any,payRollMonth: any){
    
    let url = this.baseurl+'/selfcertification/payPeriodMonth?empTypeId='+empTypeId + '&payRollMonth='+payRollMonth;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  

  getSelfCertificationPending(empTypeId : any,payrollMonth: any){
    
    let url = this.baseurl+'/selfcertification/selfCertificationPendingList?empTypeId='+empTypeId+'&payrollMonth='+payrollMonth;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  getMonthLockData(empPerId: any, payRollMonth: any){
    
    let url = this.baseurl+'/monthLockData?empPerId='+empPerId+'&payRollMonth='+payRollMonth;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  postSelfCertificationData(self) {
    let url = this.baseurl + '/selfcertification';
    var body = JSON.stringify(self);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post(url, self, requestOptions).map(x => x.json());
}

}