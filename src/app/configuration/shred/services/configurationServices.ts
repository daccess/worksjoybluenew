import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { ConfigurationData } from '../models/configurationmodel';
import { MainURL } from '../../../shared/configurl';
import { from } from 'rxjs';

@Injectable()
export class ConfigurationService {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  PosConfigurtaionservice(configuration : ConfigurationData){
    let url = this.baseurl + '/attendaceconfugeration';
    var body = JSON.stringify(configuration);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
 
}