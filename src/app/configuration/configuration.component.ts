import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../app/shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { ConfigurationService } from './shred/services/configurationServices';
import { ConfigurationData } from './shred/models/configurationmodel';
import { from } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material';
import { style } from '@angular/animations';
import { directiveCreate } from '@angular/core/src/render3/instructions';
import { dataService } from '../shared/services/dataService';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css'],
  providers: [ConfigurationService, DatePipe]
})
export class ConfigurationComponent implements OnInit {

  // selectedemptypeobj: any;
  selectedEmp: any;
  selectedSubEmpTypeObj: any;
  baseurl = MainURL.HostUrl;
  Employeedataall: any;
  compId: any;

  selectedEmpTypeObj: any;
  //model: any = {}
  isedit = false;

  isShow: boolean = true;

  lateRuleApllicabeloradvance: any;
  settingShow: boolean = true;

  model: any = {
    subEmployeeTypeMasters: []
  }
  advanSettingLateAndEarlyForPayDed: any
  ConfigurationData: ConfigurationData;

  AttendenceConfiAllData: any;
  EditAllEmpData: any;
  dateTime: any;
  Selectededitobj: any;
  upadateModel: any = {};

  maxLimit = 31;
  loggedUser: any;
  compName: string;
  cotSettingExtraWorkingList = [
    {
      // "cotExtraWorkId": 0,
      "dayType": "weekDay",
      "fullDay": null,
      "halfDay": null
    },
    {
      // "cotExtraWorkId": 0,
      "dayType": "weeklyOff",
      "fullDay": null,
      "halfDay": null
    },
    {
      // "cotExtraWorkId": 0,
      "dayType": "holiday",
      "fullDay": null,
      "halfDay": null
    }
  ];
  staggerOffSettingExtraWorkingList = [
    {
      "dayType": "weekDay",
      "fullDay": null,
      "halfDay": null,
    // "sofSetExtraWorkId":0
    },
    {
      "dayType": "weeklyOff",
      "fullDay": null,
      "halfDay": null
    // "sofSetExtraWorkId":0
    }
    // {
    //   // "cotExtraWorkId": 0,
    //   "dayType": "holiday",
    //   "fullDay": null,
    //   "halfDay": null
    // }
  ];
  selectUndefinedOptionValue: number;
  selectUndefinedOption: number;
  themeColor: string = "nav-pills-blue";
  subEmpTypeData: any = [];
  subEmpTypeDataobj1any: any;
  subCatFlag: boolean = false;
  dropdownSettingsEmploymentType: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  subCatErrMsg: string;

  constructor(public httpService: HttpClient,
    public chRef: ChangeDetectorRef,
    public confService: ConfigurationService,
    public toastr: ToastrService,
    private datePipe: DatePipe,
    public Spinner: NgxSpinnerService,
    public dataService: dataService) {
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.compName = this.loggedUser.compName;
    this.compId = this.loggedUser.compId;
    this.ConfigurationData = new ConfigurationData()
    
    this.ConfigurationData.applicableType = 'Overtime';
    this.ConfigurationData.lateRuleApllicabeloradvance = 'lateRuleApllicabel';
    this.ConfigurationData.includeWeekndsInPay = true
    this.ConfigurationData.includeHolidayInPay = true
    this.ConfigurationData.lateComming = true
    this.ConfigurationData.earlyGoing = true
    this.ConfigurationData.onDuty = true
    this.ConfigurationData.gatePass = true
    this.ConfigurationData.permissionReq = true
    this.ConfigurationData.perrPeriodToApply = 'Monthly'
    this.ConfigurationData.workfromHome = true
    this.ConfigurationData.coff = true
    this.ConfigurationData.absentOnPrefix = true;
    this.ConfigurationData.wasOnDuty = true;
    this.ConfigurationData.wasOnTraining = true;
    this.ConfigurationData.missPunch = true;
    this.ConfigurationData.lateComeAndEarlyGo = true;
    this.ConfigurationData.extraWork = true;
    this.ConfigurationData.perReqCon = true;
   this.ConfigurationData.actualWorkingHoursOfDay = 0.3;
   this.ConfigurationData.futureDays = true;
    //this.EditAllEmpData = this.ConfigurationData;
    this.selectedEmpTypeObj = "";
    this.selectedSubEmpTypeObj = "";
    // this.subEmpTypeDataobj1='';
    this.model.subEmpTypeData = '';
    this.ConfigurationData.effFromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();

    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    this.ConfigurationData.lateRuleApllicabel=false;
    this.ConfigurationData.earlyRuleIsApplicabel=false;
    //commited by priyanka k
  // this.ConfigurationData.perReqCon = false;
    this.ConfigurationData.extraWorReqAutIsReq=false;
  }
  togglaHeader() {
    this.isShow = !this.isShow;
  }

  ngOnInit() {

    this.getemployee();
    // this.ConfigurationData.staggerOffSettingExtraWorkingList = this.staggerOffSettingExtraWorkingList;
    this.getAttendenceConfiguration();
    this.dropdownSettingsEmploymentType = {
      singleSelection: false,
      idField: 'subEmpTypeId',
      textField: 'subEmpTypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }

  change(){
    this.ConfigurationData.perReqCon =  !this.ConfigurationData.perReqCon
  }

  getemployee() {
    let testempactive = this.baseurl + '/EmployeeType/Active/';
    this.httpService.get(testempactive + this.compId).subscribe(
      data => {
        this.Employeedataall = data["result"];
      },
      (err: HttpErrorResponse) => {
      }
    );

  }

  permissionReqChanged(event){
    this.ConfigurationData.perReqCon = event;
  }
  EmpValue(event) {
    this.ConfigurationData.subEmployeeTypeMasters = [];
    this.selectedEmpTypeObj = parseInt(event.target.value)
    this.subEmpType(this.selectedEmpTypeObj)
  }
  subEmpType(selectedValue) {
    this.subEmpTypeData = [];
    let urlsubempType = this.baseurl + '/SubEmployeeType/Active/';
    this.httpService.get(urlsubempType + selectedValue).subscribe(data => {
      this.subEmpTypeData = data["result"];
    },
    (err: HttpErrorResponse) => {
      this.subEmpTypeData = [];
    });
  }
  nextFisrt() {
    document.getElementById('pills-profile-tab').click();
  }

  nextSecond() {
    document.getElementById('pills-profile-tab1').click();
  }

  // nextThird() {
  //   document.getElementById('pills-profile-tab2').click();
  // }

  nextFour() {
    document.getElementById('pills-profile-tab3').click();

  }

  nextFive(){
    document.getElementById('pills-profile-tab2').click();
  }


  AllDataSave(f) {
    let url = "checktAttendConfigExist";
    if (this.isedit == false) {
     for(let i = 0; i < this.ConfigurationData.subEmployeeTypeMasters.length; i++){
           this.dataService.getRecordByIds(url,this.selectedEmpTypeObj,this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeId)
            .subscribe(data => {
             
              if(data.statusCode == "200"){
                this.subCatFlag = true;
                this.subCatErrMsg = "Attendance Configuration Alredy Exist for SubCategory "+this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeName+" ..!";
                this.toastr.error(this.subCatErrMsg);
              }
              else{
                this.subCatFlag = false;
              } 
            },(err: HttpErrorResponse) => {
              // this.toastr.error('Server Side Error..!');
              this.subCatFlag = false;
            });

     }
    }
      
    if(this.subCatFlag){
      this.toastr.error(this.subCatErrMsg);
      return;
    }
    this.ConfigurationData.cotSettingExtraWorkingList = this.cotSettingExtraWorkingList;
    // if(this.ConfigurationData.extraWorReqAutIsReq){
    //   this.ConfigurationData.extraWork = true;
    // }
    // else{
    //   this.ConfigurationData.extraWork = false;
    // }
    //  this.ConfigurationData.subEmployeeTypeMasters = this.model.subEmployeeTypeMasters;
    if (this.isedit == false) {
     
      parseInt(this.ConfigurationData.deductionInHrs)
      this.ConfigurationData.empTypeId = this.selectedEmpTypeObj;
      this.ConfigurationData.subEmpTypeName = this.selectedSubEmpTypeObj;
      this.Spinner.show();
      this.confService.PosConfigurtaionservice(this.ConfigurationData).subscribe(data => {
          this.getAttendenceConfiguration();
          f.reset();
          this.reset();
          // this.ConfigurationData = new ConfigurationData();
          this.Spinner.hide()
          this.toastr.success('Attendence Configuration Information Inserted Successfully!', 'Attendence Configuration');

          document.getElementById('pills-first-tab').click();
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide()
            this.toastr.error('Server Side Error..!', 'Attendence Configuration');
          });
    }
    else {

      // let urlupdate = this.baseurl + '/company';
      let urlupdate = this.baseurl + '/attendaceconfugeration';
      this.ConfigurationData.empTypeId = this.selectedEmpTypeObj;
      this.ConfigurationData.subEmpTypeName = this.selectedSubEmpTypeObj;
      this.upadateModel.empTypeId = this.selectedEmpTypeObj;
      this.Spinner.show()
      this.httpService.put(urlupdate, this.upadateModel).subscribe(data => {
          this.getAttendenceConfiguration();
          f.reset();
          this.reset();
          // this.ConfigurationData = new ConfigurationData();
          this.Spinner.hide()
          this.toastr.success('Attendence Configuration Updated Successfully!', 'Attendence Info Update');
          document.getElementById('pills-first-tab').click();
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Attendence Info Update');
          });

    }
  }

  reset() {

    setTimeout(() => {
      this.isedit = false;
      this.subCatFlag = false;
      this.ConfigurationData = new ConfigurationData()
      this.ConfigurationData.lateRuleApllicabeloradvance = 'lateRuleApllicabel';
      this.ConfigurationData.applicableType = 'Overtime';
      this.ConfigurationData.includeWeekndsInPay = true
      this.ConfigurationData.includeHolidayInPay = true
      this.ConfigurationData.lateComming = true
      this.ConfigurationData.earlyGoing = true
      this.ConfigurationData.onDuty = true
      this.ConfigurationData.gatePass = true
      this.ConfigurationData.permissionReq = true
      this.ConfigurationData.perrPeriodToApply = 'Monthly'
      this.ConfigurationData.workfromHome = true
      this.ConfigurationData.coff = true
      this.ConfigurationData.absentOnPrefix = true;
      this.ConfigurationData.wasOnDuty = true;
      this.ConfigurationData.wasOnTraining = true;
      this.ConfigurationData.lateComeAndEarlyGo = true;
      this.ConfigurationData.missPunch = true;
      this.ConfigurationData.extraWork = true;
      this.selectedEmpTypeObj = "";
      this.selectedSubEmpTypeObj = "";
      this.ConfigurationData.effFromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    }, 100);
  }

  getAttendenceConfiguration() {
    let AttendenceConfiList = this.baseurl + '/attendaceconfugeration/List/';
    this.httpService.get(AttendenceConfiList + this.compId).subscribe(data => {
        this.AttendenceConfiAllData = data["result"];
        for (let i = 0; i < this.AttendenceConfiAllData.length; i++) {
          this.AttendenceConfiAllData[i].subEmpTypeName = "";
          if (this.AttendenceConfiAllData[i].subEmployeeTypeAttenResDtoList) {
            for (let j = 0; j < this.AttendenceConfiAllData[i].subEmployeeTypeAttenResDtoList.length; j++) {
              if (this.AttendenceConfiAllData[i].subEmpTypeName) {
                this.AttendenceConfiAllData[i].subEmpTypeName = this.AttendenceConfiAllData[i].subEmpTypeName + " , " + this.AttendenceConfiAllData[i].subEmployeeTypeAttenResDtoList[j].subEmpTypeName;
              }
              else {
                this.AttendenceConfiAllData[i].subEmpTypeName = this.AttendenceConfiAllData[i].subEmployeeTypeAttenResDtoList[j].subEmpTypeName;
              }
            }
          }
          // this.dateTime = new Date(this.AttendenceConfiAllData[i].effFromDate);
        }
        this.chRef.detectChanges();
        setTimeout(function () {
          $(function () { //#CompTable
            var table = $('#emplisttable').DataTable();
          });
        }, 1000);

        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });

  }

  actualWorkingHoursOfDayChange(e) {
    this.ConfigurationData.actualWorkingHoursOfDay = e;
  }
  deductionInHrsChange(e) {
    this.ConfigurationData.deductionInHrs = e;
  }
  coffFlag: boolean = false;
  extraWorkingChanged(e) {
    if (this.ConfigurationData.coffWeekday || this.ConfigurationData.coffWeeklyOff || this.ConfigurationData.coffHoliday) {
      this.coffFlag = true;
    }
    else {
      this.coffFlag = false;
    }
  }

  sFlag: boolean = false;
  stragerChanged(e) {
    if (this.ConfigurationData.soffWeekday || this.ConfigurationData.soffWeeklyOff) {
      this.sFlag = true;
    }
    else {
      this.sFlag = false;
    }
  }

  edit(item) {
    this.Selectededitobj = item.attenConId;
    this.getEditData(this.Selectededitobj)
    this.isedit = true;

  }

  getEditData(attenConId) {
    let editURL = this.baseurl + '/attendaceconfugeration/';
    this.httpService.get(editURL + attenConId).subscribe(data => {
        this.EditAllEmpData = data["result"];
        // for (let i = 0; i < this.EditAllEmpData.length; i++) {
        this.dateTime = new Date(this.EditAllEmpData.effFromDate)
        // }
        var dateT = this.datePipe.transform(this.dateTime, 'yyyy-MM-dd');
        // this.ConfigurationData = this.EditAllEmpData
        this.ConfigurationData.effFromDate = dateT;
        // this.ConfigurationData.lateEarlyGoConId;
        this.ConfigurationData.lateRuleApllicabel = this.EditAllEmpData['lateRuleApllicabel'];
        this.ConfigurationData.graceTimeForLatePeriod = this.EditAllEmpData['graceTimeForLatePeriod'];
        this.ConfigurationData.wantToAddDeductionOfDaysOnLate = this.EditAllEmpData['wantToAddDeductionOfDaysOnLate'];
        this.ConfigurationData.noOffLateCommingAllowPerMonth = this.EditAllEmpData['noOffLateCommingAllowPerMonth'];
        this.ConfigurationData.noOffDaysToDeductAfterCrossingAllowedEvent = this.EditAllEmpData['noOffDaysToDeductAfterCrossingAllowedEvent'];
        this.ConfigurationData.earlyRuleIsApplicabel = this.EditAllEmpData['earlyRuleIsApplicabel'];
        this.ConfigurationData.graceTimeForEarlyPeriod = this.EditAllEmpData['graceTimeForEarlyPeriod'];
        this.ConfigurationData.wantToAddDeductionOnEarly = this.EditAllEmpData['wantToAddDeductionOnEarly'];
        this.ConfigurationData.noOffEarlyGoingsAllowedInAMonth = this.EditAllEmpData['noOffEarlyGoingsAllowedInAMonth'];
        this.ConfigurationData.noDaysToDeductAfterCrossingAllowed = this.EditAllEmpData['noDaysToDeductAfterCrossingAllowed'];
        this.ConfigurationData.advanSettingLateAndEarlyForPayDed = this.EditAllEmpData['advanSettingLateAndEarlyForPayDed'];
        this.ConfigurationData.actualWorkingHoursOfDay = this.EditAllEmpData['actualWorkingHoursOfDay'];
        this.ConfigurationData.noOfMinPerMonth = this.EditAllEmpData['noOfMinPerMonth'];
        this.ConfigurationData.deductionInHrs = this.EditAllEmpData['deductionInHrs'];
        this.ConfigurationData.applicableType = this.EditAllEmpData['applicableType'];
        this.ConfigurationData.extraWorReqAutIsReq = this.EditAllEmpData['extraWorReqAutIsReq'];
        this.ConfigurationData.futureDays = this.EditAllEmpData['futureDays'];
        this.ConfigurationData.pastDays = this.EditAllEmpData['pastDays'];
        this.ConfigurationData.coffWeekday = this.EditAllEmpData['coffWeekday'];
        this.ConfigurationData.coffWeeklyOff = this.EditAllEmpData['coffWeeklyOff'];
        this.ConfigurationData.coffHoliday = this.EditAllEmpData['coffHoliday'];
        this.ConfigurationData.coffHalfday = this.EditAllEmpData['coffHalfday'];
        this.ConfigurationData.coffCollapsedDays = this.EditAllEmpData['coffCollapsedDays'];
        this.ConfigurationData.includeWeekndsInPay = this.EditAllEmpData['includeWeekndsInPay'];
        this.ConfigurationData.includeHolidayInPay = this.EditAllEmpData['includeHolidayInPay'];
        this.ConfigurationData.lateComming = this.EditAllEmpData['lateComming'];
        this.ConfigurationData.earlyGoing = this.EditAllEmpData['earlyGoing'];
        this.ConfigurationData.onDuty = this.EditAllEmpData['onDuty'];
        this.ConfigurationData.gatePass = this.EditAllEmpData['gatePass'];
        this.ConfigurationData.permissionReq = this.EditAllEmpData['permissionReq'];
        this.ConfigurationData.perReqCon = this.EditAllEmpData['perReqCon'];
        this.ConfigurationData.perrPeriodToApply = this.EditAllEmpData['perrPeriodToApply'];
        this.ConfigurationData.numOffPerReqAllPerMon = this.EditAllEmpData['numOffPerReqAllPerMon'];
        this.ConfigurationData.numPerReqAllPerDay = this.EditAllEmpData['numPerReqAllPerDay'];
        this.ConfigurationData.maxMinutesAllowedRequest = this.EditAllEmpData['maxMinutesAllowedRequest'];
        this.ConfigurationData.numOfDaysBeforeYouCanApply = this.EditAllEmpData['numOfDaysBeforeYouCanApply'];
        this.ConfigurationData.numOfDaysAfterYouCanApply = this.EditAllEmpData['numOfDaysAfterYouCanApply'];
        this.ConfigurationData.applicableType = this.EditAllEmpData['applicableType'];
        this.ConfigurationData.overtimeMinMinutes = this.EditAllEmpData['overtimeMinMinutes'];
        this.ConfigurationData.extraWorReqAutIsReq = this.EditAllEmpData['extraWorReqAutIsReq'];
        this.ConfigurationData.futureDays = this.EditAllEmpData['futureDays'];
        this.ConfigurationData.pastDays = this.EditAllEmpData['pastDays'];
        this.ConfigurationData.coffWeekday = this.EditAllEmpData['coffWeekday'];
        this.ConfigurationData.coffWeeklyOff = this.EditAllEmpData['coffWeeklyOff'];
        this.ConfigurationData.coffHoliday = this.EditAllEmpData['coffHoliday'];
        this.ConfigurationData.coffHalfday = this.EditAllEmpData['coffHalfday'];
        this.ConfigurationData.coffCollapsedDays = this.EditAllEmpData['coffCollapsedDays'];
        this.ConfigurationData.wasOnTraining = this.EditAllEmpData['wasOnTraining'];
        this.ConfigurationData.wasOnDuty = this.EditAllEmpData['wasOnDuty'];
        this.ConfigurationData.numOffPerReqAllPerYear = this.EditAllEmpData['numOffPerReqAllPerYear'];
        this.upadateModel = this.ConfigurationData;
        this.upadateModel.lateEarlyGoConId = this.EditAllEmpData['lateEarlyGoConId'];
        this.upadateModel.perReqConf = this.EditAllEmpData['perReqConf'];
        this.upadateModel.attReqId = this.EditAllEmpData['attReqId'];
        this.upadateModel.payrollSettingId = this.EditAllEmpData['payrollSettingId'];
        this.upadateModel.comOffOveTimeId = this.EditAllEmpData['comOffOveTimeId'];
        this.upadateModel.attenConId = this.EditAllEmpData['attenConId'];
        this.ConfigurationData.workfromHome = this.EditAllEmpData['workfromHome'];
        this.ConfigurationData.coff = this.EditAllEmpData['coff'];
        // this.ConfigurationData.absentOnPrefix = this.EditAllEmpData['absentOnPrefix'];
        this.cotSettingExtraWorkingList = this.EditAllEmpData['cotSettingExtraWorkingResDtos'];
        this.ConfigurationData.subEmployeeTypeMasters = this.EditAllEmpData.subEmployeeTypeAttendanceResDtos;
        this.ConfigurationData.lateComeAndEarlyGo = this.EditAllEmpData.lateComeAndEarlyGo;
        this.ConfigurationData.missPunch = this.EditAllEmpData.missPunch;
        this.ConfigurationData.extraWork = this.EditAllEmpData.extraWork;
        this.ConfigurationData.lateRuleApllicabeloradvance = this.EditAllEmpData.lateRuleApllicabeloradvance;
        this.selectedEmpTypeObj = this.EditAllEmpData['employeeTypeMaster']['empTypeId'];
        this.subEmpType(this.selectedEmpTypeObj);
        this.selectedSubEmpTypeObj = this.EditAllEmpData['subEmpTypeName'];
        // this.ConfigurationData.isSOffApplicable = this.EditAllEmpData.staggeredOffConfiguration.isSOffApplicable;
        // this.ConfigurationData.soffWeekday = this.EditAllEmpData.staggeredOffConfiguration.soffWeekday;
        // this.ConfigurationData.soffWeeklyOff = this.EditAllEmpData.staggeredOffConfiguration.soffWeeklyOff;
        // this.ConfigurationData.soffCollapseDays = this.EditAllEmpData.staggeredOffConfiguration.soffCollapseDays;
        // this.ConfigurationData.staggerOffSettingExtraWorkingList = this.EditAllEmpData.staggerOffSettingExtraWorkingList;
        // this.ConfigurationData.soffConfigId =  this.EditAllEmpData.staggeredOffConfiguration.soffConfigId;
        // this.ConfigurationData.numOffDaysContWorking = this.EditAllEmpData.staggeredOffConfiguration.numOffDaysContWorking;
      },
      (err: HttpErrorResponse) => {
      });
  }

  clearDeductionofDays() {
    this.ConfigurationData.noOffLateCommingAllowPerMonth = null;
    this.ConfigurationData.noOffDaysToDeductAfterCrossingAllowedEvent = this.selectUndefinedOptionValue;
  }

  effectiveDate(event) {
    this.ConfigurationData.effFromDate = event;
  }
  clearPerReqApp() {
    this.ConfigurationData.numOffPerReqAllPerYear = null;
    this.ConfigurationData.numOffPerReqAllPerMon = null;
    this.ConfigurationData.numPerReqAllPerDay = null;
    this.ConfigurationData.maxMinutesAllowedRequest = null;
    this.ConfigurationData.numOfDaysBeforeYouCanApply = null;
    this.ConfigurationData.numOfDaysAfterYouCanApply = null;
  }
  clearholidayoff() {
    this.cotSettingExtraWorkingList[1].fullDay = null;
    this.cotSettingExtraWorkingList[1].halfDay = null;
  }
  clearWeeklyoff() {
    this.cotSettingExtraWorkingList[0].halfDay = null;
    this.cotSettingExtraWorkingList[0].fullDay = null;
  }
  clearWeekday() {
    this.cotSettingExtraWorkingList[2].halfDay = null;
    this.cotSettingExtraWorkingList[2].fullDay = null;
  }
  clearLateRule() {
    this.ConfigurationData.graceTimeForLatePeriod = null;
    this.ConfigurationData.wantToAddDeductionOfDaysOnLate = false;
  }
  clearEarlyRule() {
    this.ConfigurationData.graceTimeForEarlyPeriod = null;
    this.ConfigurationData.wantToAddDeductionOnEarly = false;
  }
  clearDeductioOfDay() {
    this.ConfigurationData.noOffEarlyGoingsAllowedInAMonth = null;
    this.ConfigurationData.noDaysToDeductAfterCrossingAllowed = this.selectUndefinedOption;
  }
  clearExtra() {
    if (this.ConfigurationData.extraWorReqAutIsReq == true) {
      this.ConfigurationData.futureDays = true;
    }
    else {
      this.ConfigurationData.futureDays = false;
    }

  }
  clearOverT(){
    this.ConfigurationData.overtimeMinMinutes =  null
  }
  clearCoff() {
    this.ConfigurationData.extraWorReqAutIsReq = false;
    this.ConfigurationData.coffWeekday = false;
    this.ConfigurationData.coffCollapsedDays = null;
    this.ConfigurationData.coffWeekday = false;
    this.ConfigurationData.coffWeeklyOff = false;
    this.ConfigurationData.coffHoliday = false;
  }
  clear(){
  
    this.ConfigurationData.numOffPerReqAllPerYear = null
    this.ConfigurationData.numOffPerReqAllPerMon = null
    this.ConfigurationData.numPerReqAllPerDay = null
    this.ConfigurationData.maxMinutesAllowedRequest = null
    this.ConfigurationData.numOfDaysBeforeYouCanApply = null
    this.ConfigurationData .numOfDaysAfterYouCanApply = null

  }

  sclearWeeklyoff() {
    //this.ConfigurationData.staggerOffSettingExtraWorkingList[0].halfDay = null;
    // this.ConfigurationData.staggerOffSettingExtraWorkingList[0].fullDay = null;
  }
  sclearWeekday() {
    //this.ConfigurationData.staggerOffSettingExtraWorkingList[1].halfDay = null;
    // this.ConfigurationData.staggerOffSettingExtraWorkingList[1].fullDay = null;
  }
  clearstaggeredoff() {
   // this.ConfigurationData.staggerOffSettingExtraWorkingList[0].halfDay = null;
    // this.ConfigurationData.staggerOffSettingExtraWorkingList[0].fullDay = null;
   // this.ConfigurationData.staggerOffSettingExtraWorkingList[1].halfDay = null;
    // this.ConfigurationData.staggerOffSettingExtraWorkingList[1].fullDay = null;
    // this.ConfigurationData.isSOffApplicable = false;
    // this.ConfigurationData.soffCollapseDays = null;
    // this.ConfigurationData.soffWeekday = false;
    // this.ConfigurationData.soffWeeklyOff = false;
    // this.ConfigurationData.numOffDaysContWorking = null;
  }

  perReqConChanged(event){
    // this.ConfigurationData.perReqCon = false;
    this.ConfigurationData.permissionReq = event;
  }
  clearMonthly(){
    this.ConfigurationData.numPerReqAllPerDay = 0;
    this.ConfigurationData.numOffPerReqAllPerYear = 0;
    this.ConfigurationData.maxMinutesAllowedRequest = 0;
    this.ConfigurationData.numOffPerReqAllPerMon = 0;
  }
  clearYearly(){
    this.ConfigurationData.numPerReqAllPerDay = 0;
    this.ConfigurationData.numOffPerReqAllPerYear = 0;
    this.ConfigurationData.maxMinutesAllowedRequest = 0;
    this.ConfigurationData.numOffPerReqAllPerMon = 0;
  }
  subEmpFlag: boolean = false;
  selectClickSubEmp() {
    
    let url = "checktAttendConfigExist";

    for(let i = 0; i < this.ConfigurationData.subEmployeeTypeMasters.length; i++){
      let flag = false;
      // if(this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeId){
        if(this.isedit){
          for(let j = 0; j < this.EditAllEmpData.subEmployeeTypeAttendanceResDtos.length; j++){
            if(this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeId == this.EditAllEmpData.subEmployeeTypeAttendanceResDtos[j].subEmpTypeId)
            {
              flag = true;
            }
          }
          if(flag){
            this.subCatFlag = false;
            continue;
          }
          else{
            this.dataService.getRecordByIds(url,this.selectedEmpTypeObj,this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeId)
            .subscribe(data => {
              console.log("****************",data);
              if(data.statusCode == "200"){
                this.subCatFlag = true;
                this.subCatErrMsg = "Attendance Configuration Alredy Exist for SubCategory "+this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeName+" ..!";
                this.toastr.error(this.subCatErrMsg);
              }
              else{
                this.subCatFlag = false;
              }
            },(err: HttpErrorResponse) => {
              // this.toastr.error('Server Side Error..!');
              this.subCatFlag = false;
            });

          }
          
        }
        else{
          this.dataService.getRecordByIds(url,this.selectedEmpTypeObj,this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeId)
          .subscribe(data => {
            console.log("****************",data);
            if(data.statusCode == "200"){
              this.subCatFlag = true;
              this.subCatErrMsg = "Attendance Configuration Alredy Exist for SubCategory "+this.ConfigurationData.subEmployeeTypeMasters[i].subEmpTypeName+" ..!";
              this.toastr.error(this.subCatErrMsg);
            }
            else{
              this.subCatFlag = false;
            }
          },(err: HttpErrorResponse) => {
            // this.toastr.error('Server Side Error..!');
            this.subCatFlag = false;
          });
        }
      
      // }
       
      if(this.subCatFlag){
        break;
      }
    }
  }


  selectClickCategory() {
    if (this.model.subEmpTypeData.length == 0) {
      this.subEmpFlag = true;
    }
    else {
      this.subEmpFlag = false;
    }
  }

}

