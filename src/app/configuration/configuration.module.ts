import { routing } from './configuration.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { ConfigurationService } from './shred/services/configurationServices';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ConfigurationComponent } from './configuration.component'
import { from } from 'rxjs';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


@NgModule({
    declarations: [
        ConfigurationComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule
    ],
    providers: [ConfigurationService]
  })
  export class ConfigurationModule { 
      constructor(){

      }
  }
  