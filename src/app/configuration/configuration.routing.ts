import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ConfigurationComponent } from 'src/app/configuration/configuration.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ConfigurationComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'configuration',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'configuration',
                    component: ConfigurationComponent
                }

            ]

    }
  
    
 
]

export const routing = RouterModule.forChild(appRoutes);