import { Component, OnInit } from '@angular/core';
import { ClearenceService } from '../shared/services/clearenceservice';

@Component({
  selector: 'app-clearancedetailsemp',
  templateUrl: './clearancedetailsemp.component.html',
  styleUrls: ['./clearancedetailsemp.component.css']
})
export class ClearancedetailsempComponent implements OnInit {
  loggedUser:any;
  clearanceList =[];
  constructor(private service : ClearenceService) { }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getClearanceList()
    
  }
  dataTableFlag:boolean=true;
  getClearanceList(){
    let obj={
      empPerId:this.loggedUser.empPerId
    }
   this.dataTableFlag = false;
    this.service.clearanceDetails(obj).subscribe(data=>{
      this.clearanceList = data.result;
      let obj = new Array();
      for (let i = 0; i < this.clearanceList.length; i++) {
        this.clearanceList[i].fullName = this.clearanceList[i].fullName +" "+this.clearanceList[i].lastName;
        if(!this.clearanceList[i].officialContactNumber){
          this.clearanceList[i].officialContactNumber = 0;
        }
      }
      for (let i = 0; i < this.clearanceList.length; i++) {
        for (let j = 0; j < this.clearanceList.length; j++) {
          if(this.clearanceList[i].queryAdminRoleName == this.clearanceList[j].queryAdminRoleName && i!=j){
            this.clearanceList[i].fullName = this.clearanceList[i].fullName +" / "+this.clearanceList[j].fullName;
            this.clearanceList[i].officialContactNumber = this.clearanceList[i].officialContactNumber +" / "+this.clearanceList[j].officialContactNumber;
            this.clearanceList.splice(j,1);
          
          }
        }
      }
      this.dataTableFlag = true;
      setTimeout(function () {
        $(function () {
          var table = $('#emptable').DataTable();
        });
      }, 100);
    })
  }

  changeRecaptchaLanguage() {
    document.querySelector('.g-recaptcha').innerHTML = '';
  
    var script = document.createElement('script');
    script.src = 'https://www.google.com/recaptcha/api.js?hl=' + 'en';
    script.async = true;
    script.defer = true;
    document.querySelector('head').appendChild(script);
  }
  languagesArray = ["ar","af","am","hy","az","eu","bn","bg","ca"]

  updateGoogleCaptchaLanguage() {
      // Get GoogleCaptcha iframe
      var iframeGoogleCaptcha = $('#captcha_container').find('iframe');
  
      // Get language code from iframe
      // var language = iframeGoogleCaptcha.attr("src").match(/hl=(.*?)&/).pop();
  
      // Get selected language code from drop down
      var selectedLanguage = $('#ddllanguageListsGoogleCaptcha').val();
      
      // Check if language code of element is not equal by selected language, we need to set new language code
      // if (language !== selectedLanguage) {
          // For setting new language 
          iframeGoogleCaptcha.attr("src", iframeGoogleCaptcha.attr("src").replace(/hl=(.*?)&/, 'hl=' + selectedLanguage + '&'));
      // }
  }
}
