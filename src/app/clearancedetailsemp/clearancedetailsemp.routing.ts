import { Routes, RouterModule } from '@angular/router'

import { ClearancedetailsempComponent } from './clearancedetailsemp.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ClearancedetailsempComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'clearancedetailsforemp',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'clearancedetailsforemp',
                    component: ClearancedetailsempComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);