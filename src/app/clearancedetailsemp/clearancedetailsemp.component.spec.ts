import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearancedetailsempComponent } from './clearancedetailsemp.component';

describe('ClearancedetailsempComponent', () => {
  let component: ClearancedetailsempComponent;
  let fixture: ComponentFixture<ClearancedetailsempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearancedetailsempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearancedetailsempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
