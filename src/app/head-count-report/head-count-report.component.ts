import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-head-count-report',
  templateUrl: './head-count-report.component.html',
  styleUrls: ['./head-count-report.component.css'],
  providers:[ExcelService,DatePipe]
})
export class HeadCountReportComponent implements OnInit {

  AllReportData: any;
  AllReport: any;
  displayReposrt=[];
  srnoCheckbox = true;
  namedeptcheckbox = true;
  strengthCheckbox = true;
  presentCheckbox = true;
  absentCheckbox = true;
  marked = true;
  marked1 = true;
  // marked2 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;

  fromDate: any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;

  constructor(private excelService:ExcelService,private datePipe: DatePipe) { 
    this.fromDate = sessionStorage.getItem("fromDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.AllReportData = sessionStorage.getItem('HeadCountreportData')
    this.AllReport = JSON.parse(this.AllReportData);
    let data1 = this.AllReport[0].headCountPresentAbsentResDtos;
    let data2 = this.AllReport[0].headCountTotalResDtoList;
    for(let i=0;i<data1.length;i++){
      for(let j=0;j<data2.length;j++){
        if(data1[i].deptName.trim() == data2[j].deptName.trim()){
          let obj = {
            present : data1[i].present,
            deptName : data1[i].deptName.trim(),
            total : data2[j].total,
            absent : data2[j].total - data1[i].present 
          }
          this.displayReposrt.push(obj)
        }
      }
    }
  }

  ngOnInit() {
  }

  exportAsXLSX():void {
    let excelData = new Array();
    for (let i = 0; i < this.displayReposrt.length; i++) {
      let obj : any= {}
      if(this.marked){
        obj.sr = this.displayReposrt[i]+1;
         }

       if(this.marked1){
      obj.deptName = this.displayReposrt[i].deptName.trim()
       }
       if(this.marked2){
        obj.total = this.displayReposrt[i].total
      }
      if(this.marked3){
        obj.present = this.displayReposrt[i].present
      }
      if(this.marked4){
      obj.absent = this.displayReposrt[i].absent
      }

       excelData.push(obj)
    }
    this.excelService.exportAsExcelFile(excelData, 'Head_count_report');
  }

  toggleVisibility(e){
    this.marked = e.target.checked;
  }

  toggleVisibility1(e){
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e){
    this.marked2 = e.target.checked;
  }

  toggleVisibility3(e){
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e){
    this.marked4 = e.target.checked;
  }

  capturePDF(){

    let item = {
      header:"Department",
      title : "abc"
    }
    let item1 = {
      header:"Strengh",
      title : "cde"
    }
    let item2 = {
      header:"Present",
      title : "efg"
    }
    let item3 = {
      header:"Absent",
      title : "efg"
    }
    let item4 = {
      header:"Sr.No",
      title : "efg"
    }
    

    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [];
    if(this.marked){
      col.push(item4);
    }
    if(this.marked1){
      col.push(item);
    }
    if(this.marked2){
      col.push(item1);
    }
    if(this.marked3){
      col.push(item2);
    }
    if(this.marked4){
      col.push(item3);
    }
    
    
    var rows = [];
    var rowCountModNew = this.displayReposrt;

    rowCountModNew.forEach((element , index ) => {
      let obj = []
      if(this.marked){
        obj.push(index+1);
      }
      if(this.marked1){
        obj.push(element.deptName.trim());
      }

      if(this.marked2){
        obj.push(element.total);
      }

      if(this.marked3){
        obj.push(element.present);
      }

      if(this.marked4){
        obj.push(element.absent);
      }

    rows.push(obj);
  });
  doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14 ,'Company Name :'+ this.compName)

    
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12);
    doc.setFontStyle("Arial")
    doc.text(280, 14,'Head Count Report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(8)
    doc.setFontStyle("Arial")
    doc.text(287, 24 ,'From' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy"))
    

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"))
      doc.autoTable(col, rows,{
        tableLineColor: [189, 195, 199],
        tableLineWidth: 0.75,
        headerStyles: {
          fillColor: [103, 132, 130],   
      },
      
        styles: {
          halign: 'center'
        },
        theme: 'grid',
        margin: { top: 50, left: 5, right: 5, bottom: 50 }
      });
      doc.save('Head_count'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');
  }
}
