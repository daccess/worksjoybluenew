import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadCountReportComponent } from './head-count-report.component';

describe('HeadCountReportComponent', () => {
  let component: HeadCountReportComponent;
  let fixture: ComponentFixture<HeadCountReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadCountReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadCountReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
