import { Routes, RouterModule } from '@angular/router'
import { HeadCountReportComponent } from './head-count-report.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: HeadCountReportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'headcountreports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'headcountreports',
                    component: HeadCountReportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);