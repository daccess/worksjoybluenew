import { ContractorOnBoardingModule } from './contractor-on-boarding.module';

describe('ContractorOnBoardingModule', () => {
  let contractorOnBoardingModule: ContractorOnBoardingModule;

  beforeEach(() => {
    contractorOnBoardingModule = new ContractorOnBoardingModule();
  });

  it('should create an instance', () => {
    expect(contractorOnBoardingModule).toBeTruthy();
  });
});
