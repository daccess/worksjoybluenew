import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorOnBoardingComponent } from './contractor-on-boarding.component';

describe('ContractorOnBoardingComponent', () => {
  let component: ContractorOnBoardingComponent;
  let fixture: ComponentFixture<ContractorOnBoardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorOnBoardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorOnBoardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
