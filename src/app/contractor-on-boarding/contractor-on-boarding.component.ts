import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { MainURL } from "../shared/configurl";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { RoleMasterService } from "../shared/services/RoleMasterService";


@Component({
  selector: "app-contractor-on-boarding",
  templateUrl: "./contractor-on-boarding.component.html",
  styleUrls: ["./contractor-on-boarding.component.css"],
})
export class ContractorOnBoardingComponent implements OnInit {
  themeColor: string = "nav-pills-blue";
  baseurl = MainURL.HostUrl;
  locationFlag = false;
  dropdownSettings: {};
  dropdownSettings1: {};
  myObject: any = {};

  selectedEmp:any = [];
  selectedEmp1:any = [];
  ContractorCompanyName: any;
  ContractorName: any;
  EmailId: any;
  IsEditeFlag:boolean=false;
  ContactNumber: any;
  VendorCode: any;
  SelectServiceTypeList: any=[];
  SelectLocationList: any=[];
  searchCriteria: string; // Property to store the selected search criteria
  searchQuery: string = ""; 
  // ContractorForm:any = {

  //   ContractorCompanyName:'',
  //   ContractorName:'',
  //   EmailId:'',
  //   ContactNumber:'',
  //   SelectServiceTypeList:'',
  //   SelectLocationList:'',
  // };
  user: string;
   StatusFlag: boolean = false
  isedit = false;
  users: any;
  token: any;
  allEmpdata: any;
  EmpData: any;
  allEmpdata1: any;
  EmpData1: any;
  alldataofDropDown: any;
  RoleId: any;
  allContractor: any;
  allContractorData: any;
  allContractorone: any =[];
result: any;
  isEditFlag: boolean;
  getContractorOnboardingIds: any;
  filteredContractors: any;
  searchByColumn: any;
  serviceFromDate: any;
  serviceToDate: any;
  initialEmailId: any;
  initialContactNumber: any;
  initialVendorCode: any;
  initialSelectedEmp: any[];
  initialRoleId: any;
  editdata: any;
  EditedDatas: any;
  conMasterId: any;
  conMasterIds: any;
  compIds: any;
  constructor(
    private httpSer: HttpClient,
    public Spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private roleservice: RoleMasterService
  ) {}

  ngOnInit() {
    this.user = sessionStorage.getItem("loggeduser");

    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.compIds=this.users.roleList.compId;


    this.getAllServiceType();
    this.getAllLocation();
    this.getAllRole();
    this.GetAllContractorMasters();

    this.dropdownSettings = {
      singleSelection: false,
      idField: "conSerTypeId",
      textField: "conSerTypeName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 100,
      allowSearchFilter: true,
    };
    this.dropdownSettings1 = {
      singleSelection: false,
      idField: "locationId",
      textField: "locationName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 100,
      allowSearchFilter: true,
    };
  }
  getAllRole() {
    
    this.roleservice.GetAllRole(this.token).subscribe((data:any) => {
      this.alldataofDropDown = data.result;
      this.RoleId = this.alldataofDropDown.assignCandidateRoleList[0].roleId;
      console.log("hshfhsah",this.RoleId)
    });
  }
  getAllServiceType() {
    let url = this.baseurl + "/getAllContractorServiceTypes";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpSer.get(url, { headers }).subscribe(
      (data) => {
        this.allEmpdata = data["result"];
        this.EmpData = this.allEmpdata;
        for (let index = 0; index < this.EmpData.length; index++) {
          this.EmpData[index].conSerTypeName =
            this.EmpData[index].conSerTypeId +
            "-" +
            this.EmpData[index].conSerTypeName;
        }
      },
      (err: HttpErrorResponse) => {}
    );
  }
  selectServiceType() {
    if (this.SelectServiceTypeList.length == 0) {
      this.locationFlag = true;
    } else {
      this.locationFlag = false;
    }
  }
  onItemSelect(item: any) {
    const id = item;
    if (this.selectedEmp.length == 0) {
      this.selectedEmp.push(item);
      // this.selectEmp(id);
    } else {
      for (var i = 0; i < this.selectedEmp.length; i++) {
        if (this.selectedEmp[i].conSerTypeId == item.conSerTypeId) {
        } else {
          this.selectedEmp.push(item);
        }
      }
    }
  }
  onSelectAll(items: any) {
    this.selectedEmp = items;
  }
  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedEmp.length; i++) {
      if (this.selectedEmp.length == 0) {
        if (this.selectedEmp[i].conSerTypeId == item.conSerTypeId) {
          this.selectedEmp.splice(i, 1);
        }
      } else {
        if (this.selectedEmp[i].conSerTypeId == item.conSerTypeId) {
          this.selectedEmp.splice(i, 1);
        }
      }
    }
  }
  onDeSelectAll(item: any) {
    this.selectedEmp = [];
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }

  getAllLocation() {
    let url = this.baseurl + "/getAllLocations";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpSer.get(url, { headers }).subscribe(
      (data) => {
        this.allEmpdata1 = data["result"];
        this.EmpData1 = this.allEmpdata1;
        for (let index = 0; index < this.EmpData1.length; index++) {
          this.EmpData1[index].locationName =
            this.EmpData1[index].locationId +
            "-" +
            this.EmpData1[index].locationName;
        }
      },
      (err: HttpErrorResponse) => {}
    );
  }
  GetAllContractorMasters() {
    let url = this.baseurl + "/getAllContractorMaster";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.httpSer.get(url, { headers }).subscribe((data) => {
      this.allContractorone = data["result"];
      console.log("Contractor Deatils",this.allContractorone)
      
      setTimeout(function () {

        $(function () {

          var table = $('#contractorOnBoardingTables').DataTable({

            retrieve: true,
           
            searching:true
          })

        });
      }, 100);
      


    },
    (err: HttpErrorResponse) => {}

    );
  }

  selectServiceType1() {
    if (this.SelectLocationList.length == 0) {
      this.locationFlag = true;
    } else {
      this.locationFlag = false;
    }
  }
  onItemSelect1(item: any) {
    const id = item;
    if (this.selectedEmp1.length == 0) {
      this.selectedEmp1.push(item);
      // this.selectEmp(id);
    } else {
      for (var i = 0; i < this.selectedEmp1.length; i++) {
        if (this.selectedEmp1[i].locationId == item.locationId) {
        } else {
          this.selectedEmp1.push(item);
        }
      }
    }
  }
  onSelectAll1(items: any) {
    this.selectedEmp1 = items;
  }
  OnItemDeSelect1(item: any) {
    for (var i = 0; i < this.selectedEmp1.length; i++) {
      if (this.selectedEmp1.length == 0) {
        if (this.selectedEmp1[i].locationId == item.locationId) {
          this.selectedEmp1.splice(i, 1);
        }
      } else {
        if (this.selectedEmp1[i].locationId == item.locationId) {
          this.selectedEmp1.splice(i, 1);
        }
      }
    }
  }
  onDeSelectAll1(item: any) {
    this.selectedEmp1 = [];
  }

  submitContractorOnBoarding() {
    if(this.IsEditeFlag==false){

    debugger
    var obj = {
      conCompanyName: this.ContractorCompanyName,
      contractorName: this.ContractorName,
      emailId: this.EmailId,
      vendorCode: this.VendorCode,
      mobileNo: this.ContactNumber,
      selectServiceTypeList: this.selectedEmp,
      selectLocationList: this.selectedEmp1,
      roleId: this.RoleId,
      serviceFromDate:this.serviceFromDate,
      serviceToDate:this.serviceToDate,
      compId:this.compIds,
    };
    // this.myObject.push(obj)
    console.log(obj);
    let url = this.baseurl + "/contractorOnBoarding";
    const tokens = this.token;
    const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
    this.Spinner.show();
    this.httpSer.post(url, obj, { headers }).subscribe(
      (data) => {
        this.Spinner.hide();

        this.toastr.success("Contractor On Boarding Inserted Successfully");
        document.getElementById('extra-list-tab').click();
      
        this.GetAllContractorMasters()
      },
      // (err: HttpErrorResponse) => {
      //   this.Spinner.hide();
      //   this.toastr.error("Server Side Error..!");
       
      // }
      err => {
        
        this.Spinner.hide();
        if (err) {
          this.toastr.error(err.error.result); // Use err.error.result instead of err.json().result
          console.log("err.error.result", err.error.result);
        } else {
          this.toastr.error("Server Side Error..!");
        }
      });
      
    
    }
    else{
      var uniqueSelectLocationList = Array.from(new Set(this.selectedEmp1.map(item => item.locationId)))
  .map(locationId => this.selectedEmp1.find(item => item.locationId === locationId));

console.log(uniqueSelectLocationList);
var uniqueServiceTypeList = Array.from(new Set(this.selectedEmp.map(item => item.conSerTypeId)))
.map(conSerTypeId => this.selectedEmp.find(item => item.conSerTypeId === conSerTypeId));

console.log(uniqueSelectLocationList);
      let obj = {
        conCompanyName: this.ContractorCompanyName,
        contractorName: this.ContractorName,
        emailId: this.EmailId,
        vendorCode: this.VendorCode,
        mobileNo: this.ContactNumber,
        
        selectServiceTypeList: uniqueServiceTypeList,
        selectLocationList: uniqueSelectLocationList,
        roleId: this.RoleId,
        serviceFromDate:this.serviceFromDate,
        serviceToDate:this.serviceToDate,
        conMasterId:this.conMasterId,
      };

      console.log(obj);
      let url = this.baseurl + "/updateContractorForLocationHR";
      const tokens = this.token;
      const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
      this.Spinner.show();
      this.httpSer.put(url, obj, { headers }).subscribe(
        (data) => {
          this.Spinner.hide();
  
          this.toastr.success("Contractor On Boarding update Successfully");
          document.getElementById('extra-list-tab').click();
          this.IsEditeFlag=false
        
          this.GetAllContractorMasters()
        },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error("Server Side Error..!");
         
        }
      );
    }

    
   
  }


  
  

  // Function to reset the form to its initial state
// resetForm() {
//   this.ContractorCompanyName = this.ContractorCompanyName;
//   this.ContractorName = this.ContractorName;
//   this.EmailId = this.initialEmailId;
//   this.ContactNumber = this.initialContactNumber;
//   this.selectedEmp = this.initialSelectedEmp;
//   this.selectedEmp1 = this.initialSelectedEmp;
//   this.RoleId = this.initialRoleId;
// }

  
  
resetFormOne() {
  this.ContractorCompanyName = ''; // Reset the Contractor Company Name
  this.ContractorName = ''; // Reset the Contractor Name
  this.EmailId = this.initialEmailId; // Reset EmailId to its initial value
  this.ContactNumber = this.initialContactNumber; // Reset ContactNumber to its initial value
  this.VendorCode= this.initialVendorCode,
  this.selectedEmp = this.initialSelectedEmp; // Reset selectedEmp to its initial value
  this.selectedEmp1 = this.initialSelectedEmp; // Reset selectedEmp1 to its initial value
  this.RoleId = this.initialRoleId; // Reset RoleId to its initial value
}
 
getOnBoardingById(e:any){
  this.IsEditeFlag=true
  this.conMasterId=e.conMasterId
  let url = this.baseurl + `/getContractorMasterById?conMasterId=${this.conMasterId}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
  this.httpSer.get(url, { headers }).subscribe((data:any) => {
    this.editdata=data;
    this.EditedDatas=this.editdata.result;
    this.ContractorCompanyName=this.EditedDatas.conCompanyName;
    this.ContractorName=this.EditedDatas.firstName;
    this.ContactNumber=this.EditedDatas.mobileNo;
    this.EmailId=this.EditedDatas.emailId;
    this.VendorCode=this.EditedDatas.vendorCode;
    this.serviceFromDate=this.EditedDatas.fromDate;
    this.serviceToDate=this.EditedDatas.toDate;
this.SelectServiceTypeList=this.EditedDatas.conServiceTypeIdList1;
this.compIds=this.EditedDatas.compId;

for(var i=0; i<this.SelectServiceTypeList.length;i++){
  if(this.SelectServiceTypeList.length){
  this.selectedEmp.push(this.SelectServiceTypeList[i]);
  }
}
this.SelectLocationList=this.EditedDatas.locationIdList1;
for(var i=0; i<this.SelectLocationList.length;i++){
  if(this.SelectLocationList.length){
  this.selectedEmp1.push(this.SelectLocationList[i]);
  }
}
document.getElementById('extra-apply-tab').click();



    },(err: HttpErrorResponse) => {
      this.Spinner.hide()
      this.toastr.error("Server Side Error..!");
    })
  }

deleteOnBoardingById(e:any){
  
  this.conMasterIds=e.conMasterId
  let url = this.baseurl + `/deleteContractorMasterById?conMasterId=${this.conMasterIds}`;
  const tokens = this.token;
  const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
  this.Spinner.show();
  this.httpSer.delete(url, { headers }).subscribe((data:any) => {
    this.toastr.success("contractor master delete successfully")
    this.Spinner.hide();
   this.GetAllContractorMasters()
    },(err: HttpErrorResponse) => {
      this.Spinner.hide()
      this.toastr.error("Server Side Error..!");
    })
  }

}
  
  
  

  
  
  


 

  

