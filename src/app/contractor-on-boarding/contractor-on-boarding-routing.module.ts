import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorOnBoardingComponent } from './contractor-on-boarding.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorOnBoardingComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorOnBoarding',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorOnBoarding',
                  component: ContractorOnBoardingComponent
              }

          ]

  }


]
export const routing = RouterModule.forChild(appRoutes);
