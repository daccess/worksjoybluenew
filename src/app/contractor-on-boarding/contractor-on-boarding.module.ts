import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './contractor-on-boarding-routing.module';
import { ContractorOnBoardingComponent } from './contractor-on-boarding.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  imports: [
    CommonModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    DataTablesModule,
  ],
  declarations: [
    ContractorOnBoardingComponent,
    
  ]
})
export class ContractorOnBoardingModule { }
