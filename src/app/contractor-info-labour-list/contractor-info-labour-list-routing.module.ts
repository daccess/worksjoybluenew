import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorInfoLabourListComponent } from './contractor-info-labour-list.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorInfoLabourListComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorinfolist',
                  pathMatch :'full'
                  
              },
              {
                  path:'contractorinfolist',
                  component: ContractorInfoLabourListComponent
              }

          ]

  }


]
export const contractorinfoRouting = RouterModule.forChild(appRoutes);

