import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contractor-info-labour-list',
  templateUrl: './contractor-info-labour-list.component.html',
  styleUrls: ['./contractor-info-labour-list.component.css']
})
export class ContractorInfoLabourListComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  LabourData: any;
  labourData: any;
  image:any;
  labourIds: any;
  labourlistEdit: string;
  locationdata: any;
  locationId: any;
  empids: any;
  searchLabour:any;
  selectUndefinedOptionValue:any;
  searchByLabour:any;
  searchByColumn:any;
  labourId: any;
  roleNames: any;
  supervisorEmpIds: any;

  constructor(public httpservice:HttpClient,public router:Router,public Spinner:NgxSpinnerService,public toastr: ToastrService) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId
    this.roleNames=this.users.roleList.roleName;
    this.supervisorEmpIds=this.users.roleList.contrctorEmpId
    this.getLabourData();
  }
  getLabourData() {
  
    let url = this.baseurl+`/getAllLabourInfo?empId=${this.empids}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.LabourData = data['result']
      setTimeout(function () {

        $(function () {

          var table = $('#emptable').DataTable({

            retrieve: true,
            searching:false

          })

        });
      }, 100);
      this.Spinner.hide();

    },
      (err: HttpErrorResponse) => {

      })
      if(this.roleNames=='Supervisor'){
        let url = this.baseurl+`/getAllLabourInfo?empId=${this.supervisorEmpIds}`;
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpservice.get(url, { headers }).subscribe(data => {
          
          this.LabourData = data['result']
          setTimeout(function () {
    
            $(function () {
    
              var table = $('#emptable').DataTable({
    
                retrieve: true,
                searching:false
    
              })
    
            });
          }, 100);
          this.Spinner.hide();
    
        },
          (err: HttpErrorResponse) => {
    
          })
      }

  }
  

}
