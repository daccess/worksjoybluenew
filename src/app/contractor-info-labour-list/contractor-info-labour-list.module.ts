import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {contractorinfoRouting } from './contractor-info-labour-list-routing.module';
import { ContractorInfoLabourListComponent } from './contractor-info-labour-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    contractorinfoRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ContractorInfoLabourListComponent
  ]
})
export class ContractorInfoLabourListModule { }
