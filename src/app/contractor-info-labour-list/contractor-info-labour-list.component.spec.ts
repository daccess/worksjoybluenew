import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorInfoLabourListComponent } from './contractor-info-labour-list.component';

describe('ContractorInfoLabourListComponent', () => {
  let component: ContractorInfoLabourListComponent;
  let fixture: ComponentFixture<ContractorInfoLabourListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorInfoLabourListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorInfoLabourListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
