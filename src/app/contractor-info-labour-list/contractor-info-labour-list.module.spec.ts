import { ContractorInfoLabourListModule } from './contractor-info-labour-list.module';

describe('ContractorInfoLabourListModule', () => {
  let contractorInfoLabourListModule: ContractorInfoLabourListModule;

  beforeEach(() => {
    contractorInfoLabourListModule = new ContractorInfoLabourListModule();
  });

  it('should create an instance', () => {
    expect(contractorInfoLabourListModule).toBeTruthy();
  });
});
