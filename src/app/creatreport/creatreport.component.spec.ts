import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatreportComponent } from './creatreport.component';

describe('CreatreportComponent', () => {
  let component: CreatreportComponent;
  let fixture: ComponentFixture<CreatreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
