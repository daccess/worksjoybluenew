import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from '../../app/login/login.component';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { CreatreportComponent } from 'src/app/creatreport/creatreport.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: CreatreportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'creatreport',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'creatreport',
                    component: CreatreportComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);