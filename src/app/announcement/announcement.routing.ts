import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { AnnouncementComponent } from 'src/app/announcement/announcement.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: AnnouncementComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'announcement',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'announcement',
                    component: AnnouncementComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);