import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ContractorregisterreportComponent } from './contractorregisterreport.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path: '', component: ContractorregisterreportComponent,
    children:[
      {
        path:'',
        redirectTo : 'contractorregisterreports',
        pathMatch :'full'
        
      }]
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [DatePipe],
  declarations: [ContractorregisterreportComponent]
})
export class ContractorregisterreportModule { }
