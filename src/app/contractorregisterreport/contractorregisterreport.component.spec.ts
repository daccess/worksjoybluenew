import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorregisterreportComponent } from './contractorregisterreport.component';

describe('ContractorregisterreportComponent', () => {
  let component: ContractorregisterreportComponent;
  let fixture: ComponentFixture<ContractorregisterreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorregisterreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorregisterreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
