import { Component, OnInit } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
@Component({
  selector: 'app-contractorregisterreport',
  templateUrl: './contractorregisterreport.component.html',
  styleUrls: ['./contractorregisterreport.component.css']
})
export class ContractorregisterreportComponent implements OnInit {
  // fromDate: string;
  toDate: string;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  report_data: any;
  marked_sr=true;
  marked_nature=true;
  marked_name=true;
  marked_address=true;
  marked_contract_type=true;
  marked_period_from=true;
  marked_period_to=true;
  marked_manpower=true;
  marked_licence_from=true;
  marked_licence_to=true;
  marked_wc=true;
  marked_epf=true;
  marked_esic=true;
  marked_gst=true;
  marked_contact=true;
  report_time: string;
  total_month_days: number;
constructor(public toastr: ToastrService, private datePipe: DatePipe,private InfoService:InfoService,private router: Router) {
    // this.fromDate = sessionStorage.getItem("fromDate");
    // this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName;
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.report_data = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
}

  ngOnInit() {
    for(let i=0;i<this.report_data.length;i++){
      this.report_data[i].Sr=i+1;
    }
  }
  toggle_sr(e) {
    this.marked_sr = e.target.checked;
  }
  toggle_emp_id(e) {
    this.marked_nature = e.target.checked;
  }
  toggle_name(e) {
    this.marked_name = e.target.checked;
  }
  toggle_address(e){
    this.marked_address = e.target.checked;
  }
  toggle_nature(e) {
    this.marked_nature = e.target.checked;
  }
  toggle_contract_type(e) {
    this.marked_contract_type = e.target.checked;
  }
  toggle_period_from(e) {
    this.marked_period_from = e.target.checked;
  }
  toggle_period_to(e) {
    this.marked_period_to = e.target.checked;
  }
  toggle_manpower(e) {
    this.marked_manpower = e.target.checked;
  }
  toggle_licence_from(e) {
    this.marked_licence_from = e.target.checked;
  }
  toggle_licence_to(e) {
    this.marked_licence_to = e.target.checked;
  }
  toggle_wc(e) {
    this.marked_wc = e.target.checked;
  }
  toggle_epf(e) {
    this.marked_epf = e.target.checked;
  }
  toggle_esic(e) {
    this.marked_esic = e.target.checked;
  }
  toggle_gst(e){
    this.marked_gst = e.target.checked;
  }
  toggle_contact(e){
    this.marked_contact=e.target.checked;
  }
  
//convert timestamp to date
TimestamptoDate(timestamp){
  var dateString = new Date(timestamp);
  let temp_Date=moment(dateString).format("YYYY-MM-DD");
  return temp_Date;
}
  //Excel Export
  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename='Contactor-Register-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  //pdf
  capturePdf() {
    let item_sr = {
      header: "Sr.No"
    }
    let name = {
      header: "Name Of The Contractor"
    }
    let address={
      header: "Address Of The Contractor"
    }
    let nature = {
      header: "Nature Of Work"
    }
    let contract_type = {
      header: "Contract Type"
    }
    let period_from = {
      header: "Contractor Period From"
    }
    let period_to = {
      header: "Contractor Period To"
    }
    let manpower = {
      header: "No Of Manpower"
    }
    let licence_from = {
      header: "Labour Licence From"
    }
    let licence_exp = {
      header: "Labour Licence Expiry Date"
    }
    let wc_exp = {
      header: "WC Policy Expiry Date"
    }
    let epf = {
      header: "EPF Registration No"
    }
    let esic = {
      header: "ESIC Registration No"
    }
    let gst = {
      header: "GST Registration No"
    }
    let contact={
      header:"Contact No"
    }
    var doc = new jsPDF('landscape', 'px', 'a4');
    // doc.addPage();
    var pageCount = doc.internal.getNumberOfPages();
    for (let i = 0; i < pageCount; i++) {
      doc.setPage(i);
      doc.setTextColor(48, 80, 139)
      doc.setFontSize(10)
      doc.text(580, 12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
    }
    var col = [];
    if (this.marked_sr) {
      col.push(item_sr);
    }
    if (this.marked_name) {
      col.push(name);
    }
    if (this.marked_address) {
      col.push(address);
    }
    if (this.marked_nature) {
      col.push(nature);
    }
    if (this.marked_contract_type) {
      col.push(contract_type);
    }
    if (this.marked_period_from) {
      col.push(period_from);
    }
    if (this.marked_period_to) {
      col.push(period_to);
    }
    if (this.marked_manpower) {
      col.push(manpower);
    }
    if (this.marked_licence_from) {
      col.push(licence_from);
    }
    if (this.marked_licence_to) {
      col.push(licence_exp);
    }
    if (this.marked_wc) {
      col.push(wc_exp);
    }
    if (this.marked_epf) {
      col.push(epf);
    }
    if (this.marked_esic) {
      col.push(esic);
    }
    if (this.marked_gst) {
      col.push(gst);
    }
    if (this.marked_contact) {
      col.push(contact);
    }
    var rows = [];
    var rowCountModNew = this.report_data;
    rowCountModNew.forEach(element => {
      let obj = []
      if (this.marked_sr) {
        obj.push(element.Sr);
      }
      if (this.marked_name) {
        obj.push(element.conFirmName);
      }
      if (this.marked_address) {
        obj.push(element.conAddress);
      }
      if (this.marked_nature) {
        obj.push(element.conSerTypeDesc);
      }
      if (this.marked_contract_type) {
        obj.push(element.conSerZTypeName);
      }
      if (this.marked_period_from) {
        obj.push(element.startDate);
      }
      if (this.marked_period_to) {
        obj.push(element.expDate);
      }
      if (this.marked_manpower) {
        obj.push(element.noOfManpwrApproved);
      }
      if (this.marked_licence_from) {
        obj.push(this.TimestamptoDate(element.labLicStartDate));
      }
      if (this.marked_licence_to) {
        obj.push(this.TimestamptoDate(element.labLicExpDate));
      }
      if (this.marked_wc) {
        obj.push(this.TimestamptoDate(element.wcExpDate));
      }
      if (this.marked_epf) {
        obj.push(element.epfRegNo);
      }
      if (this.marked_esic) {
        obj.push(element.esicNumber);
      }
      if (this.marked_gst) {
        obj.push(element.gstRegNo);
      }
      if (this.marked_contact) {
        obj.push(element.conContactNo);
      }
      rows.push(obj);
    });

    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14, 'Company Name :' + this.compName)

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12)
    doc.setFontStyle("Arial")
    doc.text(280, 14, 'Contractor Register Report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    // doc.text(263, 24, 'From date' + ' : ' + this.datePipe.transform(this.fromDate, "dd-MMM-yyyy") + ' ' + 'To date' + ' : ' + this.datePipe.transform(this.toDate, "dd-MMM-yyyy")
    // )

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44, 'Report date' + ' : ' + this.datePipe.transform(this.todaysDate, "dd-MMM-yyyy hh:mm:ss a"))

    doc.autoTable(col, rows, {
      columnStyles: {
        1: {halign:'left'},
        3: {halign:'left'},
        4: {halign:'left'},
        5: {halign:'left'},
      },
      tableLineColor: [190, 191, 191],
      tableLineWidth: 0.5,
      headStyles: {
        fillColor: [103, 132, 130],
      },
      styles: {
        halign: 'center',
        cellPadding: 0.5, fontSize: 6
      },
      theme: 'grid',
      pageBreak:'avoid',
      margin: { top: 60,  bottom: 50 }
    });
    doc.save('Contactor-Register-Report' + this.datePipe.transform(this.todaysDate, "dd-MM-yyyy hh:mm:ss") + '.pdf');
  }
}
