import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
// Services
import {RolePipe} from './pipes/role-pipe';
import {NumbersOnlyInputDirective} from './directives/numbersonly';
import {AlphabetOnlyDirective} from './directives/alphabet-only.directive';
import { InfoService } from "./services/infoService";
import { AlphanumericOnlyDirective } from "./directives/alphanumeric-only";
@NgModule({
	imports: [CommonModule],
	declarations: [
		// pipes
		RolePipe,
		//directive
		NumbersOnlyInputDirective,
		AlphabetOnlyDirective,
		AlphanumericOnlyDirective
	],
	exports: [
		// pipes
		RolePipe,
		//directive
		NumbersOnlyInputDirective,
		AlphabetOnlyDirective,
		AlphanumericOnlyDirective
	],
	providers: [InfoService]
})
export class SharedModule {
}