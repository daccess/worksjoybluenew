export class IPagination{
    originalArr: any[ ];
    tempArray2: any[ ];
    totalPages; 
    tempArray: any[];
    currentPage: number;
    rowsPerPage: number;
     
    initArray(arr):void{
      this.tempArray=arr;
    }
   
  fillTable(cPage):any[]{
   this.originalArr=this.tempArray;
   this.currentPage=cPage;
   
   let totalRows =this.originalArr.length;
 
   //this.rowsPerPage =RwPerPage;
 
   let remainder=totalRows%this.rowsPerPage;
 
   this.totalPages=(totalRows - remainder)/this.rowsPerPage;      
 
   if(remainder!=0) this.totalPages=this.totalPages + 1;
  
   let startIndex=(this.currentPage-1)*this.rowsPerPage;
   let endIndex=startIndex + this.rowsPerPage - 1; 
   
   
   if(this.currentPage!=this.totalPages) 
   {
    this.tempArray2=new Array(this.rowsPerPage);
   } 
 
   if(this.currentPage==this.totalPages) 
   {
     if(remainder!=0)
     {
       endIndex=startIndex + remainder - 1;
       this.tempArray2=new  Array(remainder);
     }
   }
  
    for(let j=0,i=startIndex; i<=endIndex; i++, j++){ 
       this.tempArray2[j]=this.originalArr[i];
    }
 
    this.originalArr=this.tempArray2;
    return this.originalArr;
 }
 
 }