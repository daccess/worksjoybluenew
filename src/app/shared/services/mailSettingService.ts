import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Logindata } from '../model/loginmodel'
import { MainURL } from '../configurl';

@Injectable()
export class MailSettingService {
    beha:any;
    baseurl = MainURL.HostUrl;
    loginResponse:any;
    constructor(private http : Http) { }

    emailConfiguration(body){
        let url = this.baseurl + '/emailConfiguration';
        var body1 = JSON.stringify(body);
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.post(url,body1,requestOptions).map(x => x.json()
        );
    }

    getEmailList(compId){
        let url = this.baseurl + '/emailConfiguration/byCompanyId/'+compId;
        // var body1 = JSON.stringify();
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.get(url,).map(x => x.json()
        );
    }

    getEmailbyid(id){
        let url = this.baseurl + '/emailConfiguration/'+id;
        // var body1 = JSON.stringify();
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.get(url).map(x => x.json()
        );
    }

    updateConfiguration(body){
        let url = this.baseurl + '/emailConfiguration';
        var body1 = JSON.stringify(body);
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.put(url,body).map(x => x.json()
        );
    }

    deleteEmailbyid(id){
        let url = this.baseurl + '/emailConfiguration/'+id;
        return this.http.delete(url).map(x => x.json()
        );
    }

    statusChangecall(compId,emailConfigId){
        let url = this.baseurl + '/emailConfiguration/statusChange?compId='+compId+'&emailConfigId='+emailConfigId
        console.log(url);
        
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.put(url,headerOptions).map(x => x.json()
        );
    }
}