import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ContractServiceTypeModel } from '../model/ContractorTypeServiceModel'
import { MainURL } from '../configurl';

@Injectable()
export class ContractorTypeMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }

   postContractorTypeMaster(Con : ContractServiceTypeModel){
    let url = this.baseurl + '/ConSerType';
    var body = JSON.stringify(Con);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
 
  
}