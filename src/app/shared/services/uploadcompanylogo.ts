import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MainURL } from '../configurl';

@Injectable()
export class UploadcompanylogoService {

  baseUrl = MainURL.HostUrl;

  constructor(private http : HttpClient) { }

  postcompanylogo(fileToUpload: File) {

    let url = this.baseUrl + '/fileHandaling';
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint, formData);
  }

}