import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs'

import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Logindata } from '../model/loginmodel'
import { MainURL } from '../configurl';


import {  EventEmitter } from '@angular/core';    
import { Subscription } from 'rxjs/internal/Subscription';  
@Injectable()
export class MessagingService {

  currentMessage = new BehaviorSubject(null);
  baseurl = MainURL.HostUrl;
  constructor(private http: Http,
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  /**
   * update token in firebase database
   * 
   * @param userId userId as a key 
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token
        this.angularFireDB.object('fcmTokens/').update(data)
      })
  }

  /**
   * request permission for notification from firebase cloud messaging
   * 
   * @param userId userId
   */
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.updateToken(userId, token);
        let user = sessionStorage.getItem('loggedUser');
        let loggedUser = JSON.parse(user);
        let obj = {
          token: token,
          empPerId: loggedUser.empPerId
        }
        console.log(obj);

        // this.postToken(obj).subscribe(data => {
        //   console.log("token save successfully", data);

        // }, err => {
        //   console.log("error save token", err);
        // })
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  postToken(obj) {
    let url = this.baseurl + '/Token';
    var body = JSON.stringify(obj);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post(url, body, requestOptions).map(x => x.json()

    );
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
        this.currentMessage.next(payload);
      })
  }

  deleteToken(id) {
    let url = this.baseurl + '/Token/' + id;
    return this.http.delete(url).map(x => x.json());
  }

  invokeFirstComponentFunction = new EventEmitter();    
  subsVar: Subscription;

  onFirstComponentButtonClick() {    
    this.invokeFirstComponentFunction.emit();    
  }    
}