import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../configurl';
@Injectable({
  providedIn: 'root'
})
export class CustomreportsService {

  reportFields = [];
  selectedReportFields = [];
  absentReportFields = [
    {
      field: 'Name',
      type : 'string',
      check: false
    },
    {
      field: 'EmpId',
      type : 'string',
      check: false
    },
    {
      field: 'Branch',
      type : 'string',
      check: false
    },
    {
      field: 'Department',
      type : 'string',
      check: false
    },
    {
      field: 'Category',
      type : 'string',
      check: false
    },
    {
      field: 'Absent Date',
      type : 'date',
      check: false
    },
    {
      field: 'Status',
      type : 'string',
      check: false
    }
  ];

  attendenceReportFields = [
    {field:'Name',type : 'string',check:false},{field:'Branch',type : 'string',check:false},{field:'Department',type : 'string',check:false},{field:'Category',type : 'string',check:false},{field:'Shift',type : 'string',check:false},{field:'Attendance Date',type : 'date',check:false},{field:'In-Time',type : 'time',check:false},{field:'Out-Time',type : 'time',check:false},{field:'Working Duration',type : 'number',check:false},{field:'Status Of The Day',type : 'string',check:false},{field:'Status',type : 'string',check:false}
  ]
  // 'string','date','number','date','time'
  stringOperators = ['Contains','Not Contains','Equals To','Not Equals'];
  dateOperators = ['Equals To','Greater Than','Less Than'];
  nuberOperators = ['Equals To','Greater Than','Less Than'];
  
  baseurl = MainURL.HostUrl;
  reportsData = [];
  constructor(private http : Http) {
  
  }

  seeReportData(model){
    let url = this.baseurl + '/customizeAbsentReport';
    var body = {customizeReportsConditionReqDtos:model};
    // var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
    }
}
