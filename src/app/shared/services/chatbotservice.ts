import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { chatmodeldata } from '../model/chatmodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class ChatService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postChatdata(chat){
    let url = this.baseurl + '/chat';
    var body = JSON.stringify(chat);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  addNewQuestion(chat){
    let url = this.baseurl + '/newQuestions';
    var body = JSON.stringify(chat);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

}