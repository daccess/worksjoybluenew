import { Injectable } from "@angular/core";
import { MainURL } from "../configurl";
import { Http, RequestOptions, RequestMethod, Headers } from "@angular/http";
import { BulkUpload } from "../model/BulkUploadModel";

@Injectable()
export class BulkUploadService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postHolidayExcel(excelData : BulkUpload){
    let url = this.baseurl + '/bulkHoliday';
    var body = excelData;
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  postLeaveBalExcel(excelData : BulkUpload){
    let url = this.baseurl + '/bulkLeaveBalanceUpload';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  postLeaveBalDeductionExcel(excelData : BulkUpload){
    let url = this.baseurl + '/bulkLeaveBalDeduction';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

   async postFile(fileToUpload) {
    let url = this.baseurl + '/uploadFile';
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    return await this.http.post(endpoint, formData).toPromise();
  }

  getallDocument() {
    let url = this.baseurl + '/documentByList';
    return this.http.get(url).map(x => x.json());;
  }
  
  postBulkDocuments(docs) {
    let url = this.baseurl + '/documentInsert';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postBulkLateEarly(docs) {
    let url = this.baseurl + '/lateEarlyUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postCompanyMaster(docs) {
    let url = this.baseurl + '/companyMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postBussiGroupMaster(docs) {
    let url = this.baseurl + '/bussinessGroupMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postFuncUnitMaster(docs) {
    let url = this.baseurl + '/functionUnitMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postDepartmentMaster(docs) {
    let url = this.baseurl + '/departmentMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postSubDeptMaster(docs) {
    let url = this.baseurl + '/subDepartmentMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postEmployeeCategoryMaster(docs) {
    let url = this.baseurl + '/employeeCategoryMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postSubEmpCategoryMaster(docs) {
    let url = this.baseurl + '/subEmployeeCategoryMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postPayGroupMaster(docs) {
    let url = this.baseurl + '/payGroupMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postGradeMaster(docs) {
    let url = this.baseurl + '/gradeMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postDesigMaster(docs) {
    let url = this.baseurl + '/designationMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postCostCenterMaster(docs) {
    let url = this.baseurl + '/costCenterMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }

  postLocationMaster(docs) {
    let url = this.baseurl + '/locationMasterBulkUpload';
    let body = JSON.stringify(docs);
    return this.http.post(url, docs).map(x => x.json());
  }


}