import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { CalenderMaster } from '../model/clendermastermodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class CalenderMasterService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postCalenderMaster(busim : CalenderMaster){
    let url = this.baseurl + '/WekklyOffCalender';
    var body = JSON.stringify(busim);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

}