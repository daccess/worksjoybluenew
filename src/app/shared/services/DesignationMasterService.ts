import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { EmpTypMaster } from'../model/EmplyoeeTypeMaster'
import { DesignationMaster } from '../model/DesignationMasterModel';
import { MainURL } from '../configurl';
@Injectable()
export class DesignationMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }
   postDesignationMaster(des : DesignationMaster){
    let url = this.baseurl + '/Designation';
    var body = JSON.stringify(des);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}