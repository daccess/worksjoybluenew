import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { departmentmodel } from '../model/departmentmastermodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class departmentMasterService {
//   selectedbusiness : businessmaster;
//   businessList : businessmaster[];
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postdepartmentMaster(deptm : departmentmodel,data:any){
    const token =data; // Replace with your actual JWT token
  const headers:any = new HttpHeaders({
    'Authorization': 'Bearer ' +token
  });
  let url = this.baseurl + '/getRoleFlowSetting';
    var body = JSON.stringify(deptm);
    
    return this.http.post(url,body,{headers});
  }
   getlabourIds(url:any,token:any){
  
    return this.http.get(url,token).map(x => x.json());
  }

  // deleteCompanyMaster(id: number) {
  //   return this.http.delete('http://localhost:28750/api/Employee/' + id).map(res => res.json());
  // }
}