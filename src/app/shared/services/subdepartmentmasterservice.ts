import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { subdepartmentmodel } from '../model/subdepartmentmastermodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class subdepartmentMasterService {
//   selectedbusiness : businessmaster;
//   businessList : businessmaster[];
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postsubdepartmentMaster(subdeptm : subdepartmentmodel){
    let url = this.baseurl + '/SubDepartment';
    var body = JSON.stringify(subdeptm);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  // deleteCompanyMaster(id: number) {
  //   return this.http.delete('http://localhost:28750/api/Employee/' + id).map(res => res.json());
  // }
}