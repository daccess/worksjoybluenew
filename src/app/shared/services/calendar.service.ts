import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  attendanceData = {
    daysInLastMonth : [],
    daysInThisMonth :[],
    daysInNextMonth :[]
    // januaryDays : [],
    // februaryDays : [],
    // marchDays : [],
    // aprilDays : [],
    // mayDays : [],
    // juneDays : [],
    // julyDays : [],
    // augustDays : [],
    // septemberDays : [],
    // octoberDays : [],
    // novemberDays : [],
    // decemberDays : []
  };
  monthAttendance = [];
  date: any;
  daysInLastMonth = [];
  HolidayData = [];
  daysInThisMonth = [];
  daysInNextMonth = [];
  currentYear: any;
  constructor() { }


 getCalendarAttendance(date , monthAttendance , HolidayData , currentYear){
   this.attendanceData = {
    daysInLastMonth : [],
    daysInThisMonth :[],
    daysInNextMonth :[]
  };
    this.monthAttendance = monthAttendance;
    this.date = date;
    this.HolidayData = HolidayData;
    this.currentYear = currentYear;
    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.attendanceData.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    let obj;

    for (let i = 0; i < thisNumOfDays; i++) {
      let item;
      let flag = false;
      for (let j = 0; j < this.monthAttendance.length; j++) {

        if (!this.monthAttendance[j].checkIn) {
          this.monthAttendance[j].checkIn = new Date(this.monthAttendance[j].checkIn).setHours(0, 0, 0);
        }
        if (!this.monthAttendance[j].checkOut) {
          this.monthAttendance[j].checkOut = new Date(this.monthAttendance[j].checkOut).setHours(0, 0, 0);
        }

        if (new Date(this.monthAttendance[j].attenDate).getDate() == i + 1) {
          flag = true;
          // console.log(new Date(this.monthAttendance[i].attenDate).getDate());
          if (this.monthAttendance[j].statusOfDay == 'WD') {

            if (this.monthAttendance[j].attendanceStatus == 'P' || this.monthAttendance[j].attendanceStatus == 'VP' || this.monthAttendance[j].attendanceStatus == 'HF') {

              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
            //  else if(this.monthAttendance[j].lateEarlyStatus=='LTEG'){
            //   item = {
            //     day: i + 1,
            //     status: "LT",
            //     date: this.monthAttendance[j].attenDate,
            //     checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
            //     checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
            //   }
            //   }
              else if (this.monthAttendance[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].leaveStatus,
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  // checkIn: this.monthAttendance[j].coffStatus + " Coff",
                  checkIn: this.monthAttendance[j].coffStatus,
                  checkOut: ""
                }
              }
              /*START of Staggered off*/ 
              else if (this.monthAttendance[j].soffStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].soffStatus,
                  checkOut: ""
                }
              }
              /*END of Staggered off*/
              else if (this.monthAttendance[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].odStatus
                }
              }
              else if (this.monthAttendance[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].wfhStatus
                }
              }

              else {
                let checkIn = "";
                let checkOut = "";
                if (this.monthAttendance[j].checkIn) {
                  checkIn = new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes();
                }
                if (this.monthAttendance[j].checkOut) {
                  checkOut = new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration);
                }
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: 'In ' + checkIn,
                  checkOut: ' Out ' + checkOut
                }
              }
            }
           else if (this.monthAttendance[j].attendanceStatus == 'A' && this.monthAttendance[j].lateEarlyStatus=='LTEG') {

              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "A",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
            }
           
          // else if(this.monthAttendance[j].attendanceStatus=='A' || this.monthAttendance[j].attendanceStatus == 'HF'){
          //        item = {
          //         day: i,
          //         status: "A",
          //         checkIn: "Absent",
          //         date: this.monthAttendance[j].attenDate
          //       }
          //   }
            else if (this.monthAttendance[j].attendanceStatus == 'HF' && !this.monthAttendance[j].wfhStatus) {
              item = {
                day: i + 1,
                date: this.monthAttendance[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'HF' && this.monthAttendance[j].wfhStatus) {
              item = {
                day: i + 1,
                status: "WFH",
                date: this.monthAttendance[j].attenDate,
                // checkIn: "Work From Home"
                checkIn: this.monthAttendance[j].wfhStatus
                
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.monthAttendance[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.monthAttendance[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.monthAttendance[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.monthAttendance[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              /*START of Staggered off*/
              else if (this.monthAttendance[j].soffStatus) {
                item = {
                  day: i + 1,
                  status: "SO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Soff",
                  checkOut: ""
                }
              }
              /*END of Staggered off*/
              else if (this.monthAttendance[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn:this.monthAttendance[j].leaveStatus,
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].wfhStatus) {
                item = {
                day: i + 1,
                status: "WFH",
                date: this.monthAttendance[j].attenDate,
                // checkIn: "Work From Home"
                checkIn: this.monthAttendance[j].wfhStatus
                }
                }
              else if (this.monthAttendance[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].wfhStatus
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.monthAttendance[j].attenDate
                }
              }

            }


          }

          else if (!this.monthAttendance[j].statusOfDay) {

            if (this.monthAttendance[j].attendanceStatus == 'P') {

              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].odStatus
                }
              }

              else {
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'HF') {
              item = {
                day: i + 1,
                date: this.monthAttendance[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.monthAttendance[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.monthAttendance[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.monthAttendance[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.monthAttendance[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].soffStatus) {
                item = {
                  day: i + 1,
                  status: "SO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Soff",
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].leaveStatus +"Leave",
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.monthAttendance[j].attenDate
                }
              }

            }


          }
          else {
            if (this.monthAttendance[j].statusOfDay == 'HO') {
              let holiDayName;
              for (let m = 0; m < this.HolidayData.length; m++) {

                if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.monthAttendance[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.monthAttendance[j].attenDate).getTime()) {
                  holiDayName = this.HolidayData[m].holiDayName;
                }

              }
              item = {
                day: i + 1,
                status: "HO",
                checkIn: holiDayName
              }
            } else {
              item = {
                day: i + 1,
                status: "WO",
                checkIn: "Week Off"
              }
            }

          }
          if(this.monthAttendance[j].statusOfDay == 'WD'){
            if (this.monthAttendance[j].attendanceStatus == 'HF' && this.monthAttendance[j].lateEarlyStatus=='LTEG') {

              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
            }
            else if(this.monthAttendance[j].attendanceStatus == 'HF' && this.monthAttendance[j].lateEarlyStatus=='EG'){
              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "EG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
            }

            else if(this.monthAttendance[j].attendanceStatus == 'A'){
              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.monthAttendance[j].attenDate,
                 
                }
              }
            }
          }

          this.attendanceData.daysInThisMonth.push(item);
        }
      }
      if (!flag) {

        let holiDayName;
        let holidayFlag: boolean = false;
        if (this.HolidayData) {
          for (let m = 0; m < this.HolidayData.length; m++) {
            holidayFlag = false;
            if (new Date(new Date(this.HolidayData[m].startDate).getFullYear(), new Date(this.HolidayData[m].startDate).getMonth(), new Date(this.HolidayData[m].startDate).getDate(), 0, 0, 0, 0).getTime() <= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime() && new Date(new Date(this.HolidayData[m].endtDate).getFullYear(), new Date(this.HolidayData[m].endtDate).getMonth(), new Date(this.HolidayData[m].endtDate).getDate(), 0, 0, 0, 0).getTime() >= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime()) {
              holiDayName = this.HolidayData[m].holiDayName;
              //console.log("holiday", item);
              holidayFlag = true;
              break;
            }
          }
        }

        if (holidayFlag) {
          item = {
            day: i + 1,
            status: "HO",
            checkIn: holiDayName
          }
        }
        else {
          item = {
            day: i + 1,
            status: "Ot"
          }
        }
        this.attendanceData.daysInThisMonth.push(item);

      }

    }
    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    let nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
    for (let i = 0; i < (6 - lastDayThisMonth); i++) {
      this.attendanceData.daysInNextMonth.push(i + 1);
    }
    console.log(this.attendanceData);
    
    return this.attendanceData;
  }

  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    var ohours;
    var ominutes;
    if (rhours < 10) {
      ohours = "0" + rhours;
    }
    else {
      ohours = rhours;
    }

    if (rminutes < 10) {
      ominutes = "0" + rminutes;
    }
    else {
      ominutes = rminutes;
    }
    return ohours + ":" + rminutes;
  }


  getYearlyCalendarAttendance(thisNumOfDays , month , monthAttendance, HolidayData){
    console.log(thisNumOfDays , month , monthAttendance);
    
    this.monthAttendance = monthAttendance;
    this.attendanceData = {
      daysInLastMonth : [],
      daysInThisMonth :[],
      daysInNextMonth :[]
    };
    this.HolidayData = HolidayData;
    for (let i = 1; i <= thisNumOfDays; i++) {
      let item;
      let flag = false;
      for (let j = 0; j < this.monthAttendance.length; j++) {

        if (!this.monthAttendance[j].checkIn) {
          this.monthAttendance[j].checkIn = new Date(this.monthAttendance[j].checkIn).setHours(0, 0, 0);
        }
        if (!this.monthAttendance[j].checkOut) {
          this.monthAttendance[j].checkOut = new Date(this.monthAttendance[j].checkOut).setHours(0, 0, 0);
        }

        if (new Date(this.monthAttendance[j].attenDate).getDate() == i && new Date(this.monthAttendance[j].attenDate).getMonth() + 1 == month) {
          flag = true;
          
          if (this.monthAttendance[j].statusOfDay == 'WD') {

            if (this.monthAttendance[j].attendanceStatus == 'P' || this.monthAttendance[j].attendanceStatus == 'VP') {

              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].leaveStatus) {
                item = {
                  day: i,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].leaveStatus,
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].coffStatus) {
                item = {
                  day: i,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].coffStatus + " Coff",
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].soffStatus) {
                item = {
                  day: i,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].soffStatus,
                  checkOut: ""
                }
              }
              else if (this.monthAttendance[j].odStatus) {
                item = {
                  day: i,
                  status: "OD",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "On Duty"
                }
              }
              else if (this.monthAttendance[j].wfhStatus) {
                item = {
                  day: i,
                  status: "WFH",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Work From Home"
                }
              }

              else {
                let checkIn = "";
                let checkOut = "";
                if (this.monthAttendance[j].checkIn) {
                  checkIn = new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes();
                }
                if (this.monthAttendance[j].checkOut) {
                  checkOut = new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration);
                }
                item = {
                  day: i,
                  status: "P",
                  checkIn: 'In ' + checkIn,
                  checkOut: ' Out ' + checkOut
                }
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'HF' && !this.monthAttendance[j].wfhStatus) {
              item = {
                day: i,
                date: this.monthAttendance[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'HF' && this.monthAttendance[j].wfhStatus) {
              item = {
                day: i,
                status: "WFH",
                date: this.monthAttendance[j].attenDate,
                checkIn: "Work From Home"
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.monthAttendance[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.monthAttendance[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.monthAttendance[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.monthAttendance[j].coffStatus) {
                item = {
                  day: i,
                  status: "CO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Coff",
                  checkOut: "Coff"
                }
              }
              /**START of Staggered off */
              else if (this.monthAttendance[j].soffStatus) {
                item = {
                  day: i,
                  status: "SO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Soff",
                  checkOut: "Soff"
                }
              }
              /**END of Staggered off */
              else if (this.monthAttendance[j].leaveStatus) {
                item = {
                  day: i,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].leaveStatus+"Leave",
                  checkOut: this.monthAttendance[j].leaveStatus+"Leave"
                }
              }
              else if (this.monthAttendance[j].wfhStatus) {
                item = {
                day: i,
                status: "WFH",
                date: this.monthAttendance[j].attenDate,
                checkIn: "Work From Home"
                }
                }
              else if (this.monthAttendance[j].lateEarlyStatus == "LT") {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "EG") {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].wfhStatus) {
                item = {
                  day: i,
                  status: "WFH",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Work From Home"
                }
              }
              else {
                item = {
                  day: i,
                  status: "A",
                  checkIn: "Absent",
                  date: this.monthAttendance[j].attenDate
                }
              }

            }


          }
          else if (!this.monthAttendance[j].statusOfDay) {

            if (this.monthAttendance[j].attendanceStatus == 'P') {

              if (this.monthAttendance[j].lateEarlyStatus) {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].odStatus) {
                item = {
                  day: i,
                  status: "OD",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "On Duty"
                }
              }
              else {
                item = {
                  day: i,
                  status: "P",
                  checkIn: "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'HF') {
              item = {
                day: i,
                date: this.monthAttendance[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
              }
            }
            else if (this.monthAttendance[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.monthAttendance[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.monthAttendance[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.monthAttendance[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.monthAttendance[j].coffStatus) {
                item = {
                  day: i,
                  status: "CO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Coff",
                  checkOut: "Coff"
                }
              }
              /**START of staggered off */
              else if (this.monthAttendance[j].soffStatus) {
                item = {
                  day: i,
                  status: "SO",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Soff",
                  checkOut: "Soff"
                }
              }
              /**END of staggered off */
              else if (this.monthAttendance[j].leaveStatus) {
                item = {
                  day: i,
                  status: "L",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: this.monthAttendance[j].leaveStatus +"Leave",
                  checkOut:this.monthAttendance[j].leaveStatus+ "Leave"
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "LT") {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else if (this.monthAttendance[j].lateEarlyStatus == "EG") {
                item = {
                  day: i,
                  status: "LT",
                  date: this.monthAttendance[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.monthAttendance[j].checkIn).getHours() + ":" + new Date(this.monthAttendance[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.monthAttendance[j].checkOut).getHours() + ":" + new Date(this.monthAttendance[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.monthAttendance[j].workDuration)
                }
              }
              else {
                item = {
                  day: i,
                  status: "A",
                  checkIn: "Absent",
                  date: this.monthAttendance[j].attenDate
                }
              }

            }


          }

          else {
            if (this.monthAttendance[j].statusOfDay == 'HO') {
              let holiDayName;
              for (let m = 0; m < this.HolidayData.length; m++) {
                // console.log("HolidayData[]",this.HolidayData);
                if (new Date(this.HolidayData[m].startDate).getTime() == new Date(this.monthAttendance[j].attenDate).getTime()) {
                  holiDayName = this.HolidayData[m].holiDayName;
                  // console.log("holiDayName",holiDayName);

                }

              }
              item = {
                day: i,
                status: "HO",
                checkIn: holiDayName
              }
            }

            else
              item = {
                day: i,
                status: "WO"
              }
          }

          this.attendanceData.daysInThisMonth.push(item);

          // if(month == 1){
          //   this.attendanceData.januaryDays.push(item);
          // }
          // if(month == 2){
          //   this.attendanceData.februaryDays.push(item);
          // }
          // if(month == 3){
          //   this.attendanceData.marchDays.push(item);
          // }
          // if(month == 4){
          //   this.attendanceData.aprilDays.push(item);
          // }
          // if(month == 5){
          //   this.attendanceData.mayDays.push(item);
          // }
          // if(month == 6){
          //   this.attendanceData.juneDays.push(item);
          // }
          // if(month == 7){
          //   this.attendanceData.julyDays.push(item);
          // }
          // if(month == 8){
          //   this.attendanceData.augustDays.push(item);
          // }
          // if(month == 9){
          //   this.attendanceData.septemberDays.push(item);
          // }
          // if(month == 10){
          //   this.attendanceData.octoberDays.push(item);
          // }
          // if(month == 11){
          //   this.attendanceData.novemberDays.push(item);
          // }
          // if(month == 12){
          //   this.attendanceData.decemberDays.push(item);
          // }
        }
      }

      if (!flag) {

        var newDate = new Date(this.currentYear, 0, i)

        let hObj;
        if (newDate.getDay() == 0) {   //if Sunday
          hObj = {
            day: i,
            status: "Ot"
          }
        }
        else if (newDate.getDay() == 6) {   //if Saturday
          hObj = {
            day: i,
            status: "Ot"
          }
        }
        else {
          hObj = {
            day: i,
            status: "Ot"
          }
        }


        this.attendanceData.daysInThisMonth.push(hObj);
        // if(month == 1){
        //   this.attendanceData.januaryDays.push(hObj);
        // }
        // if(month == 2){
        //   this.attendanceData.februaryDays.push(hObj);
        // }
        // if(month == 3){
        //   this.attendanceData.marchDays.push(hObj);
        // }
        // if(month == 4){
        //   this.attendanceData.aprilDays.push(hObj);
        // }
        // if(month == 5){
        //   this.attendanceData.mayDays.push(hObj);
        // }
        // if(month == 6){
        //   this.attendanceData.juneDays.push(hObj);
        // }
        // if(month == 7){
        //   this.attendanceData.julyDays.push(hObj);
        // }
        // if(month == 8){
        //   this.attendanceData.augustDays.push(hObj);
        // }
        // if(month == 9){
        //   this.attendanceData.septemberDays.push(hObj);
        // }
        // if(month == 10){
        //   this.attendanceData.octoberDays.push(hObj);
        // }
        // if(month == 11){
        //   this.attendanceData.novemberDays.push(hObj);
        // }
        // if(month == 12){
        //   this.attendanceData.decemberDays.push(hObj);
        // }
      }
    }
    return this.attendanceData;
  }
}
