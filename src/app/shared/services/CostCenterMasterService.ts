import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {CostCenterMaster} from '../model/CostCenterModel';

// import { GradeMaster } from'../model/GradeMasterModel'
import { MainURL } from '../configurl';
@Injectable()
export class CostCenterMasterService {
//   selectedcomp : g;
//   commasterList : commaster[];
baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postCostCenterMaster(csm : CostCenterMaster){
    let url = this.baseurl + '/CostCenter';
    var body = JSON.stringify(csm);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  // deleteCompanyMaster(id: number) {
  //   return this.http.delete('http://localhost:28750/api/Employee/' + id).map(res => res.json());
  // }
}