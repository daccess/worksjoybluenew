
import { Router } from '@angular/router';
import { Injectable, NgZone } from '@angular/core';
import { Subject, Subscription, timer } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

@Injectable()



export class SessionService {
    logindata:any=[];
    LoginData:any=[];

    
    constructor(
        private router: Router,
        private ngZone: NgZone,
        private authService: AuthService){ 
    
            // this.initInterval()
    }
    Save(logindata){
        sessionStorage.setItem('loggedUser', JSON.stringify(logindata));
        
    }

    
    // ============Auto Logout Starts ======================


    get lastAction() {
        return parseInt(sessionStorage.getItem(STORE_KEY));
      }
      set lastAction(value) {
        sessionStorage.setItem(STORE_KEY, value.toString());
      }

    initListener() {
        document.body.addEventListener('click', () => this.reset());
      }
    
      reset() {
        this.lastAction = Date.now();
      }
    
      initInterval() {
        setInterval(() => {
          this.check();
        }, CHECK_INTERVALL);
      }
    
      check() {
        const now = Date.now();
        const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
        const diff = timeleft - now;
        const isTimeout = diff < 0;
    
        this.ngZone.run(() => {
            let user = sessionStorage.getItem('loggedUser');
            let logged = JSON.parse(user);
            if (isTimeout && logged) {
                sessionStorage.clear();
              this.router.navigateByUrl("#");
            }
        });
      }  
    // ============Auto Logout Ends ======================

    // ============New Auto Logout Starts ================

    minutesDisplay = 0;
    secondsDisplay = 0;
  
    endTime = 50;
  
    unsubscribe$: Subject<void> = new Subject();
    timerSubscription: Subscription;
    
    ngOnInit12() {
      this.resetTimer();
      this.authService.userActionOccured.pipe(
        takeUntil(this.unsubscribe$)
      ).subscribe(() => {
        if (this.timerSubscription) {
          this.timerSubscription.unsubscribe();
          
        }
        this.resetTimer();
      });
    }

    ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }
  
    resetTimer(endTime: number = this.endTime) {
      const interval = 1000;
      const duration = endTime * 60;
      this.timerSubscription = timer(0, interval).pipe(
        take(duration)
      ).subscribe(value =>
        this.render((duration - +value) * interval),
        err => { },
        () => {
          // this.authService.logOutUser();
           sessionStorage.clear();
           this.router.navigateByUrl("");
           
        }
      )
    }
  
    private render(count) {
      this.secondsDisplay = this.getSeconds(count);
      this.minutesDisplay = this.getMinutes(count);
    }
  
    private getSeconds(ticks: number) {
      const seconds = ((ticks % 60000) / 1000).toFixed(0);
      return this.pad(seconds);
    }
  
    private getMinutes(ticks: number) {
      const minutes = Math.floor(ticks / 60000);
      return this.pad(minutes);
    }
  
    private pad(digit: any) {
      return digit <= 9 ? '0' + digit : digit;
    }


    // ============New Auto Logout Starts ================
}
const MINUTES_UNITL_AUTO_LOGOUT = 1 // in Minutes
const CHECK_INTERVALL = 100 // in ms
const STORE_KEY = 'lastAction';