import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { PayRoleMaster } from '../model/PayRoleHeadmodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class PayRoleMasterService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postPayRolMaster(PayRole : PayRoleMaster){
    let url = this.baseurl + '/PayRollHead';
    var body = JSON.stringify(PayRole);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

}