import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { EmpTypMaster } from'../model/EmplyoeeTypeMaster'
import { MainURL } from '../configurl';

@Injectable()
export class EmployeeTypeMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }

   postEmployeeTypeMaster(Pay : EmpTypMaster){
    let url = this.baseurl + '/EmployeeType';
    var body = JSON.stringify(Pay);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  // getAllEmployeeType(empId){
  //   return this.http.get("http://localhost:8051/EmployeeType/"+empId).map(x=>x.json());
  // }
  // updateEmployeeInfo(emp){
  //   console.log("e",emp)
  //   let model={
      
  //   }
  //   return this.http.put("http://localhost:8051/EmployeeType",emp).map(x=>x.json());
  // }

  // deleteCompanyMaster(id: number) {
  //   return this.http.delete('http://localhost:28750/api/Employee/' + id).map(res => res.json());
  // }
}