import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpEvent,HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { MainURL } from '../../shared/configurl';
import { FilePreviewModel, FilePickerAdapter } from 'ngx-awesome-uploader';
import { OndutyService } from 'src/app/otherrequests/shared/services/ondutyservice';
@Injectable({
  providedIn: 'root'
})
export class dataService extends FilePickerAdapter {
    baseUrl = MainURL.HostUrl;
    Aws_flag=MainURL.Aws_flag;
    uploadInput = new EventEmitter<any>();
    inviteVisitorsUrl = MainURL.InviteVisitorUrl;
    constructor(private httpService:HttpClient) {
        super();
    }

    getRecordsByid(url,id):Observable<any>
    {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        let temp_url=url+'/'+id;
        return this.httpService.get<any>(this.baseUrl+'/'+temp_url, { headers: httpHeaders});
    }
    isLoggedin()
    {
        let token=localStorage.getItem("token");
        return token !==null;
    }
    getRecords(url:string,params:any):Observable<any>
    {
        return this.httpService.get(`${this.baseUrl+url}`,{params:params});
    }

    getRecords1(url:string,params:any):Observable<any>
    {
        return this.httpService.get(`${this.baseUrl+url}`,{params:params});
    }
    getRecordsNoFilter(url: string): Observable<any>
    {
        return this.httpService.get(`${this.baseUrl + url}`);
    }
    postRecord(url:string,id:number,data:any):Observable<any>
    {
        return this.httpService.post<any>(this.baseUrl+url+'/'+id,data);
    }

    updateRecord(url:string,id:number,data:any):Observable<any>
    {
        return this.httpService.put<any>(this.baseUrl+url+'/'+id,data);
    }
    updateRecords(url:string,data:any):Observable<any>
    {
        return this.httpService.put<any>(this.baseUrl+url,data);
    }

    deleteRecord(url:string,id:number):Observable<any>
    {
        return this.httpService.delete<any>(this.baseUrl+url+'/'+id);
    }
    createRecord(url:string,data:any):Observable<any>
    {
        return this.httpService.post<any>(this.baseUrl+url,data);
    }
    createRecord1(url:string,data:any):Observable<any>
    {
        return this.httpService.get<any>(this.baseUrl+url,data);
    }
    createRecors(url:string):Observable<any>
    {
        return this.httpService.get<any>(this.baseUrl+url);
    }


    //file upload
    postFile(fileToUpload: File,name): Observable<boolean> {
        let endpoint = this.baseUrl+'/uploadFile';
        if(this.Aws_flag!='true'){
            endpoint=this.baseUrl + '/fileHandaling';
        }
        const formData: FormData = new FormData();
        formData.append('uploadfile', fileToUpload, fileToUpload.name);
        return this.httpService.post<any>(endpoint, formData);
    }
    //awsome uploader
    public uploadFile(fileItem: FilePreviewModel) {
        let endpoint = this.baseUrl+'/uploadFile';
        if(this.Aws_flag!='true'){
            endpoint=this.baseUrl + '/fileHandaling';
        }
        const formData: FormData = new FormData();
        formData.append('uploadfile', fileItem.file);
        return this.httpService.post<any>(endpoint, formData);
    }
    public removeFile(fileItem): Observable<any> {
        this.uploadInput.emit({ type: 'cancel', id: fileItem.id });
        return this.uploadInput;
      }

      deleteRecords(url: string,StaggeredOffReqAndApproveId: any,sOffCreditId:any):Observable<any>{
        let params=new HttpParams();
        if(StaggeredOffReqAndApproveId){
			params=params.append('StaggeredOffReqAndApproveId',StaggeredOffReqAndApproveId);
		}
		if(sOffCreditId){
			params=params.append('sOffCreditId',sOffCreditId);
		}
          return this.httpService.delete<any>(this.baseUrl+url,{params:params});
      }
    /**START of Invite Visitors Service */
    getInviteVisitorsRecordsNoFilter(url: string): Observable<any>
    {
        return this.httpService.get(`${this.inviteVisitorsUrl + url}`);
    }
    createInviteVisitorsRecord(url:string,data:any):Observable<any>
    {
        return this.httpService.post<any>(this.inviteVisitorsUrl+url,data);
    }
    getInviteVisitorsRecords1(url:string,Command: string,UserId: string,VisiId: string):Observable<any>
    {
        let params = new HttpParams();
        params=params.set('Command', Command);
        params=params.set('UserId', UserId);
        params=params.set('VisiId', VisiId);
        return this.httpService.get(this.inviteVisitorsUrl+url,{params:params});
    }
    checkMobileNumber(url:string,MobileNumber: string):Observable<any>
    {
        let params = new HttpParams();
        params=params.set('MobileNumber', MobileNumber);
        return this.httpService.get(this.inviteVisitorsUrl+url,{params:params});
    }

    /**END of Invite Visitors Service */

    getPurchaseOrdersByContrAndFunctionUnit(url,contractorId, functionUnitId){
        let data = contractorId + "/" + functionUnitId;
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        let temp_url=url+'/'+data;
        return this.httpService.get<any>(this.baseUrl+'/'+temp_url, { headers: httpHeaders});
    }

    getRecordByIds(url,contractorId, functionUnitId){
        let data = contractorId + "/" + functionUnitId;
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        let temp_url=url+'/'+data;
        return this.httpService.get<any>(this.baseUrl+'/'+temp_url, { headers: httpHeaders});
    }
    getallempOfSectionAuthorityTrue(url:any,depId,locationId):Observable<any>{
      
        // return this.httpService.get<any>(this.baseUrl+url) 
            const httpHeaders = new HttpHeaders();
            httpHeaders.set('Content-Type', 'application/json');
            let temp_url=url+'/'+depId+'/'+locationId+'/';
            return this.httpService.get<any>(this.baseUrl+'/'+temp_url, { headers: httpHeaders});
        }
        getallemplist(data:any):Observable<any>{
            let url = this.baseUrl + '/EmpOfficialApproverChanges';
            return this.httpService.post<any>(url,data)
        }
        getallempRecord(data:any):Observable<any>{
            let url = this.baseUrl + '/EmpOfficialApproverChange/';
            return this.httpService.get<any>(url+data)
        }
        changedApprovals(url:any,data:any):Observable<any>{
            return this.httpService.put<any>(this.baseUrl+url,data)

        }
        SaveApprovalChange(url:any,data:any):Observable<any>{
            return this.httpService.post<any>(this.baseUrl+url,data)

        }

    getRecordByCompLocIds(url,compId, locationId){
        let data = compId + "/" + locationId;
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        let temp_url=url+'/'+data;
        return this.httpService.get<any>(this.baseUrl+'/'+temp_url, { headers: httpHeaders});
    }
    
    
    getRecordByComp(url:any,data:any):Observable<any>{
        
        return this.httpService.post(this.baseUrl+'/'+url+'/',data)
    }
     getRecordByLocation(url:any,data:any):Observable<any>{
    
        return this.httpService.post(this.baseUrl+'/'+url+'/',data)
    }

  }