import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {GradeMaster} from '../model/GradeMasterModel';
import { MainURL } from '../configurl';
// import { GradeMaster } from'../model/GradeMasterModel'

@Injectable()
export class GradeMasterService {
//   selectedcomp : g;
//   commasterList : commaster[];
baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postGradeMaster(Grd : GradeMaster){
    let url = this.baseurl + '/GradeMaster';
    var body = JSON.stringify(Grd);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  // deleteCompanyMaster(id: number) {
  //   return this.http.delete('http://localhost:28750/api/Employee/' + id).map(res => res.json());
  // }
}