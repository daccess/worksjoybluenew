import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { SubEmpTypMaster } from '../model/SubEmpTypModel';
import { MainURL } from '../configurl';
@Injectable()
export class SubEmpTypMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }

  postSubEmpTypeMaster(Sub : SubEmpTypMaster){
    let url = this.baseurl + '/SubEmployeeType';
    var body = JSON.stringify(Sub);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}