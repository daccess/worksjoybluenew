import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Logindata } from '../model/loginmodel'
import { MainURL } from '../configurl';

@Injectable()
export class LoginService {
  
  beha:any;
  baseurl = MainURL.HostUrl;
  loginResponse:any;
   constructor(private http : Http) { }
   postloginservice(login : Logindata){
    let url = this.baseurl + '/Login';
    var body = JSON.stringify(login);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json()
    );
  }
}