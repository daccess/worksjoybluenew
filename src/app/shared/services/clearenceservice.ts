import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { chatmodeldata } from '../model/chatmodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable() 
export class ClearenceService {
  baseurl = MainURL.HostUrl

  // public string : Long;
  
  constructor(private http : Http) { }
  
 
  getClearenceList(body){
    let url = this.baseurl + '/clearanceClrOrHold';
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
}

getClearenceListNew(id){
  let url = this.baseurl + '/seperationSetting/getEmployeeSeperationList/'+id;
  return this.http.get(url).map(x => x.json());
}
  clearApprove(body){
    var data = JSON.stringify(body);
   // console.log(data);
    
    let url = this.baseurl + '/updateClearanceToClearStatus';
    return this.http.put(url,body).map(x => x.json());
  }

  holdClearance(body){    
    var data = JSON.stringify(body);
   // console.log(data);    
    let url = this.baseurl + '/updateClearanceToHoldStatus';
    return this.http.put(url,body).map(x => x.json());
  }

  clearanceDetails(body){
    let url = this.baseurl + '/clearanceDetails';
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getClearanceListHR(body){
    
    let url = this.baseurl + '/clearanceDetailsForHr';
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getAllSanctioningEmployee(){
    let url = this.baseurl+'/allSanctioningEmployee';
   
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
}