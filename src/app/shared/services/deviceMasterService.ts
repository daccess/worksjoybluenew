import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { DeviceMaster } from '../model/deviceMasterModel';
import { MainURL } from '../configurl';
@Injectable()
export class DeviceMasterService {
// selectedcomp : commaster;
// commasterList : commaster[];
baseurl = MainURL.HostUrl
constructor(private http : Http) { }
postDeviceMaster(des : DeviceMaster){
let url = this.baseurl +'/Device';
var body = JSON.stringify(des);
var headerOptions = new Headers({'Content-Type':'application/json'});
var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
return this.http.post(url,body,requestOptions).map(x => x.json());
}


getAssertMasterServiceList()
{
    //let url = this.baseurl +'/getAssetList';
    return this.http.get(this.baseurl+"/getAssetMasterList");
}

postServices(body):Observable<any>
{
  let data = JSON.stringify(body);
  var headerOptions = new Headers({'Content-Type':'application/json'});
  var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
 return this.http.post(this.baseurl + "/createAssetMaster",data,requestOptions);
}

putServices(body):Observable<any>
  {
    let data = JSON.stringify(body);
   var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Put,headers : headerOptions});
   return this.http.put(this.baseurl + "/updateAssetMaster",data,requestOptions);
  }


  getServicesById(i):Observable<any>
  {
    
   return this.http.get(this.baseurl + "/getAssetMasterListById/"+i);
  }


deleteServices(i):Observable<any>
  {
    console.log("get delete");
//     var headerOptions = new Headers({'Content-Type':'application/json'});
//   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
   return this.http.delete(this.baseurl + "/deleteAssetMaster/"+i);
  }
  getLocations(compId){
    return this.http.get(this.baseurl + '/Location/Active/'+compId);
  }

}