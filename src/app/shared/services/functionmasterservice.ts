import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { functionmodel } from '../model/functionmastermodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class FunctionMasterService {
//   selectedbusiness : businessmaster;
//   businessList : businessmaster[];
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postFunctionMaster(funcm : functionmodel){
    let url = this.baseurl + '/FunctionUnitMaster';
    var body = JSON.stringify(funcm);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  // deleteCompanyMaster(id: number) {
  //   return this.http.delete('http://localhost:28750/api/Employee/' + id).map(res => res.json());
  // }
}