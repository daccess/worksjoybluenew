import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { DeviceMaster } from '../model/deviceMasterModel';
import { MainURL } from '../configurl';
@Injectable()
export class DeviceMasterService {
// selectedcomp : commaster;
// commasterList : commaster[];
baseurl = MainURL.HostUrl
constructor(private http : Http) { }
postDeviceMaster(des : DeviceMaster){
let url = this.baseurl +'/Device';
var body = JSON.stringify(des);
var headerOptions = new Headers({'Content-Type':'application/json'});
var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
return this.http.post(url,body,requestOptions).map(x => x.json());
}
}