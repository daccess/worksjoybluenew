import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { commaster } from'../model/companymastermodel'
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { MainURL } from '../../shared/configurl';
import { HttpHeaders } from '@angular/common/http';


@Injectable()
export class CompanyMasterService {
  baseurl = MainURL.HostUrl;
  leavesData : any =[];
  @Output() fire: EventEmitter<any> = new EventEmitter();

  selectedcomp : commaster;
  commasterList : commaster[];
  constructor(private http : Http) { }

  postComopanyMaster(cmp : commaster,tokens){
 
    let url = this.baseurl + '/createCompany';
    const token =tokens; // Replace with your actual JWT token
  const headers:any = new HttpHeaders({
    "Content-Type": "application/json",
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer ' +token
  });
  var body = JSON.stringify(cmp);

  return this.http.post(url,body,{headers});
    // var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
 
    
  }
  
  GetAllCompanyData(data:any): Observable<any> {
  
    let url = this.baseurl + '/getAllCompany';
    const token =data; // Replace with your actual JWT token
  
    const headers:any = new HttpHeaders({
    //   "Content-Type": "application/json",
    // 'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer ' +token
    });
  
    return this.http.get(url, { headers });
  }

  show() {
     this.fire.emit(false);
   }
   hide() {
     this.fire.emit(true);
   }

   getEmittedValue() {
     return this.fire;
   }

}