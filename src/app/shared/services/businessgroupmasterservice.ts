import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { businessmaster } from '../model/businessgroupmastermodel';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class BusinessMasterService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postBusinessMaster(busim : businessmaster){
    let url = this.baseurl + '/BusinessGroup';
    var body = JSON.stringify(busim);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

}