import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HolidayMaster } from'../model/holidayMasterModel'
import { MainURL } from '../configurl';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HolidayMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
  user: string;
  users: any;
  token: any;
   constructor(private http : Http) {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token;
    }

//    postHolidaYMaster(Hol : HolidayMaster,){
//     debugger
//     const tokens = this.token;
// const header:any = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
//     let url = this.baseurl + '/Holiday';
//     var body = JSON.stringify(Hol);
//     var headerOptions = new Headers({'Content-Type':'application/json'});
//     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions,});
//     return this.http.post(url,body).map(x => x.json());
//   }
  postHolidaYMaster(Hol: HolidayMaster): Observable<any> {
    const token = this.token; // Replace with your method to get the token
    const headers:any = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    });

    // const url = this.baseurl + '/Holiday';
    let url = this.baseurl + '/Holiday';
    return this.http.post(url, Hol, { headers: headers }).map(x => x.json());
  }

  private getToken(): string {
    // Implement your logic to retrieve the token here
    return 'your_token_here';
  }
}
