import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { ShiftMaster } from '../model/shiftmaster'
import { MainURL } from '../configurl';
import { from } from 'rxjs';

@Injectable()
export class shiftMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
    baseurl = MainURL.HostUrl
    constructor(private http : Http) { }


    postShiftMaster(shiftm : ShiftMaster){
    let url = this.baseurl + '/ShiftMaster';
    var body = JSON.stringify(shiftm);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  
  getAllShiftMaster(id){
    let url = this.baseurl + '/ShiftMaster/'+id;
    // var body = JSON.stringify(shiftm);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  getShiftById(id){
    let url = this.baseurl + '/shiftMasterById/'+id;
    // var body = JSON.stringify(shiftm);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
}