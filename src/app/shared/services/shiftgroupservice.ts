import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { ShiftGroupMaster } from '../model/shiftgroupmodel'
import { MainURL } from '../configurl';
import { from } from 'rxjs';

@Injectable()
export class shiftGroupMasterService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }
   postShiftMaster(shift : ShiftGroupMaster){
    let url = this.baseurl + '/ShiftGroup';
    var body = JSON.stringify(shift);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}