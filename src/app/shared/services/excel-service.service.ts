import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs';
import { excelimport } from '../model/excelimport.model';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MainURL } from '../../shared/configurl';
import { employeeexcelimport } from '../model/employeeexclimport';

@Injectable({
  providedIn: 'root'
})
export class ExcelServiceService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postExcel(excelData : excelimport){
    let url = this.baseurl + '/InsertEmployeeDetails';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  postExcels(excelData : employeeexcelimport){
    let url = this.baseurl + '/InsertEmployeeDetails';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  postContractExcels(excelData : employeeexcelimport){
    let url = this.baseurl + '/insertContractorDetails';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  postShiftUploadExcel(excelData : excelimport){
    let url = this.baseurl + '/printShift';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  postWeeklyOffExcel(excelData : excelimport){
    let url = this.baseurl + '/woUpload';
    var body = (excelData);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}
