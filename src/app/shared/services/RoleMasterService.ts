import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { RoleMaster } from '../model/RoleMasterModel';
import {HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RoleMasterService {
  baseurl = MainURL.HostUrl;
  constructor(private http : HttpClient) {

   }

  // postRoleMaster(Role : RoleMaster){
  //   let url = this.baseurl + '/RollMaster';
  //   var body = JSON.stringify(Role);
  //   var headerOptions = new Headers({'Content-Type':'application/json'});
  //   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  //   return this.http.post(url,body,requestOptions).map(x => x.json());
  // }
getactive(b){
  // console.log("dskfjksjd", val)
  // var body = JSON.stringify(Role);
  // var headerOptions = new Headers({'Content-Type':'application/json'});
  // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  // return this.http.get(b).map(x => x.json());
}

GetAllRole(data:any): Observable<any> {
  
  let url = this.baseurl + '/getAllRoles';
  const token =data; // Replace with your actual JWT token

  const headers:any = new HttpHeaders({
    "Content-Type": "application/json",
  'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer ' +token
  });

  return this.http.get(url, { headers });
}
GetAllRoless(data:any): Observable<any> {
  
  let url = this.baseurl + '/getAllRolesAddStaff';
  const token =data; // Replace with your actual JWT token

  const headers:any = new HttpHeaders({
    "Content-Type": "application/json",
  'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer ' +token
  });

  return this.http.get(url, { headers });
}
saveRolemaster(data,tokens){
  let url = this.baseurl + '/createRoleFlowSetting';
  const token =tokens; // Replace with your actual JWT token
  const headers:any = new HttpHeaders({
    "Content-Type": "application/json",
  'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer ' +token
  });

  return this.http.post(url,data, { headers });
}
updateRolemaster(data,tokens){
  let url = this.baseurl + '/updateRoleFlowSetting';
  const token =tokens; // Replace with your actual JWT token
  const headers:any = new HttpHeaders({
    'Authorization': 'Bearer ' +token
  });

  return this.http.put(url,data, { headers });
}
GetAllRoles(data:any): Observable<any> {
  
  let url = this.baseurl + '/getRoleFlowSetting';
  const token =data; // Replace with your actual JWT token

  const headers:any = new HttpHeaders({
  //   "Content-Type": "application/json",
  // 'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer ' +token
  });

  return this.http.get(url, { headers });
}
GetAllRoles1(data:any): Observable<any> {
  
  let url = this.baseurl + '/getAllRoles';
  const token =data; // Replace with your actual JWT token

  const headers:any = new HttpHeaders({
  //   "Content-Type": "application/json",
  // 'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer ' +token
  });

  return this.http.get(url, { headers });
}


 // Function to get all contractor master data
 GetAllContractorMaster(data: any): Observable<any> {
  const url = this.baseurl + '/getAllContractorMaster';
  const token = data; // Replace with your actual JWT token

  const headers: HttpHeaders = new HttpHeaders({
    'Authorization': 'Bearer ' + token
  });

  return this.http.get(url, { headers });
}


}