import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { EmpDashModel } from '../model/EmpDashboardModel';
import { NewsModel } from '../model/EmpNews'
import { MainURL } from '../configurl';
import { PayModel } from '../model/PayModel'
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class EmpDashSerivcService {
    // selectedcomp : commaster;
    // commasterList : commaster[];
    baseurl = MainURL.HostUrl;
    Aws_flag=MainURL.Aws_flag;
    loggedUser: any;
    compId: any;
    labourMasterId:any
    empOfficialId: any;
    empPerId: any;
    filesToUpload: any;
    empId:any;
    locationId:any;
    roleId:any;
    user: string;
    users: any;
    token: any;
    empids: any;
    labourIds: string;
    labourmasterId: string;


    constructor(private http: Http) {
        // let user = sessionStorage.getItem('loggedUser');
        // console.log('heree is user data',this.loggedUser)
        // this.loggedUser = JSON.parse(user);
       
        // this.compId = this.loggedUser.compId
        // this.labourMasterId = this.loggedUser.labourMasterId

        // this.empPerId = this.loggedUser.empPerId

        this.user = sessionStorage.getItem('loggeduser')
        this.users = JSON.parse(this.user);
        this.token = this.users.token;
        this.empids = this.users.roleList.empId
       this.labourIds=sessionStorage.getItem("LabourPerIdss")
        this.labourmasterId=sessionStorage.getItem("manRequestIds")
    }
    postToDoMaster(dash: EmpDashModel) {
        let url = this.baseurl + '/ToDoList';
        var body = JSON.stringify(dash);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }

    postnews(news: NewsModel) {
        let url = this.baseurl + '/CreateNewsFeed';
        var body = JSON.stringify(news);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }

    monthlyAttendance(labourMasterId) {
        debugger
        console.log('here is token data',this.token)
        const tokens = this.token;
        console.log('here is token data',tokens)
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        let url = this.baseurl + '/callender';
        // var body = JSON.stringify(labourMasterId);
        // var body = {"labourMasterId":1,"month":2,"year":2024};/
        let body = JSON.stringify({
            "\"labourMasterId\"": 1,
            "\"month\"": 2,
            "\"year\"": 2024
        });
        
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: tokens });
        // return this.http.get(url,body).map(x => x.json());/
        return this.http.post(url, body, requestOptions,).map(x => x.json());
    }

    postPay(Pay: PayModel) {
        let url = this.baseurl + '/payperiodsummary';
        var body = JSON.stringify(Pay);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());

    }
    postFile(fileToUpload: File) {

        let url = this.baseurl + '/uploadFile';
        if(this.Aws_flag!='true'){
            url=this.baseurl + '/fileHandaling';
        }
        const endpoint = url;
        const formData: FormData = new FormData();
        formData.append('uploadfile', fileToUpload, fileToUpload.name);


        return this.http.post(endpoint, formData).map(x => x);;

    }

    imageUrlsss : any;
    onUploadCoupon(filesToUpload:File){
        let url = this.baseurl + '/uploadFile';
        if(this.Aws_flag!='true'){
            url=this.baseurl + '/fileHandaling';
        }
        const endpoint = url;
        const formData: FormData = new FormData();
        formData.append('uploadfile', filesToUpload, filesToUpload.name);


        return this.http.post(endpoint, formData).map(x => x);;

  
    }


    getWeeklyAttendance(Pay) {
        let url = this.baseurl + '/weeklyCalendar';
        var body = JSON.stringify(Pay);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());

    }
    getOnDutyOfDate(Pay) {
        let url = this.baseurl + '/requestOdlist';
        var body = JSON.stringify(Pay);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());

    }
    getLeaveOfDate(Pay) {
        let url = this.baseurl + '/requestLeaveList';
        var body = JSON.stringify(Pay);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }

    getLateEarlycancelDate(Pay) {
        let url = this.baseurl + '/requestLateEarly';
        var body = JSON.stringify(Pay);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }

    getCoffDate(Pay) {
        let url = this.baseurl + '/requestCoffList';
        var body = JSON.stringify(Pay);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }

    updateOd(obj) {
      
        let url = this.baseurl + '/RequestOD';
        return this.http.put(url, obj).map(x => x.json());
    }

    updateLeave(id) {
        let url = this.baseurl + '/CancelLeave/' + id;
        return this.http.get(url).map(x => x.json());
    }
    updatecoff(id) {
        let url = this.baseurl + '/Cancelcoffrequest/' + id;
        return this.http.get(url).map(x => x.json());
    }

    updateExtraWork(id) {
        let url = this.baseurl + '/CancelExtraWorking/' + id;
        return this.http.get(url).map(x => x.json());
    }

    updateMissPunch(obj) {
        let url = this.baseurl + '/RequestMissPunch';
        return this.http.put(url, obj).map(x => x.json());
    }
    updateLateEarly(obj) {
        let url = this.baseurl + '/RequestLateEarly';
        return this.http.put(url, obj).map(x => x.json());
    }
    updateWorkFromHome(obj) {
        let url = this.baseurl + '/RequestWFH';
        return this.http.put(url, obj).map(x => x.json());
    }
    updatePermission(obj) {
        let url = this.baseurl + '/RequestPR';
        return this.http.put(url, obj).map(x => x.json());
    }

    // ********************for bar chart**********************

    AgeGroupWiseTotalCountOfEmp() {

        let url = this.baseurl + '/empattendance/' + this.empOfficialId;
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.get(url).map(x => x.json());
    }


    // ********************bar chart-end **********************
    deleteCoffRequest(id) {
        let url1 = this.baseurl + '/coffrequest/';
        return this.http.delete(url1 + id)
    }
    //=========Delete Requests Starts============
    deleteLeave(id) {
       
        let url1 = this.baseurl + '/LeaveRequest/';
        return this.http.delete(url1 + id)
    }
    deleteLTE(data,empOfficialId) {
      
        let url1 = this.baseurl + '/RequestLateEarly?attenDate='+data.dateAndTime1+'&empOfficialId='+empOfficialId+'&requesLateEarlyId='+data.requesLateEarlyId;;
        return this.http.delete(url1 )
    }
    deleteWFH(id) {
     
        let url1 = this.baseurl + '/RequestWFH/';
        return this.http.delete(url1 + id)
    }
    deleteMP(data,empOfficialId) {
        let url1 = this.baseurl + '/RequestMissPunch?attenDate='+data.dateAndTime1+'&empOfficialId='+empOfficialId+'&requesMissPunchId='+data.requesMissPunchId;
        return this.http.delete(url1);
    }
    deleteOD(id) {
      
        let url1 = this.baseurl + '/RequestOD/';
        return this.http.delete(url1 + id)
    }
    deletePR(id) {
      
        let url1 = this.baseurl + '/RequestPR/';
        return this.http.delete(url1 + id)
    }
    deleteEWR(id) {
      
        let url1 = this.baseurl + '/deleteextraworkingrequest/';
        return this.http.delete(url1 + id)
    }
    deleteCO(id) {
      
        let url1 = this.baseurl + '/coffrequest/';
        return this.http.delete(url1 + id)
    }


    getEmpTeam(id,locId) {
       
        let url1 = this.baseurl + '/employeeteam/' + id+'/'+locId;
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.get(url1).map(x => x.json());
    }
    //=========Delete Requests Ends============

    notYetInToday(dpId:any,locId:any) {

        let url1 = this.baseurl + '/NotYetInToday/'+dpId+'/'+locId;
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.get(url1).map(x => x.json());
    }

    todaysLeaveRequest(deptId,locId) {
     
        let url1 = this.baseurl + '/TodaysOnLeave/' +locId+'/'+ deptId;
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.get(url1).map(x => x.json());
    }

}