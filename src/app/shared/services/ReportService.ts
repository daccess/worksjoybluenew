import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { ReportModel } from '../model/ReportModel';
import { PostReport} from '../model/postRepoModel'
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class ReportService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postRepotMaster(Rep : ReportModel){
    let url = this.baseurl + '/employeedetail';
    var body = JSON.stringify(Rep);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  postAttendance(post : PostReport){
    let url = this.baseurl + '/attendanceReportNewDesign';
    var body = JSON.stringify(post);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
 
  postAttendanceEmpWise(post : any){
    let url = this.baseurl + '/attendancereportlist';
    var body = JSON.stringify(post);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getAllDepartments(compId){
    let url = this.baseurl + '/Department/'+compId;
    return this.http.get(url).map(x => x.json());
  }
  getAllExitInterviewData(model):Observable<any>{
    let url = this.baseurl + '/resignMemberByFilter';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getExitInterviewReportServiceList(empOfficialId):Observable<any>{
    let url = this.baseurl + '/exitInterviewReportList/'+empOfficialId;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

 
}