export class ShiftMaster{
    shiftMasterId: number;
    shiftName: String;
    shortName : String;
    shiftDesc:String;
    shiftCode: string;
    inTime: string;
    endTime: string;
    shiftColor: string;
    signInBeforeTime: string;
    dailyWorkingHrs: number;
    minutesToWorkForFullDay: number;
    minutesToWorkForHalfDay: number;
    flexibleShift: string;
    flexibleInStartTime: string;
    flexibleInEndTime: string;
    coreTimeStartTime: string;
    coreTimeEndTime: string;
    flexibleOutStartTime: string;
    flexibleOutEndTime: string;
    weeklyHrsNeedToWork: number; 
    status:boolean;
    shiftGroupId:number;
    shiftType: string;
    selectedShiftGroup = [];
    shiftAllowance: number;
    compId : number;
     locationId: any;
  locationid: any;
  locationId1: any;
    
}