export class PayRoleMaster {
    payRollHeadsId: number
    payRollHeadName: String
    payRollHeadDesc: String
    payRollHeadType: String
    status: boolean;
    compId: number
}