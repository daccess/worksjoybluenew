export class PayGroupMaster{
    payGroupId: number
    compId:string;
    payGroupName:string;
    payGroupDesc:String;
    status:boolean;
}