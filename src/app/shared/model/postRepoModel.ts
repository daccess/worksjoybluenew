export class PostReport{
    empOfficialId: number;
    selectedEmp=[];
    fromDate:Date;
    toDate:Date;
    customizeReportDepartmentReqDtos = []
    customizeReportSubDepartmentReqDtos = []
    customizeReportLocationReqDtos = []
    customizeReportEmploymentTypeReqDtos = []
    customizeReportSubEmployTypeReqDtos = []
    customizeReportEmpCategoryReqDtos =[]
    customizeReportSubEmpCategoryReqDtos = []
    customizeReportContractorReqDtos = []
    customizeReportBusinessGroupReqDtos = []
    customizeReportGradeReqDtos = []
    customizeReportShiftReqDtos =[]
    customizeReportsConditionReqDtos = []
    customizeReportsCostCenterReqDtos = []
    customizeReportFunctionReqDtos = []
    customizeReportDesignationReqDtos =[]
    customizeReportServiceTypeContractorReqDtos = []
    
}