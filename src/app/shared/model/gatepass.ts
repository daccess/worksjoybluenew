export class GatepassList {
  fullName: string;
  empPerId: any;
  profilePath: string;
  empGatePassId: number;
  conFirmName: string;
  personalContactNumber: string;
  emrContactNumber: string;
  bloodGroup: string;
  pfNumber: string;
  esicNumber: string;
  validStartDate: any;
}
