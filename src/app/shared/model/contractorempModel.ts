import { AnimationStyleMetadata } from "@angular/animations";

export class contractorEmployeeModel {
  empPerId: number;

  salutation: String;

  fullName: String;

  middleName: String;

  lastName: String;

  fatheName: String;

  gender: String;

  dateOfBirth: any;

  personalContactNumber: String;

  personalEmailId: String;

  profilePath: String;

  bloodGroup: String;
  deptId :any;
  empId_supervisor:any;
  empimageURl:any;

  //EmpAddress Details

  empAddressId: number;

  buildingNoCurrent: String;

  socityOrLaneCurrent: String;

  areaCurrent: String;

  cityorTahshilCurrent: String;

  stateCurrent: String;

  pincodeCurrent: number;
  conSerTypeId: number


  // Employee Contractor Emergency Data

  empEmergencyContactPersonDetailsId: number;

  nameOfPerson: String;

  emrContactNumber: String;


  //Employee Official Data

  empOfficialId: number;

  bioId: number;

  empId: String;

  reportingContractPerson: String;

  descId: number;

  locationId: number;

  subDeptId: number;

  subEmpTypeId: number;

  rollMasterId: number;

  contractorId: number;

  validStartDate: Date;

  validEndDate: Date;

  employmentType: String;

  subEmploymentType: String;

  skillCatagory: String;

  skillWisePerDaySalary: string;

  //Emp Attendance Setting

  atdSettingId: number;

  shiftType: String;

  weeklyOffType: String;

  roasterDate: Date;

  shiftPatternId: number;

  singlePunchPresent: Boolean;

  negativeAttendenceAllowed: Boolean;

  shiftMasterId: number;

  shiftMasterRosterId: number;

  Status: Boolean;

  shiftGroupId: number;

  weeklyOffCalenderMasterId: number;


  // Emp Gov Doc Info

  govDocInfoId: number;

  panNo: String;

  adharNo: String;


  // Emp Offer Letter data

  pfNumber: String;

  esicNumber: String;


  logEmpOffId: number;

  buildingNoPermant :String;
  socityOrLanePermant :String;
  areaPermant :String;
  cityOrTalukaPermant :String;
  statePermant  :String;
  pinCodePermant  :String;

  universalAccNo: String;


  //education
  eduQualificationId :String;
  highestEduction :String;
  currentyStudying :String;
  urrentStudyDetails :String;
  subOfSpecialisation :String;

  empDocMasterList:any;

  firstLevelReportingManager:any;
  secondLevelReportingManager:any;
  physicalDisability:any;

}
export class addmangmentEmployeeModel {
  adharNo: string;
  age: number;
  bioId: 0;
  dateOfBirth: string;
  dateOfJoining: string;
  docName: string;
  docType: string;
  docUrl: string;
  drivingLicenseNo: string;
  empContactCellAndEmailInfoId: 0;
  empDocId: 0;
  empDocMasterList: [
    {
      docName: string;
      docType: string;
      docUrl: string;
    }
  ];
  empId: string;
  empOfficialId: 0;
  empPerId: 0;
  filPath: string;
  fileName: string;
  fileType: string;
  fullName: string;
  gender: string;
  govDocInfoId: 0;
  joiningDetailsId: 0;
  lastName: string;
  logEmpOffId: 0;
  maritialStatus: string;
  middleName: string;
  officialContactNumber: string;
  officialEmailId: string;
  panNo: string;
  passportNo: string;
  personalContactNumber: string;
  personalEmailId: string;
  placeOfBirth: string;
  rollMasterId: 0;
  salutation: string;
  sanctioningAuthority:any;
}
