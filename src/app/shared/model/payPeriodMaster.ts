export class PayPeriodMaster {
    selectedLocation = [];
    payPeriodSettingId: number;
    payPeriodName: String;
  
    startDate: number
    endDate: number
    payRollProcessingDay: number
    
    status: boolean;
    compId: number;
    empTypeId: number;
}