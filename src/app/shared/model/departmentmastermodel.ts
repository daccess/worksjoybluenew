export class departmentmodel {
    deptId: number;
    departmentName: string;
    departmentCode: string;
    departmentDescription: string;
    status: boolean;
    compId: number;
    }
