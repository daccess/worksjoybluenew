export class LocationMaster {
    locationId: number;
    locationCode: String;
    locationName: String;
    locationDesc: String;
    locationAddress: String;
    locationConPerson: String;
    contactDetails: String;
    emailId: String;
    status: boolean;
    compId: number;
}