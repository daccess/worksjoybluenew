export class RoleMaster {
    rollMasterId: number
    rollName: String

    organizationMaster: boolean
    holiDayMaster: boolean
    shiftMaster: boolean
    contractorMaster: boolean
    weeklOffMaster: boolean
    payPeriodMaster: boolean
   // employeeMaster: boolean
   employeeMaster:boolean
   employeeList: boolean
   attendanceConfiguration: boolean
   leaveConfiguration: boolean
   importEmployee: boolean
   compId: number
   status: boolean
    roleName: String
    roleDesc: String
    roleHead: String

    transportDetails:boolean
    resignAndLeaving:boolean
    offerLeterAndJoining:boolean
    experienceOverall:boolean
    educationalDetails:boolean 
    employeeOfficials:boolean
    contactDetails:boolean
    personal:boolean
    configuration:boolean
    employee:boolean
    queryAdmin:boolean
    query :boolean
    yearlyCalendar:boolean
    announcement:boolean
    reports:boolean
    attedanceRegularisation:boolean
    others:boolean
    leaveRequest:boolean
    apply:boolean
    dashboard:boolean
    masters:boolean
    empDashboard:boolean;
    pimsHr:boolean
    pimsEmp:boolean
    empList:boolean
    importEmp:boolean
    roleMaster:boolean
    resetPass:boolean
    yearlyCal:boolean
    aproval:boolean
    viewReport:boolean
    viewAttendanceReport:boolean
}