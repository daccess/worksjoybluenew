export class mailSetting {
      emailConfigId :number;

      displayName:string;

      senderMail:string;

      password:string;

      smtpServer:string;

      mailUserName:string;

      portNo:string;

      ssl:boolean;
      compId:any;
      empOfficialId:any;
      status:boolean
}