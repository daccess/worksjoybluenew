export class ContractorMaster {


  contractorId: number;

  conName: String;

  conFirmName: String;

  conAddress: String;

  conEmail: String;

  conContactNo: number;

  conServiceType: String

  conSerZTypeName: string

  conType: String;

  conFirmDesc: String;

  conContactPer: String;

  conContactPerCon: String;

  conContactPerEmail: String;

  startDate: any;

  expDate: any;

  noOfManpwrApproved: number;

  alcLicNo: String;

  labLicStartDate: any;

  labLicExpDate: any;

  approvedStrength: number;

  wcNo: String;

  wcExpDate: any;

  empCoveredUndrWc: number;

  epfRegNo: String;

  esic: any;

  esicApplicble: any;

  esicfRegNo: String;

  gstRegNo: String;

  conSerTypeId: any;

  status: Boolean;

  panNo: string;

  pf: String;

  pfApplicable: any;

  lwfApplicable: any;

  tinNo: string

  lwf: any;

  selectedLocation: any;
  contractorGovServiceMaster: any
  contractorLocationMasters: any = [];
  startDatae: any
  expDatae: any;
  conGovSerId: any
  getSelectedLocation: any
  contractorServiceTypeMaster: any
  licenceNo: any;
  push: any;
  locationId: any;
  conValidityId:any;
  pfNumber:any;
  esicNumber:any;
  lwFNumber:any;
}
