import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HolidayMaster } from'../model/holidayMasterModel'
import { MainURL } from '../configurl';

@Injectable()
export class LayOutService {
  // selectedcomp : commaster;
  // commasterList : commaster[];
  baseurl = MainURL.HostUrl
   constructor(private http : Http) { }

   getBellAlerts(id){
    let url = this.baseurl + '/getRequestApproveList/'+id;
    return this.http.get(url).map(x => x.json());
  }

  updateLeave(obj){
    let url = this.baseurl + '/approveLeaveRequestApproveNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }

  updateQuery(obj){
    let url = this.baseurl + '/singleClearRaiseQueryNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
  updateMissPunch(obj){
    let url = this.baseurl + '/approveMissPunchNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
  updateOnDuty(obj){
    let url = this.baseurl + '/approveODNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
  updateLateEarly(obj){
    let url = this.baseurl + '/approveLateEarlyNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
  updateWorkFromHome(obj){
    let url = this.baseurl + '/approveWFHNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
  updatePermissionRequest(obj){
    let url = this.baseurl + '/approvePRNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
  getSuggestionList(empPerId){
    let url = this.baseurl + '/getSuggestionList/';
    return this.http.get(url+empPerId).map(x => x.json());
  }

  updateExtraWorkRequest(obj){
    let url = this.baseurl + '/approveExtraWorkingNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }

  updateExtraWorkRequestHr(obj){
    let url = this.baseurl + '/requestToWork/';
    return this.http.put(url,obj).map(x => x.json());
  }

  // updateExtraWorkRequestHr(obj){
  //   let url = this.baseurl + '/approveExtraWorkingNotification/';
  //   return this.http.put(url,obj).map(x => x.json());
  // }

  updateLeaveBlock(obj){
    let url = this.baseurl + '/leaveBlock/';
    return this.http.put(url,obj).map(x => x.json());
  }

  updateDateBlock(obj){
    let url = this.baseurl + '/singleClearBlockDateNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }

  updateCoff(obj){
    let url = this.baseurl + '/approveCoffNotification/';
    return this.http.put(url,obj).map(x => x.json());
  }
 
  clearAll(id){
    // let body  = JSON.stringify(obj)
    let url = this.baseurl + '/clearAllButton/'+id;
    return this.http.put(url,'').map(x => x.json());
  }

  getSearchSuggestionList(text,locationId:any){
    let url = this.baseurl + '/EmployeeOperationSearch/';
    return this.http.get(url+locationId+'/'+text).map(x => x.json());
  }

  suggestThisEmployee(id){
    let url = this.baseurl + '/EmployeeById/';
    return this.http.get(url+id).map(x => x.json());
  }
}