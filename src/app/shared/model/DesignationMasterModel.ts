export class DesignationMaster{
    desigId: number;
    desigName:String;
    desigDesc:String;
    desigLevel:Number;
     status:boolean;
     compId:String;
}