export class DeviceMaster {
    deviceMasterId:number;
    deviceName:String;
    deviceIpAddress:String;
    deviceSerialNumber:String;
    deviceUsedFor:String;
    deviceExactLocation:String;
    deviceStatus:boolean;
    locationId:number 
    ioStatus:any;
    }