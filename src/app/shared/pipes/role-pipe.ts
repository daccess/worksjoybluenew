// Angular
import { Pipe, PipeTransform } from '@angular/core';
/**
 * Returns only first letter of string
 */
@Pipe({
	name: 'rolePipe'
})
export class RolePipe implements PipeTransform {

	/**
	 * Transform
	 *
	 * @param value: any
	 * @param args: any
	 */
	transform(value: any, args?: any): any {
		// console.log(value);
		let temp_array=[];
      if(value){
		for(let i=0;i<value.length;i++){
			console.log(value[i].rollName);
			if(value[i].rollName=='User'|| value[i].rollName=='Contractor Manager' || value[i].rollName=='Contractor Supervisor'){
				temp_array.push(value[i]);
			}
		}
		console.log(temp_array);
		return temp_array;
	  }else{
		  return value;
	  }
	}
}