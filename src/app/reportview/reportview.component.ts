import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import html2canvas from 'html2canvas';
import { ExcelService } from '../absentreport/excel.service';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as moment from 'moment';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
@Component({
  selector: 'app-reportview',
  templateUrl: './reportview.component.html',
  styleUrls: ['./reportview.component.css'],
  providers: [DatePipe, ExcelService]
})
export class ReportviewComponent implements OnInit {
  AllReport = [];
  myDate: any;
  nameCheckbox = true;
  shiftcheckbox = true;
  attendenceCheckbox = true;
  intimeCheckbox = true;
  outtimeCheckbox = true;
  workingCheckbox = true;
  statusdayChecckbox = true;
  statusChecckbox = true;
  branchCheckbox = true;
  deptCheckbox  = true;
  catCheckbox = true;
  srNoCheckbox = true
  marked = true;
  markedb = true;
  markedd = true;
  markedc = true;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked9= true;
  marked_empid=true;
  fromDate: any;
  toDate: any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  user_data=[];
  report_time: string;
  attendance_data:any;
  result_data_modify: any;
  
  constructor(private datePipe: DatePipe, private excelService: ExcelService,private InfoService:InfoService,private router: Router) {
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    var customizeOrEmp = sessionStorage.getItem('customizeOrEmp');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.myDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.result_data_modify = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MMM-dd');
    if(customizeOrEmp == "employee"){
      this.AllReport.push(this.result_data_modify);
    }
    else{
      this.AllReport = this.result_data_modify;
    }
    for(let i=0;i<this.AllReport.length;i++){

      if(this.AllReport[i].length!=0){
        for(let l= 0; l<this.AllReport[i].length; l++){

          if(this.AllReport[i][l].checkIn==null){
           this.AllReport[i][l].attendanceStatus='IPF';
          }
          if(this.AllReport[i][l].checkOut==null){
           this.AllReport[i][l].attendanceStatus='OPF';
          }
          if(this.AllReport[i][l].checkIn==null && this.AllReport[i][l].checkOut==null){
           this.AllReport[i][l].attendanceStatus='A';
          }
          
         }
        for(let j=0;j<this.AllReport[i].length;j++){
               this.user_data.push(this.AllReport[i][j]);
             }
            }
         
           
              
           
            
            
      }
     
   
    
console.log(  this.AllReport);

    this.attendance_data = Array.from(this.user_data.reduce((m, {
      empId, fullName,lastName,attenDate,locationName,oldEmpId,statusOfDay,empTypeName,deptName,attendanceStatus,shiftName,checkIn,checkOut,workDuration,requestDetails}) => 
      m.set(empId, [...(m.get(empId) || []), {"empId":empId,"fullName":fullName,"lastName":lastName,"attenDate":attenDate,"locationName":locationName,"oldEmpId":oldEmpId,"statusOfDay":statusOfDay,"empTypeName":empTypeName,"deptName":deptName,"attendanceStatus":attendanceStatus,"shiftName":shiftName,"checkIn":checkIn,"checkOut":checkOut,"workDuration":workDuration,"requestDetails":requestDetails}]), new Map), ([empno, empname]) => ({
        empno, "emp_details":empname
      }));



      // }

  }

  ngOnInit() {
    console.log(this.attendance_data);
  }
  timeDuration: string;
  in_time: any;
  out_time:any;
  timeConvert(n) {
    let temp=(Math.floor(n / 60) + ':' + n % 60);
    this.timeDuration=temp;
    return this.timeDuration;
  }
  getInTime(timestamp){
    if(timestamp!=null){
      let temp_in_time=new Date(timestamp);
      this.in_time=moment(temp_in_time).format('HH.mm');
      return this.in_time;
    }
  }

  getOutTime(timestamp){
    if(timestamp!=null){
      let temp_out_time=new Date(timestamp);
      this.out_time=moment(temp_out_time).format('HH.mm');
      return this.out_time;
    }
   
  }

  toggleVisibilitybranch(e){
    this.markedb = e.target.checked;
  }

  toggleVisibilitydept(e){
    this.markedd = e.target.checked;
  }

  toggleVisibilitycat(e){
    this.markedc = e.target.checked;
  }

  toggleVisibility(e) {
    this.marked = e.target.checked;
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }

  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }
  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
  toggleVisibilityID(e) {
    this.marked_empid = e.target.checked;
  }
  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename='Attendance-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#employees_attendance',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(0, 0, 0);
        doc.setFontSize(7);
        doc.setFontStyle("Arial");
        doc.text(13, 15, 'Abbreviation:- HF : Half day, WD: Week Day, WFH: Work From Home, HO: Holiday , WO : Week Off , CO : Coff, OD : On Duty');
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
    doc.save('Attendance-Report'+this.report_time+'.pdf');  
  }
  }


