import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ReportviewComponent } from 'src/app/reportview/reportview.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ReportviewComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'viewreport',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'viewreport',
                    component: ReportviewComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);