import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractormasterComponent } from './contractormaster.component';

describe('ContractormasterComponent', () => {
  let component: ContractormasterComponent;
  let fixture: ComponentFixture<ContractormasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractormasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractormasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
