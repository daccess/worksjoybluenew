import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { ContractServiceTypeModel } from 'src/app/shared/model/ContractorTypeServiceModel';
import { ContractorTypeMasterService } from '../../../shared/services/ContractorTypeMasterService';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from "@angular/router";

class addcompanym {
  compId: number
};
@Component({
  selector: 'app-contratcm',
  templateUrl: './contratcm.component.html',
  styleUrls: ['./contratcm.component.css']
})
export class ContratcmComponent implements OnInit {
  addcompany: addcompanym;
  StatusFlag:boolean=false

  ContractModel: ContractServiceTypeModel;
  selectedCompanyobj: any
  companydata: any;
  // private selectUndefinedOptionValue:any;
  baseurl = MainURL.HostUrl;
  compId: any;
  // compdata = [];
  ContractMASTER: any;
  isedit = false;
  Selectededitobj: any;
  selectedComp: Number;
  allContractorData: any;
  //updatecompany: any;
  geteditid: any;
  loggedUser: any;
  errordata: any;
  msg: any;
  errflag: boolean;
  dltmodelFlag: boolean;
  dltObje: any;
  loconame: String;
  user: string;
  users: any;
  token: any;
  editedData: any;
  contractorEditData: any;
  editedDatas: any;

  constructor(public httpService: HttpClient,public Spinner :NgxSpinnerService, public Contractor: ContractorTypeMasterService , public chRef: ChangeDetectorRef,private toastr: ToastrService,private router: Router) {

    this.addcompany = new addcompanym()
    this.ContractModel = new ContractServiceTypeModel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.selectedCompanyobj = "";
    // this.compId =  this.loggedUser.compId;
    this.getallContractMaster();
  }
  
  error(){
    if(this.loconame != this.ContractModel.conSerTypeName){
    let obj={
      compId : this.compId,
      conSerTypeName:this.ContractModel.conSerTypeName
      }
      let url = this.baseurl + '/contractorSerType';
      this.httpService.post(url,obj).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        });
    }
  }

  onSubmit(f) {
    this.Spinner.show();
    this.ContractModel.status=this.flag1;
    this.StatusFlag=false
    if (this.isedit == false) {
      this.ContractModel.compId = this.selectedCompanyobj;
      
      let url = this.baseurl + '/createContractorServiceType';
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        this.httpService.post(url,this.ContractModel,{headers}).subscribe(data => {
         
          this.Spinner.hide();
          f.reset();
          this.selectedCompanyobj ='';
          this.toastr.success('Contractor Service Type Master Information Added Successfully!');
          this.getallContractMaster()
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Something Went Wrong Please Try Again Later..!');
        });
    }
    
         else {
      let url1 = this.baseurl + '/updateContractorServiceType';
      this.ContractModel.conSerTypeId = this.geteditid;
      this.ContractModel.compId = this.selectedCompanyobj;
      const tokens = this.token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.put(url1, this.ContractModel,{headers})
        .subscribe(data => {
          this.Spinner.hide();
          f.reset();
          this.selectedCompanyobj ='';
          this.toastr.success('Contractor Service Type Master Information Updated Successfully!');
          this.getallContractMaster()
          this.isedit=false
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Something Went Wrong Please Try Again Later..!');
          this.isedit=false
        }); 
    }
  }
  dltmodelEvent(){
    this.dltmodelFlag=true;
    this.dltObjfun(this.dltObje);
    this.Spinner.show();
  }
 
  dltObj(obj){
    this.dltmodelFlag=false
    this.dltObje = obj.conSerTypeId;
    this.dltObjfun(this.dltObje)
  }
  dltObjfun(object){
    
      if(this.dltmodelFlag==true){
        const tokens = this.token;
        const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
        let url1 = this.baseurl + `/deleteContractorServiceTypeById?conSerTypeId=${this.dltObje}`;
        this.ContractModel.conSerTypeId = this.dltObje;
        this.httpService.delete(url1 ,{headers}).subscribe(data => {
          this.Spinner.hide();
          this.getallContractMaster()
          this.toastr.success('Delete Successfully');
         
        },(err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Something Went Wrong Please Try Again Later..!');
        });
      }
  

  }
  ngValue(event) {
    this.selectedCompanyobj = parseInt(event.target.value)
  }
  edit(object) {
    this.geteditid = object.conSerTypeId
    this.editcon(this.geteditid)
    this.isedit = true;
    this.StatusFlag=true
    this.flag1 = object.status;
  }
  flag1 : boolean = true;
  editcon(geteditid) {
    let urledit = this.baseurl + `/getContractorServiceTypeById?conSerTypeId=${geteditid}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    
    this.httpService.get(urledit ,{headers}).subscribe((data:any) => {
      // this.contractorEditData=data;
      
      this.editedData=data;
        this.editedDatas= this.editedData.result
        this.ContractModel.status =this.editedDatas.status
        this.ContractModel.conSerTypeName = this.editedDatas.conSerTypeName
        this.ContractModel.conSerTypeDesc = this.editedDatas.conSerTypeDesc
        this.loconame = this.ContractModel.conSerTypeName
        var comp_id = this.editedDatas.companyMaster.compId;
        for (var i = 0; i < this.companydata.length; i++) {
          if (this.companydata[i].compId == comp_id) {
            this.selectedCompanyobj = comp_id;
          }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }

  ngOnInit() {
    
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token
    this.getallContractMaster();
    this.getCompany()
   
  }
  getCompany(){
    
    let url = this.baseurl + '/getAllCompany';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpService.get(url,{headers}).subscribe(data => {
        this.companydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    }
  dataTableFlag : boolean  = true
  getallContractMaster() {
    let url1 = this.baseurl + '/getAllContractorServiceTypes/';
    this.dataTableFlag = false
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpService.get(url1 ,{headers}).subscribe(
      data => {
        this.ContractMASTER = data["result"];
        this.dataTableFlag = true
        this.chRef.detectChanges(); 
        setTimeout(function () {
          $(function () {
            var table = $('#Business').DataTable();
          });
        }, 1000);
        
        this.chRef.detectChanges();

      },
      (err: HttpErrorResponse) => {
      });
  }
  uiswitch(event) {
    this.flag1 = !this.flag1;
  }  
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }
  Cancel(){
    this.router.navigateByUrl('/layout/settingpage')
  }
}
