import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratcmComponent } from './contratcm.component';

describe('ContratcmComponent', () => {
  let component: ContratcmComponent;
  let fixture: ComponentFixture<ContratcmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratcmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratcmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
