import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { ContractorMaster } from 'src/app/shared/model/ContractorMasterModel';
import {ContractorMasterService} from '../../../shared/services/ContractorMasterService';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from "@angular/router";
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-contractormaster1', 
  templateUrl: './contractormaster1.component.html',
  styleUrls: ['./contractormaster1.component.css']
})
export class Contractormaster1Component implements OnInit {

  ContractMasterModel: ContractorMaster;
  selectedCompanyobj: any
  ActiServiceTypedata: any;

  baseurl = MainURL.HostUrl;
  compId: any;

  errflag: boolean
  errflag2: boolean
  errflag3: boolean
  errflag4: boolean
  errflag5: boolean
  errflag6: boolean
  errflag7: boolean
  errflag8: boolean
  errflag9: boolean;
  errflag10: boolean;
  errflag11: boolean;
  exiestContact: any;
  addArray = [];
  addOnArray = [];
  ContractMast: any;
  isedit = false;
  Selectededitobj: any;
  selectedComp: Number;
  StatusFlag: boolean = false
  flag1: Boolean;
  geteditid: any;
  startTimeChanged: any;
  currentdate: Date;
  newdate: number;
  conContactNo: any
  selectedLocation: any;
  selectedItems: any[];
  dropdownSettings: {
    singleSelection: boolean;idField: string;textField: string;selectAllText: string;unSelectAllText: string;itemsShowLimit: number;allowSearchFilter: boolean;
  };
  allActiveData: any;
  selectGoveSerObj: any
  selectedCotractobj: any;
  loggedUser: any;
  dltmodelFlag: boolean;
  dltObje: any;
  today: string;
  errordata: any;
  msg: string;
  conEmail: any;
  exiestEmail: any;
  Emailerrordata: any;
  exiestWcNo: any;
  wcNo: String;
  Wcerrordata: any;
  epfRegNo: any;
  Epferrordata: any;
  epfExiestNo: any;
  emailObject: any = {};
  gstRegNo: String;
  regExiestNo: String;
  Regerrordata: any;
  exiestEsicfNo: String;
  esicfRegNo: String;
  esicferrordata: any;
  labLicStartDate: any
  labLicExpDate: any;
  wcExpDate: any
  startDate: any
  expDate: any
  exiestPanNo: string;
  panNo: string;
  PANerrordata: any;
  exiestTinNo: string;
  tinNo: string;
  Tinerrordata: any;
  exiestLicenceNo: any;
  licenceNoerrordata: any;
  licenceNo: any;
  addObject: any = {}
  LocationOption: any;
  locationName: any;
  locationId: any
  selectedLocationName: any;
  conGovSerId: any;
  GoveServiceData: any;
  esicApplicble: string;
  lwfApplicable: string;
  exiestLwlNo: string;
  pfApplicable: any;
  pfExiestNo: string;
  ExiestepfNo: String;
  chooselocation: any;
  c: any;
  body: any;
  constructor(public httpService: HttpClient, public Spinner: NgxSpinnerService, public Contractor: ContractorMasterService, private chRef: ChangeDetectorRef, public toastr: ToastrService,private router: Router) {

    this.ContractMasterModel = new ContractorMaster();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.compId = this.loggedUser.compId;
    this.getallContractMasters();
    this.selectedCotractobj = "";
    this.ContractMasterModel.esicApplicble = false;
    this.ContractMasterModel.pfApplicable = false;
    this.ContractMasterModel.lwfApplicable = false;
  }

  ngOnInit() {
    this.ContractMasterModel.startDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    this.ContractMasterModel.expDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    this.ContractMasterModel.labLicStartDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    this.ContractMasterModel.labLicExpDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    this.ContractMasterModel.wcExpDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
    let url1 = this.baseurl + '/CosSerType/Active/';
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.ActiServiceTypedata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
    this.currentdate = new Date();
    this.currentdate.setDate(this.currentdate.getDate() + 1);
    let url2 = this.baseurl + '/Location/Active/';
    this.httpService.get(url2 + this.compId).subscribe(data => {
        this.allActiveData = data["result"];
      },
      (err: HttpErrorResponse) => {
      }
    );
    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }
  dataTableFlag: boolean = true
  getallContractMasters() {
    let url1 = this.baseurl + '/Contractor/';
    this.dataTableFlag = false
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.ContractMast = data["result"];
        this.dataTableFlag = true;
        this.chRef.detectChanges();
        setTimeout(function () {
          $(function () {
            var table = $('#Contable').DataTable();
          });
        }, 1000);

        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      }
    );
  }

  getallGovServiceData(event,val) {
    this.GoveServiceData=[];
    let selected_service_type;
    if(event){
      selected_service_type=event.target.value;
    }
    if(val){
      selected_service_type=val;
    }

    let url1 = this.baseurl + '/contractorActiveList';
    this.httpService.get(url1).subscribe(
      data => {
        let temp_data = data["result"];
        if(temp_data){
          for(let i=0;i<temp_data.length;i++){
              if(temp_data[i].contractorServiceTypeMaster){
                if(temp_data[i].contractorServiceTypeMaster.conSerTypeId==selected_service_type){
                    let temp_key=temp_data[i].contractorServiceTypeMaster.conSerZTypeName+' - '+temp_data[i].validityMonths+' months';
                    this.GoveServiceData.push({
                      "service_validity":temp_key,
                      "conValidityId":temp_data[i].conValidityId
                    });
                }
              }
          }
        }
      },
      (err: HttpErrorResponse) => {
      }
    );
  }
  handler(event, date) {
    if (date == 'start') {
      this.ContractMasterModel.startDate = event.target.value;
    } else if (date == 'end') {
      this.ContractMasterModel.expDate = event.target.value;
    } else {
      this.ContractMasterModel.startDate = (new Date(this.ContractMasterModel.startDate)).getTime(),
        this.ContractMasterModel.expDate = (new Date(this.ContractMasterModel.expDate)).getTime()
    }
    if (date == 'Laberstart') {
      this.ContractMasterModel.labLicStartDate = event.target.value;
    } else if (date == 'Laberend') {
      this.ContractMasterModel.labLicExpDate = event.target.value;

    } else if (date == 'wcExdate') {
      this.ContractMasterModel.wcExpDate = event.target.value;
    } else {
      this.ContractMasterModel.labLicStartDate = (new Date(this.ContractMasterModel.labLicStartDate)).getTime(),
      this.ContractMasterModel.labLicExpDate = (new Date(this.ContractMasterModel.labLicExpDate)).getTime(),
      this.ContractMasterModel.wcExpDate = event.target.value;
      this.ContractMasterModel.startDate = (new Date(this.ContractMasterModel.startDate)).getTime(),
      this.ContractMasterModel.expDate = (new Date(this.ContractMasterModel.expDate)).getTime()
    }
  }
  Startvalue(event) {
    this.ContractMasterModel.startDate = event.target.value;
    this.startDate = event;
  }
  onSubmit(f) {
    this.Spinner.show();
    this.StatusFlag = false
    this.ContractMasterModel.status = this.flag1;
    this.ContractMasterModel.startDate = (new Date(this.ContractMasterModel.startDate)).getTime();
    this.ContractMasterModel.expDate = (new Date(this.ContractMasterModel.expDate)).getTime();
    this.ContractMasterModel.labLicStartDate = (new Date(this.ContractMasterModel.labLicStartDate)).getTime();
    this.ContractMasterModel.labLicExpDate = (new Date(this.ContractMasterModel.labLicExpDate)).getTime();
    this.ContractMasterModel.wcExpDate = (new Date(this.ContractMasterModel.wcExpDate)).getTime();
    this.ContractMasterModel.contractorLocationMasters = [];
    this.ContractMasterModel.contractorLocationMasters = this.addOnArray;
    this.ContractMasterModel.conSerTypeId = this.selectedCompanyobj;
    this.ContractMasterModel.conValidityId = this.selectGoveSerObj;
    setTimeout(() => {
      if (this.isedit == false) {
        this.Contractor.postContractorMaster(this.ContractMasterModel)
          .subscribe(data => {
              this.Spinner.hide();
              if(data.statusCode==201){
                //f.reset();
                this.resetForm();
                f.form.markAsUntouched();
                this.ContractMasterModel.selectedLocation = [];
                this.toastr.success('Contractor Master Information Inserted Successfully!');
                this.getallContractMasters();
              }
              
            },
            (err: HttpErrorResponse) => {
              this.Spinner.hide();
              this.toastr.error('Something went wrong..!!');
            });
  
      } else {
        this.errflag = false;
        this.errflag2 = false;
        this.errflag3 = false;
        this.errflag4 = false;
        this.errflag5 = false;
        this.errflag6 = false;
        this.errflag7 = false;
        this.errflag8 = false;
        this.errflag9 = false;
        this.errflag10 = false;
        this.errflag11 = false;
        let url1 = this.baseurl + '/ContractorMaster';
        this.ContractMasterModel.contractorId = this.geteditid;
        this.ContractMasterModel.conSerTypeId = this.selectedCotractobj;
        this.httpService.put(url1, this.ContractMasterModel).subscribe(data => {
             // f.reset();
              this.resetForm();
              //f.markAsPristine();
              f.form.markAsUntouched();
              this.toastr.success('Contractor Master Information Updated Successfully!', 'Contractor-Master');
              this.getallContractMasters();
              this.isedit = false;
              this.Spinner.hide();
            },
            (err: HttpErrorResponse) => {
              this.Spinner.hide();
              this.toastr.error('Something went wrong while updating..!!', 'Contractor-Master');
              this.isedit = false;
            }
          );
      }
    }, 100);
  }
  edit(object) {
    this.geteditid = object.contractorId;
    this.editcon(this.geteditid);
    this.isedit = true;
    this.StatusFlag = true;
    this.flag1 = object.status;
  }
  editcon(geteditid) {

    let urledit = this.baseurl + '/Contractor/getById/';
    this.httpService.get(urledit + geteditid).subscribe(data => {
        this.ContractMasterModel.status = data['result']['status'];
        this.ContractMasterModel.conFirmName = data['result']['conFirmName'];
        this.ContractMasterModel.conName = data['result']['conName'];
        this.ContractMasterModel.conContactNo = data['result']['conContactNo'];
        this.ContractMasterModel.conType = data['result']['conType'];
        this.ContractMasterModel.conServiceType = data['result']['conServiceType'];
        this.ContractMasterModel.noOfManpwrApproved = data['result']['noOfManpwrApproved'];
        this.ContractMasterModel.alcLicNo = data['result']['alcLicNo'];
        this.ContractMasterModel.labLicStartDate = data['result']['labLicStartDate'];
        this.ContractMasterModel.labLicExpDate = data['result']['labLicExpDate'];
        this.ContractMasterModel.approvedStrength = data['result']['approvedStrength'];
        this.ContractMasterModel.wcNo = data['result']['wcNo'];
        this.ContractMasterModel.wcExpDate = data['result']['wcExpDate'];
        this.ContractMasterModel.empCoveredUndrWc = data['result']['empCoveredUndrWc'];
        this.ContractMasterModel.epfRegNo = data['result']['epfRegNo'];
        this.ContractMasterModel.esicfRegNo = data['result']['esicfRegNo'];
        this.ContractMasterModel.gstRegNo = data['result']['gstRegNo'];
        this.ContractMasterModel.conAddress = data['result']['conAddress'];
        this.ContractMasterModel.conEmail = data['result']['conEmail'];
        this.ContractMasterModel.startDate = data['result']['startDate'];
        this.ContractMasterModel.expDate = data['result']['expDate'];
        this.ContractMasterModel.esicApplicble = data['result']['esicApplicble'];
        this.ContractMasterModel.pfApplicable = data['result']['pfApplicable'];
        this.ContractMasterModel.lwfApplicable = data['result']['lwfApplicable'];
        this.ContractMasterModel.tinNo = data['result']['tinNo'];
        this.ContractMasterModel.panNo = data['result']['panNo'];
        this.ContractMasterModel.esicNumber = data['result']['esicNumber'];
        this.ContractMasterModel.pfNumber = data['result']['pfNumber'];
        this.ContractMasterModel.lwFNumber = data['result']['lwFNumber'];
        this.addOnArray=[];
        if(data['result']['selectedLocation'].length!=0){
          this.selectedLocation=data['result']['selectedLocation'][0].locationId;
          this.locationId=data['result']['selectedLocation'][0].locationId;
          this.ContractMasterModel.licenceNo = data['result']['selectedLocation'][0]['licenceNo'];

          //list
          for(let arr of data['result']['selectedLocation']){
            let obj = {
              "licenceNo": arr.licenceNo,
              "locationMaster":{
                "locationName": arr.locationName,
                "locationId": arr.locationId
              },
              "locationName": arr.locationName,
            }
            this.addOnArray.push(obj);
          }
        }
        this.chooselocation = this.selectedLocation;
        this.exiestContact = this.ContractMasterModel.conContactNo;
        this.exiestEmail = this.ContractMasterModel.conEmail;
        this.exiestWcNo = this.ContractMasterModel.wcNo;
        this.epfExiestNo = this.ContractMasterModel.epfRegNo;
        this.regExiestNo = this.ContractMasterModel.gstRegNo;
        this.exiestEsicfNo = this.ContractMasterModel.esicfRegNo;
        this.exiestPanNo = this.ContractMasterModel.panNo;
        this.exiestTinNo = this.ContractMasterModel.tinNo;
        this.exiestLicenceNo = this.ContractMasterModel.licenceNo;
        this.pfApplicable = this.ContractMasterModel.pfApplicable;
        this.epfRegNo = this.ContractMasterModel.epfRegNo;
        if(data['result']['contractorServiceTypeMaster']){
          this.selectedCotractobj = data['result']['contractorServiceTypeMaster']['conSerTypeId'];
        }
        this.getallGovServiceData('',this.selectedCotractobj);
        this.selectGoveSerObj = data['result']['conValidityId'];
      },
      (err: HttpErrorResponse) => {
      });

  }

  Locationdata(event) {
    this.selectedLocation = parseInt(event.target.value);
    this.chooselocation = this.selectedLocation;
  }

  dltmodelEvent() {
    this.dltmodelFlag = true;
    this.dltObjfun(this.dltObje);

  }
  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.contractorId;
    this.dltObjfun(this.dltObje);
  }
  dltObjfun(object) {

    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/contractor/';
      this.ContractMasterModel.contractorId = this.dltObje;
      this.httpService.delete(url1 + this.dltObje).subscribe(data => {
          this.getallContractMasters();
          this.toastr.success('Deleted Successfully', 'Contractor-Master');
          this.getallContractMasters();
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('References is present so you can not delete..!');
        });
    }
  }

  resetForm() {
    
    setTimeout(() => {
      this.selectedCotractobj = '';
      this.selectGoveSerObj = '';
      this.ContractMasterModel.startDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
      this.ContractMasterModel.expDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
      this.ContractMasterModel.labLicStartDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
      this.ContractMasterModel.labLicExpDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
      this.ContractMasterModel.wcExpDate = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear();
      this.ContractMasterModel.selectedLocation = [];
      this.addOnArray=[];
      this.ContractMasterModel.conFirmName = "";
      this.ContractMasterModel.conName = "";
      this.ContractMasterModel.conContactNo = null;
      this.ContractMasterModel.conEmail = "";
      this.ContractMasterModel.conAddress = "";
      this.ContractMasterModel.noOfManpwrApproved = null;
      this.ContractMasterModel.wcNo = "";
      this.ContractMasterModel.epfRegNo = "";
      this.ContractMasterModel.gstRegNo = "";
      this.ContractMasterModel.pfApplicable = false;
      this.ContractMasterModel.pfNumber = "";
      this.ContractMasterModel.esicApplicble = false;
      this.ContractMasterModel.esicNumber = "";
      this.ContractMasterModel.lwfApplicable = false;
      this.ContractMasterModel.lwFNumber = "";
      this.ContractMasterModel.tinNo = "";
      this.ContractMasterModel.panNo = "";
      this.ContractMasterModel.licenceNo = "";
      this.ContractMasterModel.status = false;
    }, 100);
    
  }

  ngValue(event) {
    this.selectedCompanyobj = parseInt(event.target.value);
  }
  GoveSerData(event) {
    this.selectGoveSerObj = parseInt(event.target.value);
  }

  uiswitch(event) {
    this.flag1 = !this.flag1;
  }
  error() {

    if (this.exiestContact != this.ContractMasterModel.conContactNo) {
      this.conContactNo = this.ContractMasterModel.conContactNo;
      let url = this.baseurl + '/contractorMasterCon/';
      this.httpService.get(url + this.conContactNo).subscribe((data: any) => {
          this.errordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag = false;
        });
    }
  }
  Emailerror() {
    if (this.exiestEmail != this.ContractMasterModel.conEmail) {
      this.emailObject.conEmail = this.ContractMasterModel.conEmail;
      let url = this.baseurl + '/contractorMasterEmail';
      this.httpService.post(url, this.emailObject).subscribe((data: any) => {
          this.Emailerrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag2 = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag2 = false;
        });
    }
  }
  WcNoerror() {

    if (this.exiestWcNo != this.ContractMasterModel.wcNo) {
      this.wcNo = this.ContractMasterModel.wcNo;
      let url = this.baseurl + '/contractorMasterWcNo/';
      this.httpService.get(url + this.wcNo).subscribe((data: any) => {
          this.Wcerrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag3 = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag3 = false
        });
    }
  }

  pfError() {

    if (this.pfExiestNo != this.ContractMasterModel.pfNumber) {
      let temp = this.ContractMasterModel.pfNumber;
      let url = this.baseurl + '/contractorMasterPfApplicable/';
      this.httpService.get(url + temp).subscribe((data: any) => {
          this.Epferrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag4 = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag4 = false;
        });
    }
  }

  gstRegNoerror() {

    if (this.regExiestNo != this.ContractMasterModel.gstRegNo) {
      this.gstRegNo = this.ContractMasterModel.gstRegNo;
      let url = this.baseurl + '/contractorMastergstRegNo/';
      this.httpService.get(url + this.gstRegNo).subscribe((data: any) => {
          this.Regerrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag5 = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag5 = false;
        });
    }
  }

  epfNoerror() {
    if (this.ExiestepfNo != this.ContractMasterModel.epfRegNo) {
      this.epfRegNo = this.ContractMasterModel.epfRegNo;
      let url = this.baseurl + '/contractorMasterEpfRegNo/';
      this.httpService.get(url + this.epfRegNo).subscribe((data: any) => {
          this.Epferrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag11 = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag11 = false;
        });
    }
  }

  EsicfEerror() {
    if (this.exiestEsicfNo != this.ContractMasterModel.esicNumber) {
      let temp_esic = this.ContractMasterModel.esicNumber;
      let url = this.baseurl + '/contractorMasterEsicApplicble/';
      this.httpService.get(url + temp_esic).subscribe((data: any) => {
          this.esicferrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag6 = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag6 = false
        });
    }
  }
  exiestPAN() {
    if (this.exiestPanNo != this.ContractMasterModel.panNo)
      this.panNo = this.ContractMasterModel.panNo;
    let url = this.baseurl + '/contractorMasterPanNo/';
    this.httpService.get(url + this.panNo).subscribe((data: any) => {
        this.PANerrordata = data["result"];
        this.msg = data.message;
        this.toastr.error(this.msg);
        this.errflag7 = true;
      },
      (err: HttpErrorResponse) => {
        this.errflag7 = false;
      });
  }
  exiesTin() {
    if (this.exiestTinNo != this.ContractMasterModel.tinNo)
      this.tinNo = this.ContractMasterModel.tinNo;
    let url = this.baseurl + '/contractorMasterTinNo/';
    this.httpService.get(url + this.tinNo).subscribe((data: any) => {
        this.Tinerrordata = data["result"];
        this.msg = data.message;
        this.toastr.error(this.msg);
        this.errflag8 = true;
      },
      (err: HttpErrorResponse) => {
        this.errflag8 = false;
      });
  }
  exiestLicence() {
    let url = this.baseurl + '/contractorMasterlicenceNo/';
    this.httpService.get(url + this.ContractMasterModel.licenceNo).subscribe((data: any) => {
        if(data.message!="Not Found"){
          this.licenceNoerrordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag9 = true;
        }
      },
      (err: HttpErrorResponse) => {
        this.errflag9 = false;
      });
  }

  exiestLwf() {
    if (this.exiestLwlNo != this.ContractMasterModel.lwFNumber){
      let temp_lwf = this.ContractMasterModel.lwFNumber;
      let url = this.baseurl + '/contractorMasterLwfApplicable/';
      this.httpService.get(url + temp_lwf).subscribe((data: any) => {
        this.licenceNoerrordata = data["result"];
        this.msg = data.message;
        this.toastr.error(this.msg);
        this.errflag10 = true;
      },
      (err: HttpErrorResponse) => {
        this.errflag10 = false;
      });
    }
  }

  isEdit2: boolean = false;
  editindex: number = 0;
  index: number;

  AddValue(ff) {
    for (let i = 0; i < this.allActiveData.length; i++) {
      if (this.allActiveData[i].locationId == this.selectedLocation) {
        this.selectedLocation = this.allActiveData[i].locationName;
      }
    }
    this.licenceNo = this.ContractMasterModel.licenceNo;
    this.locationName = this.selectedLocation;
    this.locationId = this.chooselocation;
    let obj = {
      "licenceNo": this.licenceNo,
      "locationMaster":{
        "locationName": this.locationName,
        "locationId": this.locationId
      }
    }
    this.addOnArray.push(obj);
    ff.reset();
  }
  editres(c, b) {
    // this.locationName = this.addOnArray['locationName'];
    this.ContractMasterModel.licenceNo=c.licenceNo;
    this.selectedLocation=c.locationMaster.locationId;
    this.locationId=c.locationMaster.locationId;
    this.isEdit2 = true;
  }
  deleteFlag: boolean;
  indexDelete: number;
  deleteres(b) {
    this.deleteFlag = true;
    this.indexDelete = b;
    this.addOnArray.splice(b, 1);
  }
  reset(f: NgForm) {
    this.isedit = false;
    this.StatusFlag = false;
    //f.reset();
    this.resetForm();
    this.ContractMasterModel = new ContractorMaster();
    this.selectedLocation =[];
    this.selectedLocation='';
    this.ContractMasterModel.licenceNo='';
    
  }
  Cancel(){
    this.router.navigateByUrl('/layout/settingpage')
  }
}
