import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Contractormaster1Component } from './contractormaster1.component';

describe('Contractormaster1Component', () => {
  let component: Contractormaster1Component;
  let fixture: ComponentFixture<Contractormaster1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Contractormaster1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Contractormaster1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
