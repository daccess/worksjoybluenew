import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorservicetypeComponent } from './contractorservicetype.component';

describe('ContractorservicetypeComponent', () => {
  let component: ContractorservicetypeComponent;
  let fixture: ComponentFixture<ContractorservicetypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorservicetypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorservicetypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
