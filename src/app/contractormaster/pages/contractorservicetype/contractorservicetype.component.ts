import { Component, OnInit } from '@angular/core';
import { MainURL } from '../../../shared/configurl';
import { ContractServiceTypeModel } from 'src/app/shared/model/ContractorTypeServiceModel';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { dataService } from 'src/app/shared/services/dataService';
import { Router } from "@angular/router";


@Component({
  selector: 'app-contractorservicetype',
  templateUrl: './contractorservicetype.component.html',
  styleUrls: ['./contractorservicetype.component.css']
})
export class ContractorservicetypeComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  ContractorServiceModel:ContractServiceTypeModel
  ServiceTypedata: any;
  compId: any;
  conGovTypeName:string
  StatusFlag: boolean = false;
  status: ContractServiceTypeModel;
  flag: Boolean;
  loggedUser: any;
  isedit = false;
  dltmodelFlag: boolean;
  allServiceTypedata: any;
  Selectededitobj: any;
  dltObje: any;
  contractor_service_url='CosSerType/Active';
  contractor_service_data:any;
  constructor(public httpService: HttpClient,public toastr:ToastrService,private dataservice:dataService,private router: Router) {
    this.ContractorServiceModel=new ContractServiceTypeModel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.compId =  this.loggedUser.compId;
    this.getAllcontractServiceData();
   }

  ngOnInit() {
    this.getContractorServiceType();
  }

  getAllcontractServiceData(){
    this.ContractorServiceModel.status = this.flag; 
    let url1 = this.baseurl + '/getContractorValidityList';
    this.httpService.get(url1).subscribe(
      data => {
       this.allServiceTypedata = data["result"];
       setTimeout(function () {
        $(function () {
          var table = $('#Business').DataTable();
        });
      }, 1000);
      },
      (err: HttpErrorResponse) => {
      });
  }
    onSubmit(f){
      if(this.ContractorServiceModel.validityMonths==null){
        this.toastr.error('Please select validity months..!', 'Contractor ServiceType-Master');
        return false;
      }
      this.StatusFlag=false;
    this.ContractorServiceModel.status = this.flag;
    if(this.isedit == false){
      this.ContractorServiceModel.compId = this.compId;
      let url1 = this.baseurl + '/createContractorValidity';
      this.httpService.post(url1 , this.ContractorServiceModel).subscribe(
        data => {
          if(data['message']!='Creation Failed'){
            this.ServiceTypedata = data["result"];
            this.toastr.success('Contractor Validity master added Successfully!');
            this.getAllcontractServiceData();
            f.reset()
          }
        },
        (err: HttpErrorResponse) => {
          this.toastr.error('Something went wrong..!', 'Contractor ServiceType-Master');
        });
    }
    else{
      let url3 = this.baseurl + '/updateContractorValidity';
      this.httpService.put(url3,this.ContractorServiceModel).subscribe(data => {
        f.reset();
        this.toastr.success('Contractor ServiceType master updated successfully!', 'Contractor ServiceType Master');
        this.getAllcontractServiceData();
        this.isedit=false;
      },
      (err: HttpErrorResponse) => {
        this.isedit=false;
        this.toastr.error('Something went wrong while updating..!');
      });
    }
  }
  
  editServiceType(object) {
    this.Selectededitobj = object.conValidityId;
    this.ContractorServiceModel.conValidityId = this.Selectededitobj;
    this.isedit = true;
    this.StatusFlag=true;
    this.flag = object.status;
    this.editServiceTypeobject(this.Selectededitobj);
  }
  editServiceTypeobject(Selectededitobj){
    let urledit = this.baseurl + '/getContractorValidityById/';
    this.httpService.get(urledit + Selectededitobj).subscribe(data => {
      this.ContractorServiceModel.status = data['result']['status'];
      this.flag = data['result']['status'];
      this.ContractorServiceModel.conGovTypeName = data['result']['conGovTypeName'];
      this.ContractorServiceModel.validityMonths = data['result']['validityMonths'];
      this.ContractorServiceModel.rejoinAllowed = data['result']['rejoinAllowed'];
      this.ContractorServiceModel.extOfValidity = data['result']['extOfValidity'];
      if(data['result'].contractorServiceTypeMaster){
        this.ContractorServiceModel.conSerTypeId=data['result'].contractorServiceTypeMaster.conSerTypeId;
      }
    },
    (err: HttpErrorResponse) => {
    });
  }

  dltmodelEvent(){
    this.dltmodelFlag=true;
    this.deleteServiceType(this.dltObje);
  }

  dltObj(obj){
    this.dltmodelFlag=false;
    this.dltObje = obj.conValidityId;
    this.deleteServiceType(this.dltObje);
  }
  deleteServiceType(object){
    if (this.dltmodelFlag == true) {
      this.ContractorServiceModel.conValidityId=this.dltObje
      let url2=this.baseurl + '/deleteContractorValidityMaster/';
      this.httpService.delete(url2 + this.dltObje ).subscribe(data => {
        if(data['statusCode']==200){
          this.toastr.success('Contractor ServiceType Master Deleted successfully')
          this.getAllcontractServiceData();
        }
        },(err: HttpErrorResponse) => {
          this.toastr.error('Something went wrong while deleting...')
      });
    }
  }
  uiswitch(event){
    
  }
  open(c){
    this.flag = !this.flag;
  }
  //Reset
  Reset(){
    this.ContractorServiceModel=new ContractServiceTypeModel();
  }
  //getContractorServiceType
  getContractorServiceType(){
    if(this.compId){
      this.dataservice.getRecordsByid(this.contractor_service_url,this.compId).subscribe(res=>{
        if(res.statusCode==200){
          this.contractor_service_data=res.result;
        }
      })
    }
  }
  Cancel(){
    this.router.navigateByUrl('/layout/settingpage');
  }
}
