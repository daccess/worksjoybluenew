import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { ContractormasterComponent } from './contractormaster.component';
import { ContratcmComponent } from './pages/contratcm/contratcm.component';
import { Contractormaster1Component } from './pages/contractormaster1/contractormaster1.component';
import { ContractorservicetypeComponent } from './pages/contractorservicetype/contractorservicetype.component';
const appRoutes: Routes = [
    { 
             
        path: '', component: ContractormasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'contractmaster',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'contractmaster',
                    component: ContratcmComponent
                },
                {
                    path:'contract',
                    component: Contractormaster1Component
                },
                {
                    path:'contractorservicetype',
                    component: ContractorservicetypeComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);