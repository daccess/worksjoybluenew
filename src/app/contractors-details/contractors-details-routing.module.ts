import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorsDetailsComponent } from './contractors-details.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorsDetailsComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorsDetails',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'contractorsDetails',
                  component: ContractorsDetailsComponent
              }

          ]

  }


]

export const routingforContractor = RouterModule.forChild(appRoutes);