import { ContractorsDetailsModule } from './contractors-details.module';

describe('ContractorsDetailsModule', () => {
  let contractorsDetailsModule: ContractorsDetailsModule;

  beforeEach(() => {
    contractorsDetailsModule = new ContractorsDetailsModule();
  });

  it('should create an instance', () => {
    expect(contractorsDetailsModule).toBeTruthy();
  });
});
