import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {routingforContractor } from './contractors-details-routing.module';
import { ContractorsDetailsComponent } from './contractors-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  imports: [
    CommonModule,
    routingforContractor,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [
    ContractorsDetailsComponent
  ]
})
export class ContractorsDetailsModule { }
