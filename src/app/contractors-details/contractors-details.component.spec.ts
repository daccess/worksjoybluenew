import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorsDetailsComponent } from './contractors-details.component';

describe('ContractorsDetailsComponent', () => {
  let component: ContractorsDetailsComponent;
  let fixture: ComponentFixture<ContractorsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
