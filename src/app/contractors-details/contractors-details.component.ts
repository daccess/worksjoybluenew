import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-contractors-details',
  templateUrl: './contractors-details.component.html',
  styleUrls: ['./contractors-details.component.css']
})
export class ContractorsDetailsComponent implements OnInit {
  isDisabled = true;
  fileurl = 'assets/file/Daily_Manual_Transaction.xlsx';
  user: string;
  users: any;
  token: any;
  baseurl = MainURL.HostUrl;
  // contractorServiceType: any;
  companyName: any;
  firstName: any;
  mobileNo: any;
  emergencyContactNumber: any;
  email_id: any;
  typeOfCom: any
  contractordata: any;
  empIds: any;
  contractorAllData: any;
  typeOfCompany: any;
  emailId: any;
  conSerTypeId: any;

  ownerName: any;
  wcNumber: any;

  pfNumber: any;
  fileToUpload: File = null;
  obj: any = {};
  obj1: any = {};
  obj2: any = {};
  obj3: any = {};
  imageUrl: string;
  wcPolicyDocument: any
  gstNumber: any
  receiptUrl: any;
  imagevalidation: any;
  imageUrl1: any;
  receiptUrl1: any;
  imageUrl2: any;
  imageUrl3: any;
  receiptUrl2: any;
  receiptUrl3: any;
  imagevalidation1: string;
  wcLicExpDate: any;
  labourLicExpDate: any;
  addOnArray = [];
  shortimage1: any;
  areaOfwork: any;
  labourLicense: any;
  shortImage: string;
  conMasterId: any;
  conCompanyName: any;
  serviceTypeFlag: boolean = false
  conSerTypeIdList: any;
  contractorServiceType = [];
  locationMastersLis = []

  dropdownSettings: {}
  dropdownSettings1: {}
  locationIDList: any;
  selectedFileName: any;
  serviceFromDate: any;
  serviceToDate: any;
  shortImage2: any;


  constructor(private httpservice: HttpClient, public Spinner: NgxSpinnerService, public toastr: ToastrService) { }

  ngOnInit() {

    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token
    this.empIds = this.users.roleList.empId

    this.getContractorByID();

    // this.getContractorServiceType()
    this.getContractorLocation();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'conSerTypeId',
      textField: 'conSerTypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 100,
      allowSearchFilter: true,
    }
    this.dropdownSettings1 = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 100,
      allowSearchFilter: true,
    }
  }

  handleFileInputone(event: any) {
    const files = event.target.files;
    if (files.length > 0) {
      this.selectedFileName = files[0].name; // Set the selected file name
    } else {
      this.selectedFileName = ''; // Clear the file name if no file is selected
    }
  }




  trackByOptionId(index: number, contractorServiceType: any): number {
    return contractorServiceType.id;
  }
  getContractorServiceType() {
    let url = this.baseurl + '/getAllContractorServiceTypes';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.contractorServiceType = data['result']
      for (let index = 0; index < this.contractorServiceType.length; index++) {
        this.contractorServiceType[index].conSerTypeName = this.contractorServiceType[index].conSerTypeId + "-" + this.contractorServiceType[index].conSerTypeName;

      }


    },
      (err: HttpErrorResponse) => {

      })

  }
  getContractorLocation() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.locationMastersLis = data['result']
      for (let index = 0; index < this.locationMastersLis.length; index++) {
        this.locationMastersLis[index].locationName = this.locationMastersLis[index].locationId + "-" + this.locationMastersLis[index].locationName;

      }


    },
      (err: HttpErrorResponse) => {

      })

  }


  toggleExpanded() {

    const button = document.getElementById('heading-1');
    const isExpanded = button.getAttribute('aria-expanded') === 'true';

    if (isExpanded) {
      button.setAttribute('aria-expanded', 'false');
    } else {
      button.setAttribute('aria-expanded', 'true');
    }
  }
  getContractorByID() {

    let url = this.baseurl + `/getContractorByEmpId?empId=${this.empIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.contractordata = data;
      this.contractorAllData = this.contractordata.result;
      this.conCompanyName = this.contractorAllData.conCompanyName;
      this.mobileNo = this.contractorAllData.mobileNo;
      this.typeOfCompany = this.contractorAllData.typeOfCompany;
      this.emailId = this.contractorAllData.emailId;
      this.areaOfwork = this.contractorAllData.areaOfwork;

      this.emergencyContactNumber = this.contractorAllData.emergencyContactNumber;

      this.conSerTypeId = this.contractorAllData.conSerTypeId;

      this.ownerName = this.contractorAllData.firstName;
      this.serviceFromDate = this.contractorAllData.fromDate;
      this.serviceToDate = this.contractorAllData.toDate;
      this.pfNumber = this.contractorAllData.pfNumber;
      this.wcNumber = this.contractorAllData.wcNumber;
      this.wcLicExpDate = this.contractorAllData.wcLicExpDate;
      this.labourLicExpDate = this.contractorAllData.labourLicExpDate;
      this.imageUrl1 = this.contractorAllData.wcPolicyDocument;
      this.imageUrl3 = this.contractorAllData.gstNumber;

      this.imageUrl = this.contractorAllData.labourLicense;
      this.conMasterId = this.contractorAllData.conMasterId;
      this.imageUrl2 = this.contractorAllData.compLogoUrl;

      this.conSerTypeIdList = this.contractorAllData.contractorServiceTypes;
      this.locationIDList = this.contractorAllData.locationMastersList;
      this.getContractorServiceType();
    },
      (err: HttpErrorResponse) => {

      })
  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    this.obj.name = file.item(0).name;
  
    // Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
      this.receiptUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  
    let flag1 = false;
    if (file) {
      var img = new Image();
      img.src = window.URL.createObjectURL(file[0]);
  
      img.onload = () => {
        var width = img.naturalWidth,
          height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
  
        if (width < 1000 && height < 1000) {
          flag1 = true;
        } else {
          flag1 = false;
        }
      };
  
      setTimeout(() => {
        if (flag1) {
          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.imageUrl = event.target.result;
          };
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation = "false";
        } else {
          this.toastr.error("Please upload an image up to 1 MB");
          this.imagevalidation = "true";
        }
      }, 300);
    }
  }
  
  downloadImage() {
    const link = document.createElement('a');
    link.href = this.imageUrl;
    link.download = 'downloaded_image.png';
    link.click();
  }
  

  handleFileInput1(file: FileList) {
    this.fileToUpload = file.item(0);
    this.wcPolicyDocument = this.fileToUpload.name;




    //       this.fileToUpload = file.item(0);
    // this.labourLicense = this.fileToUpload.name;



    this.fileToUpload = file.item(0);
    this.obj1.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl1 = event.target.result.substring(0, 10);
      this.receiptUrl1 = event.target.result
    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.imageUrl1 = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation1 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation1 = "true"
        }
      }, 300);
    }
  }





  getCurrentDate(): string {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, '0');
    const day = String(today.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  AddData(v) {

    this.shortimage1 = this.imageUrl1.substring(0, 10);
    this.shortImage = this.imageUrl.substring(0, 10);
    let obj = {

      "wcLicExpDate": this.wcLicExpDate,
      "labourLicExpDate": this.labourLicExpDate,
      "wcPolicyDocument": this.shortimage1,
      "labourLicense": this.shortImage,
      "gstNumber": this.shortImage2,
      // "details":th

    }
    console.log("this is the expense", obj)
    this.addOnArray.push(obj)
    this.wcLicExpDate = ''
    this.labourLicExpDate = ''
    this.imageUrl1 = ''
    this.imageUrl = ''
  }
  handleFileInput3(file: FileList) {

    this.fileToUpload = file.item(0);
    this.obj2.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl2 = event.target.result.substring(0, 10);;
      this.receiptUrl2 = event.target.result

      this.obj2.name = file.item(0).name;


    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.imageUrl2 = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation = "true"
        }
      }, 300);
    }
  }


  // add new for gst Number
  handleFileInput4(file: FileList) {

    this.fileToUpload = file.item(0);
    this.obj3.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl3 = event.target.result.substring(0, 10);;
      this.receiptUrl3 = event.target.result

      this.obj3.name = file.item(0).name;


    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.imageUrl3 = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation = "true"
        }
      }, 300);
    }
  }






  selectServiceType1() {
    if (this.contractorServiceType.length == 0) {
      this.serviceTypeFlag = true;
    }
    else {
      this.serviceTypeFlag = false;
    }
  }
  onItemSelect1(item: any) {

    const id = item;
    if (this.contractorServiceType.length == 0) {
      this.contractorServiceType.push(item)
      // this.selectEmp(id);
    } else {
      for (var i = 0; i < this.contractorServiceType.length; i++) {
        if (this.contractorServiceType[i].conSerTypeId == item.conSerTypeId) {

        } else {
          this.contractorServiceType.push(item)
        }
      }

    }
  }
  onSelectAll1(items: any) {
    this.contractorServiceType = items;

  }
  OnItemDeSelect1(item: any) {

    for (var i = 0; i < this.contractorServiceType.length; i++) {
      if (this.contractorServiceType.length == 0) {

        if (this.contractorServiceType[i].conSerTypeId == item.conSerTypeId) {
          this.contractorServiceType.splice(i, 1);
        }
      } else {

        if (this.contractorServiceType[i].conSerTypeId == item.conSerTypeId) {
          this.contractorServiceType.splice(i, 1);
        }

      }
    }
  }
  onDeSelectAll1(item: any) {
    this.contractorServiceType = [];
  }

  selectServiceType2() {
    if (this.locationMastersLis.length == 0) {
      this.serviceTypeFlag = true;
    }
    else {
      this.serviceTypeFlag = false;
    }
  }
  onItemSelect2(item: any) {

    const id = item;
    if (this.locationMastersLis.length == 0) {
      this.locationMastersLis.push(item)
      // this.selectEmp(id);
    } else {
      for (var i = 0; i < this.locationMastersLis.length; i++) {
        if (this.locationMastersLis[i].locationId == item.locationId) {

        } else {
          this.locationMastersLis.push(item)
        }
      }

    }
  }
  onSelectAll2(items: any) {
    this.locationMastersLis = items;

  }
  OnItemDeSelect2(item: any) {

    for (var i = 0; i < this.locationMastersLis.length; i++) {
      if (this.locationMastersLis.length == 0) {

        if (this.locationMastersLis[i].locationId == item.locationId) {
          this.contractorServiceType.splice(i, 1);
        }
      } else {

        if (this.locationMastersLis[i].locationId == item.locationId) {
          this.locationMastersLis.splice(i, 1);
        }

      }
    }
  }
  onDeSelectAll2(item: any) {
    this.locationMastersLis = [];
  }
  resetForm() {
    this.labourLicense = null;
    this.gstNumber = null;
    this.labourLicExpDate = null;
    this.wcPolicyDocument = null;
    this.wcLicExpDate = null;
  }

  submitContractorDetails() {
    let obj = {
      "conMasterId": this.conMasterId,
      "wcLicExpDate": this.wcLicExpDate,
      "labourLicExpDate": this.labourLicExpDate,
      "wcPolicyDocument": this.imageUrl1,
      "labourLicense": this.imageUrl,
      "gstNumber": this.imageUrl3,
      "conCompanyName": this.conCompanyName,
      "mobileNo": this.mobileNo,
      "typeOfCompany": this.typeOfCompany,
      "emailId": this.emailId,
      "emergencyContactNumber": this.emergencyContactNumber,
      "conSerTypeId": this.conSerTypeId,

      "ownerName": this.ownerName,
      "pfNumber": this.pfNumber,
      "wcNumber": this.wcNumber,
      "compLogoUrl": this.imageUrl2,
      "contractorServiceTypes": this.contractorServiceType,
      "locationMastersList": this.locationMastersLis,
      "areaOfwork": this.areaOfwork,
      "serviceFromDate": this.serviceFromDate,
      "serviceToDate": this.serviceToDate

    }

    let url = this.baseurl + '/updateContractorMaster';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj, { headers }).subscribe(data => {
      this.toastr.success("contractor details updated successfully")

      // this.contractorServiceType=data['result']

    },
      (err: HttpErrorResponse) => {

      })
  }
  collapsenext() {

    var toggleButton = document.getElementById('toggleButton');
    var collapseElement = document.getElementById('collapse-2');

    toggleButton.addEventListener('click', function () {
      var isExpanded = collapseElement.classList.contains('show');
      toggleButton.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    });
  }
  collapsenext1() {

    var toggleButton = document.getElementById('toggleButton1');
    var collapseElement = document.getElementById('collapse-3');

    toggleButton.addEventListener('click', function () {
      var isExpanded = collapseElement.classList.contains('show');
      toggleButton.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    });
  }
  isContentExpanded = false;

  toggleContent() {

    if (this.ownerName == null || this.emergencyContactNumber == null || this.typeOfCompany == null || this.areaOfwork == null) {
      this.toastr.error("Please fill all Mandatory Field")
    }
    else {
      this.isContentExpanded = !this.isContentExpanded;
    }
  }

}
