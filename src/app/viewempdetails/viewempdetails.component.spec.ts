import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewempdetailsComponent } from './viewempdetails.component';

describe('ViewempdetailsComponent', () => {
  let component: ViewempdetailsComponent;
  let fixture: ComponentFixture<ViewempdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewempdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewempdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
