import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ViewempdetailsComponent } from 'src/app/viewempdetails/viewempdetails.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ViewempdetailsComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'viewempdetails',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'viewempdetails',
                    component: ViewempdetailsComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);