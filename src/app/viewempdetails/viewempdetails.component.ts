import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewempdetails',
  templateUrl: './viewempdetails.component.html',
  styleUrls: ['./viewempdetails.component.css']
})
export class ViewempdetailsComponent implements OnInit {
  
  getEmpID: any;
  baseurl = MainURL.HostUrl;
  EmployeePersondata: any;

  empGender: any;
  empfullName: any;
  empNationality: any;
  empDOB: any;
  empAge: any;
  empPlaceOfBirth: any;
  empMaritalStatus: any;
  empMealPreference: any;
  empKnownLanguage: any;
  empNameOfPerson: any;
  emprelation: any;
  empRelation: any;
  empAddress: any;
  empwhatsAppNumber: any;
  emppinCode:any;
  empcity: any;
  empbloodGroup: any;
  empwieght: any;
  empHeight: any;
  empPhysicalDisability: any;
  empHealthIssues: any;
  empFamilyDetails: any;
  dependancy: any;
  empDependency: any;
  bioId: any;
  empId: any;
  empAddressdata: any;
  empAddressDatacity: any;
  pinCode1: any;
  skypeId: any;
  whatsAppNumber: any;
  empcurrentyStudying: any;
  empsubOfSpecialisation: any;
  empAlldataEducation: any;

  empCostCenterNumber: any;
  empBusinessGroup: any;
  empFunctionUNit: any;
  empDepartmentName: any;
  empSubdepartmentName: any;
  empLocation: any;
  empInGlobalProjet: any;
  empDesigntaionName: any;
  empGradeName: any;
  empEmployeementType: any;
  empSubEmployeementType: any;
  empCategory: any;
  empEmployeementSubcategory: any;
  empSkillcategory: any;
  empEmployeeStatus: any;
  empStartDateOfTraining: any;
  empEndDateOFtraining: any;
  empStartDateOfProbabation: any;
  empDateOfConfirmation: any;
  empFirstLevelReportingManager: any;
  empSecondLevelReportingManager: any;
  empThirdLevelReportingManager: any;
  empSanctionigAuthority: any;
  loggedUser: any;
  compId: any;
  empOfficialId: any;
  empPerId: any;

  constructor(public httpService: HttpClient,public chRef: ChangeDetectorRef,private router: Router,private route: ActivatedRoute) { 
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.compId =  this.loggedUser.compId;
      this.empOfficialId = this.loggedUser.empOfficialId;
      this.empPerId = this.loggedUser.empPerId;
    }

  ngOnInit() {
    this.getEmpID = sessionStorage.getItem('EmpData');
    this.getEmployeelist(this.getEmpID);
  }

  getEmployeelist(getEmpID){
    let url = this.baseurl + '/ViewEmployee/';
    this.httpService.get(url + getEmpID).subscribe(data => {
        this.EmployeePersondata = data["result"];
        this.bioId = this.EmployeePersondata.bioId;
        this.empId = this.EmployeePersondata.empId;
        this.empGender = this.EmployeePersondata.gender;
        this.empfullName = this.EmployeePersondata.fullName;
        this.empNationality = this.EmployeePersondata.nationality;
        this.empDOB = this.EmployeePersondata.dateOfBirth;
        this.empAge = this.EmployeePersondata.age;
        this.empPlaceOfBirth = this.EmployeePersondata.placeOfBirth;
        this.empMaritalStatus = this.EmployeePersondata.maritialStatus;
        this.empMealPreference = this.EmployeePersondata.regularMealPreference
        this.empKnownLanguage = this.EmployeePersondata.empKnownLangugesResDtoList;
        this.empNameOfPerson = this.EmployeePersondata.nameOfPerson;
        this.empRelation = this.EmployeePersondata.relation;
        this.empAddress = this.EmployeePersondata.address;
        this.empwhatsAppNumber = this.EmployeePersondata.whatsAppNumber;
        this.emppinCode = this.EmployeePersondata.pinCode;
        this.empcity = this.EmployeePersondata.city;
        this.empbloodGroup = this.EmployeePersondata.bloodGroup;
        this.empwieght = this.EmployeePersondata.wieght;
        this.empHeight = this.EmployeePersondata.height;
        this.empPhysicalDisability = this.EmployeePersondata.physicalDisability;
        this.empHealthIssues = this.EmployeePersondata.healthIssues;
        this.empFamilyDetails = this.EmployeePersondata.empFamilyMembersResDtoList;
        this.empAddressdata = this.EmployeePersondata.address1;
        this.empAddressDatacity = this.EmployeePersondata.city1;
        this.pinCode1 = this.EmployeePersondata.pinCode1;
        this.skypeId = this.EmployeePersondata.skypeId;
        this.whatsAppNumber = this.EmployeePersondata.whatsAppNumber;
        this.empcurrentyStudying = this.EmployeePersondata.currentyStudying;
        this.empsubOfSpecialisation = this.EmployeePersondata.subOfSpecialisation;
        this.empAlldataEducation = this.EmployeePersondata.empEducationalDetailsResDtoList;
        this.empCostCenterNumber = this.EmployeePersondata.costCenterName;
        this.empFunctionUNit = this.EmployeePersondata.functionUnitName;
        this.empSubdepartmentName = this.EmployeePersondata.subDeptName;
        this.empBusinessGroup = this.EmployeePersondata.busiGrupName;
        this.empDepartmentName = this.EmployeePersondata.deptName;
        this.empLocation = this.EmployeePersondata.locationName;
        this.empGradeName = this.EmployeePersondata.gradeName;
        this.empDesigntaionName = this.EmployeePersondata.descName;
        this.empInGlobalProjet = this.EmployeePersondata.contributingToGlobalProjects;
        this.empEmployeementType = this.EmployeePersondata.employmentType;
        this.empSubEmployeementType = this.EmployeePersondata.subEmploymentType;
        this.empSkillcategory = this.EmployeePersondata.skillCatagory;
        this.empStartDateOfTraining = this.EmployeePersondata.startDateOfTraining;
        this.empEndDateOFtraining = this.EmployeePersondata.endDateOfTraining;
        this.empStartDateOfProbabation = this.EmployeePersondata.startDateOfprobation;
        this.empEmployeeStatus = this.EmployeePersondata.empStatus;
        this.empSanctionigAuthority = this.EmployeePersondata.sanctioningAuthority;
      },
      (err: HttpErrorResponse) => {
      });
  }
}
