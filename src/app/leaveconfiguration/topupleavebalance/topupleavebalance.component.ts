import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MainURL } from 'src/app/shared/configurl';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { leaveConfigurationService } from '../shared/services/leaveConfigurationService';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { dataService } from 'src/app/shared/services/dataService';
import { LayOutService } from 'src/app/shared/model/layOutService';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { datePipe } from 'src/app/shared/pipes/custom-date-pipe';


@Component({
  selector: 'app-topupleavebalance',
  templateUrl: './topupleavebalance.component.html',
  styleUrls: ['./topupleavebalance.component.css']
})
export class TopupleavebalanceComponent implements OnInit {
  themeColor: string;
  topUpLeaveModel : topUpLeaveModel;
  baseurl = MainURL.HostUrl;
  

Employeedata: any = [];
compId: number;
loggedUser: any;
leaveConfigList1:any = [];
leaveConfigList:any = [];
empPerId: any;
empId:any;
balanceId:any;
loggedUserempperId :any;
selectedempPerId:any;
selectleavetypeid:any;
leavePatternId:any;
leaveid:any;
leavetype_id:any =" ";
leave_url='getLeaveDataForTopUp';
savetopleavebalance_url = '/saveTopUpLeaveBalance';
gettopleavebalancelist_url = '/getTopUpLeaveBalanceDetails';
gettopbalanceleavelist:any =[];
leavepatternlist :any = [];
empSearchSuggestionList = [];
dataTableFlag : boolean = true;

getleavefortopbalancebyleaveid_url = 'getLeaveDataForTopUpLeave'; 
  constructor(public httpService: HttpClient, private leaverequestService:  leaveConfigurationService,private chRef: ChangeDetectorRef, private layOutService : LayOutService,private dataservice :dataService, public Spinner: NgxSpinnerService,private toastr: ToastrService,private datePipe: DatePipe) {
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.topUpLeaveModel = new topUpLeaveModel();
    this.compId = this.loggedUser.compId;
    this.loggedUserempperId =this.loggedUser.empPerId;
  
  
   }
   selectedItems = []
   dropdownSettings : any =[];
  ngOnInit() {
    this.getAllEmployee();
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'empPerId',
      textField: 'fullName',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
  }

selectLeaveType(value) {
    this.leavetype_id = value;
    for (var i = 0; i < this.leaveConfigList.length; i++) {
    if (this.leaveConfigList[i].leaveId ==  value){
       this.balanceId=this.leaveConfigList[i].currentLeaves;
    }
    this.getLeavedataforTopleavebalance(this.leavetype_id); 
 }
  }
  selectLeavePattern(value){
   this.leavePatternId =value;
  }
   getAllEmployee() {
    if(this.loggedUser.roleHead=='Admin'){
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(
      data => {
        this.Employeedata = data["result"];
        for (let index = 0; index < this.Employeedata.length; index++) {
          this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName ;
          
        }
       },(err: HttpErrorResponse) => {
        this.Employeedata =[];
      });
    }
    else if(this.loggedUser.roleHead=='HR'){
    
      let url = this.baseurl + '/EmployeeAttendanceProcess/';

    this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(
      data => {
        this.Employeedata = data["result"];
        for (let index = 0; index < this.Employeedata.length; index++) {
          this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName ;
          
        }
       },(err: HttpErrorResponse) => {
        this.Employeedata =[];
      });
    }
  }

  onItemSelect(item: any) {
    this.empId= item.empId;
    this.empPerId =item.empPerId ;
    this.leaverequestService.getRecordsByid(this.leave_url,this.empPerId).subscribe((data: any) => {
      this.leaveConfigList = data.result;
    }, err => {
    })
    this.leaveConfigList = [];
  }
    
  onSubmit(f){
  this.Spinner.show();
   this.topUpLeaveModel.empId =this.empId;
   this.topUpLeaveModel.empPerId =this.empPerId ;
    this.topUpLeaveModel.leaveBalanceId =this.balanceId;
    this.Spinner.show();
    this.leaverequestService.createRecord(this.savetopleavebalance_url,this.topUpLeaveModel).subscribe((data: any) => {
        if(data.statusCode==201){
          var totalleavecount = this.balanceId + this.topUpLeaveModel.topUpBal;
          this.toastr.success("TopUpLeave balance = " + totalleavecount + " Added Sucessfully !");
          f.reset();
          this.Spinner.hide();
        }
      }, err => {
        this.toastr.error("Something went wrong..!!");
      })
  }

  refresh_tab(){
     var table = $('#Business').DataTable().destroy();
    this.dataservice.getRecordsNoFilter(this.gettopleavebalancelist_url).subscribe((data:any)=>{
      data.result.forEach(element => {
        element.name = element.fullName +' '+element.lastName;
        delete element.fullName;
        delete element.lastName;
      });
      this.gettopbalanceleavelist =data.result;
      this.dataTableFlag = true;
      this.chRef.detectChanges();
      var i = 1;
      var t = $('#Business').DataTable({
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0,
            
        },
        {
          "targets": [ 6 ],
          "visible": false
      }
       ],
       "data": Object.values(this.gettopbalanceleavelist),
       
        "columns": [
          
            { "data": "empId" },
            { "data": "name" },
            { "data": "leaveName" },
            { "data": "patternName" },
            { "data": "topUpBal" },
            { "data": "topUpBalEntryDate",
            render: function (data, type, row, meta) {
              var date = new Date(data);
              var month = date.getMonth() + 1;
              return ( date.getDate() + "-" +(month.toString().length > 1 ? month : "0" + month) + "-" + date.getFullYear());

            }
          },
          { "data": "topUpBalEntryDate" },
            
        ],
        "order": [[ 6, 'desc' ] ]
     });
   this.chRef.detectChanges();
    this.Spinner.hide();
    },err =>{
         this.gettopbalanceleavelist = [];
    })
   }
getLeavedataforTopleavebalance(leaveid){
  this.leaverequestService.getRecordsByid(this.getleavefortopbalancebyleaveid_url,leaveid).subscribe((data: any) => {
    this.leavepatternlist = data.result;
  }, err => {
    this.leavepatternlist =[];
  })
}
reset(f:NgForm) {
    f.resetForm();
  setTimeout(() => {    
      this.topUpLeaveModel.empId = "";
     this.topUpLeaveModel.empPerId = 0;
      this.topUpLeaveModel.leaveBalanceId = 0 ;
      this.topUpLeaveModel.topUpBal;
    }, 1000);
  }

}
export class topUpLeaveModel {
  empId: string;
  empPerId: 0;
  leaveBalanceId: 0;
  leavePatternId: 0;
  topUpBal: 0;
  hrRequestToWorkReqDtoList =[];
}