import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopupleavebalanceComponent } from './topupleavebalance.component';

describe('TopupleavebalanceComponent', () => {
  let component: TopupleavebalanceComponent;
  let fixture: ComponentFixture<TopupleavebalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopupleavebalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopupleavebalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
