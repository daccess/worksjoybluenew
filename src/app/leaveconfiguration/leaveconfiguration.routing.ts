import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { LeaveconfigurationComponent } from 'src/app/leaveconfiguration/leaveconfiguration.component';
import { TopupleavebalanceComponent } from './topupleavebalance/topupleavebalance.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: LeaveconfigurationComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leaveconfiguration',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaveconfiguration',
                    component: LeaveconfigurationComponent
                }
                
            ]

    },
    {
        path:'',
        component: TopupleavebalanceComponent,
        children:[
            {
                path:'',
                redirectTo : 'topupleavebalance',
                pathMatch :'full'
                
            },
        
            {
                path:'topupleavebalance',
                component: TopupleavebalanceComponent
            }
            
        ]
    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);