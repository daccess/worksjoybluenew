import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { leaveConfigurationService } from './shared/services/leaveConfigurationService';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../../app/shared/configurl';
import { from, empty } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { LeaveModel } from './shared/models/leaveConfigurationModel';
import { NgxSpinnerService } from 'ngx-spinner';
import { event } from 'jquery';
@Component({
  selector: 'app-leaveconfiguration',
  templateUrl: './leaveconfiguration.component.html',
  styleUrls: ['./leaveconfiguration.component.css']
})
export class LeaveconfigurationComponent implements OnInit {

  leaveflag= false;
  dltmodelFlag: boolean;


  baseurl = MainURL.HostUrl;
  model: any = {
    genderMasterList: [], leaveClubbingListNew: [], employmentTypes: [], subEmploymentTypes: [], leaveClubbingList: [], employeeTypeMasterList: [], subEmployeeTypeMasters: [], gradeMasterList: [], locationMasterList: []
    , employementTypeName: [], subEmployementTypeName: [], leaveTypeToMerge: [], DepartMasterList: []
  };
  companyId: any;
  gradeList: any = [];
  selectedGrade: any = [];
  locationData: any = [];
  employmentTypeData: any = [];
  LeaveEncashData: any = [];
  selectedLocation: any = [];
  selectedEmploymentType: any = [];
  selectedLeaveEncash: any = [];
  selectAll: boolean = false;
  dropdownSettingsGrade: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsLocation: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsEmploymentType: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsLeaveEncash: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };

  AttendenceConfiAllData = [];
  isEdit: boolean = false;
  GenderList = [
    { id: 1, gender: "Male" },
    { id: 2, gender: "Female" },
    { id: 3, gender: "Other" }
  ];
  selectedGenderList: any = [];

  usedStatus: boolean;

  dropdownSettingsGender: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsLeaveClubbing: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  selectedLeaveConfig = [];
  employmentType = [];
  SubEmploymentType = [];
  dropdownSettingsEmployeeCategory: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dropdownSettingsSubEmploymentType: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };

  themeColor: string = "nav-pills-blue";

  leaveModel: LeaveModel;
  dropdownSettingsSubEmployeeType: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  changeleaveId: any;
  errordata: any;
  errflag: boolean;
  getcmp: string;
  leaveName: any;
  leavePatternList: any;
  leaveId: number;
  Leavelistflag: boolean;
  leavePtternId: number;
  hideButtonFlag: boolean
  leaveoOrflag: boolean;
  leaveOrflag: boolean;
  patternName: any;
  msg: any;
  lerrflag: boolean;
  leaveCode: String;
  getCode: String;
  forPatternId: any;
  leave: any
  name: string;
  loggedUser: any;
  departmentlistdata = [];
  dropdownSettingsDepat: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  dltObje: any;
  validation: any;
  texts: any;
  require: boolean;
  requires: boolean;


  constructor(public toastr: ToastrService, public Spinner: NgxSpinnerService, public httpService: HttpClient, public chRef: ChangeDetectorRef, private leaveService: leaveConfigurationService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);

    this.companyId = this.loggedUser.compId;
    this.leave = ""
    this.model.status = true;
    this.model.maxApplUnit = "Select";
    this.model.carryForwardFrequency = "Select";
    this.model.leaveTransferOn = "On Confirmation";
    this.model.creditOnEvent = "ON Joining";
    this.model.roundedOff = "Select";
    this.model.calculateOnConfirmationOrDOJ = "From DOJ";
    this.model.leaveCreditFrequency = "Select";
    this.model.leaveCreditStartmonth = "Select";
    this.model.allowPastDays = true;
    this.model.allowPastDaysCout = 1;
    this.model.allowFutureDays = true;
    this.model.allowFutureDaysCout = 60;
    this.model.interveningHoliday = true;
    this.model.prefixWo = true;
    this.model.suffixWo = true;
    this.model.prefixHo = true;
    this.model.suffixHo = true;
    this.model.interveningWeeklyoff = true;
    this.model.miniumDaysToApply = 0.5;
    this.model.encashableAndCarryForward = false;
    this.model.creditOn =true;
    this.model.initialiseOn =true;
    this.model.onJoining=true;
   



    this.model.inNoticePeriod = false;
    this.model.advanceLeaveTaking = false;
    this.model.medicalCertificate = false;
    this.model.leaveTrasfer = false;
    this.model.proRate = false;

    // this.model.noWorkDays = 1;
    // this.model.noOfLeavesToCredit = 0;
    this.model.leaveCreditFrequency = "Yearly";
    this.model.leaveCreditStartmonth = "January";
    this.model.leaveOrCoff = "leave";
    this.employmentType = [
      { id: 1, type: "Consultant OR Retainer" },
      { id: 2, type: "On Contact Job" },
      { id: 3, type: "On Contact Labour" },
      { id: 4, type: "On Contact Services" },
      { id: 5, type: "On Roll" }
    ]

    this.SubEmploymentType = [
      { id: 1, type: "Confirmed" },
      { id: 2, type: "On Probation" },
      { id: 3, type: "Temporary" },
      { id: 4, type: "Trainee" }
    ];
    this.model.applicableDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.encashmentFrequency = 'yearly';
    this.getConfigurationList();
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    this.leaveModel = new LeaveModel();
    this.leaveModel.typeOfLeave = "Paid";
    this.leaveModel.leaveOrCoff = "leave"
    this.model.leaveId = "";
    this.model.initialiseCredit = "initialise_at_start";
    this.LeaveDetailsList();
     this.Leavelistflag = false
    this.getLeavePattern(this.forPatternId);
    this.hideButtonFlag = false
    this.leaveOrflag = true;
    this.departmentList();
  }
  encahsmentTypeChanged(e) {
    this.model.encashmentFrequency = 'yearly';
  }
  accumulationLimitType(val) {
    if (val == "yearly") {
      this.model.accumulationLimitDays = 60;
    } else {
      this.model.accumulationLimitDays = 30;
    }

  }

  leaveAccStartMonChanged(e) {
    }
  leaveAccEndMonChanged(e) {
}
  getLeavePattern(forPatternId) {
    this.forPatternId = forPatternId;
    let url = this.baseurl + '/leavePattern/';
    this.httpService.get(url + this.forPatternId).subscribe((data: any) => {
        this.leavePatternList = [];
        this.leavePatternList = data['result']
      },
      (err: HttpErrorResponse) => {
      }
    );

  }
  getConfigurationList() {
    let obj = {
      compId: this.companyId
    }
    this.leaveService.getLeaveConfigurationList(obj).subscribe(data => {
      this.AttendenceConfiAllData = data.json().result;
      for (let i = 0; i < this.AttendenceConfiAllData.length; i++) {
        this.AttendenceConfiAllData[i].leaveCodeName = this.AttendenceConfiAllData[i].leaveCode + " - " + this.AttendenceConfiAllData[i].patternName;
      }
      this.chRef.detectChanges();
      setTimeout(function () {
        $(function () {
          var table = $('#CompTable').DataTable();
        });
      }, 1000);

      this.chRef.detectChanges();

    }, err => {
    })
  }
  edit(value) {
    if (this.Leavelistflag == true) {
      this.leaveflag = false;
      let obj = {
        compId: this.companyId,
        leaveId: this.forPatternId,
        leavePatternId: this.leavePtternId
      }
      this.hideButtonFlag = true;
      let item;
      this.model.leaveId = '';
      this.leaveService.getLeaveConfigurationById(obj).subscribe(data => {
        this.isEdit = true;
        item = data.result;
        this.model.leaveId = item.leaveId;
        this.model.leavePatternId = item.leavePatternId;
        this.getLeavePattern(this.model.leaveId);
        if (item.leaveOrCoff == 'coff'){
          this.leaveOrflag = false
        }
        else {
          this.leaveOrflag = true
        }
        let subEmployementTypeNameObj = new Array();
        if (item.leaveDetailSubEmpTypeResDtoList) {
          for (let i = 0; i < item.leaveDetailSubEmpTypeResDtoList.length; i++) {
            for (let j = 0; j < this.SubEmploymentType.length; j++) {
              if (this.SubEmploymentType[j].type == item.leaveDetailSubEmpTypeResDtoList[i].subEmployementTypeName) {
                subEmployementTypeNameObj.push(this.SubEmploymentType[j]);
              }
            }
          }
        }

        this.model.subEmployementTypeName = subEmployementTypeNameObj;
        let employementTypeNameObj = new Array();
        for (let i = 0; i < item.leaveDetailEmpTypeResDtoList.length; i++) {
          for (let j = 0; j < this.employmentType.length; j++) {
            if (this.employmentType[j].type == item.leaveDetailEmpTypeResDtoList[i].employementTypeName) {
              employementTypeNameObj.push(this.employmentType[j]);
            }
          }
        }
        this.model.employementTypeName = employementTypeNameObj;
        let genderObj = new Array();
        if (item.male == true) {
          let obj1 = { id: 1, gender: "Male" };
          //  this.model.genderMasterList.push(obj1);
          genderObj.push(obj1);
        }
        if (item.female == true) {
          let obj2 = { id: 2, gender: "Female" };
          // this.model.genderMasterList.push(obj2); 
          genderObj.push(obj2);
        }
        if (item.other == true) {
          let obj3 = { id: 3, gender: "Other" };
          // this.model.genderMasterList.push(obj3); 
          genderObj.push(obj3);
        }
        this.model.genderMasterList = genderObj;
        if (item.employeementType) {
          let tempEmployeementType = item.employeementType.split(",");
          let tempEmployeementTypeObj = new Array();
          for (let i = 0; i < tempEmployeementType.length; i++) {
            for (let j = 0; j < this.employmentType.length; j++) {
              if (tempEmployeementType[i] == this.employmentType[j].type) {
                tempEmployeementTypeObj.push(this.employmentType[j]);
              }
            }
          }
          this.model.employmentTypes = tempEmployeementTypeObj;
        }

        // if (item.subEmployeementType) {
        //   let tempSubEmploymentTypes = item.subEmployeementType.split(",");
        //   let tempSubEmploymentTypesObj = new Array();
        //   for (let i = 0; i < tempSubEmploymentTypes.length; i++) {
        //     for (let j = 0; j < this.SubEmploymentType.length; j++) {
        //       if (tempSubEmploymentTypes[i] == this.SubEmploymentType[j].type) {
        //         tempSubEmploymentTypesObj.push(this.SubEmploymentType[j]);
        //       }
        //     }
        //   }
        //   this.model.subEmploymentTypes = tempSubEmploymentTypesObj;
        // }

        this.model.yearaccumulation = item.yearaccumulation;
        this.model.midaccumulation = item.midaccumulation;
        this.model.yearaccumulationLimit = item.yearaccumulationLimit;
        this.model.midaccumulationLimit = item.midaccumulationLimit;
        this.model.midYearEncahsmentFrequency = item.midYearEncahsmentFrequency;
        this.model.numberOfAppPerFrequency = item.numberOfAppPerFrequency;
        this.model.creditOnJoing = item.creditOnJoing;
        this.model.creditOnConfirmation = item.creditOnConfirmation;
        this.model.yearEncahsmentFrequency = item.yearEncahsmentFrequency;
        let obj = new Array();
        obj = item.leaveClubbingResDtoList
        for (let i = 0; i < this.AttendenceConfiAllData.length; i++) {
          for (let j = 0; j < obj.length; j++) {
            if (this.AttendenceConfiAllData[i].leaveId == obj[j].leaveId) {

              obj[j].leaveCodeName  = this.AttendenceConfiAllData[i].leaveCode +" - "+this.AttendenceConfiAllData[i].patternName;

            }
          }
        }
        this.model.leaveClubbingResDtoList = item.leaveClubbingResDtoList;
        this.model.leaveClubbingListNew = obj;
        this.usedStatus = item.usedStatus;
        this.model.leaveId = item.leaveId;
        this.model.status = item.status;
        this.model.accumulationLimitDays = item.accumulationLimitDays;
        this.model.payRollHeads = item.payRollHeadResDtos;
        this.model.allowDaysPerMonth = item.allowDaysPerMonth;
        this.model.allowFutureDays = item.allowFutureDays;
        this.model.allowFutureDaysCout = item.allowFutureDaysCout;
        this.model.allowHalfdat = item.allowHalfdat;
        this.model.allowPastDays = item.allowPastDays
        this.model.allowPastDaysCout = item.allowPastDaysCout
        this.model.annualLeaveDays = item.annualLeaveDays
        this.model.applicableDate = item.applicableDate
        this.model.autoLeaveEncashment = item.autoLeaveEncashment
        this.model.calculateOnConfirmationOrDOJ = item.calculateOnConfirmationOrDOJ
        this.model.cancelLeavePeriod = item.cancelLeavePeriod
        this.model.carryForwardAllowed = item.carryForwardAllowed
        this.model.carryForwardFrequency = item.carryForwardFrequency
        this.model.casualLeaveClubbing = item.casualLeaveClubbing
        this.model.creditBasedOnWorkDays = item.creditBasedOnWorkDays
        this.model.creditOnEvent = item.creditOnEvent
        this.model.currYearNegBal = item.currYearNegBal
        this.model. initialiseOn= item.initialiseOn
        this.model.currYearNegBalCout = item.currYearNegBalCout;

        this.model.employeeTypeMasterList = item.employeeTypeResDtos;
        this.subEmpType(item.employeeTypeResDtos);
        this.model.subEmployeeTypeMasters = item.subEmployeeTypeLeaveResDtos;
        this.model.encahsmentFrequency = item.encahsmentFrequency;
        this.model.encahsmentType = item.encahsmentType;
        this.model.seperation = item.seperation;
        this.model.encashmentFrequency = item.encashmentFrequency;
        this.model.encashable = item.encashable
        this.model.female = item.female
        this.model.male = item.male
        this.model.other = item.other;
        // this.model.gradeMasterList = item.gradeMasterResDtos
        this.model.reinitializeBalanceCount = item.reinitializeBalanceCount;
        this.model.interveningHoliday = item.interveningHoliday
        this.model.interveningWeeklyoff = item.interveningWeeklyoff
        this.model.leaveDetailId = item.leaveDetailId;
        this.model.intimateLeavesPriorInDays = item.intimateLeavesPriorInDays
        this.model.leaveAccEndMon = item.leaveAccEndMon
        this.model.leaveAccStartMon = item.leaveAccStartMon
        this.model.leaveCarryForwardId = item.leaveCarryForwardId
        this.model.leaveClubbingId = item.leaveClubbingId
        this.model.leaveCode = item.leaveCode
        this.model.leaveCommonId = item.leaveCommonId
        this.model.leaveCreditId = item.leaveCreditId
        this.model.leaveEncashmentId = item.leaveEncashmentId
        this.model.leaveId = item.leaveId
        this.model.leaveMaster = item.leaveMaster
        this.model.leaveName = item.leaveName
        this.model.leavePatternId = item.leavePatternId
        this.model.leaveReinitializeId = item.leaveReinitializeId
        this.model.leaveTransferId = item.leaveTransferId
        this.model.leaveTransferOn = item.leaveTransferOn
        this.model.leaveTrasfer = item.leaveTrasfer
        this.model.leaveValidationId = item.leaveValidationId
        this.model.locationMasterList = item.locationResDtos
        this.model.maternityLeaveClubbing = item.maternityLeaveClubbing
        this.model.maxApplAllow = item.maxApplAllow
        this.model.maxApplUnit = item.maxApplUnit
        this.model.maxLimDayForEnchash = item.maxLimDayForEnchash
        this.model.maximumDaysToApply = item.maximumDaysToApply
        this.model.minLimForEncash = item.minLimForEncash
        this.model.miniumDaysToApply = item.miniumDaysToApply
        this.model.nextYearNegBal = item.nextYearNegBal
        this.model.nextYearNegBalCout = item.nextYearNegBalCout
        this.model.nextYearNegBal = item.nextYearNegBal
        this.model.noOfLeavesTakComp = item.noOfLeavesTakComp
        this.model.noOfLeavesToCredit = item.noOfLeavesToCredit
        this.model.noWorkDays = item.noWorkDays
        this.model.numberOfAppPerFrequency = item.numberOfAppPerFrequency
        this.model.other = item.other
        this.model.overallcarryfwdlimit = item.overallcarryfwdlimit
        this.model.paidLeaveClubbing = item.paidLeaveClubbing
        this.model.prefixHo = item.prefixHo
        this.model.prefixWo = item.prefixWo
        this.model.previousYearNegBal = item.previousYearNegBal
        this.model.previousYearNegBalCout = item.previousYearNegBalCout
        this.model.privilegeLeaveClubbing = item.privilegeLeaveClubbing
        this.model.proRate = item.proRate
        this.model.quarantineLeaveClubbing = item.quarantineLeaveClubbing
        this.model.reinitializeBalance = item.reinitializeBalance
        this.model.roundedOff = item.roundedOff
        this.model.sabbaticalLeaveClubbing = item.sabbaticalLeaveClubbing
        this.model.sickLeaveClubbing = item.sickLeaveClubbing
        this.model.suffixHo = item.suffixHo
        this.model.suffixWo = item.suffixWo
        this.model.typeOfLeave = item.typeOfLeave
        this.model.watingPeriod = item.watingPeriod
        this.model.leaveCreditStartmonth = item.leaveCreditStartmonth;
        this.model.leaveCreditFrequency = item.leaveCreditFrequency;
        this.model.accumulable = item.accumulable;

        this.model.medicalCertificate = item.medicalCertificate;
        this.model.afterHowManydays = item.afterHowManydays;
        this.model.encashableAndCarryForward = item.encashableAndCarryForward;
        this.model.lapseLeavesConvert = item.lapseLeavesConvert;
        this.model.leaveTakenCOmpularyOnOneYear = item.leaveTakenCOmpularyOnOneYear;

        this.model.encFreqType = item.encFreqType;
        this.model.encSeperType = item.encSeperType;
        this.model.minBalanceToMaintain = item.minBalanceToMaintain;
        this.model.leaveTransfDays = item.leaveTransfDays;

        this.model.initialiseCredit = item.initialiseCredit;
        this.model.onConfirmation = item.onConfirmation;
        this.model.onJoining = item.onJoining;
        this.model.creditOn = item.creditOn;
        this.model.initialiseOn= item.initialiseOn;
        this.model.leaveEncashAndCarryforwardId = item.leaveEncashAndCarryforwardId;
        this.model.leaveTransferId = item.leaveTransferId;
        this.model.patternName = item.patternName
        this.model.leaveTypeToMerge = item.leaveTypeToMerge
        this.Leavelistflag = false


      }, err => {
        this.resetForm();
      
      })

    }

    else {
      for(let i = 0 ; i < this.leaveDetailsList.length; i++){
        if(this.leaveDetailsList[i].leaveId == value.leaveId){
          if(this.leaveDetailsList[i].leaveOrCoff == 'soff' || this.leaveDetailsList[i].leaveOrCoff == 'coff' || this.leaveDetailsList[i].typeOfLeave =="Unpaid" ){
            this.leaveOrflag = true;
          }
          else{
            this.leaveOrflag = false;
          }
        }
      }
      this.leaveflag = false

      this.Leavelistflag = false
      let obj = {
        compId: this.companyId,
        leaveId: value.leaveId,
        leavePatternId: value.leavePatternId
      }
      let item;
      this.hideButtonFlag = false
      this.model.leaveId = "";
      this.leaveService.getLeaveConfigurationById(obj).subscribe(data => {
        this.isEdit = true;
        item = data.result;

        this.model.leaveId = item.leaveId;
        this.model.leavePatternId = item.leavePatternId;
        let subEmployementTypeNameObj = new Array();
        if (item.leaveDetailSubEmpTypeResDtoList) {
          for (let i = 0; i < item.leaveDetailSubEmpTypeResDtoList.length; i++) {
            for (let j = 0; j < this.SubEmploymentType.length; j++) {
              if (this.SubEmploymentType[j].type == item.leaveDetailSubEmpTypeResDtoList[i].subEmployementTypeName) {
                subEmployementTypeNameObj.push(this.SubEmploymentType[j]);
              }
            }
          }
        }

        this.model.subEmployementTypeName = subEmployementTypeNameObj;

        let employementTypeNameObj = new Array();
        for (let i = 0; i < item.leaveDetailEmpTypeResDtoList.length; i++) {
          for (let j = 0; j < this.employmentType.length; j++) {
            if (this.employmentType[j].type == item.leaveDetailEmpTypeResDtoList[i].employementTypeName) {
              employementTypeNameObj.push(this.employmentType[j]);
            }
          }
        }
        this.model.employementTypeName = employementTypeNameObj;
        let genderObj = new Array();
        if (item.male == true) {
          let obj1 = { id: 1, gender: "Male" };
          genderObj.push(obj1);
        }
        if (item.female == true) {
          let obj2 = { id: 2, gender: "Female" };
          genderObj.push(obj2);
        }
        if (item.other == true) {
          let obj3 = { id: 3, gender: "Other" };
          genderObj.push(obj3);
        }
        this.model.genderMasterList = genderObj;
        if (item.employeementType) {
          let tempEmployeementType = item.employeementType.split(",");
          let tempEmployeementTypeObj = new Array();
          for (let i = 0; i < tempEmployeementType.length; i++) {
            for (let j = 0; j < this.employmentType.length; j++) {
              if (tempEmployeementType[i] == this.employmentType[j].type) {
                tempEmployeementTypeObj.push(this.employmentType[j]);
              }
            }
          }
          this.model.employmentTypes = tempEmployeementTypeObj;
        }

        if (item.subEmployeementType) {
          let tempSubEmploymentTypes = item.subEmployeementType.split(",");
          let tempSubEmploymentTypesObj = new Array();
          for (let i = 0; i < tempSubEmploymentTypes.length; i++) {
            for (let j = 0; j < this.SubEmploymentType.length; j++) {
              if (tempSubEmploymentTypes[i] == this.SubEmploymentType[j].type) {
                tempSubEmploymentTypesObj.push(this.SubEmploymentType[j]);
              }
            }
          }
          this.model.subEmploymentTypes = tempSubEmploymentTypesObj;
        }

      this.model.yearaccumulation = item.yearaccumulation;
      this.model.midaccumulation = item.midaccumulation;
      this.model.yearaccumulationLimit = item.yearaccumulationLimit;
      this.model.midaccumulationLimit = item.midaccumulationLimit;
      this.model.midYearEncahsmentFrequency = item.midYearEncahsmentFrequency;
      this.model.numberOfAppPerFrequency = item.numberOfAppPerFrequency;
      this.model.creditOnJoing = item.creditOnJoing;
      this.model.onJoining=item.onJoining;
      this.model.creditOnConfirmation = item.creditOnConfirmation;
      this.model.yearEncahsmentFrequency = item.yearEncahsmentFrequency;
      let obj = new Array();
      let obj1 = new Array();
      if(item.leaveClubbingResDtoList){
        for (let i = 0; i < item.leaveClubbingResDtoList.length; i++) {

          // item.leaveClubbingResDtoList[i].leaveId = item.leaveClubbingResDtoList[i].leaveIdClubbing;
          let object = {
            // leaveIdClubbing: item.leaveClubbingResDtoList[i].leaveIdClubbing,
            // leaveId: item.leaveClubbingResDtoList[i].leaveIdClubbing,
            leavePatternId: item.leaveClubbingResDtoList[i].leaveIdClubbing,
            leaveIdClubbing: item.leaveClubbingResDtoList[i].leaveIdClubbing,
            // leaveCode: item.leaveClubbingResDtoList[i].leaveCode
          };
          obj1.push(object);
    
        }
        obj = obj1;
        for (let i = 0; i < this.AttendenceConfiAllData.length; i++) {
          for (let j = 0; j < obj.length; j++) {
            if (this.AttendenceConfiAllData[i].leavePatternId == obj[j].leavePatternId) {
    
              obj[j].leaveCodeName  = this.AttendenceConfiAllData[i].leaveCode +" - "+this.AttendenceConfiAllData[i].patternName;
              // this.AttendenceConfiAllData[i].leaveCodeName = this.AttendenceConfiAllData[i].leaveCode +" - "+this.AttendenceConfiAllData[i].patternName;
            }
          }
        }
      }
      
      this.model.leaveClubbingResDtoList = item.leaveClubbingResDtoList;
      this.model.leaveClubbingListNew = obj;
      
      this.usedStatus = item.usedStatus;
      this.model.leaveId = item.leaveId;
      this.model.status = item.status;
      this.model.accumulationLimitDays = item.accumulationLimitDays;
      this.model.payRollHeads = item.payRollHeadResDtos;
      this.model.allowDaysPerMonth = item.allowDaysPerMonth;
      this.model.allowFutureDays = item.allowFutureDays;
      this.model.allowFutureDaysCout = item.allowFutureDaysCout;
      this.model.allowHalfdat = item.allowHalfdat;
      this.model.allowPastDays = item.allowPastDays
      this.model.allowPastDaysCout = item.allowPastDaysCout
      this.model.annualLeaveDays = item.annualLeaveDays
      this.model.applicableDate = item.applicableDate
      this.model.autoLeaveEncashment = item.autoLeaveEncashment
      this.model.calculateOnConfirmationOrDOJ = item.calculateOnConfirmationOrDOJ
      this.model.cancelLeavePeriod = item.cancelLeavePeriod
      this.model.carryForwardAllowed = item.carryForwardAllowed
      this.model.carryForwardFrequency = item.carryForwardFrequency
      this.model.casualLeaveClubbing = item.casualLeaveClubbing
      this.model.creditBasedOnWorkDays = item.creditBasedOnWorkDays
      this.model.creditOnEvent = item.creditOnEvent
      this.model. initialiseOn= item.initialiseOn
      this.model.onJoining=item.onJoining
      this.model.currYearNegBal = item.currYearNegBal
      this.model.currYearNegBalCout = item.currYearNegBalCout;

      this.model.employeeTypeMasterList = item.employeeTypeResDtos;
      this.subEmpType(item.employeeTypeResDtos);
      this.model.subEmployeeTypeMasters = item.subEmployeeTypeLeaveResDtos;
      this.model.encahsmentFrequency = item.encahsmentFrequency;
      this.model.encahsmentType = item.encahsmentType;
      this.model.seperation = item.seperation;
      this.model.encashmentFrequency = item.encashmentFrequency;
      this.model.encashable = item.encashable
      this.model.female = item.female
      this.model.male = item.male
      this.model.other = item.other;
      // this.model.gradeMasterList = item.gradeMasterResDtos
      this.model.inNoticePeriod = item.inNoticePeriod
      this.model.advanceLeaveTaking=item.advanceLeaveTaking
      this.model.reinitializeBalanceCount = item.reinitializeBalanceCount;

      this.model.interveningHoliday = item.interveningHoliday
      this.model.interveningWeeklyoff = item.interveningWeeklyoff
      this.model.leaveDetailId = item.leaveDetailId;
      this.model.intimateLeavesPriorInDays = item.intimateLeavesPriorInDays
      this.model.leaveAccEndMon = item.leaveAccEndMon
      this.model.leaveAccStartMon = item.leaveAccStartMon
      this.model.leaveCarryForwardId = item.leaveCarryForwardId
      this.model.leaveClubbingId = item.leaveClubbingId
      this.model.leaveCode = item.leaveCode
      this.model.leaveCommonId = item.leaveCommonId
      this.model.leaveCreditId = item.leaveCreditId
      this.model.leaveEncashmentId = item.leaveEncashmentId
      this.model.leaveId = item.leaveId
      this.model.leaveMaster = item.leaveMaster
      this.model.leaveName = item.leaveName
      this.model.leavePatternId = item.leavePatternId
      this.model.leaveReinitializeId = item.leaveReinitializeId
      this.model.leaveTransferId = item.leaveTransferId
      this.model.leaveTransferOn = item.leaveTransferOn
      this.model.leaveTrasfer = item.leaveTrasfer
      this.model.leaveValidationId = item.leaveValidationId
      this.model.locationMasterList = item.locationResDtos
      this.model.maternityLeaveClubbing = item.maternityLeaveClubbing
      this.model.maxApplAllow = item.maxApplAllow
      this.model.maxApplUnit = item.maxApplUnit
      this.model.maxLimDayForEnchash = item.maxLimDayForEnchash
      this.model.maximumDaysToApply = item.maximumDaysToApply
      this.model.minLimForEncash = item.minLimForEncash
      this.model.miniumDaysToApply = item.miniumDaysToApply
      this.model.nextYearNegBal = item.nextYearNegBal
      this.model.nextYearNegBalCout = item.nextYearNegBalCout
      this.model.nextYearNegBal = item.nextYearNegBal
      this.model.noOfLeavesTakComp = item.noOfLeavesTakComp
      this.model.noOfLeavesToCredit = item.noOfLeavesToCredit
      this.model.noWorkDays = item.noWorkDays
      this.model.numberOfAppPerFrequency = item.numberOfAppPerFrequency
      this.model.other = item.other
      this.model.overallcarryfwdlimit = item.overallcarryfwdlimit
      this.model.paidLeaveClubbing = item.paidLeaveClubbing
      this.model.prefixHo = item.prefixHo
      this.model.prefixWo = item.prefixWo
      this.model.previousYearNegBal = item.previousYearNegBal
      this.model.previousYearNegBalCout = item.previousYearNegBalCout
      this.model.privilegeLeaveClubbing = item.privilegeLeaveClubbing
      this.model.proRate = item.proRate
      this.model.quarantineLeaveClubbing = item.quarantineLeaveClubbing
      this.model.reinitializeBalance = item.reinitializeBalance
      this.model.roundedOff = item.roundedOff
      this.model.sabbaticalLeaveClubbing = item.sabbaticalLeaveClubbing
      this.model.sickLeaveClubbing = item.sickLeaveClubbing
      this.model.suffixHo = item.suffixHo
      this.model.suffixWo = item.suffixWo
      this.model.typeOfLeave = item.typeOfLeave
      this.model.watingPeriod = item.watingPeriod
      this.model.leaveCreditStartmonth = item.leaveCreditStartmonth;
      this.model.leaveCreditFrequency = item.leaveCreditFrequency;
      this.model.accumulable = item.accumulable;
      

      this.model.encashableAndCarryForward = item.encashableAndCarryForward;
      this.model.lapseLeavesConvert = item.lapseLeavesConvert;
      this.model.leaveTakenCOmpularyOnOneYear = item.leaveTakenCOmpularyOnOneYear;

      this.model.encFreqType = item.encFreqType;
      this.model.encSeperType = item.encSeperType;
      this.model.minBalanceToMaintain = item.minBalanceToMaintain;
      this.model.leaveTransfDays = item.leaveTransfDays;

      this.model.initialiseCredit = item.initialiseCredit;
      this.model.onConfirmation = item.onConfirmation;
      this.model.onJoining = item.onJoining;
      this.model.creditOn = item.creditOn;
      this.model.initialiseOn= item.initialiseOn;


      this.model.leaveEncashAndCarryforwardId = item.leaveEncashAndCarryforwardId;
      this.model.leaveTransferId = item.leaveTransferId;
      this.model.medicalCertificate = item.medicalCertificate;
      this.model.afterHowManydays = item.afterHowManydays;
      this.model.patternName = item.patternName;
      let obj2 = new Array();
      obj2 = item.leaveTypeToMerge;
      for (let i = 0; i < item.leaveTypeToMerge.length; i++) {
        // item.leaveTypeToMerge[i].leavePatternId = item.leaveTypeToMerge[i].leaveTypeToMergeId;
        for (let j = 0; j < this.AttendenceConfiAllData.length; j++) {
          if (this.AttendenceConfiAllData[j].leavePatternId == item.leaveTypeToMerge[i].leavePatternId) {
            
            obj2[j].leaveCodeName  = this.AttendenceConfiAllData[j].leaveCode +" - "+this.AttendenceConfiAllData[j].patternName;
            // this.AttendenceConfiAllData[i].leaveCodeName = this.AttendenceConfiAllData[i].leaveCode +" - "+this.AttendenceConfiAllData[i].patternName;
          }
        }
      }
      this.model.leaveTypeToMerge = obj2;
      this.Leavelistflag = false
    }, err => {
        this.resetForm();
        this.isEdit = false;
      })

    }
  }

  leaveTakenCompulsory(event){
    this.model.noOfLeavesTakComp = "";
   
  }

  medicalCertificateChange(event){
    
    this.validation=event
    this.model.afterHowManydays = "";
  }
  selectAllOf() {
    if (!this.selectAll) {
      this.model.casualLeaveClubbing = true;
      this.model.sickLeaveClubbing = true;
      this.model.paidLeaveClubbing = true;
      this.model.privilegeLeaveClubbing = true;
      this.model.maternityLeaveClubbing = true;
      this.model.quarantineLeaveClubbing = true;
      this.model.sabbaticalLeaveClubbing = true;
      this.model.casualLeaveClubbing = true;
      this.model.casualLeaveClubbing = true;
      this.model.casualLeaveClubbing = true;
      for (let i = 0; i < this.AttendenceConfiAllData.length; i++) {
        this.AttendenceConfiAllData[i].check = true;
      }
    }
    else {
      for (let i = 0; i < this.AttendenceConfiAllData.length; i++) {
        this.AttendenceConfiAllData[i].check = false;
      }
      this.model.casualLeaveClubbing = false;
      this.model.sickLeaveClubbing = false;
      this.model.paidLeaveClubbing = false;
      this.model.privilegeLeaveClubbing = false;
      this.model.maternityLeaveClubbing = false;
      this.model.quarantineLeaveClubbing = false;
      this.model.sabbaticalLeaveClubbing = false;
      this.model.casualLeaveClubbing = false;
      this.model.casualLeaveClubbing = false;
      this.model.casualLeaveClubbing = false;
    }
  }

  ngOnInit() {
    this.dropdownSettingsGrade = {
      singleSelection: false,
      idField: 'gradeId',
      textField: 'gradeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsLocation = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsEmployeeCategory = {
      singleSelection: false,
      idField: 'empTypeId',
      textField: 'empTypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsSubEmployeeType = {
      singleSelection: false,
      idField: 'subEmpTypeId',
      textField: 'subEmpTypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    this.dropdownSettingsLeaveEncash = {
      singleSelection: false,
      idField: 'payRollHeadsId',
      textField: 'payRollHeadName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsGender = {
      singleSelection: false,
      idField: 'id',
      textField: 'gender',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsLeaveClubbing = {
      singleSelection: false,
      idField: 'leavePatternId',
      textField: 'leaveCodeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsEmploymentType = {
      singleSelection: false,
      idField: 'id',
      textField: 'type',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsSubEmploymentType = {
      singleSelection: false,
      idField: 'id',
      textField: 'type',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownSettingsDepat = {
      singleSelection: false,
      idField: 'deptId',
      textField: 'deptName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    this.leaveService.getGrade(this.companyId).subscribe(data => {
      this.gradeList = data.json().result;
    });
    if(this.loggedUser.roleHead=='Admin'){
    this.leaveService.getLocaction(this.companyId).subscribe(data => {
      this.locationData = data.json().result;
    });
  }
  else if(this.loggedUser.roleHead=='HR'){
    
    this.leaveService.getLocactionwise(this.loggedUser.locationId).subscribe(data => {
      this.locationData = data.json().result;
    });
  }
    this.leaveService.getEmploymentType(this.companyId).subscribe(data => {
       this.employmentTypeData = data.json().result;
    })
    this.leaveService.getPayRollHead(this.companyId).subscribe(data => {
      this.LeaveEncashData = data.json().result;
    })
  }
  Lerror() {
    this.patternName = this.model.patternName;
      let url = this.baseurl + '/leavePatternError/';
      this.httpService.get(url + this.patternName).subscribe((data: any) => {
      this.errordata = data["result"];
      this.msg = data.message
      this.toastr.error(this.msg);
      this.lerrflag = true
    },
    (err: HttpErrorResponse) => {
      this.lerrflag = false
    });
  }


  //===========================Gender Start========================

  onItemSelectG(item: any) {
    if (this.selectedGenderList.length == 0) {
      this.selectedGenderList.push(item)
    } else {
      this.selectedGenderList.push(item)
    }
  }
  onSelectAllG(items: any) {
    this.selectedGenderList = items;
  }

  OnItemDeSelectG(item: any) {
    for (var i = 0; i < this.selectedGenderList.length; i++) {
      if (this.selectedGenderList[i].gradeId == item.gradeId) {
        this.selectedGenderList.splice(i, 1)
      }
    }
  }

  onDeSelectAllG(item: any) {
    this.selectedGenderList = [];
  }
  onItemSelect(item: any) {
    if (this.selectedGrade.length == 0) {
      this.selectedGrade.push(item)
    } else {
      this.selectedGrade.push(item)
    }
  }
  onSelectAll(items: any) {
    this.selectedGrade = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedGrade.length; i++) {
      if (this.selectedGrade[i].gradeId == item.gradeId) {
        this.selectedGrade.splice(i, 1);
      }
    }
  }
  onDeSelectAll(item: any) {
    this.selectedGrade = [];
  }
  onItemSelectL(item: any) {
    if (this.selectedLocation.length == 0) {
      this.selectedLocation.push(item)
    } else {
      this.selectedLocation.push(item)
    }
  }
  onSelectAllL(items: any) {
    this.selectedLocation = items;
  }

  OnItemDeSelectL(item: any) {
    for (var i = 0; i < this.selectedLocation.length; i++) {
      if (this.selectedGrade[i].locationId == item.locationId) {
        this.selectedGrade.splice(i, 1)
      }
    }
  }
  onDeSelectAllL(item: any) {
    this.selectedLocation = [];
  }
  onItemSelectE(item: any) {
    if (this.selectedEmploymentType.length == 0) {
      this.selectedEmploymentType.push(item)
    } else {
      this.selectedEmploymentType.push(item)
    }
    this.subEmpType(this.selectedEmploymentType);
  }
  onSelectAllE(items: any) {
    this.selectedEmploymentType = items;
    this.subEmpType(this.selectedEmploymentType);
  }

  OnItemDeSelectE(item: any) {
    for (var i = 0; i < this.selectedEmploymentType.length; i++) {
      if (this.selectedEmploymentType[i].empTypeId == item.empTypeId) {
        this.selectedEmploymentType.splice(i, 1);
      }
    }
    this.subEmpType(this.selectedEmploymentType);
  }

  onDeSelectAllE(item: any) {
    this.selectedEmploymentType = [];
    this.subEmpType(this.selectedEmploymentType);
  }
  onItemSelectLE(item: any) {
    if (this.selectedLeaveEncash.length == 0) {
      this.selectedLeaveEncash.push(item);
    } else {
      this.selectedLeaveEncash.push(item);
    }
  }
  onSelectAllLE(items: any) {
    this.selectedLeaveEncash = items;
  }

  OnItemDeSelectLE(item: any) {
    for (var i = 0; i < this.selectedLeaveEncash.length; i++) {
      if (this.selectedLeaveEncash[i].payRollHeadsId == item.payRollHeadsId) {
        this.selectedLeaveEncash.splice(i, 1)
      }
    }
  }

  onDeSelectAllLE(item: any) {
    this.selectedLeaveEncash = [];
  }
  onItemSelectC(item: any) {
    if (this.selectedLeaveConfig.length == 0) {
      this.selectedLeaveConfig.push(item)
    } else {
      this.selectedLeaveConfig.push(item)
    }
  }
  onSelectAllC(items: any) {
    this.selectedLeaveConfig = items;
  }

  OnItemDeSelectC(item: any) {
    for (var i = 0; i < this.selectedLeaveConfig.length; i++) {
      if (this.selectedLeaveConfig[i].leaveId == item.leaveId) {
        this.selectedLeaveConfig.splice(i, 1);
      }
    }
  }

  onDeSelectAllC(item: any) {
    this.selectedLeaveConfig = [];
  }
  goToSecond() {
    document.getElementById('tab5').click();
  }
  goToThird() {
    document.getElementById('tab3').click();
  }
  goToFourth() {
    document.getElementById('tab4').click();
  }
  goToFifth() {
    document.getElementById('tab5').click();
  }
  goToSixth() {
    document.getElementById('tab7').click();
  }
  goToSeventh() {
    document.getElementById('tab7').click();
  }
  goToEighth() {
    
    if(this.model.allowPastDaysCout==null||this.model.allowFutureDaysCout==null||this.model.miniumDaysToApply==null ||this.model.maximumDaysToApply==null){
   this.toastr.error("please fill up all required field")
    }
    else if(this.model.afterHowManydays==''&& this.validation==true||this.require==true&& this.model.allowPastDaysCout==null ||this.model.allowFutureDaysCout==null&& this.requires==true){
      this.toastr.error("please fill up all required field")
    }
    else{
      document.getElementById('tab8').click();
    }
    
  }
  goToNinth() {
    document.getElementById('tab9').click();
  }
  // =================== Validations Clicks Starts ===================

  selectClickCategory() {
    if (this.model.employeeTypeMasterList.length == 0) {
      this.categoryFlag = true;
    }
    else {
      this.categoryFlag = false;
    }
  }

  selectClickSubCategory() {
    if (this.model.subEmployeeTypeMasters.length == 0) {
      this.subCategoryFlag = true;
    }
    else {
      this.subCategoryFlag = false;
    }
  }

  selectClickEmploymentType() {
    if (this.model.employementTypeName.length == 0) {
      this.employmentTypeFlag = true;
    }
    else {
      this.employmentTypeFlag = false;
    }
  }
  // selectClickGrade() {
  //   if (this.model.gradeMasterList.length == 0) {
  //     this.gradeFlag = true;
  //   }
  //   else {
  //     this.gradeFlag = false;
  //   }
  // }
  selectClickLocation() {
    if (this.model.locationMasterList.length == 0) {
      this.locationFlag = true;
    }
    else {
      this.locationFlag = false;
    }
  }
  selectClickSubEmploymentType() {
    if (this.model.subEmployementTypeName.length == 0) {
      this.subEmploymentTypeFlag = true;
    }
    else {
      this.subEmploymentTypeFlag = false;
    }
  }
  selectClickGender() {
    if (this.model.genderMasterList.length == 0) {
      this.genderFlag = true;
    }
    else {
      this.genderFlag = false;
    }
  }

  selectClickClubbing() {

  }

  leavePatternvalue(event) {
    this.leavePtternId = parseInt(event)
    this.Leavelistflag = true
    this.edit(this.leavePtternId)
  }
  // =================== Validations Clicks Starts ===================
  resetForm() {
    setTimeout(() => {
      this.Leavelistflag = false;
      this.leaveOrflag = true;
      this.isEdit = false;
      this.hideButtonFlag = false;
      this.model.typeOfLeave = "Paid";
      this.model.status = true;
      this.model.applicableDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.usedStatus = false;
      this.model.payRollHeads = [];
      this.model.allowFutureDays = false;
      this.model.allowHalfdat = false;
      this.model.allowPastDays = false;
      this.model.autoLeaveEncashment = false;
      this.model.calculateOnConfirmationOrDOJ = "From DOJ";
      this.model.carryForwardAllowed = false;
      this.model.carryForwardFrequency = "Select";
      this.model.casualLeaveClubbing = false;
      this.model.creditBasedOnWorkDays = false;
      this.model.creditOnEvent = "ON Joining";
      this.model.initialiseOn="Joining"
      this.model.currYearNegBal = false;
      this.model.employeeTypeMasterList = [];
      this.model.encahsmentFrequency = "";
      this.model.encashable = false;
      this.model.female = false;
      this.model.male = false;
      this.model.other = false;
      // this.model.gradeMasterList = [];
      this.model.interveningHoliday = true;
      this.model.interveningWeeklyoff = true;
      this.model.leavePatternId = '';
      this.model.patternName = ''
      this.leave = ''
      this.leavePatternList = []
      this.model.leaveTransferOn = "On Confirmation";
      this.model.leaveTrasfer = false;
      this.model.locationMasterList = [];
      this.model.maternityLeaveClubbing = false;
      this.model.maxApplUnit = "Select";
      this.model.nextYearNegBal = false;
      this.model.paidLeaveClubbing = false;
      this.model.prefixHo = true;
      this.model.prefixWo = true;
      this.model.previousYearNegBal = false;
      // this.model.previousYearNegBalCout = "";
      this.model.privilegeLeaveClubbing = false;
      this.model.proRate = false;
      this.model.quarantineLeaveClubbing = false;
      this.model.reinitializeBalance = false;
      this.model.roundedOff = "Select";
      this.model.sabbaticalLeaveClubbing = false;
      this.model.sickLeaveClubbing = false;
      this.model.suffixHo = true;
      this.model.suffixWo = true;
      // this.model.watingPeriod = "";
      this.model.leaveCreditStartmonth = "Select";
      this.model.leaveCreditFrequency = "Select";
      this.model.leaveTransferNumberOfDays = "";
      this.selectAll = false;
      this.model.subEmploymentTypes = [];
      this.model.employmentTypes = [];
      this.model.leaveClubbingListNew = [];
      this.model.leaveClubbingList = [];
      this.model.leaveTypeToMerge = []
      this.model.leaveClubbingResDtoList = [];
      this.model.genderMasterList = [];
      this.model.subEmpTypeData = []
      this.model.locationData = []
      this.model.employmentType = []
      this.model.employmentTypeData = []

      this.model.gradeList = []
      this.model.locationData = []
      this.model.GenderList = []
      this.model.creditOnJoing = true;
      this.model.creditOnConfirmation = false;

      this.model.encahsmentType = false;
      this.model.encashmentFrequency = 'yearly';
      this.model.seperation = false;
      this.model.yearEncahsmentFrequency = false;
      this.model.midYearEncahsmentFrequency = false;
      this.model.midaccumulation = false;
      this.model.yearaccumulation = false;
      this.model.accumulable = false;
      this.locationFlag = false;
      this.gradeFlag = false;
      this.categoryFlag = false;
      this.subEmploymentTypeFlag = false;
      this.employmentTypeFlag = false;
      this.model.subEmployementTypeName = [];
      this.model.employementTypeName = [];
      this.model.subEmployeeTypeMasters = [];
      this.model.leaveTypeToMerge = [];
      this.model.leavePatternId = ''
      this.leave = ''
    }, 100);

  }
  buttonClick() {
    for (let i = 0; i < this.model.employmentTypes.length; i++) {
      if (this.model.employeementType) {
        this.model.employeementType = this.model.employeementType + "," + this.model.employmentTypes[i].type;
      }
      else {
        this.model.employeementType = this.model.employmentTypes[i].type;
      }
    }
    for (let i = 0; i < this.model.subEmploymentTypes.length; i++) {

      if (this.model.subEmployeementType) {
        this.model.subEmployeementType = this.model.employeementType + "," + this.model.subEmploymentTypes[i].type;
      }
      else {
        this.model.subEmployeementType = this.model.subEmploymentTypes[i].type;
      }
    }
  }
  categoryFlag: boolean = false;
  subCategoryFlag: boolean = false;
  employmentTypeFlag: boolean = false;
  subEmploymentTypeFlag: boolean = false;
  gradeFlag: boolean = false;
  locationFlag: boolean = false;
  genderFlag: boolean = false;
  leavetype:string;
  onSubmit(f) {
    let male: boolean = false;
    let female: boolean = false;
    let other: boolean = false;
    for (let i = 0; i < this.model.genderMasterList.length; i++) {
      if (this.model.genderMasterList[i].id == 1) {
        male = true;
      }
      if (this.model.genderMasterList[i].id == 2) {
        female = true;
      }
      if (this.model.genderMasterList[i].id == 3) {
        other = true;
      }
    }
    this.model.male = male;
    this.model.female = female;
    this.model.other = other;
    let employeementTypeString: string = "";
    for (let i = 0; i < this.model.employmentTypes.length; i++) {
      if (this.model.employeementType) {
        employeementTypeString = employeementTypeString + "," + this.model.employmentTypes[i].type;
      }
      else {
        employeementTypeString = this.model.employmentTypes[i].type;
      }

    }
    this.model.employeementType = employeementTypeString;
    let subEmployeementTypeString: string = "";
    for (let i = 0; i < this.model.subEmploymentTypes.length; i++) {

      if (this.model.subEmployeementType) {
        subEmployeementTypeString = subEmployeementTypeString + "," + this.model.subEmploymentTypes[i].type;
      }
      else {
        subEmployeementTypeString = this.model.subEmploymentTypes[i].type;
      }
    } 
    this.model.subEmployeementType = subEmployeementTypeString;
    if (this.model.leaveClubbingList.length > 0) {
      for (let i = 0; i < this.model.leaveClubbingList.length; i++) {
        this.model.leaveClubbingList[i].leaveIdClubbing = this.model.leaveClubbingList[i].leavePatternId;
      }
    }
    // =========Leave Clubbing Loop Starts =========================
    this.model.leaveClubbingList = new Array();
    for (let i = 0; i < this.model.leaveClubbingListNew.length; i++) {
      let item = {
        leaveIdClubbing: this.model.leaveClubbingListNew[i].leavePatternId,
        leaveId: this.model.leaveId
      }
      this.model.leaveClubbingList.push(item);
    }

    if (this.model.genderMasterList.length && this.model.employementTypeName.length  && this.model.employeeTypeMasterList.length &&
      this.model.locationMasterList.length) {
      if (this.isEdit) {
        let empType = new Array();
        for (let i = 0; i < this.model.employementTypeName.length; i++) {
          if (this.model.employementTypeName[i]) {
            empType.push(this.model.employementTypeName[i].type)
          }

        }
        this.model.employementTypeName = empType;
        let subEmpType = new Array();
        for (let i = 0; i < this.model.subEmployementTypeName.length; i++) {
          if (this.model.subEmployementTypeName[i]) {
            subEmpType.push(this.model.subEmployementTypeName[i].type)
          }
        }
        this.model.subEmployementTypeName = subEmpType;
        this.Spinner.show()
        console.log("modeldata", this.model);
        let urlupdate = this.baseurl + '/leave/list';
        this.httpService.put(urlupdate, this.model).subscribe(data => {
            this.Spinner.hide()
            this.toastr.success("Leave Configuration Updated Successfully");
            this.isEdit = false;
            this.leaveflag = false;
            f.reset();
            this.resetForm();
            this.getConfigurationList();
          },
            (err: HttpErrorResponse) => {
              this.Spinner.hide();
              this.toastr.error("Failed To Update");

            });

      }
      else {
        let empType = new Array();
        for (let i = 0; i < this.model.employementTypeName.length; i++) {
          if (this.model.employementTypeName) {
            empType.push(this.model.employementTypeName[i].type)
          }

        }
        this.model.employementTypeName = empType;
        let subEmpType = new Array();
        for (let i = 0; i < this.model.subEmployementTypeName.length; i++) {
          if (this.model.subEmployementTypeName[i]) {
            subEmpType.push(this.model.subEmployementTypeName[i].type)
          }
        }
        this.model.subEmployementTypeName = subEmpType;
        this.Spinner.show()
      
        this.leaveService.postLeaveConfiguration(this.model).subscribe(data => {
          this.Spinner.hide()
          this.toastr.success("Leave Configuration Added Successfully");
          this.isEdit = false;
          this.leaveflag =false;
          f.reset();
          this.resetForm();
          this.getConfigurationList();


        }, err => {
          this.Spinner.hide()
          this.toastr.error("Failed");

        })
      }
    }
    else {
      this.Spinner.hide();
      this.toastr.error("Please Chek All Mandatory Fields");
      if (!this.model.employmentTypes.length) {
        this.employmentTypeFlag = true;
        this.leaveOrflag=true;
      }
      if (!this.model.subEmploymentTypes.length) {
        this.subEmploymentTypeFlag = true;
      }
      if (!this.model.employeeTypeMasterList.length) {
        this.categoryFlag = true;
      }

      // if (!this.model.gradeMasterList.length) {
      //   this.gradeFlag = true;
      // }
      if (!this.model.locationMasterList.length) {
        this.locationFlag = true;
      }
      if (!this.model.genderMasterList.length) {
        this.genderFlag = true;
      }
    }
  }
  isSubmitted: boolean = false;
  changed() {
    this.isSubmitted = true;
  }

  daysChanged() {

  }

  clearFeilds() {
    this.model.autoLeaveEncashment = false;
    this.model.noOfLeavesTakComp = "";
    this.model.minLimForEncash = "";
    this.model.maxLimDayForEnchash = "";
    this.model.encahsmentType = false;
    this.model.seperation = false;
    this.model.overallcarryfwdlimit = "";
    this.model.leaveTakenCOmpularyOnOneYear = false;
    this.model.lapseLeavesConvert = false;
    this.model.encFreqType = false;
    this.model.encSeperType = false;
    this.model.encashmentFrequency = "yearly";
    this.model.numberOfAppPerFrequency = "";
    this.model.minBalanceToMaintain = "";
    this.model.payRollHeads = [];
  }


  clearCarry() {
    this.model.carryForwardFrequency = "";
    this.model.overallcarryfwdlimit = "";
  }
  clearLeaveTransfer() {
    this.model.leaveTransferOn = "On Confirmation";
    this.model.days = "";
    this.model.leaveTypeToMerge = [];
    this.model.leaveTransfDays = "";
  }
  clearReinitialize() {
    this.model.reinitializeBalanceCount = null
  }
  previousYearNegBal() {
    this.model.previousYearNegBalCout = null
  }
  currYearNegBal() {
    this.model.currYearNegBalCout = null
  }
  nextYearNegBal() {
    this.model.nextYearNegBalCout = null;
  }


  subEmpTypeData: any = [];
  subEmpType(selectedValues) {
    this.subEmpTypeData = [];
    let obj: any = new Object()
    obj.empTypeToSubEmployee = selectedValues
    let body = JSON.stringify(obj);
    let urlsubempType = this.baseurl + '/empCatagory/subCatageory/';
    this.httpService.post(urlsubempType, obj).subscribe(data => {
        this.subEmpTypeData = data;
        let tempData = new Array();
        let object: any = new Object();
        for (let i = 0; i < this.subEmpTypeData.length; i++) {
          for (let j = 0; j < this.subEmpTypeData[i].length; j++) {
            object.subEmpTypeName = this.subEmpTypeData[i][j].empTypeName + "-" + this.subEmpTypeData[i][j].subEmpTypeName;
            object.subEmpTypeId = this.subEmpTypeData[i][j].subEmpTypeId;
            object.empTypeId = this.subEmpTypeData[i][j].empTypeId;
            tempData.push(object);
            object = new Object();
          }
        }
        this.subEmpTypeData = tempData;
      }, (err: HttpErrorResponse) => {
        this.subEmpTypeData = [];
      }
    );

  }
  initialiseCreditChanged(event) {
    if (event == "initialise_at_start") {
      this.model.creditBasedOnWorkDays = false;
    }
    this.model.creditOn = "joining";
    this.model.initialiseOn = "joining";
    
  }
  applicableDateChanged(event) {
    this.model.applicableDate = event;
  }
  // =================Leave Master Starts ======================

  addLeaveModal(){
    var dirtyFormID = 'formleave';
    var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
    resetForm.reset();
    setTimeout(() => {
      this.leaveModel = new LeaveModel();
      this.leaveModel.typeOfLeave = "Paid";
      this.leaveModel.leaveOrCoff = "leave";
      this.isLeaveDetailsEdit = false;
      this.LeaveDetailsList();
    }, 100);
  }

  isLeaveDetailsEdit: boolean = false;
  leaveDetailsList = [];
  templeaveDEtailsList = [];
  addLeaveDetails(form) {
    if (!this.isLeaveDetailsEdit) {
      this.Spinner.show()
      this.leaveService.createLeaveInLeaveMaster(this.leaveModel).subscribe(data => {
        this.Spinner.hide()
        this.toastr.success("Leave Added Successfully");
        this.isLeaveDetailsEdit = false;
        form.reset();

        setTimeout(() => {
          this.leaveModel = new LeaveModel();
          this.leaveModel.typeOfLeave = "Paid";
          this.leaveModel.leaveOrCoff = "leave";
          this.LeaveDetailsList();
        }, 100);
      }, err => {
        this.Spinner.hide()
        this.toastr.error("Failed To Add Leave");
      })
    }
    else {
      this.Spinner.show();
      console.log("***************************", this.leaveModel);
      this.leaveService.updateLeaveInLeaveMaster(this.leaveModel).subscribe(data => {
        this.Spinner.hide();
        this.toastr.success("Leave Updated Successfully");
        this.isLeaveDetailsEdit = false;
        form.reset();
        setTimeout(() => {
          this.leaveModel = new LeaveModel();
          this.leaveModel.typeOfLeave = "Paid";
          this.leaveModel.leaveOrCoff = "leave";
          this.LeaveDetailsList();
        }, 100);
      }, err => {
        this.Spinner.hide();
        this.toastr.error("Failed To Update Leave");
      })
    }
  }
  resetLEaveDetails(f1) {
    this.errflag = false;
  }

  // this.leaveflag=false;
  LeaveDetailsList() {
    this.leaveService.getLeaveMasterList().subscribe(data => {
      this.leaveDetailsList = data.result;
      for (let index = 0; index < this.leaveDetailsList.length; index++) {
        this.leaveDetailsList[index].leaveCode = this.leaveDetailsList[index].leaveName + "-" + this.leaveDetailsList[index].leaveCode;

      }
    }, err => {
    })
  }
  value(data) {
    this.changeleaveId = data;
    for (let index = 0; index < this.leaveDetailsList.length; index++) {
      if(this.leaveDetailsList[index].leaveId==this.changeleaveId ){
        this.templeaveDEtailsList[index] = this.leaveDetailsList[index];
       if(this.templeaveDEtailsList[index].typeOfLeave=="Unpaid" ||  this.templeaveDEtailsList[index].leaveOrCoff=="coff" || this.templeaveDEtailsList[index].leaveOrCoff=="soff" ){
        this.leaveflag = false;
        this.leaveOrflag = true;
      }
      else{
        this.leaveflag = false;
          this.leaveOrflag = false;
      }
    }
    }
    let item = { leaveId: data }
  // this.edit(item);
  }
  valueLeave(data) {
    this.forPatternId = data
    this.getLeavePattern(this.forPatternId)
  }

  error() {
    if (this.leaveModel.leaveName != this.leaveName) {
      this.getcmp = this.leaveModel.leaveName
      let url = this.baseurl + '/leaveErrorMsg/';
      this.httpService.get(url + this.getcmp).subscribe((data: any) => {
          this.errordata = data["result"];
          //  this.msg= data.message
          this.toastr.error("Already Exits");
          this.errflag = true;
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        }
      );
    }
  }

  errorCode() {
    if (this.leaveModel.leaveCode != this.leaveCode) {

      this.getCode = this.leaveModel.leaveCode
      let url = this.baseurl + '/leaveCodeError/';
      this.httpService.get(url + this.getCode).subscribe((data: any) => {
          this.errordata = data["result"];
          this.toastr.error("Already Exits");
          this.errflag = true
        },
        (err: HttpErrorResponse) => {
        }
      );
    }
  }

  editLeaveDetails(item) {
    this.leaveService.getLeaveMasterById(item.leaveId).subscribe(data => {
      this.isLeaveDetailsEdit = true;
      this.leaveModel = data.result[0];
    if( this.leaveModel.typeOfLeave=="Unpaid" ){
            
            this.leaveflag = false;
            this.leaveOrflag = true;
       }
    }, err => {

    })
  }
  deleteLeaveId: any;
  deleteLeaveD(item) {
    this.deleteLeaveId = item.leaveId;
  }
  deleteLeaveDetails() {
    this.leaveService.deleteLeaveDetails(this.deleteLeaveId).subscribe(data => {
      this.LeaveDetailsList();
    }, err => {
      this.toastr.error("Leave is already used by employees so you can not delete !!!");
    })
  }
  clearf() {
    this.require=true
    this.model.allowPastDaysCout = null
    this.model.allowFutureDaysCout = null
    this.model.intimateLeavesPriorInDays = null
  }
  clearf1() {
    this.requires=true;
    this.model.allowFutureDaysCout = null
    this.model.intimateLeavesPriorInDays = null
  }
  departmentList() {
    let busiurl = this.baseurl + '/Department/';
    this.httpService.get(busiurl + this.companyId).subscribe(data => {
        this.departmentlistdata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }
  dltmodelEvent(){
    this.dltmodelFlag=true;
    this.deleteLeave(this.dltObje);
  }
  
  dltObject(obj){
    this.dltmodelFlag=false
    this.dltObje = obj.leavePatternId;
    this.deleteLeave(this.dltObje);
  }
  deleteLeave(object){
    if (this.dltmodelFlag == true) {
      this.leaveModel.leavePatternId=this.dltObje
        let url = this.baseurl + '/deleteLeavePattern/';
        this.httpService.delete(url + this.dltObje ).subscribe(data => {
            this.toastr.success('Leave deleted successfully');
            this.getConfigurationList();
          },(err: HttpErrorResponse) => {
            this.toastr.error('References is present so you can not delete..!');
         });
      }
    }

    
    
    
}
