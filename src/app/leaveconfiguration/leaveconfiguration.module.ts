import { routing } from './leaveconfiguration.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import { CommonModule, DatePipe } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { LeaveconfigurationComponent } from './leaveconfiguration.component'
import { leaveConfigurationService } from './shared/services/leaveConfigurationService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TopupleavebalanceComponent } from './topupleavebalance/topupleavebalance.component';


@NgModule({
    declarations: [
        LeaveconfigurationComponent,
        TopupleavebalanceComponent
    ],
    imports: [
        routing,
        ToastrModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CommonModule,
        HttpClientModule,
        DataTablesModule,
        NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [leaveConfigurationService, DatePipe]
})
export class LeaveConfigurationModule {
    constructor() {

    }
}
