export class leaveConfigurationModel {
    leaveId: number;

    leaveName: string;

    leaveCode: string;
    
    typeOfLeave: string;

    annualLeaveDays: string;

    status: boolean;

    cancelLeavePeriod: number;

    inNoticePeriod: boolean;

    watingPeriod: number;

    leaveValidationId: number;

    interveningHoliday: boolean;

    prefixWo: boolean;

    suffixWo: boolean;

    prefixHo: boolean;

    suffixHo: boolean;

    interveningWeeklyoff: boolean;

    leaveCommonId: number;

    nextYearNegBal: boolean;

    allowPastDays: boolean;

    allowPastDaysCout: number;

    allowFutureDays: boolean;

    allowFutureDaysCout: number;

    miniumDaysToApply: number;

    intimateLeavesPriorInDays: number;

    maxApplAllow: number;

    maxApplUnit: string;

    allowHalfdat: boolean;

    maximumDaysToApply: number;

    allowDaysPerMonth: number;

    leaveCreditId: number;

    onJoining: boolean;

    onConfirmation: boolean;

    creditBasedOnWorkDays: boolean;

    noWorkDays: number;

    noOfLeavesToCredit: number;

    calculateOnDoj: boolean;

    calculateOnConfirmatioon: boolean;

    leaveEncashmentId: boolean;

    encashable: boolean;

    autoLeaveEncashment: boolean;

    noOfLeavesTakComp: number;

    minLimForEncash: number;

    maxLimDayForEnchash: number;

    encahsmentType: string;

    encahsmentFrequency: number;

    numberOfAppPerFrequency: number;

    accumulationLimitDays: number;

    leaveAccStartMon: string;

    leaveAccEndMon: string;

    leaveReinitializeId: number;

    reinitializeBalance: boolean;

    previousYearNegBal: boolean;

    previousYearNegBalCout: number;

    currYearNegBal: boolean;

    currYearNegBalCout: number;

    next_year_neg_bal: boolean;

    nextYearNegBalCout: number;

    leaveTransferId: number;

    leaveTrasfer: boolean;

    onConformation: boolean;

    afterNoDaysJoin: boolean;

    days: number;

    // private LeaveMaster leaveMaster;
    LeaveDescMaster = [];

    LeaveLocationMaster = [];

    LeaveOrganisationMaster = [];

    LeaveGradeMaster = [];

    LeaveGenderMaster = [];

    EmpTypeWiseLeaveMaster = [];

    // suffixHo : boolean;
    casualLeaveClubbing : boolean;

    sickLeaveClubbing : boolean;

    paidLeaveClubbing : boolean;

    privilegeLeaveClubbing : boolean;

    maternityLeaveClubbing : boolean;

    quarantineLeaveClubbing : boolean;

    sabbaticalLeaveClubbing : boolean;

    leaveEncashCalculateOn = [];

    carryForwardAllowed : boolean;

    carryForwardFrequency : string;

    overallcarryfwdlimit : string;

    leaveTransferOn : string;

    leaveTransferNumberOfDays : number;

    creditOnEvent : string;

    proRate : boolean;

    calculateOnConfirmationOrDOJ : string;

    leaveCreditFrequency : string;

    leaveCreditStartmonth : string;

    male : boolean;

    female : boolean;

    other : boolean;

    reinitializeBalanceCount : number;

    rounedOff : string;

    applicableDate : Date;

    
}

export class LeaveModel {
    leaveId : number;
    leaveName : string
    leaveCode : String
    typeOfLeave : string
    leaveOrCoff : string
  leavePatternId: any;
}