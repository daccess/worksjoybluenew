import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

// import { satutory } from '../models/satutorymodel';
// import { MainURL } from '../../../shared/configurl';
import { from } from 'rxjs';
import { MainURL } from 'src/app/shared/configurl';
//import { satutory } from 'src/app/employee/shared/models/satutorymodel';
import { leaveConfigurationModel } from '../models/leaveConfigurationModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { applyLeave } from '../model/applyLeave';

@Injectable()
export class leaveConfigurationService
 {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http,public httpService: HttpClient) { }

  postLeaveConfiguration(model){
    let url = this.baseurl + '/leave';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions)
    // .map(x => x.json());
  }

  getRecordsByid(url,id):Observable<any>
  {
      const httpHeaders = new HttpHeaders();
      httpHeaders.set('Content-Type', 'application/json');
      let temp_url=url+'/'+id;
      return this.httpService.get<any>(this.baseurl+'/'+temp_url, { headers: httpHeaders});
  }

  updateLeaveConfiguration(model){
    let url = this.baseurl + '/leave/list';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.httpService.put(url,body)
    // .map(x => x.json());
  }

  getLeaveConfigurationById(model){
    let url = this.baseurl + '/leave/byId?compId='+model.compId+'&leaveId='+model.leaveId+'&leavePatternId='+model.leavePatternId;
    return this.http.get(url).map(x => x.json());
  }

  getLeaveConfigurationList(model){
    let url = this.baseurl + '/leave/list';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions);
  }

getGrade(id: number) {
    return this.http.get(this.baseurl +'/GradeMaster/Active/'+ id)
   
  }

  getLocaction(id: number) {
    return this.http.get(this.baseurl +'/Location/Active/'+ id)
  }
  getLocactionwise(id: number) {
    return this.http.get(this.baseurl +'/LocationWise/'+ id)
  }

  getEmploymentType(id: number) {
    return this.http.get(this.baseurl +'/EmployeeType/Active/'+ id)
  }
  
  getPayRollHead(id: number) {
    return this.http.get(this.baseurl +'/PayRollHead/Active/'+ id)
  }

  // =================== Leave Master Starts ==============

  

  createLeaveInLeaveMaster(model){
    let url = this.baseurl + '/leaveMaster';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }


  createRecord(url:string,data:any):Observable<any>
    {
        return this.httpService.post<any>(this.baseurl+url,data);
    }

  
  getLeaveDataForTopUpLeave(leaveid){
    return this.http.get(this.baseurl +'/getLeaveDataForTopUpLeave/'+ leaveid)
  }

  getLeaveMasterList(){
    let url = this.baseurl + '/AllLeaves';
    return this.http.get(url).map(x => x.json());
  }
  getLeavebalancebyID(id:number){
    return this.http.get(this.baseurl +'/PayRollHead/Active/'+ id)
  }

  getLeaveMasterById(leaveId){
    let url = this.baseurl + '/leaveMasterData?leaveId='+leaveId;
    return this.http.get(url).map(x => x.json());
  }
  updateLeaveInLeaveMaster(model){
    let url = this.baseurl + '/leaveMaster';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,model);
    // .map(x => x.json());
  }

  deleteLeaveDetails(leaveId){
    let url = this.baseurl + '/deleteleave/'+leaveId;
    return this.http.delete(url).map(x => x.json());
  }
}