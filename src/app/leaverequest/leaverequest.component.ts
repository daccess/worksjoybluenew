import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { leaveRequestService } from './services/leaverequestservice';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { applyLeave } from './model/applyLeave';
import { ToastrService } from 'ngx-toastr';
import { CoffReq } from '../otherrequests/shared/models/coffReqModel'
import { coffService } from '../otherrequests/shared/services/CoffService';
import { PersonalRequetsService } from '../otherrequests/shared/services/personalRequestService';
import { CalendarService } from '../shared/services/calendar.service';
import { FilePreviewModel } from 'ngx-awesome-uploader';
import { dataService } from '../shared/services/dataService';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
import { SoffReq } from '../otherrequests/shared/models/soffReqModel';
import { NgxSpinnerService } from 'ngx-spinner';


declare var $;
@Component({
  selector: 'app-leaverequest',
  templateUrl: './leaverequest.component.html',
  styleUrls: ['./leaverequest.component.css']
})
export class LeaverequestComponent implements OnInit {
  isShow: boolean = true;
  baseurl = MainURL.HostUrl;
  adapter = new dataService(this.httpService);
  date: any = new Date();
  daysInThisMonth: any = [];
  daysInLastMonth: any = [];
  daysInNextMonth: any = [];
  monthNames: any;
  coffModel: CoffReq;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  absentDay: any = "17"
  currentMonth1: any;
  from: any;
  Selectund: any
  to: any;
  leaveDays = [];
  searchText: string = "";
  model: any = { frmdate: "", todate: "", leavereson: "", handedover: [], empnotified: [], type: "", typeofleave: "Fullday" };
  compId: number;
  Employeedata: any = [];
  selectedEmployee: any = [];
  dropdownSettings: any;
  toBeNotified: any = [];
  handedWorkTo: any = [];
  empOfficialId: any;
  attendanceData: any = [];
  calAttendance: any = [];
  leaveConfigList:any = []=[];
  selectUndefinedOptionValue: any = "Select";
  loggedUser: any;
  empPerId: any;
  totalLeaveBalance = [];
  casualTotalLeaveBalance: any;
  casualCurrentLeaveBalance: any;
  privilegeTotalLeaveBalance: any;
  privilegeCurrentLeaveBalance: number;
  sickTotalLeaveBalance: any;
  sickCurrentLeaveBalance: any;
  otherLeaveBalance: any;
  bal: any;
  RequestObj: { empPerId: any; fromDate: any; status: "Pending"; coffOrLeave: any };
  tempModel: any = {};
  leaveApplicationList = [];
  modeflag: boolean
  deleteItem: any;
  isLeaveEdit: boolean = false;
  today: string;
  HolidayData = [];
  AllHRRequestToWorkList: any = [];
  empFlag: boolean;
  searchQuery: any;
  designation: any;
  empFlag1: boolean;
  searchQuery1: any;
  minimumDays: number;
  themeColor: string = "nav-pills-blue";
  maxDateForApply: any;
  slectecdoption: any;
  todaysDate: any;
  FullDaydateData: any;
  getfullDayDate: any;
  HalfDaydateData: any;
  getHlafDayDate: any;
  CoffList = [];
  isCoffEdit: boolean = false;
  leaveOrCoff: any;
  coffFlag: Boolean = false
  dltrequestCoffId: any;
  dltmodelFlagcoff: boolean;
  LeaveMaster: number;
  leaveMaster: any = {}
 fileSelected: any;
  leaveModel: any;
  medicalCertificate: boolean;
  medicalCertificateDays: number = 0;
  afterHowManydays: string;
  modal: any;
  leavePatternList: any;
  coffOrLeave: string;
  firstLevelReportingManager: any;
  secondLevelReportingManager: any;
  thirdLevelReportingManager: any;
  weekn: number;
  imageURL;
  leavePaidOrUnpaid:string;
  reaplyStatus: boolean = false;


  /*START For Staggered Off */
  soffFlag: Boolean = false;
  soffModel: SoffReq;
  coffdata: any=[];
  getSofffullDayDate: any;
  isSoffEdit: boolean = false;
  dltrequestSoffId: any;
  dltSoffId: any;
  dltmodelFlagsoff: boolean;
  removefile:boolean= false;
  defaultDate:any;
  errMsg:any;
  /*END For Staggered Off */

  firstSecHalfFlag = false;
  fileData:any;
  showImagePreview = true;
  HalfDayRadioVal = "Halfday";
  leaveErrorMessage1: any;
  leaveErrorMessage2: any;
  leaveErrorMessageFlag1: boolean;
  leaveErrorMessageFlag3: boolean;
  leaveErrorMessageFlag2: boolean;
  LeaveData: any;

  constructor(public toastr: ToastrService, 
    public personalservice: PersonalRequetsService, 
    public CoffServic: coffService, 
    private service: CompanyMasterService, 
    private leaverequestService: leaveRequestService, 
    public httpService: HttpClient,
    public chRef: ChangeDetectorRef,
    private calendarService : CalendarService,
    private dataservice:dataService,
    private InfoService:InfoService,
    private router:Router,
    public Spinner :NgxSpinnerService,
) {
    this.model.type = "Select";
    this.model.typeofleave = "Fullday";
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.coffModel = new CoffReq();
    this.leaveMaster = { leaveId: Number }
    this.modeflag = false;
    this.compId = this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId
    
    this.empPerId = this.loggedUser.empPerId
    this.getLeaveSuggestionList();
    this.getDataHRRequestToWorkList();
    this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.coffModel.dateAndTime = this.todaysDate;
    this.coffModel.coffType = "Full Day";
    let obj: any = new Object();
    obj.filePath = this.model.filePath;
    this.FullDaygetdate();
    this.HalfDaygetdate();
    this.configList();
    this.Holidays();
    this.getAttendanceOfMonth(new Date().getMonth() + 1, new Date().getFullYear());
    this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.maxDateForApply = new Date().getFullYear() + "-" + 12 + "-" + 31;
    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.coffOrLeave = 'leave';
    this.getAllEmployee();
    this.Leaves();
    this.tempModel.thisdate = new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1;
    this.today = new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1;
    this.RequestObj = {
      empPerId: this.empPerId,
      fromDate: new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1,
      status: "Pending",
      coffOrLeave: this.coffOrLeave
    }
    this.getLeaveApplicationListByEmpId();
    this.selectedLeaveCount = 1;
    this.model.selectedLeaveCount = 1;
    if (sessionStorage.getItem('from') == 'yearly') {
      this.model.frmdate = sessionStorage.getItem('requestedDate');
      this.model.todate = sessionStorage.getItem('requestedDate');
      sessionStorage.setItem('from', '');
      sessionStorage.setItem('requestedDate', new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate());
    }

    if (sessionStorage.getItem('from') == 'empd') {
      if (sessionStorage.getItem('requestedType') == 'L') {
        this.isLeaveEdit = true;
      
        let data = sessionStorage.getItem('data');
        this.editLeave(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
    }
    if (sessionStorage.getItem('from') == 'empd') {
      if (sessionStorage.getItem('requestedType') == 'CO') {
        this.coffFlag = true;
      
        let data = sessionStorage.getItem('data');
        this.editCoffRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
    }
    this.statusType = "Pending";
    let themeColor = sessionStorage.getItem('themeColor');
    if (themeColor) {
      this.themeColor = themeColor;
    }

    /*START FOR Staggered Off */
    this.soffModel = new SoffReq();
    this.getSoffFulldate();
    this.defaultDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.soffModel.forDate = this.defaultDate;
    /*END FOR Staggered Off */
  }
  getType(data){
  }
  configList(){
    this.leaverequestService.getLeaveConfigurationList(this.empPerId).subscribe((data: any) => {
      this.leaveConfigList = data.json().result;
    }, err => {
      this.leaveConfigList = err.json().result;
    })
  }
  weekAttendance = [];
  statusType: "Pending";
  selectThisDate(event) {
     this.RequestObj = {
      empPerId: this.empPerId,
      fromDate: event,
      status: this.statusType,
      coffOrLeave: this.coffOrLeave

    };
    this.getLeaveApplicationListByEmpId();
  }
  list() {
    this.RequestObj = {
      empPerId: this.empPerId,
      fromDate: new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1,
      status: this.statusType,
      coffOrLeave: this.coffOrLeave

    };
    this.getLeaveApplicationListByEmpId();
  }
  statusTypeChanged(e) {
    this.RequestObj.status = this.statusType
   this.getLeaveApplicationListByEmpId();
  }
  //====================holidays===========================
  Holidays() {
    let url = this.baseurl + '/Holiday?empOfficialId='+this.loggedUser.empOfficialId+'&year='+new Date().getFullYear();
    this.httpService.get(url + this.loggedUser.empOfficialId).subscribe(
      data => {
       this.HolidayData = data["result"];
      },
      (err: HttpErrorResponse) => {
        if (err) {
        }
      });
  }
 
  leaveSuggestionList = [];
  getLeaveSuggestionList() {
    this.leaverequestService.getLeaveSuggestionListById(this.empPerId).subscribe(data => {
     this.leaveSuggestionList = [];
      this.leaveSuggestionList = data.json().result;
    }, err => {
     this.leaveSuggestionList = [];
    })
  }

  acceptSuggestion(item, i) {
  this.leaveSuggestionList = []
    document.getElementById('missCol').click();
    let obj = {
      suggestionId: item.suggestionId,
      approverId: this.loggedUser.firstLevelReportingManager,
      approverRemark: item.approverRemark,
      compId: this.compId,
      empId: item.empId,
      fromDate: item.fromDate,
      leaveRequestId: item.leaveRequestId,
      toDate: item.toDate
    }
   this.leaverequestService.acceptLeaveSuggestion(obj).subscribe(data => {
     this.getLeaveSuggestionList();
      this.toastr.success('Successfully accepted leave suggestion');
    }, err => {
       this.toastr.error('Failed to accept leave suggestion');
      this.leaveSuggestionList = []
    })
  }

  rejectSuggestion(item, i) {
    this.leaveSuggestionList = []
     let obj = {
      finalStatus: item.finalStatus,
      leaveRequestId: item.leaveRequestId,
      leaveReson: item.reason,
      rejectOrApproveReson : item.approverRemark
    }
  document.getElementById('L' + i).click();
    this.leaverequestService.rejectLeaveSuggestion(obj).subscribe(data => {
      this.toastr.success('leave suggetstion rejected successfully');
      this.getLeaveSuggestionList();
    }, err => {
        this.leaveSuggestionList = []
      this.toastr.error('Failed to reject leave suggestion');
    })
  }
 
  reApplyLeave(item, i) {
      let editData;
    this.leaverequestService.getLeaveApplicationListById(item.leaveRequestId).subscribe(data => {
      editData = data.json().result;
        this.isLeaveEdit = true;
      this.model.leaveRequestId = editData.leaveRequestId;
      this.leavePatternId = editData.leavePatternId;
      this.model.type = editData.leaveId;
      this.model.typeofleave = editData.typeOfLeave;
      this.model.frmdate = editData.startDate;
      this.model.todate = editData.endDate;
      this.model.leavereson = editData.leaveReson;
      this.model.handedover = editData.handedOverWorkList;
      this.model.empnotified = editData.employeesToBeNotifiedList;
      this.model.filepath =  editData.medCertifPath;
      let file =editData.medCertifPath;
      if(file != null){
       this.selectedFileName =file.split("/").pop();
     }
      
      this.model.updateFromLeaveOrSugg = "suggestion";
      for (let i = 0; i < editData.handedOverWorkList.length; i++) {
        this.searchQuery = editData.handedOverWorkList[i].fullName + " " + editData.handedOverWorkList[i].lastName;
      }

      for (let i = 0; i < editData.employeesToBeNotifiedList.length; i++) {
        this.searchQuery1 = editData.employeesToBeNotifiedList[i].fullName + " " + editData.employeesToBeNotifiedList[i].lastName;
      }

      this.selectFromDate(editData.startDate);
      this.selectToDate(editData.endDate);
    
      document.getElementById('pills-home-tab').click();
     // this.getLeaveSuggestionList();
       this.leaveSuggestionList=[];
    }, err => {
    })
  }

  modelEvent() {
    this.modeflag = true;
    this.deleteLeaveRequest(this.deleteItem)
  }
  getLeave(item) {
   this.modeflag = true;
    this.deleteItem = item;
  }
  deleteLeaveRequest(item) {
   this.leaverequestService.deleteLeaveRequest(item.leaveRequestId).subscribe(data => {
      this.getLeaveApplicationListByEmpId();
      this.toastr.success("Leave deleted successfully");
      this.configList();
    }, err => {
       this.toastr.error("Failed to delete leave");
    })
  }
  getLeaveApplicationListByEmpId() {
    this.leaveApplicationList = []
    this.leaverequestService.getLeaveApplicationListByEmpId(this.RequestObj).subscribe(data => {
    this.leaveApplicationList = data.result;
      this.leaveOrCoff = data.result.leaveOrCoff
    }, err => {
     this.workFlowList = [];
    })
  }
 
  selectedFileName: string;
  editLeave(item) {
  
    this.modeflag = false;
    let editData: any = [];
    this.leaverequestService.getLeaveApplicationListById(item.leaveRequestId).subscribe(data => {
      editData = data.json().result;
      if(editData.typeOfLeave == ("Second Half")){
        this.firstSecHalfFlag = true;
        this.minimumDays = 0.5;
        this.model.selectedLeaveCount=0.5
        this.selectedLeaveCount=0.5
        this.HalfDayRadioVal = "Halfday";
      }
      else if(editData.typeOfLeave == "First Half"){
        this.firstSecHalfFlag = true;
        this.minimumDays = 0.5;
         this.model.selectedLeaveCount=0.5
         this.selectedLeaveCount=0.5
        this.HalfDayRadioVal = "Halfday";
      }
      else{
        this.firstSecHalfFlag = false;
        this.HalfDayRadioVal = "Halfday";
      }
      this.isLeaveEdit = true;
      this.chRef.detectChanges();
      this.model.empPerId = editData.empPerId;
      this.model.leaveRequestId = editData.leaveRequestId;
      this.model.type = editData.leaveId;
      this.model.typeofleave = editData.typeOfLeave;
      this.model.frmdate = editData.startDate;
      this.model.todate = editData.endDate;
      this.model.leavereson = editData.leaveReson;
      this.model.handedover = editData.handedOverWorkList;
      this.model.empnotified = editData.employeesToBeNotifiedList;
      this.model.updateFromLeaveOrSugg = "leave";
      this.leavePatternId = editData.leavePatternId;
      this.imageURL = editData.medCertifPath;
      if(editData.handedOverWorkList){
        for (let i = 0; i < editData.handedOverWorkList.length; i++) {
          // this.searchQuery = editData.handedOverWorkList[i].fullName;
          this.searchQuery = editData.handedOverWorkList[i].fullName + " " + editData.handedOverWorkList[i].lastName;
        }
      }
      
      if(editData.employeesToBeNotifiedList){
        for (let i = 0; i < editData.employeesToBeNotifiedList.length; i++) {
          // this.searchQuery1 = editData.employeesToBeNotifiedList[i].fullName;
          this.searchQuery1 = editData.employeesToBeNotifiedList[i].fullName + " " + editData.employeesToBeNotifiedList[i].lastName;
        }
      }
      this.model.filePath = editData.medCertifPath;
      
      let file =editData.medCertifPath;
      if(file != null){
      //  this.selectedFileName =file.split("/").pop();
       this.selectedFileName = file;
     }
      
      this.model.frmdate = editData.startDate;
      this.model.todate = editData.endDate;
      this.leaveErrorMessageFlag = true;
      // this.selectFromDate(editData.startDate);
      // this.selectToDate(editData.endDate);
      document.getElementById('pills-home-tab').click();
    }, err => {
    })
    
  }
  getAttendanceOfMonth(month, year) {
    let obj = {
      "empOfficialId": this.loggedUser.empOfficialId,
      "month": month,
      "year": year
    }

    this.leaverequestService.monthlyAttendance(obj).subscribe(data => {
      this.attendanceData = data.result;
      this.getDaysOfMonth();
    }, (err => {
      this.getDaysOfMonth();
    })
    )
  }
  show() {
    this.model.frmdate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
  }
  Leaves() {

    let url = this.baseurl + '/EmpLeavesBalanceDetails/';
    this.httpService.get(url + this.empPerId).subscribe(
      (data: any) => {
     this.totalLeaveBalance = data.result.list;
        for (let i = 0; i < this.totalLeaveBalance.length; i++) {

          if (this.totalLeaveBalance[i].leaveName == 'Casual Leave') {
            this.casualTotalLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
            this.casualCurrentLeaveBalance = this.totalLeaveBalance[i].currentLeaves;
          }
          if (this.totalLeaveBalance[i].leaveName == 'privilege leave') {
            this.privilegeTotalLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
            this.privilegeCurrentLeaveBalance = this.totalLeaveBalance[i].currentLeaves;
           }
          if (this.totalLeaveBalance[i].leaveName == 'Sick Leave') {
            this.sickTotalLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
            this.sickCurrentLeaveBalance = this.totalLeaveBalance[i].currentLeaves;
           
          }
          if (this.totalLeaveBalance[i].leaveName == 'Other') {
            this.otherLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
          }

        }

      },
      (err: HttpErrorResponse) => {
      });

  }
  reset(f,file) {
  this.showImagePreview = false;
    f.reset();
    // window.location.reload();
  //this.onRemoveSuccess(this.fileData);
      this.selectedLeaveCount = 1;
      this.model.selectedLeaveCount = 1;
      this.searchQuery = null;
      this.searchQuery1 = null;
      this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.type = "";
      this.model.typeofleave = "Fullday";
      this.model.leavereson = "";
     this.model.filePath = "";
   // this.medicalCertificateDays = 0;
  
  }

 
  ngOnInit() {
    this.showImagePreview = true;
     this.InfoService.currentMessage.subscribe(message => {
     this.coffdata=message;
      if( this.coffdata!='default message'){
          this.model.type =this.coffdata.leaveId;
          this.coffFlag = true;
          this.coffModel.coffCreditedId = this.coffdata.coffCreditedId;
          this.coffModel.dateAndTime = this.todaysDate;
          this.coffModel.coffType = this.coffdata.typeOfCredit;
          if(this.coffModel.coffType == "Full-Day"){
            this.coffModel.coffType ='Full Day';
          }
          if(this.coffModel.coffType == "Half-Day"){
            this.coffModel.coffType ='Half Day';
          }
          this.LeaveMaster = this.coffdata.leaveId;
         this.coffModel.extraWorkedDay = this.coffdata.extraWorkedDay;
          for (let i = 0; i < this.leaveConfigList.length; i++) {
            if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
              this.leavePatternId = this.leaveConfigList[i].leavePatternId;
              this.leaveMaster.leaveId  = this.leaveConfigList[i].leaveId;
            }
          }
        }else{
          this.coffdata=[];
        }
        
      });

   this.dropdownSettings = {
      singleSelection: false,
      idField: 'empPerId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    if (sessionStorage.getItem('from') == 'layout') {
     
      sessionStorage.setItem('from', '');
      document.getElementById('pills-suggest-tab').click();
    }
    
  }
 onItemSelect(item: any) {
 if (this.handedWorkTo.length == 0) {
      this.handedWorkTo.push(item)
    } else {
      this.handedWorkTo.push(item)
    }
  }
  onSelectAll(items: any) {
  
    this.handedWorkTo = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.handedWorkTo.length; i++) {
      if (this.handedWorkTo[i].locationId == item.locationId) {
        this.handedWorkTo.splice(i, 1)
      }
    }
  }

  onDeSelectAll(item: any) {
    this.handedWorkTo = [];
  }
  onItemSelect1(item: any) {
    
    if (this.toBeNotified.length == 0) {
      this.toBeNotified.push(item)
    } else {
      this.toBeNotified.push(item)
    }
  }
  onSelectAll1(items: any) {
   
    this.toBeNotified = items;
  }

  OnItemDeSelect1(item: any) {
    for (var i = 0; i < this.toBeNotified.length; i++) {
      if (this.toBeNotified[i].locationId == item.locationId) {
        this.toBeNotified.splice(i, 1)
      }
    }
  }
  onDeSelectAll1(item: any) {
    this.toBeNotified = [];
  }
  toDateFlag: boolean = false;
  typeOfLeave(typeofleave) {
    
    this.medicalCertificateDays = 0;
  if (this.model.typeofleave == 'Fullday') {
      this.medicalCertificateDays = 0;
      this.HalfDayRadioVal = "";
      this.firstSecHalfFlag = false;
      this.toDateFlag = false;
      var oneDay = 24 * 60 * 60 * 1000;
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
      
      this.selectedLeaveCount = diffDays + 1;
    }
    else {
      this.medicalCertificateDays = 0;
      this.toDateFlag = true;
      this.model.todate = this.model.frmdate;
      this.selectedLeaveCount = 0.5;
    }
    this.model.selectedLeaveCount = this.selectedLeaveCount;
   

    let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    

    let leavePatternId;
    let currentLeaves;
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves
      }
    }

    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.loggedUser.empId,
      leavePatternId: leavePatternId,
       leaveType: this.model.type,
      empOfficialId: this.loggedUser.empOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves,
      medicalCertificate:this.model.filePath,
      "leavePaidOrUnpaid": this.leavePaidOrUnpaid
    }

    if (this.model.type) {
      this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
        
        this.medicalCertificate = data.result.medicalCertificate;
        this.medicalCertificateDays = parseInt(data.result.afterHowManydays);
        this.leaveErrorMessage = data.result;
        this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
        this.leaveErrorMessageFlag = true;
      }, err => {
        

        if (err.json()) {
          
          this.leaveErrorMessage = err.json().result;
          this.medicalCertificate = this.leaveErrorMessage["medicalCertificate"];
          if(this.leaveErrorMessage["afterHowManydays"] != null){
            this.medicalCertificateDays = parseInt(this.leaveErrorMessage["afterHowManydays"]);
          }
          else{
            this.medicalCertificateDays = 0;
          }
          
          this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
          this.leaveErrorMessageFlag = false;
        }
        else {
          this.leaveErrorMessage = [];
          this.medicalCertificateDays = 0;
        }
      })
    }
  }
  selectedLeaveCount: number = 0;
  leaveErrorMessageFlag: boolean = false;
  selectFromDate(event) {

    
   this.leaveBalanceFlag = false;
    var oneDay = 24 * 60 * 60 * 1000;
    this.model.frmdate = event;
    if (this.model.typeofleave == 'Fullday') {

      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
      
      this.selectedLeaveCount = diffDays + 1;
    }
    else {

      this.model.todate = event;
      this.selectedLeaveCount = 0.5;
    }

    if (new Date(this.model.frmdate).getTime() > new Date(this.model.todate).getTime()) {
      this.model.todate = event;
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
     
      this.selectedLeaveCount = diffDays + 1;

    }
    this.model.selectedLeaveCount = this.selectedLeaveCount;
   let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    let leavePatternId;
    let currentLeaves;
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves;
        this.leavePaidOrUnpaid = this.leaveConfigList[i].leavePaidOrUnpaid;
      }
    }
    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.loggedUser.empId,
      leavePatternId: leavePatternId,
      empOfficialId: this.loggedUser.empOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves,
      "leavePaidOrUnpaid":this.leavePaidOrUnpaid
    }
  
    if (this.model.type) {
      this.leaveErrorMessage=[]
      this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
       
        if(data.result !=null){

       this.medicalCertificate = data.result.medicalCertificate;
        if(data.result.afterHowManydays != null){
          this.medicalCertificateDays = parseInt(data.result.afterHowManydays);
        }
        else{
          this.medicalCertificateDays = 0;
        }
        
        this.leaveErrorMessage = data.result;
        console.log("this.leaveErrorMessage2",this.leaveErrorMessage1);
        
        this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
        this.leaveErrorMessageFlag = true;
        console.log("flagstatus",  this.leaveErrorMessageFlag);
      } 
      },err => {
        

        if(err.json()) {
          this.leaveErrorMessage=[];
          this.leaveErrorMessage1=[]
          this.leaveErrorMessage2=[]
          // this.leaveErrorMessage=[]
          this.leaveErrorMessageFlag = false;
          // this.leaveErrorMessageFlag1 = true;
         this.leaveErrorMessage = err.json().result;
         console.log("this.leaveErrorMessage",this.leaveErrorMessage1);
          this.medicalCertificate = this.leaveErrorMessage["medicalCertificate"];
          if(this.leaveErrorMessage["afterHowManydays"] != null){
            this.medicalCertificateDays = parseInt(this.leaveErrorMessage["afterHowManydays"]);
          }
          else{
            this.medicalCertificateDays = 0;
          }
         
          this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
          this.leaveErrorMessageFlag = false;
          console.log("flagstatus",this.leaveErrorMessageFlag);
        }
        else {
          this.leaveErrorMessage = [];
        }
      })
    }


    for (let i = 0; i < this.totalLeaveBalance.length; i++) {
      for (let j = 0; j < this.leaveConfigList.length; j++) {
        if (this.totalLeaveBalance[i].leaveId == this.model.type) {
          let leaveBalence = this.totalLeaveBalance[i].currentLeaves;
          if (leaveBalence >= this.selectedLeaveCount) {
            this.leaveBalanceFlag = true;
          }

        }
      }
    }
  }
  leaveErrorMessage = [{ message: "" }];
  leaveBalanceFlag: boolean = true;
  selectToDate(e) {
    
    this.model.todate = e;
    var oneDay = 24 * 60 * 60 * 1000;
    this.leaveBalanceFlag = false;
    var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));

    

    if (this.model.typeofleave == 'Fullday') {
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
     
      this.selectedLeaveCount = diffDays + 1;
    }
    else {
      this.selectedLeaveCount = 0.5;
    }

    if (new Date(this.model.frmdate).getTime() > new Date(e).getTime()) {
      this.model.todate = e;
    }

    this.model.selectedLeaveCount = this.selectedLeaveCount;
    let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    
    let leavePatternId;
    let currentLeaves;
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves;
      }
    }
    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.loggedUser.empId,
      leavePatternId: leavePatternId,
      empOfficialId: this.loggedUser.empOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves,
      "leavePaidOrUnpaid":this.leavePaidOrUnpaid
    }
   if (this.model.type) {
    this.leaveErrorMessage=[]
      this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
       if(data.result !=null){
        this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
        this.medicalCertificate = data.result.medicalCertificate;
        this.leaveErrorMessage = data.result;
        if(data.result.afterHowManydays != null){
          this.medicalCertificateDays = parseInt(data.result.afterHowManydays);
        }
        else{
          this.medicalCertificateDays = 0;
        }
        this.leaveErrorMessageFlag = true;
        // this.leaveErrorMessageFlag3 = true;
      }
      },err => {
        if (err.json()) {
          // this.leaveErrorMessageFlag3 = true;
          this.leaveErrorMessage=[];
          this.leaveErrorMessage1=[]
          this.leaveErrorMessage2=[]
         this.leaveErrorMessage = err.json().result;
        //  this.toastr.error(this.leaveErrorMessage[0].message );
          this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
          this.medicalCertificate = this.leaveErrorMessage["medicalCertificate"];
          if(this.leaveErrorMessage["afterHowManydays"] != null){
            // this.medicalCertificateDays = parseInt(data.result.afterHowManydays);
          }
          else{
            this.medicalCertificateDays = 0;
          }
          this.medicalCertificateDays = parseInt(this.leaveErrorMessage["afterHowManydays"]);
          this.leaveErrorMessageFlag = false;
        }
      })
    }

    for (let i = 0; i < this.totalLeaveBalance.length; i++) {
      for (let j = 0; j < this.leaveConfigList.length; j++) {
        if (this.totalLeaveBalance[i].leaveId == this.model.type) {
          let leaveBalence = this.totalLeaveBalance[i].currentLeaves;
           if (leaveBalence >= this.selectedLeaveCount) {
            this.leaveBalanceFlag = true;
          }
        }
      }
    }
   
  }

 
  leavePatternId: any;
  selectLeaveType(list,value) {
    
    this.model.typeofleave = "Fullday";
    this.firstSecHalfFlag = false;
    this.HalfDayRadioVal = ""; 
   
    if(value){
      for(let i=0;i<list.length;i++){
        if(list[i].leaveId==value){
            this.leavePaidOrUnpaid=list[i].leavePaidOrUnpaid;
        }
       
      }
    }
    this.coffFlag = false;
    this.soffFlag = false;
    let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    this.weekn = weekNumber;

    let leavePatternId;
    let currentLeaves
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves
      }
    }
    this.leavePatternId = leavePatternId

    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.loggedUser.empId,
      leavePatternId: leavePatternId,
      empOfficialId: this.loggedUser.empOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves,
      "leavePaidOrUnpaid":this.leavePaidOrUnpaid
    }
    this.LeaveMaster = obj.leaveId;
    this.leaveErrorMessage=[]
      this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
      this.leaveErrorMessage = data.result;
      
      this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
     
      this.medicalCertificate = data.result.medicalCertificate;
      if(data.result.afterHowManydays != null){
        this.medicalCertificateDays = parseInt(data.result.afterHowManydays);
      }
      else{
        this.medicalCertificateDays = 0;
      }
      
      this.leaveOrCoff = data.result.leaveOrCoff;
      
      if (this.leaveOrCoff == "coff") {
        this.coffFlag = true
      } else {
        this.coffFlag = false
      }
      /*START For Staggered Off*/ 
      if(this.leaveOrCoff == "soff"){
        this.soffFlag = true;
      }
      else {
        this.soffFlag = false;
      }
      /*End For Staggered Off*/ 
   this.leaveErrorMessageFlag = true;

    }, err => {
      if (err.json().result) {
       
       this.leaveErrorMessage = err.json().result;
        this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
        // this.leaveErrorMessageFlag2 = false;
        this.medicalCertificate = err.json().result;
        if(this.leaveErrorMessage["afterHowManydays"] != null){
          this.medicalCertificateDays = parseInt(this.leaveErrorMessage["afterHowManydays"]);
        }
        else{
          this.medicalCertificateDays = 0;
        }
        
        this.leaveOrCoff = err.json().result.leaveOrCoff
        if (this.leaveOrCoff == "coff") {
          this.coffFlag = true;
        } else {
          this.coffFlag = false;
        }
        /*START For Staggered Off*/ 
        if(this.leaveOrCoff == "soff"){
          this.soffFlag = true;
        }
        else {
          this.soffFlag = false;
        }
        /*End For Staggered Off*/ 
        }
        else {
        this.leaveErrorMessage = [];
        }

    })
  }

  showFirstSecHalf(){
    this.medicalCertificateDays = 0;
    this.firstSecHalfFlag = true;
    this.model.typeofleave = "First Half";
    this.typeOfLeave("First Half");
   }
 
  onSearchChange(searchtext) {
    this.searchText = searchtext;
   }

  deleteLanguage(i) {
    this.selectedEmployee.splice(i, 1);
  }
getAllEmployee() {
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(
      data => {
        this.Employeedata = data["result"];    
      },
      (err: HttpErrorResponse) => {
        
      });
  }
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    let ServiceAttendanceDate = this.calendarService.getCalendarAttendance(this.date , this.attendanceData , this.HolidayData , this.currentYear);
    this.daysInThisMonth = ServiceAttendanceDate.daysInThisMonth;
    this.daysInLastMonth = ServiceAttendanceDate.daysInLastMonth;
    this.daysInNextMonth = ServiceAttendanceDate.daysInNextMonth;
  }

  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ":" + rminutes;
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    
    this.attendanceData = []
     this.getDaysOfMonth();
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
   this.getDaysOfMonth();
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  
  selectDate(day) {
    this.currentDate = day;

    if (this.from == 'from') {
      this.model.frmdate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
    } else if (this.from == 'to') {
      this.model.todate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
    }
  }
  fixDecimals(value: string) {
    value = "" + value;
    value = value.trim();
    value = parseFloat(value).toFixed(2);
    return value;
  }
  

  onSubmit(f) {
    
    if (!this.loggedUser.firstLevelReportingManager) {
      this.loggedUser.firstLevelReportingManager = 0;
    }
    if (!this.loggedUser.secondLevelReportingManager) {
      this.loggedUser.secondLevelReportingManager = 0;
    }
    if (!this.loggedUser.thirdLevelReportingManager) {
      this.loggedUser.thirdLevelReportingManager = 0;
    }
    if (this.leaveConfigList) {
      for (let i = 0; i < this.leaveConfigList.length; i++) {
        if (this.leaveConfigList[i].leaveId == this.model.type) {
          this.model.leaveCode = this.leaveConfigList[i].leaveCode;
          this.model.leaveName = this.leaveConfigList[i].leaveName;
        }
      }
    }
      
    if (this.reaplyStatus) {
        this.reaplyStatus= false;

    }
    else {

      if (this.isLeaveEdit) {
        let obj1 = new applyLeave();
      
        obj1.compId =  this.loggedUser.compId
        obj1.leaveCount = this.selectedLeaveCount;
        obj1.leaveRequestId = this.model.leaveRequestId;
        obj1.handedOverWorkList = this.model.handedover;
        obj1.employeesToBeNotifiedList = this.model.empnotified;
        obj1.empPerId = this.empPerId;
        obj1.startDate = this.model.frmdate;
        obj1.endDate = this.model.todate;
        obj1.leaveReson = this.model.leavereson;
        obj1.leaveId = parseInt(this.model.type);
        obj1.leaveCode = this.model.leaveCode;
        obj1.typeOfLeave = this.model.typeofleave;
        obj1.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
        obj1.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
        obj1.subEmpTypeId = this.loggedUser.subEmpTypeId;
        obj1.leavePatternId = this.leavePatternId;
        obj1.empOfficialId = this.loggedUser.empOfficialId;
        obj1.updateFromLeaveOrSugg = this.model.updateFromLeaveOrSugg;
        obj1.medCertifPath =this.model.filePath;      
        let file = this.model.filePath;
        if(file != null){
         this.selectedFileName =file.split("/").pop();
       }
        if (this.loggedUser.leaveHrApproval) {
          obj1.thirdLevelReportingManager = this.loggedUser.hrEmpId;
        }
        else {
          obj1.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
        }
        if(obj1.typeOfLeave =="Unpaid"){
          this.leaveConfigList=true;
        }
        if(this.reaplyStatus == true){
          
          this.getLeaveSuggestionList();
        }

        obj1.medCertifPath = this.model.filePath;
       
        this.isLeaveEdit = false;
     
        this.leaverequestService.updateLeaveRequest(obj1).subscribe(data => {
          this.toastr.success('Leave updated successfully');
          this.medicalCertificate = true;
          this.selectedLeaveCount = 1;
          this.model.selectedLeaveCount = 1;
          this.selectedFileName = "No File Choosen";
          this.getLeaveApplicationListByEmpId();
          this.configList();
          this.Leaves();
          f.reset();
          this.reset(f,this.model.filePath);
          this.resetCoff(f)
          $("#file").val('')
        }, err => {
          this.toastr.error(err.json().result.message);
        })
      } else {
        if (this.compareDate(new Date(this.model.frmdate), new Date(this.model.todate))) {
        
          let obj1 = new applyLeave();
          obj1.leaveCount = this.selectedLeaveCount;
          obj1.handedOverWorkList = this.model.handedover;
          obj1.employeesToBeNotifiedList = this.model.empnotified;
          obj1.compId =  this.loggedUser.compId
          obj1.empPerId = this.empPerId;
          obj1.startDate = this.model.frmdate;
          obj1.endDate = this.model.todate;
          obj1.leaveReson = this.model.leavereson;
          obj1.leaveId = parseInt(this.model.type);
          obj1.leaveCode = this.model.leaveCode;
          obj1.leaveName = this.model.leaveName;
          obj1.dayOfWeek = this.weekn;
          obj1.medCertifPath =this.model.filePath;
         obj1.typeOfLeave = this.model.typeofleave;
          obj1.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
          obj1.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
          obj1.subEmpTypeId = this.loggedUser.subEmpTypeId;
          obj1.leavePatternId = this.leavePatternId;
          obj1.empOfficialId = this.loggedUser.empOfficialId;
          if (this.loggedUser.leaveHrApproval) {
            obj1.thirdLevelReportingManager = this.loggedUser.hrEmpId;
          }
          else {
            obj1.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
          }

       obj1.afterHowManydays = this.afterHowManydays;
          this.leaverequestService.postleave(obj1).subscribe(data => {
            console.log("this is the data",this.LeaveData)         
           this.toastr.success('Leave applied successfully');
           this.router.navigate(['/layout/attendancemenu/attendancemenu']);
            this.configList();
            this.selectedLeaveCount = 1;
            this.medicalCertificate = false;
            this.firstSecHalfFlag = false;
            this.model.filePath = '';
            if(obj1.typeOfLeave == 'Unpaid'){
              this.model.selectedLeaveCount = 0;
            }
            this.model.selectedLeaveCount = 1;
             
           this.getLeaveApplicationListByEmpId();
            this.Leaves();
            f.reset();
            this.showImagePreview=false;
           // this.reset(f,this.model.filePath);
           
          }, err => {
            this.toastr.error(err.json().result.message);
            if(err.json().result.message=='Medical Cirtificate Required'){
              this.router.navigate(['/layout/attendancemenu/attendancemenu']);
            }
            this.model=[];
            this.reset(f,this.model.filePath);
          })
        }
      }
    }
  }

  compareDate(date1: Date, date2: Date) {
    let d1 = new Date(date1);
    let d2 = new Date(date2);
    let same = d1.getTime() === d2.getTime();
    if (same) return true;
    if (d1 > d2) return false;
    if (d1 < d2) return true;
  }


  getDataHRRequestToWorkList() {
    this.AllHRRequestToWorkList = [];
    let url = this.baseurl + '/HRRequestToWorkList/';
    this.httpService.get(url + this.empPerId).subscribe(
      data => {
       
        this.AllHRRequestToWorkList = data['result']
      },
      (err: HttpErrorResponse) => {
       
        this.AllHRRequestToWorkList = []
      });

  }

  acceptHRworkingRequest(data, j) {
    document.getElementById('LE' + j).click();
    
    let obj = {
      hrRequestToWorkId: data.hrRequestToWorkId,
      workingDate: data.workingDate,
      empOfficialId: this.loggedUser.empOfficialId,
      empId: data.empId,
      shiftMasterId: data.shiftMasterId,
      reason: data.reason,
      empReason: data.empReason
    }
    

    this.leaverequestService.acceptHrRequest(obj).subscribe(data => {
      
      // this.getDataHRRequestToWorkList();
      this.toastr.success('Extra work request accepted successfully');
      this.getDataHRRequestToWorkList();
    }, err => {
     
      this.toastr.error('Failed to accept extra work request');
      // this.getDataHRRequestToWorkList();
    })

  }

  rejectHRworkingRequest(data, j) {
    document.getElementById('LE' + j).click();
    let obj = {
      hrRequestToWorkId: data.hrRequestToWorkId,
      workingDate: data.workingDate,
      empOfficialId: this.empPerId,
      empId: data.empId,
      shiftMasterId: data.shiftMasterId,
      reason: data.reason,
      empReason: data.empReason
    }

    this.leaverequestService.rejectHrRequest(obj).subscribe(data => {
      this.getDataHRRequestToWorkList();
      this.toastr.success('Extra work request rejected successfully');
    }, err => {
     
      this.toastr.error('Failed to reject extra work request');
    })

  }
  //=======================Work Flow Start=======================
  workFlowList: any = [];
  leaveWorkFlow(item) {
    let leaveRequestId;
    if(item.finalStatus == "Approved"){
      leaveRequestId = item.leaveApproveRequestId;
    }
    else{
      leaveRequestId = item.leaveRequestId;
    }
    this.workFlowList = [];
    this.leaverequestService.getLeaveWorkFlowList(leaveRequestId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  leaveApproveWorkFlow(item) {
    this.leaverequestService.getLeaveApproveWorkFlowList(item.leaveRequestId).subscribe(data => {
      }, err => {
      this.workFlowList = [];
    })
  }
  //=======================Work Flow Ends=======================

  arrowkeyLocation = 0;
  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
       break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }

  filterItem(value) {
   
    if (!value) {
      this.empFlag = false;
    }
    if (value) {
      this.empFlag = true;
    }
  }

  searchEmpPerId: any;
  suggestThis(item) {
    this.model.handedover = [item];
  
    this.searchQuery = item.fullName + " " + item.lastName + " - " + item.empId;
    this.searchEmpPerId = item.empPerId;
  
    this.designation = item.descName
    this.empFlag = false;
  }


  arrowkeyLocation1 = 0;
  keyDown1(event: KeyboardEvent) {
 

    switch (event.keyCode) {
      case 38:
        this.arrowkeyLocation1--;
   
        break;
      case 40:
        this.arrowkeyLocation1++;
     
        break;
    }
  }

  filterItem1(value) {
  
    if (!value) {
      this.empFlag1 = false;
    }
    if (value) {
      this.empFlag1 = true;
    }
  }

  searchEmpPerId1: any;
  suggestThis1(item) {
    this.model.empnotified = [item];
    this.searchQuery1 = item.fullName + " " + item.lastName + " - " + item.empId;
  
    this.searchEmpPerId1 = item.empPerId;
  
    this.designation = item.descName
    this.empFlag1 = false;
  }

  fileToUpload: File = null;

  
  resetCoff(f) {
    setTimeout(() => {
      this.selectedLeaveCount = 1;
      this.model.selectedLeaveCount = 1;
      this.searchQuery = null;
      this.searchQuery1 = null;
      this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.type = "";
      this.model.typeofleave = "Fullday";
      this.model.leavereson = "";

      this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.coffModel.coffCreditedId = this.slectecdoption;
      this.coffModel.dateAndTime = this.todaysDate;
      this.model.type = 'Selectund'
      this.coffModel.halfDayType = "First Half";
      this.coffModel.coffType = "Full Day";
      this.coffModel.reson = " "

      
    }, 100);
  }
  FullDaygetdate() {
    this.loggedUser.empPerId = this.empPerId
    let url = this.baseurl + '/coffregularisation/';
    this.httpService.get(url + this.empPerId).subscribe(
      (data: any) => {
        if(data != []){
        this.getfullDayDate = data.result;
        }else{
          this.getfullDayDate = [];
        }
      },
      (err: HttpErrorResponse) => {
     
      }
    );

  }

  /**START OF Staggered apply form dropdown functinality */
  getSoffFulldate(){
    this.dataservice.getRecordsByid('/getSoffCriditedData',this.empPerId).subscribe((data: any) => {
     
        this.getSofffullDayDate = data.result;
      },
      (err: HttpErrorResponse) => {
      
      }
    );
  }
  /**END OF Staggered apply form dropdown functinality */

  coffDateChanged(e) {
    this.coffModel.dateAndTime = e;
  }
  HalfDaygetdate() {
    this.loggedUser.empPerId = this.empPerId;
    let url = this.baseurl + '/halfcoffregularisation/';
    this.httpService.get(url + this.empPerId).subscribe(
      (data: any) => {
        if(data != []){
        this.getHlafDayDate = data.result;
      
        }else{
          this.getHlafDayDate = [];
        }
      },
      (err: HttpErrorResponse) => {
      
      }
    );
  }
 
  onSubmitCoff() {
    if (this.loggedUser.firstLevelReportingManager == null) {
      this.coffModel.firstLevelReportingManager = 0
    } else {
      this.coffModel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager
    }
    if (this.loggedUser.secondLevelReportingManager == null) {
      this.coffModel.secondLevelReportingManager = 0
    } else {
      this.coffModel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager
    }
    if (this.loggedUser.thirdLevelReportingManager == null) {
      this.coffModel.thirdLevelReportingManager = 0
    } else {
      this.coffModel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager
    }
    //half day type issue
    if(this.coffModel.coffType == "Full Day"){
      this.coffModel.halfDayType="";
    }
    if (this.isCoffEdit) {
      this.coffModel.leavePatternId = this.leavePatternId
      this.coffModel.empPerId = this.empPerId
      if (this.leaveConfigList) {
        for (let i = 0; i < this.leaveConfigList.length; i++) {
          if (this.leaveConfigList[i].leavePatternId == this.coffModel.leavePatternId) {
          this.coffModel.leaveName = this.leaveConfigList[i].leaveName;
          }
        }
      }
      this.CoffServic.updateCoffRequest(this.coffModel).subscribe(data => {
      
        this.isCoffEdit = false;
        this.getLeaveApplicationListByEmpId();
        this.model.type = this.medicalCertificate;
      
        this.coffModel.reson = ''
        this.toastr.success('C-Off request updated successfully!', 'C-Off');
        this.configList();
        this.coffFlag = false
       
      }, err => {
      
        this.toastr.error(err.json().result.message);
      })
    }
    else {
    
      this.coffModel.empPerId = this.loggedUser.empPerId;

      this.leaveMaster.leaveId = this.LeaveMaster;
      this.coffModel.leaveMaster = this.leaveMaster;
      this.coffModel.leavePatternId = this.leavePatternId;
      if (this.leaveConfigList) {
        for (let i = 0; i < this.leaveConfigList.length; i++) {
          if (this.leaveConfigList[i].leavePatternId == this.coffModel.leavePatternId) {
              this.coffModel.leaveName = this.leaveConfigList[i].leaveName;
          }
        }
      }
    
      this.CoffServic.postCoffRequest(this.coffModel).subscribe(data => {
        
          this.getLeaveApplicationListByEmpId();
          this.model.type = this.Selectund;
          this.coffModel.reson = '';
          this.toastr.success('C-Off applied successfully!');
          this.configList();
          this.router.navigate(['/layout/attendancemenu/attendancemenu']);
          setTimeout(() => {
            this.selectedLeaveCount = 1;
            this.model.selectedLeaveCount = 1;
            this.searchQuery = null;
            this.searchQuery1 = null;
            this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            this.model.type = "";
            this.model.typeofleave = "Fullday";
            this.model.leavereson = "";
      
            this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            this.coffModel.coffCreditedId = this.slectecdoption;
            this.coffModel.dateAndTime = this.todaysDate;
            this.model.type = 'Selectund'
            this.coffModel.halfDayType = "First Half";
            this.coffModel.coffType = "Full Day";
            this.coffModel.reson = " "
        },100);
          this.FullDaydateData();
          this.HalfDaygetdate();
          this.coffFlag = false;
        },
          (err) => {
            this.toastr.error(err.json().result.message);
          });
    }
  }

  
  editCoffRequest(item) {
    this.coffFlag = true;
    this.soffFlag = false;
    this.CoffServic.getCoffById(item.requesCoffIdId).subscribe(data => {
      this.coffModel.requesCoffIdId = item.requesCoffIdId;
     document.getElementById('pills-home-tab').click();
      let editData: any;
     
      editData = data.json().result;
    
      this.isCoffEdit = true;
      this.coffModel.requesCoffIdId = item.requesCoffIdId;
      this.coffModel.dateAndTime = editData.dateAndTime;
      this.coffModel.halfDayType = editData.halfDayType;
      this.coffModel.halfDayType = editData.halfDayType;
      this.coffModel.reson = editData.reson;
      this.coffModel.coffType = editData.coffType;
      this.coffModel.coffCreditedId = editData.coffCreditedId;
      this.coffModel.extraWorkedDay = editData.extraWorkedDay;
      this.coffModel.leaveMaster = editData.leaveMaster
      this.model.type = editData.leaveId
      this.model.leaveId = editData.leaveId;
    }, err => {

    })
  }

  getCofflist() {
    
    this.CoffList = new Array();
    this.CoffServic.getCoffRequestList(this.RequestObj).subscribe(data => {
      
      this.CoffList = data.result;
      
      setTimeout(function () {
        $(function () {
          var table = $('#CompTableCo').DataTable();
        });
      }, 1000);
    }, err => {
     
      this.CoffList = [];
      this.workFlowList = [];
    })
  }

  coffApproveWorkFlow(item) {
   
    this.personalservice.getCoffApproveWorkflowList(item.requesCoffIdId).subscribe(data => {
    
      this.workFlowList = data.json().result;
    }, err => {
     
      this.workFlowList = [];

    })
  }

  coffWorkFlow(item) {
    
    this.personalservice.getCoffWorkflowList(item.requesCoffIdId).subscribe(data => {
    
      this.workFlowList = data.json().result;
    }, err => {
  
      this.workFlowList = [];
    })
  }
  modelobj: any;
  

  onFileAdded(file: FilePreviewModel) 
{
  this.showImagePreview = true;
  this.fileData = file;
 this.dataservice.uploadFile(file).subscribe(data => {
    if(data.result){
    
      this.model.filePath=data.result.imageUrl;
   
      this.imageURL=data.result.imageUrl;
      this.selectedFileName =data.result.imageUrl;
    
    }else{
      this.model.filePath='';
     this.imageURL='';
      this.selectedFileName ='';
     this.onRemoveSuccess(file);
    }
  })
}
onRemoveSuccess(e: FilePreviewModel)
{
  this.removefile = true;
  let image=this.model.filePath;
  image =null;
  this.imageURL= '';
  this.selectedFileName = '';
 // showPreviewContainer
}

  
  dltCOffmodelEvent() {
    this.dltmodelFlagcoff = true;
    this.deleteCoffOBJ(this.dltrequestCoffId);
  }

  deleteCoffRequest(item) {
    this.dltmodelFlagcoff = false;
   
    this.dltrequestCoffId = item.requesCoffIdId;
    this.deleteCoffOBJ(item.requesCoffIdId);
  }

  deleteCoffOBJ(requesCoffIdId) {
    if (this.dltmodelFlagcoff == true) {
      if (this.dltmodelFlagcoff == true) {
        this.CoffServic.deleteCoffRequest(requesCoffIdId).subscribe(data => {
        
          this.getCofflist();
          this.configList();
          this.toastr.success('C-off request deleted successfully');
          this.getLeaveApplicationListByEmpId();
          this.FullDaygetdate(); 
          this.HalfDaydateData();
         }, err => {
        
          this.toastr.error('Failed to delete C-off request');
          
        });
      }
    }
  }

  /*---------------START of staggered of apply Form Submit functionality--------------*/
  getCollapseDate(e,id){
    for(let i = 0; i < this.getSofffullDayDate.length; i++){
      if(this.getSofffullDayDate[i].soffCreditId == id){
        this.soffModel.collapsDate = this.getSofffullDayDate[i].collapsDate;
      }
    }
  }
  /*---------------END of staggered off apply Form Submit functionality--------------*/

  /*---------------START of staggered off apply Form Submit functionality--------------*/
  onSubmitSoff() {
    if (this.loggedUser.firstLevelReportingManager == null) {
      this.soffModel.firstLevelReportingManager = 0;
    } else {
      this.soffModel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    }
    if (this.loggedUser.secondLevelReportingManager == null) {
      this.soffModel.secondLevelReportingManager = 0;
    } else {
      this.soffModel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    }
    if (this.loggedUser.thirdLevelReportingManager == null) {
      this.soffModel.thirdLevelReportingManager = 0;
    } else {
      this.soffModel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }
    if (this.isSoffEdit) {
      this.soffModel.empPerId = this.loggedUser.empPerId;
      this.leaveMaster.leaveId = this.LeaveMaster;
      this.soffModel.leaveMaster = this.leaveMaster;
      this.soffModel.leavePatternId = this.leavePatternId;
      this.soffModel.compId = this.loggedUser.compId;
    
      if (this.leaveConfigList) {
        for (let i = 0; i < this.leaveConfigList.length; i++) {
          if (this.leaveConfigList[i].leavePatternId == this.soffModel.leavePatternId) {
          this.soffModel.leaveName = this.leaveConfigList[i].leaveName;
          }
        }
      }
      this.dataservice.updateRecords('/soffrequest',this.soffModel).subscribe(data => {
        this.isSoffEdit = false;
        this.getLeaveApplicationListByEmpId();
        this.model.type = this.medicalCertificate;
        this.soffModel.reson = '';
        this.toastr.success('S-Off request updated successfully!', 'S-Off');
        this.configList();
        this.getSoffFulldate();
        this.soffFlag = false;
      }, err => {
        this.errMsg = err.error.result.message;
        this.toastr.error(this.errMsg);
      })
    }
    else{
      this.soffModel.empPerId = this.loggedUser.empPerId;
      this.leaveMaster.leaveId = this.LeaveMaster;
      this.soffModel.leaveMaster = this.leaveMaster;
      this.soffModel.leavePatternId = this.leavePatternId;
      this.soffModel.compId = this.loggedUser.compId;
      if (this.leaveConfigList) {
        for (let i = 0; i < this.leaveConfigList.length; i++) {
          if (this.leaveConfigList[i].leavePatternId == this.soffModel.leavePatternId) {
              this.soffModel.leaveName = this.leaveConfigList[i].leaveName;
          }
        }
      }
    this.dataservice.createRecord('/soffrequest',this.soffModel)
    .subscribe(data => {
    
      this.errMsg = data.result.message;
      this.getLeaveApplicationListByEmpId();
      this.model.type = this.Selectund;
      this.soffModel.reson = '';
      this.toastr.success('Staggered-Off applied successfully!');
      this.configList();
      setTimeout(() => {
        this.selectedLeaveCount = 1;
        this.model.selectedLeaveCount = 1;
        this.searchQuery = null;
        this.searchQuery1 = null;
        this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
        this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
        this.soffModel.soffCreditId = this.slectecdoption;
        this.soffModel.forDate = this.todaysDate;
        this.soffModel.collapsDate = null;
        this.model.type = 'Selectund';
        this.soffModel.reson = " "; 
      }, 100);
      this.getSoffFulldate();
      this.soffFlag = false;
    },
      (err) => {
        this.errMsg = err.error.result.message;
        this.toastr.error(this.errMsg);
      });
    }
     
  }
  /*---------------END of staggered of apply Form Submit functionality--------------*/

  /**--------------START OF Staggered off Reset Form Functinality-------------------  */
  resetSoff(f) {
    setTimeout(() => {
      this.selectedLeaveCount = 1;
      this.model.selectedLeaveCount = 1;
      this.searchQuery = null;
      this.searchQuery1 = null;
      this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.soffModel.soffCreditId = this.slectecdoption;
      this.soffModel.forDate = this.todaysDate;
      this.soffModel.collapsDate = null;
      this.model.type = 'Selectund';
      this.soffModel.reson = " "; 
    }, 100);
  }
  /**--------------END OF Staggered off Reset Form Functinality-------------------  */

  /*---------------START of staggered off Edit button click Form functionality--------------*/
  editSoffRequest(item) {
    this.soffFlag = true;
    this.coffFlag = false;
   
    this.dataservice.getRecordsByid('/soffrequest',item).subscribe(data => {
      let editData: any;
      this.soffModel.staggeredOffReqAndApproveId = item.staggeredOffReqAndApproveId;
      document.getElementById('pills-home-tab').click();
      editData = data.result;
      this.isSoffEdit = true;
      this.soffModel.staggeredOffReqAndApproveId = editData.staggeredOffReqAndApproveId;
      this.soffModel.forDate = editData.forDate;
      this.soffModel.reson = editData.reson;
      this.soffModel.collapsDate = editData.collapsDate;
      this.soffModel.soffCreditId = editData.soffCreditId;
      this.soffModel.soGenToDate = editData.soGenToDate;
      this.soffModel.leaveMaster = editData.leaveMaster;
      this.soffModel.leaveId = editData.leaveId;
      this.soffModel.finalStatus = editData.finalStatus;
      this.soffModel.empPerId = this.loggedUser.empPerId;
      this.soffModel.leavePatternId = editData.leavePatternId;
      this.soffModel.leaveName = editData.leaveName;
      this.model.type = editData.leaveId
      this.model.leaveId = editData.leaveId;
    }, err => {
      this.toastr.error('Failed to Edit S-off request');
    })
  }
  /*---------------END of staggered off Edit button click Form functionality--------------*/

  /*---------------START of staggered off Delete button click Form functionality--------------*/
  dltSOffmodelEvent() {
    this.dltmodelFlagsoff = true;
    this.deleteSoffOBJ(this.dltrequestSoffId, this.dltSoffId);
  }

  deleteSoffRequest(staggeredOffReqAndApproveId,sOffCreditId) {
    this.dltmodelFlagsoff = false;
    this.dltrequestSoffId = staggeredOffReqAndApproveId ;
    this.dltSoffId = sOffCreditId;
    this.deleteSoffOBJ(staggeredOffReqAndApproveId,sOffCreditId);
  }

  deleteSoffOBJ(staggeredOffReqAndApproveId,sOffCreditId) {
    if (this.dltmodelFlagsoff == true) {
      if (this.dltmodelFlagsoff == true) {
        this.dataservice.deleteRecords('/soffrequest',staggeredOffReqAndApproveId,sOffCreditId).subscribe(data => {
          this.getCofflist();
          this.configList();
          this.getSoffFulldate();
          this.toastr.success('S-off request deleted successfully');
          this.getLeaveApplicationListByEmpId();
        }, err => {
          this.toastr.error('Failed to delete S-off request');
        });
      }
    }
  }
  /*---------------END of staggered off Delete button click Form functionality--------------*/

  cancelSoff(){
    this.router.navigate(['/layout/attendancemenu']);
  }

  soffForDate(event) {
    this.soffModel.forDate = event;
  }

  cancelLeave(){
    this.router.navigate(['/layout/attendancemenu/attendancemenu']);
   
  }
 
}
