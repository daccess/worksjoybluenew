import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { LeaverequestComponent } from 'src/app/leaverequest/leaverequest.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: LeaverequestComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leaverequest',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaverequest',
                    component: LeaverequestComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);