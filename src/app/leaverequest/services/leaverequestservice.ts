import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

// import { satutory } from '../models/satutorymodel';
// import { MainURL } from '../../../shared/configurl';
import { from } from 'rxjs';
import { MainURL } from 'src/app/shared/configurl';
//import { satutory } from 'src/app/employee/shared/models/satutorymodel';
import { applyLeave } from '../model/applyLeave';

@Injectable()
export class leaveRequestService
 {
 
  baseurl = MainURL.HostUrl;
  empOfficialId : string = ""
  constructor(private http : Http) { }

  postleave(leave : applyLeave){
    let url = this.baseurl + '/leaverequest';
    var body = JSON.stringify(leave);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getLeaveConfigurationList(empPerId){
    let url = this.baseurl + '/getLeaveData/'+empPerId;
    return this.http.get(url);
  }
  monthlyAttendance(empOfficialId){
    let url = this.baseurl + '/callender';
    var body = JSON.stringify(empOfficialId);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getLeaveApplicationListByEmpId(empPerId){
    let url = this.baseurl + '/requestApplication';
    var body = JSON.stringify(empPerId);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  deleteLeaveRequest(id){
    let url1 = this.baseurl +'/LeaveRequest/';
    return this.http.delete(url1 +id)
  }
  updateLeaveRequest(obj){
    let url1 = this.baseurl +'/updateleave/';
    return this.http.put(url1 ,obj)
  }

  getLeaveApplicationListById(id){
    let url = this.baseurl + '/getLeaveList/'+id;
    // var body = JSON.stringify(model);
    return this.http.get(url);
  }

  checkConfigForLeaveApply(leave){
    let url = this.baseurl + '/checkLeaveConfiguration';
    var body = JSON.stringify(leave);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getLeaveSuggestionListById(id){
    let url = this.baseurl + '/newDateSuggestion/'+id;
    // var body = JSON.stringify(model);
    return this.http.get(url);
  }

  // getHRRequestToWorkListById(id){
  //   let url = this.baseurl + '/HRRequestToWorkList/'+id;
  //   // var body = JSON.stringify(model);
  //   return this.http.get(url);
  // }

  rejectLeaveSuggestion(obj){
    let url1 = this.baseurl +'/rejectLeaveSuggestion/';
    return this.http.put(url1 ,obj)
  }

  acceptLeaveSuggestion(leave){
    let url = this.baseurl + '/leaveSuggestion';
    var body = JSON.stringify(leave);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  acceptHrRequest(hrrquest){
    let url = this.baseurl + '/hrRequestToWorkaccept';
    var body = JSON.stringify(hrrquest);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    // return this.http.get(url,body).map(x => x.json());
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  rejectHrRequest(obj){
    let url1 = this.baseurl +'/hrRequestToWorkReject/';
    return this.http.put(url1 ,obj)
  }

  getLeaveWorkFlowList(id){
    let url = this.baseurl + '/workflow/getWorkflowListOfLeave/'+id;
    // var body = JSON.stringify(model);
    return this.http.get(url);
  }
  getLeaveApproveWorkFlowList(id){
    let url = this.baseurl + '/workflow/getWorkflowApproveListOfLeave/'+id;
    // var body = JSON.stringify(model);
    return this.http.get(url);
  }
}