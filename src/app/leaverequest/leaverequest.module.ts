import { routing } from './leaverequest.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SearchPipeForEmpleave } from './SearchPipe/Serach';
import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { LeaverequestComponent } from './leaverequest.component'
import { leaveRequestService } from './services/leaverequestservice';
import { FilterPipe } from './pipe/filter.pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { datePipe } from '../shared/pipes/custom-date-pipe';
import { coffService } from '../otherrequests/shared/services/CoffService';
import { PersonalRequetsService } from '../otherrequests/shared/services/personalRequestService';
import { FilePickerModule } from 'ngx-awesome-uploader';


@NgModule({
    declarations: [
        LeaverequestComponent,
        FilterPipe,
        SearchPipeForEmpleave, 
        datePipe
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     FilePickerModule,
     NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [leaveRequestService,coffService,PersonalRequetsService]
  })
  export class LeaverequestModule { 
      constructor(){

      }
  }
  