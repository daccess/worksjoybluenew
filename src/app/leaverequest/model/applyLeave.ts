export class applyLeave {
  leaveName: any; 
 empPerId: number;
  startDate: string;
  endDate: string;
  leaveReson: string;
  handedOverWorkList = [];
  employeesToBeNotifiedList = [];
  EmployeePerInfo: string;
  leaveId: number;
  typeOfLeave: string;
  leaveCode: any;
  leaveRequestId: any;
  leaveCount: number;
  leavePatternId : any;
  firstLevelReportingManager: any = 0;
  secondLevelReportingManager: any = 0;
  thirdLevelReportingManager: any = 0;
  // addressType1: string;
  // address1: string;
  // country1: string;
  // state1: string;
  // dist1: string;
  // city1: string;
  // pinCode1: string;
  dayOfWeek:any;
  compId : any
  afterHowManydays:String;
  medCertifPath: any;
  subEmpTypeId: any;
  empOfficialId: any;
  updateFromLeaveOrSugg: any;

}