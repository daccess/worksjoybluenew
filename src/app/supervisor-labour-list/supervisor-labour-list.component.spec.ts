import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLabourListComponent } from './supervisor-labour-list.component';

describe('SupervisorLabourListComponent', () => {
  let component: SupervisorLabourListComponent;
  let fixture: ComponentFixture<SupervisorLabourListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLabourListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLabourListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
