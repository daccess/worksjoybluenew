import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-supervisor-labour-list',
  templateUrl: './supervisor-labour-list.component.html',
  styleUrls: ['./supervisor-labour-list.component.css']
})
export class SupervisorLabourListComponent implements OnInit {
  LabourDatas: any;
  baseurl = MainURL.HostUrl;
  empids: any;
  token: any;
  user: string;
  users: any;

  locationdata: any;
  locationId: any;
  labourlistEdit: string;
  labourlistEdit1:string;
  labourIds: any;
  locationIds: any;
  labourperids: any;
  labourId: any;
  manReqStatusIde: any;
  contractorId: any;
  constructor(public httpservice:HttpClient,public router:Router,public Spinner:NgxSpinnerService,public toastr: ToastrService) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId;
    this.contractorId=this.users.roleList.contractorId
    // this.getlocationData()
    this.getLabourData();
  }
  getLabourData() {
    debugger
    let url = this.baseurl + `/SupervisorLabourList/${this.contractorId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.LabourDatas = data['result']
      setTimeout(function () {
  
        $(function () {
  
          var table = $('#emptable').DataTable({
  
            retrieve: true,
            searching:false
          })
  
        });
      }, 100);
      this.Spinner.hide();
  
    },
   
      (err: HttpErrorResponse) => {
  
      })
  
  }
  gotoDocument(e:any){
    debugger
    this.labourId=e.labourInfo.labourPerId;
    this.manReqStatusIde=e.labourMasterId;
    sessionStorage.setItem("manRequestIds",this.manReqStatusIde)
    sessionStorage.setItem("LabourPerIdss",this.labourId)
    this.router.navigate(['/layout/originalPass'])
    
  }

}
