export class SoffReq {
    soffCreditId: any;
    forDate: Date;
    reson: String;
    empPerId: number
    requestSoffId: number;
    leaveMaster : { leaveId: any}
    subEmpTypeId: any;
    leavePatternId : any;
    compId : any;
    leaveName: any;
    firstLevelReportingManager: number;
    secondLevelReportingManager: number;
    thirdLevelReportingManager: number;
    soffType: String;
    soGenToDate: any;
    collapsDate: Date;
    empId:any;
    staggeredOffReqAndApproveId: any;
    leaveId: any;
    finalStatus: any;
}