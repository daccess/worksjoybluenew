export class lateearlyTypedata {
  firstLevelReportingManager: number;
  secondLevelReportingManager: number;
  thirdLevelReportingManager: number;
  empPerId: number;
  requesLateEarlyId: number;
  requestType: string;
  reson: string;
  dateAndTime1: any;
  attendanceId: any;
  empTypeId: any;
  subEmpTypeId: any;
  compId: any
  dateAndTime2: any;
}
