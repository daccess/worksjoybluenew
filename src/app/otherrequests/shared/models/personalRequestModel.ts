export class personalrequestdata {
  firstLevelReportingManager: number;
  secondLevelReportingManager: number;
  thirdLevelReportingManager: number;
  empPerId: number;
  requestPRId: number;
  dateAndTime: any;
  // dateAndTime1: Date;
  reson: string;
  enteryDateAndTime: Date;
  finalStatus: string;
  startTime: any;
  endTime: any;
  empTypeId: any;
  subEmpTypeId: any;
  compId : any

  // startdate : any;
}