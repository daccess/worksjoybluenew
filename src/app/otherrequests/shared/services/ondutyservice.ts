import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { onDutydata } from '../models/onDutyModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class OndutyService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postondutyRequest(onduty : onDutydata){
   
    let url = this.baseurl + '/RequestOD';
    var body = JSON.stringify(onduty);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getOnDutyRequestList(personal){
    let url = this.baseurl + '/requestod';
    var body = JSON.stringify(personal);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  deleteOnDutyRequest(id){
    let url1 = this.baseurl +'/RequestOD/';
    return this.http.delete(url1 +id)
  }
  updateOnDutyRequest(obj){
    let url1 = this.baseurl +'/ODrequestEdit/';
    return this.http.put(url1 ,obj)
  }
  getOnDutyRequestById(id){
    return this.http.get(this.baseurl +'/ODrequest/'+ id)
  }
}