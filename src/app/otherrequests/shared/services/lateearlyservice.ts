import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { lateearlyTypedata } from '../models/lateearlyModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class LateEarlyService {
  baseurl = MainURL.HostUrl
  requesLateEarlyId: any;
  constructor(private http : Http) { }

  postlateearlyRequest(lateearly : lateearlyTypedata){
    let url = this.baseurl + '/RequestLateEarly';
    var body = JSON.stringify(lateearly);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getLateEarlyDateList(empOfficialId){
      return this.http.get(this.baseurl +'/LateEarlyList/'+ empOfficialId)
    
  }
  getEarlyLateDateList(empOfficialId){
    return this.http.get(this.baseurl +'/EarlyLateList/'+ empOfficialId)  
}
  getLateEarlyRequestList(personal){
    let url = this.baseurl + '/requestlateearly';
    var body = JSON.stringify(personal);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  deleteLateEarlyRequest(id,attenDate,empOfficialIdL){
    parseInt(empOfficialIdL)
    parseInt(id)
    let url1 = this.baseurl +'/RequestLateEarly?attenDate='+attenDate+'&empOfficialId='+empOfficialIdL+'&requesLateEarlyId=';
    return this.http.delete(url1 + id)
  }
  updateLateEarlyRequest(obj){
    let url1 = this.baseurl +'/LateEarlyEditrequest/';
    return this.http.put(url1 ,obj)
  }
  getLateEarlyRequestById(id){
    return this.http.get(this.baseurl +'/requesLateEarly/'+ id)
  }
}