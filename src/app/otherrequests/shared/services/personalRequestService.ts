import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { personalrequestdata } from '../models/personalRequestModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class PersonalRequetsService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postPersonalRequest(personal : personalrequestdata){
    let url = this.baseurl + '/RequestPR';
    var body = JSON.stringify(personal);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getPermissionRequestList(personal){
    let url = this.baseurl + '/requestpr';
    var body = JSON.stringify(personal);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getExtraWorkRequestList(personal){
    let url = this.baseurl + '/requestextraworkinglist';
    var body = JSON.stringify(personal);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  DeletePermissionRequest(id){
    let url1 = this.baseurl +'/RequestPR/';
    return this.http.delete(url1 +id)
  }

  updatePermissionRequest(obj){
    let url1 = this.baseurl +'/PRrequestEdit/';
    return this.http.put(url1 ,obj)
  }

  getPermissionRequestById(id){
    let url1 = this.baseurl +'/PRrequest/';
    return this.http.get(url1 +id)
    // .map(x=>{
    //   x.json()
    // });
  }

  getPrWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfPR/'+id;
    return this.http.get(url1)
  }

  getLateEarlyWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfLateEarly/'+id;
    return this.http.get(url1)
  }

  getMissPunchWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfMissPunch/'+id;
    return this.http.get(url1)
  }

  getOdWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfOD/'+id;
    return this.http.get(url1)
  }

  getWorkFromHomeWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfWFH/'+id;
    return this.http.get(url1)
  }

  getExtraWorkWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfExtraWork/'+id;
    return this.http.get(url1)
  }

  getCoffWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfCoff/'+id;
    return this.http.get(url1)
  }

  //=========Approve , Reject , Cancel Work Flow ==============

  getPrApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowApproveListOfPR/'+id;
    return this.http.get(url1)
  }

  getLateEarlyApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowApproveListOfLateEarly/'+id;
    return this.http.get(url1)
  }

  getMissPunchApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowApproveListOfMissPunch/'+id;
    return this.http.get(url1)
  }

  getOdApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowApproveListOfOD/'+id;
    return this.http.get(url1)
  }

  getWorkFromHomeApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowApproveListOfWFH/'+id;
    return this.http.get(url1)
  }

  getExtraWorkApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfApproveExtraWork/'+id;
    return this.http.get(url1)
  }

  getCoffApproveWorkflowList(id){
    let url1 = this.baseurl +'/workflow/getWorkflowListOfApproveCoff/'+id;
    return this.http.get(url1)
  }

  //=========Approve , Reject , Cancel Work Flow ==============
}