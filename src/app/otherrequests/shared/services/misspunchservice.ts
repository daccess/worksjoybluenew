import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { missPunchdata } from '../models/missPunchModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class MisspunchService {
  baseurl = MainURL.HostUrl
  constructor(private http: Http) { }

  postmisspunchRequest(misspunch: missPunchdata) {
    let url = this.baseurl + '/RequestMissPunch';
    var body = JSON.stringify(misspunch);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post(url, body, requestOptions).map(x => x.json());
  }
  getLateMissPunchList(empOfficialId) {
    return this.http.get(this.baseurl + '/MissPunchList/' + empOfficialId)
  }

  getMissPunchRequestList(personal){
    let url = this.baseurl + '/requestmisspunch';
    var body = JSON.stringify(personal);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  deleteMissPunchRequest(id,attenDate,empOfficialId){
    let url1 = this.baseurl +'/RequestMissPunch?attenDate='+attenDate+'&empOfficialId='+empOfficialId+'&requesMissPunchId='+id;
    return this.http.delete(url1)
  }
  updateMissPunchRequest(obj){
    let url1 = this.baseurl +'/MissPunchrequestEdit/';
    return this.http.put(url1 ,obj)
  }
  getMissPunchRequestById(id){
    return this.http.get(this.baseurl +'/requesMissPunch/'+ id)
  }

}