import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { workfromhomedata } from '../models/workfromhomemodel';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class WorkfromService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postworkfromRequest(workhome : workfromhomedata){
    let url = this.baseurl + '/RequestWFH';
    var body = JSON.stringify(workhome);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getWorkFromHomeRequestList(personal){
    let url = this.baseurl + '/requestwfh';
    var body = JSON.stringify(personal);
    
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  deleteWorkFromHomeRequest(id){
    let url1 = this.baseurl +'/RequestWFH/';
    return this.http.delete(url1 +id)
  }
  updateWorkFromHomeRequest(obj){
    let url1 = this.baseurl +'/WFHrequestEdit/';
    return this.http.put(url1 ,obj)
  }
  getWorkFromHomeRequestById(id){
    return this.http.get(this.baseurl +'/requestWFHId/'+ id)
  }
  postExtraWorkRequest(workhome){
    let url = this.baseurl + '/extraworking';
    var body = JSON.stringify(workhome);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  updateExtraWorkRequest(obj){
    let url1 = this.baseurl +'/extraworkrequest/';
    return this.http.put(url1 ,obj)
  }
  deleteExtraWorkRequest(id){
    let url1 = this.baseurl +'/deleteextraworkingrequest/';
    return this.http.delete(url1 +id)
  }
  getExtraWorkRequestById(id){
    return this.http.get(this.baseurl +'/getExtraWorkingRequest/'+ id)
  }
}