import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { commaster } from'../model/companymastermodel'
import { lateearlyTypedata } from '../models/lateearlyModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';
import { CoffReq} from '../models/coffReqModel'

@Injectable()
export class coffService {
  baseurl = MainURL.HostUrl
  requesLateEarlyId: any;
  constructor(private http : Http) { }

  postCoffRequest(coff : CoffReq){
    let url = this.baseurl + '/coffrequest';
    var body = JSON.stringify(coff);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getCoffRequestList(personal){
    let url = this.baseurl + '/coffapproverequest';
    var body = JSON.stringify(personal);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  deleteCoffRequest(id){
    let url1 = this.baseurl +'/coffrequest/';
    return this.http.delete(url1 +id)
  }
  updateCoffRequest(obj){
    let url1 = this.baseurl +'/updatecoffrequest';
    return this.http.put(url1 ,obj)
  }
  getCoffById(id){
    return this.http.get(this.baseurl +'/coffrequest/'+ id)
  }

  postFile(fileToUpload: File) {
    let url = this.baseurl + '/uploadFile';
    
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    // formData.append('ImageCaption', caption);
    
    return this.http.post(endpoint, formData).map(x => x);;
    }
}