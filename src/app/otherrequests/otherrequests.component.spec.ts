import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherrequestsComponent } from './otherrequests.component';

describe('OtherrequestsComponent', () => {
  let component: OtherrequestsComponent;
  let fixture: ComponentFixture<OtherrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
