import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { OtherrequestsComponent } from 'src/app/otherrequests/otherrequests.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: OtherrequestsComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'otherrequests',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'otherrequests',
                    component: OtherrequestsComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);