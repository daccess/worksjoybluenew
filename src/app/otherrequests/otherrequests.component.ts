import { Component, OnInit } from '@angular/core';

import { personalrequestdata } from './shared/models/personalRequestModel';
import { lateearlyTypedata } from './shared/models/lateearlyModel';
import { onDutydata } from './shared/models/onDutyModel';
import { missPunchdata } from './shared/models/missPunchModel';
import { workfromhomedata } from './shared/models/workfromhomemodel';
import { PersonalRequetsService } from './shared/services/personalRequestService';
import { from } from 'rxjs'; 
import { OndutyService } from './shared/services/ondutyservice';
import { MisspunchService } from './shared/services/misspunchservice';
import { LateEarlyService } from './shared/services/lateearlyservice';
import { WorkfromService } from './shared/services/workfromhomeservice';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { extraWork } from './shared/models/extraWork';
import { MainURL } from '../shared/configurl';
import { Http } from '@angular/http';
import { CoffReq } from './shared/models/coffReqModel'
import { coffService } from './shared/services/CoffService';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { NgxSpinnerService } from 'ngx-spinner';
import { leaveRequestService } from '../leaverequest/services/leaverequestservice';
import { NewRegularisationModule } from '../new-regularisation/newregularisation.module';
import { CalendarService } from '../shared/services/calendar.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otherrequests',
  templateUrl: './otherrequests.component.html',
  styleUrls: ['./otherrequests.component.css'],
  providers: [PersonalRequetsService, OndutyService, MisspunchService, LateEarlyService, WorkfromService, leaveRequestService]
})
export class OtherrequestsComponent implements OnInit {
  empPerId: any;
  empOfficialId: any;
  compId: any;
  loggedUser: any;

  leavetype: string = "";
  public dateTime: Date;
  // permissionRequest: any = { startdate: "", time: "", enddate: "", endtime: "", anyother: "" };
  // regularisationType: any = { seldate: "", anyother: "" };
  missPunch: any = { enddate: "", anyother: "" };
  onDuty: any = { startdate: "", time: "", enddate: "", endtime: "", anyother: "" }

  personalReqModel: personalrequestdata;
  misspunchmodel: missPunchdata;
  ondutymodel: onDutydata;
  lateearlymodel: lateearlyTypedata;
  workfrommodel: workfromhomedata;
  extraWorkModel: extraWork;
  coffModel: CoffReq;
  show: string = 'list';
  lateEarlyList = [];
  earlyLateList = [];
  tempList = [];
  missPunchList = [];
  todaysDate: any;
  todaysTime: any;
  todaysTimeEnd: any;
  requestType: string = "";
  RequestObj: any = {}
  PermissionRequestList: any = [];
  LateEarlyRequestList: any = [];
  missPunchRequestList = [];
  OnDutyRequestList = [];
  workFromHomeRequestList = [];
  CoffListt = [];
  thisdate: Date;
  tempModel: any = {};
  isPermissionEdit: boolean = false;
  isLateEarlyEdit: boolean = false;
  isMissPunchEdit: boolean = false;
  isOnDutyEdit: boolean = false;

  isWorkFromHomeEdit: boolean = false;
  isExtraWorkEdit: boolean = false;
  isCoffEdit: boolean = false;
  Shiftmastergroupdata: any;
  shiftData: any;
  ExtraWorkRequestList: any[];
  detdateData: any;
  FullDaydateData: any;
  getfullDayDate: any;
  HalfDaydateData: any;
  getHlafDayDate: any;
  CoffList: any = [];
  statusType: string = "Pending";
  currentdate: Date;
  slectecdoption: any;
  slectecdoptionExtraWork: any;
  selectUndefinedOptionValue: any;

  themeColor: string = "nav-pills-blue";
  dltPermission: any;
  dltlatecoming: any;
  dltrequesMissPunchId: any;
  dltmodelFlag: boolean;
  dltmodelFlagLate: boolean;
  dltmodelFlagmiss: boolean;
  dltmodelFlagonDuty: boolean;
  dltrequestOnDutyId: any;
  dltmodelFlagwfm: boolean;
  dltrequestWFHId: any;
  dltmodelFlagewr: boolean;
  dltrequestEWRId: any;
  dltmodelFlagcoff: boolean;
  dltrequestCoffId: any;
  attenDate: any;
  empOfficialIdL: number;
  minDate: any;

  slectecdoptionResonExtraWork: any;
  slectecdoptionResonMisspunch: any;
  requestedDate: any;
  prreqdate: any;
  showEdit: any;

  constructor(private leaverequestService: leaveRequestService,
    public personalservice: PersonalRequetsService,
    public ondutyserve: OndutyService,
    public misspunchserve: MisspunchService,
    public lateservice: LateEarlyService,
    public workservice: WorkfromService,
    public CoffServic: coffService,
    public toastr: ToastrService,
    private http: HttpClient,
    public Spinner: NgxSpinnerService,
    private router:Router,
    private calendarService : CalendarService) {

    this.currentdate = new Date();
    this.minDate = new Date(Date.now());

     this.leavetype = "";
    // alert(this.leavetype)
    this.tempModel.thisdate = new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1;
    // this.tempModel.thisdate = 1 +"-"+(new Date().getMonth())+"-"+new Date().getFullYear();
    this.personalReqModel = new personalrequestdata();
    this.misspunchmodel = new missPunchdata();
    this.ondutymodel = new onDutydata();
    this.lateearlymodel = new lateearlyTypedata();
    this.workfrommodel = new workfromhomedata();
    this.extraWorkModel = new extraWork();
    this.coffModel = new CoffReq();
    this.lateearlymodel.requestType = 'late_coming';
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.prreqdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
 console.log("permissionrequestdatedatainonit",     this.prreqdate );

 
    this.empOfficialId = this.loggedUser.empOfficialId
    if(!this.loggedUser.firstLevelReportingManager){
      this.loggedUser.firstLevelReportingManager = 0;
    }
    if(!this.loggedUser.secondLevelReportingManager){
      this.loggedUser.secondLevelReportingManager = 0;
    }
    if(!this.loggedUser.thirdLevelReportingManager){
      this.loggedUser.thirdLevelReportingManager = 0;
    }
    if(!this.loggedUser.hrEmpId){
      this.loggedUser.hrEmpId =0;
    }
    this.empPerId = this.loggedUser.empPerId;
    this.personalReqModel.empPerId = this.empPerId;
    this.ondutymodel.empPerId = this.empPerId;
    this.lateearlymodel.empPerId = this.empPerId;
    this.workfrommodel.empPerId = this.empPerId;
    this.extraWorkModel.empPerId = this.empPerId;
    this.misspunchmodel.empPerId = this.empPerId;
    this.misspunchmodel.dateAndTime1 = '';
    this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();

    this.Holidays();
    this.getAttendanceOfMonth(new Date().getMonth() + 1, new Date().getFullYear())

    if (new Date().getMinutes() < 10) {
      if (new Date().getHours() < 10) {
        this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
        this.todaysTimeEnd = "0" + new Date().getHours() + ":0" + (new Date().getMinutes() + 1);
      }
      else {
        this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
        this.todaysTimeEnd = new Date().getHours() + ":0" + (new Date().getMinutes() + 1);
      }

    }
    else {
      if (new Date().getHours() < 10) {
        this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
        this.todaysTimeEnd = "0" + new Date().getHours() + ":" + (new Date().getMinutes() + 1);
      }
      else {
        this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
        this.todaysTimeEnd = new Date().getHours() + ":" + (new Date().getMinutes() + 1);
      }

    }
    this.RequestObj = {
      empPerId: this.empPerId,
      fromDate: new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1,
      status: this.statusType
    };

    this.lateearlymodel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.lateearlymodel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.lateEarlyHrApproval) {
      this.lateearlymodel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.lateearlymodel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }


    this.personalReqModel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.personalReqModel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.prHrApproval) {
      this.personalReqModel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.personalReqModel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }


    this.misspunchmodel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.misspunchmodel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.missPunchHrApproval) {
      this.misspunchmodel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.misspunchmodel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }

    this.ondutymodel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.ondutymodel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.onDutyHrApproval) {
      this.ondutymodel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.ondutymodel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }

    this.workfrommodel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.workfrommodel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.wfhHrApproval) {
      this.workfrommodel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.workfrommodel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }


    this.extraWorkModel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.extraWorkModel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.extraWorkHrApproval) {
      this.extraWorkModel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.extraWorkModel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }

    this.extraWorkModel.shiftMasterId = this.slectecdoptionExtraWork;
    //coff Model start
    this.coffModel.firstLevelReportingManager = this.loggedUser.firstLevelReportingManager;
    this.coffModel.secondLevelReportingManager = this.loggedUser.secondLevelReportingManager;
    if (this.loggedUser.coffHrApproval) {
      this.coffModel.thirdLevelReportingManager = this.loggedUser.hrEmpId;
    }
    else {
      this.coffModel.thirdLevelReportingManager = this.loggedUser.thirdLevelReportingManager;
    }

    //coff model end

    // this.getPermissionRequestList();
    this.personalReqModel.dateAndTime = this.todaysDate;
    this.personalReqModel.startTime = this.todaysTime;
    this.personalReqModel.endTime = this.todaysTime;

    this.lateearlymodel.dateAndTime1 = "";
    this.misspunchmodel.dateAndTime1 = this.todaysDate;
    this.misspunchmodel.startTime = this.todaysTime;
    this.misspunchmodel.endTime = "";
    this.misspunchmodel.requestType = 'In';

    this.ondutymodel.fromDate = this.todaysDate;
    this.ondutymodel.toDate = this.todaysDate;
    this.ondutymodel.startTime = this.todaysTime;
    this.ondutymodel.endTime = this.todaysTimeEnd;
    this.ondutymodel.requestType = 'Day';
    

    this.workfrommodel.forDate = this.todaysDate;
    this.workfrommodel.toDate = this.todaysDate;

    this.extraWorkModel.workingDate = this.todaysDate;

    this.coffModel.dateAndTime = this.todaysDate;
    this.getLateEarlyDateList();
    this.getEarlyLateDateList();
    this.getLateMissPunchDateList();
    this.shiftmastergroup();
    this.getCofflist()
    if (sessionStorage.getItem('from') == 'yearly') {
    
      this.leavetype = sessionStorage.getItem('requestedType');
       this.requestedDate = sessionStorage.getItem('requestedDate');
     
      
      // let requestedDate = sessionStorage.getItem('requestedDate');
      // this.model.todate = sessionStorage.getItem('requestedDate');
      sessionStorage.setItem('from', '');
      sessionStorage.setItem('requestedDate', new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate());
      // 
      if (this.leavetype == 'PR') {
        this.personalReqModel.dateAndTime =  this.prreqdate ;
        this.personalReqModel.startTime = this.todaysTime;
        this.personalReqModel.endTime = this.todaysTime;
      } else if (this.leavetype == 'MP') {
        this.misspunchmodel.dateAndTime1 = this.requestedDate;
        this.misspunchmodel.startTime = "";
        this.misspunchmodel.endTime = "";
        this.misspunchmodel.requestType = 'In';
      } else if (this.leavetype == 'WFH') {
        
        let date = new Date(this.requestedDate).getFullYear() + "-" + (new Date(this.requestedDate).getMonth() + 1) + "-" + new Date(this.requestedDate).getDate();
        this.workfrommodel.forDate = this.requestedDate
        console.log("fromdateinlateearly",this.workfrommodel.forDate);
        this.workfrommodel.toDate = this.requestedDate
      } else if (this.leavetype == 'LTE') {

      } else if (this.leavetype == 'OD F') {
        this.leavetype = "OD";
        this.ondutymodel.fromDate = this.requestedDate;
        this.ondutymodel.toDate = this.requestedDate;
        this.ondutymodel.startTime = this.todaysTime;
        this.ondutymodel.endTime = this.todaysTimeEnd;
        this.ondutymodel.requestType = 'Day';
      } else if (this.leavetype == 'OD H') {
        this.leavetype = "OD";
        this.ondutymodel.fromDate =this.requestedDate;
        this.ondutymodel.toDate =this.requestedDate;
        this.ondutymodel.startTime = this.todaysTime;
        this.ondutymodel.endTime = this.todaysTimeEnd;
        this.ondutymodel.requestType = 'Time';
      } else if (this.leavetype == 'CO') {

        this.coffModel.dateAndTime =this.requestedDate;

      } else if (this.leavetype == 'EWR') {
        this.leavetype = "EWR";
        this.extraWorkModel.workingDate = this.requestedDate;
      }
    }
    else {
      this.leavetype = "PR";
    }

    if (sessionStorage.getItem('from') == 'empd') {
      if (sessionStorage.getItem('requestedType') == 'LTE') {
        this.isLateEarlyEdit = true;
        this.leavetype = 'LTE';
        let data = sessionStorage.getItem('data');
        this.editLateEarlyRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
      else if (sessionStorage.getItem('requestedType') == 'WFH') {
        this.isWorkFromHomeEdit = true;
        this.leavetype = 'WFH';
        let data = sessionStorage.getItem('data');
        this.editWorkFromHomeRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
      else if (sessionStorage.getItem('requestedType') == 'MP') {
        this.isMissPunchEdit = true;
        this.leavetype = 'MP';
        let data = sessionStorage.getItem('data');
        this.editMissPunchRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
      else if (sessionStorage.getItem('requestedType') == 'OD') {
        this.isWorkFromHomeEdit = true;
        this.leavetype = 'OD';
        let data = sessionStorage.getItem('data');
        this.editOnDutyRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
      else if (sessionStorage.getItem('requestedType') == 'PR') {
        this.isWorkFromHomeEdit = true;
        this.leavetype = 'PR';
        let data = sessionStorage.getItem('data');
        this.editPermissionRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
      else if (sessionStorage.getItem('requestedType') == 'CO') {
        this.leavetype = 'CO';
        let data = sessionStorage.getItem('data');
        this.editCoffRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
      else if (sessionStorage.getItem('requestedType') == 'EWR') {
        this.leavetype = 'EWR';
        let data = sessionStorage.getItem('data');
        this.editExtraWorkRequest(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
    }


    if (sessionStorage.getItem('from') == 'empdd') {
      this.leavetype = 'CO';
      let data: any = JSON.parse(sessionStorage.getItem('data'));
      this.coffModel.coffCreditedId = data.coffCreditedId
      // this.editCoffRequest(JSON.parse(data));
      sessionStorage.setItem('from', '');
    }

    this.FullDaygetdate();
    this.HalfDaygetdate();

    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
  }


  ChangingValue(event) {
  }
  selectRequestType(e) {
    this.showEdit=e.target.value
    setTimeout(function () {
      $(function () {
        var table = $('#CompTablePr').DataTable();

      });
    }, 100);
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableLt').DataTable();

      });
    }, 100);
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableMp').DataTable();

      });
    }, 100);
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableOd').DataTable();

      });
    }, 100);
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableWfh').DataTable();

      });
    }, 100);
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableEwr').DataTable();

      });
    }, 100);
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableCo').DataTable();

      });
    }, 100);
  }
  baseurl = MainURL.HostUrl
  shiftmastergroup() {
    
  let url = this.baseurl + '/ShiftMaster/ByActive/';
  this.http.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
    this.shiftData = data;
    this.Shiftmastergroupdata= this.shiftData.result;
    console.log("shiftdata", this.Shiftmastergroupdata);
    },(err: HttpErrorResponse) => {
    });
  }
  length: number;
  ngOnInit() {
    
    this.length = parseInt(sessionStorage.getItem('length'));
    this.getPermissionRequestList();
    this.getLateEarlyRequestList();
    this.getMissPunchRequestList();
    this.getOnDutyRequestList();
    this.getWorkFromHomeRequestList();
    this.getExtraWorkRequestList();
    if (sessionStorage.getItem('from') == 'layout') {
      // if(parseInt(sessionStorage.getItem('length')) > 0){
      document.getElementById('pills-extra-tab').click();
      // }
    }
    this.coffModel.coffType = "Full Day"

    
    this.coffModel.halfDayType = 'First Half'


  }
  selectStartDate(e) {
    this.workfrommodel.forDate = e;

    if (this.workfrommodel.forDate.getTime() > this.workfrommodel.toDate.getTime()) {
      this.workfrommodel.toDate = e;
    }
  }
  selectEndDate(e) {
    this.workfrommodel.toDate = e;
  }
  selectODStartDate(e) {
    this.ondutymodel.fromDate = e;
    // this.ondutymodel.newFromDate= this.ondutymodel.fromDate = e;
    // this.ondutymodel.toDate = e;
    if (new Date(e).getTime() > new Date(this.ondutymodel.toDate).getTime()) {
      this.ondutymodel.toDate = e;
      // this.ondutymodel.newToDate= this.ondutymodel.fromDate = e;
    }
    if(this.ondutymodel.fromDate){
      let temp_selected_month=moment(this.ondutymodel.fromDate).month();
      let selected_month=temp_selected_month+1;
      //year
      let temp_selected_year=moment(this.ondutymodel.fromDate).year();
      this.getAttendanceOfMonth(selected_month,temp_selected_year);
    }
  }
  selectODEndDate(e) {
    this.ondutymodel.toDate = e;
    if (new Date(this.ondutymodel.fromDate).getTime() < new Date(e).getTime()) {

    }
  }
  selectPRStartDate(e) {
    this.personalReqModel.dateAndTime = e;
    console.log("permissionrequestdate&&&&&",  this.personalReqModel.dateAndTime);
  }
  lateEarly(val) {
  }

  flag: boolean = false;
  cancel() {
    this.flag = !this.flag;
    this.lateEarlyList = this.tempList;

    if (this.flag) {
      this.lateearlymodel.dateAndTime1 = this.todaysDate;
    }
    else {
      this.lateearlymodel.dateAndTime1 = this.lateEarlyList[0].attenDate;
    }
  }

  flagm: boolean = false;
  selectmisspDatecancel() {
    this.flagm = !this.flagm;
    if (this.flagm) {
      this.misspunchmodel.dateAndTime1 = this.todaysDate;
    }
    else {
      this.misspunchmodel.dateAndTime1 = '';
    }

  }

  selectmissDate(date) {
    this.misspunchmodel.dateAndTime1 = date;
    // alert(this.misspunchmodel.dateAndTime1)
  }

  getLateEarlyDateList() {
    this.lateservice.getLateEarlyDateList(this.empPerId).subscribe(data => {
      this.lateEarlyList = data.json().result;  
      if (this.lateEarlyList.length == 0) {
        this.flag = true;
      }
      this.tempList = this.lateEarlyList;
      this.lateearlymodel.dateAndTime1 = this.lateEarlyList[0].attenDate;
    }, err => {

    })
  }
  getEarlyLateDateList() {
    this.lateservice.getEarlyLateDateList(this.loggedUser.empPerId).subscribe(data => {
      this.earlyLateList = data.json().result;
      this.lateearlymodel.dateAndTime1 = this.earlyLateList[0].attenDate;
    }, err => {

    })
  }

  getLateMissPunchDateList() {
    this.misspunchserve.getLateMissPunchList(this.empOfficialId).subscribe(data => {
      this.missPunchList = data.json().result;
      let date = this.misspunchmodel.dateAndTime1;
      this.misspunchmodel.dateAndTime1 = this.todaysDate;

      this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
      let flag : boolean = false;
      for (let i = 0; i < this.missPunchList.length; i++) {
        if(new Date(this.missPunchList[i].attenDate).getTime() == new Date(this.misspunchmodel.dateAndTime1).getTime()){
          flag = true;
        }
      }
      setTimeout(() => {
        if(flag){
        
          if((new Date(date).getMonth()+1) < 10){
            this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-0"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
            if(new Date(date).getDate() < 10){
              this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-0"+(new Date(date).getMonth()+1)+"-0"+new Date(date).getDate();
            }
          }
          else{
            this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
          }
          
      }
      else{
        this.flagm = true;
          if((new Date(date).getMonth()+1) < 10){
            this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-0"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
            if(new Date(date).getDate() < 10){
              this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-0"+(new Date(date).getMonth()+1)+"-0"+new Date(date).getDate();
            }
          }
          else{
            this.misspunchmodel.dateAndTime1 = new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
          }
          
      }
      }, 200);
    }, err => {

    })
  }
  selectedMissPunchDate: number = 0;
  inTimeFlag: boolean = true;
  outTimeFlag: boolean = true;
  missPunchSelected(e) {
    this.selectedMissPunchDate = new Date(e).getTime();
    for (let i = 0; i < this.missPunchList.length; i++) {
      if (new Date(this.missPunchList[i].attenDate).getTime() == new Date(e).getTime()) {
        if (this.missPunchList[i].checkIn) {
          this.inTimeFlag = true;
          if (new Date(this.missPunchList[i].checkIn).getMinutes() < 10) {
            if (new Date(this.missPunchList[i].checkIn).getHours() < 10) {
              this.misspunchmodel.startTime = "0" + new Date(this.missPunchList[i].checkIn).getHours() + ":0" + (new Date(this.missPunchList[i].checkIn).getMinutes());
            }
            else {
              this.misspunchmodel.endTime = new Date(this.missPunchList[i].checkIn).getHours() + ":0" + (new Date(this.missPunchList[i].checkIn).getMinutes());
            }

          }
          else {
            if (new Date(this.missPunchList[i].checkIn).getHours() < 10) {
              this.misspunchmodel.startTime = "0" + new Date(this.missPunchList[i].checkIn).getHours() + ":" + (new Date(this.missPunchList[i].checkIn).getMinutes());
            }
            else {
              this.misspunchmodel.endTime = new Date(this.missPunchList[i].checkIn).getHours() + ":" + (new Date(this.missPunchList[i].checkIn).getMinutes());
            }
          }

        }
        else {
          this.misspunchmodel.startTime = '';
          this.inTimeFlag = false;
        }
        if (this.missPunchList[i].checkOut) {
          this.outTimeFlag = true;
          //  this.misspunchmodel.outTime = '';
          if (new Date(this.missPunchList[i].checkOut).getMinutes() < 10) {
            if (new Date(this.missPunchList[i].checkOut).getHours() < 10) {
              this.misspunchmodel.endTime = "0" + new Date(this.missPunchList[i].checkOut).getHours() + ":0" + (new Date(this.missPunchList[i].checkOut).getMinutes());
            }
            else {
              this.misspunchmodel.endTime = new Date(this.missPunchList[i].checkOut).getHours() + ":0" + (new Date(this.missPunchList[i].checkOut).getMinutes());
            }

          }
          else {
            if (new Date(this.missPunchList[i].checkOut).getHours() < 10) {
              this.misspunchmodel.endTime = "0" + new Date(this.missPunchList[i].checkOut).getHours() + ":" + (new Date(this.missPunchList[i].checkOut).getMinutes());
            }
            else {
              this.misspunchmodel.endTime = new Date(this.missPunchList[i].checkOut).getHours() + ":" + (new Date(this.missPunchList[i].checkOut).getMinutes());
            }

          }
        }
        else {
          this.misspunchmodel.endTime = '';
          this.outTimeFlag = false;
        }
      }

    }
  }


  missPunchTypeChanged(event){
    if(event == "In"){
      this.misspunchmodel.startTime = "";
        this.misspunchmodel.endTime = "";
    }
    else if(event == "Out"){
      this.misspunchmodel.startTime = "";
      this.misspunchmodel.endTime = "";
    }
    else{
      this.misspunchmodel.startTime = "";
      this.misspunchmodel.endTime = "";
    }
  }

  prStartTimeChanged(event) {

  }
  onSubmit(f) {
    

    this.personalReqModel.empTypeId = this.loggedUser.empTypeId;
    this.personalReqModel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.personalReqModel.compId =  this.loggedUser.compId;
    this.personalReqModel.dateAndTime=this.prreqdate;
    console.log("permissionrequestdt",this.personalReqModel.dateAndTime);
    if (this.isPermissionEdit) {
      if (this.personalReqModel.startTime == this.personalReqModel.endTime) {
        this.toastr.error('Start-time and End-time can not be identical.')
      }
      else{
        this.Spinner.show();
        this.personalservice.updatePermissionRequest(this.personalReqModel).subscribe(data => {
  
          this.Spinner.hide();
          this.isPermissionEdit = false;
          f.reset();
          f.submitted = false;
          this.resetPermission();
          this.Spinner.hide()
          this.toastr.success('Permission request updated successfully!', 'Permission Request');
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
        }, err => {
          this.Spinner.hide()
          if (err.json.result) {
            this.toastr.error(err.json().result.message);
          }
          else{
            this.Spinner.hide()
            this.toastr.error("Failed To update permission request");
          }
        })
      }

    } else {
      if (this.personalReqModel.startTime == this.personalReqModel.endTime) {
        this.toastr.error('Start-time and End-time can not be identical.')
      }
      else {
        this.Spinner.show();
        
        this.personalReqModel.dateAndTime= this.prreqdate;
        this.personalservice.postPersonalRequest(this.personalReqModel).subscribe(data => {
            this.Spinner.hide();
            f.reset();
            f.submitted = false;
            this.resetPermission();
            this.toastr.success('Permission request applied successfully!', 'Permission Request');
            this.getPermissionRequestList();
            this.getLateEarlyRequestList();
            this.getMissPunchRequestList();
            this.getOnDutyRequestList();
            this.getWorkFromHomeRequestList();

          }, err => {
            this.Spinner.hide();
            if(err.json().result){
              this.toastr.error(err.json().result.message);
            }
            else{
              this.toastr.error("Failed to apply permission request");
            }
          })
      }
    }
  }

  resetPermission() {
    this.isPermissionEdit = false;
    setTimeout(() => {
      if (new Date().getMinutes() < 10) {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
        }

      }
      else {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
        }

      }

      this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.personalReqModel.startTime = this.todaysTime;
      this.personalReqModel.dateAndTime = this.todaysDate;
      this.personalReqModel.endTime = this.todaysTime;

    }, 100);


  }
  onSubmit1(f) {
    this.lateearlymodel.empTypeId = this.loggedUser.empTypeId;
    this.lateearlymodel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.lateearlymodel.compId =  this.loggedUser.compId
    if (this.isLateEarlyEdit) {
      this.Spinner.show();

      this.lateservice.updateLateEarlyRequest(this.lateearlymodel).subscribe(data => {
        this.Spinner.hide();
        this.isLateEarlyEdit = false;
        f.reset();
        f.submitted = false;
        this.resetLateEarly();
        if(this.lateearlymodel.requestType == "late_coming"){
          this.Spinner.hide();
          this.toastr.success('Late Coming Request updated successfully');
        }
        else{
          this.Spinner.hide();
          this.toastr.success('Early Going Request updated successfully');
        }
        
        this.getLateEarlyDateList();
        this.getEarlyLateDateList();
        this.getPermissionRequestList();
        this.getLateEarlyRequestList();
        this.getMissPunchRequestList();
        this.getOnDutyRequestList();
        this.getWorkFromHomeRequestList();
      }, err => {
        this.Spinner.hide();
        if(err.json().result){
          this.toastr.error(err.json().result.message);
        }
        else{
          if(this.lateearlymodel.requestType == "late_coming"){
            this.Spinner.hide();
            this.toastr.success('Failed to update Late Coming request');
          }
          else{
            this.Spinner.hide();
            this.toastr.success('Failed to update Early Going request');
          }
        }
        
      })
    }
    else {
      if (this.lateearlymodel.requestType == 'late_coming') {
        for (let i = 0; i < this.lateEarlyList.length; i++) {

          // let element = this.lateEarlyList[i].
          if (new Date(this.lateEarlyList[i].attenDate).getTime() == new Date(this.lateearlymodel.dateAndTime1).getTime())  {
            this.lateearlymodel.attendanceId = this.lateEarlyList[i].attendanceId;
          }
        }
      }
      else if (this.lateearlymodel.requestType == 'early_going') {
        for (let i = 0; i < this.earlyLateList.length; i++) {

          // let element = this.earlyLateList[i].
          if (new Date(this.earlyLateList[i].attenDate).getTime() == new Date(this.lateearlymodel.dateAndTime1).getTime()) {
            this.lateearlymodel.attendanceId = this.earlyLateList[i].attendanceId;
          }
        }
      }
      this.Spinner.show()
      // this.lateearlymodel.requesLateEarlyId =item.requesLateEarlyId;
      console.log("modaldata9s",this.lateearlymodel);
      this.lateservice.postlateearlyRequest(this.lateearlymodel).subscribe(data => {
     
          this.Spinner.hide();
          console.log("modaldata9s",this.lateearlymodel);
          this.getLateEarlyDateList();
          this.getEarlyLateDateList();
          if(this.lateearlymodel.requestType == "late_coming"){
            this.Spinner.hide();
            this.toastr.success('Late Coming request applied successfully');
          }
          else{
            this.Spinner.hide();
            this.toastr.success('Early Going request applied successfully');
          }
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
          f.reset();
          f.submitted = false;
          this.resetLateEarly();
        }, err => {
          this.Spinner.hide()
          if(err.json().result){
            this.toastr.error(err.json().result.message);
          }
          else{
            if(this.lateearlymodel.requestType == "late_coming"){
              this.Spinner.hide();
              this.toastr.error('Failed to apply Late Coming request');
            }
            else{
              this.Spinner.hide();
              this.toastr.success('Failed to apply Early Going request');
            }
          }
        })
    }
  }
  resetLateEarly() {
    this.isLateEarlyEdit = false;
    setTimeout(() => {
      this.lateearlymodel.requestType = 'late_coming';
      this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      if (new Date().getMinutes() < 10) {
        this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
      }
      else {
        this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
      }
      if (!this.flag) {
        this.lateearlymodel.dateAndTime1 = this.lateEarlyList[0].attenDate;
      }
      this.lateearlymodel.dateAndTime1 = this.lateEarlyList[0].attenDate;
      this.flag = false;

    }, 100);

  }
  onSubmit2(f) {
    this.lateearlymodel.empTypeId = this.loggedUser.empTypeId;
    this.lateearlymodel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.misspunchmodel.compId =  this.loggedUser.compId
    let flag : boolean = false;
    if (this.isMissPunchEdit) {
      this.Spinner.show();
      this.misspunchserve.updateMissPunchRequest(this.misspunchmodel).subscribe(data => {
        this.Spinner.hide()
        this.isMissPunchEdit = false;
        f.reset();
        f.submitted = false;
        this.toastr.success("Miss Punch request updated successfully");
        this.Spinner.hide();
        this.resetMissPunch();
        this.getPermissionRequestList();
        this.getLateEarlyRequestList();
        this.getMissPunchRequestList();
        this.getOnDutyRequestList();
        this.getWorkFromHomeRequestList();
      }, err => {
        this.Spinner.hide()
        if(err.json().result){
          this.toastr.error(err.json().result.message);
        }
        else{
          this.Spinner.hide();
          this.toastr.error("Failed to update Miss Punch request");
        }
      })


    }
    else {
      for (let i = 0; i < this.missPunchList.length; i++) {

        if (new Date(this.missPunchList[i].attenDate).getTime() == new Date(this.selectedMissPunchDate).getTime()) {
          this.misspunchmodel.attendanceId = this.missPunchList[i].attendanceId;
        }
      }
      this.Spinner.show()
      this.misspunchserve.postmisspunchRequest(this.misspunchmodel)
        .subscribe(data => {
          this.Spinner.hide();
          f.reset();
          f.submitted = false;
          this.resetMissPunch();
          this.Spinner.hide();
          this.toastr.success('Miss Punch request applied successfully');
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
          this.getLateMissPunchDateList();
        },
          err => {
            this.Spinner.hide();
            if(err.json().result){
              this.toastr.error(err.json().result.message);
            }
            else{
              this.toastr.error("Failed to apply Miss Punch request");
            }
          })
    }
  

  }
  resetMissPunch() {
    this.isMissPunchEdit = false;
    setTimeout(() => {
      this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.misspunchmodel.requestType = "In";
      if (new Date().getMinutes() < 10) {
        this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
      }
      else {
        this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
      }
      this.misspunchmodel.dateAndTime1 = "";
      // this.misspunchmodel.startTime = this.todaysTime;
      this.misspunchmodel.startTime = '';
      this.misspunchmodel.endTime = '';
      this.misspunchmodel.reson = "Select Reason For Misspunch";
      this.slectecdoptionResonMisspunch = "Select Reason For Misspunch";
      // this.misspunchmodel.requestType = 'In';
    }, 100);
    this.flagm = false;
  }
  onSubmit3(f) {
    
    // od req changes
    
    for(let i=0;i<this.attendanceData.length;i++){
      if(this.attendanceData[i].attenDatez || this.ondutymodel.fromDate){
        this.ondutymodel.statusOfDay=this.attendanceData[i].statusOfDay;
        let temp_empTypeId=JSON.parse(sessionStorage.getItem('loggedUser'));
        this.ondutymodel.empTypeId=temp_empTypeId.empTypeId;
        this.ondutymodel.empOfficialId=temp_empTypeId.empOfficialId;
       
      }
    }
    this.ondutymodel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.ondutymodel.compId =  this.loggedUser.compId
    
    if (this.isOnDutyEdit) {
      this.Spinner.show()
      if (this.ondutymodel.requestType == "Time") {
        this.ondutymodel.toDate = this.ondutymodel.fromDate;
      }
      // this.ondutymodel.newFromDate=this.ondutymodel.fromDate;
      // this.ondutymodel.newToDate=this.ondutymodel.toDate;

      this.ondutyserve.updateOnDutyRequest(this.ondutymodel).subscribe(data => {
        this.Spinner.hide()
        this.isOnDutyEdit = false;
        f.reset();
        f.submitted = false;
        this.resetOnDuty();
        this.Spinner.hide();
        this.toastr.success('On Duty request updated successfully');
        this.getPermissionRequestList();
        this.getLateEarlyRequestList();
        this.getMissPunchRequestList();
        this.getOnDutyRequestList();
        this.getWorkFromHomeRequestList();
      }, err => {
        this.Spinner.hide();
        if(err.json().result){
          this.toastr.error(err.json().result.message);
        }
        else{
          this.Spinner.hide();
          this.toastr.error("Failed to update On Duty request");
        }
      })
    } else {
      if (this.ondutymodel.requestType == "Time") {
        this.ondutymodel.toDate = this.ondutymodel.fromDate;
      }
      this.Spinner.show()
     
      
    
      this.ondutymodel.empTypeId=this.loggedUser.empTypeId;
      this.ondutyserve.postondutyRequest(this.ondutymodel).subscribe(data => {
       
          this.Spinner.hide();
          f.reset();
          f.submitted = false;
          this.resetOnDuty();
          this.Spinner.hide();
          this.toastr.success('On Duty request appplied successfully');
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
        }, err => {
          this.Spinner.hide();
          if(err.json().result){
            this.toastr.error(err.json().result.message);
          }
          else{
            this.Spinner.hide();
            this.toastr.error("Failed to apply On Duty request");
          }
        })
    }

  }
  resetOnDuty() {
    this.isOnDutyEdit = false;
    setTimeout(() => {
      this.ondutymodel.requestType = 'Day';
      this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      if (new Date().getMinutes() < 10) {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":0" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
        }

      }
      else {
        if (new Date().getHours() < 10) {
          this.todaysTime = "0" + new Date().getHours() + ":" + (new Date().getMinutes());
        }
        else {
          this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
        }

      }
      this.ondutymodel.startTime = this.todaysTime;
      this.ondutymodel.endTime = this.todaysTimeEnd;
      this.ondutymodel.fromDate = this.todaysDate;
      this.ondutymodel.toDate = this.todaysDate;


    }, 100);

  }
  onDutyStartTimeChanged(e) {
  }
  onDutyEndTimeChanged(e) {
  }
  onSubmit4(f) {
    this.workfrommodel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.workfrommodel.compId =  this.loggedUser.compId
    if (this.isWorkFromHomeEdit) {
      this.Spinner.show();
      this.workservice.updateWorkFromHomeRequest(this.workfrommodel).subscribe(data => {
        this.Spinner.hide();
        this.isWorkFromHomeEdit = false;
        f.reset();
        f.submitted = false;
        this.resetWorkFromHome();
        this.Spinner.hide();
        this.toastr.success('Work From Home request updated successfully');
        this.getPermissionRequestList();
        this.getLateEarlyRequestList();
        this.getMissPunchRequestList();
        this.getOnDutyRequestList();
        this.getWorkFromHomeRequestList();
      }, err => {
        this.Spinner.hide()
        if(err.json().result){
          this.toastr.error(err.json().result.message);
        }
        else{
          this.Spinner.hide();
          this.toastr.error("Failed to update Work From Home request");
        }
      })
    } else {
      this.Spinner.show()
      this.workservice.postworkfromRequest(this.workfrommodel).subscribe(data => {
          this.Spinner.hide();
          f.reset();
          f.submitted = false;
          this.resetWorkFromHome();
          this.toastr.success('Work From Home request applied successfully');
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
        }, err => {
          this.Spinner.hide();
          if(err.json().result){
            this.toastr.error(err.json().result.message);
          }
          else{
            this.Spinner.hide();
            this.toastr.error("Failed to apply Work From Home request");
          }
        })
    }

  }

 
  onSubmitExtraWork(f) {
    this.extraWorkModel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.extraWorkModel.compId =  this.loggedUser.compId
    let shiftName = "";
    for (let i = 0; i < this.Shiftmastergroupdata.length; i++) {
      if(this.Shiftmastergroupdata[i].shiftMasterId == this.extraWorkModel.shiftMasterId){
          shiftName = this.Shiftmastergroupdata[i].shiftName;
      }
    }
    this.extraWorkModel.shiftName = shiftName;
    if (this.isExtraWorkEdit) {
      this.Spinner.show();
      this.workservice.updateExtraWorkRequest(this.extraWorkModel).subscribe(data => {
        this.Spinner.hide()
        this.isExtraWorkEdit = false;
        // this.resetWorkFromHome();
        this.Spinner.hide();
        this.toastr.success('Extra Work request updated successfully');
        f.reset();
        f.submitted = false;
        this.resetExtraWork();
        this.getCofflist();
        this.getPermissionRequestList();
        this.getLateEarlyRequestList();
        this.getMissPunchRequestList();
        this.getOnDutyRequestList();
        this.getWorkFromHomeRequestList();
        this.getExtraWorkRequestList();
      }, err => {
        this.Spinner.hide();
        if(err.json().result){
          this.toastr.error(err.json().result.message);
        }
        else{
          this.Spinner.hide();
          this.toastr.error("Failed to update Extra Work request");
        }
      })
    } else {
      this.Spinner.show();
      this.workservice.postExtraWorkRequest(this.extraWorkModel).subscribe(data => {
          this.Spinner.hide();
          f.reset();
          this.isExtraWorkEdit = false;
          f.submitted = false;
          this.resetExtraWork();
          this.toastr.success('Extra Work request applied successfully');
          this.getCofflist();
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
          this.getExtraWorkRequestList();

        }, err => {
          this.Spinner.hide()
          if (err.json().result) {
            this.toastr.error(err.json().result.message);
          }
          else {
            this.Spinner.hide();
            this.toastr.error("Failed to apply Extra Work request");
          }

        })
    }
  }
  resetWorkFromHome() {
    this.isExtraWorkEdit = false;
    setTimeout(() => {
      this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      if (new Date().getMinutes() < 10) {
        this.todaysTime = new Date().getHours() + ":0" + (new Date().getMinutes());
      }
      else {
        this.todaysTime = new Date().getHours() + ":" + (new Date().getMinutes());
      }
      this.workfrommodel.forDate = this.todaysDate;
      this.workfrommodel.toDate = this.todaysDate;


    }, 100);

  }

  CancelWorkfromhome(){
    this.router.navigate(['layout/attendancemenu/attendancemenu']);
  }

  resetCoff() {
    setTimeout(() => {

      this.coffModel.coffCreditedId = this.slectecdoption;
      this.coffModel.dateAndTime = this.todaysDate;

      this.coffModel.halfDayType = ''
      this.coffModel.coffType = "Full Day";

    }, 100);
  }
  resetExtraWork() {
    this.isExtraWorkEdit = false;
    setTimeout(() => {
      this.extraWorkModel.requestType = '';

      this.extraWorkModel.workingDate = this.todaysDate;
      // this.extraWorkModel.workingDate = new Date();
      this.extraWorkModel.shiftMasterId = this.slectecdoptionExtraWork;
      this.extraWorkModel.resone = "Select Reason For Extra Work";
      this.slectecdoptionResonExtraWork = "Select Reason For Extra Work";
    }, 100);

  }
  selectedLateEarlyDate: any;
  selectedLateEarlyDateFlag: boolean = false;
  selectDate(date) {
    this.selectedLateEarlyDate = new Date(date).getTime();
    if (this.selectedLateEarlyDate >= new Date().getTime()) {
      this.selectedLateEarlyDateFlag = false;
    }
    else {
      this.selectedLateEarlyDateFlag = true;
    }
    this.lateearlymodel.dateAndTime1 = date;
  }

  getPermissionRequestList() {
    this.PermissionRequestList = new Array();
    this.personalservice.getPermissionRequestList(this.RequestObj).subscribe(data => {
      this.PermissionRequestList = data.result;
      setTimeout(function () {
        $(function () {
          var table = $('#CompTablePr').DataTable();

        });
      }, 1000);

    }, err => {

    })
  }
  getExtraWorkRequestList() {
    this.ExtraWorkRequestList = new Array();
    this.personalservice.getExtraWorkRequestList(this.RequestObj).subscribe(data => {
      this.ExtraWorkRequestList = data.result;
      setTimeout(function () {
        //modified by priyanka k 
        $(function () {
          $('#CompTableEwr').dataTable();

        });
      }, 1000);
    }, err => {
    })
  }
  getLateEarlyRequestList() {
    this.LateEarlyRequestList = new Array();
    this.lateservice.getLateEarlyRequestList(this.RequestObj).subscribe(data => {
      this.LateEarlyRequestList = data.result;
      console.log("lateearlylist", this.LateEarlyRequestList );
      setTimeout(function () {
        $(function () {
          var table = $('#CompTableLt').DataTable();

        });
      }, 1000);
    }, err => {
    })
  }


  // getOnDutyRequestList = [];
  getMissPunchRequestList() {
    this.missPunchRequestList = new Array();
    setTimeout(function () {
      $(function () {
        var table = $('#CompTableMp').DataTable();

      });
    }, 1000);
    this.misspunchserve.getMissPunchRequestList(this.RequestObj).subscribe(data => {
      this.missPunchRequestList = data.result;
    }, err => {
    })
  }

  getOnDutyRequestList() {
    this.OnDutyRequestList = new Array();
    this.ondutyserve.getOnDutyRequestList(this.RequestObj).subscribe(data => {
      this.OnDutyRequestList = data.result;
      setTimeout(function () {
        $(function () {
          var table = $('#CompTableOd').DataTable();

        });
      }, 1000);

    }, err => {

    })
  }

  getWorkFromHomeRequestList() {
    this.workFromHomeRequestList = new Array();
    this.workservice.getWorkFromHomeRequestList(this.RequestObj).subscribe(data => {
      this.workFromHomeRequestList = data.result;
      setTimeout(function () {
        $(function () {
          var table = $('#CompTableWfh').DataTable();

        });
      }, 1000);

    }, err => {
    })
  }

  selectThisDate(event) {
    this.RequestObj = {
      empPerId: this.empPerId,
      fromDate: event,
      status: this.statusType
    };
    this.getPermissionRequestList();
    this.getLateEarlyRequestList();
    this.getMissPunchRequestList();
    this.getOnDutyRequestList();
    this.getWorkFromHomeRequestList();
    this.getCofflist();
    this.getExtraWorkRequestList();
  }
  statusTypeChanged(e) {
    this.RequestObj.status = this.statusType;

    this.getPermissionRequestList();
    this.getLateEarlyRequestList();
    this.getMissPunchRequestList();
    this.getOnDutyRequestList();
    this.getWorkFromHomeRequestList();
    this.getCofflist();
    this.getExtraWorkRequestList();
  }
  coffDateChanged(e) {
    this.coffModel.dateAndTime = e;
  }
  editPermissionRequest(item) {
    this.leavetype = "PR";
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }

    this.personalservice.getPermissionRequestById(item.requestPRId).subscribe(data => {
      this.personalReqModel.requestPRId = item.requestPRId;
      // this.personalReqModel.empPerId = this.empPerId;
      this.personalReqModel.dateAndTime = data.json().result.dateAndTime;
      console.log("dataprrequest",this.personalReqModel.dateAndTime )
      this.personalReqModel.endTime = data.json().result.endTime;
      this.personalReqModel.startTime = data.json().result.startTime;
      this.personalReqModel.reson = data.json().result.reson;
      this.isPermissionEdit = true;
    }, err => {

    })

  }
  // ==============================delete permission===============================
  dltPermissionmodelEvent() {
    this.Spinner.show()
    this.dltmodelFlag = true;
    this.dltObjpermission(this.dltPermission);
  }

  deletePermissionRequest(item) {
    this.dltmodelFlag = false;
    this.dltPermission = item.requestPRId;
    this.dltObjpermission(item.requestPRId)
  }

  dltObjpermission(requestPRId) {
    if (this.dltmodelFlag == true) {
      if (this.dltmodelFlag == true) {
        this.personalservice.DeletePermissionRequest(requestPRId).subscribe(data => {
          this.Spinner.hide();
          this.toastr.success('Permission request deleted successfully');
          this.getPermissionRequestList();

        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete Permission request');
        })
      }
    }
  }

  dltlatecomingmodelEvent() {
    this.Spinner.show()
    this.dltmodelFlagLate = true;
    this.dltObjlateComing(this.dltlatecoming,this.attenDate,this.empOfficialIdL);
  }

  deleteLateEarlyRequest(item) {
    this.dltmodelFlagLate = false;
    this.empOfficialIdL = parseInt(this.loggedUser.empOfficialId);
    this.dltlatecoming = item.requesLateEarlyId;
    this.attenDate = item.attenDate;
    this.dltObjlateComing(item.requesLateEarlyId,item.attenDate,this.empOfficialIdL)
  }

  dltObjlateComing(requesLateEarlyId,attenDate,empOfficialIdL) {
    if (this.dltmodelFlagLate == true) {
      if (this.dltmodelFlagLate == true) {
        this.lateservice.deleteLateEarlyRequest(requesLateEarlyId,attenDate,empOfficialIdL).subscribe(data => {
          this.Spinner.hide();
          this.getLateEarlyRequestList();
          this.toastr.success('Late/Early request deleted successfully');
        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete Late/Early request');
        });
      }
    }
  }

  editLateEarlyRequest(item) {
  
    this.leavetype = "LTE";
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }
    let editData: any;

    this.lateservice.getLateEarlyRequestById(item.requesLateEarlyId).subscribe(data => {
      editData = data.json().result;
      this.isLateEarlyEdit = true;
      console.log("flagdata",this.isLateEarlyEdit );
      // this.lateearlymodel.empPerId = this.empPerId;
      this.lateearlymodel.requesLateEarlyId = item.requesLateEarlyId;
      this.lateearlymodel.requestType = editData.requestType;
      this.lateearlymodel.dateAndTime1 = editData.dateAndTime1;
      console.log("dataeditinearly",this.lateearlymodel.dateAndTime1);
      this.lateearlymodel.reson = editData.reson;
    }, err => {

    })
  }


  editMissPunchRequest(item) {
    
    this.leavetype = "MP";
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }

    let editData: any;
    this.misspunchserve.getMissPunchRequestById(item.requesMissPunchId).subscribe(data => {
      editData = data.json().result;
      this.isMissPunchEdit = true;
      this.misspunchmodel.requesMissPunchId = editData.requesMissPunchId;
      this.misspunchmodel.dateAndTime1 = editData.dateAndTime1;
      this.misspunchmodel.requestType = editData.requestType;

      if(editData.startTime){
        var lastIndexST = editData.startTime.split(':');
        this.misspunchmodel.startTime = lastIndexST[0]+":"+lastIndexST[1];
      }
      
      if(editData.endTime){
        var lastIndexET = editData.endTime.split(':');
      this.misspunchmodel.endTime = lastIndexET[0]+":"+lastIndexET[1];
      }
      // this.ondutymodel.startTime = editData.startTime;
      
      this.flagm = true;
      this.misspunchmodel.reson = editData.reson;
    }, err => {

    })
  }

  dltrequesAttenDate : any;
  dltrequesEmpOfficialId : any;
  dltmisspunchmodelEvent() {
    this.Spinner.show()
    this.dltmodelFlagmiss = true;
    this.deleteMissPunchOBJ(this.dltrequesMissPunchId,this.dltrequesAttenDate,this.dltrequesEmpOfficialId);
  }

  deleteMissPunchRequest(item) {
    this.dltmodelFlagmiss = false;
    this.dltrequesMissPunchId = item.requesMissPunchId;
    this.dltrequesAttenDate = item.dateAndTime1;
    this.dltrequesEmpOfficialId = this.loggedUser.empOfficialId;
  }

  deleteMissPunchOBJ(requesMissPunchId,attenDate,empOfficialId) {
    if (this.dltmodelFlagmiss == true) {
      if (this.dltmodelFlagmiss == true) {
        this.misspunchserve.deleteMissPunchRequest(requesMissPunchId,attenDate,empOfficialId).subscribe(data => {
          this.Spinner.hide();
          this.getMissPunchRequestList();
          this.toastr.success('Miss Punch request deleted successfully');
        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete Miss Punch request');
        })
      }
    }
  }

  editOnDutyRequest(item) {
    this.leavetype = "OD";
    let editData: any;
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }

    this.ondutyserve.getOnDutyRequestById(item.requesODId).subscribe(data => {
      editData = data.json().result;
      this.isOnDutyEdit = true;
      this.ondutymodel.requesODId = item.requesODId;
      this.ondutymodel.requestType = editData.requestType;
      this.ondutymodel.fromDate = editData.fromDate;
      this.ondutymodel.toDate = editData.toDate;
      this.ondutymodel.oldFromDate=editData.fromDate;
      this.ondutymodel.oldToDate=editData.toDate;
      this.ondutymodel.reson = editData.reson;
      if(editData.startTime.split(':')){
        var lastIndexST = editData.startTime.split(':');
        this.ondutymodel.startTime = lastIndexST[0]+":"+lastIndexST[1];
      }
      if(editData.endTime.split(':')){
        var lastIndexET = editData.endTime.split(':');
        this.ondutymodel.endTime = lastIndexET[0]+":"+lastIndexET[1];
      }
    }, err => {

    })
  }
  dltondutymodelEvent() {
    this.Spinner.show()
    this.dltmodelFlagonDuty = true;
    this.deleteOnDutyOBJ(this.dltrequestOnDutyId);
  }

  deleteOnDutyRequest(item) {
    this.dltmodelFlagonDuty = false;
    this.dltrequestOnDutyId = item.requesODId;
    this.deleteOnDutyOBJ(item.requesODId);
  }

  deleteOnDutyOBJ(requesODId) {
    if (this.dltmodelFlagonDuty == true) {
      if (this.dltmodelFlagonDuty == true) {
        this.ondutyserve.deleteOnDutyRequest(requesODId).subscribe(data => {
          this.Spinner.hide();
          this.getOnDutyRequestList();
          this.toastr.success('On Duty request deleted successfully');
        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete On Duty request');
        });
      }
    }
  }



  editWorkFromHomeRequest(item) {
    this.leavetype = "WFH";
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }
    if (!item.requestWFHId) {
      item.requestWFHId = item.requestWFH;
    }
    let editData: any;

    this.workservice.getWorkFromHomeRequestById(item.requestWFHId).subscribe(data => {
      editData = data.json().result;
      this.isWorkFromHomeEdit = true;
      this.workfrommodel.requestWFHId = item.requestWFHId;
      this.workfrommodel.forDate = editData.forDate;
      this.workfrommodel.toDate = editData.toDate;
      this.workfrommodel.reson = editData.reson;
    }, err => {

    })
  }
  dltwfhmodelEvent() {
    this.Spinner.show()
    this.dltmodelFlagwfm = true;
    this.deleteWFHOBJ(this.dltrequestWFHId);
  }

  deleteWorkFromHomeRequest(item) {
    this.dltmodelFlagwfm = false;
    this.dltrequestWFHId = item.requestWFHId;
    this.deleteWFHOBJ(item.requestWFHId);
  }

  deleteWFHOBJ(requestWFHId) {
    if (this.dltmodelFlagwfm == true) {
      if (this.dltmodelFlagwfm == true) {
        this.workservice.deleteWorkFromHomeRequest(requestWFHId).subscribe(data => {
          this.Spinner.hide();
          this.getWorkFromHomeRequestList();
          this.toastr.success('Work From Home request deleted successfully');
        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete Work From Home request');
        });
      }
    }
  }


  editExtraWorkRequest(item) {
    this.leavetype = "EWR";
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }
    this.workservice.getExtraWorkRequestById(item.extraWorkingRequestID).subscribe(data => {
      let editData = data.json().result;
      this.isExtraWorkEdit = true;
      this.extraWorkModel.resone = editData.resone;
      this.extraWorkModel.extraWorkingRequestID = editData.extraWorkingRequestID;
      this.extraWorkModel.workingDate = editData.workingDate;
      this.extraWorkModel.shiftMasterId = editData.shiftMasterId;
    }, err => {

    })
  }

  dltEWRmodelEvent() {
    this.Spinner.show()
    this.dltmodelFlagewr = true;
    this.deleteEWROBJ(this.dltrequestEWRId);
  }

  deleteExtraWorkRequest(item) {
    this.dltmodelFlagewr = false;
    this.dltrequestEWRId = item.extraWorkingRequestID;
    this.deleteEWROBJ(item.extraWorkingRequestID);
  }

  deleteEWROBJ(extraWorkingRequestID) {
    if (this.dltmodelFlagewr == true) {
      if (this.dltmodelFlagewr == true) {

        this.workservice.deleteExtraWorkRequest(extraWorkingRequestID).subscribe(data => {
          this.Spinner.hide();
          this.getExtraWorkRequestList();
          this.toastr.success('Extra Work request deleted successfully');
        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete Extra Work request');
        });
      }
    }
  }



  editCoffRequest(item) {
    this.leavetype = "CO";
    if (sessionStorage.getItem('from') != 'empd') {
      document.getElementById('pills-home-tab').click();
    }
    let editData: any;
    this.CoffServic.getCoffById(item.requesCoffIdId).subscribe(data => {
      this.coffModel.requesCoffIdId = item.requesCoffIdId;
     
      // document.getElementById('pills-home-tab').click();
     
      
      editData = data.json().result;
      this.isCoffEdit = true;
      this.coffModel.requesCoffIdId = item.requesCoffIdId;
      this.coffModel.dateAndTime = editData.dateAndTime;
      this.coffModel.halfDayType = editData.halfDayType;
      this.coffModel.halfDayType = editData.halfDayType;
      this.coffModel.reson = editData.reson;
      this.coffModel.coffType = editData.coffType;
      this.coffModel.coffCreditedId = editData.coffCreditedId
    }, err => {

    })
   
   
  }

  dltCOffmodelEvent() {
    this.Spinner.show()
    this.dltmodelFlagcoff = true;
    this.deleteCoffOBJ(this.dltrequestCoffId);
  }

  deleteCoffRequest(item) {
    this.dltmodelFlagcoff = false;
    this.dltrequestCoffId = item.requesCoffIdId;
    this.deleteCoffOBJ(item.requesCoffIdId);
  }

  deleteCoffOBJ(requesCoffIdId) {
    if (this.dltmodelFlagcoff == true) {
      if (this.dltmodelFlagcoff == true) {
        this.CoffServic.deleteCoffRequest(requesCoffIdId).subscribe(data => {
          this.Spinner.hide();
          this.getCofflist();
          this.toastr.success('C-off request deleted successfully');
        }, err => {
          this.Spinner.hide();
          this.toastr.error('Failed to delete C-off request');
        });
      }
    }
  }

  FullDaygetdate() {
    this.loggedUser.empPerId = this.empPerId
    let url = this.baseurl + '/coffregularisation/';
    this.http.get(url + this.empPerId).subscribe((data: any) => {
        this.FullDaydateData = data;
        this.getfullDayDate = this.FullDaydateData.result;
      },
      (err: HttpErrorResponse) => {
      });
  }


  HalfDaygetdate() {
    this.loggedUser.empPerId = this.empPerId
    let url = this.baseurl + '/halfcoffregularisation/';
    this.http.get(url + this.empPerId).subscribe((data: any) => {
        this.HalfDaydateData = data;
        this.getHlafDayDate = this.HalfDaydateData.result;
      },
      (err: HttpErrorResponse) => {
      });
  }
  onSubmitCoff() {
    this.coffModel.subEmpTypeId = this.loggedUser.subEmpTypeId;
    this.coffModel.compId =  this.loggedUser.compId
    if (this.isCoffEdit) {
      this.Spinner.show()
      this.coffModel.empPerId = this.empPerId;
      this.CoffServic.updateCoffRequest(this.coffModel).subscribe(data => {
        this.Spinner.hide();
        this.isCoffEdit = false;
        this.resetCoff();
        this.toastr.success('On Duty request updated successfully');
        this.getCofflist();
        this.getPermissionRequestList();
        this.getLateEarlyRequestList();
        this.getMissPunchRequestList();
        this.getOnDutyRequestList();
        this.getWorkFromHomeRequestList();
      }, err => {
        this.Spinner.hide();
        this.toastr.error('Failed to update On Duty request');
      })
    }
    else {
      this.coffModel.empPerId = this.loggedUser.empPerId;
      this.Spinner.show()
      this.CoffServic.postCoffRequest(this.coffModel).subscribe(data => {
          this.Spinner.hide();
          this.resetCoff();
          this.toastr.success('C-Off request applied successfully');
          this.getCofflist();
          this.getPermissionRequestList();
          this.getLateEarlyRequestList();
          this.getMissPunchRequestList();
          this.getOnDutyRequestList();
          this.getWorkFromHomeRequestList();
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Failed to apply C-Off request');
          });
    }
  }

  getCofflist() {
    this.CoffList = new Array();
    this.CoffServic.getCoffRequestList(this.RequestObj).subscribe(data => {
      this.CoffList = data.result;
      setTimeout(function () {
        $(function () {
          var table = $('#CompTableCo').DataTable();

        });
      }, 1000);
    }, err => {

    })
  }

  //=======================Work Flow History Starts ===============================
  workFlowList: any = [];
  prWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getPrWorkflowList(item.requestPRId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }

    }, err => {
      this.workFlowList = [];
    })
  }

  LateEarlyWorkFlow(item) {
  
    this.workFlowList = [];
    this.personalservice.getLateEarlyWorkflowList(item.requesLateEarlyId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  missPunchWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getMissPunchWorkflowList(item.requesMissPunchId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  odWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getOdWorkflowList(item.requesODId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  workFromHomeWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getWorkFromHomeWorkflowList(item.requestWFHId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  extraWorkWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getExtraWorkWorkflowList(item.extraWorkingRequestID).subscribe(data => {
      let workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  coffWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getCoffWorkflowList(item.requesCoffIdId).subscribe(data => {
      let workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  //=======Approve , Reject , Cancel Work Flow============

  prApproveWorkFlow(item) {
    this.workFlowList = [];
    let requestPRId;
    if(item.finalStatus =="Rejected" || item.finalStatus =="Approved"){
      requestPRId = item.requestApprovePRId;
    }
    else{
      requestPRId = item.requestPRId;
    }

    this.personalservice.getPrApproveWorkflowList(requestPRId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  LateEarlyApproveWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getLateEarlyApproveWorkflowList(item.requesLateEarlyId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  missPunchApproveWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getMissPunchApproveWorkflowList(item.requesMissPunchId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  odApproveWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getOdApproveWorkflowList(item.requesODId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
          console.log("odworkflow",  this.workFlowList);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  workFromHomeApproveWorkFlow(item) {
    this.workFlowList = [];
    let requestWFHId;
    if(item.finalStatus =="Rejected" || item.finalStatus =="Approved"){
      requestWFHId = item.requestApproveWFHId;
    }
    else{
      requestWFHId = item.requestWFHId;
    }
    this.personalservice.getWorkFromHomeApproveWorkflowList(requestWFHId).subscribe(data => {
      let  workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  extraWorkApproveWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getExtraWorkApproveWorkflowList(item.extraWorkingRequestID).subscribe(data => {
      let workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }

  coffApproveWorkFlow(item) {
    this.workFlowList = [];
    this.personalservice.getCoffApproveWorkflowList(item.requesCoffIdId).subscribe(data => {
      let workFlowListData = new Array();
      workFlowListData = data.json().result;
      for (let i = 0; i < workFlowListData.length; i++) {
        if(workFlowListData[i].empId){
          this.workFlowList.push(workFlowListData[i]);
        }
      }
    }, err => {
      this.workFlowList = [];
    })
  }
  //=======Approve , Reject , Cancel Work Flow============
  //=======================Work Flow History Ends ===============================

  ectraworkDate(event) {
    this.extraWorkModel.workingDate = event;
  }

  CoffFordate(event) {
    this.coffModel.dateAndTime = event;
  }


  // =============Calendar Starts ====================
  attendanceData = [];
  daysInThisMonth: any = [];
  daysInLastMonth: any = [];
  daysInNextMonth: any = [];
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  currentMonth: any;
  currentYear: any;
  date: any = new Date();
  currentDate: any;
  HolidayData = [];
  getAttendanceOfMonth(month, year) {
    let obj = {
      "empOfficialId": this.empOfficialId,
      "month": month,
      "year": year
    }

    this.leaverequestService.monthlyAttendance(obj).subscribe(data => {
      this.attendanceData = data.result;

      this.getDaysOfMonth();
    }, (err => {
      this.getDaysOfMonth();
    })
    )
  }

  //====================holidays===========================
  Holidays() {

    let url = this.baseurl + '/Holiday?empOfficialId='+this.loggedUser.empOfficialId+'&year='+new Date().getFullYear();
    this.http.get(url + this.loggedUser.empOfficialId).subscribe(
      (data: any) => {
        this.HolidayData = data.result;
      },
      (err: HttpErrorResponse) => {
      });

  }
  //====================holidays===========================
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    let ServiceAttendanceData = this.calendarService.getCalendarAttendance(this.date , this.attendanceData , this.HolidayData , this.currentYear);
    this.daysInThisMonth = ServiceAttendanceData.daysInThisMonth;
    this.daysInLastMonth = ServiceAttendanceData.daysInLastMonth;
    this.daysInNextMonth = ServiceAttendanceData.daysInNextMonth;
  }

  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ":" + rminutes;
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.attendanceData = []
    // this.getDaysOfMonth();
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.attendanceData = []
    // this.getDaysOfMonth();
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  selectDateCal(day) {
    this.currentDate = day;
  }
}
