import { routing } from './otherrequests.routing';
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { PersonalRequetsService } from './shared/services/personalRequestService';
import { LateEarlyService } from './shared/services/lateearlyservice';
import { MisspunchService } from './shared/services/misspunchservice';
import { OndutyService } from './shared/services/ondutyservice';
import { WorkfromService } from './shared/services/workfromhomeservice';

import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { OtherrequestsComponent } from './otherrequests.component'
import { UiSwitchModule } from 'ngx-ui-switch';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { coffService } from './shared/services/CoffService';
import { leaveRequestService } from '../supervisorleave-request/services/leaverequestservice';
import { MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [
        OtherrequestsComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
     DataTablesModule,
     OwlDateTimeModule, 
     OwlNativeDateTimeModule,
     NgbModule,

 

  MatDatepickerModule,        // <----- import(must)
  MatNativeDateModule,  
    ],
    providers: [leaveRequestService,
        PersonalRequetsService,
         LateEarlyService, 
         MisspunchService, 
         OndutyService,
         WorkfromService,
         coffService
         
    ]
  })
  export class OtherrequestsModule { 
      constructor(){

      }
  }
  