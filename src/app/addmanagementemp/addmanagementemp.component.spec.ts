import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmanagementempComponent } from './addmanagementemp.component';

describe('AddmanagementempComponent', () => {
  let component: AddmanagementempComponent;
  let fixture: ComponentFixture<AddmanagementempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmanagementempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmanagementempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
