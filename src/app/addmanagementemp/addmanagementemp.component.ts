import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { Http } from '@angular/http';
import { addmangmentEmployeeModel } from '../shared/model/contractorempModel';
import { coffService } from '../otherrequests/shared/services/CoffService';

@Component({
  selector: 'app-addmanagementemp',
  templateUrl: './addmanagementemp.component.html',
  styleUrls: ['./addmanagementemp.component.css']
})
export class AddmanagementempComponent implements OnInit {
  fileq : any;
  errflag : any;
  addmanagmentEp : addmangmentEmployeeModel
  today: string;
  baseurl = MainURL.HostUrl
  fileToUpload: File = null;
  imageUrl: string;
  fileUrl: string;
  empPerId:any;
  selectedFileName: string = "No File Choosen";
  modelobj: any;
  fileSelected: any;
  AssetsPath=MainURL.AssetsPath;
  model: any;

  constructor(
    private http: Http,
    private CoffServic: coffService
    ) { 

    this.today = new Date().getFullYear()+ "/" + (new Date().getMonth() + 1) + "/" +new Date().getDate();

    // this.addmanagmentEp.dateOfBirth  =   this.today;

    this.addmanagmentEp = new addmangmentEmployeeModel()
    this.addmanagmentEp.gender = 'Male';
    this.addmanagmentEp.salutation = 'MR';
  }

  ngOnInit() {
  }
  changeSalu(){
    if(this.addmanagmentEp.gender == 'Male'){
      this.addmanagmentEp.salutation = 'MR'
    }
    if(this.addmanagmentEp.gender == 'Female'){
      this.addmanagmentEp.salutation = 'Mrs'
    }
    if(this.addmanagmentEp.gender == 'Female'){
      this.addmanagmentEp.salutation = 'Miss'
    }
}
genderChange() {

  if (this.addmanagmentEp.salutation == 'MR') {
    this.addmanagmentEp.gender = 'Male'
  }
  if (this.addmanagmentEp.salutation == 'Mrs') {
    this.addmanagmentEp.gender = 'Female'
  }
  if (this.addmanagmentEp.salutation == 'Miss') {
    this.addmanagmentEp.gender = 'Female'
  }
}

getdate(event){
this.addmanagmentEp.dateOfBirth=event;
if (event) {
  var dob = event; //Here Im getting dob
var today = new Date();
var birthDate = new Date(dob);

this.addmanagmentEp.age = today.getFullYear() - birthDate.getFullYear();
var m = today.getMonth() - birthDate.getMonth();
if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
  this.addmanagmentEp.age--;
  
}

    }
}
// calculateage(getvalue) {
//   var dob = new Date(this.familyModel.dob);
//   //console.log("this is the selected date"+this.dob);
//    var cur = new Date();
//   var diffD = Math.round((Math.abs(dob - cur) / (24 *60 * 60 * 1000))/365)
//   this.age=diffD;
//   console.log("this is diff between current date and selected date"+diffD)
//   // this.diff = this.cur - this.dob; //This is the difference in milliseconds
//   // this.age = Math.round(this.diff / 31557600000); //Divide by 1000*60*60*24*365.25
  
//   // console.log("this is diff in two date selected date and current date"+this.age)
//   // this.familyModel.age = this.age;
  
// }



handleFilesInput(files: FileList) {
  this.selectedFileName = "No File Choosen";
  this.fileToUpload = files.item(0);
  this.selectedFileName = this.fileToUpload.name;
  this.uploadFileToActivity();
}


uploadFileToActivity() {
  let obj = this.fileSelected;
  obj = this.modelobj;
  this.CoffServic.postFile(this.fileToUpload).subscribe(data => {
    // do something, if upload success
    this.fileUrl = data.json().result.imageUrl;
    this.addmanagmentEp.docUrl  = this.fileUrl;

  }, error => {
  });
}


//For Image file uploading

handleImageFilesInput(files: FileList) {
  this.selectedFileName = "No File Choosen";
  this.fileToUpload = files.item(0);
  this.selectedFileName = this.fileToUpload.name;
  this.uploadImageFileToActivity();
}


uploadImageFileToActivity() {
  let obj = this.fileSelected;
  obj = this.modelobj;
  this.CoffServic.postFile(this.fileToUpload).subscribe(data => {
    // do something, if upload success
    this.imageUrl = data.json().result.imageUrl;
    this.addmanagmentEp.filPath = this.imageUrl;

  }, error => {
  });
}

onSubmit(f){
  
  let url = this.baseurl + '/employer';
  return this.http.post(url, this.addmanagmentEp).subscribe(data =>{
  });
}
keyPressNumbers(event) {
  var charCode = (event.which) ? event.which : event.keyCode;
  // Only Numbers 0-9
  if ((charCode < 48 || charCode > 57)) {
    event.preventDefault();
    return false;
  } else {
    return true;
  }
}
}