import { Routes, RouterModule } from '@angular/router'
import { AddmanagementempComponent } from './addmanagementemp.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: AddmanagementempComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'addmanagementemp',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'addmanagementemp',
                    component: AddmanagementempComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);