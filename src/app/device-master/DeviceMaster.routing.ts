import { Routes, RouterModule } from '@angular/router'
import { LayoutComponent } from 'src/app/layout/layout.component';
import { DeviceMasterComponent } from 'src/app/device-master/device-master.component';
import { DeviceMComponent } from 'src/app/device-master/Pages/device-master/device-master.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: DeviceMasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'devicemaster',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'devicemaster',
                    component: DeviceMComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);