import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { DeviceMasterService } from '../../../shared/services/deviceMasterService';
import { DeviceMaster } from '../../../shared/model/deviceMasterModel';
import { from } from 'rxjs';
import { MainURL } from '../../../shared/configurl';
import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-device-master',
  templateUrl: './device-master.component.html',
  styleUrls: ['./device-master.component.css']
})
export class DeviceMComponent implements OnInit {
  Devicemodel: DeviceMaster;
  baseurl = MainURL.HostUrl;
  locationData: any;
  alldevice: any;
  Selectededitobj: any;
  isedit = false;
  selecteLocationobj: any;
  loggedUser: any;
  compId: any;
  errordata: any;
  msg: any;
  payRollHeadName: any;
  errflag: boolean;
  dltmodelFlag: boolean;
  dltObje: any;
  loconame: String;
  public selectUndefinedOptionValue:any;
  public selectedDeviceUsed: any;
  constructor( public httpService: HttpClient, 
    public Deviceservice: DeviceMasterService, 
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService)
     {
      this.Devicemodel=new DeviceMaster();
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.compId =  this.loggedUser.compId
     }

  ngOnInit() {
    this.getalldevice()
    let url = this.baseurl + '/Location/1';
    this.httpService.get(url).subscribe(data => {
        this.locationData = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }
  StatusFlag:boolean=false;
  onSubmit(f){
    this.StatusFlag=false;
     if(this.isedit == false){
        this.Devicemodel.locationId  = this.selecteLocationobj;
        this.Deviceservice.postDeviceMaster(this.Devicemodel).subscribe(data => {
          f.reset();
          this.resetForm();
          this.toastr.success('Device-Master Information Inserted Successfully!', 'Device-Master');
          this.getalldevice();
        },(err: HttpErrorResponse) => {
        this.toastr.error('Server Side Error..!', 'Device Master');
      });
  }
  else{
    let url1 = this.baseurl + '/Device'; 
    this.Devicemodel.locationId = this.selecteLocationobj;
    this.Devicemodel.deviceMasterId = this.Selectededitobj;
    this.httpService.put(url1,this.Devicemodel).subscribe(data => {
      f.reset();
      this.resetForm();
      this.toastr.success('Device-Master Information Updated Successfully!', 'Device-Master');
      this.getalldevice()
      this.isedit=false
    },(err: HttpErrorResponse) => {
      this.toastr.error('Server Side Error..!', 'Device Master');
      this.isedit=false
    });
  }
}
dltmodelEvent(f){
  this.dltmodelFlag=true;
  this.dltObjfun(this.dltObje);
  f.reset();
  this.resetForm();
}
dltObj(obj){
  this.dltmodelFlag=false;
  this.dltObje =obj.deviceMasterId;
  this.dltObjfun(this.dltObje);
}
dltObjfun(object){
    if(this.dltmodelFlag==true){
      let url1 = this.baseurl + '/device/';
      this.Devicemodel.deviceMasterId= this.dltObje;
      this.httpService.delete(url1 + this.dltObje).subscribe(data => {
        this.toastr.success('Delete Successfully', 'Device-Master');
        this.getalldevice()
      },(err: HttpErrorResponse) => {
        this.toastr.error('Server Side Error..!', 'Device-Master');
      });
    }
}
 
error(){
  if(this.loconame != this.Devicemodel.deviceName){
   this.payRollHeadName=this.Devicemodel.deviceName
      let url = this.baseurl + '/deviceMaster/';
      this.httpService.get(url+ this.payRollHeadName).subscribe((data :any) => {
         this.errordata= data["result"];
         this.msg= data.message
         this.toastr.error(this.msg);
         this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        }
      );
    }
  }
uiswitch(event){
   var check = event.target['className'];
  if(event.target.className == "switch switch-medium checked"){
    this.Devicemodel.deviceStatus = true;
  }else{
     this.Devicemodel.deviceStatus = false;
  }

}

ngValue(event){
   this.selecteLocationobj = parseInt(event.target.value);
}
dataTableFlag : boolean  = true
getalldevice(){
    let url = this.baseurl + '/device';
    this.dataTableFlag   = false
    this.httpService.get(url).subscribe(data => {
        this.alldevice = data["result"];
        this.dataTableFlag   = true
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable();
          });
        }, 1000);
        this.chRef.detectChanges();     
      },(err: HttpErrorResponse) => {
      });
  }

  edit(object){
    this.Selectededitobj = object.deviceMasterId;
    this.isedit = true;
    this.StatusFlag=true;
    this.editobject(this.Selectededitobj);
  }
  
  editobject(Selectededitobj){
      let urledit = this.baseurl + '/device/';
      this.httpService.get(urledit + Selectededitobj).subscribe(data => {
          this.Devicemodel.deviceName = data['result']['deviceName'];
          this.Devicemodel.deviceIpAddress = data['result']['deviceIpAddress'];
          this.Devicemodel.deviceSerialNumber = data['result']['deviceSerialNumber'];
          this.Devicemodel.deviceUsedFor = data['result']['deviceUsedFor'];
          this.Devicemodel.deviceExactLocation = data['result']['deviceExactLocation'];
          this.Devicemodel.deviceStatus = data['result']['deviceStatus']
          this.Devicemodel.ioStatus = data['result']['ioStatus'];
          this.loconame = this.Devicemodel.deviceName
          var loc_id = data['result']['locationMaster']['locationId'];
        for(var i=0;i<this.locationData.length;i++){
           if(this.locationData[i].locationId == loc_id){
             this.selecteLocationobj =  loc_id;
           }
        }
      },
      (err: HttpErrorResponse) => {
      });
    }
    resetForm(){
      setTimeout(() => {
        this.selecteLocationobj = this.selectUndefinedOptionValue;
      }, 100);
    }
    reset(){
      this.isedit = false
      this.StatusFlag= false;
    }
}
