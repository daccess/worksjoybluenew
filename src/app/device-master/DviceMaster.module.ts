import { routing } from './DeviceMaster.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UiSwitchModule } from 'ngx-ui-switch';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
 import { DeviceMasterComponent } from './device-master.component';
import { DeviceMComponent } from './Pages/device-master/device-master.component';
import { HolidayMasterService } from '../shared/services/holidayMasterService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DeviceMasterService } from '../shared/services/deviceMasterService';

@NgModule({
    declarations: [
        DeviceMasterComponent,
        DeviceMComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule.forRoot()
     
    ],
    providers: [DeviceMasterService]
  })
  export class DeviceModule { 
      constructor(){

      }
  }
  