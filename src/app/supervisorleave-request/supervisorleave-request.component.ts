import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CompanyMasterService } from 'src/app/shared/services/companymasterservice';
import { leaveRequestService } from './services/leaverequestservice';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { applyLeave } from './model/applyLeave';
import { ToastrService } from 'ngx-toastr';

declare var $;

@Component({
  selector: 'app-supervisorleave-request',
  templateUrl: './supervisorleave-request.component.html',
  styleUrls: ['./supervisorleave-request.component.css']
})
export class SupervisorleaveRequestComponent implements OnInit {

  baseurl = MainURL.HostUrl
  date: any = new Date();
  daysInThisMonth: any = [];
  daysInLastMonth: any = [];
  daysInNextMonth: any = [];
  monthNames: any;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  absentDay: any = "17"
  currentMonth1: any;
  from: any;
  to: any;
  leaveDays = [];
  searchText: string = "";
  model: any = { empPerId :"",frmdate: "", todate: "", leavereson: "", handedover: [], empnotified: [], type: "", typeofleave: "Fullday" };
  compId: number;
  Employeedata: any = [];
  EmployeedataLeave: any = [];
  selectedEmployee: any = [];
  dropdownSettings: any;
  toBeNotified: any = [];
  handedWorkTo: any = [];
  empOfficialId: any;
  attendanceData: any = [];
  calAttendance: any = [];
  leaveConfigList: any = [];
  // selectUndefinedOptionValue: any = "Select";
  loggedUser: any;
  empPerId: any;
  empId: any;
  totalLeaveBalance = [];
  casualTotalLeaveBalance: any;
  casualCurrentLeaveBalance: any;
  privilegeTotalLeaveBalance: any;
  privilegeCurrentLeaveBalance: number;
  sickTotalLeaveBalance: any;
  sickCurrentLeaveBalance: any;
  otherLeaveBalance: any;
  bal: any;
  RequestObj: { empId: any; fromDate: any; status: "Pending" };
  tempModel: any = {};
  leaveApplicationList = [];
  modeflag: boolean
  deleteItem: any;
  isLeaveEdit: boolean = false;
  today: string;
  HolidayData = [];

  AllHRRequestToWorkList: any;
  empFlag: boolean;
  searchQuery: any;
  designation: any;
  empFlag1: boolean;
  searchQuery1: any;
  minimumDays: number;
  themeColor: string = "nav-pills-blue";
  selectedempPerId: any;
  EmployeeApproverdata: any = [];
  firstLevel: any;
  secondLevel: any;
  thirdLevel: any;
  getEmpOfficialId: any;
  hideCalender: boolean = false;

  public selectUndefinedOptionValue: any
  leavePatternId: any;
  personalData: any;
  weekn: any;
  roleofUser:any; 
  leaveErrorMessage = [{ message: "" }];
  leaveBalanceFlag: boolean = true;
  leavePaidOrUnpaid: any;
  constructor(public toastr: ToastrService, private service: CompanyMasterService, private leaverequestService: leaveRequestService, public httpService: HttpClient,
    public chRef: ChangeDetectorRef) {
    this.model.typeofleave = "Fullday";
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.modeflag = false;
    this.compId = this.loggedUser.compId;
    this.empPerId = this.loggedUser.empPerId;
    this.empId = this.loggedUser.empId;
    this.roleofUser = this.loggedUser.mrollMaster.roleName;
    this.getAllEmployeeForLeave();
    this.Holidays();
    this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    this.getAllEmployee();
    this.Leaves();
    this.tempModel.thisdate = new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1;
    this.today = new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1;
    this.RequestObj = {
      empId: this.empId,
      fromDate: new Date().getFullYear() + "-" + (new Date().getMonth()) + "-" + 1,
      status: "Pending"
    }
    this.getLeaveApplicationListByEmpId();
    this.selectedLeaveCount = 1;
    this.model.selectedLeaveCount = 1;
    if (sessionStorage.getItem('from') == 'yearly') {
      this.model.frmdate = sessionStorage.getItem('requestedDate');
      this.model.todate = sessionStorage.getItem('requestedDate');
      sessionStorage.setItem('from', '');
      sessionStorage.setItem('requestedDate', new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate());
    }

    if (sessionStorage.getItem('from') == 'empd') {
      if (sessionStorage.getItem('requestedType') == 'L') {
        this.isLeaveEdit = true;
        let data = sessionStorage.getItem('data');
        this.editLeave(JSON.parse(data));
        sessionStorage.setItem('from', '');
      }
    }
    this.statusType = "Pending";
    let themeColor = sessionStorage.getItem('themeColor');
    if (themeColor) {
      this.themeColor = themeColor;
    }
  }
  weekAttendance = [];
  statusType: "Pending";
  selectThisDate(event) {
    this.RequestObj = {
      empId: this.empId,
      fromDate: event,
      status: this.statusType
    };
    
    this.getLeaveApplicationListByEmpId();
  }
  statusTypeChanged(e) {
    this.RequestObj.status = this.statusType;
    this.getLeaveApplicationListByEmpId();
  }
  //====================holidays===========================
  Holidays() {
    let url = this.baseurl + '/HoliDays/';
    this.httpService.get(url + this.loggedUser.locationId).subscribe(data => {
        this.HolidayData = data["result"];
      },
      (err: HttpErrorResponse) => {
      });

  }
  reaplyStatus: boolean = false;
  //=================Leave Suggestion Ends===============
  modelEvent() {
    this.modeflag = true;
    this.deleteLeaveRequest(this.deleteItem)
  }
  getLeave(item) {
    this.modeflag = true;
    this.deleteItem = item;
  }
  deleteLeaveRequest(item) {
    this.leaverequestService.deleteLeaveRequest(item.leaveRequestId).subscribe(data => {
      this.getLeaveApplicationListByEmpId();
      this.toastr.success("Successfully Deleted Your Leave");
    }, err => {
      this.toastr.error("Failed To Delete Your Leave");
    })
  }
  getLeaveApplicationListByEmpId() {
    this.leaveApplicationList = []
    this.leaverequestService.getLeaveApplicationListByEmpId(this.RequestObj).subscribe(data => {
      this.leaveApplicationList = data.result;
    }, err => {
    })
  }
  editLeave(item) {
    this.modeflag = false;
    let editData: any = [];
    this.leaverequestService.getLeaveApplicationListById(item.leaveRequestId).subscribe(data => {
      editData = data.json().result;
      this.isLeaveEdit = true;
      this.chRef.detectChanges();
      this.model.empPerId = editData.empPerId;
      this.selectEmp(editData.empPerId);
      this.getLeaveData(editData.empPerId);
      this.getApoproveObject(editData.empPerId);
      this.model.leaveRequestId = editData.leaveRequestId;
      this.model.type = editData.leaveId;
      this.model.typeofleave = editData.typeOfLeave;
      this.model.frmdate = editData.startDate;
      this.model.todate = editData.endDate;
      this.model.leavereson = editData.leaveReson;
      this.model.handedover = editData.handedOverWorkList;
      this.model.empnotified = editData.employeesToBeNotifiedList;
      this.model.updateFromLeaveOrSugg = "leave";
      this.leavePatternId = editData.leavePatternId;
      if(editData.handedOverWorkList){
        for (let i = 0; i < editData.handedOverWorkList.length; i++) {
          this.searchQuery = editData.handedOverWorkList[i].fullName + " " + editData.handedOverWorkList[i].lastName;
        }
      }
      
      if(editData.employeesToBeNotifiedList){
        for (let i = 0; i < editData.employeesToBeNotifiedList.length; i++) {
          this.searchQuery1 = editData.employeesToBeNotifiedList[i].fullName + " " + editData.employeesToBeNotifiedList[i].lastName;
        }
      }
      this.model.frmdate = editData.startDate;
      this.model.todate = editData.endDate;
      this.leaveErrorMessageFlag = true;
      document.getElementById('pills-home-tab').click();
    }, err => {

    })
  }
  getAttendanceOfMonth(month, year) {
    let obj = {
      "empOfficialId": 606,
      "month": month,
      "year": year
    }
    this.leaverequestService.monthlyAttendance(obj).subscribe(data => {
      this.attendanceData = data.result;
      this.getDaysOfMonth();
    }, (err => {
      this.getDaysOfMonth();
    })
    )
  }
  show() {
    this.model.frmdate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
  }
  Leaves() {

    let url = this.baseurl + '/EmpLeavesBalanceDetails/';
    this.httpService.get(url + this.empPerId).subscribe((data: any) => {
        this.totalLeaveBalance = data.result.list;
        for (let i = 0; i < this.totalLeaveBalance.length; i++) {
          if (this.totalLeaveBalance[i].leaveName == 'Casual Leave') {
            this.casualTotalLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
            this.casualCurrentLeaveBalance = this.totalLeaveBalance[i].currentLeaves;
          }
          if (this.totalLeaveBalance[i].leaveName == 'privilege leave') {
            this.privilegeTotalLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
            this.privilegeCurrentLeaveBalance = this.totalLeaveBalance[i].currentLeaves;
          }
          if (this.totalLeaveBalance[i].leaveName == 'Sick Leave') {
            this.sickTotalLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
            this.sickCurrentLeaveBalance = this.totalLeaveBalance[i].currentLeaves;
          }
          if (this.totalLeaveBalance[i].leaveName == 'Other') {
            this.otherLeaveBalance = this.totalLeaveBalance[i].totalLeaves;
          }
        }
      },
      (err: HttpErrorResponse) => {
      });

  }
  reset() {
    this.leaveBalanceFlag = false;
    setTimeout(() => {   
      this.selectedLeaveCount = 1;
      this.model.selectedLeaveCount = 1;
      this.searchQuery = null;
      this.searchQuery1 = null;
      this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.type = "";
      this.model.typeofleave = "Fullday";
      this.model.leavereson = "";
    }, 100);
  }

  fromToDate(value) {
    this.from = value;
  }
  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'empPerId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    if (sessionStorage.getItem('from') == 'layout') {
      sessionStorage.setItem('from', '');
      document.getElementById('pills-suggest-tab').click();
    }
  }

  //==========================
  onItemSelect(item: any) {
    if (this.handedWorkTo.length == 0) {
      this.handedWorkTo.push(item);
    } else {
      this.handedWorkTo.push(item);
    }
  }
  onSelectAll(items: any) {
    this.handedWorkTo = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.handedWorkTo.length; i++) {
      if (this.handedWorkTo[i].locationId == item.locationId) {
        this.handedWorkTo.splice(i, 1);
      }
    }
  }

  onDeSelectAll(item: any) {
    this.handedWorkTo = [];
  }
  onItemSelect1(item: any) {
    if (this.toBeNotified.length == 0) {
      this.toBeNotified.push(item);
    } else {
      this.toBeNotified.push(item);
    }
  }
  onSelectAll1(items: any) {
    this.toBeNotified = items;
  }

  OnItemDeSelect1(item: any) {
    for (var i = 0; i < this.toBeNotified.length; i++) {
      if (this.toBeNotified[i].locationId == item.locationId) {
        this.toBeNotified.splice(i, 1);
      }
    }
  }
  onDeSelectAll1(item: any) {
    this.toBeNotified = [];
  }
  toDateFlag: boolean = false;
  typeOfLeave(typeofleave) {
    if (this.model.typeofleave == 'Fullday') {
      this.toDateFlag = false;
      var oneDay = 24 * 60 * 60 * 1000;
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
      this.selectedLeaveCount = diffDays + 1;
    }
    else {
      this.toDateFlag = true;
      this.model.todate = this.model.frmdate;
      this.selectedLeaveCount = 0.5;
    }
    this.model.selectedLeaveCount = this.selectedLeaveCount;
  }
  selectedLeaveCount: number = 0;
  leaveErrorMessageFlag: boolean = false;
  selectFromDate(event) {
    this.leaveBalanceFlag = false;
    var oneDay = 24 * 60 * 60 * 1000;
    this.model.frmdate = event;
    if (this.model.typeofleave == 'Fullday') {
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
      this.selectedLeaveCount = diffDays + 1;
    }
    else {
      this.model.todate = event;
      this.selectedLeaveCount = 0.5;
    }

    if (new Date(this.model.frmdate).getTime() > new Date(this.model.todate).getTime()) {
      this.model.todate = event;
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
      this.selectedLeaveCount = diffDays + 1;

    }
    this.model.selectedLeaveCount = this.selectedLeaveCount;
    let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    this.weekn = weekNumber;
    let leavePatternId;
    let currentLeaves
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves;
        this.leavePaidOrUnpaid = this.leaveConfigList[i].leavePaidOrUnpaid;
      }
    }
    this.leavePatternId = leavePatternId

    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.empId,
      leavePatternId: leavePatternId,
      // leaveType: this.model.type,
      empOfficialId: this.getEmpOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves,
      "leavePaidOrUnpaid":this.leavePaidOrUnpaid
    }
    if (this.model.type) {
      this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
        this.leaveErrorMessage = data.result;
        this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
        this.leaveErrorMessageFlag = true;
      }, err => {
        if(err.json()) {
          this.leaveErrorMessage = err.json().result;
          this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
           this.leaveErrorMessageFlag = false;
         }
         else {
           this.leaveErrorMessage = [];
         }
      })
    }
    for (let i = 0; i < this.totalLeaveBalance.length; i++) {
      for (let j = 0; j < this.leaveConfigList.length; j++) {
        if (this.totalLeaveBalance[i].leaveId == this.model.type) {
          let leaveBalence = this.totalLeaveBalance[i].currentLeaves;
          if (leaveBalence >= this.selectedLeaveCount) {
            this.leaveBalanceFlag = true;
          }

        }
      }
    }
  }
 
  selectToDate(e) {
    this.model.todate = e;
    var oneDay = 24 * 60 * 60 * 1000;
    this.leaveBalanceFlag = false;
    var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
    if (this.model.typeofleave == 'Fullday') {
      var diffDays = Math.round(Math.abs((new Date(this.model.frmdate).getTime() - new Date(this.model.todate).getTime()) / (oneDay)));
      this.selectedLeaveCount = diffDays + 1;
    }
    else {
      this.selectedLeaveCount = 0.5;
    }
    if (new Date(this.model.frmdate).getTime() > new Date(e).getTime()) {
      this.model.todate = e;
    }
    this.model.selectedLeaveCount = this.selectedLeaveCount;
    let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    this.weekn = weekNumber;
    let leavePatternId;
    let currentLeaves
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves
      }
    }
    this.leavePatternId = leavePatternId

    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.empId,
      leavePatternId: leavePatternId,
      // leaveType: this.model.type,
      empOfficialId: this.getEmpOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves
    }
    if (this.model.type) {
      this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
        this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
        this.leaveErrorMessageFlag = true;
      }, err => {
        if (err.json()) {
          this.leaveErrorMessage = err.json().result;
          this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
          this.leaveErrorMessageFlag = false;
        }else{
          this.leaveErrorMessage = [];
        }
      })
    }

    for (let i = 0; i < this.totalLeaveBalance.length; i++) {
      for (let j = 0; j < this.leaveConfigList.length; j++) {
        if (this.totalLeaveBalance[i].leaveId == this.model.type) {
          let leaveBalence = this.totalLeaveBalance[i].currentLeaves;
          if (leaveBalence >= this.selectedLeaveCount) {
            this.leaveBalanceFlag = true;
          }
        }
      }
    }
  }

  selectLeaveType(list,value) {
    if(value){
      for(let i=0;i<list.length;i++){
        if(list[i].leaveId==value){
            this.leavePaidOrUnpaid=list[i].leavePaidOrUnpaid;
        }
       
      }
    }
    let dateObj = new Date(this.model.frmdate)
    var firstDayThisMonth = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(dateObj.getFullYear(), dateObj.getMonth(), 0).getDate();
    let daysObj = new Array()
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysObj.push(i);
    }
    let weekNumber = Math.floor((new Date(this.model.frmdate).getDate() + daysObj.length) / 7) + 1;
    this.weekn = weekNumber;
    let leavePatternId;
    let currentLeaves
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == parseInt(this.model.type)) {
        leavePatternId = this.leaveConfigList[i].leavePatternId;
        currentLeaves = this.leaveConfigList[i].currentLeaves
      }
    }
    this.leavePatternId = leavePatternId

    let obj = {
      leaveId: parseInt(this.model.type),
      empPerId: this.empPerId,
      empId: this.empId,
      leavePatternId: leavePatternId,
      // leaveType: this.model.type,
      empOfficialId: this.getEmpOfficialId,
      startDate: this.model.frmdate,
      endDate: this.model.todate,
      typeOfLeave: this.model.typeofleave,
      locationId: this.loggedUser.locationId,
      dayOfWeek: weekNumber,
      leaveCount: this.model.selectedLeaveCount,
      currentLeaves: currentLeaves,
      "leavePaidOrUnpaid":this.leavePaidOrUnpaid
    }
    this.leaverequestService.checkConfigForLeaveApply(obj).subscribe(data => {
      this.leaveErrorMessage = data.result;
      this.minimumDays = this.leaveErrorMessage['miniumDaysToApply'];
      this.leaveErrorMessageFlag = true;
    }, err => {
      if (err.json()) {
        this.leaveErrorMessage = err.json().result;
        this.leaveErrorMessageFlag = false;
      }
    })
  }
  onSearchChange(searchtext) {
    this.searchText = searchtext;
  }
  deleteLanguage(i) {
    this.selectedEmployee.splice(i, 1);
  }
  getAllEmployee() {
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.Employeedata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    let obj;
    for (let i = 0; i < thisNumOfDays; i++) {
      let item;
      let flag = false;
      for (let j = 0; j < this.attendanceData.length; j++) {
        if (!this.attendanceData[j].checkIn) {
          this.attendanceData[j].checkIn = new Date(this.attendanceData[j].checkIn).setHours(0, 0, 0);
        }
        if (!this.attendanceData[j].checkOut) {
          this.attendanceData[j].checkOut = new Date(this.attendanceData[j].checkOut).setHours(0, 0, 0);
        }

        if (new Date(this.attendanceData[j].attenDate).getDate() == i + 1) {
          flag = true;
          if (this.attendanceData[j].statusOfDay == 'WD') {
            if (this.attendanceData[j].attendanceStatus == 'P' || this.attendanceData[j].attendanceStatus == 'VP') {
              if (this.attendanceData[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: this.attendanceData[j].leaveStatus,
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: this.attendanceData[j].coffStatus + " Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "On Duty"
                }
              }
              else if (this.attendanceData[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Work From Home"
                }
              }

              else {
                let checkIn = "";
                let checkOut = "";
                if (this.attendanceData[j].checkIn) {
                  checkIn = new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes();
                }
                if (this.attendanceData[j].checkOut) {
                  checkOut = new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration);
                }
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: 'In ' + checkIn,
                  checkOut: ' Out ' + checkOut
                }
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF' && !this.attendanceData[j].wfhStatus) {
              item = {
                day: i + 1,
                date: this.attendanceData[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF' && this.attendanceData[j].wfhStatus) {
              item = {
                day: i + 1,
                status: "WFH",
                date: this.attendanceData[j].attenDate,
                checkIn: "Work From Home"
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.attendanceData[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Leave",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].wfhStatus) {
                item = {
                  day: i + 1,
                  status: "WFH",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Work From Home"
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.attendanceData[j].attenDate
                }
              }

            }


          }
          else if (!this.attendanceData[j].statusOfDay) {

            if (this.attendanceData[j].attendanceStatus == 'P') {

              if (this.attendanceData[j].lateEarlyStatus) {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].odStatus) {
                item = {
                  day: i + 1,
                  status: "OD",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "On Duty"
                }
              }



              else {
                item = {
                  day: i + 1,
                  status: "P",
                  checkIn: "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'HF') {
              item = {
                day: i + 1,
                date: this.attendanceData[j].attenDate,
                status: "LT",
                checkIn: "Half Day " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
              }
            }
            else if (this.attendanceData[j].attendanceStatus == 'A') {
              let holiDayName;
              if (this.attendanceData[j].statusOfDay == 'HO') {
                for (let m = 0; m < this.HolidayData.length; m++) {

                  if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                    holiDayName = this.HolidayData[m].holiDayName;
                  }

                }
                item = {
                  day: i + 1,
                  status: "HO",
                  checkIn: holiDayName
                }
              }
              else if (this.attendanceData[j].coffStatus) {
                item = {
                  day: i + 1,
                  status: "CO",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Coff",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].leaveStatus) {
                item = {
                  day: i + 1,
                  status: "L",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Leave",
                  checkOut: ""
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LT") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LT " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "LTEG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "LTEG " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else if (this.attendanceData[j].lateEarlyStatus == "EG") {
                item = {
                  day: i + 1,
                  status: "LT",
                  date: this.attendanceData[j].attenDate,
                  checkIn: "Early Going " + "In " + new Date(this.attendanceData[j].checkIn).getHours() + ":" + new Date(this.attendanceData[j].checkIn).getMinutes(),
                  checkOut: " Out " + new Date(this.attendanceData[j].checkOut).getHours() + ":" + new Date(this.attendanceData[j].checkOut).getMinutes() + ", WD:" + this.timeConvert(this.attendanceData[j].workDuration)
                }
              }
              else {
                item = {
                  day: i + 1,
                  status: "A",
                  checkIn: "Absent",
                  date: this.attendanceData[j].attenDate
                }
              }

            }


          }
          else {
            if (this.attendanceData[j].statusOfDay == 'HO') {
              let holiDayName;
              for (let m = 0; m < this.HolidayData.length; m++) {

                if (new Date(this.HolidayData[m].startDate).getTime() <= new Date(this.attendanceData[j].attenDate).getTime() && new Date(this.HolidayData[m].endtDate).getTime() >= new Date(this.attendanceData[j].attenDate).getTime()) {
                  holiDayName = this.HolidayData[m].holiDayName;
                }

              }
              item = {
                day: i + 1,
                status: "HO",
                checkIn: holiDayName
              }
            } else {
              item = {
                day: i + 1,
                status: "WO",
                checkIn: "Week Off"
              }
            }

          }

          this.daysInThisMonth.push(item);
        }
      }
      if (!flag) {

        let holiDayName;
        let holidayFlag: boolean = false;
        if (this.HolidayData) {
          for (let m = 0; m < this.HolidayData.length; m++) {
            holidayFlag = false;
            if (new Date(new Date(this.HolidayData[m].startDate).getFullYear(), new Date(this.HolidayData[m].startDate).getMonth(), new Date(this.HolidayData[m].startDate).getDate(), 0, 0, 0, 0).getTime() <= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime() && new Date(new Date(this.HolidayData[m].endtDate).getFullYear(), new Date(this.HolidayData[m].endtDate).getMonth(), new Date(this.HolidayData[m].endtDate).getDate(), 0, 0, 0, 0).getTime() >= new Date(this.currentYear, this.date.getMonth(), i + 1, 0, 0, 0, 0).getTime()) {
              holiDayName = this.HolidayData[m].holiDayName;
              holidayFlag = true;
              break;
            }
          }
        }

        if (holidayFlag) {
          item = {
            day: i + 1,
            status: "HO",
            checkIn: holiDayName
          }
        }
        else {
          item = {
            day: i + 1,
            status: "Ot"
          }
        }
        this.daysInThisMonth.push(item);

      }

    }
    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    let nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
    for (let i = 0; i < (6 - lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i + 1);
    }
  }




  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ":" + rminutes;
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.attendanceData = [];
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.attendanceData = [];
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
  }
  selectDate(day) {
    this.currentDate = day;
    if (this.from == 'from') {
      this.model.frmdate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
    } else if (this.from == 'to') {
      this.model.todate = (this.date.getMonth() + 1) + "-" + day + "-" + this.date.getFullYear();
    }
  }
  selectEmp(e){
  this.selectedempPerId = e;
  this.getLeaveData(this.selectedempPerId);
  this.getApoproveObject(this.selectedempPerId);
  this.getInfo();
  for (let i = 0; i < this.EmployeedataLeave.length; i++) {
     if(this.EmployeedataLeave[i].empPerId == this.selectedempPerId){
        this.getEmpOfficialId = this.EmployeedataLeave[i].empOfficialId;
        this.getAttendanceOfMonth(new Date().getMonth() + 1, new Date().getFullYear())
     }
  }
  this.hideCalender = true;
  }

  getInfo(){
    let url = this.baseurl + '/employeeMasterbyId/';
    this.httpService.get(url + this.selectedempPerId).subscribe(data => {
        this.personalData = data["result"];
        this.model.subEmpTypeId = this.personalData.subEmpTypeId;
      },(err: HttpErrorResponse) => {
      });
    }
  getLeaveData(selectedempPerId){
    this.leaverequestService.getLeaveConfigurationList(selectedempPerId).subscribe((data: any) => {
      this.leaveConfigList = data.json().result;
    }, err => {
      this.leaveConfigList = err.json().result;
    })
  }

  getApoproveObject(obj){
    let url = this.baseurl + '/getReportingPerson/';
    this.httpService.get(url + obj).subscribe(data => {
         this.EmployeeApproverdata = data['result'];
        this.firstLevel = data['result']['firstLevelReportingManager'];
        if(data['result']['secondLevelReportingManager']){
          this.secondLevel = data['result']['secondLevelReportingManager'];
        }
        else{
          this.secondLevel = 0;
        }
        if(data['result']['thirdLevelReportingManager']){
          this.thirdLevel = data['result']['thirdLevelReportingManager'];
        }
        else{
          this.thirdLevel = 0;
        }
      },
      (err: HttpErrorResponse) => {
      });
  }

  fixDecimals(value: string) {
    value = "" + value;
    value = value.trim();
    value = parseFloat(value).toFixed(2);
    return value;
  }
  onSubmit(f) {
    for (let i = 0; i < this.leaveConfigList.length; i++) {
      if (this.leaveConfigList[i].leaveId == this.model.type) {
        this.model.leaveCode = this.leaveConfigList[i].leaveCode; 
      }
    }
    if (this.reaplyStatus) {
      this.reaplyStatus = false;
    }
    else {
      if (this.isLeaveEdit) {
        let obj1 = new applyLeave();
        obj1.leaveCount = this.selectedLeaveCount;
        obj1.leaveRequestId = this.model.leaveRequestId;
        obj1.handedOverWorkList = this.model.handedover;
        obj1.employeesToBeNotifiedList = this.model.empnotified;
        obj1.empPerId = this.selectedempPerId;
        obj1.startDate = this.model.frmdate;
        obj1.endDate = this.model.todate;
        obj1.leaveReson = this.model.leavereson;
        obj1.leaveId = this.model.type;
        obj1.leaveCode = this.model.leaveCode;
        obj1.typeOfLeave = this.model.typeofleave;
        obj1.firstLevelReportingManager = this.firstLevel;
        obj1.secondLevelReportingManager = this.secondLevel;
        obj1.thirdLevelReportingManager = this.thirdLevel;

        obj1.subEmpTypeId = this.model.subEmpTypeId;
        obj1.leavePatternId = this.leavePatternId;
        obj1.empOfficialId = this.getEmpOfficialId;
        obj1.updateFromLeaveOrSugg = this.model.updateFromLeaveOrSugg;
        obj1.dayOfWeek = this.weekn;
        this.isLeaveEdit = false;
        this.leaverequestService.updateLeaveRequest(obj1).subscribe(data => {
          this.toastr.success('Leave Updated Successfully');
          this.selectedLeaveCount = 1;
          this.model.selectedLeaveCount = 1;
          this.getLeaveApplicationListByEmpId();
          this.Leaves();
          f.reset();
          this.reset();
        }, err => {
          if(err.json().result){
            this.toastr.error(err.json().result.message);
          }
          else{
            this.toastr.error('Failed to update leave');
          }
        })
      } else {
        if (this.compareDate(new Date(this.model.frmdate), new Date(this.model.todate))) {
          let obj1 = new applyLeave();
          obj1.leaveCount = this.selectedLeaveCount;
          obj1.handedOverWorkList = this.model.handedover;
          obj1.employeesToBeNotifiedList = this.model.empnotified;
          obj1.empPerId = this.selectedempPerId;
          obj1.startDate = this.model.frmdate;
          obj1.endDate = this.model.todate;
          obj1.leaveReson = this.model.leavereson;
          obj1.leaveId = parseInt(this.model.type);
          obj1.leaveCode = this.model.leaveCode;
          obj1.typeOfLeave = this.model.typeofleave;
          obj1.firstLevelReportingManager = this.firstLevel;
          obj1.secondLevelReportingManager = this.secondLevel;
          obj1.thirdLevelReportingManager = this.thirdLevel;
          obj1.subEmpTypeId = this.model.subEmpTypeId;
          obj1.leavePatternId = this.leavePatternId;
          obj1.empOfficialId = this.getEmpOfficialId;
          obj1.updateFromLeaveOrSugg = this.model.updateFromLeaveOrSugg;
          obj1.dayOfWeek = this.weekn;
          this.leaverequestService.postleave(obj1).subscribe(data => {
            this.toastr.success('Leave Applied Successfully');
            this.selectedLeaveCount = 1;
            this.model.selectedLeaveCount = 1;
            this.getLeaveApplicationListByEmpId();
            this.Leaves();
            f.reset();
            this.reset();
          }, err => {
            if(err.json().result){
              this.toastr.error(err.json().result.message);
            }
            else{
              this.toastr.error('Failed apply leave');
            }
          })
        }
      }

    }
  }

  compareDate(date1: Date, date2: Date) {
    let d1 = new Date(date1);
    let d2 = new Date(date2);
    let same = d1.getTime() === d2.getTime();
    if (same) return true;
    if (d1 > d2) return false;
    if (d1 < d2) return true;
  }
  workFlowList: any = [];
  leaveWorkFlow(item) {
    this.leaverequestService.getLeaveWorkFlowList(item.leaveRequestId).subscribe(data => {
    }, err => {
      this.workFlowList = [];
    })
  }

  leaveApproveWorkFlow(item) {
    this.leaverequestService.getLeaveApproveWorkFlowList(item.leaveRequestId).subscribe(data => {
    }, err => {
      this.workFlowList = [];
    })
  }
  //=======================Work Flow Ends=======================

  arrowkeyLocation = 0;
  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }

  filterItem(value) {
    if (!value) {
      this.empFlag = false;
    }
    if (value) {
      this.empFlag = true;
    }
  }

  searchEmpPerId: any;
  suggestThis(item) {
    this.model.handedover = [item];
    this.searchQuery = item.fullName + " " + item.lastName + " - " + item.empId;
    this.searchEmpPerId = item.empPerId;
    this.designation = item.descName;
    this.empFlag = false;
  }
  arrowkeyLocation1 = 0;
  keyDown1(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation1--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation1++;
        break;
    }
  }

  filterItem1(value) {
    if (!value) {
      this.empFlag1 = false;
    }
    if (value) {
      this.empFlag1 = true;
    }
  }

  searchEmpPerId1: any;
  suggestThis1(item) {
    this.model.empnotified = [item];
    this.searchQuery1 = item.fullName + " " + item.lastName + " - " + item.empId;
    this.searchEmpPerId1 = item.empPerId;
    this.designation = item.descName;
    this.empFlag1 = false;
  }

  getAllEmployeeForLeave() {
  
    let url = this.baseurl + '/employeelistunderempid/';
    if(this.roleofUser == "HR"){
      this.httpService.get(url + this.loggedUser.empId).subscribe(data => {
        this.EmployeedataLeave = data['result'];
      },
      (err: HttpErrorResponse) => {
      });
    }
    if(this.roleofUser == "Admin"){
    let url = this.baseurl + '/EmployeeMasterOperationAdmin/';
    this.httpService.get(url + this.loggedUser.compId).subscribe(data => {
      this.EmployeedataLeave = data['result'];
    },
    (err: HttpErrorResponse) => {
    });
  }
}
}
