import { Routes, RouterModule } from '@angular/router'
import { SupervisorleaveRequestComponent } from './supervisorleave-request.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: SupervisorleaveRequestComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'supervisorleaverequest',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'supervisorleaverequest',
                    component: SupervisorleaveRequestComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);