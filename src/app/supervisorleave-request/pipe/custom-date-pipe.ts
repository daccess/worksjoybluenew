import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'datePipe' })
export class datePipe implements PipeTransform {
  // adding a default value in case you don't want to pass the format then 'yyyy-MM-dd' will be used
  transform(date1: Date | any,format: string = 'yyyy-MM-dd'): string {
    let date   = new Date(date1);  // if orginal type was a string
    // date.setDate(date.getDate() - day);
    return new DatePipe('en-US').transform(date, format);
  }
}