import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'minuteToHours'
})
export class MinuteSecondsPipe implements PipeTransform {

//   transform(value: number): string {
//     let  temp = value * 60;
//     const hours = Math.floor((temp/3600));
//     const minutes: number = Math.floor((temp/ 60)/60);
//      const seconds=Math.floor(temp % 3600 % 60);
//     return hours + ':' + minutes + ':'+ seconds;
//   }
//   transform(value: number): string {
//     let hours = Math.floor(value / 60);
//     let minutes = Math.floor(value % 60);
//     return '0'+hours + ':' + minutes;
//   }
transform(value: any, args?: any): any {
    return moment(value,'HH:mm').format("HH:mm A");
  }
}