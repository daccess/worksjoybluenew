import { routing } from './supervisor-request.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SearchPipeForEmpleave } from './SearchPipe/Serach';
import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { leaveRequestService } from './services/leaverequestservice';
import { FilterPipe } from './pipe/filter.pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { datePipe } from './pipe/custom-date-pipe';
import { SupervisorleaveRequestComponent } from './supervisorleave-request.component';


@NgModule({
    declarations: [
        SupervisorleaveRequestComponent,
        FilterPipe,
        SearchPipeForEmpleave, 
        datePipe
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [leaveRequestService]
  })
  export class SupervisorLeaveRequestModule { 
      constructor(){

      }
  }
  