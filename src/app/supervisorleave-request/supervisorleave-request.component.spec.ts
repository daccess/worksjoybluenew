import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorleaveRequestComponent } from './supervisorleave-request.component';

describe('SupervisorleaveRequestComponent', () => {
  let component: SupervisorleaveRequestComponent;
  let fixture: ComponentFixture<SupervisorleaveRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorleaveRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorleaveRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
