export class applyLeave {
  empPerId: number;
  startDate: string;
  endDate: string;
  leaveReson: string;
  handedOverWorkList = [];
  employeesToBeNotifiedList = [];
  EmployeePerInfo: string;
  leaveId:  number;
  typeOfLeave: string;
  leaveCode: any;
  leaveRequestId: any;
  leaveCount: number;
  firstLevelReportingManager: any;
  secondLevelReportingManager: any;
  thirdLevelReportingManager: any;
  subEmpTypeId: any;
  leavePatternId: any;
  empOfficialId: any;
  updateFromLeaveOrSugg: any;
  dayOfWeek: any;
}


