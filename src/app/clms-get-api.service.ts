import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MainURL } from './shared/configurl';

@Injectable({
  providedIn: 'root'
})
export class ClmsGetApiService {
  baseurl = MainURL.HostUrl;
  

  private authToken: string; // Set this value when you have the authentication token

  constructor(private http: HttpClient) {}

  setAuthToken(token: string): void {
    this.authToken = token;
  }

  getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.authToken}`,
    });
  }

  getAllDepartments(): Observable<any> {
    const url = `${this.baseurl}/getAllDepartments`;
    const headers = this.getHeaders();

    return this.http.get(url, { headers });
  }
  getAllDesignations(){
    const url = `${this.baseurl}/getAllDesignations`;
    const headers = this.getHeaders();

    return this.http.get(url, { headers })
  }
  getAllCompanyData(){
    const url = `${this.baseurl}/getAllCompany`;
    const headers = this.getHeaders();

    return this.http.get(url, { headers })
  }
  getContractorLocationName(){
    const url = `${this.baseurl}/getAllLocations`;
    const headers = this.getHeaders();

    return this.http.get(url, { headers })
  }
  GetAllEmployees(){
    const url = `${this.baseurl}/getAllEmpCategories`;
    const headers = this.getHeaders();

    return this.http.get(url, { headers })
  }
}
