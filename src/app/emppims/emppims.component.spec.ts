import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmppimsComponent } from './emppims.component';

describe('EmppimsComponent', () => {
  let component: EmppimsComponent;
  let fixture: ComponentFixture<EmppimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmppimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmppimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
