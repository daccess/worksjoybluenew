import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-emppims',
  templateUrl: './emppims.component.html',
  styleUrls: ['./emppims.component.css']
})
export class EmppimsComponent implements OnInit {
  themeColor: string = "nav-pills-blue";
  loggedUser: any;
  

  constructor(public httpService: HttpClient) {
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    let userData = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(userData);
  }

  ngOnInit() {
    let q = sessionStorage.getItem('q');
    if (q == 'personal') {
    }
    else if (q == 'education') {
      this.goToExpDetails(q)
    }
    else if (q == 'others') {
      this.goToEmpDetails(q)
    }
    else if (q == 'contact') {
      this.goToEducation(q)
    }
  }

  goToEducation(e) {
    document.getElementById('contactDetails-tab').click();
  }

  goToEmpDetails(e) {;
    document.getElementById('others-tab').click();
  }

  goToExpDetails(e) {
    document.getElementById('empeducation-tab').click();
  }

  goToOthersDetails(e) {
    if(this.loggedUser.employeeStatus){
      document.getElementById('empeducation-tab').click();
    }
    else{
      document.getElementById('empdetails-tab').click();
    }
  }
  ngOnDestroy() {
    sessionStorage.setItem('IsEmployeeEdit', 'false')
  }
}

