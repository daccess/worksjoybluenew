import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { empContactData } from '../shared/Models/empContactDetails';
import { EmpContactService } from '../shared/Services/empService';
import { ToastrService } from 'ngx-toastr';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
selector: 'app-experiencedetails',
templateUrl: './experiencedetails.component.html',
styleUrls: ['./experiencedetails.component.css']
})
export class ExperiencedetailsComponent implements OnInit {
@Output() someEvent3 = new EventEmitter<string>();
contactModel: empContactData;
baseurl= MainURL.HostUrl;
maxlength:any=10
empPerId: any;
contactData: any;
isEmployeeEdit:any="false";
loggedUser:any;
whoCliked:any;
errordata: any;
msg: any;
errflag: boolean;
slect:any;
empPerIdRole: any;
Rolename: any;
constructor(public empConServe: EmpContactService,public Spinner: NgxSpinnerService, public toastr: ToastrService, public httpService: HttpClient) { 
this.contactModel = new empContactData();
this.errflag = false;
this.whoCliked = sessionStorage.getItem('whoClicked');
this.isEmployeeEdit = sessionStorage.getItem('IsEmployeeEdit');
this.clicked();
let user = sessionStorage.getItem('loggedUser');
this.loggedUser = JSON.parse(user);
this.empPerId = this.loggedUser.empPerId;
this.contactModel.logEmpOffId = this.loggedUser.empOfficialId;
this.empPerIdRole = this.loggedUser.empPerId;
this.Rolename = this.loggedUser.mrollMaster.roleName;
this.checkRole();
}
ngOnInit(){

}
checkRole(){
  if(this.loggedUser.RoleHead == 'Admin'|| this.loggedUser.RoleHead == 'HR'){
    this.contactModel.hrLogId = this.empPerIdRole;
   }else{
     this.contactModel.empLogId = this.empPerIdRole;
   }
}

Erroremail(){
  if(this.contactData["officialEmailId"]!=this.contactModel.officialEmailId){
    let obj ={
      officialEmailId : this.contactModel.officialEmailId
  }
  let url = this.baseurl + '/empOfficialMailIdCheck';
  this.httpService.post(url,obj).subscribe((data :any) => {
     this.errordata= data["result"];
     this.msg= data.message
     this.toastr.error(this.msg);
     this.errflag = true
    },
    (err: HttpErrorResponse) => {
      this.errflag = false
    });
  }
}

clicked(){
  if(this.whoCliked=='HR'){
    let user = sessionStorage.getItem('EmpData');
    this.loggedUser = JSON.parse(user);
    this.empPerId = this.loggedUser;
  }else if(this.whoCliked=="EMP"){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.empPerId = this.loggedUser.empPerId;
  }
  this.getInfo();
}
getInfo(){
    let url = this.baseurl + '/EditEmpDetails/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
    this.contactData = data["result"];
    this.contactModel.buildingNoPermant = this.contactData["buildingNoPermant"];
    this.contactModel.buildingNoCurrent = this.contactData["buildingNoCurrent"];
    this.contactModel.socityOrLanePermant = this.contactData["socityOrLanePermant"];
    this.contactModel.socityOrLaneCurrent = this.contactData["socityOrLaneCurrent"];
    this.contactModel.areaPermant = this.contactData["areaPermant"];
    this.contactModel.areaCurrent = this.contactData["areaCurrent"];
    this.contactModel.cityOrTalukaPermant = this.contactData["cityOrTalukaPermant"];
    this.contactModel.cityorTahshilCurrent = this.contactData["cityorTahshilCurrent"];
    this.contactModel.statePermant = this.contactData["statePermant"];
    this.contactModel.stateCurrent = this.contactData["stateCurrent"];
    this.contactModel.pinCodePermant = this.contactData["pinCodePermant"];
    this.contactModel.pincodeCurrent = this.contactData["pincodeCurrent"];
    this.contactModel.officialContactNumber = this.contactData["officialContactNumber"];
    this.contactModel.alternateContactNumber = this.contactData["alternateContactNumber"];
    // this.contactModel.alternatecontactNumber = this.contactData["alternatecontactNumber"];
    this.contactModel.personalContactNumber = this.contactData["personalContactNumber"]
    this.contactModel.personalEmailId = this.contactData["personalEmailId"]
    this.contactModel.officialEmailId = this.contactData["officialEmailId"];
    this.contactModel.epersonalEmailId = this.contactData["epersonalEmailId"];
    this.contactModel.nameOfPerson = this.contactData["nameOfPerson"];
    this.contactModel.relation = this.contactData["relation"];
    this.contactModel.address = this.contactData["address"];
    this.contactModel.emrContactNumber = this.contactData["emrContactNumber"];
    this.contactModel.emrAlternatecontactNumber = this.contactData["emrAlternatecontactNumber"];
    this.contactModel.skypeId = this.contactData["skypeId"];
    this.contactModel.whatsAppNumber = this.contactData["whatsAppNumber"];
    this.contactModel.empAddressId = this.contactData["empAddressId"];
    this.contactModel.empPerId = this.contactData["empPerId"];
    this.contactModel.empContactCellAndEmailInfoId = this.contactData["empContactCellAndEmailInfoId"];
    this.contactModel.empEmergencyContactPersonDetailsId = this.contactData["empEmergencyContactPersonDetailsId"];
    this.contactModel.empscialContactsId = this.contactData["empscialContactsId"];
  },
  (err: HttpErrorResponse) => {
    
  });
}
callParent(){
  this.someEvent3.next('from others');
} 
storeInfo(){
  if(this.isEmployeeEdit=='true'){
    this.updateRecord();
    this.callParent();
  }else{
    this.saveAllData();
    this.callParent();
  }
}
saveAllData(){
  this.Spinner.show();
  this.contactModel.empPerId = this.empPerId;
  this.empConServe.postContactData(this.contactModel).subscribe(data => {
  this.callParent();
  this.toastr.success('Contact Information Inserted Successfully...!', 'Contact Information');
  this.Spinner.hide();
  },
  (err: HttpErrorResponse) => {
    this.Spinner.hide();
    this.toastr.error('Server Side Error...!', 'Contact Information');
  });
}

checkAddress(event){
  if(event.target.checked == true){
    this.contactModel.buildingNoPermant = this.contactModel.buildingNoCurrent;
    this.contactModel.socityOrLanePermant = this.contactModel.socityOrLaneCurrent;
    this.contactModel.areaPermant = this.contactModel.areaCurrent;
    this.contactModel.cityOrTalukaPermant = this.contactModel.cityorTahshilCurrent;
    this.contactModel.statePermant = this.contactModel.stateCurrent;
    this.contactModel.pinCodePermant = this.contactModel.pincodeCurrent;
  }
  else{
    this.contactModel.buildingNoPermant = null;
    this.contactModel.socityOrLanePermant = null;
    this.contactModel.areaPermant = null;
    this.contactModel.cityOrTalukaPermant = null;
    this.contactModel.statePermant = null
    this.contactModel.pinCodePermant = null;
  }
}

updateRecord(){
    this.Spinner.show();
    let url1 = this.baseurl + '/addressAndContactDetailInfo';
    let dataToSend = (this.contactModel);
    this.contactModel.employeeStatus = this.loggedUser.employeeStatus;
    this.httpService.put(url1,this.contactModel).subscribe(data => {
    this.Spinner.hide();
    this.toastr.success('Contact Information Updated Successfully!', 'Education Information');
  },
  (err: HttpErrorResponse) => {
    this.Spinner.hide();
    this.toastr.error('Server Side Error..!', 'Contact Information');
  });
}
setEmail(event){
  this.contactModel.epersonalEmailId = event;
}
errorpersonal() {
    let obj = {
      personalEmailId: this.contactModel.personalEmailId
    }
    let url = this.baseurl + '/empPerMailIdCheck';
    this.httpService.post(url, obj).subscribe((data: any) => {
        this.errordata = data["result"];
        this.msg = data.message
        this.toastr.error(this.msg);
        this.errflag = true
      },
      (err: HttpErrorResponse) => {
        this.errflag = false
      });
  }
}