import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { empBasicData } from '../shared/Models/empBasicinfomodel';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../../shared/configurl'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-emppimsbasicform',
    templateUrl: './emppimsbasicform.component.html',
    styleUrls: ['./emppimsbasicform.component.css']
})
export class EmppimsbasicformComponent implements OnInit {
    EmpBasicModel: empBasicData;
    birthdate: any;
    cur: any;
    diff: any;
    age: any;
    baseurl = MainURL.HostUrl
    loggedUser: any;
    whoCliked: any;
    empPerId: any;
    BasicData: any;
    bioId: any;
    empId: any;
    deptName: any;
    descName: any;
    fullname: any;
    empPerIdRole: any;
    Rolename: any;
    constructor(public httpService: HttpClient,public Spinner: NgxSpinnerService, private router: Router, public toastr: ToastrService, private route: ActivatedRoute) {
        this.EmpBasicModel = new empBasicData();
        this.whoCliked = sessionStorage.getItem('whoClicked');
        this.clicked();
        let user = sessionStorage.getItem('loggedUser');
        this.loggedUser = JSON.parse(user);
        this.empPerId = this.loggedUser.empPerId;
        this.fullname = this.loggedUser.fullName;
        this.EmpBasicModel.logEmpOffId = this.loggedUser.empOfficialId;
        this.empPerIdRole = this.loggedUser.empPerId;
        this.Rolename = this.loggedUser.mrollMaster.roleName;
        this.checkRole();
    }
    ngOnInit() {
        this.getData();
    }
    checkRole(){

        if(this.loggedUser.RoleHead == 'Admin'|| this.loggedUser.RoleHead == 'HR'){
         this.EmpBasicModel.hrLogId = this.empPerIdRole
        }else{
          this.EmpBasicModel.empLogId = this.empPerIdRole
        }
      }

    getData() {
        let url = this.baseurl + '/employeeMasterbyId/';
        this.httpService.get(url + this.empPerId).subscribe(data => {
            this.BasicData = data["result"];
            this.bioId = this.BasicData["bioId"];
            this.empId = this.BasicData["empId"];
            this.deptName = this.BasicData["deptName"];
            this.descName = this.BasicData["descName"];
            this.EmpBasicModel.fullName = this.BasicData["fullName"];
            this.EmpBasicModel.firstName = this.BasicData["firstName"];
            this.EmpBasicModel.middleName = this.BasicData["middleName"];
            this.EmpBasicModel.lastName = this.BasicData["lastName"];
            this.EmpBasicModel.gender = this.BasicData["gender"];
            this.EmpBasicModel.dateOfBirth = this.BasicData["dateOfBirth"];
            this.EmpBasicModel.age = this.BasicData["age"];
            this.EmpBasicModel.personalContactNumber = this.BasicData["personalContactNumber"];
            this.EmpBasicModel.personalEmailId = this.BasicData["personalEmailId"];
        }, (err: HttpErrorResponse) => {
        });
    }
    disEdit:boolean
    clicked() {
        if (this.whoCliked == 'HR') {
            let user = sessionStorage.getItem('EmpData');
            this.loggedUser = JSON.parse(user);
            this.empPerId = this.loggedUser;
            this.disEdit = false;
        } else if (this.whoCliked == "EMP") {
            let user = sessionStorage.getItem('loggedUser');
            this.loggedUser = JSON.parse(user);
            this.empPerId = this.loggedUser.empPerId;
            this.disEdit=true;
        }
    }
    getdate(event) {
        this.calculateage(event.target.value)
    }
    calculateage(getvalue) {
        this.birthdate = new Date(this.EmpBasicModel.dateOfBirth);
        this.cur = new Date();
        this.diff = this.cur - this.birthdate; //This is the difference in milliseconds
        this.age = Math.floor(this.diff / 31557600000); //Divide by 1000*60*60*24*365.25
        this.EmpBasicModel.age = this.age;
    }
    saveEmpBasic(EmpBasicModel) {
        this.Spinner.show();
        this.EmpBasicModel.empPerId = this.empPerId;
        let url1 = this.baseurl + '/HrEmpPerInfo';
        this.httpService.put(url1, this.EmpBasicModel).subscribe(data => {
            sessionStorage.setItem("whoClicked","EMP")
            this.toastr.success('Employee Basic Information Inserted Successfully!', 'Employee Basic Information');
            this.router.navigateByUrl('layout/emppims/empdetails')
            this.Spinner.hide()
        },
        (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Employee Basic Information');
        });
    }
}