import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmppimsbasicformComponent } from './emppimsbasicform.component';

describe('EmppimsbasicformComponent', () => {
  let component: EmppimsbasicformComponent;
  let fixture: ComponentFixture<EmppimsbasicformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmppimsbasicformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmppimsbasicformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
