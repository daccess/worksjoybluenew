import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class UploadImageService {

  baseUrl = MainURL.HostUrl;

  constructor(private http : HttpClient) { }

  postFile(fileToUpload: File) {

    let url = this.baseUrl + '/fileHandaling';

    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    // formData.append('ImageCaption', caption);
    return this.http.post(endpoint, formData);
  }

  uploadFile(fileToUpload: File,fileName:string){
     console.log(fileName,fileToUpload);
      
    let url = this.baseUrl + '/empDocUpload';

    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('fileName', fileName);
    return this.http.post(endpoint, formData);
  }

}