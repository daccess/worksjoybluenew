import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MainURL } from '../../../shared/configurl';

@Injectable()
export class UploadprofilepersonalService {

  baseUrl = MainURL.HostUrl;
  empDetails: any;

  constructor(private http : HttpClient) {
    this.empDetails = JSON.parse(localStorage.getItem('userDetails'))
  
   }

  postProfile(fileToUpload: File) {

    let url = this.baseUrl + '/fileHandaling';
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint, formData);
  }

  postempProfie(fileToUpload: File,empId) {
    let url = this.baseUrl + 'UploadEmpProfile';
    
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    formData.append('empPerId', empId);
    console.log(formData);    
    return this.http.post(endpoint, formData).map(x => x);
  }
}