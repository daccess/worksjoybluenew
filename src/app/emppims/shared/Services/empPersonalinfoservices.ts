import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { empPersonalInformationData } from '../Models/empPersonalInfomodel';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class EmpPersonalService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }
    postPersonalData(empPersonal: empPersonalInformationData) {
        let url = this.baseurl + '/empoloyeePersonalInfo';
        var body = JSON.stringify(empPersonal);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
    updatePersonalData(empPersonal: empPersonalInformationData) {
        let url = this.baseurl + '/empoloyeePersonalInfo';
        var body = JSON.stringify(empPersonal);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.put(url, body, requestOptions).map(x => x.json());
    }
}