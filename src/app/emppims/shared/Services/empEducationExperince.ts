import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { QualificationModel } from '../Models/empQualificationdetail';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class EmpEducationExperinceService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }
    postExpEduData(empQuali: QualificationModel) {
        let url = this.baseurl + '/educationAndExperinceInfo';
        var body = JSON.stringify(empQuali);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
}