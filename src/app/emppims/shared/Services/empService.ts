import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { empContactData } from '../Models/empContactDetails';
import { MainURL } from '../../../shared/configurl';
import { QualificationModel } from '../Models/empQualificationdetail';
import { empOtherData } from '../Models/empOtherModel';
import { empPersonalInformationData } from '../Models/empPersonalInfomodel';

@Injectable()
export class EmpContactService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }

    // --------------for contact---------------------
    postContactData(empContact: empContactData) {
        let url = this.baseurl + '/addressAndContactDetailInfo';
        var body = JSON.stringify(empContact);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
    updateContactData(empContact: empContactData) {
        let url = this.baseurl + '/addressAndContactDetailInfo';
        var body = JSON.stringify(empContact);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.put(url, body, requestOptions).map(x => x.json());
    }

    // -----------------------for educationExperience--------------------
    postExpEduData(empQuali: QualificationModel) {
        let url = this.baseurl + '/educationAndExperinceInfo';
        var body = JSON.stringify(empQuali);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
    updateExpEduData(empQuali: QualificationModel) {
        let url = this.baseurl + '/educationAndExperinceInfo';
        var body = JSON.stringify(empQuali);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.put(url, body, requestOptions).map(x => x.json());
    }

    // ---------------------------- for other details-------------
    postEmpOthersData(empOthers: empOtherData) {
        let url = this.baseurl + '/empOtherInfoDetail';
        var body = JSON.stringify(empOthers);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
    updateEmpOthersData(empOthers: empOtherData) {
        let url = this.baseurl + '/empOtherInfoDetail';
        var body = JSON.stringify(empOthers);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.put(url, body, requestOptions).map(x => x.json());
    }

    // --------------------------- for personal information-------------------
    postPersonalData(empPersonal: empPersonalInformationData) {
        let url = this.baseurl + '/empoloyeePersonalInfo';
        var body = JSON.stringify(empPersonal);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, body, requestOptions).map(x => x.json());
    }
    updatePersonalData(empPersonal: empPersonalInformationData) {
        let url = this.baseurl + '/EmployeeDetailPut';
        var body = empPersonal;
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.put(url, body).map(x => x.json());
    }
}