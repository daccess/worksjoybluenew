export class empPersonalInformationData{
    empPerId: number;
    placeOfBirth: string;
    fatheName: string;
    motherName: string;
    maritialStatus: string;
    nationality: string;
    //empKnownLanguagesList: any;
    language: any;
    bloodGroup: string;
    height: number;
    wieght: number;
    physicalDisability: string;
    physicalDisabilityDetails: string;
    healthIssues: string;
    healthIssuesDetails: string;
    regularMealPreference: string;
    anySpecialInstructionforMeal: string;
    empMealPreferencesId: number;
    empPhysicalInfoid: number;
    empFamilyMembers: any;
    fullName: any;
    firstName: string;
    middleName: string;
    lastName: string;
    filPath: string;
    fileType: string;
    fileName: string;
    logEmpOffId:any;
    dob: Date;
    hrLogId: any;
    empLogId: any;
  
    currentHlthIssue:String;
  currentHlthIssueDet: any;
  employeeStatus: any;
    }