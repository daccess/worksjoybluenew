export class QualificationModel{
    empPerId: number;
    eduQualificationId: number;
    empEductionalDataList: any;
    highestEduction: string;
    subOfSpecialisation: string;
    currentyStudying: any;
    empExperienceDetailsList: any;
    planForFutureStudy: boolean;
    planForFutureStudyDetails: string;
    ifAnyResearchPapers: boolean;
    researchPapersDetails: string;
    interestedInSports: boolean;
    sportDetails: string;
    empFutureStudyAndSportId: number;
    logEmpOffId:any;
    hrLogId: any;
    empLogId: any;
    currentStudyDetails:string
  employeeStatus: any;
}