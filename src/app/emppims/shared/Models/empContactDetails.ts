export class empContactData{
    empPerId: number;
    officialContactNumber: string;
    //cellAndEmailalternateContactNumber: string;
    officialEmailId: string;
    // personalEmailId1: string;
    // currentAddress: string;
    // permanentAddress: string;
    nameOfPerson: string;
    relation: string;
    address: string;
    // contactNumber: string;
    alternateContactNumber: string;
    // alternatecontactNumber1: string;
    skypeId: string;
    whatsAppNumber: string;
    empAddressId: any;
    empContactCellAndEmailInfoId: any;
    empEmergencyContactPersonDetailsId: any;
    empscialContactsId: any;
    buildingNoCurrent: string;
    socityOrLaneCurrent: string;
    areaCurrent: string;
    cityorTahshilCurrent: string;
    stateCurrent: string;
    pincodeCurrent: number;
    buildingNoPermant: string;
    socityOrLanePermant: string;
    areaPermant: string;
    cityOrTalukaPermant: string;
    statePermant: string;
    pinCodePermant: number;
    epersonalEmailId: string;
    emrContactNumber: any;
    emrAlternatecontactNumber: any;
    logEmpOffId:any;
    hrLogId:any;
    empLogId:any;
    personalContactNumber:any
    personalEmailId:string
  employeeStatus: any;
    
}