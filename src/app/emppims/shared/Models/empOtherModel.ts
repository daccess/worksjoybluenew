export class empOtherData{
  empOfficialId: number;
  empPerId: number;
  transportDetailsId: number;
  govDocInfoId: number;
  transportationMode: string;
  pickupPoint: string;
  droPoint: string;
  transportPurpose: string;
  selectedFutureLocation = [];
  selectedNowLocation = [];
  preferdTravelingOrNot: string;
  empBankDetailsMasterList: any;
  adharNo: string;
  areYouNri: boolean;
  citizenShip: string;
  drivingLicenseNo: string;
  panNo: string;
  passportNo: string;
  socialSecurityNo: string;
  universalAccNo: string;
  preffereTravelingOrNot: any;
  workLocationPreferenceId: number;
  empDocMasterList = [];
  logEmpOffId:any;
  hrLogId: any;
  empLogId: any;
  busStopName:string;
  busStopId:any;
  busCode:string
  busId: number;
    employeeStatus: any;
}