export class empBasicData{
    empPerId: number;
    fullName: string;
    gender: string;
    dateOfBirth: Date;
    age: number;
    personalContactNumber: string;
    personalEmailId: string;
    firstName: string;
    middleName: string;
    lastName: string;
    logEmpOffId:any;
    empLogId: any;
    hrLogId: any;
    }