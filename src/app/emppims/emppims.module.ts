import { routing } from './emppims.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { PimshrComponent } from 'src/app/pimshr/pimshr.component';
import { EmppimsComponent } from 'src/app/emppims/emppims.component';
import { EmppersonalinfoComponent } from './emppersonalinfo/emppersonalinfo.component';
import { EmpeducationComponent } from './empeducation/empeducation.component';
import { EmpdetailsComponent } from './empdetails/empdetails.component';
import { ExperiencedetailsComponent } from './experiencedetails/experiencedetails.component';
import { OtherdetailsComponent } from './otherdetails/otherdetails.component';
import { EmppimsbasicformComponent } from './emppimsbasicform/emppimsbasicform.component';

import { EmpPersonalService } from './shared/Services/empPersonalinfoservices';
import { EmpEducationExperinceService } from './shared/Services/empEducationExperince';
import { EmpContactService } from './shared/Services/empService';
import { EmpOthersService } from './shared/Services/empOthersDetails';
import { from } from 'rxjs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { UploadImageService } from './shared/Services/fileuploadservice';

@NgModule({
    declarations: [
        EmppimsComponent,
        EmppersonalinfoComponent,
        EmpeducationComponent,
        EmpdetailsComponent,
        ExperiencedetailsComponent,
        OtherdetailsComponent,
        EmppimsbasicformComponent
    ],
    imports: [
        routing,
        ToastrModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CommonModule,
        HttpClientModule,
        DataTablesModule,
        NgbModule,
        //  UploadImageService,
        NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [EmpPersonalService, EmpEducationExperinceService, EmpContactService, EmpOthersService]
})
export class EmppimsModule {
    constructor() {

    }
}
