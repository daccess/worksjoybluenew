import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class FamilyInfoService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }
    postFamilyInfoData(family) {
        let url = this.baseurl + '/empoloyeeFamilyDetails';
        var body = JSON.stringify(family);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, family, requestOptions).map(x => x.json());
    }

    putFamilyInfoData(family) {
        let url = this.baseurl + '/empoloyeeFamilyDetails';
        var body = JSON.stringify(family);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, family, requestOptions).map(x => x.json());
    }
    deleteFamilyData(id: number) {
        let url = this.baseurl + '/deleteEmpFamilyDetails';
        return this.http.delete(url + '/' + id);
      }

      getDuplicateElation(empPerId: any, relation: any){
    
        let url = this.baseurl+'/duplicateFamilyMembers?empPerId='+empPerId+'&relation='+relation;
        // var body = JSON.stringify(resignation);
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
        return this.http.get(url).map(x => x.json());
      }

}