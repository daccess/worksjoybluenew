import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmppersonalinfoComponent } from './emppersonalinfo.component';

describe('EmppersonalinfoComponent', () => {
  let component: EmppersonalinfoComponent;
  let fixture: ComponentFixture<EmppersonalinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmppersonalinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmppersonalinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
