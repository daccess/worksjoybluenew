import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../../shared/configurl';
import { empPersonalInformationData } from '../shared/Models/empPersonalInfomodel';
import { EmpContactService } from '../shared/Services/empService';
import { UploadprofilepersonalService } from '../shared/Services/uploadpersonalprofile'
import { LayoutComponent } from 'src/app/layout/layout.component';
import { Http } from '@angular/http';
import { FamilyInfoService } from './services/familyInfoservices';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-emppersonalinfo',
  templateUrl: './emppersonalinfo.component.html',
  styleUrls: ['./emppersonalinfo.component.css'],
  providers: [UploadprofilepersonalService, FamilyInfoService, DatePipe]
})
export class EmppersonalinfoComponent implements OnInit {
  @Output() someEvent = new EventEmitter<string>();
  empPersonalModel: empPersonalInformationData;
  languagemodel: any = {}
  familyModel: any = {}
  maxDate = new Date();
  allFamilyMemberList = [];
  // empKnownLanguagesList = [];
  addReourceObject: any = {}
  personalData: any;
  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  empPerId: any;
  filePath: any;
  fileName: any;
  fileType: any;
  image64: any;
  todaydate: any;
  empFamilyMembersResDtoList: any;
  empKnownLangugesResDtoList: any;
  isEmployeeEdit: any = "false";
  loggedUser: any;
  whoCliked: any;
  // sendImagedata: any;
  imageUrl: any;
  profile_Url:any;
  fileToUpload: File = null;
  sendImagedata: any;
  buttonDisabled: boolean;
  selectUndefinedOptionValue: any;
  select: any
  dob: any;
  cur: any;
  diff: any;
  age: number;
  empPerIdRole: any;
  Rolename: any;
  logempOfficialId: any;
  emplFamilyMembersId: any;
  perId: any;
  today: any;
  relationEmpPerId: any;
  resultDuplicateRelation: any;
  resultDuplicateCondition: any;
  countriesdata:any;
  currentHlthIssue:any;
  constructor(private layout: LayoutComponent, public uploadprofile: UploadprofilepersonalService,
    public empService: EmpContactService, public toastr: ToastrService, public httpService: HttpClient,
    private http: Http, public Spinner: NgxSpinnerService, public familyService: FamilyInfoService, public datepipe: DatePipe, public chRef: ChangeDetectorRef,private _sanitizer: DomSanitizer) {
    this.buttonDisabled = null;
    this.familyModel.dependancy = false
    this.todaydate = new Date().getDate() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getFullYear();
    var dte = new Date();
    dte.setDate(dte.getDate() - 6588);
    this.familyModel.dob = new Date();
    this.empPersonalModel = new empPersonalInformationData();
    this.whoCliked = sessionStorage.getItem('whoClicked');
    this.isEmployeeEdit = sessionStorage.getItem('IsEmployeeEdit');
    this.clicked();
    let user = sessionStorage.getItem('loggedUser');
    this.empPersonalModel.logEmpOffId = this.loggedUser.empOfficialId
    this.loggedUser = JSON.parse(user);
    this.empPerIdRole = this.loggedUser.empPerId;
    this.Rolename = this.loggedUser.mrollMaster.roleName;
    this.logempOfficialId = this.loggedUser.empOfficialId;
    this.familyModel.logEmpOffId = this.logempOfficialId;
    this.checkRole();
  }

  ngOnInit() {
    this.empPersonalModel.maritialStatus = 'S';
    this.empPersonalModel.physicalDisability = 'No';
    this.empPersonalModel.healthIssues = 'No';
    this.empPersonalModel.currentHlthIssue ='No';
    this.today = new Date().toJSON().split('T')[0];
  }
  checkRole() {
    if (this.loggedUser.RoleHead == 'Admin' || this.loggedUser.RoleHead == 'HR') {
      this.empPersonalModel.hrLogId = this.empPerIdRole;
      this.familyModel.hrLogId = this.empPerIdRole;
    } else {
      this.empPersonalModel.empLogId = this.empPerIdRole;
      this.familyModel.empLogId = this.empPerIdRole;
    }
  }
  uploadFile(fileLoader) {
    fileLoader.click();
  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      // this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    this.postprofileimage(this.fileToUpload);
  }
  postprofileimage(files) {
    this.ppostFile(this.fileToUpload).subscribe((data: any) => {
      if(this.Aws_flag=='true'){
        this.profile_Url = JSON.parse(data._body).result.profilePath;
        this.imageUrl = JSON.parse(data._body).result.profilePath;
      }else{
        this.profile_Url = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + JSON.parse(data._body).result.imageBase64);
        this.imageUrl=JSON.parse(data._body).result.filPath;
      }

    })

  }

  ppostFile(fileToUpload: File) {
    let url = this.baseurl + '/UploadEmpProfile';
    if(this.Aws_flag!='true'){
      url=this.baseurl + '/fileHandaling';
    }
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    formData.append('empPerId', this.empPerId);
    return this.http.post(endpoint, formData).map(x => x);;
  }

 isEditName:boolean 
  getInfo() {
    let url = this.baseurl + '/EditEmpDetails/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.personalData = data["result"];
        this.empFamilyMembersResDtoList = this.personalData["empFamilyMembersResDtoList"];
        this.empKnownLangugesResDtoList = this.personalData["empKnownLangugesResDtoList"];
        this.isEditName=true;
        this.empPersonalModel.language = this.personalData["language"];
        this.allFamilyMemberList = this.empFamilyMembersResDtoList;
        this.empPersonalModel.fullName = this.personalData["fullName"];
        this.empPersonalModel.middleName = this.personalData["middleName"];
        this.empPersonalModel.lastName = this.personalData["lastName"];
        this.empPersonalModel.placeOfBirth = this.personalData["placeOfBirth"];
        this.empPersonalModel.fatheName = this.personalData["fatheName"];
        this.empPersonalModel.motherName = this.personalData["motherName"];
        this.empPersonalModel.maritialStatus = this.personalData["maritialStatus"];
        this.empPersonalModel.nationality = this.personalData["nationality"];
        this.empPersonalModel.empPerId = this.personalData["empPerId"];
        this.empPerId = this.personalData["empPerId"];
        this.empPersonalModel.bloodGroup = this.personalData["bloodGroup"];
        this.empPersonalModel.height = this.personalData["height"];
        this.empPersonalModel.wieght = this.personalData["wieght"];
        this.empPersonalModel.currentHlthIssueDet =this.personalData["currentHlthIssueDet"]
        this.empPersonalModel.physicalDisabilityDetails = this.personalData["physicalDisabilityDetails"];
        this.empPersonalModel.healthIssuesDetails = this.personalData["healthIssuesDetails"];
        this.empPersonalModel.regularMealPreference = this.personalData["regularMealPreference"];
        this.empPersonalModel.anySpecialInstructionforMeal = this.personalData["anySpecialInstructionforMeal"];
        this.empPersonalModel.empMealPreferencesId = this.personalData["empMealPreferencesId"];
        this.empPersonalModel.empPhysicalInfoid = this.personalData["empPhysicalInfoid"];
        this.image64 = this.personalData['imageBase64'];
        if(this.Aws_flag!='true'){
          this.profile_Url=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.personalData['profilePath']);
          this.imageUrl = this.personalData["filePath"];
        }else{
          this.profile_Url = this.personalData["profilePath"];
          this.imageUrl = this.personalData["profilePath"];
        }
        this.empPersonalModel.filPath = this.personalData["profilePath"];
        this.empPersonalModel.fileType = this.personalData["profileType"];
        this.empPersonalModel.fileName = this.personalData["profileName"];
        if (this.personalData["physicalDisability"]) {
          this.empPersonalModel.physicalDisability = this.personalData["physicalDisability"];
        } else {
          this.empPersonalModel.physicalDisability = 'No'
        }
        if (this.personalData["healthIssues"]) {
          this.empPersonalModel.healthIssues = this.personalData["healthIssues"];
        }
        else {
          this.empPersonalModel.healthIssues = 'No'
        }
        if(this.personalData["currentHlthIssue"]){
          this.empPersonalModel.currentHlthIssue = this.personalData["currentHlthIssue"];
        }
        else{
          this.empPersonalModel.currentHlthIssue = 'No';
        }
      }, (err: HttpErrorResponse) => {
      });

  }

  clicked() {
    if (this.whoCliked == 'HR') {
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser;
      this.familyModel.empPerId = this.empPerId;
      this.relationEmpPerId = this.empPerId;
    } else if (this.whoCliked == "EMP") {
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
      this.familyModel.empPerId = this.empPerId;
      this.relationEmpPerId = this.empPerId;
    }
    else {
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
    }
    this.getInfo()
  }
  callParent() {
    this.someEvent.next('from Education');
  }
  temp = new Array();
  relationFlag: boolean = false;

  familyAdd(familyModel, f1) {
   
      if (this.isEdit == false) {
        this.allFamilyMemberList.push(familyModel)
        this.familyModel = {};
        setTimeout(() => {
          f1.reset();
        }, 100);
        this.toastr.success("user saved successfully")
      }
      else {
        this.allFamilyMemberList[this.editindex] = this.familyModel;
        this.isEdit = false;
        this.familyModel = {};
        this.flagduplicate=false;
        setTimeout(() => {
          f1.reset();
        }, 100);
      }
    }
   
  
  deleteFlag: boolean
  indexDelete: number = 0;
  DeletemodelEvent() {
    this.deleteFlag = false;
    this.allFamilyMemberList.splice(this.indexDelete, 1);
  }

  deletes(i) {
    this.deleteFlag = true;
    this.indexDelete = i;
  }

  reset() {
  }
  getfamilyDetails() {
    let url = this.baseurl + '/path/' + this.perId;
    this.httpService.get(url).subscribe(data => {
        this.allFamilyMemberList = data['result']
      }, (err: HttpErrorResponse) => {
        this.allFamilyMemberList = [];
      });
  }
  getdate(event) {
    this.familyModel.dob = event.target.value;
    this.calculateage(event.target.value);
    
  }
  calculateage(getvalue) {
    this.dob = new Date(this.familyModel.dob);
    //console.log("this is the selected date"+this.dob);
    this.cur = new Date();
    var diffD = Math.round((Math.abs(this.dob - this.cur) / (24* 60 * 60 * 1000))/365)
    this.age=diffD;
    console.log("this is diff between current date and selected date"+diffD)
    // this.diff = this.cur - this.dob; //This is the difference in milliseconds
    // this.age = Math.round(this.diff / 31557600000); //Divide by 1000*60*60*24*365.25
    
    // console.log("this is diff in two date selected date and current date"+this.age)
    // this.familyModel.age = this.age;
    
  }
 
  familyAddmore(f1) {
    setTimeout(() => {
      this.isEdit = false;
      this.familyModel = {};
      this.buttonDisabled = false;
    }, 1000);
  }
  isEdit: boolean = false;
  editindex: number = 0;
  edit(item, i) {
    this.buttonDisabled = false;
    this.familyModel = {};
    this.chRef.detectChanges();
    this.familyModel.name = item.name;
    this.familyModel.relation = item.relation;
    this.familyModel.dependancy = item.dependancy;
    this.familyModel.profession = item.profession;
    this.familyModel.contactNumber = item.contactNumber;
    this.familyModel.toBeCoveredUnderMediclaim = item.toBeCoveredUnderMediclaim;
    this.familyModel.dob = item.dob;
    this.familyModel.age = item.age;
    this.familyModel.emplFamilyMembersId = item.emplFamilyMembersId;
    this.isEdit = true;
    this.editindex = i;
  }
  saveAllData(empPersonalModel) {
    if (this.isEmployeeEdit == 'true') {
      this.Spinner.show();
      this.empPersonalModel.empPerId = this.empPerId;
      this.empPersonalModel.empFamilyMembers = this.allFamilyMemberList;
      this.empPersonalModel.filPath = this.imageUrl;
      this.empPersonalModel.fileName = this.fileName;
      this.empPersonalModel.fileType = this.fileType;
      this.empPersonalModel.employeeStatus = this.loggedUser.employeeStatus;
      this.empService.updatePersonalData(this.empPersonalModel).subscribe(data => {
        this.callParent();
        this.sendImagedata = this.imageUrl;
        this.layout.profilepic = this.imageUrl;
        this.loggedUser.profilePicBase64 = this.imageUrl;
        sessionStorage.setItem('loggedUser', JSON.stringify(this.loggedUser));
        this.toastr.success('Employee Personal Information Updated Successfully!', 'Employee Personal Information');
        this.Spinner.hide()
      },
        (err: HttpErrorResponse) => {
          this.toastr.error('Server Side Error..!', 'Employee Personal Information');
          this.Spinner.hide()
        });
    }
    else {
      this.Spinner.show();
      this.empPersonalModel.empPerId = this.empPerId;
      this.empPersonalModel.empFamilyMembers = this.allFamilyMemberList;
      this.empPersonalModel.filPath = this.imageUrl;
      this.empPersonalModel.fileName = this.fileName;
      this.empPersonalModel.fileType = this.fileType;
      this.empService.postPersonalData(this.empPersonalModel).subscribe(data => {
          this.sendImagedata = this.imageUrl;
          this.layout.profilepic = this.imageUrl;
          this.loggedUser.profilePicBase64 = this.imageUrl;
          this.toastr.success('Employee Personal Information Inserted Successfully!', 'Employee Personal Information');
          this.Spinner.hide()
          this.callParent();
        },
          (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Employee Personal Information');
          });
    }
  }
  flagduplicate: boolean = false;
  getRlation(event) {
     let flag = false;
    for (let i = 0; i < this.allFamilyMemberList.length; i++) {
      if (this.allFamilyMemberList[i].relation == this.familyModel.relation && this.familyModel.relation != "Brother" && this.familyModel.relation != "Sister" &&  this.familyModel.relation != "Doughter" && this.familyModel.relation != "Son" ) {
        this.familyModel.relation = this.allFamilyMemberList[i].relation;
        this.flagduplicate = true;
        flag= true; 
      }
    }
    this.flagduplicate = false
    if (flag) {
      this.flagduplicate = true;
    }
  }
}
