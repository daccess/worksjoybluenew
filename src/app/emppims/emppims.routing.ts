import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';

import { EmppimsComponent } from 'src/app/emppims/emppims.component';
import { EmppimsbasicformComponent } from './emppimsbasicform/emppimsbasicform.component';


const appRoutes: Routes = [
    {

        path: '', component: EmppimsbasicformComponent,

        children: [
            // {
            //     path:'',
            //     redirectTo : 'emppims',
            //     pathMatch :'full'

            // },

            {
                path: 'emppims',
                component: EmppimsbasicformComponent
            }

        ]

    },
    {
        path: 'empdetails',
        component: EmppimsComponent,

    }


]

// const appRoutes: Routes = [
//     { 

//         path: '', component: EmppimsbasicformComponent,

//             children:[
//                 {
//                     path:'pimsemp',
//                     component: EmppimsbasicformComponent
//                 }

//             ]
//     },
//     {
//         path : 'empdetails',
//         component : EmppimsComponent,

//     }

// ]

export const routing = RouterModule.forChild(appRoutes);