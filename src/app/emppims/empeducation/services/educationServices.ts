import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class EducationInfoService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }
    postEducationInfoData(education) {
        let url = this.baseurl + '/empoloyeeEmpExperience';
        var body = JSON.stringify(education);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, education, requestOptions).map(x => x.json());
    }

    putEducationInfoData(education) {
        let url = this.baseurl + '/empoloyeeEmpExperience';
        var body = JSON.stringify(education);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, education, requestOptions).map(x => x.json());
    }
    deleteEducationData(id: number) {
        let url = this.baseurl + '/deleteEmpExperienceDetails';
        return this.http.delete(url + '/' + id);
    }
    postEducationTableInfoData(qualification) {
        let url = this.baseurl + '/empoloyeeEducationData';
        var body = JSON.stringify(qualification);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, qualification, requestOptions).map(x => x.json());
    }
    putEducationTableInfoData(qualification) {
        let url = this.baseurl + '/empoloyeeEducationData';
        var body = JSON.stringify(qualification);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, qualification, requestOptions).map(x => x.json());
    }

    deleteEducationTableInfoData(id: number) {
        let url = this.baseurl + '/deleteEmpEducationData';
        return this.http.delete(url + '/' + id);
    }

}