import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { QualificationModel } from '../shared/Models/empQualificationdetail';
import { ToastrService } from 'ngx-toastr';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { MainURL } from '../../shared/configurl';
import { EmpContactService } from '../shared/Services/empService';
import { EducationInfoService } from './services/educationServices';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-empeducation',
  templateUrl: './empeducation.component.html',
  styleUrls: ['./empeducation.component.css'],
  providers:[EducationInfoService]
})
export class EmpeducationComponent implements OnInit {
  @Output() someEvent1 = new EventEmitter<string>();

  addExperinceDetails: any ={}
  addQualificationDetails: any ={}
  addQualification = []
  addExperince = []
  empEduExpModel: QualificationModel;
  highestEduction: string;
  currentyStudying: any;
  subOfSpecialisation: string;
  planForFutureStudy: any = false;
  planForFutureStudyDetails: any;
  ifAnyResearchPapers: any=false;
  researchPapersDetails: any;
  interestedInSports: any=false;
  sportDetails: any;
  baseurl= MainURL.HostUrl;
  empPerId: any;
  educationData: any;
  isEmployeeEdit:any="false";
  loggedUser:any;
  whoCliked:any;
  eduQualificationId: any;
  Startval: any;
  Endval: any;
  buttonDisabledexp: boolean;
  buttonDisablededu: boolean;
  slect : any
  logInUserData: any;
  empPerIdRole: any;
  Rolename: any;
  logempOfficialId: any;
  perId: any;
  experienceDetailsId: any;
  pereduId: any;
  empEductionalDataId: any;
  currentStudyDetails: string;
  constructor(public EmpEduExp: EmpContactService, 
    public toastr: ToastrService, 
    public httpService: HttpClient,
    public Spinner: NgxSpinnerService,
    public eduService: EducationInfoService) { 
    this.buttonDisabledexp = null;
    this.buttonDisablededu = null;
    this.whoCliked = sessionStorage.getItem('whoClicked');
    this.isEmployeeEdit = sessionStorage.getItem('IsEmployeeEdit');
    this.clicked();
    this.empEduExpModel = new QualificationModel();
    let logInUser = sessionStorage.getItem('loggedUser');
    this.logInUserData = JSON.parse(logInUser);
    this.empPerIdRole = this.logInUserData.empPerId;
    this.Rolename = this.logInUserData.mrollMaster.roleName;
    this.logempOfficialId = this.logInUserData.empOfficialId;
    this.addExperinceDetails.logEmpOffId = this.logempOfficialId;
    this.addQualificationDetails.logEmpOffId = this.logempOfficialId;
    this.checkRole();
    this.addQualificationDetails.passingYear = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.addExperinceDetails.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.addExperinceDetails.toDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
  }
  clicked(){
    if(this.whoCliked=='HR'){
      let user = sessionStorage.getItem('EmpData');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser;
      this.addExperinceDetails.empPerId = this.empPerId;
      this.addQualificationDetails.empPerId = this.empPerId;
    }else if(this.whoCliked=="EMP"){
      let user = sessionStorage.getItem('loggedUser');
      this.loggedUser = JSON.parse(user);
      this.empPerId = this.loggedUser.empPerId;
      this.addExperinceDetails.empPerId = this.empPerId;
      this.addQualificationDetails.empPerId = this.empPerId;
    }
    this.geteditData();
  }
  ngOnInit(){
  }
  checkRole(){
    if(this.loggedUser.RoleHead == 'Admin'||this.loggedUser.RoleHead== 'HR'){
      this.empEduExpModel.hrLogId = this.empPerIdRole;
      this.addExperinceDetails.hrLogId = this.empPerIdRole;
      this.addQualificationDetails.hrLogId = this.empPerIdRole;
     }else{
       this.empEduExpModel.empLogId = this.empPerIdRole;
       this.addExperinceDetails.empLogId = this.empPerIdRole;
       this.addQualificationDetails.empLogId = this.empPerIdRole;
     }
  }
  Startvalue(event) {
    this.Startval = event.target.value;
    this.addExperinceDetails.fromDate = this.Startval;
  }
  Endvalue(event) {
    this.Endval = event.target.value;
    this.addExperinceDetails.toDate = this.Endval;
  }
  geteditData(){
    let url = this.baseurl + '/EditEmpDetails/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.educationData = data["result"];
        this.addExperince = this.educationData["empExperienceResDetailsList"]
        this.addQualification = this.educationData["empEducationalDetailsResDtoList"]
        this.eduQualificationId = this.educationData["eduQualificationId"]
        this.highestEduction = this.educationData["highestEduction"];
        this.addQualificationDetails.highestEduction = this.educationData["highestEduction"];
        this.addQualificationDetails.subOfSpecialisation = this.educationData["subOfSpecialisation"];
        this.currentyStudying = this.educationData["currentyStudying"];
        this.subOfSpecialisation = this.educationData["subOfSpecialisation"];
        this.empEduExpModel.empFutureStudyAndSportId = this.educationData["empFutureStudyAndSportId"];
        this.planForFutureStudy = this.educationData["planForFutureStudy"];
        this.planForFutureStudyDetails = this.educationData["planForFutureStudyDetails"];
        this.ifAnyResearchPapers = this.educationData["ifAnyResearchPapers"];
        this.researchPapersDetails = this.educationData["researchPapersDetails"];
        this.interestedInSports = this.educationData["interestedInSports"];
        this.sportDetails = this.educationData["sportDetails"];
        this.empEduExpModel.logEmpOffId = this.logInUserData.empOfficialId;
      },(err: HttpErrorResponse) => {
      });
  }
  callParent() {
    this.someEvent1.next('from employee');
    }
    temp1 = new Array()
  // ===================epx  add ==============
  AddExperince(addExperinceDetails,f2){
    if(this.isEdit1 == false){
      this.addExperince.push(addExperinceDetails);
      this.addExperinceDetails = {};
      f2.reset();
    }else{
      this.addExperince[this.editindex1] = this.addExperinceDetails;
      this.isEdit1 = false;
      this.addExperinceDetails = {};
    }
  }
  getExperinceDetails(){
    let url = this.baseurl + '/path2/'+this.perId;
    this.httpService.get(url).subscribe(data => {
      this.addExperince = data['result'];
      },(err: HttpErrorResponse) => {
        this.addExperince = [];
      });
  }
  AddExperincemore(addExperinceDetails){
    this.addExperinceDetails = {}
    this.buttonDisabledexp = null;
  }
  temp = new Array()
  currentyStdy(event){
    if (event == true) {
      this.empEduExpModel.currentyStudying = 'Yes';
    } else {
      this.empEduExpModel.currentyStudying = 'No';
    }
  }
  AddQaulification(addQualificationDetails,f1){
    if(this.isEdit == false){
      this.addQualification.push(addQualificationDetails);
      this.addQualificationDetails = {};
      f1.reset();
      setTimeout(() => {
        this.addQualificationDetails.highestEduction = this.educationData["highestEduction"];
        this.addQualificationDetails.subOfSpecialisation = this.educationData["subOfSpecialisation"];
      }, 200);
    }else{
      this.addQualification[this.editindex] = this.addQualificationDetails;
      this.isEdit = false;
      this.addQualificationDetails = {};
      f1.reset();
      setTimeout(() => {
        this.addQualificationDetails.highestEduction = this.educationData["highestEduction"];
        this.addQualificationDetails.subOfSpecialisation = this.educationData["subOfSpecialisation"];
      }, 200);
    }
  }
  getEducationDetails(){
    let url = this.baseurl + '/path1/'+this.pereduId;
    this.httpService.get(url).subscribe(data => {
      this.addQualification = data['result'] 
      },(err: HttpErrorResponse) => {
        this.addQualification = [];
      });
  }
  AddQaulificationmore(addQualificationDetails,f1){
    this.addQualificationDetails = {}
    f1.reset();
    setTimeout(() => {
      this.addQualificationDetails.highestEduction = this.educationData["highestEduction"];
      this.addQualificationDetails.subOfSpecialisation = this.educationData["subOfSpecialisation"];
    }, 200);
    this.empEduExpModel.highestEduction = this.highestEduction;
    this.addQualificationDetails.highestEduction = this.educationData["highestEduction"];
    this.addQualificationDetails.subOfSpecialisation = this.educationData["subOfSpecialisation"];
    this.buttonDisablededu = null;
  }
  isEdit : boolean = false;
  editindex : number = 0;
  edit(item,i){
    this.addQualificationDetails = {};
    this.addQualificationDetails.eduQualificationId = item.eduQualificationId;
    this.addQualificationDetails.empEductionalDataId = item.empEductionalDataId;
    this.addQualificationDetails.currentyStudying = item.currentyStudying;
    this.addQualificationDetails.eduName = item.eduName;
    this.addQualificationDetails.schoolOrCollege = item.schoolOrCollege;
    this.addQualificationDetails.passingYear = item.passingYear;
    this.addQualificationDetails.grade = item.grade;
    this.addQualificationDetails.percentage = item.percentage;
    this.addQualificationDetails.univercityOrBoard = item.univercityOrBoard;
    this.addQualificationDetails.highestEduction = this.educationData["highestEduction"];
    this.addQualificationDetails.subOfSpecialisation = this.educationData["subOfSpecialisation"];
    this.isEdit = true;
    this.editindex = i;
  }

  isEdit1 : boolean = false;
  editindex1 : number = 0;
  editexp(item,i){
    this.addExperinceDetails.experienceDetailsId = item.experienceDetailsId;
    this.addExperinceDetails.companyName = item.companyName;
    this.addExperinceDetails.companyAddress = item.companyAddress;
    this.addExperinceDetails.cmpContactNumber = item.cmpContactNumber;
    this.addExperinceDetails.designationOnLeaving = item.designationOnLeaving;
    this.addExperinceDetails.companyWebsite = item.companyWebsite;
    this.addExperinceDetails.departmentWorkedIn = item.departmentWorkedIn;
    this.addExperinceDetails.jobDescription = item.jobDescription;
    this.addExperinceDetails.fromDate = item.fromDate;
    this.addExperinceDetails.toDate = item.toDate;
    this.addExperinceDetails.totalExperience = item.totalExperience;
    this.addExperinceDetails.ctc = item.ctc;
    this.addExperinceDetails.reasonForLeaving =  item.reasonForLeaving;
    this.isEdit1 = true;
    this.editindex1 = i;
  }
  deleteFlag:boolean
  indexDelete: number = 0;
  DeletemodelEvent(){
    this.deleteFlag = false;
    this.addQualification.splice(this.indexDelete, 1);
  }
  deletes(i){
    this.deleteFlag = true;
    this.indexDelete = i;
  }
  // ========================delete for Experience data start =================
  deleteFlag1:boolean
  indexDelete1: number = 0;
   DeletemodelEvent2(){
    this.deleteFlag1=false
    this.addExperince.splice(this.indexDelete1, 1);
  }
  deleteexp(i){
    this.deleteFlag1 = true;
    this.indexDelete1 = i;
  }
  passingYearfunc(event){
    this.addQualificationDetails.passingYear = event;
  }
  saveAllData(){
  if(this.isEmployeeEdit=='true'){
    this.Spinner.show()
    this.empEduExpModel.empPerId = this.empPerId;
    this.empEduExpModel.highestEduction = this.highestEduction;
    this.empEduExpModel.currentyStudying = this.currentyStudying;
    this.empEduExpModel.currentStudyDetails = this.currentStudyDetails;
    this.empEduExpModel.subOfSpecialisation = this.subOfSpecialisation;
    this.empEduExpModel.planForFutureStudy = this.planForFutureStudy;
    this.empEduExpModel.planForFutureStudyDetails = this.planForFutureStudyDetails;
    this.empEduExpModel.ifAnyResearchPapers = this.ifAnyResearchPapers;
    this.empEduExpModel.researchPapersDetails = this.researchPapersDetails;
    this.empEduExpModel.interestedInSports = this.interestedInSports;
    this.empEduExpModel.sportDetails = this.sportDetails;
    this.empEduExpModel.empEductionalDataList = this.addQualification;
    this.empEduExpModel.empExperienceDetailsList = this.addExperince;
    this.empEduExpModel.eduQualificationId = this.eduQualificationId;
    this.empEduExpModel.employeeStatus = this.loggedUser.employeeStatus;
    let url1 = this.baseurl + '/educationAndExperinceInfo';
    this.httpService.put(url1,this.empEduExpModel).subscribe(data => {
      this.Spinner.hide()
      this.toastr.success('Education Information Updated Successfully!');
      this.callParent();
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!');
    });
  }
  else{
    this.Spinner.show();
    this.empEduExpModel.empPerId = this.empPerId;
    this.empEduExpModel.highestEduction = this.highestEduction;
    this.empEduExpModel.currentyStudying = this.currentyStudying;
    this.empEduExpModel.currentStudyDetails = this.currentStudyDetails;
    this.empEduExpModel.subOfSpecialisation = this.subOfSpecialisation;
    this.empEduExpModel.planForFutureStudy = this.planForFutureStudy;
    this.empEduExpModel.planForFutureStudyDetails = this.planForFutureStudyDetails;
    this.empEduExpModel.ifAnyResearchPapers = this.ifAnyResearchPapers;
    this.empEduExpModel.researchPapersDetails = this.researchPapersDetails;
    this.empEduExpModel.interestedInSports = this.interestedInSports;
    this.empEduExpModel.sportDetails = this.sportDetails;
    this.empEduExpModel.empEductionalDataList = this.addQualification;
    this.empEduExpModel.empExperienceDetailsList = this.addExperince;
    this.EmpEduExp.postExpEduData(this.empEduExpModel).subscribe(data => {
      this.Spinner.hide();
      this.toastr.success('Qualification & Experince Information Inserted Successfully!');
      this.callParent();
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!');
    });
  }
   
}

}
