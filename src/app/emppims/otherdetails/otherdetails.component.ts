import { Component, OnInit, ElementRef, Sanitizer } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from 'src/app/shared/configurl';
import { ToastrService } from 'ngx-toastr';

import { EmpOthersService } from '../shared/Services/empOthersDetails';
import { empOtherData } from '../shared/Models/empOtherModel';
import { EmpContactService } from '../shared/Services/empService';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UploadImageService } from '../../emppims/shared/Services/fileuploadservice';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { OtherTableDetailInfoService } from './services/otherAllTableServices';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-otherdetails',
    templateUrl: './otherdetails.component.html',
    styleUrls: ['./otherdetails.component.css'],
    providers: [UploadImageService, OtherTableDetailInfoService]
})
export class OtherdetailsComponent implements OnInit {
    documentMasterId: number;
    ListImageData: any = []
    slected: string
    imageUrl: string = "/assets/img/default-image.png";
    fileToUpload: File = null;
    filesData = new Array();
    flag: boolean = false;
    fileInfo: any = { id: "", filename: "", fileURL: "", data: "", docUrl:""}
    obj: any;
    base64URL: any;
    closeResult: string;
    fileName: string = "";
    addBankDetails: any = {}
    locationdata: any
    compId: any;
    baseurl = MainURL.HostUrl;
    Selected: any;
    selectUndefinedOptionValue: any;
    empBankDetailsMasterList = [];
    dropdownSettingsLocation: {
        singleSelection: boolean;
        idField: string;
        textField: string;
        selectAllText: string;
        unSelectAllText: string;
        itemsShowLimit: number;
        allowSearchFilter: boolean;
    };
    selectedLocation: any = [];
    dropdownSettingsLocation1: {
        singleSelection: boolean;
        idField: string;
        textField: string;
        selectAllText: string;
        unSelectAllText: string;
        itemsShowLimit: number;
        allowSearchFilter: boolean;
    };
    selectedLocation1: any = [];
    otherModel: empOtherData;
    empPerId: any;
    othersData: any = [];
    isEmployeeEdit: any = "false";
    loggedUser: any;
    whoCliked: any;
    empOfficialId: any;
    // workLocationNowPreferancesResDtos: any;
    buttonDisabled: boolean;
    loggedUserData: any;
    empPerIdRole: any;
    Rolename: any;
    logempOfficialId: any;
    perId: any;
    bankDetailsId: any;
    fileUpload: any = {};
    empDocId: any;
    documentdata: any;
    fileNamee: any;
    fileList: any;
    locationId:any;
    constructor(public httpService: HttpClient,
        private router: Router,
        public empotherserve: EmpContactService,
        public toastr: ToastrService,
        private modalService: NgbModal,
        private sanitizer: DomSanitizer,
        private imageService: UploadImageService,
        public Spinner: NgxSpinnerService,
        public otherTableServices: OtherTableDetailInfoService) {
        this.otherModel = new empOtherData();
        this.whoCliked = sessionStorage.getItem('whoClicked')
        this.isEmployeeEdit = sessionStorage.getItem('IsEmployeeEdit')
        let userData = sessionStorage.getItem('loggedUser');
        this.loggedUserData = JSON.parse(userData);
        this.compId = this.loggedUserData.compId;
        this.logempOfficialId = this.loggedUserData.empOfficialId;
        this.addBankDetails.logEmpOffId = this.logempOfficialId;
        this.fileUpload.logEmpOffId = this.logempOfficialId;
        this.addBankDetails.empOfficialId = this.logempOfficialId;
        this.empPerIdRole = this.loggedUserData.empPerId;
        this.Rolename = this.loggedUserData.mrollMaster.roleName;
        this.checkRole();
        this.clicked();
        this.getDocumentList();
        this.fileInfo.documentMasterId = "Selectdoc";
        this.otherModel.preferdTravelingOrNot = "selectUndefinedOptionValue";
        this.locationId=this.loggedUserData.locationId;
    }
    ngOnInit() {

    }
    checkRole() {
        if (this.loggedUser.RoleHead== 'Admin'|| this.loggedUser.RoleHead == 'HR') {
            this.otherModel.hrLogId = this.empPerIdRole;
            this.addBankDetails.hrLogId = this.empPerIdRole;
            this.fileUpload.hrLogId = this.empPerIdRole;
            this.perId = this.empPerIdRole;
        } else {
            this.otherModel.empLogId = this.empPerIdRole;
            this.addBankDetails.empLogId = this.empPerIdRole;
            this.fileUpload.empLogId = this.empPerIdRole;
            this.perId = this.empPerIdRole;
        }

    }
    hidebuttn: boolean = false;
    flagAdhar: boolean;
    getAdharData = {};
    EditFlagsAdhar: any
    getDuplicateAdhar(adharNo) {
        this.hidebuttn = false;
        if (this.EditFlagsAdhar = 'true') {
            if (this.othersData['adharNo'] != adharNo) {
                this.flagAdhar = false;
                let url1 = this.baseurl + '/duplicateAdhar?adharNo=' + adharNo + '&compId=' + this.compId;
                this.httpService.get(url1).subscribe((data: any) => {
                    this.flagAdhar = true;
                    this.hidebuttn = true;
                    this.getAdharData = data;
                }, (err: HttpErrorResponse) => {
                    this.hidebuttn = false;
                });
            }
        }
    }
    /*To check Duplicate passport Numbers*/
    flagPass: boolean
    EditFlagpass: any
    getPassPortNo = { message: "" };
    getDuplicatePassPortNoList(passportNo) {
        this.flagPass = false
        this.hidebuttn = false;
        if (this.EditFlagpass = 'true') {
            if (this.othersData['passportNo'] != passportNo) {
                let url1 = this.baseurl + '/duplicatePassportNo?compId=' + this.compId + '&passportNo=' + passportNo;
                this.httpService.get(url1).subscribe((data: any) => {
                    this.flagPass = true;
                    this.getPassPortNo = data;
                    this.hidebuttn = true;
                }, (err: HttpErrorResponse) => {
                    this.flagPass = false
                    this.hidebuttn = false;
                });
            }
        }
    }
    /*To check Duplicates entries of driving licence */
    flagDL: boolean
    EditFlagDL: any
    getDrivingLicenceNo = { message: "" };
    getDuplicateDLNoList(drivingLicenceNo) {
        this.flagDL = false
        this.hidebuttn = false;
        if (this.EditFlagDL = 'true') {
            if (this.othersData['drivingLicenceNo'] != drivingLicenceNo) {
                let url1 = this.baseurl + '/duplicateEmpLicenceNo?compId=' + this.compId + '&licenceNo=' + drivingLicenceNo;
                this.httpService.get(url1).subscribe((data: any) => {
                this.flagDL = true;
                this.getDrivingLicenceNo = data;
                this.hidebuttn = true;
            }, (err: HttpErrorResponse) => {
                this.flagDL = false
                this.hidebuttn = false;
            });
            }
        }
    }
    flagAC: boolean;
    getAccountNo = { massege: "" };
    item: any = {};
    getDuplicateBankACNo(accountNumber) {
        this.flagAC = false;
        if (this.isEdit1) {
            if (this.bankItem['accountNumber'] != accountNumber) {
                let url1 = this.baseurl + '/duplicateBankAcc?accountNumber=' + accountNumber + '&compId=' + this.compId;
                this.httpService.get(url1).subscribe((data: any) => {
                this.flagAC = true;
                this.getAccountNo = data;
                this.hidebuttn = true;
                }, (err: HttpErrorResponse) => {
                    this.flagAC = false
                    this.hidebuttn = false;
                });
            }
        }
        else {
            let url1 = this.baseurl + '/duplicateBankAcc?accountNumber=' + accountNumber + '&compId=' + this.compId;
            this.httpService.get(url1).subscribe((data: any) => {
                    this.flagAC = true;
                    this.getAccountNo = data;
                }, (err: HttpErrorResponse) => {
                    this.flagAC = false
                    this.hidebuttn = false;
                });
        }
    }
    EditFlagsPan: any;
    flagPan: boolean;
    getPanData = {};
    getDuplicatePan(panNo) {
        if (this.EditFlagsPan = 'true') {
            if (this.othersData['panNo'] != panNo) {
                this.flagPan = false
                this.hidebuttn = false;
                let url1 = this.baseurl + '/duplicatePan?compId=' + this.compId + '&panNo=' + panNo;
                this.httpService.get(url1).subscribe((data: any) => {
                this.flagPan = true;
                this.hidebuttn = true;
                this.getPanData = data;
                }, (err: HttpErrorResponse) => {
                    this.flagPan = false;
                    this.hidebuttn = false;
                });
            }
        }

    }
    EditFlagsUAN: any;
    flagUAN: boolean
    getUANData: {};
    getDuplicateUAN(universalAccNo) {
        if (this.EditFlagsUAN = 'true') {
            if (this.othersData['universalAccNo'] != universalAccNo) {
                this.flagUAN = false;
                this.hidebuttn = false;
                let url1 = this.baseurl + "/duplicateuan?compId=" + this.compId + "&universalAccNo=" + universalAccNo;
                this.httpService.get(url1).subscribe((data: any) => {
                this.flagUAN = true;
                this.hidebuttn = true;
                this.getUANData = data;
                }, (err: HttpErrorResponse) => {
                this.hidebuttn = false;
                });
            }
        }
    }
    getInfo() {
        let url1 = this.baseurl + '/Location/Active/';
        this.httpService.get(url1 + this.compId).subscribe(data => {
            this.locationdata = data["result"];
            },
            (err: HttpErrorResponse) => {
            });
        this.dropdownSettingsLocation = {
            singleSelection: false,
            idField: 'locationId',
            textField: 'locationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 5,
            allowSearchFilter: true
        };
        this.dropdownSettingsLocation1 = {
            singleSelection: false,
            idField: 'locationId',
            textField: 'locationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 5,
            allowSearchFilter: true
        };
        let url = this.baseurl + '/EditEmpDetails/';
        this.httpService.get(url + this.empPerId).subscribe(data => {
            this.othersData = data["result"];
            if(this.othersData["transportationMode"]){
                this.otherModel.transportationMode = this.othersData["transportationMode"];
            }
            this.otherModel.transportationMode = this.othersData["transportationMode"];
            this.otherModel.pickupPoint = this.othersData["pickupPoint"];
            this.otherModel.droPoint = this.othersData["droPoint"];
            this.otherModel.transportPurpose = this.othersData["transportPurpose"];
            this.otherModel.selectedNowLocation = this.othersData["workLocationNowPreferancesResDtos"];
            this.otherModel.selectedFutureLocation = this.othersData["workLocationFuturePreferancesResDtos"];
            this.otherModel.preferdTravelingOrNot = this.othersData["preferdTravelingOrNot"];
            this.otherModel.panNo = this.othersData["panNo"];
            this.otherModel.adharNo = this.othersData["adharNo"];
            this.otherModel.universalAccNo = this.othersData["universalAccNo"];
            this.otherModel.drivingLicenseNo = this.othersData["drivingLicenseNo"];
            this.otherModel.passportNo = this.othersData["passportNo"];
            this.otherModel.socialSecurityNo = this.othersData["socialSecurityNo"];
            this.otherModel.citizenShip = this.othersData["citizenShip"]
            this.otherModel.areYouNri = this.othersData["areYouNri"];
            this.empBankDetailsMasterList = this.othersData["empBankDetailsResDtoList"];
            this.otherModel.empOfficialId = this.othersData["empOfficialId"];
            // alert(this.otherModel.empOfficialId);
            this.otherModel.empPerId = this.othersData["empPerId"];
            this.otherModel.transportDetailsId = this.othersData["transportDetailsId"];
            this.otherModel.govDocInfoId = this.othersData["govDocInfoId"];
            this.otherModel.workLocationPreferenceId = this.othersData["workLocationPreferenceId"]
            this.otherModel.empDocMasterList = this.othersData["empDocMasterList"]
            this.filesData = this.otherModel.empDocMasterList;

            for (let i = 0; i < this.filesData.length; i++) {
                var lastIndex = this.filesData[i].docUrl.lastIndexOf('-');
                this.filesData[i].fileName = this.filesData[i].docUrl.substr(lastIndex + 1);
            }
            this.selectedLocation = this.otherModel.selectedNowLocation;
            this.selectedLocation1 = this.otherModel.selectedFutureLocation;

        }, (err: HttpErrorResponse) => {
        });
    }
    getDocumentList() {
        let url = this.baseurl + '/active/activeDocumentdata/';
        this.httpService.get(url + this.compId).subscribe(data => {
        this.documentdata = data["result"];
        }, (err: HttpErrorResponse) => {

        });
    }
    clicked() {
        if (this.whoCliked == 'HR') {
            let user = sessionStorage.getItem('EmpData');
            this.loggedUser = JSON.parse(user);
            this.empPerId = this.loggedUser;
            this.addBankDetails.empPerId = this.empPerId;
            this.fileUpload.empPerId = this.empPerId;
        } else if (this.whoCliked == "EMP") {
            let user = sessionStorage.getItem('loggedUser');
            this.loggedUser = JSON.parse(user);
            this.empPerId = this.loggedUser.empPerId;
            this.empOfficialId = this.loggedUserData.empOfficialId;
            this.addBankDetails.empPerId = this.empPerId;
            this.fileUpload.empPerId = this.empPerId;
        }
        this.getInfo()
    }
    //==========================Location Start=========================
    onItemSelectL(item: any) {
        if (this.selectedLocation.length == 0) {
            this.selectedLocation.push(item)
        } else {
            this.selectedLocation.push(item)
        }
        if (this.selectedLocation.length > 0) {
        }
    }
    onSelectAllL(items: any) {
        this.selectedLocation = items;
    }
    OnItemDeSelectL(item: any) {
        for (var i = 0; i < this.selectedLocation.length; i++) {
            if (this.selectedLocation[i].locationId == item.locationId) {
                this.selectedLocation.splice(i, 1)
            }
        }
    }
    onDeSelectAllL(item: any) {
        this.selectedLocation = [];
    }
    onItemSelectL1(item: any) {
        if (this.selectedLocation1.length == 0) {
            this.selectedLocation1.push(item)
        } else {
            this.selectedLocation1.push(item)
        }
    }
    onSelectAllL1(items: any) {
        this.selectedLocation1 = items;
    }
    OnItemDeSelectL1(item: any) {
        for (var i = 0; i < this.selectedLocation1.length; i++) {
            if (this.selectedLocation1[i].locationId == item.locationId) {
                this.selectedLocation1.splice(i, 1)
            }
        }
    }
    onDeSelectAllL1(item: any) {
        this.selectedLocation1 = [];
    }
    temp1 = new Array()
    AddBankDetails(addBankDetails, f2) {
        if (this.isEdit1 == false) {
            this.empBankDetailsMasterList.push(addBankDetails);
            this.addBankDetails = {};
            f2.reset();
        } else {
            this.empBankDetailsMasterList[this.editindex1] = this.addBankDetails;
            this.isEdit1 = false;
            this.addBankDetails = {};
            f2.reset();
        }
    }
    deleteFlag: boolean
    indexDelete: number = 0;
    DeletemodelEvent() {
        this.deleteFlag = false;
        this.empBankDetailsMasterList.splice(this.indexDelete, 1);
    }
    deletebank(i) {
        this.deleteFlag = true;
        this.indexDelete = i;
    }
    getListBankDeatils() {
        let url = this.baseurl + '/path3/' + this.perId;
        this.httpService.get(url).subscribe(data => {
            this.empBankDetailsMasterList = data['result']
        }, (err: HttpErrorResponse) => {
            this.empBankDetailsMasterList = [];
        });
    }
    AddBankDetailsmore(addBankDetails) {
        this.addBankDetails.nameOfAccHolder = null;
        this.addBankDetails.bankName = null;
        this.addBankDetails.accType = null;
        this.addBankDetails.branchAddr = null;
        this.addBankDetails.accountNumber = null;
        this.addBankDetails.ifscCode = null;
        this.addBankDetails.city = null;
        this.addBankDetails.swiftCode = null;
        this.addBankDetails.creditcard = null;
    }
    storeInfo() {
        if (this.isEmployeeEdit == 'true') {
            this.updateRecord();
        } else {
            this.saveAllData();
        }
    }
    saveAllData() {
        this.Spinner.show();
        this.otherModel.empPerId = this.empPerId;
        this.otherModel.empOfficialId = this.empOfficialId;
        this.otherModel.selectedFutureLocation = this.selectedLocation1;
        this.otherModel.selectedNowLocation = this.selectedLocation;
        this.otherModel.empBankDetailsMasterList = this.empBankDetailsMasterList;
        this.otherModel.empDocMasterList = this.filesData;
        this.empotherserve.postEmpOthersData(this.otherModel).subscribe(data => {
            this.toastr.success('Other Information Inserted Successfully!', 'Employee Other Information');
            if(this.loggedUser.employeeStatus){
                sessionStorage.clear();
                this.router.navigateByUrl('');
            }
            else{
                this.router.navigateByUrl('/layout/home');
            }
            this.Spinner.hide();
        },
        (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Employee Other Information');
        });
    }

    updateRecord() {
        this.Spinner.show();
        this.otherModel.empPerId = this.empPerId;
        this.otherModel.selectedFutureLocation = this.selectedLocation1;
        this.otherModel.selectedNowLocation = this.selectedLocation;
        this.otherModel.empBankDetailsMasterList = this.empBankDetailsMasterList;
        this.otherModel.empDocMasterList = this.filesData;
        this.otherModel.employeeStatus = this.loggedUser.employeeStatus;
        let url1 = this.baseurl + '/empOtherInfoDetail';
        this.httpService.put(url1, this.otherModel).subscribe(data => {
            this.Spinner.hide();
            this.toastr.success(' Please Note Data will Go for HR Verification after approval your data will get reflect !!', 'Alert');
            if(this.loggedUser.employeeStatus){
                sessionStorage.clear();
                this.router.navigateByUrl('');
            }
            else{
                this.router.navigateByUrl('/layout/home');
            }
        },
        (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Other Information');
        });
    }
    fileNameFlag: boolean = false;
    handleFileInput(file: FileList) {
        this.fileToUpload = file.item(0);
        this.fileNamee = file.item(0).name;
        this.fileInfo.fileName = file.item(0).name;
        //Show image preview
        this.fileNameFlag = true
        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.imageUrl = event.target.result;
        }
        reader.readAsDataURL(this.fileToUpload);
    }
    uploadFile() {
        this.imageService.uploadFile(this.fileToUpload, this.fileInfo.fileName).subscribe(data => {
            this.ListImageData.push(data['result']);
            // Image.value = null;
            this.filesData = this.ListImageData;
            for (let i = 0; i < this.filesData.length; i++) {
                var lastIndex = this.filesData[i].docUrl.lastIndexOf('-');
                this.filesData[i].fileName = this.filesData[i].docUrl.substr(lastIndex + 1);
            }
            this.Spinner.hide();
        });
    }
    isFileEdit: boolean = false;
    OnSubmit(Image) {
        if (this.isFileEdit) {
            this.fileNameFlag = false;
            this.flag = true;
            this.isFileEdit = false;
            this.uploadDocuments1();
        }
        else {
            this.flag = true;
            this.uploadDocuments();
        }
    }
    DeletemodelEvent11(){
        this.deleteFlag = false;
        this.filesData.splice(this.indexDelete,1);
    }
    deleteDoc(i){
        this.deleteFlag = true;
        this.indexDelete = i 
    }
    view(i) {
        this.base64URL = this.sanitizer.bypassSecurityTrustResourceUrl(i);
    }
    MyDocobjUrl:any;
    MyDocobjName:any
    uploadDocuments() {
        this.Spinner.show();
        this.imageService.uploadFile(this.fileToUpload, this.fileInfo.docName).subscribe((data : any) => {
            this.fileInfo.docName = null;
            this.fileInfo.docUrl = data['result']['docUrl'];
            this.fileInfo.docUrl = data.result.docUrl;
            this.fileInfo.docType = data['result']['docType'];
            this.toastr.success('Upload Document Successfully!', 'Document Upload');
            this.Spinner.hide();
        },
        (err: HttpErrorResponse) => {
            this.toastr.error('Server Side Error..!', 'Document Upload');
            this.Spinner.hide()
        });
    }
    uploadDocuments1() {
        this.Spinner.show();
        this.imageService.uploadFile(this.fileToUpload, this.fileInfo.docName).subscribe(data => {
            this.fileInfo.docName = null;
            this.fileInfo.docUrl = data['result']['docUrl'];
            this.fileInfo.docType = data['result']['docType'];
            this.fileInfo.docName = data['result']['docName'];
            this.toastr.success('Update Document Successfully!', 'Document Upload');
            this.Spinner.hide()
        },
        (err: HttpErrorResponse) => {
            this.toastr.error('Server Side Error..!', 'Document Upload');
            this.Spinner.hide()
        });
    }
    selectedCompanyobj: any;
    saveDocInfo() {
        for (let i = 0; i < this.documentdata.length; i++) {
            if(this.documentdata[i].documentMasterId == this.fileInfo.documentMasterId){
                this.fileInfo.docName = this.documentdata[i].documentName;
            }
        }
        if(!this.filesData){
            this.filesData = new Array();
        }
        if(this.isFileEdit){
            this.filesData[this.docIndex] = this.fileInfo;
            this.fileInfo = {};
            this.fileInfo.documentMasterId = "Selectdoc";
            this.fileInfo.docName = null;
            this.fileNameFlag = false
        }
        else{
            this.filesData.push(this.fileInfo);
            this.fileInfo = {};
            this.fileInfo.documentMasterId = "Selectdoc";
            this.fileInfo.docName = "";
            this.fileNameFlag = false
        }
    }
    docUrl: any;
    docFlag: boolean = true;
    getDocInfoData() {
        this.docFlag = false;
        let url = this.baseurl + '/path4/' + this.perId;
        this.httpService.get(url).subscribe(data => {
        if (data['result']) {
            this.filesData = data['result'];
            for (let i = 0; i < this.filesData.length; i++) {
                var lastIndex = this.filesData[i].docUrl.lastIndexOf('-');
                this.filesData[i].fileName = this.filesData[i].docUrl.substr(lastIndex + 1);
            }
            this.fileName = data['result'].docType;
            this.docFlag = true;
            if (this.docUrl) {
                this.fileList = this.docUrl.substring(0, this.docUrl.lastIndexOf("/") + 1);
            }
        }
        }, (err: HttpErrorResponse) => {
            this.docFlag = true;
            this.filesData = [];
        });
    }
    fileType
    open(item) {
        this.base64URL = null;
        this.base64URL = this.sanitizer.bypassSecurityTrustResourceUrl(item.docUrl);
        this.fileName = item.docName;
        this.fileType = item.docType;
        if (this.fileType != 'image/png' && this.fileType != 'image/jpeg') {
            window.open(item.docUrl, '_blank');
        }
    }
    reset() {
        this.fileUpload.fileName = null;
    }
    index: number;
    docIndex : number;
    edit(i,index) {
        this.fileNameFlag = true;
        this.fileUpload.empDocId = i.empDocId;
        this.fileInfo.docName = i.docName;
        this.fileUpload.documentMasterId = i.documentMasterId;
        this.fileInfo.documentMasterId = i.documentMasterId;
        this.fileUpload.docUrl = i.docUrl;
        this.fileInfo.docUrl = i.docUrl;
        this.fileUpload.fileNamee = "";
        this.fileUpload.docType = i.docType;
        this.fileInfo.docType = i.docType;
        this.isFileEdit = true;
        this.docIndex = index;
        let fileNamee = i.docUrl.lastIndexOf('/');
        this.fileNamee = i.docUrl.substr(fileNamee + 1);
        this.fileUpload.docUrl = this.fileNamee;
    }
    isEdit1: boolean = false;
    editindex1: number = 0;
    bankItem: any = {};
    editbank(item, i) {
        // this.addBankDetails = {}
        this.addBankDetails.bankDetailsId = item.bankDetailsId;
        this.addBankDetails.nameOfAccHolder = item.nameOfAccHolder;
        this.addBankDetails.bankName = item.bankName;
        this.addBankDetails.accType = item.accType;
        this.addBankDetails.branchAddr = item.branchAddr;
        this.addBankDetails.accountNumber = item.accountNumber;
        this.addBankDetails.ifscCode = item.ifscCode;
        this.addBankDetails.city = item.city;
        this.addBankDetails.swiftCode = item.swiftCode;
        this.addBankDetails.creditcard = item.creditcard
        this.bankItem = item;
        this.isEdit1 = true;
        this.editindex1 = i;
    }
    hide: boolean = false;
    transportevent(event) {
        if (event == 'Company Bus') {
            this.hide = true
        }
        else {
            this.hide = false
        }
    }
}