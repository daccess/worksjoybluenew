import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../../shared/configurl';

@Injectable()
export class OtherTableDetailInfoService {

    baseurl = MainURL.HostUrl;

    constructor(private http: Http) {

    }
    postBankInfoData(bank) {
        let url = this.baseurl + '/empoloyeeBankData';
        var body = JSON.stringify(bank);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, bank, requestOptions).map(x => x.json());
    }

    putBankInfoData(bank) {
        let url = this.baseurl + '/empoloyeeBankData';
        var body = JSON.stringify(bank);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, bank, requestOptions).map(x => x.json());
    }
    deleteBankData(id: number) {
        let url = this.baseurl + '/deleteEmpBankDetails';
        return this.http.delete(url + '/' + id);
      }

      postDocInfoData(Doc) {
        let url = this.baseurl + '/empoloyeeDocInfo ';
        var body = JSON.stringify(Doc);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(url, Doc, requestOptions).map(x => x.json());
    }

    deleteDocData(id: number) {
        let url = this.baseurl + '/deleteEmpDocMaster';
        return this.http.delete(url + '/' + id);
      }

}