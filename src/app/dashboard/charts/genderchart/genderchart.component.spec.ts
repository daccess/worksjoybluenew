import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenderchartComponent } from './genderchart.component';

describe('GenderchartComponent', () => {
  let component: GenderchartComponent;
  let fixture: ComponentFixture<GenderchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenderchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenderchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
