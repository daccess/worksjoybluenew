import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';
@Component({
  selector: 'app-genderchart',
  templateUrl: './genderchart.component.html',
  styleUrls: ['./genderchart.component.css']
})
export class GenderchartComponent implements OnInit {

  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  public pieChartType: string = 'pie';

  flag : boolean = false;
  displayData : any = [];
  loggedUser: any;
  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    service.genderDiversity(this.loggedUser.compId).subscribe(data=>{
      let labels = new Array();
      let category = new Array();
      for (let i = 0; i< data.result.length; i++) {
          labels.push(data.result[i].gender);
          category.push(data.result[i].genderCnt);
          let obj = {
              label : data.result[i].gender,
              category : data.result[i].genderCnt
          }
          this.displayData.push(obj)
      }

      this.pieChartData = category;
      this.pieChartLabels = labels;
      this.flag = true;
  },err=>{
    this.pieChartData = [];
    this.pieChartLabels = [];
    this.flag = true;
  })
   }

  ngOnInit() {
  }

  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
}
