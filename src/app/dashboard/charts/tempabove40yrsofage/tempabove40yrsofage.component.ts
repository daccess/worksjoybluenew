import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-tempabove40yrsofage',
  templateUrl: './tempabove40yrsofage.component.html',
  styleUrls: ['./tempabove40yrsofage.component.css']
})
export class Tempabove40yrsofageComponent implements OnInit {
  loggedUser: any;

  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    service.TempEmpAbove40(this.loggedUser.compId).subscribe(data=>{
      // this.barChartData = [
      //   { data: data.result, label: 'Fixed' },
      //   // {data: [344, 23, 0, 14], label: 'Temp'}
      // ];
    })
   }


  ngOnInit() {
  }
  public barChartLabels: string[] = ['Assembly-Comber', 'Assembly-Head and foot stock', 'Assembly-section assembly', 'FGAI & packing', 'Finance-Finance(Excise)', 'Machining cubical', 'Machining round-Bottom', 'Operational Excellence', 'Parts Logistics', 'Sheet Metal-Bending', 'Sheet Metal-Laser', 'Sheet Metal-Powder Coating', 'Sheet Metal-Welding', 'Top roller assembly', 'Top roller Machining', 'Internal Logistics', 'Repair Services(ELO)', 'Installation', 'Supply managment(Supplier)'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  barChartOptions = [];
  public barChartData: any[] = [
    { data: [1, 1, 1, 3, 0, 2, 3, 1, 0, 2, 1, 10, 3, 2, 1, 4, 2, 2, 1], label: 'Fixed' },
    // {data: [344, 23, 0, 14], label: 'Temp'}
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#4C680A",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    // { // grey

    //   backgroundColor: '#4a96d5',
    //   borderColor: '#fff',
    //   pointBackgroundColor: 'blue',
    //   pointBorderColor: '#fff',
    //   pointHoverBackgroundColor: '#fff',
    //   pointHoverBorderColor: '#fff'
    // }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
