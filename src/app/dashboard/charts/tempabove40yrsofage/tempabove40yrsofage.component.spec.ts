import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tempabove40yrsofageComponent } from './tempabove40yrsofage.component';

describe('Tempabove40yrsofageComponent', () => {
  let component: Tempabove40yrsofageComponent;
  let fixture: ComponentFixture<Tempabove40yrsofageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tempabove40yrsofageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Tempabove40yrsofageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
