import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectvsindirectComponent } from './directvsindirect.component';

describe('DirectvsindirectComponent', () => {
  let component: DirectvsindirectComponent;
  let fixture: ComponentFixture<DirectvsindirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectvsindirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectvsindirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
