import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-agegroupwisefixedemponly',
  templateUrl: './agegroupwisefixedemponly.component.html',
  styleUrls: ['./agegroupwisefixedemponly.component.css']
})
export class AgegroupwisefixedemponlyComponent implements OnInit {
  barChartOptions: any;
  loggedUser: any;
  chartFlag: boolean = false;
  constructor(private service: DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    service.AgeGroupWiseTotalCountOfEmp(this.loggedUser.compId).subscribe((data: any) => {
      let ageBet18_20 = data.result.ageBet18_20;
      let ageBet21_25 = data.result.ageBet21_25;
      let ageBet26_30 = data.result.ageBet26_30;
      let ageBet31_35 = data.result.ageBet31_35;
      let ageBet36_40 = data.result.ageBet36_40;
      let ageBet41_45 = data.result.ageBet41_45;
      let ageBet46_50 = data.result.ageBet46_50;
      let ageBet51_55 = data.result.ageBet51_55;
      let ageGreater55 = data.result.ageGreater55;

      let obj = [
        ageBet18_20,
        ageBet21_25,
        ageBet26_30,
        ageBet31_35,
        ageBet36_40,
        ageBet41_45,
        ageBet46_50,
        ageBet51_55,
        ageGreater55
      ]
      this.barChartData = [
        { data: obj, label: 'Fixed' },
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartData = [
        { data: [], label: 'Fixed' },
      ];
      this.chartFlag = true;
    })
  }

  ngOnInit() {
  }
  public barChartLabels: string[] = ['18-20 years', '21-25 years', '26-30 years', '31-35 years', '36-40 years', '41-45 years', '46-50 years', '51-55 years', 'more than 55 years'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Fixed' },
  ];

  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    {
      backgroundColor: "#09275A",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },

  ]

  public randomize(): void {
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
