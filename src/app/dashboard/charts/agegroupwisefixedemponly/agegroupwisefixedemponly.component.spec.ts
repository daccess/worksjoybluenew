import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgegroupwisefixedemponlyComponent } from './agegroupwisefixedemponly.component';

describe('AgegroupwisefixedemponlyComponent', () => {
  let component: AgegroupwisefixedemponlyComponent;
  let fixture: ComponentFixture<AgegroupwisefixedemponlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgegroupwisefixedemponlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgegroupwisefixedemponlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
