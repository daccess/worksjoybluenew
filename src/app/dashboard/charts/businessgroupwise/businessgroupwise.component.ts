import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-businessgroupwise',
  templateUrl: './businessgroupwise.component.html',
  styleUrls: ['./businessgroupwise.component.css']
})
export class BusinessgroupwiseComponent implements OnInit {
  barChartOptions: any;
  loggedUser: any;
  chartFlag: boolean = true;
  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.chartFlag = false;
    service.BusinessGroupWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data=>{
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
        for (let i = 0; i < data.result.length; i++) {
          labels.push(data.result[i].busiGrupName);
          tempEmpData.push(data.result[i].busiGrupWiseTempEmpCnt);
          fixEmpData.push(data.result[i].busiGrupWiseFixEmpCnt);
        }
        this.barChartLabels = labels;
        this.barChartData = [
          { data: fixEmpData, label: 'Fixed' },
          { data: tempEmpData, label: 'Temp' }
        ];
        this.chartFlag = true;
    },err=>{
        this.chartFlag = true;
        this.barChartLabels = [];
        this.barChartData = [
          { data: [], label: 'Fixed' },
          { data: [], label: 'Temp' }
        ];
    })
   }

  ngOnInit() {
  }

  public barChartLabels: string[] = ['RMS', 'RAS', 'Components', 'Business Support'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [891, 138, 8, 68], label: 'Fixed' },
    { data: [344, 23, 0, 14], label: 'Temp' }
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#74b87b",
      // borderColor: 'black',
      // pointBackgroundColor: 'rgba(148,159,177,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: '#4a96d5',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
