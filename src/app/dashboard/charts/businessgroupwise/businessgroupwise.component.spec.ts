import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessgroupwiseComponent } from './businessgroupwise.component';

describe('BusinessgroupwiseComponent', () => {
  let component: BusinessgroupwiseComponent;
  let fixture: ComponentFixture<BusinessgroupwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessgroupwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessgroupwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
