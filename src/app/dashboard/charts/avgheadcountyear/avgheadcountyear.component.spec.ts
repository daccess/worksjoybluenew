import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvgheadcountyearComponent } from './avgheadcountyear.component';

describe('AvgheadcountyearComponent', () => {
  let component: AvgheadcountyearComponent;
  let fixture: ComponentFixture<AvgheadcountyearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvgheadcountyearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvgheadcountyearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
