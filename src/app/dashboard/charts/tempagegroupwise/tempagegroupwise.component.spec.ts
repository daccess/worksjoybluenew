import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempagegroupwiseComponent } from './tempagegroupwise.component';

describe('TempagegroupwiseComponent', () => {
  let component: TempagegroupwiseComponent;
  let fixture: ComponentFixture<TempagegroupwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempagegroupwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempagegroupwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
