import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadcountcategorywiseComponent } from './headcountcategorywise.component';

describe('HeadcountcategorywiseComponent', () => {
  let component: HeadcountcategorywiseComponent;
  let fixture: ComponentFixture<HeadcountcategorywiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadcountcategorywiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadcountcategorywiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
