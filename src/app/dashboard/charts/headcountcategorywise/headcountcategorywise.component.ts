import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-headcountcategorywise',
  templateUrl: './headcountcategorywise.component.html',
  styleUrls: ['./headcountcategorywise.component.css']
})
export class HeadcountcategorywiseComponent implements OnInit {

  flag: boolean = false;
  displayData: any = [];
  loggedUser: any;
  chartFlag: boolean = false;
  constructor(private service: DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.empGradeCount();
  }
  view: any[] = [270, 500];
  ngOnInit() {
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [], label: 'Fixed' },
    { data: [], label: 'Temp' }
  ];


  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    {
      backgroundColor: "#976832",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
     
    },
    {

      backgroundColor: '#9bbb58',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]

  public randomize(): void {
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

  selectTypeChanged(event) {
    if (event == "grade") {
      this.empGradeCount();
    }
    else if (event == "department") {
      this.empDeptCount();
    }
    else if (event == "designation") {
      this.empDescCount();
    }
    else if (event == "skill") {
      this.empSkillCount();
    }
    else if (event == "emptype") {
      this.categoryWiseTotalCountOfEmp();
    }
  }

  empGradeCount() {
    this.chartFlag = false;
    this.service.empGradeCount(this.loggedUser.compId).subscribe(data => {
      // console.log(data);
      
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].gradename);
        tempEmpData.push(data.result[i].gradewiseTempCnt);
        fixEmpData.push(data.result[i].gradewiseFixedCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }
  empDeptCount() {
    this.chartFlag = false;
    this.service.empDeptCount(this.loggedUser.compId).subscribe(data => {
      // console.log("this is the data"+data['result'])
      // console.log("this is department name"+data.result.deptName)
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      // for (let i = 0; i < data.result.length; i++) {
      //   data.result[i].deptName = data.result[i].deptName.trim();
      // }
      for (let i = 0; i < data.result.length; i++) {
      
        labels.push(data.result[i].deptName);
        tempEmpData.push(data.result[i].depwiseTempCnt);
        fixEmpData.push(data.result[i].depwiseFixedCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }

  empDescCount() {
    this.chartFlag = false;
    this.service.empDescCount(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].descName);
        tempEmpData.push(data.result[i].descTempCnt);
        fixEmpData.push(data.result[i].descFixedCnt);
      }
      
      
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }

  empSkillCount() {
    this.chartFlag = false;
    this.service.empSkillCount(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].skilCategory);
        tempEmpData.push(data.result[i].skillcatTempCnt);
        fixEmpData.push(data.result[i].skillcatFixedCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }

  categoryWiseTotalCountOfEmp() {
    this.chartFlag = false;
    this.service.categoryWiseTotalCountOfEmp(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].empTypeName);
        tempEmpData.push(data.result[i].empcatTempCnt);
        fixEmpData.push(data.result[i].empcatFixedCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }

}
