import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-femaleratioinshopfloor',
  templateUrl: './femaleratioinshopfloor.component.html',
  styleUrls: ['./femaleratioinshopfloor.component.css']
})
export class FemaleratioinshopfloorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  public barChartLabels: string[] = ['Toatal female employees in Shop floor', 'FTE', 'Temp'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  barChartOptions = [];
  public barChartData: any[] = [
    { data: [380, 0, 0], label: 'Fixed' },
    { data: [0, 210, 0], label: 'Temp' },
    { data: [0, 0, 300], label: 'Temp1' }
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#74b87b",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: 'sky blue',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    },
    { // grey

      backgroundColor: '#4a96d5',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
