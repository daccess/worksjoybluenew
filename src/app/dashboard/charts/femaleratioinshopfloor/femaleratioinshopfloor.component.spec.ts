import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FemaleratioinshopfloorComponent } from './femaleratioinshopfloor.component';

describe('FemaleratioinshopfloorComponent', () => {
  let component: FemaleratioinshopfloorComponent;
  let fixture: ComponentFixture<FemaleratioinshopfloorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FemaleratioinshopfloorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FemaleratioinshopfloorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
