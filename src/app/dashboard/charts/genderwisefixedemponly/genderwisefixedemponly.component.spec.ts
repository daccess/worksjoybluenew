import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenderwisefixedemponlyComponent } from './genderwisefixedemponly.component';

describe('GenderwisefixedemponlyComponent', () => {
  let component: GenderwisefixedemponlyComponent;
  let fixture: ComponentFixture<GenderwisefixedemponlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenderwisefixedemponlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenderwisefixedemponlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
