import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-genderwisefixedemponly',
  templateUrl: './genderwisefixedemponly.component.html',
  styleUrls: ['./genderwisefixedemponly.component.css']
})
export class GenderwisefixedemponlyComponent implements OnInit {
  flag : boolean = false;
  loggedUser: any;
  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    service.GenderWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data=>{
      let fixed = new Array();
      let temp = new Array();
      fixed.push(data.result.maleFixedEmpCnt)
      temp.push(data.result.maleTempEmpCnt)

      fixed.push(data.result.femaleFixedEmpCnt)
      temp.push(data.result.femaleTempEmpCnt)
 
      this.barChartData = [
        { data: fixed, label: 'Fixed' },
        { data: temp, label: 'Temp' }
      ];
      this.flag = true;
    })
   }

  ngOnInit() {
  }
  public barChartLabels: string[] = ['MALE', 'FEMALE'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [1053, 52, 8, 68], label: 'Fixed' },
    { data: [327, 54, 0, 14], label: 'Temp' }
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#087C74",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: '#083F3B',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
