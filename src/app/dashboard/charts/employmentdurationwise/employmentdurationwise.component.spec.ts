import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploymentdurationwiseComponent } from './employmentdurationwise.component';

describe('EmploymentdurationwiseComponent', () => {
  let component: EmploymentdurationwiseComponent;
  let fixture: ComponentFixture<EmploymentdurationwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploymentdurationwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploymentdurationwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
