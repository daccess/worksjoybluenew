import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-employmentdurationwise',
  templateUrl: './employmentdurationwise.component.html',
  styleUrls: ['./employmentdurationwise.component.css']
})
export class EmploymentdurationwiseComponent implements OnInit {
  barChartOptions = [];
  loggedUser: any;
  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    service.EmploymentDurationCount(this.loggedUser.compId).subscribe(data=>{
      let exp = new Array();
      exp.push(data.result.expLess6Mth);
      exp.push(data.result.expBet1YrTo3Yr);
      exp.push(data.result.expBet3YrTo5Yr);
      exp.push(data.result.expBet5YrTo7Yr);
      exp.push(data.result.expBet6MonthTo1Year);
      exp.push(data.result.expBet7To10Yr);
      exp.push(data.result.expGreater10Yr);
      
      this.barChartData = [
        { data: exp, label: 'Fixed' },
        // {data: [344, 23, 0, 14], label: 'Temp'}
      ];
    })
   }

  ngOnInit() {
  }
  public barChartLabels: string[] = ['0 - 6 months', '1 - 3 yrs', '3 - 5 yrs', '5 - 7 yrs', '7 - 10 yrs', '< 10 yrs'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [34, 43, 91, 185, 208, 384, 160], label: 'Fixed' },
    // {data: [344, 23, 0, 14], label: 'Temp'}
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#3F3208",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    // { // grey

    //   backgroundColor: '#4a96d5',
    //   borderColor: '#fff',
    //   pointBackgroundColor: 'blue',
    //   pointBorderColor: '#fff',
    //   pointHoverBackgroundColor: '#fff',
    //   pointHoverBorderColor: '#fff'
    // }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
