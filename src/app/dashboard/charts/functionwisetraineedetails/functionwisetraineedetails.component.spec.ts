import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionwisetraineedetailsComponent } from './functionwisetraineedetails.component';

describe('FunctionwisetraineedetailsComponent', () => {
  let component: FunctionwisetraineedetailsComponent;
  let fixture: ComponentFixture<FunctionwisetraineedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionwisetraineedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionwisetraineedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
