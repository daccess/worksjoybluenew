import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-functionwisetraineedetails',
  templateUrl: './functionwisetraineedetails.component.html',
  styleUrls: ['./functionwisetraineedetails.component.css']
})
export class FunctionwisetraineedetailsComponent implements OnInit {
  loggedUser: any;

  constructor() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
   }

  ngOnInit() {
  }
  public barChartLabels: string[] = ['Business Support T&C', 'Marketing & Sales', 'Operations', 'Parts A/S', 'Services A/S', 'Products', 'Installation'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  barChartOptions = [];
  public barChartData: any[] = [
    { data: [0, 1, 7, 4, 2, 1, 2], label: 'Trainee' },
    { data: [0, 0, 16, 0, 0, 0, 0], label: 'VET' }
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#727513",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: '#6E6E68',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
