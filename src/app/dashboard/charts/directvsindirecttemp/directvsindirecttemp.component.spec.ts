import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectvsindirecttempComponent } from './directvsindirecttemp.component';

describe('DirectvsindirecttempComponent', () => {
  let component: DirectvsindirecttempComponent;
  let fixture: ComponentFixture<DirectvsindirecttempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectvsindirecttempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectvsindirecttempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
