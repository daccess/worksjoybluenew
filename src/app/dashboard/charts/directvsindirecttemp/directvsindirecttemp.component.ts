import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-directvsindirecttemp',
  templateUrl: './directvsindirecttemp.component.html',
  styleUrls: ['./directvsindirecttemp.component.css']
})
export class DirectvsindirecttempComponent implements OnInit {
  barChartOptions: any;
  loggedUser: any;
  chartFlag: boolean = false;
  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.chartFlag = false;
    service.CurrentYearMonthWiseDirectAndIndirectTempEmp(this.loggedUser.compId).subscribe(data=>{
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
        for (let i = 0; i < data.result.length; i++) {
          labels.push(data.result[i].busiGrupName);
          tempEmpData.push(data.result[i].busiGrupWiseTempEmpCnt);
          fixEmpData.push(data.result[i].busiGrupWiseFixEmpCnt);
        }
        this.barChartLabels = labels;
        this.barChartData = [
          { data: fixEmpData, label: 'Direct' },
          { data: tempEmpData, label: 'Indirect' }
        ];
        this.chartFlag = true;
    },err=>{
        this.chartFlag = true;
    })
   }
  ngOnInit() {
  }


  public barChartLabels: string[] = ['Jan-18', 'Feb-18', 'Mar-18', 'Apr-18', 'May-18', 'June-18', 'July-18'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [800, 200, 190, 180, 120, 250, 80], label: 'Direct' },
    { data: [400, 200, 130, 220, 130, 270, 60], label: 'Indirect' }
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#074351",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: '#4a96d5',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }


}
