import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-functionswise',
  templateUrl: './functionswise.component.html',
  styleUrls: ['./functionswise.component.css']
})
export class FunctionswiseComponent implements OnInit {
  barChartOptions: any;
  loggedUser: any;
  chartFlag: boolean = true;
  constructor(private service : DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.chartFlag = false;
    service.FunctionWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data=>{
      let fixEmpData = new Array();
      fixEmpData.push(data.result.lessthan1yr);
      fixEmpData.push(data.result.expBet1YrTo2Yr);
      fixEmpData.push(data.result.expBet2YrTo3Yr);
      fixEmpData.push(data.result.expBet3YrTo4Yr);
      fixEmpData.push(data.result.expBet4YrTo5Yr);
      fixEmpData.push(data.result.exp5Yrabove);
        this.barChartData = [
          { data: fixEmpData, label: 'Employees' }
        ];
        this.chartFlag = true;
    },err=>{
        this.chartFlag = true;
        this.barChartData = [
          { data: [], label: 'Employees' }
        ];
    })
   }

  ngOnInit() {
  }

  public barChartLabels: string[] = ['0-1 years','1-2 years','2-3 years','3-4 years','4-5years','5+ years'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [0,0,0,0,0,0], label: 'Employees' },
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#305690",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
