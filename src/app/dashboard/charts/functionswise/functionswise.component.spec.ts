import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionswiseComponent } from './functionswise.component';

describe('FunctionswiseComponent', () => {
  let component: FunctionswiseComponent;
  let fixture: ComponentFixture<FunctionswiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionswiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionswiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
