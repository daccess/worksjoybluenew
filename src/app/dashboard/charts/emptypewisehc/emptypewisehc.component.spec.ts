import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptypewisehcComponent } from './emptypewisehc.component';

describe('EmptypewisehcComponent', () => {
  let component: EmptypewisehcComponent;
  let fixture: ComponentFixture<EmptypewisehcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptypewisehcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptypewisehcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
