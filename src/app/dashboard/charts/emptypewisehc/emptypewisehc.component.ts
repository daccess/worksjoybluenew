import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-emptypewisehc',
  templateUrl: './emptypewisehc.component.html',
  styleUrls: ['./emptypewisehc.component.css']
})
export class EmptypewisehcComponent implements OnInit {

  // public pieChartLabels: string[] = ['Managerial Staff', 'Staff', 'Junior Staff', 'Operators', 'Temp Direct Labours', 'Temp Indirect Labours', 'Expart'];
  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  // public pieChartData: number[] = [];
  public pieChartType: string = 'pie';

  flag: boolean = false;
  displayData: any = [];
  loggedUser: any;
  constructor(private service: DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    service.CategoryWiseTotalCountOfEmp(this.loggedUser.compId).subscribe(data => {
    console.log(data)
      // let labels = new Array();
      // let category = new Array();
      let fixedEmp = new Array();
      let tempEmp = new Array();
      let labels = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].empTypeName);
        // category.push(data.result[i].empcatFixedCnt);
        fixedEmp.push(data.result[i].empcatFixedCnt);
        tempEmp.push(data.result[i].empcatTempCnt);
        
        // let obj = {
          
        //   label: data.result[i].empTypeName,
        //   category: data.result[i].categoryWiseEmpCnt
        // }
        // // this.displayData.push(obj)
        // console.log("display",this.displayData)
      }

      this.barChartLabels = labels;
      this.barChartData =[
        { data: fixedEmp, label: 'Fixed' },
        { data: tempEmp, label: 'Temp' }
      ];
      this.flag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData =[
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.flag = true;
    })
  }

  ngOnInit() {
  }

  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [], label: 'Fixed' },
    { data: [], label: 'Temp' }
  ];
  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
}
