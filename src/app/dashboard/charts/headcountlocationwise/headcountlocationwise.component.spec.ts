import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadcountlocationwiseComponent } from './headcountlocationwise.component';

describe('HeadcountlocationwiseComponent', () => {
  let component: HeadcountlocationwiseComponent;
  let fixture: ComponentFixture<HeadcountlocationwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadcountlocationwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadcountlocationwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
