import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';

@Component({
  selector: 'app-headcountlocationwise',
  templateUrl: './headcountlocationwise.component.html',
  styleUrls: ['./headcountlocationwise.component.css']
})
export class HeadcountlocationwiseComponent implements OnInit {
  loggedUser: any;
  chartFlag: boolean = true;
  constructor(private service: DashboardserviceService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.locationWiseCurrentMonthTotalCountOfEmp();
  }

  ngOnInit() {
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  // public barChartLabels: string[] = ['Pune', 'Chandigarh', 'Coimbtore', 'Others', 'Ahmedabad'];
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [], label: 'Fixed' },
    { data: [], label: 'Temp' }
  ];

  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { 
      backgroundColor: "#976832",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { 
      backgroundColor: '#9bbb58',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

  selectTypeChanged(event) {
    if (event == "location") {
      this.locationWiseCurrentMonthTotalCountOfEmp();
    }
    else if (event == "business") {
      this.businessGroupWiseCurrentMonthTotalCountOfEmp();
    }
    else if (event == "function") {
      this.functionWiseCurrentMonthTotalCountOfEmp();
    }
    else if (event == "gender") {
      this.genderWiseCurrentMonthTotalCountOfEmp();
    }
  }

  businessGroupWiseCurrentMonthTotalCountOfEmp() {
    this.chartFlag = false;
    this.service.businessGroupWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].busiGrupName);
        tempEmpData.push(data.result[i].busiGrupWiseTempEmpCnt);
        fixEmpData.push(data.result[i].busiGrupWiseFixEmpCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }
  locationWiseCurrentMonthTotalCountOfEmp() {
    this.chartFlag = false;
    this.service.locationWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].locationName);
        tempEmpData.push(data.result[i].locationWiseTempEmpCnt);
        fixEmpData.push(data.result[i].locationWiseFixEmpCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }

  functionWiseCurrentMonthTotalCountOfEmp() {
    this.chartFlag = false;
    this.service.functionWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      for (let i = 0; i < data.result.length; i++) {
        labels.push(data.result[i].funstionUnitName);
        tempEmpData.push(data.result[i].functionUnitWiseTempEmpCnt);
        fixEmpData.push(data.result[i].functionUnitWiseFixEmpCnt);
      }
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];
      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }

  genderWiseCurrentMonthTotalCountOfEmp() {
    this.chartFlag = false;
    this.service.genderWiseCurrentMonthTotalCountOfEmp(this.loggedUser.compId).subscribe(data => {
      let labels = new Array();
      let tempEmpData = new Array();
      let fixEmpData = new Array();
      labels = ["Male", "Female"];
      tempEmpData = [data.result.maleTempEmpCnt, data.result.femaleTempEmpCnt];
      fixEmpData = [data.result.maleFixedEmpCnt, data.result.femaleFixedEmpCnt];
      this.barChartLabels = labels;
      this.barChartData = [
        { data: fixEmpData, label: 'Fixed' },
        { data: tempEmpData, label: 'Temp' }
      ];

      this.chartFlag = true;
    }, err => {
      this.barChartLabels = [];
      this.barChartData = [
        { data: [], label: 'Fixed' },
        { data: [], label: 'Temp' }
      ];
      this.chartFlag = true;
    })
  }
}
