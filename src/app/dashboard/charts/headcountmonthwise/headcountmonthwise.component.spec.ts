import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadcountmonthwiseComponent } from './headcountmonthwise.component';

describe('HeadcountmonthwiseComponent', () => {
  let component: HeadcountmonthwiseComponent;
  let fixture: ComponentFixture<HeadcountmonthwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadcountmonthwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadcountmonthwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
