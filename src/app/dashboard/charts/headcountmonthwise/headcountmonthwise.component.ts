import { Component, OnInit } from '@angular/core';
// import { DxChartModule } from 'devextreme-angular';
// import { colorSets } from '../src/utils/color-sets';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DashboardserviceService } from '../../shared/services/dashboardservice.service';
@Component({
    selector: 'app-headcountmonthwise',
    templateUrl: './headcountmonthwise.component.html',
    styleUrls: ['./headcountmonthwise.component.css']
})
export class HeadcountmonthwiseComponent implements OnInit {
    loggedUser: any;
    monthNames: string[];
    chartFlag: boolean = true;
   
    constructor(private service : DashboardserviceService) {
       
        let user = sessionStorage.getItem('loggedUser');
        this.loggedUser = JSON.parse(user);
        this.chartFlag = false;
        service.CurrentYearMonthWiseTotalCountOfEmp(this.loggedUser.compId).subscribe(data=>{
            let fixedEmp = new Array();
            let tempEmp = new Array();
            let labels = new Array();
            for (let i = 0; i < data.result.length; i++) {
              
                fixedEmp.push(data.result[i].fixedCnt);
                tempEmp.push(data.result[i].tempCnt);
               
                 labels.push(''+data.result[i].cntMonth+'');
               
              }
              this.barChartLabels = labels;
              this.barChartData =[
                { data: fixedEmp, label: 'Fixed' },
                { data: tempEmp, label: 'Temp' }
              ];
              this.chartFlag = true;      
            },err=>{
              this.barChartLabels = [];
              this.barChartData =[
                { data: [], label: 'Fixed' },
                { data: [], label: 'Temp' }
              ];
              this.chartFlag = true;
            })
    }
        ngOnInit() {

        }
        //public barChartLabels: string[] = ['0-1 years','1-2 years','2-3 years','3-4 years','4-5years','5+ years'];
        public barChartOptions: any = {
            scaleShowVerticalLines: false,
            responsive: true,
            scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true,
                      mirror:false,
                      suggestedMin: 0,
                      suggestedMax: 100
                  }
              }]
          }
                
          };
          public barChartLabels: string[] = [];
          public barChartType: string = 'bar';
          public barChartLegend: boolean = true;
        
          public barChartData: any[] = [
            { data: [], label: 'Fixed' },
            { data: [], label: 'Temp' }
          ];
        
          // events
          public chartClicked(e: any): void {
          }
        
          public chartHovered(e: any): void {
          }
          public barChartColors: Array<any> = [
            { // grey
              backgroundColor: "#74b87b",
              borderColor: 'black',
              pointBackgroundColor: 'rgba(148,159,177,1)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            },
            { // grey
        
              backgroundColor: 'rgb(251, 109, 109)',
              borderColor: '#fff',
              pointBackgroundColor: 'blue',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: '#fff'
            }
          ]
        
        
          public randomize(): void {
            let data = [
              Math.round(Math.random() * 100),
              59,
              80,
              (Math.random() * 100),
              56,
              (Math.random() * 100),
              40];
            let clone = JSON.parse(JSON.stringify(this.barChartData));
            clone[0].data = data;
            this.barChartData = clone;
          }
        }
        
    