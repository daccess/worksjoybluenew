import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: DashboardComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'dashboard',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'dashboard',
                    component: DashboardComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);