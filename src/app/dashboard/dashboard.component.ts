import { Component, OnInit } from '@angular/core';
import { DashboardserviceService } from './shared/services/dashboardservice.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', './dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  imageUrl = MainURL.imageUrl;
  loggedUser: any;
  compId: any;
  currentYear : number;
  previousYear : number;
  totalGrowthAndAvgHeadCnt  = {empAvgCntLastYear:0,empAvgCntCurrentYear:0,growthrate:0};
  attrition = {attritionlastyear: 0,attritioncurrentyear: 0,attritionlastmonth: 0,atrritionCurrentMonth: 0};
  selectType : string = "location";
  headcountcategorywiseType : string = "grade";
  currentMonthAttrition = {lastYearGrowthRate:0};
  constructor(private service: DashboardserviceService) { 
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.currentYear = new Date().getFullYear();
    this.previousYear = new Date().getFullYear()-1;
    
    this.compId =  this.loggedUser.compId;
  }
  totalEmployee: any = {employeeFixedCount : 0,employeeTempCount : 0};
  ngOnInit() {
    this.getToatlEmployees();
    this.getTotalGrowthAndAvgHeadCnt();
    this.getAttrition();
    this.getCurrentMonthAttrition();
  }

  getToatlEmployees() {
    this.service.getTotalEmpCount(this.compId)
      .subscribe(data => {
        this.totalEmployee = data.result;
      }, 
      (err: HttpErrorResponse) => {
      }
      );
  }
  getTotalGrowthAndAvgHeadCnt(){
    this.service.getTotalGrowthAndAvgHeadCnt(this.compId).subscribe(data=>{
        this.totalGrowthAndAvgHeadCnt = data.result;
        this.totalGrowthAndAvgHeadCnt.empAvgCntCurrentYear = parseInt(this.totalGrowthAndAvgHeadCnt.empAvgCntCurrentYear.toFixed());
        this.totalGrowthAndAvgHeadCnt.empAvgCntLastYear = parseInt(this.totalGrowthAndAvgHeadCnt.empAvgCntLastYear.toFixed());
    },(err: HttpErrorResponse)=>{

    })
  }
  getAttrition(){
    this.service.getAttrition(this.compId).subscribe(data=>{
        this.attrition = data.result;
    },(err : HttpErrorResponse)=>{
      
    })
  }
  getCurrentMonthAttrition(){
    // 
    this.service.getCurrentMonthAttrition(this.compId).subscribe(data=>{
       this.currentMonthAttrition = data.result;
    },(err : HttpErrorResponse)=>{

    })
  }

  
}