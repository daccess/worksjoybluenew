import { routing } from './dashboard.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';


import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from './dashboard.component'
import { EmployeelistComponent } from 'src/app/employeelist/employeelist.component';
import { AvgheadcountyearComponent } from './charts/avgheadcountyear/avgheadcountyear.component';
import { HeadcountmonthwiseComponent } from './charts/headcountmonthwise/headcountmonthwise.component';
// import { DxChartModule } from 'devextreme-angular';
import { HeadcountcategorywiseComponent } from './charts/headcountcategorywise/headcountcategorywise.component';
import { HeadcountlocationwiseComponent } from './charts/headcountlocationwise/headcountlocationwise.component';
import { DirectvsindirectComponent } from './charts/directvsindirect/directvsindirect.component';
import { DirectvsindirecttempComponent } from './charts/directvsindirecttemp/directvsindirecttemp.component';
import { FunctionswiseComponent } from './charts/functionswise/functionswise.component';
import { BusinessgroupwiseComponent } from './charts/businessgroupwise/businessgroupwise.component';
import { AgegroupwisefixedemponlyComponent } from './charts/agegroupwisefixedemponly/agegroupwisefixedemponly.component';
import { GenderwisefixedemponlyComponent } from './charts/genderwisefixedemponly/genderwisefixedemponly.component';
import { EmploymentdurationwiseComponent } from './charts/employmentdurationwise/employmentdurationwise.component';
import { FunctionwisetraineedetailsComponent } from './charts/functionwisetraineedetails/functionwisetraineedetails.component';
import { TempagegroupwiseComponent } from './charts/tempagegroupwise/tempagegroupwise.component';
import { FemaleratioinshopfloorComponent } from './charts/femaleratioinshopfloor/femaleratioinshopfloor.component';
import { Tempabove40yrsofageComponent } from './charts/tempabove40yrsofage/tempabove40yrsofage.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { EmptypewisehcComponent } from './charts/emptypewisehc/emptypewisehc.component';
import { GenderchartComponent } from './charts/genderchart/genderchart.component';

@NgModule({
    declarations: [
        DashboardComponent,
        // EmployeelistComponent,
        AvgheadcountyearComponent,
        HeadcountmonthwiseComponent,
        HeadcountcategorywiseComponent,
        HeadcountlocationwiseComponent,
        DirectvsindirectComponent,
        DirectvsindirecttempComponent,
        FunctionswiseComponent,
        BusinessgroupwiseComponent,
        AgegroupwisefixedemponlyComponent,
        GenderwisefixedemponlyComponent,
        EmploymentdurationwiseComponent,
        FunctionwisetraineedetailsComponent,
        TempagegroupwiseComponent,
        FemaleratioinshopfloorComponent,
        Tempabove40yrsofageComponent,
        EmptypewisehcComponent,
        GenderchartComponent
    ],
    imports: [
        // DxChartModule,
        routing,
        ToastrModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CommonModule,
        HttpClientModule,
        DataTablesModule,
        ChartsModule,
        NgxChartsModule
    ],
    providers: []
})
export class DashboardModule {
    constructor() {

    }
}
