import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from 'src/app/shared/configurl';
import { ContractorMaster } from 'src/app/shared/model/ContractorMasterModel';
import { totalEmployees } from '../models/totalEmployees';

@Injectable({
  providedIn: 'root'
})
export class DashboardserviceService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  getTotalEmpCount(id){
    let url = this.baseurl + '/employeeCount/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  getTotalGrowthAndAvgHeadCnt(id){
    let url = this.baseurl + '/avgcntemployee/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  getAttrition(id){
    let url = this.baseurl + '/attrition/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  getCurrentMonthAttrition(id){
    let url = this.baseurl + '/getCurrentMonthAttritionRate.json/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  AgeGroupWiseTotalCountOfEmp(id){
    let url = this.baseurl + '/AgeGroupWiseTotalFixedCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  averageHeadCountYearly(id){
    let url = this.baseurl + '/avgcntoverview/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  
  CurrentYearMonthWiseTotalCountOfEmp(id){
    let url = this.baseurl + '/headcntMonthwise/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  CategoryWiseTotalCountOfEmp(id){
    let url = this.baseurl + '/CategoryWiseTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }


  FunctionWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/EmployeeTenure/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  BusinessGroupWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/BusinessGroupWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  CurrentYearMonthWiseDirectAndIndirectFixedEmp(id){
    let url = this.baseurl + '/BusinessGroupWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  CurrentYearMonthWiseDirectAndIndirectTempEmp(id){
    let url = this.baseurl + '/CurrentYearMonthWiseDirectAndIndirectTempEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  GenderWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/GenderWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  EmploymentDurationCount(id){
    let url = this.baseurl + '/EmploymentDurationCount/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  AgeGroupWiseTotalTempCountOfEmp(id){
    let url = this.baseurl + '/AgeGroupWiseTotalTempCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  TempEmpAbove40(id){
    let url = this.baseurl + '/TempEmpAbove40/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json())
  }

  locationWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/LocationWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  businessGroupWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/BusinessGroupWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  functionWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/FunctionWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  genderWiseCurrentMonthTotalCountOfEmp(id){
    let url = this.baseurl + '/GenderWiseCurrentMonthTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  empGradeCount(id){
    let url = this.baseurl + '/empgradecnt/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  empDeptCount(id){
    let url = this.baseurl + '/empdepecnt/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  empDescCount(id){
    let url = this.baseurl + '/empdesignationcnt/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  empSkillCount(id){
    let url = this.baseurl + '/empskillcnt/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
  categoryWiseTotalCountOfEmp(id){
    let url = this.baseurl + '/CategoryWiseTotalCountOfEmp/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }

  genderDiversity(id){
    let url = this.baseurl + '/genderdiversity/'+id;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.get(url).map(x => x.json());
  }
}
