import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryRaisedReportComponent } from './query-raised-report.component';

describe('QueryRaisedReportComponent', () => {
  let component: QueryRaisedReportComponent;
  let fixture: ComponentFixture<QueryRaisedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryRaisedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryRaisedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
