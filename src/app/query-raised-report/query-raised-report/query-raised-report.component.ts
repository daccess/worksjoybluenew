import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InfoService } from "../../shared/services/infoService";
import * as jsPDF from 'jspdf';
import * as moment from 'moment';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-query-raised-report',
  templateUrl: './query-raised-report.component.html',
  styleUrls: ['./query-raised-report.component.css']
})
export class QueryRaisedReportComponent implements OnInit {
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;
  marked10 = true;

  srNoCheckbox = true
  empCodeCheckbox = true;
  employeeNameCheckbox = true;
  queryDetailsCheckbox = true;
  categoryCheckbox = true;
  subCategoryCheckbox = true;
  raisedOnDateCheckbox = true;
  statusCheckbox = true;
  remarkByCheckbox = true;
  remarksCheckbox = true;
 
  fromDate: string;
  toDate: string;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  AllReport: any;
  report_time: any;
  
  constructor(private InfoService:InfoService,private router: Router,private datePipe: DatePipe) {
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.AllReport = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
   }
  
  ngOnInit() {
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }

  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }

  toggleVisibility10(e) {
    this.marked10 = e.target.checked;
  }

  exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename='Query-Raised-Report'+moment().format('DD-MM-YYYY h:mm:ss a');
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a3');
    doc.autoTable({
        html: '#query_id',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
          6: {halign:'center'},
          7: {halign:'left'},
          8: {halign:'left'},
          9: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(0, 0, 0);
        doc.setFontSize(7);
        doc.setFontStyle("Arial");
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(270, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      doc.save('Query-Raised-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a')+'.pdf');
  }
}
