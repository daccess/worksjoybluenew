import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { QueryRaisedReportComponent } from './query-raised-report/query-raised-report.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '', component: QueryRaisedReportComponent,
    children:[
      {
        path:'',
        redirectTo : 'queryRaisedReports',
        pathMatch :'full'
        
      }]
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [DatePipe],
  declarations: [QueryRaisedReportComponent]
})
export class QueryRaisedReportModule { }
