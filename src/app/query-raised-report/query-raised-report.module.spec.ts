import { QueryRaisedReportModule } from './query-raised-report.module';

describe('QueryRaisedReportModule', () => {
  let queryRaisedReportModule: QueryRaisedReportModule;

  beforeEach(() => {
    queryRaisedReportModule = new QueryRaisedReportModule();
  });

  it('should create an instance', () => {
    expect(queryRaisedReportModule).toBeTruthy();
  });
});
