import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingdurationreportComponent } from './workingdurationreport.component';

describe('WorkingdurationreportComponent', () => {
  let component: WorkingdurationreportComponent;
  let fixture: ComponentFixture<WorkingdurationreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingdurationreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingdurationreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
