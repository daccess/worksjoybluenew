import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';
import { Title } from '@angular/platform-browser';
import { BrowserJsonp } from '@angular/http/src/backends/browser_jsonp';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ColdObservable } from 'rxjs/internal/testing/ColdObservable';
@Component({
  selector: 'app-workingdurationreport',
  templateUrl: './workingdurationreport.component.html',
  styleUrls: ['./workingdurationreport.component.css'],
  providers:[ExcelService,DatePipe]
})
export class WorkingdurationreportComponent implements OnInit {
  @ViewChild('content') content: ElementRef;
  AllReportData: any;
  AllReport: any;
  exceldata1 =[];

  tablehead = [];
  fromDate: any;
  toDate: any;
  loggedUser: any;
  compName: any;
  todaysDate: Date;
  margins = {
    top: 70,
    bottom: 40,
    left: 30,
    width: 550
  };
  element=[] ;
  attenDate: any;
  gateDay: number;
  start=10;
  table_length: number;
  report_time: string;
  constructor(private datePipe: DatePipe,private excelService:ExcelService,private InfoService:InfoService,private router: Router) {

    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    console.log("sessionnnnn data",this.loggedUser);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.InfoService.currentMessage.subscribe(message => {
    
      if(message!='default message'){
        this.AllReport = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
    // this.AllReportData = sessionStorage.getItem('workdurationreportData');
    
    // this.AllReport = JSON.parse(this.AllReportData);
    let tempAllReport = this.AllReport;
    let temp_array=[];
    for (let i = 0; i < tempAllReport.length; i++) {
      if(tempAllReport[i].workingDurationReportSubResDtoList){
        tempAllReport[i].Sr = i+1;
        temp_array.push(tempAllReport[i])
      }
      
    }
    this.AllReport = temp_array;
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");

    let getTableHead = sessionStorage.getItem('getTableheadworkduration');
    this.tablehead = JSON.parse(getTableHead);
    this.table_length=this.tablehead.length+7;
    console.log("tbale g",this.tablehead);
    for(let i=0; i<this.AllReport.length; i++){
      // console.log("viioviov",this.AllReport[i].workingDurationReportSubResDtoList);
      if(this.AllReport[i].workingDurationReportSubResDtoList){
         //test if empty
         if(this.AllReport[i].workingDurationReportSubResDtoList[0].attenDate){
          let temp= this.AllReport[i].workingDurationReportSubResDtoList[0].attenDate.match(/[^\s-]+-?/g);
          let parsed_date=parseInt(temp[2]);
          let temp_start_date=this.fromDate.match(/[^\s-]+-?/g);
          let parsed_start_date=parseInt(temp_start_date[2]);
          // console.log(parsed_start_date);
          if(parsed_date!=parsed_start_date){
            // console.log("not equal");
            for(let k=1;k<parsed_date;k++){
                let temp_date={
                "attenDate":" ",
                "attendanceStatus":" ",
                "checkIn":" ",
                "checkOut":" ",
                "coffStatus":" ",
                "descName":" ",
                "shiftCode":" ",
                "shiftName":" ",
                "statusOfDay":" ",
                "workDuration":" "
              }
              this.setemptydata();
              this.AllReport[i].workingDurationReportSubResDtoList.unshift(temp_date);
            }
            
          }
        } 

        // for(let i=0; i<this.AllReport.length; i++){
        
        //   if(this.AllReport[i].workingDurationReportSubResDtoList.checkIn=='null'){
        //     this.AllReport[i].workingDurationReportSubResDtoList.statusOfDay='IPF';
        //   }
        // }
        for(let l= 0; l<this.AllReport[i].workingDurationReportSubResDtoList.length; l++){

         if(this.AllReport[i].workingDurationReportSubResDtoList[l].checkIn==null){
          this.AllReport[i].workingDurationReportSubResDtoList[l].attendanceStatus='IPF';
         }
         if(this.AllReport[i].workingDurationReportSubResDtoList[l].checkOut==null){
          this.AllReport[i].workingDurationReportSubResDtoList[l].attendanceStatus='OPF';
         }
         if(this.AllReport[i].workingDurationReportSubResDtoList[l].checkIn==null && this.AllReport[i].workingDurationReportSubResDtoList[l].checkOut==null){
          this.AllReport[i].workingDurationReportSubResDtoList[l].attendanceStatus='A';
         }
         
        }


      for(let j= 0; j<this.AllReport[i].workingDurationReportSubResDtoList.length; j++){
        this.AllReport[i].workingDurationReportSubResDtoList[j].Sr = i + 1;
        this.exceldata1.push(this.AllReport[i].workingDurationReportSubResDtoList[j]);
      }
     }
    }
   }

  ngOnInit(){
    console.log("reportdata",this.AllReport);
  }

  exportAsXLSX():void {
    console.log("excl",this.exceldata1);

    let excelData = new Array();
    for (let i = 0; i < this.exceldata1.length; i++) {

      let obj : any= {
        empId: this.exceldata1[i].empId,
        fullName: this.exceldata1[i].fullName + " " + this.exceldata1[i].lastName ,
        deptName: this.exceldata1[i].deptName,
        descName: this.exceldata1[i].descName,
        statusOfDay: this.exceldata1[i].statusOfDay,
        checkIn: this.exceldata1[i].checkIn,
        checkOut: this.exceldata1[i].checkOut,
        workDuration: this.exceldata1[i].workDuration,
        coffStatus: this.exceldata1[i].coffStatus,
        attendanceStatus: this.exceldata1[i].attendanceStatus,
        shiftCode: this.exceldata1[i].shiftCode
       }

       excelData.push(obj)
    }

    this.excelService.exportAsExcelFile(excelData, 'Working_duration_Report');
  }
  

  // public captureScreen(){
  //   //pdf configuration
  //   var doc = new jsPDF( 'l', 'mm', 'a4');
  //   //set Header of PDF
  //   doc.setTextColor(48, 80, 139);
  //   doc.setFontSize(12);
  //   doc.setFontStyle("Arial");
  //   doc.text(22, 14 ,'Company Name :'+ this.compName);
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFontSize(9);
  //   doc.setFontStyle("Arial");
  //   doc.text(22, 42 ,'Abbreviation :- A : Absent Days, P : Present Days, HF : Half day, ER: Early Days, PD: Paid Days');
  //   doc.text(22, 50 ,'LT: Late Days, LEP : Late Early Punish, WO: Weekly Off, HO: Holiday , WOH : Week Off And Holiday, TWD : Total Work Duration');
  //   doc.setTextColor(48, 80, 139);
  //   doc.setFontSize(12);
  //   doc.setFontStyle("Arial");
  //   doc.text(120, 20,'Work Duration Report');
  //   doc.setTextColor(40, 80, 139);
  //   doc.setFontSize(12);
  //   doc.setFontStyle("Arial");
  //   doc.text(95, 24 ,'From date' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy") + ' ' +' To date' + ' : ' + this.datePipe.transform(this.toDate,"dd-MMM-yyyy"));
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFontSize(9);
  //   doc.setFontStyle("Arial");
  //   doc.text(212, 48 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"));
  //   doc.lineHeightProportion = 2;
  //     var rows = [];
  //     var all_employees_data = this.AllReport;
  //     let types = ["Day","In","Out","Dur","Ot","Status","Shift","Summary"];
  //     //header configuration
  //     var col = [];
  //     let temp={
  //       'content':'Type'
  //     }
  //     col.push(temp);
  //     for (let index = 0; index < this.tablehead.length; index++) {
  //       let item12={
  //         content : this.tablehead[index]
  //       }
  //       col.push(item12);
  //     }
  //     let head = [col];
  //     //header configuration :: END
  //     //create table for each employee
  //     all_employees_data.forEach(element => {
  //       // console.log(element);
  //       if(element.workingDurationReportSubResDtoList){
  //       let tempRows = new Array();
  //       //create body of table 
  //       for (let i = 0; i < types.length; i++) {
  //         let obj ;
  //         if(i == 0 && element.workingDurationReportSubResDtoList){
  //           obj = [types[i]]
  //         }
  //         else{
  //           obj = [types[i]]
  //         }
  //         if(element.workingDurationReportSubResDtoList){
  //         for (let j = 0; j < element.workingDurationReportSubResDtoList.length; j++) {
  //           this.attenDate= element.workingDurationReportSubResDtoList[j].attenDate;
  //           this.gateDay = new Date(element.workingDurationReportSubResDtoList[j].attenDate).getDate();
  //             if(i == 0){
  //               obj.push(element.workingDurationReportSubResDtoList[j].statusOfDay);
  //             }
  //             if(i == 1){
  //               obj.push(element.workingDurationReportSubResDtoList[j].checkIn);
  //             }
  //             if(i == 2){
  //               obj.push(element.workingDurationReportSubResDtoList[j].checkOut);
  //             }
  //             if(i == 3){
  //               obj.push(element.workingDurationReportSubResDtoList[j].workDuration);
  //             }
  //             if(i == 4){
  //               obj.push(0);
  //             } 
  //             if(i == 5){
  //               obj.push(element.workingDurationReportSubResDtoList[j].attendanceStatus);
  //             }
  //             if(i == 6){
  //               obj.push(element.workingDurationReportSubResDtoList[j].shiftCode);
  //             }
  //         }
  //       }

  //         if(i == 7){
  //             obj.push("P:"+element.workDurationReportTotalResDto.presentDays); 
  //             obj.push("A:"+element.workDurationReportTotalResDto.absentDays); 
  //             obj.push("Leave:"+element.workDurationReportTotalResDto.leave); 
             
  //             obj.push("WO:"+element.workDurationReportTotalResDto.weekOff); 
  //             obj.push("HO:"+element.workDurationReportTotalResDto.holiday);
                
  //             obj.push("Paid Days:"+element.workDurationReportTotalResDto.paidDays); 
  //             obj.push("TWD:"+element.workDurationReportTotalResDto.totalWorkDur); 
             
  //             obj.push("LT:"+element.workDurationReportTotalResDto.lateDays); 
  //             obj.push("ER:"+element.workDurationReportTotalResDto.earlyDays); 
  //             obj.push("OT:"+element.workDurationReportTotalResDto.overTime); 
  //             obj.push("COFF:"+element.workDurationReportTotalResDto.coff); 
  //             obj.push("LEP:"+element.workDurationReportTotalResDto.lateEarlyPunish);
  //             obj.push("WFH:"+element.workDurationReportTotalResDto.wfhCnt);
  //           }
  //         tempRows.push(obj);
  //         rows = tempRows;
  //       }
  //       if(element.Sr==1){
  //        this.start=68;
  //        doc.text('Sr.No : '+element.workingDurationReportSubResDtoList[0].Sr+' Employee ID : '+element.workingDurationReportSubResDtoList[0].empId+' Employee Name  : '+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName + '   Department Name : '+element.workingDurationReportSubResDtoList[0].deptName+'  Designation : '+element.workingDurationReportSubResDtoList[0].descName, 14,63);
  //       //  doc.text('Department Name :'+element.workingDurationReportSubResDtoList[0].deptName , 14, 65);
  //       //  doc.text('Designation :'+element.workingDurationReportSubResDtoList[0].descName , 14, finalY + 70);
  //       }else{
  //         var finalY = doc.lastAutoTable.finalY;
  //         this.start=finalY + 19;
  //         // obj = [element.Sr,element.workingDurationReportSubResDtoList[0].empId,element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName,element.workingDurationReportSubResDtoList[0].deptName,element.workingDurationReportSubResDtoList[0].descName,types[i]]
  //         // doc.text('Employee Name :'+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName, 14,  finalY + 10);
  //         // doc.text('Department Name :'+element.workingDurationReportSubResDtoList[0].deptName , 14, finalY + 15);
  //         // doc.text('Designation :'+element.workingDurationReportSubResDtoList[0].descName , 14, finalY + 20);
  //         doc.text('Sr.No : '+element.workingDurationReportSubResDtoList[0].Sr+' Employee ID : '+element.workingDurationReportSubResDtoList[0].empId+' Employee Name  : '+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName + '   Department Name : '+element.workingDurationReportSubResDtoList[0].deptName+'  Designation : '+element.workingDurationReportSubResDtoList[0].descName, 14, finalY + 14);
  //       }
  //       doc.autoTable({
  //         head: head,
  //         body: rows,
  //         startY: this.start,
  //         columnStyles: {
  //           0: {cellWidth: 8},
  //           1: {cellWidth: 8},
  //           2: {cellWidth: 8},
  //           3: {cellWidth: 8},
  //           4: {cellWidth: 8},
  //           5: {cellWidth: 8},
  //           6: {cellWidth: 8},
  //           7: {cellWidth: 8},
  //           8: {cellWidth: 8},
  //           9: {cellWidth: 8},
  //           10:{cellWidth: 8},
  //           11:{cellWidth: 8},
  //           12: {cellWidth: 8},
  //           13: {cellWidth: 8},
  //           14: {cellWidth: 8},
  //           15: {cellWidth: 8},
  //           16: {cellWidth: 8},
  //           17: {cellWidth: 8},
  //           18: {cellWidth: 8},
  //           19: {cellWidth: 8},
  //           20: {cellWidth: 8},
  //           21: {cellWidth: 8},
  //           22: {cellWidth: 8},
  //           23: {cellWidth: 8},
  //           24: {cellWidth: 8},
  //           25: {cellWidth: 8},
  //           26: {cellWidth: 8},
  //           27: {cellWidth: 8},
  //           28: {cellWidth: 8},
  //           29 :{cellWidth: 8},
  //           30:{cellWidth: 8},
  //           31: {cellWidth: 8},
  //           32: {cellWidth: 8},
  //           33: {cellWidth: 8},
  //           34: {cellWidth: 8},
  //           35: {cellWidth: 8},
  //         },
  //         tableLineColor: [189, 195, 199],
  //         tableLineWidth: 0.5,
  //         tableWidth: 'wrap',
  //         headStyles: {
  //           fillColor: [103, 132, 130],   
  //       },
  //         styles: {
  //           halign: 'center',
  //           cellPadding: 0.5, fontSize: 6
  //         },
  //         theme: 'grid',
  //         pageBreak:'avoid',
  //         margin: { top: 60,  bottom: 50 }
  //       });
  //     }
  //   });
  //    //set page count
  //    var pageCount = doc.internal.getNumberOfPages();
  //    for(let i = 0; i < pageCount; i++) { 
  //      doc.setPage(i); 
  //      doc.setTextColor(48, 80, 139)
  //      doc.text(250,12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
  //    }
  //    doc.save('Working-Duration-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a')+'.pdf');
  //   // doc.save('working_Report_report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');
  // }
  setemptydata(){
    for(let i=0; i<this.AllReport.length; i++){
      if(this.AllReport[i].workingDurationReportSubResDtoList){
        for(let j= 0; j<this.AllReport[i].workingDurationReportSubResDtoList.length; j++){
          if(this.AllReport[i].workingDurationReportSubResDtoList[j].empId){
            this.AllReport[i].workingDurationReportSubResDtoList[0].empId=this.AllReport[i].workingDurationReportSubResDtoList[j].empId;
          }
          if(this.AllReport[i].workingDurationReportSubResDtoList[j].fullName){
            this.AllReport[i].workingDurationReportSubResDtoList[0].fullName=this.AllReport[i].workingDurationReportSubResDtoList[j].fullName;
          }
          if(this.AllReport[i].workingDurationReportSubResDtoList[j].lastName){
            this.AllReport[i].workingDurationReportSubResDtoList[0].lastName=this.AllReport[i].workingDurationReportSubResDtoList[j].lastName;
          }
          if(this.AllReport[i].workingDurationReportSubResDtoList[j].deptName){
            this.AllReport[i].workingDurationReportSubResDtoList[0].deptName=this.AllReport[i].workingDurationReportSubResDtoList[j].deptName;
          }
          if(this.AllReport[i].workingDurationReportSubResDtoList[j].descName){
            this.AllReport[i].workingDurationReportSubResDtoList[0].descName=this.AllReport[i].workingDurationReportSubResDtoList[j].descName;
          }
        }
      }
    }
  }
  //new excel
  exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename='Working-Duration-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }

  public captureScreen(){
    //pdf configuration
    var doc = new jsPDF( 'l', 'mm', 'a4');
  
    doc.setTextColor(48, 75, 139);
    doc.setFontSize(12);
    doc.setFontStyle("Arial");
    doc.text(115, 15 ,'Company Name :'+ this.compName);
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(9);
    doc.setFontStyle("Arial");
    doc.text(22, 30 ,'Abbreviation :- A : Absent Days, P : Present Days, HF : Half day, ER: Early Days, PD: Paid Days , OD:On Duty , OPF : Out Punch Forgot ');
    doc.text(22, 35 ,'LT: Late Days, LEP : Late Early Punish, WO: Weekly Off, HO: Holiday , WOH : Week Off And Holiday, TWD : Total Work Duration');
    doc.setTextColor(48, 80, 139);
    doc.setFontSize(12);
    doc.setFontStyle("Arial");
    doc.text(120, 20,'Work Duration Report');
    doc.setTextColor(40, 80, 139);
    doc.setFontSize(12);
    doc.setFontStyle("Arial");
    doc.text(95, 24 ,'From date' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy") + ' ' +' To date' + ' : ' + this.datePipe.transform(this.toDate,"dd-MMM-yyyy"));
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(9);
    doc.setFontStyle("Arial");
    doc.text(212, 35 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"));
    //doc.lineHeightProportion = 2;
      var rows = [];
      var all_employees_data = this.AllReport;
      let types = ["Day","In","Out","Dur","Ot","Status","Shift","Summary"];
      //header configuration
      var col = [];
      let temp={
        'content':'Type'
      }
      col.push(temp);
      for (let index = 0; index < this.tablehead.length; index++) {
        let item12={
          content : this.tablehead[index]
        }
        col.push(item12);
      }
     // let head = [col];
      //header configuration :: END
      //create table for each employee
      all_employees_data.forEach(element => {
        // console.log(element);
        if(element.workingDurationReportSubResDtoList){
        let tempRows = new Array();
        //create body of table 
        for (let i = 0; i < types.length; i++) {
          let obj ;
          if(i == 0 && element.workingDurationReportSubResDtoList){
            obj = [types[i]]
          }
          else{
            obj = [types[i]]
          }
          if(element.workingDurationReportSubResDtoList){
          for (let j = 0; j < element.workingDurationReportSubResDtoList.length; j++) {
            this.attenDate= element.workingDurationReportSubResDtoList[j].attenDate;
            this.gateDay = new Date(element.workingDurationReportSubResDtoList[j].attenDate).getDate();
              if(i == 0){
                obj.push(element.workingDurationReportSubResDtoList[j].statusOfDay);
              }
              if(i == 1){
                obj.push(element.workingDurationReportSubResDtoList[j].checkIn);
              }
              if(i == 2){
                obj.push(element.workingDurationReportSubResDtoList[j].checkOut);
              }
              if(i == 3){
                obj.push(element.workingDurationReportSubResDtoList[j].workDuration);
              }
              if(i == 4){
                obj.push(0);
              } 
              if(i == 5){
                obj.push(element.workingDurationReportSubResDtoList[j].attendanceStatus);
              }
              if(i == 6){
                obj.push(element.workingDurationReportSubResDtoList[j].shiftCode);
              }
          }
        }

          if(i == 7){
              obj.push("P:"+element.workDurationReportTotalResDto.presentDays); 
              obj.push("A:"+element.workDurationReportTotalResDto.absentDays); 
              obj.push("Leave:"+element.workDurationReportTotalResDto.leave); 
             
              obj.push("WO:"+element.workDurationReportTotalResDto.weekOff); 
              obj.push("HO:"+element.workDurationReportTotalResDto.holiday);
                
              obj.push("Paid Days:"+element.workDurationReportTotalResDto.paidDays); 
              obj.push("TWD:"+element.workDurationReportTotalResDto.totalWorkDur); 
             
              obj.push("LT:"+element.workDurationReportTotalResDto.lateDays); 
              obj.push("ER:"+element.workDurationReportTotalResDto.earlyDays); 
              obj.push("OT:"+element.workDurationReportTotalResDto.overTime); 
              obj.push("COFF:"+element.workDurationReportTotalResDto.coff); 
              obj.push("LEP:"+element.workDurationReportTotalResDto.lateEarlyPunish);
              obj.push("WFH:"+element.workDurationReportTotalResDto.wfhCnt);
              obj.push("OD:"+element.workDurationReportTotalResDto.odCount);
            }
          tempRows.push(obj);
          rows = tempRows;
        }
        if(element.Sr==1){
         this.start=45;
        // doc.text('Sr.No : '+element.workingDurationReportSubResDtoList[0].Sr+' Employee ID : '+element.workingDurationReportSubResDtoList[0].empId+' Employee Name  : '+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName + '   Department Name : '+element.workingDurationReportSubResDtoList[0].deptName+'  Designation : '+element.workingDurationReportSubResDtoList[0].descName, 14,42);
        //  doc.text('Department Name :'+element.workingDurationReportSubResDtoList[0].deptName , 14, 65);
        //  doc.text('Designation :'+element.workingDurationReportSubResDtoList[0].descName , 14, finalY + 70);
        }else{
          var finalY = doc.lastAutoTable.finalY;
          this.start=finalY + 5;
          // obj = [element.Sr,element.workingDurationReportSubResDtoList[0].empId,element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName,element.workingDurationReportSubResDtoList[0].deptName,element.workingDurationReportSubResDtoList[0].descName,types[i]]
          // doc.text('Employee Name :'+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName, 14,  finalY + 10);
          // doc.text('Department Name :'+element.workingDurationReportSubResDtoList[0].deptName , 14, finalY + 15);
          // doc.text('Designation :'+element.workingDurationReportSubResDtoList[0].descName , 14, finalY + 20);
          //doc.text('Sr.No : '+element.workingDurationReportSubResDtoList[0].Sr+' Employee ID : '+element.workingDurationReportSubResDtoList[0].empId+' Employee Name  : '+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName + '   Department Name : '+element.workingDurationReportSubResDtoList[0].deptName+'  Designation : '+element.workingDurationReportSubResDtoList[0].descName, 14, this.start);
        }
        let heading = 'Sr.No : '+element.workingDurationReportSubResDtoList[0].Sr+' Employee ID : '+element.workingDurationReportSubResDtoList[0].empId+' Employee Name  : '+element.workingDurationReportSubResDtoList[0].fullName +' '+element.workingDurationReportSubResDtoList[0].lastName + '   Department Name : '+element.workingDurationReportSubResDtoList[0].deptName+'  Designation : '+element.workingDurationReportSubResDtoList[0].descName;
        let head = [
          [
              {content: heading, colSpan: 50, styles: {halign: 'left', fillColor: [22, 160, 133],fontSize: 7}}, 
             
          ],
          col,
        ];

        // var finalY = doc.lastAutoTable.finalY;
        // this.start=finalY + 12;
        console.log("this.start=>",this.start);

        doc.autoTable({
          head: head,
          body: rows,
          startY: this.start,
          columnStyles: {
            0: {cellWidth: 8},
            1: {cellWidth: 8},
            2: {cellWidth: 8},
            3: {cellWidth: 8},
            4: {cellWidth: 8},
            5: {cellWidth: 8},
            6: {cellWidth: 8},
            7: {cellWidth: 8},
            8: {cellWidth: 8},
            9: {cellWidth: 8},
            10:{cellWidth: 8},
            11:{cellWidth: 8},
            12: {cellWidth: 8},
            13: {cellWidth: 8},
            14: {cellWidth: 8},
            15: {cellWidth: 8},
            16: {cellWidth: 8},
            17: {cellWidth: 8},
            18: {cellWidth: 8},
            19: {cellWidth: 8},
            20: {cellWidth: 8},
            21: {cellWidth: 8},
            22: {cellWidth: 8},
            23: {cellWidth: 8},
            24: {cellWidth: 8},
            25: {cellWidth: 8},
            26: {cellWidth: 8},
            27: {cellWidth: 8},
            28: {cellWidth: 8},
            29 :{cellWidth: 8},
            30:{cellWidth: 8},
            31: {cellWidth: 8},
            32: {cellWidth: 8},
            33: {cellWidth: 8},
            34: {cellWidth: 8},
            35: {cellWidth: 8},
          },
          tableLineColor: [189, 195, 199],
          tableLineWidth: 0.5,
          tableWidth: 'wrap',
          headStyles: {
            fillColor: [103, 132, 130],   
        },
          styles: {
            halign: 'center',
            cellPadding: 0.5, fontSize: 6
          },
          theme: 'grid',
          pageBreak:'avoid',
          margin: { top: 15,  bottom: 0 }
        });
      }
    });
     //set page count
     var pageCount = doc.internal.getNumberOfPages();
     for(let i = 0; i < pageCount; i++) { 
       doc.setPage(i); 
       doc.setTextColor(48, 80, 139)
       doc.text(250,12, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
     }
     doc.save('Working-Duration-Report'+'-'+moment().format('DD-MM-YYYY h:mm:ss a')+'.pdf');
    // doc.save('working_Report_report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');
  }

 
}
