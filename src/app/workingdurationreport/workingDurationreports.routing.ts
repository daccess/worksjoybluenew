import { Routes, RouterModule } from '@angular/router'
import { WorkingdurationreportComponent } from './workingdurationreport.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: WorkingdurationreportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'workingDurationreports',
                    pathMatch :'full'
                    
                },
                {
                    path:'workingDurationreports',
                    component: WorkingdurationreportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);