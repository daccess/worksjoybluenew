import { routing } from './pimshr.routing';
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { PimshrComponent } from 'src/app/pimshr/pimshr.component';
import { HrofficialdetailsComponent } from 'src/app/pimshr/hrofficialdetails/hrofficialdetails.component';
import { HrattendancedetailsComponent } from './hrattendancedetails/hrattendancedetails.component';
import { HrsalarydetailsComponent } from './hrsalarydetails/hrsalarydetails.component';
import { HrpimsbasicformComponent } from './hrpimsbasicform/hrpimsbasicform.component';
import { HrothersdetailsComponent } from './hrothersdetails/hrothersdetails.component';

import { HRBasicDataService } from '../pimshr/Services/HRBasicEmpInfo';
import { HRempoffService } from '../pimshr/Services/HRempOfficialservice';
import { HRAttendenceDataService } from './Services/HRservice';
import { HRSalaryService } from '../pimshr/Services/HRsalarydetailsservice';
import { HRempOthersService } from '../pimshr/Services/HRempOthersservice';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { from } from 'rxjs';
import { SearchPipeForEmpDash } from '../pimshr/SearchPipe/Serach'
import {SharedModule} from '../shared/shared.module'; 
@NgModule({
    declarations: [
        PimshrComponent,
        HrofficialdetailsComponent,
        HrattendancedetailsComponent,
        HrsalarydetailsComponent,
        HrpimsbasicformComponent,
        HrothersdetailsComponent,
        SearchPipeForEmpDash
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgMultiSelectDropDownModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     SharedModule
    ],
    providers: [HRBasicDataService, 
        HRempoffService, 
        HRAttendenceDataService,
        HRSalaryService,
        HrattendancedetailsComponent,
        HrothersdetailsComponent,
        HrsalarydetailsComponent,
        HRempOthersService
        ]
  })
  export class PimshrModule { 
      constructor(){

      }
  }
  