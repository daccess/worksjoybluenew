import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrofficialdetailsComponent } from './hrofficialdetails.component';

describe('HrofficialdetailsComponent', () => {
  let component: HrofficialdetailsComponent;
  let fixture: ComponentFixture<HrofficialdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrofficialdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrofficialdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
