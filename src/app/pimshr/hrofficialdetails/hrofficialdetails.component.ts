import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MainURL } from '../../shared/configurl';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { HRempOfficialdata } from '../Models/HRempOfficial';
import { HRempoffService } from '../Services/HRempOfficialservice';
import { ToastrService } from 'ngx-toastr';
import { HRAttendenceDataService } from '../Services/HRservice';
import { HrattendancedetailsComponent } from '../hrattendancedetails/hrattendancedetails.component'
import { NgxSpinnerService } from 'ngx-spinner';

import { from } from 'rxjs';
import { DatePipe } from '@angular/common';
import { PimshrComponent } from '../pimshr.component';
import { HrsalarydetailsComponent } from '../hrsalarydetails/hrsalarydetails.component';
import { HrothersdetailsComponent } from '../hrothersdetails/hrothersdetails.component';
import { HRattendencesetting } from '../Models/HRempAttendence';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
@Component({
  selector: 'app-hrofficialdetails',
  templateUrl: './hrofficialdetails.component.html',
  styleUrls: ['./hrofficialdetails.component.css']
})
export class HrofficialdetailsComponent implements OnInit {
  @Output() someEvent = new EventEmitter<string>();
  baseurl = MainURL.HostUrl;
  costcenterData: any;
  businessGroupData: any;
  functionData: any;
  DepartmentAllData: any;
  SubDepartmentData: any;
  LocationAllData: any;
  DesignationAllData: any;
  gradeAllData: any;
  empTypeAllData: any;
  subempTypeAllData: any;
  FirstLevelAlldata = [];
  SecondLevelAlldata: any;
  ThirdLevelAlldata: any;
  Deligationdata: any;
  ActiveRoll: any;
  compId: any;
  empFlag: boolean;
  empFlag1: boolean;
  empFlag2: boolean;
  empFlag5: boolean;

  searchQuery: any;
  searchQuery1: any;
  searchQuery2: any;
  searchQuery5: any;

  selectedCoctCenterobj: any = "";
  selectedBusinessobj: any = "";
  selectedFunctionobj: any = "";
  selectedDepartmentobj: any = "";
  selectedSubDepartmentobj: any = "";
  selectedLocationobj: any = "";
  selectedDesignationobj: any = "";
  selectedGradeobj: any = "";
  selectedEmpTypeobj: any = "";
  selectedSubEmpTypeobj: any = "";
  selectedFirstLevelobj: any = "";
  selectedsecondLevelobj: any = "";
  selectedThirdLevelobj: any = "";
  selectedObjectRoleMasterId: any = "";

  HROfficialModel: HRempOfficialdata;


  object: any;

  dropdownSettings: {};
  LocationondropdownSettings: {};
  FunctionondropdownSettings: {};
  DepartmentondropdownSettings: {};
  SubDepartmentondropdownSettings: {};
  selectedItems: any[];
  //businessgroupActive: any;
  LocationcationActive: any;
  functiondataActive: any;
  depatmentActivelist: any;
  subdepatmentActivelist: any;

  selectedBusinessGroup = [];
  selectedLocationGroup = [];
  selectedFunctionGroup = [];
  selectedDepartmentGroup = [];
  selectedSubDepartmentGroup = [];


  flag: boolean = true;
  flag2: boolean = false;

  flagl: boolean = true;
  flagl2: boolean = false;

  flagf = true;
  flagf2 = false;

  flagd = true;
  flagd2 = false;

  flagsub = true;
  flagdsub2 = false;

  RoleHead: any;

  empPerId: any;
  empOfficialId: any;
  loggedUser: any;

  editData: any;
  selectedroleobj: any;
  isEditHR: any;
  condition: boolean

  firstlevelmanager: any;
  secondlevelmanager: any;
  thirdlevelmanager: any;
  delegationEmp: any;
  value: any;
  value1: any;
  value2: any;
  value3: any;
  value4: any;
  value5: any;
  value6: any;


  empId: any;
  getEmpBasicData: any;
  getAllBasicInfo: any;
  generateempPersonId: any;

  fullName: any;
  weeklyCalendarId: any;
  SelectProbationPeriod: any
  selectUndefinedOptionValue: any = "";
  a: any;
  getId: any;
  empTraineeDetailseList = [];
  empTemporyDetailsList = [];
  empProbationDetailsList = [];
  documentsSubmitted: boolean;
  documentsVerified: boolean;

  preonboarding: string = "";
  disable_flag=false;
   locationId: any;
  isEditAdmin: string;
  weeklyLocationID: any;
  rollName: any;
  locoid: any;
  currentDate: string;
  Shiftgroupdata: any;
  selectedLocationId: any=0;
  selectedlocationids: any=0;
  WeeklyOffcalendarData = [];
  selectedshiftgroupobj: any;
  Shiftmastergroupdata: any;
  selectedshiftmastergroupobj: any;
  ShiftPatterndata: any;
  selectedshiftpatternobj: number;
  ShiftPatternshiftdata: any;
  selectedshiftobj: number;
  selectedWeeklyGroupobj: any;
  weeklyOffType: string ="Fixed";
  atdSettingId: any;
  sequenceOne: any;
  shiftName1: any;
  sequencetwo: any;
  shiftName2: any;
  sequencethree: any;
  shiftName3: any;
  panNo: any;
  adharNo: any;
  pfNumber: any;
  universalAccNo: any;
  esicNumber: any;
  constructor(private others: HrothersdetailsComponent, private salary: HrsalarydetailsComponent, private pimshr: PimshrComponent, public attendance: HrattendancedetailsComponent, public httpService: HttpClient, public Spinner: NgxSpinnerService, public HROfficialservice: HRAttendenceDataService, public toastr: ToastrService,public HRattService: HRAttendenceDataService,public router:Router) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.isEditHR = sessionStorage.getItem('isEditHR');
    this.preonboarding = sessionStorage.getItem('preonboarding');
    this.compId = this.loggedUser.compId
    this.empId = this.loggedUser.empId;
    this.HROfficialModel = new HRempOfficialdata();
    this.HROfficialModel.logEmpOffId = this.loggedUser.empOfficialId;
    this.getEmpBasicData = sessionStorage.getItem('getEmpBasicData');
    this.getAllBasicInfo = JSON.parse(this.getEmpBasicData);
    this.firstLevelReporting(  this.selectedLocationId);
    this.secondLevelReporting();
    this.thirdLevelReporting();
    this.DeligationEmpdata();
    this.HROfficialModel.probationPeriod = 0;
     this.compId = this.loggedUser.compId;
    this.locationId = this.loggedUser.locationId;
 
    this.isEditAdmin = sessionStorage.getItem('isEditAdmin');
    //this.HROfficialModel = new HRattendencesetting();
    this.HROfficialModel.logEmpOffId = this.loggedUser.empOfficialId
    this.weeklyLocationID = sessionStorage.getItem('weeklyCalendarId');
    this.rollName=this.loggedUser.mrollMaster.roleName;
    this.weeklyoffdata();
  }
  // callParent() {
  //   this.someEvent.next('from attendance');
  // }

  ngOnInit() {

    if (this.HROfficialModel.documentsSubmitted == 'Yes') {
      this.documentsSubmitted = true
    } else {
      this.documentsSubmitted = false;
      this.HROfficialModel.documentsSubmitted == 'No'
    }
    this.empPerId = sessionStorage.getItem('EmpDataDetails');
    //console.log("emperifid",this.empPerId )
    this.costCenterList();
    this.businessgroup();
    this.locationGRoup();
    this.designationFunction();
    this.gradename();
    this.empType();
    this.firstLevelReporting(  this.selectedLocationId);
    this.secondLevelReporting();
    this.thirdLevelReporting();
    this.DeligationEmpdata();
    this.getActiveRoll();
    this.getContractorType();
    this.Departmentgroup();
    this.empPerId = sessionStorage.getItem('EmpDataDetails')
    //console.log("emperifid",this.empPerId )
    this.locoid= sessionStorage.getItem('locationIdvalue');
    this.shiptgroup();
    this.shiftmastergroup();
    this.shiftPattern();
    this.currentDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.HROfficialModel.roasterDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.HROfficialModel.negativeAttendenceAllowed = false;
    this.HROfficialModel.singlePunchPresent = false;
    this.HROfficialModel.negativeAttendenceAllowed = false;
    if (this.isEditHR == 'true' || this.isEditAdmin == 'true') {
        //this.getEditData1();
        this.getEditData1()
    }
    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'busiGroupId',
      textField: 'busiGrupName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };

    this.selectedItems = []
    this.LocationondropdownSettings = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };

    this.selectedItems = [
    ]
    this.FunctionondropdownSettings = {
      singleSelection: false,
      idField: 'functionUnitId',
      textField: 'functionUnitName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };

    this.selectedItems = [
    ]
    this.DepartmentondropdownSettings = {
      singleSelection: false,
      idField: 'deptId',
      textField: 'deptName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };


    this.selectedItems = [
    ]

    this.SubDepartmentondropdownSettings = {
      singleSelection: false,
      idField: 'subDeptId',
      textField: 'subDeptName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };


  


  }

  event(event) {
    this.value = event.target.value
  }
  event1(event) {
    this.value1 = event.target.value
  }
  //added by priyanka k start
  event2(event){
    
    this.value2 = event.target.value
    //console.log("eventvalue",this.value2)
  }
  event3(event){
    this.value3 = event.target.value
  }

  event4(event){
    this.value4 = event.target.value
  }
  event5(event){
    this.value5 = event.target.value
  }
  event6(event){
    this.value6 = event.target.value
  }
//added by priyanka k end
  generateEmployeeId(){
    let generateempid = this.baseurl + '/maxEmpId';
    this.httpService.get(generateempid).subscribe(data => {
        this.HROfficialModel.empId = data['result'];
      }, (err: HttpErrorResponse) => {
      }
    );
  }


  costCenterList() {
    let url = this.baseurl + '/CostCenter/Active/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.costcenterData = data["result"];

      }, (err: HttpErrorResponse) => {
      }
    );
  }


  constCenter(event) {
    this.selectedCoctCenterobj = parseInt(event.target.value);
  }

  businessgroup() {
    let url1 = this.baseurl + '/BusinessGroup/Active/';
    this.httpService.get(url1 + this.compId).subscribe(data => {
        this.businessGroupData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  businessGroup(event) {
    this.selectedBusinessobj = parseInt(event.target.value)
    this.functiongroup(this.selectedBusinessobj)
  }

  functiongroup(selectedValue) {

    let url2 = this.baseurl + '/FunctionUnitMaster/Active/';
    this.httpService.get(url2 + selectedValue).subscribe(data => {
        this.functionData = data["result"];
      }, (err: HttpErrorResponse) => {
      });

  }
  FuctionGroup(event) {
    this.selectedFunctionobj = parseInt(event.target.value);
    //this.Departmentgroup(this.selectedFunctionobj);
  }

  // Departmentgroup(selectedValue) {
  //   let url3 = this.baseurl + '/Department/Active/';
  //   this.httpService.get(url3 + selectedValue).subscribe(data => {
  //       this.DepartmentAllData = data["result"];
  //     }, (err: HttpErrorResponse) => {
  //     });
  // }

  Departmentgroup() {
    let url3 = this.baseurl + '/Department/Active/';
    //let url3 = this.baseurl + '/departmentactive/';
    this.httpService.get(url3 + this.compId).subscribe(data => {
        this.DepartmentAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }


  Departmentdata(event) {
    this.selectedSubDepartmentobj = "";
    this.selectedDepartmentobj = parseInt(event.target.value);
    this.SubDepartmentgroup(this.selectedDepartmentobj);
  }

  SubDepartmentgroup(selectedValue) {
    let url4 = this.baseurl + '/SubDepartment/Active/';
    this.httpService.get(url4 + selectedValue).subscribe(data => {
        this.SubDepartmentData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  SubDepartmentdata(event) {
    this.selectedSubDepartmentobj = parseInt(event.target.value);
  }
  hide: boolean = false;
  hidetrainee: boolean = false;
  temphide: boolean = false;
  SubEmptypeevent(event) {
    if (event == "On Probation") {
      this.hide = true;
    } else {
      this.hide = false;
    }
    if (event == "Trainee") {
      this.hidetrainee = true;
    } else {
      this.hidetrainee = false;
    }
    if (event == "Temporary") {
      this.temphide = true;
    } else {
      this.temphide = false;
    }

  }
  showProba: any;
  getExtenddate() {
    this.showProba = !this.showProba;
  }
  changeDate(event) {
    this.HROfficialModel.extenDateOfProb = event;
  }
  changetrainDate(event) {
    this.HROfficialModel.extenDateOfTrainee = event;
  }
  showTraining: any
  ClickExtendtrainingDate() {
    this.showTraining = !this.showTraining;
  }

  changeTempraryStartDate(event) {
    this.HROfficialModel.startDateOfTempWorking = event;
  }
  changeTempraryEndDate(event) {
    this.HROfficialModel.endDateOfTempWorking = event;
  }

  showTamporary: any
  ClickExtendTempory() {
    this.showTamporary = !this.showTamporary;
  }
  changeTempoDate(event) {
    this.HROfficialModel.extenDateOfTemp = event;
  }
  locationGRoup() {
    if(this.loggedUser.roleHead=='HR'){
      let url4 = this.baseurl + '/LocationWise/';
      this.httpService.get(url4 + this.loggedUser.locationId).subscribe(
      data => {
      this.LocationAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
    }
    else if(this.loggedUser.roleHead=='Admin'){
  let url4 = this.baseurl + '/Location/Active/';
  this.httpService.get(url4 + this.compId).subscribe(
  data => {
  this.LocationAllData = data["result"];
  }, (err: HttpErrorResponse) => {
  }
  );
  }
  }

  Locationdata(event) {
   
    this.selectedLocationobj = parseInt(event.target.value);
   // console.log("selectedlocationid",   this.selectedLocationobj );
    sessionStorage.setItem("locationIdvalue",this.selectedLocationobj );
    this.weeklyCalendarId = parseInt(event.target.value);
    sessionStorage.setItem("weeklyCalendarId", this.weeklyCalendarId);
   if(this.isEdit==true){
    this.selectedshiftmastergroupobj='';
    this.selectedshiftgroupobj = this.editData["shiftGroupId"]='';
    this.selectedWeeklyGroupobj = this.editData["weeklyOffCalenderMasterId"]='';
    this.searchQuery=''
   }
    this.shiptgroup();
  this.weeklyoffdata();
  this.shiftmastergroup(); 
 

  }

  designationFunction() {
    let urldesignation = this.baseurl + '/Designation/Active/';
    this.httpService.get(urldesignation + this.compId).subscribe(data => {
        this.DesignationAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  Designationdata(event) {
    this.selectedDesignationobj = parseInt(event.target.value);
  }

  gradename() {
    let urlgrade = this.baseurl + '/GradeMaster/Active/';
    this.httpService.get(urlgrade + this.compId).subscribe(data => {
      this.gradeAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  selectgradeData(event) {
    this.selectedGradeobj = parseInt(event.target.value);
  }

  empType() {
    let urlempType = this.baseurl + '/EmployeeType/Active/';
    this.httpService.get(urlempType + this.compId).subscribe(data => {
        this.empTypeAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  selectEmptype(event) {
    this.selectedSubEmpTypeobj = "";
    this.selectedEmpTypeobj = parseInt(event.target.value)
    this.subEmpType(this.selectedEmpTypeobj)
  }


  subEmpType(selectedValue) {
    let urlsubempType = this.baseurl + '/SubEmployeeType/Active/';
    this.httpService.get(urlsubempType + selectedValue).subscribe(data => {
        this.subempTypeAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  selectsubEmptype(event) {
    this.selectedSubEmpTypeobj = parseInt(event.target.value);
  }
  firstLevelReporting(e) {
    this.selectedLocationId=e;
    if(this.loggedUser.roleHead=='HR'){
    let url = this.baseurl + '/EmployeeOperation/';
    this.httpService.get(url + this.loggedUser.locationId).subscribe(data => {
        this.FirstLevelAlldata = data["result"];
        //console.log("reporterdata",   this.FirstLevelAlldata);
      },
      (err: HttpErrorResponse) => {
        this.FirstLevelAlldata = [];
      });
    }
    else if(this.loggedUser.roleHead=='Admin'){


      let url = this.baseurl + '/EmployeeOperation/';
      this.httpService.get(url + this.selectedLocationId).subscribe(data => {
          this.FirstLevelAlldata = data["result"];
          //console.log("reporterdata",   this.FirstLevelAlldata);
        },
        (err: HttpErrorResponse) => {
          this.FirstLevelAlldata = [];
        });

      }

  }
  selectFirstLevel(event) {

  }

  secondLevelReporting() {
    let url = this.baseurl + '/EmployeeOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.SecondLevelAlldata = data["result"];
      },
      (err: HttpErrorResponse) => {
        
      });

  }
  selectSecondLevel(event) {
  }

  thirdLevelReporting() {
    let url = this.baseurl + '/EmployeeOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.ThirdLevelAlldata = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }

  selectThirdLevel(event) {
  }

  DeligationEmpdata() {

    let url = this.baseurl + '/EmployeeOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
      this.Deligationdata = data["result"];
    },
    (err: HttpErrorResponse) => {
    });
  }

  getActiveRoll() {
    let url = this.baseurl + '/RollMaster/Active/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.ActiveRoll = data["result"];
      }, (err: HttpErrorResponse) => {
      });

  }


  onBusiItemSelect(item: any) {
    if (this.selectedBusinessGroup.length == 0) {
      this.selectedBusinessGroup.push(item);
    } else {
      this.selectedBusinessGroup.push(item);
    }
    if (this.selectedBusinessGroup.length > 0) {
      this.flag = true;
      this.flag2 = true;
    }
    else {
      this.flag = false;
      this.flag2 = false;
    }
  }
  onBusiSelectAll(items: any) {
    this.selectedBusinessGroup = items;
  }

  OnBusiItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedBusinessGroup.length; i++) {
      if (this.selectedBusinessGroup[i].busiGroupId == item.busiGroupId) {
        this.selectedBusinessGroup.splice(i, 1);
      }
    }
    if (this.selectedBusinessGroup.length > 0) {
      this.flag = true;
      this.flag2 = true;
    }
    else {
      this.flag = false;
      this.flag2 = false;
    }
  }
  selectClick() {
    if (this.selectedBusinessGroup.length > 0) {
      this.flag = true;
      this.flag2 = true;
    }
    else {
      this.flag = false;
      this.flag2 = false;
    }
  }
  onBusiDeSelectAll(item: any) {
    this.selectedBusinessGroup = [];
  }
  LocationonItemSelect(item: any) {
    if (this.selectedLocationGroup.length == 0) {
      this.selectedLocationGroup.push(item)
    } else {
      //for (var i = 0; i < this.selectedLocation.length; i++) {
      this.selectedLocationGroup.push(item)
    }

    if (this.selectedLocationGroup.length > 0) {
      this.flagl = true;
      this.flagl2 = true;
    }
    else {
      this.flagl = false;
      this.flagl2 = false;
    }
  }
  LocationonSelectAll(items: any) {
    this.selectedLocationGroup = items;
  }

  LocationOnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedLocationGroup.length; i++) {
      if (this.selectedLocationGroup[i].locationId == item.locationId) {
        this.selectedLocationGroup.splice(i, 1);
      }
    }
    if (this.selectedLocationGroup.length > 0) {
      this.flagl = true;
      this.flagl2 = true;
    }
    else {
      this.flagl = false;
      this.flagl2 = false;
    }
  }

  LocationonDeSelectAll(item: any) {
    this.selectedLocationGroup = [];
  }

  FunctiononItemSelect(item: any) {
    if (this.selectedFunctionGroup.length == 0) {
      this.selectedFunctionGroup.push(item);
    } else {
      this.selectedFunctionGroup.push(item);
    }

    if (this.selectedFunctionGroup.length > 0) {
      this.flagf = true;
      this.flagf2 = true;
    }
    else {
      this.flagf = false;
      this.flagf2 = false;
    }
  }
  FunctiononSelectAll(items: any) {
    this.selectedFunctionGroup = items;
  }

  FunctionOnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedFunctionGroup.length; i++) {
      if (this.selectedFunctionGroup[i].functionUnitId == item.functionUnitId) {
        this.selectedFunctionGroup.splice(i, 1);
      }
    }
    if (this.selectedFunctionGroup.length > 0) {
      this.flagf = true;
      this.flagf2 = true;
    }
    else {
      this.flagf = false;
      this.flagf2 = false;
    }
  }

  FunctiononDeSelectAll(item: any) {
    this.selectedFunctionGroup = [];
  }


  DepartmentonItemSelect(item: any) {
    if (this.selectedDepartmentGroup.length == 0) {
      this.selectedDepartmentGroup.push(item);
    } else {
      this.selectedDepartmentGroup.push(item);
    }
    if (this.selectedDepartmentGroup.length > 0) {
      this.flagd = true;
      this.flagd2 = true;
    }
    else {
      this.flagd = false;
      this.flagd2 = false;
    }
  }
  DepartmentonSelectAll(items: any) {
    this.selectedDepartmentGroup = items;
  }

  DepartmentOnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedDepartmentGroup.length; i++) {
      if (this.selectedDepartmentGroup[i].deptId == item.deptId) {
        this.selectedDepartmentGroup.splice(i, 1)
      }
    }
    if (this.selectedDepartmentGroup.length > 0) {
      this.flagd = true;
      this.flagd2 = true;
    }
    else {
      this.flagd = false;
      this.flagd2 = false;
    }
  }

  DepartmentonDeSelectAll(item: any) {
    this.selectedDepartmentGroup = [];
  }


  SubDepartmentonItemSelect(item: any) {
    if (this.selectedSubDepartmentGroup.length == 0) {
      this.selectedSubDepartmentGroup.push(item);
    } else {
      this.selectedSubDepartmentGroup.push(item);
    }
    if (this.selectedSubDepartmentGroup.length > 0) {
      this.flagsub = true;
      this.flagdsub2 = true;
    }
    else {
      this.flagsub = false;
      this.flagdsub2 = false;
    }
  }
  SubDepartmentonSelectAll(items: any) {
    this.selectedSubDepartmentGroup = items;
  }

  SubDepartmentOnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedSubDepartmentGroup.length; i++) {
      if (this.selectedSubDepartmentGroup[i].deptId == item.deptId) {
        this.selectedSubDepartmentGroup.splice(i, 1);
      }
    }
    if (this.selectedSubDepartmentGroup.length > 0) {
      this.flagsub = true;
      this.flagdsub2 = true;
    }
    else {
      this.flagsub = false;
      this.flagdsub2 = false;
    }
  }

  SubDepartmentonDeSelectAll(item: any) {
    this.selectedSubDepartmentGroup = [];
  }

  getobject(data) {
    this.object = parseInt(data.target.value);
    this.selectedObjectRoleMasterId = this.object;
    this.editRoel(this.object);
  }

  editRoel(object) {
    let urledit = this.baseurl + '/RollMaster/GetById/';
    this.httpService.get(urledit + object).subscribe(data => {
        this.RoleHead = data['result']['roleHead'];
      },
      (err: HttpErrorResponse) => {
      });
  }

  savedata(getId) {
    this.HROfficialModel.empContactCellAndEmailInfoId = getId;
  }

  isEdit: boolean;
  getEditData1() {
    let urlget = this.baseurl + '/employeeMasterbyId/' + this.empPerId;
    this.httpService.get(urlget).subscribe((data) => {
        this.editData = data["result"];
      if(this.editData.finalStatus=='Rejected'){
        this.disable_flag=true;
      }else{
        this.disable_flag=false;
      }
        this.getId = this.editData.empContactCellAndEmailInfoId;
        this.savedata(this.getId);
        this.isEdit = true;
        this.fullName = this.editData["fullName"];
        this.HROfficialModel.bioId = this.editData["bioId"];
        this.HROfficialModel.empId = this.editData["empId"];
        this.HROfficialModel.employmentType = this.editData["employmentType"];
        this.HROfficialModel.subEmploymentType = this.editData["subEmploymentType"];
        this.HROfficialModel.skillCatagory = this.editData["skillCatagory"];
        this.HROfficialModel.startDateOfTraining = this.editData["startDateOfTraining"];
        this.HROfficialModel.endDateOfTraining = this.editData["endDateOfTraining"];
        this.HROfficialModel.extensionDateOfTraining = this.editData["extensionDateOfTraining"];
        this.HROfficialModel.startDateOfprobation = this.editData["startDateOfprobation"];
        this.HROfficialModel.probationPeriod = this.editData["probationPeriod"];
        this.HROfficialModel.expectedendDAteOfConfirmation = this.editData["expectedendDAteOfConfirmation"];
        this.HROfficialModel.dateOfConfirmation = this.editData["dateOfConfirmation"];
        this.HROfficialModel.dateOfConfDate = this.editData["dateOfConfDate"];
        this.HROfficialModel.dateOfJoining = this.editData["dateOfJoining"];
        this.HROfficialModel.extenDateOfProb = this.editData["extenDateOfProb"];
        this.HROfficialModel.remarkOfProb = this.editData["remarkOfProb"]
        this.HROfficialModel.extenDateOfTrainee = this.editData["extenDateOfTrainee"];
        this.HROfficialModel.remarkofTrainee = this.editData["remarkofTrainee"];
        this.HROfficialModel.extenDateOfTemp = this.editData["extenDateOfTemp"];
        this.HROfficialModel.remarkofTemp = this.editData["remarkofTemp"];
        this.HROfficialModel.startDateOfTempWorking = this.editData["startDateOfTempWorking"];
        this.HROfficialModel.endDateOfTempWorking = this.editData["endDateOfTempWorking"];
        this.HROfficialModel.sanctioningAuthority = this.editData["sanctioningAuthority"];
        this.selectedroleobj = this.editData.rollMasterId;
        this.editRoel(this.selectedroleobj);
        this.empOfficialId = this.editData.empOfficialId;
        this.HROfficialModel.departmentMasterList = this.editData.departmentMasterList;
        this.HROfficialModel.subDepartMentMasterList = this.editData.subDepartMentMasterList;
        this.empProbationDetailsList = this.editData["empProbationDetailsList"];
        this.empTemporyDetailsList = this.editData["empTemporyDetailsList"];
        this.empTraineeDetailseList = this.editData["empTraineeDetailseList"];
        this.HROfficialModel.documentsSubmitted = this.editData["documentsSubmitted"];
        this.HROfficialModel.documentsVerified = this.editData["documentsVerified"];
        
        this.HROfficialModel.panNo=this.editData["panNo"];

        this.HROfficialModel.adharNo=this.editData["adharNo"];
        this.HROfficialModel.pfNumber=this.editData["pfNumber"];
       this.HROfficialModel.universalAccNo=this.editData["universalAccNo"];
       this.HROfficialModel.esicNumber=this.editData["esicNumber"];
       this.HROfficialModel.offerLetterAndJoiningSatutoryDataId=this.editData["offerLetterAndJoiningSatutoryDataId"];
       this.HROfficialModel.govDocInfoId=this.editData["govDocInfoId"];
      this.HROfficialModel.atdSettingId=this.editData["atdSettingId"];
        this.HROfficialModel.bussinessGroupList = this.editData.bussinessGroupList;
        this.HROfficialModel.locationMasterList = this.editData.locationMasterList;
        this.HROfficialModel.functionUnitMasterList = this.editData.functionUnitMasterList;
        // var cost_center_id = data['result']['companyMaster']['compId'];
        this.selectedBusinessobj = this.editData.busiGroupId;
        this.selectedCoctCenterobj = this.editData.costCenterId;
        this.functiongroup(this.selectedBusinessobj);
        this.selectedFunctionobj = this.editData.functionUnitId;
        this.Departmentgroup();
        this.selectedDepartmentobj = this.editData.deptId;
        this.SubDepartmentgroup(this.selectedDepartmentobj);
        this.selectedSubDepartmentobj = this.editData.subDeptId;
        this.selectedLocationobj = this.editData.locationId;
        this.attendance.weeklyoffdata(this.selectedLocationobj);
        this.selectedDesignationobj = this.editData.descId;
        this.selectedGradeobj = this.editData.gradeId;
        //selectEmptype
        this.selectedEmpTypeobj = this.editData.empTypeId;
        this.subEmpType(this.selectedEmpTypeobj)
        this.selectedSubEmpTypeobj = this.editData.subEmpTypeId;
        this.selectedObjectRoleMasterId = this.editData.rollMasterId;
        this.HROfficialModel.firstLevelReportingManager = this.editData.firstLevelReportingManager;
        this.HROfficialModel.secondLevelReportingManager = this.editData.secondLevelReportingManager;
        this.HROfficialModel.thirdLevelReportingManager = this.editData.thirdLevelReportingManager;

        this.searchQuery = this.editData.firstLevelReportingManagerName;
        this.searchQuery1 = this.editData.secondLevelReportingManagerName;
        this.searchQuery2 = this.editData.thirdLevelReportingManagerName;
        this.searchQuery5 = this.editData.delegatedEmpName;
        this.HROfficialModel.conSerTypeId = this.editData.conSerTypeId;
        this.getContractorData(this.HROfficialModel.conSerTypeId);
        this.HROfficialModel.contractorId = this.editData.contractorId;
        this.HROfficialModel.validStartDate = this.editData.validStartDate;
        this.HROfficialModel.validEndDate = this.editData.validEndDate;
        this.HROfficialModel.empContactCellAndEmailInfoId = this.editData.empContactCellAndEmailInfoId;
        this.shiptgroup();
        this.shiftmastergroup();
        this.weeklyoffdata();
        // this.HROfficialModel.shiftMasterRosterId = this.selectedshiftobj;
        this.HROfficialModel.shiftType = this.editData["shiftType"];
       // console.log("shifttype", this.HROfficialModel.shiftType);
        this.selectedshiftgroupobj = this.editData["shiftGroupId"];
        this.selectedshiftmastergroupobj = this.editData["shiftMasterId"];
        this.weeklyOffType = this.editData.weeklyOffType;
        this.selectedWeeklyGroupobj = this.editData["weeklyOffCalenderMasterId"];
      //   this.HROfficialModel.panNo=this.editData["panNo"];
      //   this.HROfficialModel.adharNo=this.editData["adharNo"];
      //   this.HROfficialModel.pfNumber=this.editData["pfNumber"];
      //  this.HROfficialModel.universalAccNo=this.editData["universalAccNo"];
      //  this.HROfficialModel.esicNumber=this.editData["esicNumber"];
    // this.HROfficialModel.weeklyOffType = this.editData.weeklyOffType;
        this.HROfficialModel.weeklyOffType = this.editData.weeklyOffType;
        this.HROfficialModel.atdSettingId = this.editData["atdSettingId"];
        this.HROfficialModel.empOfficialId = this.editData["empOfficialId"];
        this.HROfficialModel.roasterDate = this.editData["roasterDate"];
  
        if(new Date(this.editData["roasterDate"]).getTime() < new Date().getTime() && this.editData["roasterDate"]){
            this.roasterDateFlag = true;
        }
  
        this.HROfficialModel.shiftPatternId = this.editData["shiftPatternId"];
        
        this.selectedshiftpatternobj = this.editData["shiftPatternId"];
        this.shiftpatternvalue(this.selectedshiftpatternobj);
        this.selectedshiftobj = this.editData["shiftMasterRosterId"];
        this.HROfficialModel.shiftPatternShiftId = this.editData["shiftPatternShiftId"];
        this.HROfficialModel.singlePunchPresent = this.editData["singlePunchPresent"];
        this.HROfficialModel.negativeAttendenceAllowed = this.editData["negativeAttendenceAllowed"];
        this.empOfficialId = this.editData.empOfficialId;
        sessionStorage.setItem('empOfficialId',this.empOfficialId);
        this.locationId = this.editData.locationId



      }, (err: HttpErrorResponse) => {
      });

  }

  startdateoftrainig(event) {
    this.HROfficialModel.startDateOfTraining = event;
  }

  enddateoftrainig(event) {
    this.HROfficialModel.endDateOfTraining = event;
  }

  extensiondateoftraining(event) {
    this.HROfficialModel.extensionDateOfTraining = event;
  }
  employee
  startdateofprobabation(event) {
    this.HROfficialModel.startDateOfprobation = event;
  }

  expectedDateofconfirmation(event) {
    this.HROfficialModel.expectedendDAteOfConfirmation = event;
  }

  dateOfConfirm(event) {
    this.HROfficialModel.dateOfConfDate = event;
  }
  joiningDate(event) {
    this.HROfficialModel.dateOfJoining = event;
    //console.log("dateofjoin",this.HROfficialModel.dateOfJoining);
  }
  saveAlldata(f) {
    if(this.HROfficialModel.firstLevelReportingManager && !this.HROfficialModel.secondLevelReportingManager && !this.HROfficialModel.thirdLevelReportingManager){
        this.HROfficialModel.secondLevelReportingManager=0;
        this.HROfficialModel.thirdLevelReportingManager=0;
    }
    //if selects fisrt and third
    if(this.HROfficialModel.firstLevelReportingManager && !this.HROfficialModel.secondLevelReportingManager && this.HROfficialModel.thirdLevelReportingManager){
      this.toastr.error("please select second level reporting manager");
      this.HROfficialModel.thirdLevelReportingManager=0;
      this.searchQuery2=null;
      return false;
    }
    this.Spinner.show();
   
    this.HROfficialModel.deptId = this.selectedDepartmentobj;
    this.HROfficialModel.subDeptId = this.selectedSubDepartmentobj;
    this.HROfficialModel.subEmpTypeId = this.selectedSubEmpTypeobj;
    this.HROfficialModel.costCenterId = this.selectedCoctCenterobj;
    this.HROfficialModel.locationId = this.selectedLocationobj;
    this.HROfficialModel.descId = this.selectedDesignationobj;
    this.HROfficialModel.gradeId = this.selectedGradeobj;
    this.HROfficialModel.fullName = this.getAllBasicInfo.fullName;

    this.HROfficialModel.officialContactNumber = this.getAllBasicInfo.officialContactNumber;
    this.HROfficialModel.officialEmailId = this.getAllBasicInfo.officialEmailId;

    this.HROfficialModel.middleName = this.getAllBasicInfo.middleName;
    this.HROfficialModel.lastName = this.getAllBasicInfo.lastName;
    this.HROfficialModel.gender = this.getAllBasicInfo.gender;
    this.HROfficialModel.dateOfBirth = this.getAllBasicInfo.dateOfBirth;
    this.HROfficialModel.age = this.getAllBasicInfo.age;
    this.HROfficialModel.personalContactNumber = this.getAllBasicInfo.personalContactNumber;
    this.HROfficialModel.personalEmailId = this.getAllBasicInfo.personalEmailId;
    this.HROfficialModel.salutation = this.getAllBasicInfo.salutation;
    this.HROfficialModel.rollMasterId = this.selectedObjectRoleMasterId;
    this.HROfficialModel.empContactCellAndEmailInfoId = this.getId;
    this.HROfficialModel.empTypeId = this.selectedEmpTypeobj;
    this.HROfficialModel.subEmpTypeId = this.selectedSubEmpTypeobj;
    if(this.HROfficialModel.shiftType == "General"){
      this.HROfficialModel.shiftMasterId = this.selectedshiftmastergroupobj;
      this.HROfficialModel.shiftGroupId = null;
      this.HROfficialModel.shiftPatternId = null;
      this.HROfficialModel.shiftMasterRosterId = null;
  }
  if(this.HROfficialModel.shiftType == "Custom"){
      this.HROfficialModel.shiftGroupId = this.selectedshiftgroupobj;
      this.HROfficialModel.shiftMasterId = null;
      this.HROfficialModel.shiftPatternId = null;
      this.HROfficialModel.shiftMasterRosterId = null;
  }
  if(this.HROfficialModel.shiftType == "Roster"){
      this.HROfficialModel.shiftPatternId = this.selectedshiftpatternobj;
      this.HROfficialModel.shiftMasterRosterId = this.selectedshiftobj;
      this.HROfficialModel.shiftMasterId = null;
      this.HROfficialModel.shiftGroupId = null;
  }
  this.HROfficialModel.weeklyOffCalenderMasterId = this.selectedWeeklyGroupobj;
  
  this.HROfficialModel.shiftPatternShiftId = this.selectedshiftobj;
  //this.HROfficialModel.empPerId = sessionStorage.getItem('empPersonId');
  this.HROfficialModel.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
  this.HROfficialModel.atdSettingId = this.atdSettingId;
  this.HROfficialModel.weeklyOffType = this.weeklyOffType;
  
  this.HROfficialModel.panNo= this.HROfficialModel.panNo;
  this.HROfficialModel.adharNo=this.HROfficialModel.adharNo;
  this.HROfficialModel.pfNumber=this.HROfficialModel.pfNumber;
  this.HROfficialModel.universalAccNo= this.HROfficialModel.universalAccNo;
  this.HROfficialModel.esicNumber=this.HROfficialModel.esicNumber;
    this.HROfficialservice.postHROfficialInfo(this.HROfficialModel).subscribe(data => {
        //this.callParent();
        this.empOfficialId = data['result']['empOfficialId'];
        sessionStorage.setItem('empOfficialId', this.empOfficialId);
        this.generateempPersonId = data['result']['empPerId'];
        //console.log("emperifid",this.generateempPersonId  )
        sessionStorage.setItem('empPersonId', this.generateempPersonId);
        sessionStorage.setItem('weeklyCalendarId', this.weeklyCalendarId);
        this.attendance.weeklyLocationID = this.weeklyCalendarId;
        this.Spinner.hide()
      
        this.toastr.success('Employee Created Successfully!');
        this.router.navigate(['/layout/employeelist/employeelist'])
        .then(()=>{
          window.location.reload();

        });
        f.reset();
      },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Please wait');
        });
  }

  updatedata(HROfficialModel:any) {
    if(this.HROfficialModel.firstLevelReportingManager && !this.HROfficialModel.secondLevelReportingManager && !this.HROfficialModel.thirdLevelReportingManager){
        this.HROfficialModel.secondLevelReportingManager=0;
        this.HROfficialModel.thirdLevelReportingManager=0;
    }
    //if selects fisrt and third
    if(this.HROfficialModel.firstLevelReportingManager && !this.HROfficialModel.secondLevelReportingManager && this.HROfficialModel.thirdLevelReportingManager){
      this.toastr.error("please select second level reporting manager");
      this.HROfficialModel.thirdLevelReportingManager=0;
      this.searchQuery2=null;
      return false;
    }
    this.Spinner.show();
    this.HROfficialModel.deptId = this.selectedDepartmentobj;
    this.HROfficialModel.subDeptId = this.selectedSubDepartmentobj;
    this.HROfficialModel.subEmpTypeId = this.selectedSubEmpTypeobj;
    this.HROfficialModel.costCenterId = this.selectedCoctCenterobj;
    this.HROfficialModel.locationId = this.selectedLocationobj;
    this.HROfficialModel.descId = this.selectedDesignationobj;
    this.HROfficialModel.gradeId = this.selectedGradeobj;
    this.HROfficialModel.empTypeId = this.selectedEmpTypeobj;
    this.HROfficialModel.subEmpTypeId = this.selectedSubEmpTypeobj;
    this.HROfficialModel.empPerId = this.empPerId;
    this.HROfficialModel.empOfficialId = this.empOfficialId;
    this.HROfficialModel.rollMasterId = this.selectedObjectRoleMasterId;
    this.HROfficialModel.fullName = this.getAllBasicInfo.fullName;
    //this.HROfficialModel.firstName =  this.getAllBasicInfo.firstName;
    this.HROfficialModel.middleName = this.getAllBasicInfo.middleName;
    this.HROfficialModel.lastName = this.getAllBasicInfo.lastName;
    this.HROfficialModel.gender = this.getAllBasicInfo.gender;
    this.HROfficialModel.dateOfBirth = this.getAllBasicInfo.dateOfBirth;
    this.HROfficialModel.age = this.getAllBasicInfo.age;
    this.HROfficialModel.personalContactNumber = this.getAllBasicInfo.personalContactNumber;
    this.HROfficialModel.personalEmailId = this.getAllBasicInfo.personalEmailId;
    this.HROfficialModel.salutation = this.getAllBasicInfo.salutation;
    this.HROfficialModel.officialContactNumber = this.getAllBasicInfo.officialContactNumber;
    this.HROfficialModel.officialEmailId = this.getAllBasicInfo.officialEmailId;
    // this.HROfficialModel.firstLevelReportingManager = this.selectedFirstLevelobj;
    // this.HROfficialModel.secondLevelReportingManager = this.selectedsecondLevelobj;
    this.HROfficialModel.empContactCellAndEmailInfoId = this.getAllBasicInfo.empContactCellAndEmailInfoId;
    // this.HROfficialModel.thirdLevelReportingManager = this.selectedThirdLevelobj;

    this.HROfficialModel.empContactCellAndEmailInfoId = this.getId;
    if(this.HROfficialModel.shiftType == "General"){
      this.HROfficialModel.shiftMasterId = this.selectedshiftmastergroupobj;

      this.HROfficialModel.shiftGroupId = null;
      this.HROfficialModel.shiftPatternId = null;
      this.HROfficialModel.shiftMasterRosterId = null;
  }
  if(this.HROfficialModel.shiftType == "Custom"){
      this.HROfficialModel.shiftGroupId = this.selectedshiftgroupobj;

      this.HROfficialModel.shiftMasterId = null;
      this.HROfficialModel.shiftPatternId = null;
      this.HROfficialModel.shiftMasterRosterId = null;
  }
  if(this.HROfficialModel.shiftType == "Roster"){
      this.HROfficialModel.shiftPatternId = this.selectedshiftpatternobj;
      this.HROfficialModel.shiftMasterRosterId = this.selectedshiftobj;
  }

  this.shiptgroup();
  this.shiftmastergroup();
  // this.HRattendeceModel.shiftMasterRosterId = this.selectedshiftobj;
  this.HROfficialModel.shiftType =  this.HROfficialModel.shiftType
 // console.log("shifttype", this.HRattendeceModel.shiftType);
  this.selectedshiftgroupobj = this.editData["shiftGroupId"];
  this.selectedshiftmastergroupobj = this.editData["shiftMasterId"];
  this.weeklyOffType = this.editData.weeklyOffType;
  // this.selectedWeeklyGroupobj = this.editData["weeklyOffCalenderMasterId"];

// this.HRattendeceModel.weeklyOffType = this.editData.weeklyOffType;
  this.HROfficialModel.weeklyOffType = this.editData.weeklyOffType;
  this.HROfficialModel.atdSettingId = this.editData["atdSettingId"];
  this.HROfficialModel.empOfficialId = this.editData["empOfficialId"];
  this.HROfficialModel.roasterDate = this.editData["roasterDate"];

  if(new Date(this.editData["roasterDate"]).getTime() < new Date().getTime() && this.editData["roasterDate"]){
      this.roasterDateFlag = true;
  }

  this.HROfficialModel.shiftPatternId = this.editData["shiftPatternId"];
  
  this.selectedshiftpatternobj = this.editData["shiftPatternId"];
  this.shiftpatternvalue(this.selectedshiftpatternobj);
  this.selectedshiftobj = this.editData["shiftMasterRosterId"];
  this.HROfficialModel.shiftPatternShiftId = this.editData["shiftPatternShiftId"];
  this.HROfficialModel.singlePunchPresent = this.editData["singlePunchPresent"];
  this.HROfficialModel.negativeAttendenceAllowed = this.editData["negativeAttendenceAllowed"];
  this.empOfficialId = this.editData.empOfficialId;
  sessionStorage.setItem('empOfficialId',this.empOfficialId);
  this.locationId = this.editData.locationId

  this.HROfficialModel.weeklyOffCalenderMasterId = this.selectedWeeklyGroupobj;
  this.HROfficialModel.empOfficialId = this.empOfficialId;
  this.HROfficialModel.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
  this.HROfficialModel.weeklyOffType = this.weeklyOffType;
  this.HROfficialModel.panNo= this.HROfficialModel.panNo;

  this.HROfficialModel.adharNo=this.HROfficialModel.adharNo;
  this.HROfficialModel.pfNumber=this.HROfficialModel.pfNumber;
 this.HROfficialModel.universalAccNo=this.HROfficialModel.universalAccNo;
 this.HROfficialModel.esicNumber=this.HROfficialModel.esicNumber;
 this.HROfficialModel.offerLetterAndJoiningSatutoryDataId=this.editData["offerLetterAndJoiningSatutoryDataId"];
 this.HROfficialModel.govDocInfoId=this.editData["govDocInfoId"];
this.HROfficialModel.atdSettingId=this.editData["atdSettingId"];
    this.HROfficialservice.updateHROfficialInfo(this.HROfficialModel).subscribe(data => {
        this.empOfficialId = data.json()['result']['empOfficialId'];
        sessionStorage.setItem('empOfficialId', this.empOfficialId);
        this.attendance.weeklyLocationID = this.weeklyCalendarId;
        
        //this.callParent();
        this.Spinner.hide()
         this.toastr.success('Empolyee Updated Successfully!');
         this.router.navigate(['/layout/employeelist/employeelist'])
        //  .then(()=>{
        //   window.location.reload();
        //  });
        //sessionStorage.setItem('preonboarding', 'emp')
      },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('please wait');
        });

  }

 

  filterItem(value) {
    if(this.loggedUser.roleHead=='HR'){
    if (!value) {
      this.empFlag = false;
      this.searchQuery = null;
      this.HROfficialModel.firstLevelReportingManager = this.searchQuery;

    }
    if (value) {
      // this.questionData = temp;
      this.HROfficialservice.getSearchSuggestionLists(value,this.loggedUser.locationId).subscribe(data => {
        this.FirstLevelAlldata = data.result
        this.empFlag = true
      })
    }
  }
  else if(this.loggedUser.roleHead=='Admin'){
    if (!value) {
      this.empFlag = false;
      this.searchQuery = null;
      this.HROfficialModel.firstLevelReportingManager = this.searchQuery;

    }
    if (value) {
      // this.questionData = temp;
      this.HROfficialservice.getSearchSuggestionLists(value, this.selectedLocationobj).subscribe(data => {
        this.FirstLevelAlldata = data.result
        this.empFlag = true
      })
    }
  }
  }

  filterItem1(value) {
    if (!value) {
      this.empFlag1 = false;
      this.searchQuery1 = null;
      this.HROfficialModel.secondLevelReportingManager = this.searchQuery1;

    }
    if (value) {
      // this.questionData = temp;
      this.empFlag1 = true;
      this.HROfficialservice.getSearchSuggestionList(value).subscribe(data => {
        this.SecondLevelAlldata = data.result
      })
    }
  }

  filterItem2(value) {
    if (!value) {
      this.empFlag2 = false;
      this.searchQuery2 = null
      this.HROfficialModel.thirdLevelReportingManager = this.searchQuery2;

    }
    if (value) {
      // this.questionData = temp;
      this.empFlag2 = true;
      this.HROfficialservice.getSearchSuggestionList(value).subscribe(data => {
        this.ThirdLevelAlldata = data.result
      })
    }
  }

  filterItem5(value) {
    if (!value) {
      this.empFlag5 = false;
      this.searchQuery5 = null
      this.HROfficialModel.delempId = this.searchQuery5;

    }
    if (value) {
      // this.questionData = temp;
      this.empFlag5 = true;
      this.HROfficialservice.getSearchSuggestionList(value).subscribe(data => {
        this.Deligationdata = data.result
      })
    }
  }

  searchEmpId: any;
  suggestThis(item) {

    if (this.searchQuery1 == item.fullName + ' ' + item.lastName || this.searchQuery2 == item.fullName + ' ' + item.lastName) {
      this.toastr.error('Already selected..!', 'Manager');
    }
    else {
      this.searchQuery = item.fullName + ' ' + item.lastName;
      this.searchEmpId = item.empId;
      this.HROfficialModel.empId1 = item.empId;
      this.HROfficialModel.firstLevelReportingManager = item.empId;
      this.firstlevelmanager = item.fullName + ' ' + item.lastName
      this.empFlag = false;
    }

  }

  searchEmpId1: any;
  suggestThis1(item) {

    if (this.searchQuery == item.fullName + ' ' + item.lastName || this.searchQuery2 == item.fullName + ' ' + item.lastName) {
      this.toastr.error('Already selected..!', 'Manager');
    }
    else {
      this.searchQuery1 = item.fullName + ' ' + item.lastName;
      this.searchEmpId1 = item.empId;
      this.HROfficialModel.empId2 = item.empId;
      this.HROfficialModel.secondLevelReportingManager = item.empId;
      this.secondlevelmanager = item.fullName + ' ' + item.lastName
      this.empFlag1 = false;
    }
  }

  searchEmpId2: any;
  suggestThis2(item) {
    // 
    if (this.searchQuery == item.fullName + ' ' + item.lastName || this.searchQuery1 == item.fullName + ' ' + item.lastName) {
      this.toastr.error('Already selected..!', 'Manager');
    } else {
      this.searchQuery2 = item.fullName + ' ' + item.lastName;
      this.searchEmpId2 = item.empId;
      this.HROfficialModel.empId3 = item.empId;
      this.HROfficialModel.thirdLevelReportingManager = item.empId;
      this.thirdlevelmanager = item.fullName + ' ' + item.lastName
      this.empFlag2 = false;
    }

  }

  searchEmpId5: any;
  suggestThis5(item) {

    this.searchQuery5 = item.fullName + ' ' + item.lastName;
    this.searchEmpId5 = item.empId;
    this.HROfficialModel.delempId = item.empId;
    this.delegationEmp = item.fullName + ' ' + item.lastName
    this.empFlag5 = false;
  }

  contractorTypeData = [];
  getContractorType() {
    let url = this.baseurl + '/CosSerType/Active/';
    this.httpService.get(url + this.compId).subscribe((data: any) => {
        this.contractorTypeData = data.result;
        this.getContractorData(data.result[0].conSerTypeId)
      },
      (err: HttpErrorResponse) => {
      });
  }
  contractorNamesData = [];
  getContractorData(conSerTypeId) {
    let url = this.baseurl + '/Contractor/Active/';
    this.httpService.get(url + conSerTypeId).subscribe((data: any) => {
        this.contractorNamesData = data.result;
      },
      (err: HttpErrorResponse) => {

      });
  }

  arrowkeyLocation = 0;
  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }

  arrowkeyLocation1 = 0;
  keyDown1(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation1--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation1++;
        break;
    }
  }

  arrowkeyLocation2 = 0;
  keyDown2(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: //this is the ascii of arrow up
        this.arrowkeyLocation2--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation2++;
        break;
    }
  }

  arrowkeyLocation5 = 0;
  keyDown5(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: //this is the ascii of arrow up
        this.arrowkeyLocation5--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation5++;
        break;
    }
  }
  onChange(event) {
    this.GetEmployeeId(event);
  }

  getempData = { message: "" };
  flaggets: boolean = false;
  hidebuttn: boolean = false
  GetEmployeeId(empId) {
    let url1 = this.baseurl + '/duplicateempid?compId=' + this.compId + "&empId=" + empId;
    this.flaggets = false;
    this.hidebuttn = false
    if (this.isEditHR == 'true') {
      if (this.editData["empId"] != empId) {
        this.httpService.get(url1).subscribe((data: any) => {
            this.flaggets = true;
            this.getempData = data;
            this.hidebuttn = true;

          }, (err: HttpErrorResponse) => {
            this.flaggets = false;
            this.hidebuttn = false;
          });
      }
    } else {

      this.httpService.get(url1).subscribe((data: any) => {
          this.flaggets = true;
          this.getempData = data;
          this.hidebuttn = true;
        }, (err: HttpErrorResponse) => {
          this.flaggets = false;
          this.hidebuttn = false;
        });
    }
  }


  OnChangeBioId(event) {
    this.getBioId(event);
  }

  getbioData = { message: "" };
  flagBio: boolean = false;
  getBioId(bioId) {
    let url1 = this.baseurl + '/duplicatebioid?bioId=' + bioId + "&compId=" + this.compId;
    this.flagBio = false;
    this.hidebuttn = false;
    if (this.isEditHR == 'true') {
      if (this.editData["bioId"] != bioId) {
        this.httpService.get(url1).subscribe((data: any) => {
            this.flagBio = true;
            this.getbioData = data;
            this.hidebuttn = true;
          }, (err: HttpErrorResponse) => {
            this.flagBio = false;
            this.hidebuttn = false;
          });
      }
    }
    else {
      this.httpService.get(url1).subscribe((data: any) => {
          this.flagBio = true;
          this.getbioData = data;
          this.hidebuttn = true;

        }, (err: HttpErrorResponse) => {
          this.flagBio = false;
          this.hidebuttn = false;
        });
    }

  }

  searchQueryFirst: string = "";
  searchQuerySecond: string = "";
  searchQueryThird: string = "";
  empFlagFrist: boolean = false;
  empFlagSecond: boolean = false;
  empFlagThird: boolean = false;
  firstLevelSanctionData = [];
  secondLevelSanctionData = [];
  thirdLevelSanctionData = [];
  filterItemFirst(value) {
    if (!value) {
      this.empFlagFrist = false;
      this.searchQueryFirst = null;
      this.HROfficialModel.firstLevelReportingManager = this.searchQueryFirst;

    }
    if (value) {
      this.HROfficialservice.getSearchSuggestionList(value).subscribe(data => {
        this.firstLevelSanctionData = data.result
        this.empFlagFrist = true
      })
    }
  }

  filterItemSecond(value) {
    if (!value) {
      this.empFlagSecond = false;
      this.searchQuerySecond = null;
      this.HROfficialModel.secondLevelReportingManager = this.searchQuerySecond;
    }
    if (value) {
      this.empFlagSecond = true;
      this.HROfficialservice.getSearchSuggestionList(value).subscribe(data => {
        this.secondLevelSanctionData = data.result
      })
    }
  }

  filterItemThird(value) {
    if (!value) {
      this.empFlagThird = false;
      this.searchQueryThird = null
      this.HROfficialModel.thirdLevelReportingManager = this.searchQueryThird;

    }
    if (value) {
      this.empFlagThird = true;
      this.HROfficialservice.getSearchSuggestionList(value).subscribe(data => {
        this.thirdLevelSanctionData = data.result
      })
    }
  }
searchEmpIdFirst: any;
suggestThisFirst(item) {

  if (this.searchQuery1 == item.fullName +' '+item.lastName || this.searchQuery2 == item.fullName +' '+item.lastName) {
    this.toastr.error('Already selected..!', 'Manager');
  }
  else {
    this.searchQueryFirst = item.fullName+' '+item.lastName;
    this.searchEmpIdFirst = item.empId;
    this.empFlagFrist = false;
  }

}

searchEmpIdSecond: any;
suggestThisSecond(item) {

  if (this.searchQuery == item.fullName+' '+item.lastName || this.searchQuery2 == item.fullName+' '+item.lastName) {
    this.toastr.error('Already selected..!', 'Manager');
  }
  else {
    this.searchQuerySecond = item.fullName+' '+item.lastName;
    this.searchEmpIdSecond = item.empId;
    this.empFlagSecond = false;
  }
}

searchEmpIdThird: any;
suggestThisThird(item) {
  if (this.searchQuery == item.fullName+' '+item.lastName || this.searchQuery1 == item.fullName+' '+item.lastName) {
    this.toastr.error('Already selected..!', 'Manager');
  } else {
    this.searchQuerySecond = item.fullName+' '+item.lastName;
    this.searchEmpIdThird = item.empId;
    this.empFlagThird = false;
  }
}






shiptgroup() {
 
      
  //this.selectedLocationId=e;
  if(this.loggedUser.roleHead=='HR'){
  let url = this.baseurl + '/ShiftGroup/Active/';
  this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
      this.Shiftgroupdata = data["result"];
     // console.log(" Shiftgroupdata*******", this.Shiftgroupdata)
  })
  }    
  else if(this.loggedUser.roleHead=='Admin'){
     
      let url = this.baseurl + '/ShiftGroup/Active/';
      this.httpService.get(url + this.compId+'/'+this.selectedLocationobj).subscribe(data => {
          this.Shiftgroupdata = data["result"];
        // console.log(" Shiftgroupdata1233333", this.Shiftgroupdata)

  })
  }
}
weeklyoffdata() {
  
  //this.selectedlocationids=e;
  let compId = this.compId;
  this.WeeklyOffcalendarData = [];
  if(this.loggedUser.roleHead=='HR'){
  let url1 = this.baseurl + '/weeklyOfMasterActive/';
  this.httpService.get(url1 + compId+'/'+this.loggedUser.locationId).subscribe(data => {
      this.WeeklyOffcalendarData = data["result"];
     // console.log("attdancedata",   this.WeeklyOffcalendarData);
  }) 
 
}
else if(this.loggedUser.roleHead=='Admin'){
  //console.log("adminlocoid",this.selectedLocationobj)
  let url1 = this.baseurl + '/weeklyOfMasterActive/';
  this.httpService.get(url1 + compId+'/'+this.selectedLocationobj).subscribe(data => {
      this.WeeklyOffcalendarData = data["result"];
     // console.log("attdancedata",   this.WeeklyOffcalendarData);
  }) 
 

}



}




shiftgroupvalue(event) {
  this.selectedshiftgroupobj = parseInt(event.target.value);
}


shiftmastergroup() {

  if(this.loggedUser.roleHead=='HR'){
    let url = this.baseurl + '/ShiftMaster/ByActive/';
    this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
        this.Shiftmastergroupdata = data["result"];
        //console.log(" Shiftgroupdatagenral", this.Shiftgroupdata)
    })
    }    
    else if(this.loggedUser.roleHead=='Admin'){
      //console.log("locationid",this.selectedLocationobj);
       
        let url = this.baseurl + '/ShiftMaster/ByActive/';
        this.httpService.get(url + this.compId+'/'+this.selectedLocationobj).subscribe(data => {
            this.Shiftmastergroupdata = data["result"];
           //console.log(" Shiftgroupdatagenral", this.Shiftgroupdata)
  
    })
    }
}

shiftgroupmastervalue(event) {
  this.selectedshiftmastergroupobj = parseInt(event.target.value)
}

shiftPattern(){
  let url = this.baseurl + '/shiftMasterPatternActive/';
  this.httpService.get(url + this.compId).subscribe(data => {
  this.ShiftPatterndata = data["result"];
  }, (err: HttpErrorResponse) => {
  });
}

shiftpatternvalue(event){
  this.selectedshiftpatternobj = parseInt(event)
  this.getShift();
}

getShift(){

  let url = this.baseurl + '/shiftMasterPatternById/';
  this.httpService.get(url + this.selectedshiftpatternobj).subscribe(data => {
          this.ShiftPatternshiftdata = data["result"];
          this.sequenceOne = this.ShiftPatternshiftdata['sequenceOne'];
          this.shiftName1 = this.ShiftPatternshiftdata['shiftName1'];
          this.sequencetwo = this.ShiftPatternshiftdata['sequencetwo'];
          this.shiftName2 = this.ShiftPatternshiftdata['shiftName2'];
          this.sequencethree = this.ShiftPatternshiftdata['sequencethree'];
          this.shiftName3 = this.ShiftPatternshiftdata['shiftName3'];

      },(err: HttpErrorResponse) => {
      });
}

shift(event){
  this.selectedshiftobj = parseInt(event.target.value)
}

dateOfroaster(event){
  this.HROfficialModel.roasterDate = event;
}



Weeklycalendervalue(event) {
  
  this.selectedWeeklyGroupobj = parseInt(event.target.value)
}

roasterDateFlag : boolean = false;
// getEditData() {

//   let urledit = this.baseurl + '/employeeMasterbyId/' + this.empPerId;
//   this.httpService.get(urledit).subscribe((data) => {
//       this.editData = data["result"];





//       this.shiptgroup();
//       this.shiftmastergroup();
//       // this.HROfficialModel.shiftMasterRosterId = this.selectedshiftobj;
//       this.HROfficialModel.shiftType = this.editData["shiftType"];
//      // console.log("shifttype", this.HROfficialModel.shiftType);
//       this.selectedshiftgroupobj = this.editData["shiftGroupId"];
//       this.selectedshiftmastergroupobj = this.editData["shiftMasterId"];
//       this.weeklyOffType = this.editData.weeklyOffType;
//       this.selectedWeeklyGroupobj = this.editData["weeklyOffCalenderMasterId"];
//       this.HROfficialModel.panNo=this.editData["panNo"];
//       this.HROfficialModel.adharNo=this.editData["adharNo"];
//       this.HROfficialModel.pfNumber=this.editData["pfNumber"];
//      this.HROfficialModel.universalAccNo=this.editData["universalAccNo"];
//      this.HROfficialModel.esicNumber=this.editData["esicNumber"];
//   // this.HROfficialModel.weeklyOffType = this.editData.weeklyOffType;
//       this.HROfficialModel.weeklyOffType = this.editData.weeklyOffType;
//       this.HROfficialModel.atdSettingId = this.editData["atdSettingId"];
//       this.HROfficialModel.empOfficialId = this.editData["empOfficialId"];
//       this.HROfficialModel.roasterDate = this.editData["roasterDate"];

//       if(new Date(this.editData["roasterDate"]).getTime() < new Date().getTime() && this.editData["roasterDate"]){
//           this.roasterDateFlag = true;
//       }

//       this.HROfficialModel.shiftPatternId = this.editData["shiftPatternId"];
      
//       this.selectedshiftpatternobj = this.editData["shiftPatternId"];
//       this.shiftpatternvalue(this.selectedshiftpatternobj);
//       this.selectedshiftobj = this.editData["shiftMasterRosterId"];
//       this.HROfficialModel.shiftPatternShiftId = this.editData["shiftPatternShiftId"];
//       this.HROfficialModel.singlePunchPresent = this.editData["singlePunchPresent"];
//       this.HROfficialModel.negativeAttendenceAllowed = this.editData["negativeAttendenceAllowed"];
//       this.empOfficialId = this.editData.empOfficialId;
//       sessionStorage.setItem('empOfficialId',this.empOfficialId);
//       this.locationId = this.editData.locationId
//       }, (err: HttpErrorResponse) => {
//       });

// }



storeInfo(f) {
  if (this.isEditHR == 'true' || this.isEditAdmin == 'true' ) {
      this.updatedata('');
  } else {
      this. saveAlldata(f);
  }
}
}
