import { Routes, RouterModule } from '@angular/router'

import { PimshrComponent } from 'src/app/pimshr/pimshr.component';
import { HrpimsbasicformComponent } from './hrpimsbasicform/hrpimsbasicform.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: HrpimsbasicformComponent,

            children:[
                {
                    path:'pimshr',
                    component: HrpimsbasicformComponent
                }

            ]
    },
    {
        path : 'details',
        component : PimshrComponent,
        
    }
 
]

export const routing = RouterModule.forChild(appRoutes);