export class HRattendencesetting{
  atdSettingId: number;
  shiftType: string;
  shiftGroupId: number;
  shiftMasterId: number;
  weeklyOffType: string;
  weeklyOffCalenderMasterId: number;
  empOfficialId: number;
  singlePunchPresent: boolean;
  negativeAttendenceAllowed: boolean;
  empPerId: string;
  logEmpOffId:any;
  roasterDate: string;
  shiftPatternId: number;
  shiftPatternShiftId: number;
    shiftMasterRosterId: any;
    shiftName: any;
    panNo: any;
    adharNo: any;
    pfNumber: any;
    universalAccNo: any;
    esicNumber: any;
  }