export class HRempOthersmodeldata {
  // a:any;
  empPerId: number;
  variablePay: boolean;
  ptApplcable: string;
  pfApplicable: string;
  pfNumber: string;
  payMode: string;
  ptLocation: string;
  esicApplicable: string;
  esicNumber: string;
  payGroupId: number;
  empOfferLetterList: any;
  dateOfOffer: any;
  position: string;
  offeredBy: string;
  confirmationDueDate1: any;
  packageAccordingToOffer: string;
  status: string;
  dateOfJoining: any;
  dateOfConfDate: any;
  isJoined: boolean;
  documentsSubmitted: boolean;
  documentsVerified: boolean;
  appointmentLetterIsProcessed: boolean;
  empVerificationReferencesList: any;
  empGivenResourcesList: any;
  empOfficialId: number;
  offerLetterAndJoiningSatutoryDataId: any;
  logEmpOffId: any;
  offerLetterId: any;
  joiningDetailsId: any;
  // dateOfConfDate : any;
  personalAccidentPol: string;
  notApplicable: string;
  mediclaimaApplicable: string;

  policyApplicable: String;
  undermediclaimMembrs: string;
  amtCovrmediclaim: Number;
  //added by priyanka k start
  transportationMode: string;
  busStopName: string;
  busStopId: any;
  busRouteId:any;
  busCode: string
  busId: number;
  transportPurpose: string;
  vehicalCapacity: number;
  //added by priyanka k end
  bonusApplicable: any;
  exgratia: any;
  transportDetailsId: number;
  busModellist:any;
  busStopllist:any;

  droPoint: string;
  pickupPoint: string;
  busRouteName:string;


  // transportationMode:string;
}
