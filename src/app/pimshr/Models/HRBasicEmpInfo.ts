export class HRBasicdata{
        empPerId: number;
        fullName: string;
        firstName: string;
        middleName: string;
        lastName: string;
        gender: string;
        // dateOfBirth: any;
        age: number;
        personalContactNumber: string;
        personalEmailId: string;
        salutation: string;
        logEmpOffId:any;
        // isFresher: boolean;
        officialEmailId:any;
        officialContactNumber:number;
    }
