import { Component, OnInit } from '@angular/core';
import { MainURL } from './../shared/configurl';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";


@Component({
  selector: 'app-pimshr',
  templateUrl: './pimshr.component.html',
  styleUrls: ['./pimshr.component.css']
})
export class PimshrComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  parentData : any;
  themeColor : string = "nav-pills-blue";
  empPerId: string;
  editData: any;
  fullName: any;
  bioId: any;
  empId: any;
  lastName: any;
  a: boolean;
  profilePath: any;
  condition : boolean = false;
  condition2 : boolean = false;
  condition3 : boolean = false;
  isDisabled:boolean =true;
  isEditHR:any
    constructor( public httpService: HttpClient) {
    let themeColor = sessionStorage.getItem('themeColor');
    this.themeColor = themeColor;
    this.a = true;
    this.isEditHR = sessionStorage.getItem('isEditHR');
   }

  ngOnInit() {
    this.empPerId = sessionStorage.getItem('EmpDataDetails')
    this.getEditData()
  }
   getEditData(){
    let urlget = this.baseurl + '/employeeMasterbyId/'+this.empPerId;
    this.httpService.get(urlget).subscribe((data) => {
    this.editData = data["result"]; 
    this.fullName = this.editData["fullName"];
    this.bioId = this.editData["bioId"];
    this.empId = this.editData["empId"];
    this.lastName = this.editData["lastName"];
    this.profilePath=this.editData["profilePath"];
    this.condition = true;
    this.condition2 = true;
    this.condition3 = true;
  }, (err: HttpErrorResponse) => {
  });
}
 isEdit:any
  goToAttendanceDetails(e){
    this.condition = true;
    setTimeout(() => {
      document.getElementById('attendancedetails-tab').click();
    }, 200);  
  } 
    goToSalaryDetails(e){
      this.condition2 = true;
      setTimeout(() => {
        document.getElementById('salarydetails-tab').click();
      }, 200);
    }

    goToOthers(e){
      this.condition3 = true;
      setTimeout(() => {
        document.getElementById('otherdetails-tab').click();
      }, 200);
    }
    ngOnDestroy(){
      sessionStorage.setItem('isEditHR','false');
    }

    getWeeklyOffLocation(event){
     this.parentData = event;
    }
}