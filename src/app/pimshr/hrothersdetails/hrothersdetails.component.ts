import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HRempOthersmodeldata } from '../Models/HRempOthers';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../../shared/configurl';
import { Router } from '@angular/router';
import { HRAttendenceDataService } from '../Services/HRservice';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-hrothersdetails',
  templateUrl: './hrothersdetails.component.html',
  styleUrls: ['./hrothersdetails.component.css']
})
export class HrothersdetailsComponent implements OnInit {
  @Output() someEvent2 = new EventEmitter<string>();
  HRempOtherModel: HRempOthersmodeldata;
  baseurl = MainURL.HostUrl;
  paygroupdata: any;
  compId: any;
  selectedpaygroup: any;
  selectUndefinedOptionValue: string
  Offerobject: any = {};
  verificationobject: any = {};
  addReourceObject: any = {}
  addRes: any = {}
  resName: any;
  // allOfferdetail = [];
  allVerification = [];
  addResources = [];
  resources=[];

  empOfficialId: any;
  loggedUser: any;
  editData: any = [];
  verificationReferencesId: any;
  empPerId: any;
  updateButton: boolean = false
  isEditHR: any;

  buttonDisabled: boolean;
  buttonDisabledverification: boolean;
  ptApplcable: boolean;
  pfApplicable: boolean;
  esicApplicable: boolean;
  joiningDetailsId: any;
  offerLetterId: any;
  resourcesId: any;
  resEdit = false;
  TypeOfesicApplicable: any;
  mediclaimaApplicable: boolean;
  // priyanka k

  busStoplist: any;
  busdata: any;
  locationId: any;
  busStopId: any;
  busStopIds: number;
  busId: any;
  busesid: number;
  busRouteId: string;


  constructor(public HRempServe: HRAttendenceDataService,
    public httpService: HttpClient,
    public toastr: ToastrService,
    public Spinner: NgxSpinnerService,
    public router: Router) {
    this.buttonDisabledverification = null;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.isEditHR = sessionStorage.getItem('isEditHR')
    this.HRempOtherModel = new HRempOthersmodeldata();
    this.HRempOtherModel.logEmpOffId = this.loggedUser.empOfficialId
    this.HRempOtherModel.dateOfOffer = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.HRempOtherModel.confirmationDueDate1 = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.addReourceObject.givenDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.HRempOtherModel.payMode = "SelectpayMode";
    this.verificationobject.feedbackReceived = "SelectFeedback";
    this.addReourceObject.resourcesId = "";
    this.HRempOtherModel.status = "Selectstatus";
    this.HRempOtherModel.ptLocation = "selectPtlocation";
    this.HRempOtherModel.policyApplicable = 'esic';
    // added by priyanka k
    this.getbusStopName();
    this.getAllBusdata();
  }

  callParent() {
    this.someEvent2.next('from attendance');
  }

  ngOnInit() {
    this.empPerId = sessionStorage.getItem('EmpDataDetails')
    this.paygroupactive();
    this.getEditData();
    this.getList();
    this.HRempOtherModel.policyApplicable = "esic";
    // added by priyanka k
   this.getbusStopName();
   this.getAllBusdata();
   this.getbusRoutData();
  }


  ngOnDestroy() {

  }
  paygroupactive() {
    let url = this.baseurl + '/PayGroup/Active/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.paygroupdata = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  paygroupvalue(event) {
    this.HRempOtherModel.payGroupId = parseInt(event.target.value);
  }

  // added by priyanka k start

  getbusStopName() {
    let routurl = this.baseurl + '/activeBusStopData/';
    this.httpService.get(routurl + this.locationId).subscribe((data: any) => {
        this.busStoplist = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }

  mychange(r) {
    this.busStopIds = r.target.value;
    this.getAllBusdata();
  }

  changeBusId(r) {
    this.busesid = r.target.value;
    this.busCapasity();
  }
  hidebuttn: boolean = false;
  getAllBusdata() {
    this.busStopId = this.busStopIds
    let url = this.baseurl + '/getdatabybusstopid/';
    this.httpService.get(url + this.busStopId).subscribe(data => {
        this.busdata = data['result'];
      },
      (err: HttpErrorResponse) => {
      });
  }
  capacityflag: boolean
  getbuscapacitydata = {};
  busCapasity() {
    this.busId = this.busesid
    this.capacityflag = false;
    this.hidebuttn = false;
    let url = this.baseurl + '/buscapacity?busId=';
    this.httpService.get(url + this.busId).subscribe((data: any) => {
        this.hidebuttn = true;
        this.getbuscapacitydata = data;
        this.capacityflag = true;
      },
      (err: HttpErrorResponse) => {
        this.capacityflag = false;
        this.hidebuttn = false;
      });
  }
  hide: boolean = false;
  transportevent(event) {
    if (event == 'Company Bus') {
      this.hide = true;
    }
    else {
      this.hide = false;
    }
  }
  // added by priyanka k end
  resetdata() {
    this.verificationobject.nameOfTheReference = "";
    this.verificationobject.companyName = "";
    this.verificationobject.contactNo = "";
  }
  AddVerificationRefference(f2,verificationobject) {
    this.buttonDisabledverification = true;
    if (this.isEdit1 == false && verificationobject.nameOfTheReference && verificationobject.companyName && verificationobject.areasOfImprovement) {
      let obj = this.verificationobject;
      this.allVerification.push(this.verificationobject)
      this.verificationobject = new Object();
      setTimeout(() => {
        f2.reset();
      }, 100);

    } else {
      this.isEdit1 = false;
    }
  }

  AddVerificationRefferencemore(verificationobject) {
    this.verificationobject = {}
    this.buttonDisabledverification = null;
  }
  reset() {
    this.addReourceObject.resourceDesc = null;
    this.addReourceObject.resourcesId = "selectresources";
    this.addReourceObject.resourceNumber = null;
    this.addReourceObject.givenDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
  }
  AddResources(fg3) {
    this.buttonDisabled = true;
    for (let i = 0; i < this.resourceData.length; i++) {
      if (this.resourceData[i].resourcesId == this.addReourceObject.resourcesId) {
        this.addReourceObject.resourceName = this.resourceData[i].resourceName;
      }
    }
    if (this.isEdit2 == false) {
      this.resources.push(this.addReourceObject);
      this.addResources=this.resources;
      this.addReourceObject = new Object();
      setTimeout(() => {
        fg3.reset();
        this.addReourceObject.givenDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      }, 100);
    }
    else {
      this.addResources[this.editindex2] = this.addReourceObject;
      this.isEdit2 = false;
    }
  }

  AddResourcesmore(addReourceObject) {
    this.addReourceObject = {}
    this.buttonDisabled = null;
    this.addReourceObject.resourcesId = "selectresources";
    this.addReourceObject.givenDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
  }

  public getEditData() {
    let url = this.baseurl + '/employeeMasterbyId/' + this.empPerId;
    this.httpService.get(url).subscribe((data) => {
        this.editData = data["result"];
        this.HRempOtherModel.mediclaimaApplicable = this.editData["mediclaimaApplicable"]
        if (this.HRempOtherModel.mediclaimaApplicable == "Yes") {
          this.mediclaimaApplicable = true;
        }
        else {
          this.mediclaimaApplicable = false;
        }
        this.HRempOtherModel.payGroupId = this.editData["payGroupId"];
        this.HRempOtherModel.bonusApplicable = this.editData["bonusApplicable"];
        this.HRempOtherModel.variablePay = this.editData["variablePay"];
        this.HRempOtherModel.ptApplcable = this.editData["ptApplcable"];
        if (this.HRempOtherModel.ptApplcable == 'Yes') {
          this.ptApplcable = true;
        }
        else {
          this.ptApplcable = false;
          this.HRempOtherModel.ptApplcable = 'No'
        }
        this.HRempOtherModel.pfApplicable = this.editData["pfApplicable"];
        if (this.HRempOtherModel.pfApplicable == 'Yes') {
          this.pfApplicable = true
        }
        else {
          this.pfApplicable = false
          this.HRempOtherModel.pfApplicable = 'No'
        }
        this.HRempOtherModel.pfNumber = this.editData["pfNumber"];
        this.HRempOtherModel.payMode = this.editData["payMode"];
        this.HRempOtherModel.ptLocation = this.editData["ptLocation"];
        this.HRempOtherModel.esicApplicable = this.editData["esicApplicable"];
        if (this.HRempOtherModel.esicApplicable == 'Yes') {
          this.esicApplicable = true
        }
        else {
          this.esicApplicable = false
        }
        this.HRempOtherModel.policyApplicable = this.editData.policyApplicable;
        this.HRempOtherModel.esicNumber = this.editData["esicNumber"];
        this.empOfficialId = this.editData.empOfficialId;
        sessionStorage.setItem('empOfficialId', this.empOfficialId);
        this.HRempOtherModel.amtCovrmediclaim = this.editData.amtCovrmediclaim;
        this.HRempOtherModel.undermediclaimMembrs = this.editData["undermediclaimMembrs"];
        this.HRempOtherModel.dateOfOffer = this.editData["dateOfOffer"];
        this.HRempOtherModel.position = this.editData["position"];
        this.HRempOtherModel.offeredBy = this.editData["offeredBy"];
        this.HRempOtherModel.confirmationDueDate1 = this.editData["confirmationDueDate1"];
        this.HRempOtherModel.packageAccordingToOffer = this.editData["packageAccordingToOffer"];
        this.HRempOtherModel.status = this.editData["status"];
        this.HRempOtherModel.isJoined = this.editData["isJoined"];
        this.HRempOtherModel.documentsSubmitted = this.editData["documentsSubmitted"];
        this.HRempOtherModel.documentsVerified = this.editData["documentsVerified"];
        this.HRempOtherModel.appointmentLetterIsProcessed = this.editData["appointmentLetterIsProcessed"];
        this.HRempOtherModel.offerLetterAndJoiningSatutoryDataId = this.editData.offerLetterAndJoiningSatutoryDataId
        this.allVerification = this.editData["empVerificationReferencesList"];
        this.HRempOtherModel.offerLetterId = this.editData.offerLetterId;
        this.HRempOtherModel.joiningDetailsId = this.editData.joiningDetailsId;
        this.addResources = this.editData["empGivenResourcesList"];
        this.HRempOtherModel.transportDetailsId = this.editData['transportDetailsId'];
        this.HRempOtherModel.busStopName = this.editData["busStopName"];
        this.HRempOtherModel.busStopId = this.editData["busStopId"];
        this.HRempOtherModel.busRouteId = this.editData["busRouteId"];
        this.HRempOtherModel.droPoint = this.editData["droPoint"];
        this.HRempOtherModel.pickupPoint = this.editData["pickupPoint"];
        this.HRempOtherModel.transportationMode = this.editData['transportationMode'];
        this.HRempOtherModel.busRouteName = this.editData['busRouteName'];
        this.HRempOtherModel.busStopName = this.editData['busStopName'];
        this.HRempOtherModel.exgratia = this.editData['exgratia'];
        this.HRempOtherModel.transportPurpose = this.editData['transportPurpose'];
        if(this.editData['busStopId']){
          this.getbusallData(this.editData['busStopId']);
        }
      }, (err: HttpErrorResponse) => {
      });
  }

  storeInfo() {
    if (this.isEditHR == 'true') {
      this.update(this.HRempOtherModel);
    } else {
      this.saveAllData('');
    }
  }
  dateoffer(event) {
    this.HRempOtherModel.dateOfOffer = event;
  }
  confirmationDuedate(event) {
    this.HRempOtherModel.confirmationDueDate1 = event;
  }
  joiningDate(event) {
    this.HRempOtherModel.dateOfJoining = event;
  }
  confirmationd(event) {
    this.HRempOtherModel.dateOfConfDate = event;
  }
  givendate(event) {
    this.addReourceObject.givenDate = event;
  }

  saveAllData(HRempOtherModel) {
    this.Spinner.show();
    this.HRempOtherModel.empGivenResourcesList = this.addResources;
    this.HRempOtherModel.empVerificationReferencesList = this.allVerification;
    this.HRempOtherModel.empPerId = parseInt(sessionStorage.getItem('empPersonId'));
    this.HRempOtherModel.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
    this.HRempServe.postHRempOthers(this.HRempOtherModel).subscribe(data => {
        this.toastr.success('Others Details Information Inserted Successfully!');
        this.HRempOtherModel.payMode = "SelectpayMode";
        this.verificationobject.feedbackReceived = "SelectFeedback";
        this.addReourceObject.resourcesId = "selectresources";
        this.HRempOtherModel.status = "Selectstatus";
        this.callParent();
        this.Spinner.hide();
      },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Failed To Insert');
        });
  }

  update(HRempOtherModel) {
    this.Spinner.show();
    this.HRempOtherModel.empGivenResourcesList = this.addResources;
    this.HRempOtherModel.empVerificationReferencesList = this.allVerification;
    this.HRempOtherModel.empOfficialId = this.empOfficialId;
    this.HRempOtherModel.offerLetterAndJoiningSatutoryDataId = this.editData.offerLetterAndJoiningSatutoryDataId;
    this.HRempOtherModel.empPerId = this.empPerId;
    this.HRempOtherModel.offerLetterId = this.editData.offerLetterId;
    this.HRempOtherModel.joiningDetailsId = this.editData.joiningDetailsId;
    this.HRempOtherModel.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
    this.HRempServe.updateHRempOthers(this.HRempOtherModel).subscribe(data => {
        this.toastr.success('Official Information Updated Successfully!');
        this.HRempOtherModel.payMode = "SelectpayMode";
        this.verificationobject.feedbackReceived = "SelectFeedback";
        this.addReourceObject.resourcesId = "selectresources";
        this.HRempOtherModel.status = "Selectstatus";
        this.callParent();
        this.Spinner.hide();
      },
        (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Failed To Update');
        });
  }
  editOfferIndex: number;
  getEditDataOther(offer, i) {
    this.editOfferIndex = i;
    this.Offerobject.dateOfOffer = offer.dateOfOffer;
    this.Offerobject.position = offer.position;
    this.Offerobject.offeredBy = offer.offeredBy;
    this.Offerobject.confirmationDueDate = offer.confirmationDueDate;
    this.Offerobject.packageAccordingToOffer = offer.packageAccordingToOffer;
    this.Offerobject.status = offer.status;
    this.updateButton = true;
  }
  getverification(item) {
    this.verificationobject.nameOfTheReference = item.nameOfTheReference;
    this.verificationobject.companyName = item.companyName;
    this.verificationobject.contactNo = item.contactNo;
    this.verificationobject.emailId = item.emailId;
    this.verificationobject.feedbackReceived = item.feedbackReceived;
    this.verificationobject.strengths = item.strengths;
    this.verificationobject.areasOfImprovement = item.areasOfImprovement;
  }
  getResources(item) {
    this.addReourceObject.resourceName = item.resourceName;
    this.addReourceObject.resourceNumber = item.resourceNumber;
    this.addReourceObject.givenDate = item.givenDate;
    this.addReourceObject.resourceDesc = item.resourceDesc;
  }
  isEdit1: boolean = false;
  editindex1: number = 0;
  editver(item, i) {
    this.verificationobject = {};
    this.verificationobject.nameOfTheReference = item.nameOfTheReference;
    this.verificationobject.companyName = item.companyName;
    this.verificationobject.contactNo = item.contactNo;
    this.verificationobject.emailId = item.emailId;
    this.verificationobject.feedbackReceived = item.feedbackReceived;
    this.verificationobject.strengths = item.strengths;
    this.verificationobject.areasOfImprovement = item.areasOfImprovement;
    this.isEdit1 = true;
    this.editindex1 = i;
  }

  isEdit2: boolean = false;
  editindex2: number = 0;
  editres(item, i) {
    this.addReourceObject = {};
    this.addReourceObject.resourcesId = item.resourcesId;
    this.verificationobject.resourceName = item.resourceName;
    this.verificationobject.givenDate = item.givenDate;
    this.verificationobject.resourceDesc = item.resourceDesc;
    this.isEdit2 = true;
    this.editindex2 = i;
  }
  deletever(i) {
    this.allVerification.splice(i, 1);
  }
  deleteFlag: boolean;
  indexDelete: number = 0;
  deleteres(i) {
    this.deleteFlag = true;
    this.indexDelete = i;
    this.reset();
  }
  DeletemodelEvent() {
    this.addResources.splice(this.indexDelete, 1)
    this.deleteFlag = false;
    this.reset();
  }
  medicalClaim(event) {
    if (event == true) {
      this.HRempOtherModel.mediclaimaApplicable = "Yes";
    } else {
      this.HRempOtherModel.mediclaimaApplicable = "No";
    }
  }
  pfApplicableData(event) {
    if (event == true) {
      this.HRempOtherModel.pfApplicable = 'Yes';
    } else {
      this.HRempOtherModel.pfApplicable = 'No';
    }
  }
  ptApplcableData(event) {
    if (event == true) {
      this.HRempOtherModel.ptApplcable = 'Yes';
    } else {
      this.HRempOtherModel.ptApplcable = 'No';
    }
  }

  esicApplicableData(event) {
    if (event == true) {
      this.HRempOtherModel.esicApplicable = 'Yes';
      this.HRempOtherModel.personalAccidentPol = 'Yes';
      this.HRempOtherModel.notApplicable = 'Yes';
    } else {
      this.HRempOtherModel.esicApplicable = 'No';
      this.HRempOtherModel.personalAccidentPol = 'No';
      this.HRempOtherModel.notApplicable = 'No'
    }
  }
  res(s) {
  }
  addResourceName(addRes, ff3) {
    this.addRes.compId = this.compId;
    if (this.resEdit == false) {
      this.HRempServe.postResouceName(this.addRes).subscribe(data => {
        this.getList();
        this.toastr.success("Resource add successfully");
        addRes.resourceName = "";
      }, err => {
        
      })
    }
    else {
      
      this.HRempServe.putResouceName(this.addRes).subscribe(data => {
        this.getList();
        this.resEdit = false;
        this.toastr.success("Resource Updated successfully")
      }, err => {
       
        this.resEdit = false;
      })
    }
  }

  resourceData = [];
  getList() {
    let url = this.baseurl + '/resource/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.resourceData = data['result'];
      }, (err: HttpErrorResponse) => {
      });
  }
  editAddResources(res) {
    this.addRes = res;
    this.resEdit = true;
  }

  deleteAddres(res) {
    let url = this.baseurl + '/resource/';
    this.httpService.delete(url + res.resourcesId).subscribe(data => {
        this.getList();
        this.toastr.success("Resource deleted successfully")
      }, (err: HttpErrorResponse) => {
       
      });
  }
  EditFlag: any
  flagEsic: boolean;
  getESIC = { message: "" };
  getDuplicateESIC(esicNumber) {
    if (this.EditFlag = 'true') {
      if (this.editData['esicNumber'] != esicNumber) {
        this.flagEsic = false;
        this.hidebuttn = false;
        let url1 = this.baseurl + '/duplicateesicno?compId=' + this.compId + '&esicNumber=' + esicNumber;
        this.httpService.get(url1).subscribe((data: any) => {
            this.flagEsic = true;
            this.getESIC = data;
            this.hidebuttn = true;
          }, (err: HttpErrorResponse) => {
            this.flagEsic = false
            this.hidebuttn = false;
          });
      }
    }
  }
//added by priyanka k start
numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}
//added by priyanka k end
  flagPan: boolean;
  getPanData = { message: "" };
  getDuplicatePan(pfNumber) {
    if (this.EditFlag = 'true') {
      if (this.editData['pfNumber'] != pfNumber) {
        this.flagPan = false;
        this.hidebuttn = false;
        let url1 = this.baseurl + '/duplicatePFNo?compId=' + this.compId + '&pfNumber=' + pfNumber;
        this.httpService.get(url1).subscribe((data: any) => {
            this.flagPan = true;
            this.getPanData = data;
            this.hidebuttn = true;
          }, (err: HttpErrorResponse) => {
            
            this.flagPan = false
            this.hidebuttn = false;
          });
      }
    }
  }
OtherDetails:any;
hrEmpOtherDetails(){
let url=this.baseurl + '/HrEmpOtherDetails';
this.httpService.post(url ,this.HRempOtherModel).subscribe(data => {
  this.OtherDetails = data["result"];
});
}
// ...............................................transport bus master data...................................................... 

busModellist:any;
getbusRoutData(){
    let url = this.baseurl + '/busRouteByList';
    this.httpService.get( url).subscribe((data: any) => {
        this.busModellist = data.result;
      },
     );
  }
  busStopllist:any;
  getbusallData(id){
  let url2 = this.baseurl + '/getBusStopDataByRouteId/';
  this.httpService.get(url2 + id).subscribe(
    (data: any) => {
      this.busStopllist = data.result
    },
   );
 }
 busStopData(event) {
  this.busRouteId = event.target.value;
  this.getbusallData(this.busRouteId );
 }

}

