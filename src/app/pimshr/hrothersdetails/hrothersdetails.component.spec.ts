import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrothersdetailsComponent } from './hrothersdetails.component';

describe('HrothersdetailsComponent', () => {
  let component: HrothersdetailsComponent;
  let fixture: ComponentFixture<HrothersdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrothersdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrothersdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
