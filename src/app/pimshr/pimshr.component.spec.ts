import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PimshrComponent } from './pimshr.component';

describe('PimshrComponent', () => {
  let component: PimshrComponent;
  let fixture: ComponentFixture<PimshrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PimshrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PimshrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
