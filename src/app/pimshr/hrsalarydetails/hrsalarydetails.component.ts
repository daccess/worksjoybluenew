import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { HRempSalarymodeldata } from '../Models/HRsalarydetail';
import { HRSalaryService } from '../Services/HRsalarydetailsservice';
import { MainURL } from '../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HRAttendenceDataService } from '../Services/HRservice';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
@Component({
  selector: 'app-hrsalarydetails',
  templateUrl: './hrsalarydetails.component.html',
  styleUrls: ['./hrsalarydetails.component.css']
})
export class HrsalarydetailsComponent implements OnInit {
  @Output() someEvent2 = new EventEmitter<string>();
  baseurl = MainURL.HostUrl;
  payheadmasterdata: any;
  compId: any;
  model: any={};
  TableVals = [];
  applicableDate: any;
  HRSalary : HRempSalarymodeldata;
  empOfficialId: any;
  loggedUser: any;
  isEditHR:any;
  editData: any;
  empPerId:any;
  ctcDetailsId:any;
  totalsalary: any = 0;

  constructor(public HRsalaryservice: HRAttendenceDataService,public Spinner: NgxSpinnerService, public toastr: ToastrService, public httpService: HttpClient,public router: Router) { 
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.isEditHR=sessionStorage.getItem('isEditHR');
    this.compId =  this.loggedUser.compId;
    this.HRSalary = new HRempSalarymodeldata();
    this.HRSalary.logEmpOffId = this.loggedUser.empOfficialId
    this.empOfficialId = sessionStorage.getItem('empOfficialId');
    this.applicableDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.payheadmaster();
  }
  callParent() {
    this.someEvent2.next('from attendance');
  }
  ngOnInit(){
    this.empPerId = sessionStorage.getItem('EmpDataDetails');
    if(this.isEditHR=='true'){
      this.getEditData();
    }
  }
  rowClickFun(event){
    this.model.payhead = event.target.textContent;
  }

  handleKeyDown(event){
    this.model.monthdeduct = event.target.value;
    if(event.keyCode == 13){
      this.TableVals.push(this.model);
      this.model ={};
    }
    for(var i=0 ; i< this.payheadmasterdata.length; i++){
      for(var j=0; j< this.TableVals.length;j++){
        if(this.payheadmasterdata[i]['payRollHeadName'] == this.TableVals[j]['payhead']){
          this.payheadmasterdata[i]['name'] = this.TableVals[j]['monthdeduct'];
        }
      }
    }
  }
  calclate = 0;
  calculatesalary(){
    let temp = new Array();
    temp = this.payheadmasterdata;
    this.totalsalary = 0;
    for (let i = 0; i < temp.length; i++) {
      if(temp[i].amount){
        this.totalsalary = this.totalsalary + parseInt(temp[i].amount);
      }
    }
  }
  payheadmaster(){
    let url = this.baseurl + '/PayRollHead/Active/';
    this.httpService.get(url + this.compId).subscribe((data : any) => {
        let obj = data.result;
        for (let index = 0; index < obj.length; index++) {
          obj[index].amount = 0;
        }
        this.payheadmasterdata = obj;
        
      },(err: HttpErrorResponse) => {
      });
      
  }

  getEditData(){
    let urldata = this.baseurl + '/employeeMasterbyId/'+this.empPerId;
    this.httpService.get(urldata).subscribe((data) => {
        this.editData = data["result"];
        this.applicableDate = this.editData["applicableDate"];
        this.empOfficialId = this.editData.empOfficialId;
        sessionStorage.setItem('empOfficialId',this.empOfficialId);
        this.ctcDetailsId = this.editData.ctcDetailsId;
        let temp = new Array();
        let pay = new Array();
        pay = this.editData["empPayRollHeadCtcDetailList"];
        temp = this.payheadmasterdata;
        for (let i = 0; i < temp.length; i++) {
          for (let j = 0; j < pay.length; j++) {
            if(pay[j].payRollHeadsId == temp[i].payRollHeadsId){
              temp[i] = pay[j];
              this.totalsalary = this.totalsalary + temp[i].amount;
            }
          }
        }
        this.payheadmasterdata = temp;
      }, (err: HttpErrorResponse) => {
      });

  }
storeInfo(){
  if(this.isEditHR=='true'){
    this.update();
  }else{
    this.saveAllData();
  }
}

applicabledate(event){
 this.applicableDate = event;
this.HRSalary.applicableDate = this.applicableDate;
}

  saveAllData(){
    this.Spinner.show()
    this.HRSalary.applicableDate = this.applicableDate;
    this.HRSalary.payRollHeadsList = this.payheadmasterdata;
    this.HRSalary.empOfficialId = this.empOfficialId;
    this.HRSalary.ctcDetailsId = this.ctcDetailsId;
    this.HRSalary.empPerId = parseInt(sessionStorage.getItem('empPersonId'));
    this.HRSalary.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
    this.HRsalaryservice.postHRempSalary(this.HRSalary).subscribe(data=>{
      this.callParent();
      this.Spinner.hide();
      this.toastr.success('Salary Detail Inserted Successfully!', 'Salary Detail');
      this.router.navigateByUrl('/layout/employeelist');

    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Salary Detail');
    });
}

  update(){
    this.Spinner.show()
    this.HRSalary.applicableDate = this.applicableDate;
    this.HRSalary.payRollHeadsList = this.payheadmasterdata;
    this.HRSalary.empOfficialId = this.empOfficialId;
    this.HRSalary.ctcDetailsId = this.ctcDetailsId;
    this.HRSalary.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
    this.HRsalaryservice.updateHRempSalary(this.HRSalary).subscribe(data=>{
      this.callParent();
      this.toastr.success('Salary Detail Updated Successfully!', 'Salary Detail');
      this.Spinner.hide();
      this.router.navigateByUrl('/layout/employeelist');
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
      this.toastr.error('Server Side Error..!', 'Salary Detail');
    });
  }
}
