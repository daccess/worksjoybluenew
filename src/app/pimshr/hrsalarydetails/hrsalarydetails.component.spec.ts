import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrsalarydetailsComponent } from './hrsalarydetails.component';

describe('HrsalarydetailsComponent', () => {
  let component: HrsalarydetailsComponent;
  let fixture: ComponentFixture<HrsalarydetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrsalarydetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrsalarydetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
