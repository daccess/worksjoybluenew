import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HRempOfficialdata } from '../Models/HRempOfficial';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class HRempoffService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postHROfficialInfo(HRempOfficial : HRempOfficialdata){
    let url = this.baseurl + '/EmpOfficialDetails';
    var body = JSON.stringify(HRempOfficial);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  updateHROfficialInfo(HRempOfficial : HRempOfficialdata){
    let url = this.baseurl + '/EmpOfficialDetails';
    var body = JSON.stringify(HRempOfficial);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body,requestOptions).map(x => x.json());
  }
}