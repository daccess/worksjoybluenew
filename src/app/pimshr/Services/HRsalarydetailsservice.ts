import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HRempSalarymodeldata } from '../Models/HRsalarydetail';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class HRSalaryService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postHRempSalary(HRempsalary : HRempSalarymodeldata){
    let url = this.baseurl + '/EmpCtcDetails';
    var body = JSON.stringify(HRempsalary);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}