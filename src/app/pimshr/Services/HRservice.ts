import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HRattendencesetting } from '../Models/HRempAttendence';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { HRempOfficialdata } from '../Models/HRempOfficial';
import { HRBasicdata } from '../Models/HRBasicEmpInfo';
import { HRempOthersmodeldata } from '../Models/HRempOthers';
import { HRempSalarymodeldata } from '../Models/HRsalarydetail';

@Injectable()
export class HRAttendenceDataService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postAttendenceHR(HRAttendence : HRattendencesetting){
    let url = this.baseurl + '/EmpAttendanceSettings';
    var body = JSON.stringify(HRAttendence);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  updateAttendenceHR(HRAttendence : HRattendencesetting){
    let url = this.baseurl + '/EmpAttendanceDetails';
    var body = JSON.stringify(HRAttendence);
    // var headerOptions = new Headers({'Content-Type':'application/json'});
    // var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }
  //....................... for official info ...............................

  postHROfficialInfo(HRempOfficial : HRempOfficialdata){
    let url = this.baseurl + '/EmpOfficialDetails';
    var body = JSON.stringify(HRempOfficial);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  // updateHROfficialInfo(HRempOfficial : HRempOfficialdata){
  //   let url = this.baseurl + '/EmployeeOfficialData';
  //   var body = JSON.stringify(HRempOfficial);
  //   var headerOptions = new Headers({'Content-Type':'application/json'});
  //   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  //   return this.http.put(url,body,requestOptions).map(x => x.json());
  // }

  updateHROfficialInfo(HRempOfficial : HRempOfficialdata){
    let url1 = this.baseurl +'/EmployeeOfficialData';
    return this.http.put(url1 ,HRempOfficial)
  }

  // .......................for empInfo..................
  postHRBasicInfo(HRBasic : HRBasicdata){
    let url = this.baseurl + '/HrEmpPerInfo';
    var body = JSON.stringify(HRBasic);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  // updateHRBasicInfo(HRBasic : HRBasicdata){
  //   let url = this.baseurl + '/HrEmpPerInfo';
  //   var body = JSON.stringify(HRBasic);
  //   var headerOptions = new Headers({'Content-Type':'application/json'});
  //   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  //   return this.http.put(url,body).map(x => x.json());
  // }

  updateHRBasicInfo(HRBasic : HRBasicdata){
    let url1 = this.baseurl +'/HrEmpPerInfo';
    return this.http.put(url1 ,HRBasic)
  }

  // .....................for other info.....................

  postHRempOthers(HRempOther : HRempOthersmodeldata){
    let url = this.baseurl + '/HrEmpOtherDetails';
    var body = JSON.stringify(HRempOther);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  updateHRempOthers(HRempOther : HRempOthersmodeldata){
    let url = this.baseurl + '/OtherDetails';
    var body = HRempOther;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }

  // .................. salary details..................
  postHRempSalary(HRempsalary : HRempSalarymodeldata){
    let url = this.baseurl + '/EmpCtcDetails';
    var body = JSON.stringify(HRempsalary);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  updateHRempSalary(HRempsalary : HRempSalarymodeldata){
    let url = this.baseurl + '/CtcDetails';
    var body = HRempsalary;
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body).map(x => x.json());
  }
  getSearchSuggestionList(text){
    let url = this.baseurl + '/EmployeeOperation/';
    return this.http.get(url+text).map(x => x.json());
  }
  getSearchSuggestionLists(text,location){
    let url = this.baseurl + '/EmployeeOperation';
    return this.http.get(url+'/'+location+'/'+text).map(x => x.json());
  }

  postResouceName(body){
    let url = this.baseurl + '/resource';
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions);
 
  }

  putResouceName(body){
    let url = this.baseurl + '/resource';
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.put(url,body);
 
  }
  
}