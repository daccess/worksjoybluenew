import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HRempOthersmodeldata } from '../Models/HRempOthers';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class HRempOthersService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postHRempOthers(HRempOther : HRempOthersmodeldata){
    let url = this.baseurl + '/HrEmpOtherDetails';
    var body = JSON.stringify(HRempOther);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

}