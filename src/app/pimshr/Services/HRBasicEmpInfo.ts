import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HRBasicdata } from '../Models/HRBasicEmpInfo';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';

@Injectable()
export class HRBasicDataService {

  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postHRBasicInfo(HRBasic : HRBasicdata){
    let url = this.baseurl + '/HrEmpPerInfo';
    var body = JSON.stringify(HRBasic);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}