import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrpimsbasicformComponent } from './hrpimsbasicform.component';

describe('HrpimsbasicformComponent', () => {
  let component: HrpimsbasicformComponent;
  let fixture: ComponentFixture<HrpimsbasicformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrpimsbasicformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrpimsbasicformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
