import { Component, OnInit } from '@angular/core';
import { HRBasicDataService } from '../Services/HRBasicEmpInfo';
import { HRBasicdata } from '../Models/HRBasicEmpInfo';
import { from } from 'rxjs';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MainURL } from '../../shared/configurl'
import { HRAttendenceDataService } from '../Services/HRservice';
import { DatePipe } from '@angular/common'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-hrpimsbasicform',
  templateUrl: './hrpimsbasicform.component.html',
  styleUrls: ['./hrpimsbasicform.component.css'],
  providers: [HRBasicDataService, DatePipe]
})
export class HrpimsbasicformComponent implements OnInit {

  BasicHRInfoModel: HRBasicdata;
  dob:any;
  birthdate: any;
  cur: any;
  diff: any;
  age: any;
  HRBasicInfoMessage: any;

  empPerId: any;

  baseurl = MainURL.HostUrl
  loggedUser: any;
  compId: any;
  errordata: any;
  msg: any;
  errflag: boolean;

  sendNextToPageData: any;

  isEditHR: any;
  editData: any;
  today: string;

  silutation:any
  maxDate=new Date();

  constructor(private router: Router,
    public HRBasicService: HRAttendenceDataService,
    private toastr: ToastrService,
    public httpService: HttpClient,
    public Spinner: NgxSpinnerService,
    public datepipe: DatePipe) {
    this.BasicHRInfoModel = new HRBasicdata();
    this.isEditHR=sessionStorage.getItem('isEditHR')
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);

    var dte = new Date();
    dte.setDate(dte.getDate() - 6588);
    let latest_date =this.datepipe.transform(dte.toString(), 'yyyy-MM-dd');
  }

  ngOnInit() {
    this.empPerId = sessionStorage.getItem('EmpDataDetails');
    this.compId = this.loggedUser.compId;
    this.BasicHRInfoModel.logEmpOffId = this.loggedUser.empOfficialId
    this.BasicHRInfoModel.gender = 'Male';
    this.BasicHRInfoModel.salutation = 'MR';
    this.today =  new Date().toJSON().split('T')[0];
    if(this.isEditHR=='true'){
      if(this.empPerId){
        this.getEditData();
      }
    }
  }

  getEditData(){
      let urlget = this.baseurl + '/employeeMasterbyId/'+this.empPerId;
      this.httpService.get(urlget).subscribe((data) => { 
          this.editData = data["result"];
          this.BasicHRInfoModel.salutation = this.editData.salutation;
          this.BasicHRInfoModel.fullName = this.editData.fullName;
          // this.BasicHRInfoModel.firstName = this.editData.firstName;
          this.BasicHRInfoModel.middleName = this.editData.middleName;
          this.BasicHRInfoModel.lastName = this.editData.lastName;
          this.BasicHRInfoModel.gender = this.editData.gender;

          this.BasicHRInfoModel.officialContactNumber = this. editData.officialContactNumber;
          this.BasicHRInfoModel.officialEmailId = this.editData.officialEmailId;

          // this.BasicHRInfoModel.dateOfBirth = this.editData.dateOfBirth;
          // this.BasicHRInfoModel.age=this.editData.dateOfBirth;
          // this.calculateage(this.BasicHRInfoModel.age);
          this.BasicHRInfoModel.personalContactNumber = this.editData.personalContactNumber;
          this.BasicHRInfoModel.personalEmailId = this.editData.personalEmailId;
  
        }, (err: HttpErrorResponse) => {
        });

  }


  genderChange() {

    if (this.BasicHRInfoModel.salutation == 'MR') {
      this.BasicHRInfoModel.gender = 'Male'
    }
    if (this.BasicHRInfoModel.salutation == 'Mrs') {
      this.BasicHRInfoModel.gender = 'Female'
    }
    if (this.BasicHRInfoModel.salutation == 'Miss') {
      this.BasicHRInfoModel.gender = 'Female'
    }
  }

Erroremail(){
    let obj ={
      officialEmailId : this.BasicHRInfoModel.officialEmailId
  }
  let url = this.baseurl + '/empOfficialMailIdCheck';
  this.httpService.post(url,obj).subscribe((data :any) => {
     this.errordata= data["result"];
     this.msg= data.message
     this.toastr.error(this.msg);
     this.errflag = true
    },
    (err: HttpErrorResponse) => {
   this.errflag = false
    });
}

  // changeDateofBirth(event){
  // this.BasicHRInfoModel.dateOfBirth = event.target.value;
  // this.BasicHRInfoModel.age = event.target.value;
  // this.calculateage(event.target.value)
  // }


  // calculateage(getvalue){
  //   this.dob = new Date(this.BasicHRInfoModel.age);
  //   this.cur = new Date();
  //   var diffD = Math.round((Math.abs(this.dob - this.cur) / (24 *60 * 60 * 1000))/365)
  //   this.age=diffD;
  //   console.log("this is diff between current date and selected date"+this.age)
  //   this.BasicHRInfoModel.age = this.age;
  // }
  
  fullName:string
  SaveBasicInfo() {
    // if( this.BasicHRInfoModel.age > 18 &&  this.BasicHRInfoModel.age < 100){
      // if( this.BasicHRInfoModel.officialEmailId !=null){
      this.sendNextToPageData = sessionStorage.setItem('getEmpBasicData', JSON.stringify(this.BasicHRInfoModel));
      this.router.navigateByUrl('layout/pimshr/details');
      // }else{
      //     this.toastr.error('please select Official Email...');
      //   }
    // }else{
    //   this.toastr.error('please select age greater than 18');
    // }
   
    }
   
}
