import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { from } from 'rxjs';
import { MainURL } from '../../shared/configurl';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HRattendencesetting } from '../Models/HRempAttendence';
import { HRAttendenceDataService } from '../Services/HRservice';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
    selector: 'app-hrattendancedetails',
    templateUrl: './hrattendancedetails.component.html',
    styleUrls: ['./hrattendancedetails.component.css'],
    providers: [HRAttendenceDataService]
})
export class HrattendancedetailsComponent implements OnInit {
    @Output() someEvent1 = new EventEmitter<string>();
    baseurl = MainURL.HostUrl;
    compId: any;
    selectedshiftgroupobj: any;
    selectedshiftmastergroupobj: any;
    selectedshiftpatternobj: any;
    selectedshiftobj: any;

    Shiftgroupdata: any;
    Shiftmastergroupdata: any;
    WeeklyOffcalendarData = [];
    selectedWeeklyGroupobj: any;
    ShiftPatterndata = [];
    ShiftPatternshiftdata =[];
    HRattendeceModel: HRattendencesetting;

    empOfficialId: any;
    // loggedUser: any;
    editData: any;
    empPerId: any;
    atdSettingId: any;
    isEditHR: any;
    loggedUser: any;
    locationId: any;
    // weeklyLocationID: string;
     weeklyLocationID: any;
     public selectUndefinedOptionValue: any;
    sequenceOne: any;
    shiftName1: any;
    sequencetwo: any;
    shiftName2: any;
    sequencethree: any;
    shiftName3: any;
    currentDate: string;
    weeklyOffType: string ="Fixed";
    selectedLocationId: any=0;
    selectedlocationids: any=0;
    rollName: any;
    isEditAdmin: string;
    locoid: string;
    panNo:any;
    adharNo:any;
    pfNumber:any;
    universalAccNo:any;
    esicNumber:any;






    constructor(public httpService: HttpClient,public Spinner: NgxSpinnerService, public HRattService: HRAttendenceDataService,public toastr: ToastrService, public router:Router) {
        let user = sessionStorage.getItem('loggedUser');
        this.loggedUser = JSON.parse(user);
        this.compId = this.loggedUser.compId;
        this.locationId = this.loggedUser.locationId;
        this.isEditHR = sessionStorage.getItem('isEditHR');
        this.isEditAdmin = sessionStorage.getItem('isEditAdmin');
        this.HRattendeceModel = new HRattendencesetting();
        this.HRattendeceModel.logEmpOffId = this.loggedUser.empOfficialId
        this.weeklyLocationID = sessionStorage.getItem('weeklyCalendarId');
        this.rollName=this.loggedUser.mrollMaster.roleName;
        this.weeklyoffdata(this.locoid);
    }
    callParent() {
        this.someEvent1.next('from attendance');
    }
    ngOnInit() {
       
        this.empPerId = sessionStorage.getItem('EmpDataDetails')
        this.locoid= sessionStorage.getItem('locationIdvalue');
        this.shiptgroup(this.locoid);
        this.shiftmastergroup();
        this.shiftPattern();
        this.currentDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
        this.HRattendeceModel.roasterDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
        this.HRattendeceModel.negativeAttendenceAllowed = false;
        this.HRattendeceModel.singlePunchPresent = false;
        this.HRattendeceModel.negativeAttendenceAllowed = false;
        if (this.isEditHR == 'true' || this.isEditAdmin == 'true') {
            this.getEditData();
        }
    }
    shiptgroup(e) {
      
        this.selectedLocationId=e;
        if(this.loggedUser.roleHead=='HR'){
        let url = this.baseurl + '/ShiftGroup/Active/';
        this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
            this.Shiftgroupdata = data["result"];
           // console.log(" Shiftgroupdata", this.Shiftgroupdata)
        })
        }    
        else if(this.loggedUser.roleHead=='Admin'){
            console.log("locationidinattandancedetails",this.locoid);
            let url = this.baseurl + '/ShiftGroup/Active/';
            this.httpService.get(url + this.compId+'/'+this.locoid).subscribe(data => {
                this.Shiftgroupdata = data["result"];
               // console.log(" Shiftgroupdata", this.Shiftgroupdata)

        })
        }
    }
    weeklyoffdata(e) {
         
        this.selectedlocationids=e;
        let compId = this.compId;
        this.WeeklyOffcalendarData = [];
        if(this.loggedUser.roleHead=='HR'){
        let url1 = this.baseurl + '/weeklyOfMasterActive/';
        this.httpService.get(url1 + compId+'/'+this.loggedUser.locationId).subscribe(data => {
            this.WeeklyOffcalendarData = data["result"];
           // console.log("attdancedata",   this.WeeklyOffcalendarData);
        }) 
       
    }
    else if(this.loggedUser.roleHead=='Admin'){
        let url1 = this.baseurl + '/weeklyOfMasterActive/';
        this.httpService.get(url1 + compId+'/'+this.locoid).subscribe(data => {
            this.WeeklyOffcalendarData = data["result"];
           // console.log("attdancedata",   this.WeeklyOffcalendarData);
        }) 
       

    }



    }




    shiftgroupvalue(event) {
        this.selectedshiftgroupobj = parseInt(event.target.value);
    }


    shiftmastergroup() {
        let url = this.baseurl + '/ShiftMaster/ByActive/';
        this.httpService.get(url + this.compId).subscribe(data => {
        this.Shiftmastergroupdata = data["result"];
        }, (err: HttpErrorResponse) => {
        });
    }

    shiftgroupmastervalue(event) {
        this.selectedshiftmastergroupobj = parseInt(event.target.value)
    }

    shiftPattern(){
        let url = this.baseurl + '/shiftMasterPatternActive/';
        this.httpService.get(url + this.compId).subscribe(data => {
        this.ShiftPatterndata = data["result"];
        }, (err: HttpErrorResponse) => {
        });
    }

    shiftpatternvalue(event){
        this.selectedshiftpatternobj = parseInt(event)
        this.getShift();
    }
    
    getShift(){

        let url = this.baseurl + '/shiftMasterPatternById/';
        this.httpService.get(url + this.selectedshiftpatternobj).subscribe(data => {
                this.ShiftPatternshiftdata = data["result"];
                this.sequenceOne = this.ShiftPatternshiftdata['sequenceOne'];
                this.shiftName1 = this.ShiftPatternshiftdata['shiftName1'];
                this.sequencetwo = this.ShiftPatternshiftdata['sequencetwo'];
                this.shiftName2 = this.ShiftPatternshiftdata['shiftName2'];
                this.sequencethree = this.ShiftPatternshiftdata['sequencethree'];
                this.shiftName3 = this.ShiftPatternshiftdata['shiftName3'];

            },(err: HttpErrorResponse) => {
            });
    }

    shift(event){
        this.selectedshiftobj = parseInt(event.target.value)
    }

    dateOfroaster(event){
        this.HRattendeceModel.roasterDate = event;
    }
    
    

    Weeklycalendervalue(event) {
        this.selectedWeeklyGroupobj = parseInt(event.target.value)
    }

    roasterDateFlag : boolean = false;
    getEditData() {
        let urledit = this.baseurl + '/employeeMasterbyId/' + this.empPerId;
        this.httpService.get(urledit).subscribe((data) => {
            this.editData = data["result"];
            this.shiptgroup(this.selectedLocationId);
            this.shiftmastergroup();
            // this.HRattendeceModel.shiftMasterRosterId = this.selectedshiftobj;
            this.HRattendeceModel.shiftType = this.editData["shiftType"];
           // console.log("shifttype", this.HRattendeceModel.shiftType);
            this.selectedshiftgroupobj = this.editData["shiftGroupId"];
            this.selectedshiftmastergroupobj = this.editData["shiftMasterId"];
            this.weeklyOffType = this.editData.weeklyOffType;
            this.selectedWeeklyGroupobj = this.editData["weeklyOffCalenderMasterId"];
            this.HRattendeceModel.panNo=this.editData["panNo"];
            this.HRattendeceModel.adharNo=this.editData["adharNo"];
            this.HRattendeceModel.pfNumber=this.editData["pfNumber"];
           this.HRattendeceModel.universalAccNo=this.editData["universalAccNo"];
           this.HRattendeceModel.esicNumber=this.editData["esicNumber"];
        // this.HRattendeceModel.weeklyOffType = this.editData.weeklyOffType;
            this.HRattendeceModel.weeklyOffType = this.editData.weeklyOffType;
            this.HRattendeceModel.atdSettingId = this.editData["atdSettingId"];
            this.HRattendeceModel.empOfficialId = this.editData["empOfficialId"];
            this.HRattendeceModel.roasterDate = this.editData["roasterDate"];

            if(new Date(this.editData["roasterDate"]).getTime() < new Date().getTime() && this.editData["roasterDate"]){
                this.roasterDateFlag = true;
            }

            this.HRattendeceModel.shiftPatternId = this.editData["shiftPatternId"];
            
            this.selectedshiftpatternobj = this.editData["shiftPatternId"];
            this.shiftpatternvalue(this.selectedshiftpatternobj);
            this.selectedshiftobj = this.editData["shiftMasterRosterId"];
            this.HRattendeceModel.shiftPatternShiftId = this.editData["shiftPatternShiftId"];
            this.HRattendeceModel.singlePunchPresent = this.editData["singlePunchPresent"];
            this.HRattendeceModel.negativeAttendenceAllowed = this.editData["negativeAttendenceAllowed"];
            this.empOfficialId = this.editData.empOfficialId;
            sessionStorage.setItem('empOfficialId',this.empOfficialId);
            this.locationId = this.editData.locationId
            }, (err: HttpErrorResponse) => {
            });

    }

    saveAllData(f) {
        this.Spinner.show();
        if(this.HRattendeceModel.shiftType == "General"){
            this.HRattendeceModel.shiftMasterId = this.selectedshiftmastergroupobj;
            this.HRattendeceModel.shiftGroupId = null;
            this.HRattendeceModel.shiftPatternId = null;
            this.HRattendeceModel.shiftMasterRosterId = null;
        }
        if(this.HRattendeceModel.shiftType == "Custom"){
            this.HRattendeceModel.shiftGroupId = this.selectedshiftgroupobj;
            this.HRattendeceModel.shiftMasterId = null;
            this.HRattendeceModel.shiftPatternId = null;
            this.HRattendeceModel.shiftMasterRosterId = null;
        }
        if(this.HRattendeceModel.shiftType == "Roster"){
            this.HRattendeceModel.shiftPatternId = this.selectedshiftpatternobj;
            this.HRattendeceModel.shiftMasterRosterId = this.selectedshiftobj;
            this.HRattendeceModel.shiftMasterId = null;
            this.HRattendeceModel.shiftGroupId = null;
        }
        this.HRattendeceModel.weeklyOffCalenderMasterId = this.selectedWeeklyGroupobj;
        
        this.HRattendeceModel.shiftPatternShiftId = this.selectedshiftobj;
        this.HRattendeceModel.empPerId = sessionStorage.getItem('empPersonId');
        this.HRattendeceModel.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
        this.HRattendeceModel.atdSettingId = this.atdSettingId;
        this.HRattendeceModel.weeklyOffType = this.weeklyOffType;
        this.HRattService.postAttendenceHR(this.HRattendeceModel).subscribe(data => {
       // this.callParent();
        this.roasterDateFlag = false;
        
        this.Spinner.hide()
        this.router.navigate(['/layout/employeelist/employeelist']);
        this.toastr.success('Employee Created Successfully!');
        f.reset();
        },
        (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Attendence Details');
        });
    }
    updatedata() {
      
        this.Spinner.show();
        if(this.HRattendeceModel.shiftType == "General"){
            this.HRattendeceModel.shiftMasterId = this.selectedshiftmastergroupobj;

            this.HRattendeceModel.shiftGroupId = null;
            this.HRattendeceModel.shiftPatternId = null;
            this.HRattendeceModel.shiftMasterRosterId = null;
        }
        if(this.HRattendeceModel.shiftType == "Custom"){
            this.HRattendeceModel.shiftGroupId = this.selectedshiftgroupobj;

            this.HRattendeceModel.shiftMasterId = null;
            this.HRattendeceModel.shiftPatternId = null;
            this.HRattendeceModel.shiftMasterRosterId = null;
        }
        if(this.HRattendeceModel.shiftType == "Roster"){
            this.HRattendeceModel.shiftPatternId = this.selectedshiftpatternobj;
            this.HRattendeceModel.shiftMasterRosterId = this.selectedshiftobj;
        }

        this.HRattendeceModel.weeklyOffCalenderMasterId = this.selectedWeeklyGroupobj;
        this.HRattendeceModel.empOfficialId = this.empOfficialId;
        this.HRattendeceModel.empOfficialId = parseInt(sessionStorage.getItem('empOfficialId'));
        this.HRattendeceModel.weeklyOffType = this.weeklyOffType;
        let url1 = this.baseurl + '/EmpAttendanceDetails';
        this.httpService.put(url1, this.HRattendeceModel).subscribe(data => {
       // this.callParent();
      //  this.roasterDateFlag = false;
        this.Spinner.hide();
        this.router.navigate(['/layout/employeelist/employeelist']);
        this.toastr.success('Attendence Details Updated Successfully!', 'Attendence Details');
        },
        (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Server Side Error..!', 'Attendence Details');
        });
    }

    storeInfo(f) {
    
        
        if (this.isEditHR == 'true' || this.isEditAdmin == 'true' ) {
            this.updatedata();
        } else {
            this.saveAllData(f);
        }
    }
}