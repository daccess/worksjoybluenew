import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrattendancedetailsComponent } from './hrattendancedetails.component';

describe('HrattendancedetailsComponent', () => {
  let component: HrattendancedetailsComponent;
  let fixture: ComponentFixture<HrattendancedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrattendancedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrattendancedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
