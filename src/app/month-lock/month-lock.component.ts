import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { RoleMasterService } from '../shared/services/RoleMasterService';
@Component({
  selector: 'app-month-lock',
  templateUrl: './month-lock.component.html',
  styleUrls: ['./month-lock.component.css']
})
export class MonthLockComponent implements OnInit {
  user:any;
  token: any;
  users: any;
  alldataofDropDown: any;
  allRolesData: any;
  skipMedicalFlag: boolean = false;
  formData:any = {  
    
    manpowerRoleRequestId:'',
    assignRoleCandidateId:'',
    approveRoleCandidateId:'',
    provisionalRolePassId:'',
    medicalRoleId:'',
    safetyRoleId:'',
    passGenerationRoleId:'',
    authSettingId:'',
    skipMedicalFlag:''
  };

  constructor(private roleservice:RoleMasterService,private toastr:ToastrService) {
   
  
   
   }

  ngOnInit() {
  
    this.user= sessionStorage.getItem('loggeduser')
    this.users=JSON.parse(this.user)
    this.token=this.users.token
   this.getallRollesOfUser()
   this.getAllRole()
  }
  getallRollesOfUser(){
  
this.roleservice.GetAllRole(this.token).subscribe(data=>{

this.alldataofDropDown=data.result;
// this.formData.manpowerRoleRequestId=this.alldataofDropDown.manPowerRequestRoleList.roleId=3
// console.log("this is the data ",this.alldataofDropDown)
})
  }
  onCheckboxChange(){
    console.log('Checkbox value:', this.skipMedicalFlag);
  
  }
  onChange(){
    console.log('Checkbox value:', this.skipMedicalFlag);
  }
  submitRoleMaster(){
    if(this.formData.authSettingId==''){
    console.log(this.formData);
    this.roleservice.saveRolemaster(this.formData,this.token).subscribe(data=>{
      this.toastr.success("Role Master Added Successfully")
    })
  }
  else{
    this.roleservice.updateRolemaster(this.formData,this.token).subscribe(data=>{
      this.toastr.success("Role Master Updated Successfully")
  })
}
  }
  
  getAllRole(){
  
    this.roleservice.GetAllRoles(this.token).subscribe(data=>{
    // this.allRolesData=data['result'];
    console.log("this is the data",data)
    
    
    this.allRolesData=data.result;
  
    console.log("this is the data of id",this.allRolesData)
    this.formData.authSettingId=this.allRolesData.authSettingId
    this.formData.assignRoleCandidateId=this.allRolesData.assignCandidateRoleList[0].roleId;
    this.formData.manpowerRoleRequestId=this.allRolesData.manPowerRequestRoleList[0].roleId;
debugger
    this.formData.approveRoleCandidateId=this.allRolesData.approveCandidateRoleList[0].roleId;
    this.formData.provisionalRolePassId=this.allRolesData.provisionalPassRoleList[0].roleId;
    this.formData.medicalRoleId=this.allRolesData.medicalRoleList[0].roleId;
    this.formData.safetyRoleId=this.allRolesData.safetyRoleList[0].roleId;
    this.formData.passGenerationRoleId=this.allRolesData.passGenerationRoleList[0].roleId;
    debugger
    this.formData.skipMedicalFlag=this.allRolesData.skipMedicalFlag
    // this.formData.manpowerRoleRequestId=this.allRolesData.manpowerRequestId.roleId;
  })
}
 
}

