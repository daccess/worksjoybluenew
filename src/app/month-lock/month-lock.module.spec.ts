import { MonthLockModule } from './month-lock.module';

describe('MonthLockModule', () => {
  let monthLockModule: MonthLockModule;

  beforeEach(() => {
    monthLockModule = new MonthLockModule();
  });

  it('should create an instance', () => {
    expect(monthLockModule).toBeTruthy();
  });
});
