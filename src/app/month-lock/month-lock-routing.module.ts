import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonthLockComponent } from './month-lock.component';
const routes: Routes = [
  { 
             
    path: '', component: MonthLockComponent,

        children:[
            {
                path:'',
                redirectTo : 'monthlock',
                pathMatch :'full'
                
            },
        
            {
                path:'monthlock',
                component: MonthLockComponent
            }

        ]

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonthLockRoutingModule { }
