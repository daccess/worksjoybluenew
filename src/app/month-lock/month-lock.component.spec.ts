import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthLockComponent } from './month-lock.component';

describe('MonthLockComponent', () => {
  let component: MonthLockComponent;
  let fixture: ComponentFixture<MonthLockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthLockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthLockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
