import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonthLockRoutingModule } from './month-lock-routing.module';
import { MonthLockComponent } from './month-lock.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    MonthLockRoutingModule,
    FormsModule
  ],
  declarations: [MonthLockComponent]
})
export class MonthLockModule { }
