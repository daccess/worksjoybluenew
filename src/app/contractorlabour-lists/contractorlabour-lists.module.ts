import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {contractorLabourlistRouting } from './contractorlabour-lists-routing.module';
import { ContractorLabourListComponent } from '../contractor-labour-list/contractor-labour-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContractorlabourListsComponent } from './contractorlabour-lists.component';

@NgModule({
  imports: [
    CommonModule,
    contractorLabourlistRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ContractorlabourListsComponent
  ]
})
export class ContractorlabourListsModule { }
