import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorlabourListsComponent } from './contractorlabour-lists.component';

describe('ContractorlabourListsComponent', () => {
  let component: ContractorlabourListsComponent;
  let fixture: ComponentFixture<ContractorlabourListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorlabourListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorlabourListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
