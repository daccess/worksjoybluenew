import { ContractorlabourListsModule } from './contractorlabour-lists.module';

describe('ContractorlabourListsModule', () => {
  let contractorlabourListsModule: ContractorlabourListsModule;

  beforeEach(() => {
    contractorlabourListsModule = new ContractorlabourListsModule();
  });

  it('should create an instance', () => {
    expect(contractorlabourListsModule).toBeTruthy();
  });
});
