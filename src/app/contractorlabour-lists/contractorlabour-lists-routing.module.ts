import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractorlabourListsComponent } from './contractorlabour-lists.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: ContractorlabourListsComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'contractorLabourLists',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'contractorLabourLists',
                  component: ContractorlabourListsComponent
              }

          ]

  }


]

export const contractorLabourlistRouting = RouterModule.forChild(appRoutes);
