import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-contractorlabour-lists',
  templateUrl: './contractorlabour-lists.component.html',
  styleUrls: ['./contractorlabour-lists.component.css']
})
export class ContractorlabourListsComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  empids: any;
  token: any;
  user: string;
  users: any;
  LabourDatas: any;
  locationdata: any;
  locationId: any;
  labourlistEdit: string;
  labourIds: any;
  manReqStatusIde: any;
  labourId: any;

  constructor(public httpservice:HttpClient,public router:Router,public Spinner:NgxSpinnerService,public toastr: ToastrService) { }

  ngOnInit() {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user)
    this.token = this.users.token;
    this.empids=this.users.roleList.empId;
    this.locationId=this.users.roleList.locationId
    this.getlocationData()

  }
  getlocationData(){
      let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
      this.httpservice.get(url,{headers}).subscribe(data => {
      
        this.locationdata = data["result"];
          this.locationId=this.locationId
          this.getLabourData();
    },
      (err: HttpErrorResponse) => {
    
      })
    
        }
  

  getLabourData() {
    
    let url = this.baseurl+`/getFinalApproveFrontDeskLabourList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      
      this.LabourDatas = data['result']
      setTimeout(function () {

        $(function () {

          var table = $('#emptable').DataTable({

            retrieve: true,
            searching:false

         

          })

          // table
          // .order( [ 1, 'des' ] )
          // .draw();
          // $('.status-dropdown').on('change', function (e) {
          //   var status = $(this).val();
          //   $('.status-dropdown').val(status)
          //   console.log(status)
          //   table.column(6).search(status as AssignType).draw();
          // })


        });
      }, 100);
      this.Spinner.hide();
  
      // this.count1=this.manpowerAllData.length

    },
      // this.labourData=JSON.stringify(this.LabourData )
      // console.log("dsfasdfdsfsdfsdfsdafasdfsdaf",this.LabourData)
  
      (err: HttpErrorResponse) => {

      })

  }

  editLabour(e:any){
    
    this.labourlistEdit='true'
   this.labourIds= e.labourPerId
   
    sessionStorage.setItem("labourId",this.labourIds)
    sessionStorage.setItem("iseditLabour",'true')
    this.router.navigateByUrl('/layout/contractorLabour');


  }
 
  // deleteLabour(e:any){
    
  //   this.labourId=e.labourPerId
  //     let url = this.baseurl+`/deleteLabourInfoById/${this.labourId}`;
  //     const tokens = this.token;
  //     const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //     this.httpservice.delete(url, { headers }).subscribe(data => {
  //       this.toastr.success("Labour delete successfully")
  //       this.getLabourData()
  //     },(err: HttpErrorResponse) => {
  //       this.toastr.error("error while delete labour")
  // })
  
  //   }
}
