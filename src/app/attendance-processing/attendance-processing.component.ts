import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';
import { dataService } from '../shared/services/dataService';
import { InfoService } from "../shared/services/infoService";

@Component({
  selector: 'app-attendance-processing',
  templateUrl: './attendance-processing.component.html',
  styleUrls: ['./attendance-processing.component.css']
})
export class AttendanceProcessingComponent implements OnInit {
  selectedEmp= [];
  baseurl = MainURL.HostUrl;
  compId: any;
  loggedUser: any;
  dropdownSettings: any;
  Employeedata: any;
  model: any = {};
  todaysDate: any;
  selectedvalue: any;
  selectedLocationId: any=0;
  
  constructor(private dataService: dataService,
    private InfoService: InfoService,
    private router: Router,
    public httpService: HttpClient,
    public Spinner: NgxSpinnerService,
    private toastr: ToastrService) { 
      this.model.frmdate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.model.todate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
      this.todaysDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    }

  ngOnInit() {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    
    
    // this.getAllEmployee();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'bioId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 100,
      allowSearchFilter: true,
  };
  }
  selectType(e){
   
    if(this.loggedUser.roleHead=='HR'){
    this.selectedvalue=e.target.value
      let url = this.baseurl + '/EmployeeAttendanceProcess/';
      this.httpService.get(url + this.compId+'/'+this.loggedUser.locationId).subscribe(data => {
      const empData = data["result"];
      this.Employeedata=empData.filter((data)=>{ 
        return data.employmentType == this.selectedvalue;
      });
      //console.log("employeedataattandanceprocessing", this.Employeedata);
      for (let index = 0; index < this.Employeedata.length; index++) {
          this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
      }
      },(err: HttpErrorResponse) => {
      console.log(err.message);
      this.Employeedata =[];
      });
 
 
    }
    else if(this.loggedUser.roleHead=='Admin'){
      this.selectedvalue=e.target.value
      let url = this.baseurl + '/EmployeeAttendanceProcess/';
      this.httpService.get(url + this.compId+'/'+this.selectedLocationId).subscribe(data => {
      const empData = data["result"];
      this.Employeedata=empData.filter((data)=>{ 
        return data.employmentType == this.selectedvalue;
      });
      console.log("employeedataattandanceprocessing", this.Employeedata);
      for (let index = 0; index < this.Employeedata.length; index++) {
          this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
      }
      },(err: HttpErrorResponse) => {
      console.log(err.message);
      this.Employeedata =[];
      });
 
 
    }
}
  //get all employees
  // getAllEmployee() {
  //   let url = this.baseurl + '/EmployeeMasterOperation/';
  //   this.httpService.get(url + this.compId).subscribe(data => {
  //   this.Employeedata = data["result"];
  //   for (let index = 0; index < this.Employeedata.length; index++) {
  //       this.Employeedata[index].fullName = this.Employeedata[index].fullName+" "+this.Employeedata[index].lastName+" - "+this.Employeedata[index].empId ;   
  //   }
  //   },(err: HttpErrorResponse) => {
  //   console.log(err.message);
  //   this.Employeedata =[];
  //   });
  // }
  onItemSelect(item: any) {
    console.log("item", item);
    //get data
    for(let i=0;i<this.Employeedata.length;i++){
        if(this.Employeedata[i].bioId==item.bioId){
            let obj={
                "bioId":item.bioId,
            }
            this.selectedEmp.push(obj);   
        }
    }
  }
  onSelectAll(items: any) {
    console.log(items);
    this.selectedEmp = [];
    for(let j=0;j<items.length;j++){
        for(let i=0;i<this.Employeedata.length;i++){
            if(this.Employeedata[i].bioId==items[j].bioId){
                let obj={
                    "bioId":items[j].bioId,
                }
                this.selectedEmp.push(obj);   
            }
        }
    }
  }
  onItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedEmp.length; i++) {
        if (this.selectedEmp[i].bioId == item.bioId) {
            this.selectedEmp.splice(i, 1)
        }
    }
  }
  
  onDeSelectAll(item: any) {
    console.log(item);
    this.selectedEmp = [];
  }

  selectFromDate(value) {
    this.model.frmdate = value;
  }

  cancel(){
    this.router.navigate(['/layout/settingpage']);
  }

  processAttendance(){
    var selectedEmp = [];
    let url = "/attendanceDataReprocessFromDate";
    if(!this.model.selectedEmp){
      this.toastr.error("Please select employee...");
      return;
    }
    for(let i = 0; i < this.model.selectedEmp.length; i++){
      selectedEmp.push({
        "bioId": String(this.model.selectedEmp[i].bioId)
      });
    }
    let obj = {
      "fromDate": this.model.frmdate,
      "selectedEmp":selectedEmp,
      "toDate": this.model.todate
    }
    this.Spinner.show();
    this.dataService.createRecord(url, obj)
    .subscribe(data =>{
      this.Spinner.hide();
      if(data.statusCode == "200"){
        this.toastr.success("Attendance processed successfully...");
      }
      else{
        this.toastr.error("Data Already Processed !");
      }
     
    });
  }
}
