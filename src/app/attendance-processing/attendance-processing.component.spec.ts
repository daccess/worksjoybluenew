import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceProcessingComponent } from './attendance-processing.component';

describe('AttendanceProcessingComponent', () => {
  let component: AttendanceProcessingComponent;
  let fixture: ComponentFixture<AttendanceProcessingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceProcessingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
