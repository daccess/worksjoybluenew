import { Routes, RouterModule } from '@angular/router'
import { AttendanceProcessingComponent } from './attendance-processing.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: AttendanceProcessingComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'attendanceProcessing',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'attendanceProcessing',
                    component: AttendanceProcessingComponent
                }

            ]

    }
]

export const routing = RouterModule.forChild(appRoutes);