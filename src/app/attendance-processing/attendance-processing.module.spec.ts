import { AttendanceProcessingModule } from './attendance-processing.module';

describe('AttendanceProcessingModule', () => {
  let attendanceProcessingModule: AttendanceProcessingModule;

  beforeEach(() => {
    attendanceProcessingModule = new AttendanceProcessingModule();
  });

  it('should create an instance', () => {
    expect(attendanceProcessingModule).toBeTruthy();
  });
});
