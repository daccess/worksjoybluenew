import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttendanceProcessingComponent } from './attendance-processing.component';
import { routing } from './attendance-processing.routing';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  imports: [
    routing,
    CommonModule,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [AttendanceProcessingComponent]
})
export class AttendanceProcessingModule { }
