import { routing } from '../../app/transportmaster/transportmaster.routing';
import { NgModule } from '@angular/core';
 
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CalenderMasterService } from '../shared/services/clendermasterservice';
import { TransportmasterComponent } from './transportmaster.component';
import { BusmasterComponent } from './pages/busmaster/busmaster.component';
import { Busmaster2Component } from './pages/busmaster2/busmaster2.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { busStopMasterService } from '../transportmaster/pages/serviceModel/service/busstopservice';
import { busMasterService } from '../transportmaster/pages/serviceModel/service/busMasterService';
import { busRoutService } from '../transportmaster/pages/serviceModel/service/busRoutService';



import { from } from 'rxjs';
import { BusroutemasterComponent } from './pages/busroutemaster/busroutemaster.component';

@NgModule({
    declarations: [
        TransportmasterComponent,
        BusmasterComponent,
        Busmaster2Component,
        BusroutemasterComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    UiSwitchModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule
    ],
    providers: [CalenderMasterService, busMasterService, busStopMasterService,busRoutService]
  })
  export class TransportmasterModule { 
      constructor(){

      }
  }
  