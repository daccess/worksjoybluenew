import { Routes, RouterModule } from '@angular/router'

import { TransportmasterComponent } from './transportmaster.component';
import { BusmasterComponent } from './pages/busmaster/busmaster.component';
import { Busmaster2Component } from './pages/busmaster2/busmaster2.component';
import { BusroutemasterComponent } from './pages/busroutemaster/busroutemaster.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: TransportmasterComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'transportmaster',
                    pathMatch :'full'
                    
                },
                {
                    path:'busroutemaster',
                    component: BusroutemasterComponent
                },
                {
                    path:'busmaster1',
                    component: BusmasterComponent
                },
            
                {
                    path:'busmaster2',
                    component: Busmaster2Component
                }
            
               

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);