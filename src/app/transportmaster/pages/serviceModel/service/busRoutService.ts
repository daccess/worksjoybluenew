import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
//import { commaster } from'../model/companymastermodel'
import { busRoutModels } from '../../serviceModel/Model/busRoutModel';
import { from } from 'rxjs';
import { MainURL } from '../../../../shared/configurl';

@Injectable()
export class busRoutService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }
  postBusRoutdata(busroutemaster: busRoutModels){
    let url1 = this.baseurl + '/createBusRoute';

    var body = JSON.stringify(busroutemaster);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url1,body,requestOptions).map(x => x.json());
  }
}
