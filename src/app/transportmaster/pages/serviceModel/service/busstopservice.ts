import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
//import { commaster } from'../model/companymastermodel'
import { busStopModel } from '../Model/busstopModel';
import { from } from 'rxjs';
import { MainURL } from '../../../../shared/configurl';

@Injectable()
export class busStopMasterService {
  baseurl = MainURL.HostUrl
  locationId: any;
  constructor(private http : Http) { }

  postBusStopMasterdata(busstopmaster: busStopModel){
    let url = this.baseurl + '/createBusStop';
    var body = JSON.stringify(busstopmaster);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  

}