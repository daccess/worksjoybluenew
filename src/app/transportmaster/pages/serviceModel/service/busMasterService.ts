import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
//import { commaster } from'../model/companymastermodel'
import { busMasterModel } from '../Model/busMasterModel';
import { from } from 'rxjs';
import { MainURL } from '../../../../shared/configurl';

@Injectable()
export class busMasterService {
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  postBusMasterdata(busmaster: busMasterModel){
    let url = this.baseurl + '/createBus';
    var body = JSON.stringify(busmaster);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

}