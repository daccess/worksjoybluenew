export class busMasterModel{
   
    busId: number;
    busName: string;
    busStopName:string;
    busCode: string;
    status:Boolean;
    busVariable:string;
    vehicalCapacity:number;
    locationId: number;
    shiftMasterId:number;
    busStation:string;
    shiftType:string;
    busStopMasterList = [];
    busRouteMaster:any;
    busRouteId : any;
    locationMaster:any;
    busRouteName: string;
    busStopId: number;

    }