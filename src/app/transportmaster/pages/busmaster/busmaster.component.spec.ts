import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusmasterComponent } from './busmaster.component';

describe('BusmasterComponent', () => {
  let component: BusmasterComponent;
  let fixture: ComponentFixture<BusmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
