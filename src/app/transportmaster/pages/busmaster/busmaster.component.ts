import { Component, OnInit } from '@angular/core';
import { busMasterModel } from '../../pages/serviceModel/Model/busMasterModel';
import { busMasterService } from '../../pages/serviceModel/service/busMasterService';
import { from } from 'rxjs';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from '../../../shared/configurl';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-busmaster',
  templateUrl: './busmaster.component.html',
  styleUrls: ['./busmaster.component.css'],
  providers: [busMasterService]
})
export class BusmasterComponent implements OnInit {
  shiftType: any;
  isedit = false;
  busmModel: busMasterModel;
  baseurl = MainURL.HostUrl
  LocationAllData: any;
  selectedLocationobj: any;
  loggedUser: any;
  compId: any;
  StatusFlag: boolean = false;
  ShiftAllData: any;
  allBusStop: any;
  status: busMasterModel;
  flag: Boolean;
  locationMaster = {};
  errflag: boolean;
  dltmodelFlag: boolean;
  selectedShift: any = [];
  busStopMasterList = [];
  dropdownSettingsshift: {
    singleSelection: boolean;
    idField: string;
    textField: string;
    selectAllText: string;
    unSelectAllText: string;
    itemsShowLimit: number;
    allowSearchFilter: boolean;
  };
  busdata: any;
  Selectededitobj: any;
  dltObje: any;
  locationFlag = false;
  public selectUndefinedOptionValue: any;
  busSModel: any;
  busVariable: string;
  busStopList: any;
  busStopListname: any;
  busModellist: any;
  busRouteId: number;
  busList: any;
  busId: string;
  busRouteMaster: any;
  loconame: string;
  errordata: any;
  msg: any;
  busCode: string;
  requiredField: boolean = false;
  submitted = false
  locationId: any;
  busStopName: any;
  busRouteName: any;
  busStation: string;
  busStopId: any;

  constructor(public toastr: ToastrService, public busservice: busMasterService, public httpService: HttpClient, public chRef: ChangeDetectorRef) {

    this.busmModel = new busMasterModel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.busmModel.busStation = 'local';
    this.getAllBusdata()
    this.locationGRoup();

    this.dropdownSettingsshift = {
      singleSelection: false,
      idField: 'busStopId',
      textField: 'busStopName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }

  ngOnInit() {
  }

  //========================== multiple bus stop select Start =========================
  onItemSelect(item: any) {
    if (this.busStopMasterList.length == 0) {
      this.busStopMasterList.push(item)
    } else {
      this.busStopMasterList.push(item)
    }
  }

  onSelectAll(items: any) {
    this.busStopMasterList = items;
  }

  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.busStopMasterList.length; i++) {
      if (this.busStopMasterList[i].busStopId == item.busStopId) {
        this.busStopMasterList.splice(i, 1)
      } else {
      }
    }
  }
  onDeSelectAll(item: any) {
    this.busStopMasterList = [];
  }

  //===========================select bus stop End======================= 

  dataTableFlag: boolean = true
  getAllBusdata() {
    this.busmModel.status = this.flag;
    this.dataTableFlag = false;
    let url = this.baseurl + '/activeBusData/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.busdata = data['result'];
        this.dataTableFlag = true
        this.chRef.detectChanges();
        setTimeout(function () {
          $(function () {
            var table = $('#busmodel').DataTable();
          });
        }, 1000);

        this.chRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
      });
  }
  locationGRoup() {
    let url4 = this.baseurl + '/Location/Active/';
    this.httpService.get(url4 + this.compId).subscribe(data => {
        this.LocationAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      }
    );
  }
  Locationdata(event) {
    this.selectedLocationobj = parseInt(event.target.value);
    this.getbusStopName(event.target.value);
  }
  buttonDisable: boolean = true
  getbusStopName(selectedLocationobj) {
    this.busStopList = [];
    this.locationId = selectedLocationobj;
    let busurl = this.baseurl + '/getRouteDataByLocationId/';
    this.httpService.get(busurl + this.locationId).subscribe(data => {
        this.busStopList = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }
  getbusRoutName() {
    let routurl = this.baseurl + '/activeBusRouteMasterList';
    this.httpService.get(routurl).subscribe(data => {
        this.busModellist = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }
  busRoutData(event) {
    this.busRouteId = parseInt(event.target.value);
    this.getBusStopByRouteId(this.busRouteId);
  }
  submitData(f) {
    this.StatusFlag = false;
    this.busmModel.status = this.flag;
    if (this.isedit == false) {
      this.busmModel.locationMaster = {};
      this.busmModel.locationId = this.selectedLocationobj;
      for(let i = 0; i < this.busStopName.length; i++)
      {
       this.busmModel.busStopMasterList.push({
        busStopId: this.busStopName[i].busStopId
       })
      }

      this.busservice.postBusMasterdata(this.busmModel).subscribe(data => {
          this.toastr.success('Bus master added successfully!', 'Bus Master');
          this.getAllBusdata();
          f.reset();
          setTimeout(() => {
            this.busmModel.busStopMasterList = []
            this.busmModel.busStation = 'local';
          }, 100);

        },
          (err: HttpErrorResponse) => {
            this.toastr.error('Server Side Error..!', 'Bus Master');
          });

    }
    else {
      this.errflag = false
      this.busmModel.locationMaster = {};
      this.busmModel.locationId = this.selectedLocationobj;
      this,this.busmModel.busStation = this.busStation;
      let url3 = this.baseurl + '/updateBus';
      this.httpService.put(url3, this.busmModel).subscribe(data => {
          f.reset();
          setTimeout(() => {
            this.busmModel.busStopMasterList = []
            this.busmModel.busStation = 'local';
          }, 100);
          this.toastr.success('Bus master updated Successfully!', 'Bus  Master');
          this.getAllBusdata();
          this.isedit = false;
        },
          (err: HttpErrorResponse) => {
            this.isedit = false;
            this.toastr.error('Server Side Error..!', 'Bus  Master');
          });
    }
  }
  editbusMaster(object) {
    this.Selectededitobj = object.busId;
    this.busRouteId = object.busRouteId;
    this.busmModel.busId = this.Selectededitobj;
    this.isedit = true;
    this.StatusFlag = true;
    this.flag = object.status;
    this.editMasterbject(this.Selectededitobj,object.locationId,object.busRouteId)
  }
  editMasterbject(Selectededitobj,location,busRouteId) {
    this.getbusStopName(location);
    this.getBusStopByRouteId(busRouteId);
    let urledit = this.baseurl + '/bus/getById/';
    this.httpService.get(urledit + Selectededitobj).subscribe(data => {
        let busdatad = data['result'];
        this.busmModel.status = data['result']['status'];
        this.busmModel.busCode = data['result']['busCode'];
        this.busmModel.vehicalCapacity = data['result']['vehicalCapacity'];
        this.selectedLocationobj = data['result']['locationMaster']['locationId'];
        this.busmModel.shiftType = data['result']['shiftType'];
        this.loconame = this.busmModel.busCode;
        this.busStation = data['result']['busStation'];
        this.busmModel.busRouteId = busdatad.busRouteMaster.busRouteId;
        this.busmModel.busStopName = busdatad.busStopName;
      },
      (err: HttpErrorResponse) => {
      });
  }
  resetForm() {
    this.selectedLocationobj = this.selectUndefinedOptionValue;
  }
  dltmodelEvent(f) {
    this.dltmodelFlag = true;
    this.deleteBus(this.dltObje);
    f.reset()
    setTimeout(() => {
      this.busmModel.busStopMasterList = []
      this.busmModel.busStation = 'local';
    }, 100);
    this.reset()
  }
  dltObj(obj) {
    this.dltmodelFlag = false;
    this.dltObje = obj.busId;
    this.deleteBus(this.dltObje);
  }
  deleteBus(object) {
    if (this.dltmodelFlag == true) {
      this.busdata.busId = this.dltObje
      let url2 = this.baseurl + '/deleteByBusId/';
      this.httpService.delete(url2 + this.dltObje).subscribe(data => {
          this.toastr.success('Bus master deleted successfully')
          this.getAllBusdata();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('References is present so you can not delete..!');
        });
    }
  }
  uiswitch(event) {
  }
  open(c) {
    this.flag = !this.flag;
  }
  error() {
    if (this.loconame != this.busmModel.busCode) {
      this.busCode = this.busmModel.busCode
      let url = this.baseurl + '/busCode/';
      this.httpService.get(url + this.busCode).subscribe(
        (data: any) => {
          this.errordata = data["result"];
          this.msg = data.message;
          this.toastr.error(this.msg);
          this.errflag = true
        },
        (err: HttpErrorResponse) => {
          this.errflag = false
        }
      );
    }
  }
  reset() {
    this.isedit = false
    this.StatusFlag = false;
   
    setTimeout(() => {
      this.busmModel.busStopMasterList = []
      this.busmModel.busStation = 'local';

    }, 100);
  }
//  ...................................................................................................................................  
  getBusStopByRouteId(busRouteId) {
    let url=this.baseurl + '/getBusStopDataByRouteId/';
    this.httpService.get(url + busRouteId).subscribe((data:any)=>{
        this.busStopName = data.result;
      }      
    )
  }
}
