import { Component, OnInit,Output,Input, EventEmitter, ViewChild } from '@angular/core';
declare var $;
import { busStopModel } from '../../pages/serviceModel/Model/busstopModel';
import { busStopMasterService } from '../../pages/serviceModel/service/busstopservice';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ChangeDetectorRef } from '@angular/core';
import { MainURL } from '../../../shared/configurl';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-busmaster2',
  templateUrl: './busmaster2.component.html',
  styleUrls: ['./busmaster2.component.css'],
  providers: [busStopMasterService]
})
export class Busmaster2Component implements OnInit {

  busSModel: busStopModel;
  baseurl = MainURL.HostUrl
  busGroupAllData: any;
  loggedUser: any;
  compId: any;
  selectedbusobj: number;
  LocationAllData: any;
  selectedLocationobj: any;
  StatusFlag: boolean = false;
  AllBusStopdata: any;
  Selectededitobj: any;
  isedit = false;
  exiestName:any
  msg: any;
  dltmodelFlag: boolean;
  status: busStopModel;
  flag: Boolean;
  dltObje: any;
  public selectUndefinedOptionValue:any;
  busStopllist: any;
  busStopId: string;
  busDatallist: any;
  busModelList: any;
  Selectobj: any;
  locationId: any;
  lId: any;
  errflag :boolean;
  errordata: any;
  busStopName:any
  locationName: any;
  busRouteId: any;
  constructor(public busstopservice: busStopMasterService, public httpService: HttpClient, public chRef: ChangeDetectorRef, public toastr: ToastrService) {
    this.busSModel = new busStopModel();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.locationGRoup();
    this.getbusallData();
  }
  ngOnInit() {
    this.getbusRoutData();
  }

  locationGRoup() {
    let url4 = this.baseurl + '/Location/Active/';
    this.httpService.get(url4 + this.compId).subscribe(data => {
        this.LocationAllData = data["result"];
      }, (err: HttpErrorResponse) => {
      });
  }
  Locationdata(event) {
    this.selectedLocationobj = parseInt(event.target.value);
    this.busgroup(this.selectedLocationobj);
    this.busGroupAllData=[];
    this.error();
  }

  busgroup(selectedLocationobj) {
    let url4 = this.baseurl + '/activebusdata/';
    this.httpService.get(url4 + selectedLocationobj).subscribe(data => {
        this.busGroupAllData = data["result"];

      }, (err: HttpErrorResponse) => {
      });
  }

  busdata(event) {
    this.selectedbusobj = parseInt(event.target.value);
    this.busSModel.busId = this.selectedbusobj;
  }
  dataTableFlag : boolean  = true
  
  getbusallData(){
      this.busSModel.status = this.flag; 
      this.dataTableFlag   = false;
      let url2 = this.baseurl + '/busStopList';
      this.httpService.get(url2).subscribe((data: any) => {
      this.busStopllist = data.result;
      this.dataTableFlag   = true;
      this.chRef.detectChanges();
      setTimeout(function () {
        $(function () {
          var table = $('#busmodel').DataTable();
        });
      }, 1000);
      this.chRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
    });
 }

onSubmitData(f){
  this.StatusFlag=false;
  this.busSModel.status = this.flag;
  if(this.isedit == false){
  this.busSModel.locationMaster={};
  this.busSModel.busRouteId=f.value.busRouteId;
  this.busstopservice.postBusStopMasterdata(this.busSModel).subscribe(data => {
  this.toastr.success('Bus stop master added successfully!', 'Bus stop Master');
  this.getbusallData();
  f.reset();
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Server Side Error..!', 'Bus stop Master');
  });
}
else{
      this.errflag = false;
      this.busSModel.locationMaster={};
      this.busSModel.busRouteId=f.value.busRouteId;
      let url3 = this.baseurl + '/updateBusStop';
      this.httpService.put(url3,this.busSModel).subscribe(data => {
        f.reset();
        this.toastr.success(' Bus stop master updated successfully!', 'Bus stop Master');
        this.getbusallData();
        this.isedit=false;
      },
      (err: HttpErrorResponse) => {
        this.isedit=false;
        this.toastr.error('Server Side Error..!', 'Bus stop Master');
      });

    }
}

  editbusStop(object) {
  this.Selectededitobj = object.busStopId;
  this.busSModel.busStopId = this.Selectededitobj;
   this.isedit = true;
   this.StatusFlag=true;
   this.flag = object.status;
   this.editStopobject(this.Selectededitobj);
 }
 editStopobject(Selectededitobj){
  let urledit = this.baseurl + '/getById/';
  this.httpService.get(urledit + Selectededitobj).subscribe(data => {
      this.busSModel.status = data['result']['status'];
      this.busSModel.busStopName = data['result']['busStopName'];
      this.busSModel.busRouteName = data['result']['busRouteMaster']['busRouteName'];
      this.busRouteId = data['result']['busRouteMaster']['busRouteId'];
      this.exiestName = this.busSModel.busStopName;
    },
    (err: HttpErrorResponse) => {
    });
}

dltmodelEvent(f){
  this.dltmodelFlag=true;
  this.deleteBusStop(this.dltObje);
  f.reset();
  this.reset();
}

dltObj(obj){
  this.dltmodelFlag=false;
  this.dltObje = obj.busStopId;
  this.deleteBusStop(this.dltObje);
}

deleteBusStop(object){
  if (this.dltmodelFlag == true) {
  this.busStopllist.busStopId=this.dltObje
  let url2=this.baseurl + '/deleteBusStop/';
  this.httpService.delete(url2 + this.dltObje).subscribe(data => {
      this.toastr.success('Bus stop master deleted successfully');
      this.getbusallData();
    },(err: HttpErrorResponse) => {
      this.toastr.error('References is present so you can not delete..!');
    });
  }
}
open(c){
  this.flag = !this.flag;
}

error(){
  if( this.exiestName != this.busSModel.busStopName){
    this.busStopName=this.busSModel.busStopName;
    this.locationId = this.selectedLocationobj;
    let url = this.baseurl + '/busStop/?busStopName='+this.busStopName +'&locationId='+this.selectedLocationobj;
    this.httpService.get(url).subscribe((data :any) => {
       this.errordata= data["result"];
       this.msg= data.message
       this.toastr.error(this.msg);
       this.errflag = true
      },
      (err: HttpErrorResponse) => {
        this.errflag = false
      });
  }
}


@Input() adressType: string; 
@Output() setAddress: EventEmitter<any> = new EventEmitter();
@ViewChild('addresstext') addresstext: any;
    busStopLocation : string = "";
    queryWait: boolean;
    ngAfterViewInit() {
      this.getPlaceAutocomplete();
  }

  private getPlaceAutocomplete() {
  }
  reset(){
    this.isedit = false
    this.StatusFlag= false;
  }

  busModellist:any;
  getbusRoutData(){
    let url = this.baseurl + '/busRouteByList';
    this.httpService.get( url).subscribe((data: any) => {
      this.busModellist = data.result;
    });
  }
}