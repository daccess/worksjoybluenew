import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Busmaster2Component } from './busmaster2.component';

describe('Busmaster2Component', () => {
  let component: Busmaster2Component;
  let fixture: ComponentFixture<Busmaster2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Busmaster2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Busmaster2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
