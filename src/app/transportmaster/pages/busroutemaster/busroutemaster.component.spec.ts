import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusroutemasterComponent } from './busroutemaster.component';

describe('BusroutemasterComponent', () => {
  let component: BusroutemasterComponent;
  let fixture: ComponentFixture<BusroutemasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusroutemasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusroutemasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
