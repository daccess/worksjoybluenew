import { Component, OnInit } from '@angular/core';
import { busRoutModels } from '../serviceModel/Model/busRoutModel';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { MainURL } from '../../../shared/configurl';
import { busRoutService } from '../../pages/serviceModel/service/busRoutService';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef } from '@angular/core';



@Component({
  selector: 'app-busroutemaster',
  templateUrl: './busroutemaster.component.html',
  styleUrls: ['./busroutemaster.component.css']
})
export class BusroutemasterComponent implements OnInit {
  busModel:busRoutModels
  loggedUser: any;
  compId: any;
  busRouteId:any
  StatusFlag: boolean = false;
  isedit = false;
  exiestName: any;
  baseurl = MainURL.HostUrl;
  AllBusRoutdata: any;
  busRouteCode: busRoutModels;
  flag: Boolean;
  status: busRoutModels;
  busModelList: any;
  busModellist: any;
  dltmodelFlag: boolean;
  Selectededitobj: any;
  dltObje: any;
  errordata: any;
  errflag: boolean;
  msg: any;
  busRouteName: any;
  LocationAllData: any;
  locationId: any;
  constructor(private httpService:HttpClient,private busroutService:busRoutService,public toastr: ToastrService,public chRef: ChangeDetectorRef) { 
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId=this.loggedUser.compId;
    this.busModel = new busRoutModels();
    this.getLocation();
    this.getbusRoutData();
  }

  ngOnInit() {
  }
  dataTableFlag : boolean  = true
getbusRoutData(){
  this.busModel.status = this.flag;
  this.dataTableFlag   = false
  let url = this.baseurl + '/busRouteByList';
  this.httpService.get( url).subscribe((data: any) => {
    this.busModellist = data.result
    this.dataTableFlag   = true
    this.chRef.detectChanges();
    setTimeout(function () {
      $(function () {
        var table = $('#busmodel').DataTable();
      });
    }, 1000);
    this.chRef.detectChanges();
  },
  (err: HttpErrorResponse) => {
  });
}
onSubmit(f){
  this.StatusFlag=false
  this.busModel.status = this.flag;
  this.busModel.locationId= f.value.locationId;
  if(this.isedit == false){
    this.busroutService.postBusRoutdata(this.busModel).subscribe(data => {
    this.toastr.success('Bus route master added Successfully!', 'Bus Route Master');
    this.getbusRoutData();
    f.reset();
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Server Side Error..!', 'Bus Route Master');
  });
}else{
    this.busModel.locationId= f.value.locationId;
    let url3 = this.baseurl + '/updateBusRouteMaster';
    this.httpService.put(url3,this.busModel).subscribe(data => {
      f.reset();
      this.toastr.success('Bus route master updated successfully!', 'Bus Route Master');
      this.getbusRoutData();
      this.isedit=false;
    },
    (err: HttpErrorResponse) => {
      this.isedit=false;
      this.toastr.error('Server Side Error..!', 'Bus Route Master');
    });
  }
}

  editbus(object) {
    this.Selectededitobj = object.busRouteId;
    this.busModel.busRouteId = this.Selectededitobj;
    this.isedit = true;
    this.StatusFlag=true;
    this.flag = object.status;
    this.editobject(this.Selectededitobj);
  }
 editobject(Selectededitobj){
   let urledit = this.baseurl + '/getBusRouteById/';
   this.httpService.get(urledit + Selectededitobj).subscribe(data => {
       this.busModel.status = data['result']['status'];
       this.busModel.busRouteName = data['result']['busRouteName'];
       this.busModel.busRouteCode = data['result']['busRouteCode'];
       this.locationId = data['result']['locationMaster']['locationId'];
       this.busModel.locationName = data['result']['locationMaster']['locationName'];
       this.exiestName = this.busModel.busRouteName;
     },
     (err: HttpErrorResponse) => {
     });
 }

dltmodelEvent(f){
  this.dltmodelFlag=true;
  this.deleteBusRout(this.dltObje);
  f.reset();
  this.reset();
}

dltObj(obj){
  this.dltmodelFlag=false;
  this.dltObje = obj.busRouteId;
  this.deleteBusRout(this.dltObje);
}

deleteBusRout(object){
  if (this.dltmodelFlag == true) {
    this.busModellist.busRouteId=this.dltObje;
    let url2=this.baseurl + '/deleteBusRouteMaster/';
    this.httpService.delete(url2 + this.dltObje).subscribe(data => {
        this.toastr.success('Bus route master deleted successfully');
        this.getbusRoutData();
      },(err: HttpErrorResponse) => {
        this.toastr.error('References is present so you can not delete..!');
    });
  }
}
open(c){
  this.flag = !this.flag;
}

error(){
 
  if( this.exiestName != this.busModel.busRouteName){
    this.busRouteName=this.busModel.busRouteName;
    let url = this.baseurl + '/busRoute/';
    this.httpService.get(url + this.busRouteName).subscribe((data :any) => {
       this.errordata= data["result"];
       this.msg= data.message;
       this.toastr.error(this.msg);
       this.errflag = true;
      },
      (err: HttpErrorResponse) => {
        this.errflag = false;
      });
  }
}
reset(){
  this.isedit = false;
  this.StatusFlag= false;
}

getLocation(){
    let url4 = this.baseurl + '/Location/Active/';
    this.httpService.get(url4 + this.compId).subscribe(data => {
    this.LocationAllData = data["result"];
    }, (err: HttpErrorResponse) => {
    });
  }
}
