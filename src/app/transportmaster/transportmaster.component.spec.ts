import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportmasterComponent } from './transportmaster.component';

describe('TransportmasterComponent', () => {
  let component: TransportmasterComponent;
  let fixture: ComponentFixture<TransportmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
