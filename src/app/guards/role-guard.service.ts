import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';

@Injectable()
export class RoleGuard implements CanActivate {

  loggedUser: any;
  returnUrl : string = "/"
  constructor(private _authService: AuthService, private _router: Router, private _location: Location) {
    // this.loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));

    let name = next.routeConfig.path;
    this.returnUrl = state.url;

    // ================= New Role Master Starts ===========================

    if (state.url == '/layout/mymenu/mymenu') {
      if (this.loggedUser.mrollMaster.myUser) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/emppimsview') {
      if (this.loggedUser.mrollMaster.profile) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/emppimsview/empdetailsview') {
      if (this.loggedUser.mrollMaster.profile) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/emphistory/emphistory') {
      if (this.loggedUser.mrollMaster.history) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
   
    else if (state.url == '/layout/attendancemenu/attendancemenu') {
      if (this.loggedUser.mrollMaster.attendance) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/yearlycalendar/yearlycalendar') {
      if (this.loggedUser.mrollMaster.yearlyCalender) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/leaverequest/leaverequest') {
      if (this.loggedUser.mrollMaster.applyLeave) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/otherrequests/otherrequests') {
      if (this.loggedUser.mrollMaster.arRequest) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/leavedashboard/leavedashboard') {
      if (this.loggedUser.mrollMaster.leaveDashboard) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/attendencereportview/viewattendencereport') {
      if (this.loggedUser.mrollMaster.attendanceReport) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsmenu/pmsmenu') {
      if (this.loggedUser.mrollMaster.pms) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsdashboardemp/pmsdashboardemp') {
      if (this.loggedUser.mrollMaster.pmsDashboard) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/pmsdashboardemp') {
    //   if (this.loggedUser.mrollMaster.pmsDashboard) {
    //     return true;
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    else if (state.url == '/layout/pmskrasetting/pmskrasetting') {
      if (this.loggedUser.mrollMaster.goalSettingUser) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsselfapprisal/pmsselfapprisal') {
      if (this.loggedUser.mrollMaster.selfAppraisalUser) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsviewprevappraisal/pmsviewprevappraisal') {
      if (this.loggedUser.mrollMaster.previousAppraisalUser) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
  
  
    else if (state.url == '/layout/expenses/expenses') {
      if (this.loggedUser.mrollMaster.expenses) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/exitinterview/exitinterview') {
      if (this.loggedUser.mrollMaster.exitinterview) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/socialpage/socialpage') {
      if (this.loggedUser.mrollMaster.socialWorld) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/hrdashboard/hrdashboard') {
      if (this.loggedUser.mrollMaster.dashboardOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/myteamattendancemenu/myteamattendancemenu') {
      if (this.loggedUser.mrollMaster.myOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/supervisorleaverequest/supervisorleaverequest') {
      if (this.loggedUser.mrollMaster.applyLeaveOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/leavedashboard') {
    //   if (this.loggedUser.mrollMaster.leaveDashboardother) {
    //     return true;
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    else if (state.url == '/layout/clearancepage') {
      if (this.loggedUser.mrollMaster.seperationRole) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/extraworkreq/extraworkreq') {
      if ((this.loggedUser.sanctioningAuthority || this.loggedUser.mrollMaster.extraworkRequest)) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/reports/reports') {
      if (this.loggedUser.mrollMaster.allReports) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/advanceleave/advanceleave') {
      if (this.loggedUser.mrollMaster.advanceLeave) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/leaveblock/leaveblock') {
      if (this.loggedUser.mrollMaster.leaveBlock) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/shiftupload/shiftupload') {
      if (this.loggedUser.mrollMaster.uploadShift) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/selfcertificationlist/selfcertificationlist') {
      if (this.loggedUser.mrollMaster.selfCertificationDetails) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsmenuadmin/pmsmenuadmin') {
      if (this.loggedUser.mrollMaster.pmsOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsdashboard/pmsdashboard') {
      if (this.loggedUser.mrollMaster.pmsDashboardOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsgoalsettingmngr/pmsgoalsettingmngr') {
      if (this.loggedUser.mrollMaster.goalSettingOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmskraapprovals/pmskraapprovals') {
      if (this.loggedUser.mrollMaster.goalapprovalsOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsappraisalmngr/pmsappraisalmngr') {
      if (this.loggedUser.mrollMaster.appriasalsOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsapprisalshr/pmsapprisalshr') {
      if (this.loggedUser.mrollMaster.performanceStatus) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmsappcalconfig/pmsappcalconfig') {
      if (this.loggedUser.mrollMaster.appraisalCalendarConfig) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pmssetting/pmssetting') {
      if (this.loggedUser.mrollMaster.pmsSettingsOther) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/matrixmanager/matrixmanager') {
      if (this.loggedUser.mrollMaster.matrixManager) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/settingpage') {
      if (this.loggedUser.mrollMaster.setting) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/master/companymaster') {
    //   if (this.loggedUser.mrollMaster.masters) {
    //     return true;
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    //}
    else if (state.url == '/layout/holiday2/holidaymaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/shift/shiftgroupmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/maincontractmaster/contractmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }




    else if (state.url == '/layout/maincontractmaster/contract') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }





    else if (state.url == '/layout/weeklymaster/weeklyoffmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/paymaster/payperiodmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/Device/devicemaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/transportmaster/busmaster1') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }


   

    else if (state.url == '/layout/transportmaster/busroutemaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }




    else if (state.url == '/layout/transportmaster/busmaster2') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }






    else if (state.url == '/layout/asset/assetmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }

    else if (state.url == '/layout/documentmaster/documentmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }

    // new masters starts

    // else if (state.url == '/layout/master/companymaster') {
    //   if (this.loggedUser.mrollMaster.masters) {
    //     return true
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }

    else if (state.url == '/layout/master/businessmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/master/functionmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/master/departmentmaster') {
    //   if (this.loggedUser.mrollMaster.masters) {
    //     return true
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    else if (state.url == '/layout/master/subdepartmentmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/master/emptypemaster') {
    //   if (this.loggedUser.mrollMaster.masters) {
    //     return true
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }

    else if (state.url == '/layout/master/subemptypemaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/master/paygroupmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/master/grademaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/master/designationmaster') {
    //   if (this.loggedUser.mrollMaster.masters) {
    //     return true
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    
    // else if (state.url == '/layout/master/locationmaster') {
    //   if (this.loggedUser.mrollMaster.masters) {
    //     return true
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }

    else if (state.url == '/layout/paymaster/payperiodmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/shift/shiftgroupmaster' || state.url == '/layout/shift/shiftmaster') {
      if (this.loggedUser.mrollMaster.masters) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }

    // new masters ends
    else if (state.url == '/layout/leaveconfiguration/leaveconfiguration') {
      if (this.loggedUser.mrollMaster.leaveConfig) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/leaveconfiguration/topupleavebalance') {
      if (this.loggedUser.mrollMaster.leaveConfig) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/approverlevel/approverlevel') {
      if (this.loggedUser.mrollMaster.leaveConfig) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/configuration/configuration') {
       if (this.loggedUser.mrollMaster.attenConfig) {
         return true
        }
        else {
          this._router.navigate(['/layout/home']);
          return false;
       }
    }
    else if (state.url == '/layout/policyupload/policyupload') {
      if (this.loggedUser.mrollMaster.policies) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/queryconfiguration/queryconfiguration') {
      if (this.loggedUser.mrollMaster.queryConfig) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/rolemasterr') {
      if (this.loggedUser.mrollMaster.roleMaster) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/customizedashboard/customizedashboard') {
      if (this.loggedUser.mrollMaster.customizeDashboard) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
   
    else if (state.url == '/layout/mailsetting/mailsetting') {
      if (this.loggedUser.mrollMaster.emailSetting) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/workforcemenu/workforcemenu') {
    //   if (this.loggedUser.mrollMaster.workforce) {
    //     return true;
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    else if (state.url == '/layout/employeelist/employeelist') {
      if (this.loggedUser.mrollMaster.employee) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pimshr') {
      if (this.loggedUser.mrollMaster.employee) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pimshr/details') {
      if (this.loggedUser.mrollMaster.employee) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/pimshr/details?q=education') {
      if (this.loggedUser.mrollMaster.employee) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
   
    else if (state.url == '/layout/importemployee/importemployee') {
      if (this.loggedUser.mrollMaster.employee) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else if (state.url == '/layout/deactivateemployee/deactivateemployee') {
    //   if (this.loggedUser.mrollMaster.employee) {
    //     return true;
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    else if (state.url == '/layout/dashboard/dashboard') {
      if (this.loggedUser.mrollMaster.analytics) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    else if (state.url == '/layout/info-changes-notification/info-changes-notification') {
      if (this.loggedUser.mrollMaster.employeeVerification) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
   
    else if (state.url == '/layout/bulkuploading/bulkuploading') {
      if (this.loggedUser.mrollMaster.bulkUpload) {
        return true;
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
   
    // else if(state.url== '/layout/newregularisation/newregularisation'){
    //   if(this.loggedUser.sanctioningAuthority || this.loggedUser.queryRole){
    //     return true
    //   }
    //   else {
    //     this._router.navigate(['/layout/home']);
    //     return false;
    //   }
    // }
    else if (state.url == '/layout/clearancepage/clearancepage') {
      if (this.loggedUser.seperationRole) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }                      
    else if (state.url == '/layout/preonboarding/preonboarding') {
      if (this.loggedUser.employee) {
        return true
      }
      else {
        this._router.navigate(['/layout/home']);
        return false;
      }
    }
    // else {
    //   if(this.loggedUser.employeeStatus){
    //     this._router.navigateByUrl('/layout/emppims/empdetails?q=personal');
    //   }
    //   else{
    //     this._router.navigate(['/layout/home']);
    //   }
      
    //   return false;
    // }
    // ================= New Role Master Ends ===========================


  //   if (state.url == '/layout/leaveblock/leaveblock') {
  //     if (this.loggedUser.mrollMaster.leaveBlock) {
  //       return true;
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/leaveconfiguration/leaveconfiguration') {
  //     if (this.loggedUser.mrollMaster.leaveConfiguration) {
  //       return true;

  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/approverlevel/approverlevel') {
  //     if (this.loggedUser.mrollMaster.setting==true) {
  //       return true;

  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }
  //   }
  //   else if(state.url== '/layout/newregularisation/newregularisation'){
  //     if(this.loggedUser.sanctioningAuthority ==true || this.loggedUser.queryRole){
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/settingpage') {
  //     if (this.loggedUser.mrollMaster.setting == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/bulkuploading/bulkuploading') {
  //     if (this.loggedUser.mrollMaster.bulkUpload == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/configuration/configuration') {
  //     if (this.loggedUser.mrollMaster.attendanceConfiguration == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/clearancepage/clearancepage') {
  //     // this.loggedUser.mrollMaster.clearancePage == true ||
  //     if (this.loggedUser.seperationRole == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/clearancefor-hr/clearancefor-hr') {
  //     if (this.loggedUser.mrollMaster.clearancePageHr == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/maincontractmaster/contractmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/maincontractmaster/contract') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/dashboard/dashboard') {
  //     if (this.loggedUser.mrollMaster.pimsHr == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/mailsetting/mailsetting') {
  //     if (this.loggedUser.mrollMaster.emailSetting == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/hrdashboard/hrdashboard') {
  //     if (this.loggedUser.mrollMaster.attendanceDashboard == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }

  //   else if (state.url == '/layout/employeelist/employeelist') {
  //     if (this.loggedUser.mrollMaster.employeeList == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/importemployee/importemployee') {
  //     if (this.loggedUser.mrollMaster.employeeList == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/master/companymaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }

  //   else if (state.url == '/layout/master/businessmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/functionmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/departmentmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/subdepartmentmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/emptypemaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }

  //   else if (state.url == '/layout/master/subemptypemaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/paygroupmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/grademaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/designationmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/costcentermaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/master/locationmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }

  //   else if (state.url == '/layout/paymaster/payperiodmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
    
    
  //   else if (state.url == '/layout/pimshr') {
  //     if (this.loggedUser.mrollMaster.pimsHr == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/pimshr/details') {
  //     if (this.loggedUser.mrollMaster.pimsHr == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }

  //   else if (state.url == '/layout/Device/devicemaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/transportmaster/busmaster1') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/holiday2/holidaymaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/shift/shiftgroupmaster' || state.url == '/layout/shift/shiftmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);
  //       return false;
  //     }
  //   }
  //   else if (state.url == '/layout/seprationsetting/seprationsetting/seprationsetting') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/queryconfiguration/queryconfiguration') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/policyupload/policyupload') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/rolemaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else if (state.url == '/layout/rolemasterr') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }


  //   else if (state.url == '/layout/customizedashboard/customizedashboard') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }

  //   else if (state.url == '/layout/weeklymaster/weeklyoffmaster') {
  //     if (this.loggedUser.mrollMaster.masters == true) {
  //       return true
  //     }
  //     else {
  //       this._router.navigate(['/layout/home']);

  //       return false;
  //     }

  //   }
  //   else {
  //     this._router.navigate(['/layout/home']);

  //     return false;
  //   }
  }

}
