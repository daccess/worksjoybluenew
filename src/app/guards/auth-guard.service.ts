
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

loggedUser:any
  constructor(private _authService: AuthService, private _router: Router) {
    this.loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'))
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.loggedUser.sanctioningAuthority) {
        return true;
    }

    // navigate to login page
    
    this._router.navigate(['/layout/home']);
    // you can save redirect url so after authing we can move them back to the page they requested
    return false;
  }

}
