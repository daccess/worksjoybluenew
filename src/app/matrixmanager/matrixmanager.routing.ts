import { Routes, RouterModule } from '@angular/router'
import { MatrixmanagerComponent } from './matrixmanager.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: MatrixmanagerComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'matrixmanager',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'matrixmanager',
                    component: MatrixmanagerComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);