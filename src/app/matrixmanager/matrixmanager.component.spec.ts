import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatrixmanagerComponent } from './matrixmanager.component';

describe('MatrixmanagerComponent', () => {
  let component: MatrixmanagerComponent;
  let fixture: ComponentFixture<MatrixmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatrixmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatrixmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
