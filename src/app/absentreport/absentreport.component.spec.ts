import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbsentreportComponent } from './absentreport.component';

describe('AbsentreportComponent', () => {
  let component: AbsentreportComponent;
  let fixture: ComponentFixture<AbsentreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbsentreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbsentreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
