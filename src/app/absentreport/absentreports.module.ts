import { routing } from './absentreports.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ExcelService } from './excel.service';

import { CommonModule, DatePipe } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AbsentreportComponent } from './absentreport.component';


@NgModule({
    declarations: [
        AbsentreportComponent
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule
    ],
    providers: [ExcelService,DatePipe]
  })
  export class AbsentReportsModule { 
      constructor(){

      }
  }
  