import { Routes, RouterModule } from '@angular/router'

import { AbsentreportComponent } from './absentreport.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: AbsentreportComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'absentreports',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'absentreports',
                    component: AbsentreportComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);