import { Component, OnInit } from '@angular/core';
import { ExcelService } from './excel.service';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { InfoService } from "../shared/services/infoService";
import { Router } from '@angular/router';
@Component({
  selector: 'app-absentreport',
  templateUrl: './absentreport.component.html',
  styleUrls: ['./absentreport.component.css'],
  providers: [ExcelService]
})
export class AbsentreportComponent implements OnInit {

  marked = true;
  marked1 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true
  marked9 = true
  nameCheckbox = true;
  empIdcheckbox = true;
  srNoCheckbox = true
  branchCheckbox = true;
  departmentCheckbox = true;
  categoryCheckbox = true;
  absentCheckbox = true;
  reasonChecckbox = true;
  statusDay = true;
  AllReport = [];
  fromDate: any;
  toDate: any;
  todaysDate: any;
  loggedUser: any;
  compName: any;
  user_data=[];
  report_time: string;
  absent_data: any;
  report_data:any;
  result_data_modify;
  constructor(private excelService: ExcelService, public toastr: ToastrService, private datePipe: DatePipe,private InfoService:InfoService,private router: Router) {
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    var customizeOrEmp = sessionStorage.getItem('customizeOrEmp');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    this.InfoService.currentMessage.subscribe(message => {
      if(message!='default message'){
        this.result_data_modify = JSON.parse(message);
      }else{
        this.router.navigateByUrl('layout/reports/reports');
      }
    });
   // console.log(this.AllReport.length);
    if(customizeOrEmp == "employee"){
      this.AllReport.push(this.result_data_modify);
    }
    else{
      this.AllReport = this.result_data_modify;
    }
 
  for(let i=0;i<this.AllReport.length;i++){
    if(this.AllReport[i].length!=0){
      for(let j=0;j<this.AllReport[i].length;j++){
        this.user_data.push(this.AllReport[i][j]);
      }
    }
  }
  this.absent_data = Array.from(this.user_data.reduce((m, {
    empId, fullName,lastName,attenDate,locationName,oldEmpId,statusOfDay,empTypeName,deptName,attendanceStatus}) => 
    m.set(empId, [...(m.get(empId) || []), {"empId":empId,"fullName":fullName,"lastName":lastName,"attenDate":attenDate,"locationName":locationName,"oldEmpId":oldEmpId,"statusOfDay":statusOfDay,"empTypeName":empTypeName,"deptName":deptName,"attendanceStatus":attendanceStatus}]), new Map), ([empno, empname]) => ({
      empno, "emp_details":empname
    }));
  this.report_data=this.absent_data;
}

  ngOnInit() {
  }
  toggleVisibility(e) {
    this.marked = e.target.checked;
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }
  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }

  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }
  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }
  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
  exportAsXLSX(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    this.todaysDate = new Date();
    filename='Absent-Report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss");
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }
  capturePdf() {
    var doc = new jsPDF('p', 'mm', 'a4');
    doc.autoTable({
        html: '#absent_employees',
        columnStyles: {
          1: {halign:'center'},
          2: {halign:'left'},
          3: {halign:'left'},
          4: {halign:'left'},
          5: {halign:'left'},
        },
        tableLineColor: [190, 191, 191],
        tableLineWidth: 0.75,
        headStyles : {
          fillColor: [103, 132, 130],
        },
        styles: {
          halign: 'center',
          cellPadding: 0.5, fontSize: 6
        },
        didParseCell: function(cell, data) {
          if (cell.row.index === 3 && cell.row.section=="head") {
            cell.cell.styles.halign = 'right';
          }
        },
        theme: 'grid',
        pageBreak:'avoid',
        margin: { top: 20,  bottom: 20 }
      });
      var pageCount = doc.internal.getNumberOfPages();
      for (let i = 0; i < pageCount; i++) {
        doc.setPage(i);
        doc.setTextColor(0, 0, 0);
        doc.setFontSize(7);
        doc.setFontStyle("Arial");
        doc.text(13, 15, 'Abbreviation:-  WD: Working Day');
        doc.setTextColor(48, 80, 139);
        doc.setFontSize(7);
        doc.text(180, 15, doc.internal.getCurrentPageInfo().pageNumber + "/" + pageCount);
      }
      this.todaysDate = new Date();
      this.report_time=this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a");
    doc.save('Absent-Report'+this.report_time+'.pdf');  
  }
}
