import { Component, OnInit } from '@angular/core';
import { CompanyorganisationtreeService } from './shared/services/companyorganisationtree.service';
import { LayOutService } from '../shared/model/layOutService';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { MainURL } from '../shared/configurl';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  employesUnderEmpId:any;
  loggedUser: any;
  empSearchSuggestionList = [];
  searchText : string = "";
  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  compId: any;
  policydata: any;
  imageData: any;
  UrlDownload: any;
  profilePath: any;
  Extension: any;
  FirstLevelReportingManager :any;
  empId:any;
  parent_data=null;
  statuslocation:any;
  constructor(private service : CompanyorganisationtreeService, 
    private layOutService : LayOutService, 
    private router : Router,
    public httpService: HttpClient,
    public sanitizer: DomSanitizer){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.FirstLevelReportingManager =this.loggedUser.firstLevelReportingManager;
    if(this.Aws_flag=='true'){
      this.profilePath = this.loggedUser.profilePath;
    }else{
      this.profilePath=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.loggedUser.profilePath);
    }
    
    
  }
  
  ngOnInit() {
    this.policydatalist();
    this.getEmployesUnderEmpId(this.loggedUser.empId);
   }

  getEmployesUnderEmpId(empId){
     let model = {
      empId :empId
    }
  this.employesUnderEmpId = [];
  this.service.getEmployesUnderEmpId(model).subscribe(data=>{
    this.employesUnderEmpId = data.result;
    this.statuslocation = data.result.upperOrLower;
    },err=>{
  })
}

  getParentData(first_mgr,emp_id){
    let model ={
      empId :emp_id,
      firstLevelReportingManager: first_mgr
     }
    this.service.getEmployesUnderEmpId(model).subscribe(data=>{
      this.parent_data =data.result.employeePerInfoList[0];
      },err=>{
    })
  }
  underEmp(item,index){
    let model =  {
      empId : item.empId
    } 
    this.service.getEmployesUnderEmpId(model).subscribe(data=>{
      if(!item.employees){
        item.employees = data.result.employeePerInfoList;
      }
      else{
        item.employees = null;
      }  
    },err=>{ 
    })
  }
  searchFlag : boolean = false;
  getSearchSuggestionList(text){
    this.searchFlag = true;
    if(!text){
      this.empSearchSuggestionList = [];
    }
    if(text){
      this.layOutService.getSearchSuggestionList(text,this.loggedUser.locationId).subscribe(data=>{
        this.empSearchSuggestionList = data.result;
      },err=>{
        this.empSearchSuggestionList = [];
      })
   }
  }
  employeeData = [];
  suggestThisEmployee(item){
      if(this.FirstLevelReportingManager!=null){
     let model={
       empId :item.empId,
      }
    this.loggedUser = item;
      if(this.Aws_flag=='true'){
        this.profilePath = this.loggedUser.profilePath;
      }else{
        this.profilePath=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.loggedUser.profilePath);
      }
      this.searchText = item.fullName+" "+item.lastName;
      this.empSearchSuggestionList = [];
      this.getEmployesUnderEmpId(item.empId);
      this.service.getEmployesUnderEmpId(model).subscribe(data=>{
        if(data.result.upperOrLower == "Upper"){
            this.getParentData(item.firstLevelReportingManager,item.empId);
        }else{
        this.employesUnderEmpId = data.result;
        }
      },err=>{ 
      })
    }
  }
  resign(){
    this.router.navigateByUrl('/layout/resignationpage/resignationpage');
  }
  policydatalist(){
    let url = this.baseurl + '/policies/';
    this.httpService.get(url + this.compId).subscribe(data => {
       this.policydata = data["result"];
      },
      (err: HttpErrorResponse) => {
      }
    );
  }
  hideFlag : boolean =false
  downloadPolicy(data){
    this.hideFlag = false
    this.imageData = data.policyPath;
    if(this.Aws_flag=='true'){
      this.UrlDownload = this.sanitizer.bypassSecurityTrustResourceUrl(this.imageData);
    }else{
      this.UrlDownload = this.sanitizer.bypassSecurityTrustResourceUrl(data.polFileName);
    } 
    this.Extension = this.UrlDownload.changingThisBreaksApplicationSecurity.replace(/^.*\./, '');
    if(this.Extension == 'jpg' || this.Extension == 'jpeg'|| this.Extension == 'png'|| this.Extension == 'gif'|| this.Extension == 'tiff'){
      this.hideFlag = true;
      this.UrlDownload = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+this.imageData);
    }
    else{
      this.hideFlag = false;
      var a = document.createElement("a");
      // ... set a.href and a.download
      a.href = this.UrlDownload.changingThisBreaksApplicationSecurity;
      a['download'] = this.UrlDownload.changingThisBreaksApplicationSecurity;
      // Then click the link:
      var clickEvent = new MouseEvent("click", {
          "view": window,
          "bubbles": true,
          "cancelable": false
      });
      a.dispatchEvent(clickEvent);
      
    }
    if(this.Aws_flag!='true' && this.Extension=='pdf'){
      this.base64toPDF(data.policyPath);
    }
  }
  transform(path){
    let temp='data:image/png;base64,'+path;
    return this.sanitizer.bypassSecurityTrustResourceUrl(temp);
  }
  base64toPDF(data) {
    var bufferArray = this.base64ToArrayBuffer(data);
    var blobStore = new Blob([bufferArray], { type: "application/pdf" });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blobStore);
        return;
    }
    var data1 = window.URL.createObjectURL(blobStore);
    var link = document.createElement('a');
    document.body.appendChild(link);
    link.href = data1;
    link.download = "policy.pdf";
    link.click();
    window.URL.revokeObjectURL(data1);
    link.remove();
}

base64ToArrayBuffer(data) {
    var bString = window.atob(data);
    var bLength = bString.length;
    var bytes = new Uint8Array(bLength);
    for (var i = 0; i < bLength; i++) {
        var ascii = bString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
};
}
