import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { CompanyComponent } from 'src/app/company/company.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: CompanyComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'company',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'company',
                    component: CompanyComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);