import { TestBed, inject } from '@angular/core/testing';

import { ClmsGetApiService } from './clms-get-api.service';

describe('ClmsGetApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClmsGetApiService]
    });
  });

  it('should be created', inject([ClmsGetApiService], (service: ClmsGetApiService) => {
    expect(service).toBeTruthy();
  }));
});
