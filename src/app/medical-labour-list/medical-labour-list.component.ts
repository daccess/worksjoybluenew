import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medical-labour-list',
  templateUrl: './medical-labour-list.component.html',
  styleUrls: ['./medical-labour-list.component.css']
})
export class MedicalLabourListComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  locationdata: any;
  locationId: any;
  imageurls:any;
  allLabourListFormedical: any;
  manrequeststatusId: any;
  count:any;
  labourImage: any;
  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) {
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
    this.locationId=this.users.roleList.locationId
   }

  ngOnInit() {
    this.getlocationData()
  }
  getlocationData() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.locationdata = data["result"];
      this.locationId = this.locationId;
      this.getAllMedicalApprovalList();

    },
      (err: HttpErrorResponse) => {

      })

  }
  getAllMedicalApprovalList() {
    let url = this.baseurl + `/getMedicalLabourList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.allLabourListFormedical = data["result"];
    this.labourImage=this.allLabourListFormedical[0].requestStatus.labourInfo.labourImage

    },
      (err: HttpErrorResponse) => {
      });
  }
  getrequestDetails(e:any){
    this.manrequeststatusId=e.requestStatus.manReqStatusId
sessionStorage.setItem("manReqStatusId",this.manrequeststatusId)

    this.router.navigateByUrl('/layout/medicaleExaminationform/medicaleExaminationform')
  }
}
