import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { medicalRouting } from './medical-labour-list-routing.module';
import { MedicalLabourListComponent } from './medical-labour-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    medicalRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    MedicalLabourListComponent
  ]
})
export class MedicalLabourListModule { }
