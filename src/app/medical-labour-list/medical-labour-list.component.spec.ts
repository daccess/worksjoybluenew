import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalLabourListComponent } from './medical-labour-list.component';

describe('MedicalLabourListComponent', () => {
  let component: MedicalLabourListComponent;
  let fixture: ComponentFixture<MedicalLabourListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalLabourListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalLabourListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
