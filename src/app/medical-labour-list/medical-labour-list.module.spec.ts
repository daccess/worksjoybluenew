import { MedicalLabourListModule } from './medical-labour-list.module';

describe('MedicalLabourListModule', () => {
  let medicalLabourListModule: MedicalLabourListModule;

  beforeEach(() => {
    medicalLabourListModule = new MedicalLabourListModule();
  });

  it('should create an instance', () => {
    expect(medicalLabourListModule).toBeTruthy();
  });
});
