import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MedicalLabourListComponent } from './medical-labour-list.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: MedicalLabourListComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'medicalLabourList',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'medicalLabourList',
                  component: MedicalLabourListComponent
              }

          ]

  },

  // otherwise redirect to home

]

export const medicalRouting = RouterModule.forChild(appRoutes);