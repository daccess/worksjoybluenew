import { Routes, RouterModule } from '@angular/router'
import { SettingsmenuComponent } from './settingsmenu.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: SettingsmenuComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'settingsmenu',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'settingsmenu',
                    component: SettingsmenuComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);