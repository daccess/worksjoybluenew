import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftuploadComponent } from './shiftupload.component';

describe('ShiftuploadComponent', () => {
  let component: ShiftuploadComponent;
  let fixture: ComponentFixture<ShiftuploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftuploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
