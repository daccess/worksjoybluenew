import { routing } from './shiftupload.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import{CommonModule } from '@angular/common';
 import { DataTablesModule } from 'angular-datatables';
import { ShiftuploadComponent } from 'src/app/shiftupload/shiftupload.component';
import { ImportShiftServiceService, ExcelServiceShiftUpload } from './service/import-shift-service.service';


@NgModule({
    declarations: [
        ShiftuploadComponent

    
    ],
    imports: [
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
  
     
    ],
    providers: [ExcelServiceShiftUpload]
  })
  export class ShiftuploadModule { 
      constructor(){

      }
  }
  