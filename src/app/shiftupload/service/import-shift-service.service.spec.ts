import { TestBed, inject } from '@angular/core/testing';

import { ImportShiftServiceService } from './import-shift-service.service';

describe('ImportShiftServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImportShiftServiceService]
    });
  });

  it('should be created', inject([ImportShiftServiceService], (service: ImportShiftServiceService) => {
    expect(service).toBeTruthy();
  }));
});
