import { Component, OnInit ,ChangeDetectorRef } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn, NgForm } from '@angular/forms';
import { XlsxToJsonService } from '../xlsx-to-json-service';
import { ExcelServiceService } from '../shared/services/excel-service.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { excelimport } from '../shared/model/excelimport.model';
import { MainURL } from '../shared/configurl';
import { ImportShiftServiceService, ExcelServiceShiftUpload } from './service/import-shift-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {dataService} from '../shared/services/dataService';
import { DatePipe, DOCUMENT } from '@angular/common';

declare var require: any;
@Component({
  selector: 'app-shiftupload',
  templateUrl: './shiftupload.component.html',
  styleUrls: ['./shiftupload.component.css']
})
export class ShiftuploadComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  baseurl = MainURL.HostUrl
  compId: any;

  orders = [
    { title: "Personal" },
    { id: "biometricId", name: "Biometric Id" }

  ];
  excelmodel: excelimport;
  model: any = {};
  finalArray = [];
  fileExtension: any;
  showFileName: any;  
  abbrivationsResDtoList: any;
  loggedUser: any;
  today:any
 
  constructor(private formBuilder: FormBuilder,public Spinner: NgxSpinnerService, private excelService: ExcelServiceService, public shiftService1: ExcelServiceShiftUpload, private toastr: ToastrService, public httpService: HttpClient, public chRef: ChangeDetectorRef, private dataService: dataService,private datepipe:DatePipe) {

    const controls = this.orders.map(c => new FormControl(false));
    this.form = this.formBuilder.group({
      orders: new FormArray(controls, minSelectedCheckboxes(1))
    });
    this.excelmodel = new excelimport();
    this.model.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + 1;
    this.shoButtonflag = false;
    this.hidebutton = false;
    this.hideShiftTab = true;
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.today=new Date();
   
  }
  dateArr: any;
  dateArr1: any = [];
  ngOnInit() {
  
  }

  selectedFile: File;
  shitDataList: any = [];
  fileName: any;
  arrayList1: any = [];
  fromDate: any;
  toDate: any;
  shoButtonflag :  boolean =false;
  hidebutton :  boolean = false;
  
  onFileChanged(event) {
    this.showFileName =event.target.value.replace(/^.*[\\\/]/, '');
    this.fileExtension = event.target.value.replace(/^.*\./, '');
    if(this.fileExtension == 'pdf'){
      this.hidebutton = true;
    }
    else{
      this.hidebutton = false;
    }
    this.selectedFile = event.target.files[0];
    this.fileName = this.selectedFile.name;
    this.shoButtonflag = true
    var fields = this.fileName.split('_');
    this.fromDate = fields[0];
    var demoToDate = fields[1];
    var fields1 = demoToDate.split('.');
    this.toDate = fields1[0];
  
  }
  list: any = [];
  srNo: any;
  row1: any;
  onChnageFun() {
    this.list = this.form.value.orders
      .map((v, i) => v ? this.orders[i] : null)
      .filter(v => v !== null);
  }

getAbbrivations() {
  // this.compId=1;
    let url = this.baseurl + '/abbrivations/';
    this.httpService.get(url + this.compId).subscribe(data => {
      this.abbrivationsResDtoList = data["result"];
    },
    (err: HttpErrorResponse) => {
    });
};

  alldatademployeelist() {
    // this.compId = 1;
    let url = this.baseurl + '/EmployeeMasterOperation/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.shitDataList = data["result"];
      },
      (err: HttpErrorResponse) => {
      });
  }
  maxDate : any;
  mindDate : any;
  startDateSelect(e) {
  

this.model.todayDt=e;
    this.model.fromDate = e;
 
    let date = new Date(e).getTime() + 30 * 24 * 60 * 60 * 1000;
    this.maxDate =  new Date(new Date(e).getFullYear(),new Date(e).getMonth()+1,0);
    let date1 = new Date(new Date(e).getFullYear(),new Date(e).getMonth()+1,0);
    
    this.model.toDate = new Date(date1).getFullYear()+"-"+(new Date(date1).getMonth()+1)+"-"+new Date(date1).getDate();

  }
  endDateSelect(e) {
    this.model.toDate = e;
   
  }
  daysData = [];
  hideShiftTab :  boolean;
  submit() {
    this.Spinner.show();
    this.hideShiftTab = false;
    this.dateArr = {};
    this.dateArr1 = [];
    this.getAbbrivations();
    let AttendenceConfiList =  '/getShiftData';
    let obj = {
      "empId": this.loggedUser.empId,
      "locationId":this.loggedUser.locationId,
      "roleId": this.loggedUser.rollMasterId,
      "compId": this.compId,
      "fromDate": this.model.fromDate,
      "toDate": this.model.toDate
    }
    this.dataService.createRecord(AttendenceConfiList , obj)
    // this.httpService.post(AttendenceConfiList, obj)
    .subscribe((data: any) => {
        this.shitDataList = data.result.dataList;
        this.daysData=data.result.headerList;
        let tempData1 = new Array();
        let tempObj1 : any = {};
    for (let i = 0; i < this.shitDataList.length; i++) {
      tempObj1.srNo = i+1;
      tempObj1.empId = this.shitDataList[i].empId;
      tempObj1.empOfficialId = this.shitDataList[i].empOfficialId;
      if (new Date(this.model.fromDate).getDate() <= 1 && new Date(this.model.toDate).getDate() >= 1 ) {
        tempObj1.one = this.shitDataList[i].one;
      }
      if (new Date(this.model.fromDate).getDate() <= 2 && new Date(this.model.toDate).getDate() >= 2 ) {
        tempObj1.two = this.shitDataList[i].two;
      }
      if (new Date(this.model.fromDate).getDate() <= 3 && new Date(this.model.toDate).getDate() >= 3) {
        tempObj1.three = this.shitDataList[i].three;
      }
      if (new Date(this.model.fromDate).getDate() <= 4 && new Date(this.model.toDate).getDate() >= 4) {
        tempObj1.four = this.shitDataList[i].four;
      }
      if (new Date(this.model.fromDate).getDate() <= 5 && new Date(this.model.toDate).getDate() >= 5) {
        tempObj1.five = this.shitDataList[i].five;
      } 
      if (new Date(this.model.fromDate).getDate() <= 6 && new Date(this.model.toDate).getDate() >= 6) {
        tempObj1.six = this.shitDataList[i].six;
      }
      if (new Date(this.model.fromDate).getDate() <= 7 && new Date(this.model.toDate).getDate() >= 7) {
        tempObj1.seven = this.shitDataList[i].seven;
      }
      if (new Date(this.model.fromDate).getDate() <= 8 && new Date(this.model.toDate).getDate() >= 8) {
        tempObj1.eight = this.shitDataList[i].eight;
      }
      if (new Date(this.model.fromDate).getDate() <= 9 && new Date(this.model.toDate).getDate() >= 9) {
        tempObj1.nine = this.shitDataList[i].nine;
      }
      if (new Date(this.model.fromDate).getDate() <= 10 && new Date(this.model.toDate).getDate() >= 10) {
        tempObj1.ten = this.shitDataList[i].ten;
      }
      if (new Date(this.model.fromDate).getDate() <= 11 && new Date(this.model.toDate).getDate() >= 11) {
        tempObj1.eleven = this.shitDataList[i].eleven;
      }
      if (new Date(this.model.fromDate).getDate() <= 12 && new Date(this.model.toDate).getDate() >= 12) {
        tempObj1.twelve = this.shitDataList[i].twelve;
      }
      if (new Date(this.model.fromDate).getDate() <= 13 && new Date(this.model.toDate).getDate() >= 13) {
        tempObj1.thirteen = this.shitDataList[i].thirteen;
      }
      if (new Date(this.model.fromDate).getDate() <= 14 && new Date(this.model.toDate).getDate() >= 14) {
        tempObj1.fourteen = this.shitDataList[i].fourteen;
      }
      if (new Date(this.model.fromDate).getDate() <= 15 && new Date(this.model.toDate).getDate() >= 15) {
        tempObj1.fifteen = this.shitDataList[i].fifteen;
      }
      if (new Date(this.model.fromDate).getDate() <= 16 && new Date(this.model.toDate).getDate() >= 16) {
        tempObj1.sixteen = this.shitDataList[i].sixteen;
      }
      if (new Date(this.model.fromDate).getDate() <= 17 && new Date(this.model.toDate).getDate() >= 17) {
        tempObj1.seventeen = this.shitDataList[i].seventeen;
      }
      if (new Date(this.model.fromDate).getDate() <= 18 && new Date(this.model.toDate).getDate() >= 18) {
        tempObj1.eighteen = this.shitDataList[i].eighteen;
      }
      if (new Date(this.model.fromDate).getDate() <= 19 && new Date(this.model.toDate).getDate() >= 19) {
        tempObj1.nineteen = this.shitDataList[i].nineteen;
      }
      if (new Date(this.model.fromDate).getDate() <= 20 && new Date(this.model.toDate).getDate() >= 20) {
        tempObj1.twenty = this.shitDataList[i].twenty;
      }
      if (new Date(this.model.fromDate).getDate() <= 21 && new Date(this.model.toDate).getDate() >= 21) {
        tempObj1.twentyone = this.shitDataList[i].twentyone;
      }
      if (new Date(this.model.fromDate).getDate() <= 22 && new Date(this.model.toDate).getDate() >= 22) {
        tempObj1.twentytwo = this.shitDataList[i].twentytwo;
      }
      if (new Date(this.model.fromDate).getDate() <= 23 && new Date(this.model.toDate).getDate() >= 23) {
        tempObj1.twentythree = this.shitDataList[i].twentythree;
      }
      if (new Date(this.model.fromDate).getDate() <= 24 && new Date(this.model.toDate).getDate() >= 24) {
        tempObj1.twentyfour = this.shitDataList[i].twentyfour;
      }
      if (new Date(this.model.fromDate).getDate() <= 25 && new Date(this.model.toDate).getDate() >= 25) {
        tempObj1.twentyfive = this.shitDataList[i].twentyfive;
      }
      if (new Date(this.model.fromDate).getDate() <= 26 && new Date(this.model.toDate).getDate() >= 26) {
        tempObj1.twentysix = this.shitDataList[i].twentysix;
      }
      if (new Date(this.model.fromDate).getDate() <= 27 && new Date(this.model.toDate).getDate() >= 27) {
        tempObj1.twentyseven = this.shitDataList[i].twentyseven;
      }
      if (new Date(this.model.fromDate).getDate() <= 28 && new Date(this.model.toDate).getDate() >= 28) {
        tempObj1.twentyeight = this.shitDataList[i].twentyeight;
      }
      if (new Date(this.model.fromDate).getDate() <= 29 && new Date(this.model.toDate).getDate() >= 29) {
        tempObj1.twentynine = this.shitDataList[i].twentynine;
      }
      if (new Date(this.model.fromDate).getDate() <= 30 && new Date(this.model.toDate).getDate() >= 30) {
        tempObj1.thirty = this.shitDataList[i].thirty;
      }
      if (new Date(this.model.fromDate).getDate() <= 31 && new Date(this.model.toDate).getDate() >= 31) {
        tempObj1.thirtyone = this.shitDataList[i].thirtyone;
      }
      tempData1.push(tempObj1);
      tempObj1 = {};
    }
    if (this.shitDataList.length != 0) {
        this.shiftService1.exportAsExcelFile(tempData1, this.model.fromDate + "_" + this.model.toDate + "_" + "Shift Upload" + ".xlsx");
   }
    this.hideShiftTab = true;
    this.Spinner.hide();
    },
    (err: HttpErrorResponse) => {
      this.Spinner.hide();
    });
    const numWords = require('num-words');
    this.dateArr = this.getDateArray(new Date(this.model.fromDate), new Date(this.model.toDate));
    this.dateArr1.push("srNo");
    this.dateArr1.push("empId");
    this.dateArr1.push("empOfficialId");
  
    for (let index = 0; index < this.dateArr.length; index++) {
      const element = new Date(this.dateArr[index]);
      this.dateArr1.push(numWords(element.getDate()).replace(/ /g, "").toLowerCase());
    }
    const selectedOrderIds = this.dateArr1;
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('data');
    //Add Header Row
    let headerRow = worksheet.addRow(selectedOrderIds);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });
  }
 getDaysArray(start, end) {
    for(var arr=[],dt=start; dt<=end; dt.setDate(dt.getDate()+1)){
        arr.push(new Date(dt));
    }
    return arr;
};
  public result: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  shiftErrorMessage:any;
  handleFile() {
    this.Spinner.show();
    let file = this.selectedFile;
    this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
      this.excelmodel.shiftUploadReqDtoList = (data['sheets'].data);
      this.excelmodel.fromDate = this.fromDate;
      this.excelmodel.toDate = this.toDate;
      this.excelmodel.compId = this.compId;
      if (this.excelmodel.shiftUploadReqDtoList != null) {
        this.excelService.postShiftUploadExcel(this.excelmodel).subscribe(data => {
            this.Spinner.hide();
            this.toastr.success('Shifts uploded successfully');
            $("#File1").val('');
        this.shoButtonflag = false;
          },err=>{
            this.Spinner.hide();
            this.shiftErrorMessage = err.json().result.shiftValidateMessage;
            this.toastr.error(this.shiftErrorMessage);
            this.shoButtonflag = false;
          })
      }
      else {
        this.Spinner.hide();
        this.toastr.error('Shifts uploded Faild');
        this.shoButtonflag = false;
      }
    })
  }
  list1 = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
  changeStatus(obj) {
    if (this.list1[obj]) {
      this.list1[obj] = false;
    }
    else {
      this.list1[obj] = true;
    }
  }
  getDateArray = function (start, end) {
    var arr = new Array();
    var dt = new Date(start);
    while (dt <= end) {
      arr.push(new Date(dt));
      dt.setDate(dt.getDate() + 1);
    }
    return arr;
  }
  hasProp(o, name) {
    return o.hasOwnProperty(name);
  }
}
function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}