import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { ShiftuploadComponent } from 'src/app/shiftupload/shiftupload.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ShiftuploadComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'shiftupload',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'shiftupload',
                    component: ShiftuploadComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);