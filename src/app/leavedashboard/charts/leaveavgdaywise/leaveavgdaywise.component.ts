import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { coffDetails } from '../../leaveDashModal/coffDetails';
import { LeavedashboardService } from '../../service/leavedashboard.service';
@Component({
  selector: 'app-leaveavgdaywise',
  templateUrl: './leaveavgdaywise.component.html',
  styleUrls: ['./leaveavgdaywise.component.css']
})
export class LeaveavgdaywiseComponent implements OnInit {
  compId: any;
  empOfficialId: any;
  empPerId: any;
  loggedUser: any;
coffDetails : coffDetails
  wen: any;
  tue: any;
  thu: any;
  sun: any;
  sat: any;
  fri: any;
  mon: any;
  newDate: Date;
  currentY: number;
  constructor(public leaveservice:LeavedashboardService) { 
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId
    this.empPerId = this.loggedUser.empPerId
    this.coffDetails = new coffDetails();
    this.currentY = new Date().getFullYear();  
    let a = this.currentY  + "-01" + "-01";
    this.coffDetails.startDate = new Date(a)
    let b = this.currentY  + "-12" + "-31";
    this.coffDetails.endDate = new Date(b)
    this.weeklyPattrn(this.empPerId)
  }

  ngOnInit() {
  }

  weeklyPattrn(e){
    this.coffDetails.empPerId = e
    this.leaveservice.postWeeeklyPattern(this.coffDetails).subscribe(data => {
      this.mon = data['result'].mon
      this.fri = data['result'].fri
      this.sat = data['result'].sat
      this.sun = data['result'].sun
      this.thu = data['result'].thu
      this.tue = data['result'].tue
      this.wen = data['result'].wen
      this.barChartData =[{data:[this.mon,this.tue,this.wen,this.thu,this.fri,this.sat,this.sun]}]
    },(err: HttpErrorResponse) => {
        
    });
  }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartData: any[] = [
    { data: [], label: 'Leaves' },
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#31b4ff",
      borderColor: 'black',
      pointBackgroundColor: 'rgb(67, 111, 253)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(67, 111, 253)'
    }
  ]
 fromDashboard(e){
    this.coffDetails.empPerId = this.empPerId;
    let date = e + "-01" + "-01";
    this.newDate = new Date(date);
    this.coffDetails.startDate = this.newDate;
    let endDate = e + "-12" + "-31";
    let newEndDate = new Date(endDate);
    this.coffDetails.endDate = newEndDate;
    this.weeklyPattrn(this.empPerId);
  }
  fromDashboardemp(e){
    this.coffDetails.empPerId  = e
    this.weeklyPattrn(e);
  }
  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}
