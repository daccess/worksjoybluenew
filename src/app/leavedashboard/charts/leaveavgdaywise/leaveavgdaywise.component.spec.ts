import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveavgdaywiseComponent } from './leaveavgdaywise.component';

describe('LeaveavgdaywiseComponent', () => {
  let component: LeaveavgdaywiseComponent;
  let fixture: ComponentFixture<LeaveavgdaywiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveavgdaywiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveavgdaywiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
