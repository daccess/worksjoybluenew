import { Component, OnInit } from '@angular/core';
import { LeavedashboardService } from '../../service/leavedashboard.service';
import { coffDetails } from '../../leaveDashModal/coffDetails';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-leaveavgmonthwise',
  templateUrl: './leaveavgmonthwise.component.html',
  styleUrls: ['./leaveavgmonthwise.component.css']
})
export class LeaveavgmonthwiseComponent implements OnInit {

  compId: any;
  empOfficialId: any;
  empPerId: any;
  loggedUser: any;
  coffDetails:coffDetails;
  jun: any;
  feb: any;
  jan: any;
  apr: any;
  mar: any;
  jul: any;
  may: any;
  aug: any;
  oct: any;
  nov: any;
  dec: any;
  sep: any;
  newDate: Date;
  currentY: number;
  role: any;

  constructor(public leaveservice:LeavedashboardService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId
    this.empPerId = this.loggedUser.empPerId
      
    this.currentY = new Date().getFullYear();  
this.role = this.loggedUser.mrollMaster.roleName
    
    this.coffDetails = new coffDetails();
    let a =  this.currentY + "-01" + "-01";
    this.coffDetails.startDate = new Date(a)
  
    let b =  this.currentY + "-12" + "-31";
    this.coffDetails.endDate = new Date(b)
  
    this.monthlyPattrn(this.empPerId)
   }

  ngOnInit() {
  }
monthlyPattrn(e){
  this.coffDetails.empPerId = e;
 
this.leaveservice.monthlyPatternDataByCurrentYearService(this.coffDetails).subscribe(data => {
  this.jan=data['result'].jan ;
  this.feb=data['result'].feb;
  this.mar=data['result'].mar;
  this.apr=data['result'].apr;
  this.may=data['result'].may;
  this.jun=data['result'].jun;
  this.jul=data['result'].jul;
  this.aug=data['result'].aug;
  this.sep=data['result'].sep;
  this.oct=data['result'].oct;
  this.nov=data['result'].nov;
  this.dec=data['result'].dec;
  this.barChartData=[{data:[this.jan,this.feb,this.mar,this.apr,this.may,this.jun,this.jul,this.aug,this.sep,this.oct,this.nov,this.dec]}]
},
(err: HttpErrorResponse) => {
});

}
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [], label: 'Leaves' },
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#31b4ff",
      borderColor: 'black',
      pointBackgroundColor: 'rgb(67, 111, 253)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(67, 111, 253)'
    }
  ]
  fromDashboard(e){
    this.coffDetails.empPerId = this.empPerId;
    let date = e + "-01" + "-01";
    this.newDate = new Date(date);
    this.coffDetails.startDate = this.newDate;
    let endDate = e + "-12" + "-31"
    let newEndDate = new Date(endDate);
    this.coffDetails.endDate = newEndDate;
    this.monthlyPattrn(this.empPerId);
  }
  fromDashboardemp(e){
    this.coffDetails.empPerId = e
    this.monthlyPattrn(e);
  }
  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
}

