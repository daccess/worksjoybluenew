import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveavgmonthwiseComponent } from './leaveavgmonthwise.component';

describe('LeaveavgmonthwiseComponent', () => {
  let component: LeaveavgmonthwiseComponent;
  let fixture: ComponentFixture<LeaveavgmonthwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveavgmonthwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveavgmonthwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
