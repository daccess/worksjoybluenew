import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumedleaveschartComponent } from './consumedleaveschart.component';

describe('ConsumedleaveschartComponent', () => {
  let component: ConsumedleaveschartComponent;
  let fixture: ComponentFixture<ConsumedleaveschartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumedleaveschartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumedleaveschartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
