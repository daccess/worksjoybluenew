import { Component, OnInit } from '@angular/core';
import { LeavedashboardService } from '../../service/leavedashboard.service';
import {CounsumeLeaveType} from '../../leaveDashModal/counsumeType';
import { HttpErrorResponse } from '@angular/common/http';
import { coffDetails } from '../../leaveDashModal/coffDetails';
@Component({
  selector: 'app-consumedleaveschart',
  templateUrl: './consumedleaveschart.component.html',
  styleUrls: ['./consumedleaveschart.component.css']
})
export class ConsumedleaveschartComponent implements OnInit {
  compId: any;
  empOfficialId: any;
  empPerId: any;
  loggedUser: any;
  coffDetails : coffDetails;
  consumeLeaveData: any;
  doughnutChartLabelsDemo = []
  doughnutChartDataDemo = []
  newDate: Date;
  selectedYear: Event;
  currentY: number;
  constructor(public leaveService:LeavedashboardService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId
    this.empPerId = this.loggedUser.empPerId
    this.coffDetails = new coffDetails()
    this.currentY = new Date().getFullYear();  
    let a = this.currentY  + "-01" + "-01";
    this.coffDetails.startDate = new Date(a)
    let b = this.currentY  + "-12" + "-31";
    this.coffDetails.endDate = new Date(b)
    this.getCounsumeLeaveTypeData(this.empPerId);
   }
  ngOnInit() {
  }
getCounsumeLeaveTypeData(e){

    this.doughnutChartLabels = []
    this.coffDetails.empPerId = e
    this.leaveService.counsumeLeaveType(this.coffDetails).subscribe(data =>{
    this.consumeLeaveData = data['result'];
    for (let index = 0; index < this.consumeLeaveData.length; index++) {
      let leaveCode = this.consumeLeaveData[index].leaveCode;
      let count = this.consumeLeaveData[index].count
      this.doughnutChartLabels.push(leaveCode);
      this.doughnutChartDataDemo.push(count)
      this.doughnutChartData = this.doughnutChartDataDemo
      console.log("dataconsumed",    this.doughnutChartData);
    }
    this.doughnutChartDataDemo = []
    this.doughnutChartLabels = []
  },
  (err: HttpErrorResponse) => {
    this.doughnutChartData = [0]
    this.doughnutChartLabels = []
  }); 
} 
  title = 'doughnutChart';
  public doughnutChartLabels:string[] = [];
  public doughnutChartData:number[] = [12];
  public doughnutChartType:string = 'doughnut';
  public backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#c772a4"];
  public chartClicked(e:any):void {
  }
 
  public chartHovered(e:any):void {
  }

  fromDashboard(e){
    this.selectedYear = event
    this.coffDetails.empPerId = this.empPerId;
    let date = e + "-01" + "-01"
    this.newDate = new Date(date);
    this.coffDetails.startDate = this.newDate;
    let endDate = e + "-12" + "-31"
    let newEndDate = new Date(endDate)
    this.coffDetails.endDate = newEndDate
    this.doughnutChartDataDemo = []
    this.getCounsumeLeaveTypeData(this.empPerId)
  }
  fromDashboardemp(e){
    this.coffDetails.empPerId = e
    this.getCounsumeLeaveTypeData(e)
  }
}
