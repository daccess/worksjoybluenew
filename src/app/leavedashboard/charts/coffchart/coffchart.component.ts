import { Component, OnInit } from '@angular/core';
import { LeavedashboardService } from '../../service/leavedashboard.service';
import { coffDetails } from '../../leaveDashModal/coffDetails';

@Component({
  selector: 'app-coffchart',
  templateUrl: './coffchart.component.html',
  styleUrls: ['./coffchart.component.css']
})
export class CoffchartComponent implements OnInit {
  coffdetails:coffDetails;
  compId: any;
  empOfficialId: any;
  empPerId: any;
  loggedUser: any;
  colapsed: any;
  total: any;
  hello: any;
  newDate: Date;
  currentY: number;
  constructor(public leaveService:LeavedashboardService) { 
    this.coffdetails=new coffDetails();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    if(this.empPerId == null){
    this.empPerId = this.loggedUser.empPerId
    }
    this.currentY = new Date().getFullYear();  
    let a =  this.currentY + "-01" + "-01";
    this.coffdetails.startDate = new Date(a)

    let b =  this.currentY + "-12" + "-31";
    this.coffdetails.endDate = new Date(b) 
    this.coffdetails.empPerId=this.empPerId;
    this.coffDetailsChart(this.empPerId);
  }

  ngOnInit() {
    
  }
  coffDetailsChart(e){
    this.coffdetails.empPerId= e
    this.leaveService.coffColapsedChartDetailsService(this.coffdetails).subscribe(data=>{
       this.colapsed=data['result'].colapsed;
       this.total=data['result'].total;
       this.pieChartData=[this.colapsed,this.total]
    })
  }
  fromDashboard(e){
    this.coffdetails.empPerId = this.empPerId;
    let date = e + "-01" + "-01"
    this.newDate = new Date(date);
    this.coffdetails.startDate = this.newDate;
    let endDate = e + "-12" + "-31"
    let newEndDate = new Date(endDate)
    this.coffdetails.endDate = newEndDate
    this.coffDetailsChart(this.empPerId);
  }
  fromDashboardemp(e){
    this.coffdetails.empPerId = e
    this.coffDetailsChart(e);
  }
  title = 'pieChart';
  public pieChartLabels:string[] = ['Lapsed', 'Total'];
  public pieChartData:number[] = [0,0];
  public pieChartType:string = 'pie';
  public backgroundColor: ["#3e95cd", "#8e5ea2"];
  public chartClicked(e:any):void {
  }
 
  public chartHovered(e:any):void {
  }
}
