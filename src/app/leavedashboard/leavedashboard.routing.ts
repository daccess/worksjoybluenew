import { Routes, RouterModule } from '@angular/router'

import { LeavedashboardComponent } from './leavedashboard.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: LeavedashboardComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leavedashboard',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leavedashboard',
                    component: LeavedashboardComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);