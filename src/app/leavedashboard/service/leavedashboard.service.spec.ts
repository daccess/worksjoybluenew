import { TestBed, inject } from '@angular/core/testing';

import { LeavedashboardService } from './leavedashboard.service';

describe('LeavedashboardServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeavedashboardService]
    });
  });

  it('should be created', inject([LeavedashboardService], (service: LeavedashboardService) => {
    expect(service).toBeTruthy();
  }));
});
