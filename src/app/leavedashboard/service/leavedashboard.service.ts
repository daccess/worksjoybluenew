import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { MainURL } from 'src/app/shared/configurl';

import {WeeklyPattern} from '../leaveDashModal/weeklypattern'
import {CounsumeLeaveType} from '../leaveDashModal/counsumeType'
import { coffDetails } from '../leaveDashModal/coffDetails';

@Injectable({
  providedIn: 'root'
})
export class LeavedashboardService {
  baseurl = MainURL.HostUrl;
  constructor(private http : Http) { }

  postWeeeklyPattern(coffDetails : coffDetails){

    let url = this.baseurl + '/weeklyPatternData';
   var body = JSON.stringify(coffDetails);
    var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  monthlyPatternDataByCurrentYearService(coffDetails : coffDetails){
    let url = this.baseurl + '/monthlyPatternDataByCurrentYear';
   var body = JSON.stringify(coffDetails);
    var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  coffColapsedChartDetailsService(coffDetails : coffDetails){
    let url = this.baseurl + '/coffColapsedChartDetails';
   var body = JSON.stringify(coffDetails);
    var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  counsumeLeaveType(coffDetails : coffDetails){
    let url = this.baseurl + '/donateLeaveChartByCurrentYear';
   var body = JSON.stringify(coffDetails);
    var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  currentLeaveReqStatus(counsumeLeaveType : CounsumeLeaveType){
    let url = this.baseurl + '/twoMonthsBeforeLeaveStatus';
   var body = JSON.stringify(counsumeLeaveType);
    var headerOptions = new Headers({'Content-Type':'application/json'});
     var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
}
