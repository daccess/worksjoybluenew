import { routing } from './leavedashboard.routing';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LeavedashboardComponent } from './leavedashboard.component';
import { ChartsModule } from 'ng2-charts';
import { LeaveavgmonthwiseComponent } from './charts/leaveavgmonthwise/leaveavgmonthwise.component';
import { ConsumedleaveschartComponent } from './charts/consumedleaveschart/consumedleaveschart.component';
import { CoffchartComponent } from './charts/coffchart/coffchart.component';
import { LeavedashboardService } from './service/leavedashboard.service';
import { LeaveavgdaywiseComponent } from './charts/leaveavgdaywise/leaveavgdaywise.component';


@NgModule({
    declarations: [
        LeavedashboardComponent,
        ConsumedleaveschartComponent,
        CoffchartComponent,
        LeaveavgmonthwiseComponent,
        LeaveavgdaywiseComponent
      
    ],
    imports: [
    routing,
    ChartsModule,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
     DataTablesModule,
     NgMultiSelectDropDownModule
    ],
    providers: [LeavedashboardService]
  })
  export class LeavedashboardModule { 
      constructor(){

      }
  }
  