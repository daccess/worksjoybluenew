import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavedashboardComponent } from './leavedashboard.component';

describe('LeavedashboardComponent', () => {
  let component: LeavedashboardComponent;
  let fixture: ComponentFixture<LeavedashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavedashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavedashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
