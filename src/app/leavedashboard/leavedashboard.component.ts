import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { LeavedashboardService } from './service/leavedashboard.service';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import {WeeklyPattern} from './leaveDashModal/weeklypattern'
import { MainURL } from '../shared/configurl';
import {LeavesHistory} from './leaveDashModal/leavesHistory'
import { allocStylingContext } from '@angular/core/src/render3/styling';
import { coffDetails } from './leaveDashModal/coffDetails';
import { log } from 'util';
@Component({
  selector: 'app-leavedashboard',
  templateUrl: './leavedashboard.component.html',
  styleUrls: ['./leavedashboard.component.css']
})
export class LeavedashboardComponent implements OnInit {
  compId: any;
  empId:any;
  baseurl = MainURL.HostUrl
  empOfficialId: any;
  empPerId: any;
  loggedUser: any;
  weeklyPattern : WeeklyPattern
  coffDetails : coffDetails
  wen: any;
  tue: any;
  thu: any;
  sun: any;
  sat: any;
  fri: any;
  mon: any;
  currentLeaveData: any;
  LeavesData: any;
  lenght: any;
  length: any;
  leaveHistortModal :  LeavesHistory;
  leaveHistorydata : any;
  LeaveHistory = [];
  Startval: any;
  Endval: any;
  newDate: Date;
  pssvalue = [];
  yearValue = 2020
  flag
  selectedYear: any;
  myteamData: any;
  selectedCompanyobj : any
  selectUndefinedOptionValue : any
  currentY: number;
  role: any;
  constructor(public leaveservice:LeavedashboardService , public httpService: HttpClient,) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empId=this.loggedUser.empId
    this.empOfficialId = this.loggedUser.empOfficialId;
    this.empPerId = this.loggedUser.empPerId;
    this.role =  this.loggedUser.mrollMaster.roleName;
    this.weeklyPattern = new WeeklyPattern();
    this.leaveHistortModal = new LeavesHistory();
    this.coffDetails = new coffDetails();
    this.length = 0;
    this.currentLeaveReq();
    this.Leaves();
    this.getTeam();
    this.currentY = new Date().getFullYear();
    this.selectedYear = this.currentY;
    this.LeaveHistory = [{ status:'All' , Name : 'All' },
    { status:'Approved' , Name : 'Approved' },
    { status:'Pending' , Name : 'Pending' },
    { status:'Rejected' , Name : 'Rejected' }]
    this.leaveHistortModal.status = 'All';
    this.leavesHistory();
  }
  hrMode : any;
  User:any;
  ngOnInit() {
    this.hrMode = sessionStorage.getItem('hrMode');
let User = sessionStorage.getItem('loggedUser');
this.loggedUser = JSON.parse(User);
this.User = this.loggedUser.mrollMaster.roleName == 'User';
  }
currentLeaveReq(){
    this.weeklyPattern.empPerId = this.empPerId
    this.leaveservice.currentLeaveReqStatus(this.weeklyPattern).subscribe(data=>{    
    this.currentLeaveData = data['result'].sort((b, a) => new Date(b.startDate).getTime() - new Date(a.startDate).getTime());
    this.length = this.currentLeaveData.length
  },(err : HttpErrorResponse )=>{

  })
}
getTeam() {

  let url = this.baseurl + '/employeelistunderempid/';
  this.httpService.get(url + this.loggedUser.empId).subscribe(data  => {
    this.myteamData = data['result']
  },
  (err: HttpErrorResponse) => {
  });
  if(this.loggedUser.roleHead == "Admin"){
    let url = this.baseurl + '/EmployeeMasterOperationAdmin/';
    this.httpService.get(url + this.loggedUser.compId).subscribe(data => {
      this.myteamData = data['result'];
    },
    (err: HttpErrorResponse) => {
    });
  }
}
value(event){
  this.Startval = event.target.value; 
}
value1(e){
  this.Endval = e.target.value;
}
Leaves() {
  let url = this.baseurl + '/EmpLeavesBalanceDetails/';
  this.httpService.get(url + this.empPerId).subscribe(data => {
    this.LeavesData = data['result']['list'];
  },
  (err: HttpErrorResponse) => {
  });
}
selectedEmp(event){
  this.empPerId = event;
  this.coffDetails.empPerId = this.empPerId;
  sessionStorage.setItem('empPerIdByDrop',this.empPerId)
  this.Leaves();
  this.leavesHistory();
  this.currentLeaveReq();
}

valueYear(e){
  this.selectedYear = e
  this.coffDetails.empPerId = this.empPerId;
  let date = e + "-01" + "-01";
  this.newDate = new Date(date);
  this.coffDetails.startDate = this.newDate;
  let endDate = e + "-12" + "-31";
  let newEndDate = new Date(endDate);
  this.coffDetails.endDate = newEndDate;
}

dataTableFlag : boolean = true;
leavesHistory(){
  this.leaveHistortModal.empPerId = this.empPerId;
  let url = this.baseurl + '/leaveDashboardByFilter';
  this.dataTableFlag = false;
  this.httpService.post(url , this.leaveHistortModal).subscribe(data => {
      this.leaveHistorydata = data['result']
      this.dataTableFlag = true;
      setTimeout(function () {
        $(function () {
          var table = $('#leaveDash').DataTable();             
        });
      }, 100);
    },
    (err: HttpErrorResponse) => {
     this.leaveHistorydata = []
    });
  }
}
