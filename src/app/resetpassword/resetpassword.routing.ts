import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { AnnouncementComponent } from 'src/app/announcement/announcement.component';
import { ResetpasswordComponent } from 'src/app/resetpassword/resetpassword.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: ResetpasswordComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'resetpassword',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'resetpassword',
                    component: ResetpasswordComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);