import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { MainURL } from '../shared/configurl';

import { ResetPassService } from '../shared/services/ResetPassService';
import { ResetPass } from '../shared/model/ResetPassmodel';
import { from } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  ResetPass: ResetPass
  AllDepart: any;
  baseurl = MainURL.HostUrl;
  compId: string;
  selectfunctionobj: number;
  Alldep: any;
  Allemp: any;
  searchQuery: string = "";
  empFlag : boolean = false;
  userData: any;
  userid: any;
  loggedUser: any;
  empOfficialId: any;
  empPerId: any;
  selectUndefinedOptionValue: any;
  constructor(public httpService: HttpClient,
    public ResetPassServic: ResetPassService,
    public chRef: ChangeDetectorRef,
    private toastr: ToastrService) {
    this.ResetPass = new ResetPass();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId =  this.loggedUser.compId
    this.empOfficialId = this.loggedUser.empOfficialId 
    this.empPerId = this.loggedUser.empPerId
  }

  ngOnInit() {
    this.getAllDep();

  }
  filterItem(value) {
    if(!value){
      this.empFlag = false;
      // this.questionData = [];
    }
  if(value){
    // this.questionData = temp;
  this.empFlag = true;
  }
  }
  suggestThis(item){
    this.searchQuery = item.fullName;
    this.empFlag = false;
    this.ResetPass.empPerId=item.empPerId;
    this.getuser();
  }
  ngValueDepa(event) {
    this.selectfunctionobj = parseInt(event.target.value)
    this.getEmpDetail();
  }

  getAllDep() {
    let busiurl = this.baseurl + '/Department/';
    this.httpService.get(busiurl + this.compId).subscribe(data => {
        this.AllDepart = data["result"];
        this.ResetPass.deptId = data['result']['deptId']

      },
      (err: HttpErrorResponse) => {
      });
  }
  getEmpDetail() {
    let busiurl = this.baseurl + '/getEmployeeList/';
    this.httpService.get(busiurl + this.selectfunctionobj).subscribe(data => {
        this.Allemp = data["result"];
      },
      (err: HttpErrorResponse) => {
      }
    );
  }
  getuser(){
    let busiurl = this.baseurl + '/getEmployeeDetail/';
    this.httpService.get(busiurl + this.ResetPass.empPerId).subscribe(data => {
      this.userData = data["result"];
      this.userid=data['result']['userId']
      this.ResetPass.userId=this.userid
      },
      (err: HttpErrorResponse) => {
      });

  }
  transform(value: any, args?: any): any {

    if (!value) return null;
    if (!args) return value;

    args = args.toLowerCase();

    return value.filter(function (item) {
      return JSON.stringify(item).toLowerCase().includes(args);
    });
  }
putResetPass(){
  let urld = this.baseurl + '/ResetPassword';
  this.ResetPass.userId=this.userid;
  this.ResetPass.empPerId = this.selectfunctionobj;
  //this.deptmodel.functionUnitId = this.selectfunctionobj;
  this.httpService.put(urld,this.ResetPass)
  .subscribe(data => {
    this.toastr.warning('Please Check Your Mail');
   
  },
  (err: HttpErrorResponse) => {
    this.toastr.error('Server Side Error.');
  });
}

  arrowkeyLocation = 0;

  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }
}

