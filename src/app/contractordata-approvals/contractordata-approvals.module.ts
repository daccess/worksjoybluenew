import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { newrouting } from './contractordata-approvals-routing.module';
import { ContractordataApprovalsComponent } from './contractordata-approvals.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  imports: [
    CommonModule,
    newrouting,
    FormsModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [
    ContractordataApprovalsComponent


  ]
})
export class ContractordataApprovalsModule { }
