import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractordataApprovalsComponent } from './contractordata-approvals.component';

describe('ContractordataApprovalsComponent', () => {
  let component: ContractordataApprovalsComponent;
  let fixture: ComponentFixture<ContractordataApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractordataApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractordataApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
