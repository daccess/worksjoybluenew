import { ContractordataApprovalsModule } from './contractordata-approvals.module';

describe('ContractordataApprovalsModule', () => {
  let contractordataApprovalsModule: ContractordataApprovalsModule;

  beforeEach(() => {
    contractordataApprovalsModule = new ContractordataApprovalsModule();
  });

  it('should create an instance', () => {
    expect(contractordataApprovalsModule).toBeTruthy();
  });
});
