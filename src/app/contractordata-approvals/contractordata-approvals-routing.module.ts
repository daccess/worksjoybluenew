import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  ContractordataApprovalsComponent } from './contractordata-approvals.component';

const appRoutes: Routes = [
  { 
           
      path: '', component:ContractordataApprovalsComponent ,

          children:[
              {
                  path:'',
                  redirectTo : 'contractordataApprovals',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'contractordataApprovals',
                  component: ContractordataApprovalsComponent
              }

          ]

  }

  // otherwise redirect to home

]

export const newrouting = RouterModule.forChild(appRoutes);


