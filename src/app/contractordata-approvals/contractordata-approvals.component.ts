import { Component, OnInit } from '@angular/core';
import { MainURL } from '../shared/configurl';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { MessagingService } from '../shared/services/messaging.service';
import * as moment from 'moment';

@Component({
  selector: 'app-contractordata-approvals',
  templateUrl: './contractordata-approvals.component.html',
  styleUrls: ['./contractordata-approvals.component.css']
})
export class ContractordataApprovalsComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  locationdata: any;
  locationId: any;
  manpowerData: any;
  count: any;
  approveChecks: boolean = false;
  approveChecks1: boolean = false
  checked: boolean = false;
  checked1: boolean = false;
  contractorFlag=false;
  serviceTypeFlag:boolean=false
  empids: any;
  showImage: any;
  manpowerAllData: any;
  count1: any;
  manpowerDatas: any;
  allmanpowerLabourData: any;
  isEditFlag: boolean;
  httpSer: any;
  contractorData: any;
  dropdownSettings:{}
  getDeleteContractorId: any;
  GetAllContractor:any=[];
  selectedContractorEmp:any=[];
  contractorServiceType: any;
  AllContractorData: any;
  SelectContractorTypeList: any;
  empManpowerReqId: any;
  manRequestDatas: any;
  manrequest: any;
  approverRemark: any;
  labourPerId: any;
  LabourRequestListData: any;
  LabourDatas: any;
  approverRemark1: any;
  conMasterIds: any;
  contractorApproveData: any;
  ContractorData: any;
  approverRemarkss: any;
  compIds: any;

  constructor(private httpservice: HttpClient, public Spinner: NgxSpinnerService, public toastr: ToastrService) {
    
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId
    this.locationId=this.users.roleList.locationId;
    
    this.compIds=this.users.roleList.compId;

    this.getlocationData();

  }

  ngOnInit() {
    this.getlocationData();
    this.getAllContractorList()

  this.dropdownSettings = {
    singleSelection: false,
    idField: "conMasterId",
    textField: "conCompanyName",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 100,
    allowSearchFilter: true,
  };

  }
  getlocationData() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.locationdata = data["result"];
      this.locationId = this.locationId
      this.getallContractorDataRequest();
      this.getAllManpowerForHR();
      this.getAllManpowerRequestApprovedByContractor()
    },
      (err: HttpErrorResponse) => {

      })

  }
  getallContractorDataRequest() {
    let url = this.baseurl + `/getContractorApproverList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.manpowerData = data["result"];

      // this.count1=this.manpowerAllData.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  getAllManpowerRequestApprovedByContractor() {
    let url = this.baseurl + `/GetLabourActiveLHList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.allmanpowerLabourData = data["result"];

      // this.count1=this.manpowerAllData.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  ApprovedManRequest(e:any){
    
    this.empManpowerReqId=e.empManpowerReqId
    let url = this.baseurl + `/getManpowerReqById/${this.empManpowerReqId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.manRequestDatas = data;
      this.manrequest=this.manRequestDatas.result;

      // this.count=this.manpowerDatas.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  ApproveContractorRequest(e:any){
    debugger
    this.conMasterIds=e.conMasterId
    let url = this.baseurl + `/getContractorMasterById?conMasterId=${this.conMasterIds}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.contractorApproveData = data;
      this.ContractorData=this.contractorApproveData.result;

      // this.count=this.manpowerDatas.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  ApprovedLabourRequest(e:any){
  
    this.labourPerId=e.requestStatus.labourInfo.labourPerId
    let url = this.baseurl + `/GetLabourActiveById/${this.labourPerId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.LabourRequestListData = data;
      this.LabourDatas=this.LabourRequestListData.result;

      // this.count=this.manpowerDatas.length

    },
      (err: HttpErrorResponse) => {
      });
  }
  
  getAllManpowerForHR() {
    let url = this.baseurl + `/getManpowerReqLHVerify/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {
      this.manpowerDatas = data["result"];

      // this.count=this.manpowerDatas.length

    },
      (err: HttpErrorResponse) => {
      });
  }

  checkAll() {
    if (!this.checked) {
      this.contractorchecked = false;
      for (let i = 0; i < this.manpowerData.length; i++) {
        this.manpowerData[i].check = true;
        this.approveChecks = true;
      }
    }

    else {
      for (let i = 0; i < this.manpowerData.length; i++) {
        this.manpowerData[i].check = false;
        this.approveChecks = false;
      }
    }

  }

  checkAll1() {
    if (!this.checked1) {
      this.contractorchecked1 = false;
      for (let i = 0; i < this.manpowerDatas.length; i++) {
        this.manpowerDatas[i].check = true;
        this.approveChecks1 = true;
      }
    }

    else {
      for (let i = 0; i < this.manpowerDatas.length; i++) {
        this.manpowerDatas[i].check = false;
        this.approveChecks1 = false;
      }
    }

  }
  contractorchecked: boolean = false;
  checks() {
    for (let i = 0; i < this.manpowerData.length; i++) {
      // this.leaveList[i].check = true;
      if (this.manpowerData[i].check) {
        this.contractorchecked = true;
        break;
      }
      else {
        this.contractorchecked = false;
      }
    }
  }
  approveContractorData(item) {
    let selectedObj3: any = new Array();
    let obj = {
      "requestId": item.conMasterId,
      "remark": this.approverRemarkss,
      "flag": "Approved",
      "compId":this.compIds,
      "approverId": this.empids
    }
    selectedObj3.push(obj)
    let obj2 = {
      "commonApprovedReqDtos": selectedObj3
    }

    this.Spinner.show();
    let url = this.baseurl + '/updateContraReqLHVerify';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj2, { headers }).subscribe(data => {

      //document.getElementById('LeaveCol').click();
      this.getallContractorDataRequest();
      
      this.Spinner.hide();
      setTimeout(() => {
        this.getallContractorDataRequest();
      }, 1000);

      this.toastr.success('Contraictor Requisition Approved Successfully');
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Approve Contractor Requisition');
    })
  }
  rejectContractorData(item) {
    let selectedObj1: any = new Array();
    let obj = {
      "requestId": item.conMasterId,
      "remark": this.approverRemarkss,
      "flag": "Rejected",
    "compId":this.compIds,
      "approverId": this.empids,
    }
    selectedObj1.push(obj)
    let obj2 = {
      "commonApprovedReqDtos": selectedObj1
    }
    this.Spinner.show();
    let url = this.baseurl + '/updateContraReqLHVerify';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj2, { headers }).subscribe(data => {
      this.Spinner.hide()
      setTimeout(() => {
        this.getallContractorDataRequest();
      }, 1000);

      this.toastr.success('Contractor Requisition Rejected Successfully');
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Reject Contractor Requisition');
    })
  }
  approveContractor() {
    let selectedObj: any = new Array();
    for (let i = 0; i < this.manpowerData.length; i++) {

      if (this.manpowerData[i].check) {

        let obj = {
          requestId: this.manpowerData[i].conMasterId,
          empId: this.manpowerData[i].empId,
          approverId: this.empids,
          "flag": "Approved",
          remork: "",
        }
        selectedObj.push(obj);
      }

    }
    let obj = {
      "commonApprovedReqDtos": selectedObj
    }
    this.Spinner.show();
    let url = this.baseurl + '/updateContraReqLHVerify';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj, { headers }).subscribe(data => {
      this.Spinner.hide()



      this.toastr.success('Contractor Requisition Approved Successfully');
      this.getallContractorDataRequest();

    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Approved Contractor Requisition');
    })



  }
  //new code
  contractorchecked1: boolean = false;
  checks1() {
    for (let i = 0; i < this.manpowerDatas.length; i++) {
      // this.leaveList[i].check = true;
      if (this.manpowerDatas[i].check) {
        this.contractorchecked1 = true;
        break;
      }
      else {
        this.contractorchecked1 = false;
      }
    }
  }
  contractorchecked2:boolean=false;
  checks2() {
    for (let i = 0; i < this.allmanpowerLabourData.length; i++) {
      // this.leaveList[i].check = true;
      if (this.allmanpowerLabourData[i].check) {
        this.contractorchecked2 = true;
        break;
      }
      else {
        this.contractorchecked2 = false;
      }
    }
  }

  approveLR1(item) {

    var uniqueList = this.selectedContractorEmp.filter((obj, index, array) => {
      return array.findIndex(item => item.conMasterId === obj.conMasterId) === index;
    });
    if(uniqueList.length==0){
    this.toastr.error('Please select contractor')
    }
    else{
    
    console.log(uniqueList);
    let selectedObj3: any = new Array();
    let obj = {
      "requestId": item.empManpowerReqId,
      "remark": this.approverRemark,
      "flag": "Approved",
      "approverId": this.empids,

    }
    selectedObj3.push(obj)
    let obj2 = {
      "commonApprovedReqDtos": selectedObj3,
      "selectContractorIdList":uniqueList
    }

    this.Spinner.show();
    let url = this.baseurl + '/updateManpowerReqHRVerify';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj2, { headers }).subscribe(data => {

      //document.getElementById('LeaveCol').click();
      this.getAllManpowerForHR();
      this.Spinner.hide();
      setTimeout(() => {
        this.getAllManpowerForHR();
      }, 1000);

      this.toastr.success('Manpower Requisition Assign to Contractor Successfully');

      this.SelectContractorTypeList=[];
      obj2.commonApprovedReqDtos = [];
      obj2.selectContractorIdList = [];
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Approve manpower Requisition');
    })
  }
}
  approveLabour(item) {
    
    // let selectedObj4: any = new Array();
    let obj = {
      "requestId": item.manReqStatusId,
      "remark": this.approverRemark1,
      "flag": "Approved",
      "approverId": this.empids,
      // "manReqStatusId":item.manReqStatusId
    }
   

    this.Spinner.show();
    let url = this.baseurl + '/approvedOrRejectLabour';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj, { headers }).subscribe(data => {

      //document.getElementById('LeaveCol').click();
      this.getAllManpowerRequestApprovedByContractor();
      this.Spinner.hide();
      setTimeout(() => {
        this.getAllManpowerRequestApprovedByContractor();
      }, 1000);

      this.toastr.success('Labour Requisition Approved Successfully');
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Approve manpower Requisition');
    })
  }
  rejectLeaveRequest1(item) {
    let selectedObj1: any = new Array();
    let obj = {
      "requestId": item.empManpowerReqId,
      "remark": this.approverRemark,
      "flag": "Rejected",
      "approverId": this.empids,
    }
    selectedObj1.push(obj)
    let obj2 = {
      "commonApprovedReqDtos": selectedObj1
    }
    this.Spinner.show();
    let url = this.baseurl + '/updateManpowerReqHRVerify';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj2, { headers }).subscribe(data => {
      this.Spinner.hide()
      setTimeout(() => {
        this.getAllManpowerForHR();
      }, 1000);

      this.toastr.success('manpower Requisition Rejected Successfully');
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Reject manpower Requisition');
    })
  }
  rejectLabour(item) {
    let selectedObj5: any = new Array();
    let obj = {
      "requestId": item.manReqStatusId,
      "remark": item.approverRemark,
      "flag": "Rejected",
      "approverId": this.empids,

    }
    selectedObj5.push(obj)
    let obj2 = {
      "commonApprovedReqDtos": selectedObj5
    }
    this.Spinner.show();
    let url = this.baseurl + '/approvedOrRejectLabour';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj, { headers }).subscribe(data => {
      this.Spinner.hide()
      setTimeout(() => {
        this.getAllManpowerRequestApprovedByContractor();
      }, 1000);

      this.toastr.success('manpower Requisition Rejected Successfully');
    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Reject manpower Requisition');
    })
  }
  approveContractor1() {
    let selectedObj: any = new Array();
    for (let i = 0; i < this.manpowerDatas.length; i++) {

      if (this.manpowerDatas[i].check) {

        let obj = {
          requestId: this.manpowerDatas[i].empManpowerReqId,
          empId: this.manpowerDatas[i].empId,
          approverId: this.empids,
          "flag": "Approved",
          remork: "",
        }
        selectedObj.push(obj);
      }

    }
    let obj = {
      "commonApprovedReqDtos": selectedObj
    }
    this.Spinner.show();
    let url = this.baseurl + '/updateManpowerReqHRVerify';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.put(url, obj, { headers }).subscribe(data => {
      this.Spinner.hide()



      this.toastr.success('manpower Requisition Approved Successfully');
      this.getallContractorDataRequest();

    }, err => {
      this.Spinner.hide()
      this.toastr.error('Failed To Approved manpower Requisition');
    })



  }
  showPopup(i) {
    this.showImage = i.labourLicense;
    // document.getElementById("popup").style.display = "block";
  }





  // for edit contactor Onboarding list
  getContractorOnboardingById(e: any) {
    this.isEditFlag = true; // Set your edit flag to true if needed
    this.getContractorOnboardingById = e.contractorOnboardingId; // Replace with the correct property name
  
    // Define the URL for the contractor onboarding API endpoint
    let url = this.baseurl + `/getContractorOnboardingById/${this.getContractorOnboardingById}`;
    
    // Set the authorization token in headers
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  
    // Send an HTTP GET request to fetch the data
    this.httpSer.get(url, { headers }).subscribe(data => {
      // Handle the response data as needed
      // Assign the data to your component properties
      this.contractorData = data["result"]; // Replace with the actual property names
      
      // Log the data for debugging
      console.log("Contractor Data:", this.contractorData);
    });
  }
  

// for contractoronboading delete


deleteContractor(e: any) {
  const confirmation = confirm("Are you sure you want to delete this Contractor?");

  if (!confirmation) {
    return; // Do nothing if the user cancels the confirmation
  }

  this.getDeleteContractorId = e.contractorId; // Replace with the correct property name

  // Define the URL for the delete contractor API endpoint
  let url = this.baseurl + `/deleteContractor/${this.getDeleteContractorId}`;

  // Set the authorization token in headers
  const tokens = this.token;
  const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);

  // Send an HTTP DELETE request to delete the contractor
  this.httpSer.delete(url, { headers }).subscribe(data => {
    // Display a success message and refresh the list of contractors
    this.toastr.success("Contractor deleted successfully");
    this.getAllContractors(); // Replace with the function to refresh the list
  });
}
  getAllContractors() {
    throw new Error('Method not implemented.');
  }
  // getAllContractorList(){
  //   let url = this.baseurl + '/getAllContractorMaster';
  // const tokens = this.token;
  // const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
  //   this.httpservice.get(url,{headers}).subscribe(data => {
  //     debugger
  //     this.GetAllContractor=data['result']
  //     for (let index = 0; index < this.GetAllContractor.length; index++) {
  //       this.GetAllContractor[index].conCompanyName = this.GetAllContractor[index].conMasterId+"-"+this.GetAllContractor[index].conCompanyName;
        
  // }

      
     
  // },
  //   (err: HttpErrorResponse) => {
  
  //   })
  
  //     }
  selectServiceType() {
    if (this.SelectContractorTypeList.length == 0) {
      this.contractorFlag = true;
    } else {
      this.contractorFlag = false;
    }
  }
  onItemSelect(item: any) {
    const id = item;
    if (this.selectedContractorEmp.length == 0) {
      this.selectedContractorEmp.push(item);
      // this.selectEmp(id);
    } else {
      for (var i = 0; i < this.selectedContractorEmp.length; i++) {
        if (this.selectedContractorEmp[i].conMasterId == item.conMasterId) {
        } else {
          this.selectedContractorEmp.push(item);
        }
      }
    }
  }
  onSelectAll(items: any) {
    this.selectedContractorEmp = items;
  }
  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.selectedContractorEmp.length; i++) {
      if (this.selectedContractorEmp.length == 0) {
        if (this.selectedContractorEmp[i].conMasterId == item.conMasterId) {
          this.selectedContractorEmp.splice(i, 1);
        }
      } else {
        if (this.selectedContractorEmp[i].conMasterId == item.conMasterId) {
          this.selectedContractorEmp.splice(i, 1);
        }
      }
    }
  }
  onDeSelectAll(item: any) {
    this.selectedContractorEmp = [];
  }
      getAllContractorList() {
      
        let url = this.baseurl + "/getAllContractorMaster";
        const tokens = this.token;
        const headers = new HttpHeaders().set("Authorization", `Bearer ${tokens}`);
        this.httpservice.get(url, { headers }).subscribe(
          (data) => {
            this.GetAllContractor = data["result"];
            this.AllContractorData = this.GetAllContractor;
            for (let index = 0; index < this.AllContractorData.length; index++) {
              this.AllContractorData[index].conCompanyName =
                this.AllContractorData[index].conMasterId +
                "-" +
                this.AllContractorData[index].conCompanyName;
            }
          },
          (err: HttpErrorResponse) => {}
        );
      }
      // onItemSelect2(item: any) {
      //   debugger
      //         const id = item;
      //          if (this.GetAllContractor.length == 0) {
      //            this.GetAllContractor.push(item)
      //             // this.selectEmp(id);
      //          }else{
      //            for (var i = 0; i < this.GetAllContractor.length; i++) {
      //              if (this.GetAllContractor[i].conMasterId == item.conMasterId) {
             
      //              } else {
      //                  this.GetAllContractor.push(item)
      //              }
      //            }
        
      //          }
      //        }
            //  onSelectAll2(items: any) {
            //   this.GetAllContractor = items;
            
            // }
            // OnItemDeSelect2(item: any) {
            //   debugger
            //   for (var i = 0; i < this.GetAllContractor.length; i++) {
            //     if(this.GetAllContractor.length == 0){
                    
            //       if (this.GetAllContractor[i].locationId == item.locationId) {
            //           this.contractorServiceType.splice(i, 1);
            //       }
            //     }else{
                  
            //         if (this.GetAllContractor[i].locationId == item.locationId) {
            //           this.GetAllContractor.splice(i, 1);
            //       }
            
            //     }
            //   }
            // }
            // onDeSelectAll2(item: any) {
            //   this.GetAllContractor = [];
            // }
      // selectServiceType2() {
      //   if(this.GetAllContractor.length == 0){
      //     this.serviceTypeFlag = true;
      //   }
      //   else{
      //     this.serviceTypeFlag = false;
      //   }
      // } 




}
