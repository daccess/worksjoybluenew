import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { RoleMasterService } from '../shared/services/RoleMasterService';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.css']
})
export class UploadDocumentComponent implements OnInit {
  themeColor: string = "nav-pills-blue";
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  allEmpdatas: any;
  docid: string;
  aadharStatus: any;
  experienceDocStatus: any;
  panStatus: any;
  policeDocStatus: any;
  fileToUpload: File = null;
  obj1: any = {};
  obj2: any = {};
  obj4: any = {}
  obj3: any = {};
  aadharImg: any;
  experienceLetter: any;
  panCardImg: any;
  receiptUrl1: any;
  imagevalidation1: any;
  imagevalidation2: string;
  imagevalidation3: string;
  imagevalidation4: string;
  receiptUrl2: any;
  policeVerificationDoc: any;
  receiptUrl3: any
  receiptUrl4: any;

  constructor(private httpSer:HttpClient,public Spinner :NgxSpinnerService,private toastr:ToastrService,private roleservice:RoleMasterService) { }

  ngOnInit() {
    this.user= sessionStorage.getItem('loggeduser')
  
    this.users=JSON.parse(this.user)
    this.token=this.users.token;

    this.docid=sessionStorage.getItem('docId')
    this.getAllDocumentRejectedList()
  }
  getAllDocumentRejectedList(){
    let url = this.baseurl + `/getDocumentRejectData/${this.docid}`;
const tokens = this.token;
const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.get(url,{headers}).subscribe(data => {
      this.allEmpdatas = data["result"];
      this.aadharStatus=this.allEmpdatas.aadharStatus;
      this.experienceDocStatus=this.allEmpdatas.experienceDocStatus;
      this.panStatus=this.allEmpdatas.panStatus;
      this.policeDocStatus=this.allEmpdatas.policeDocStatus;
    },
    (err: HttpErrorResponse) => {

    });
  }

  handleFileInput1(file: FileList) {
  
    this.fileToUpload = file.item(0);
    this.obj1.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.aadharImg = event.target.result.substring(0, 10);;
      this.receiptUrl1 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.aadharImg = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation1 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation1 = "true"
        }
      }, 300);
    }
  }
  handleFileInput2(file: FileList) {
    
    this.fileToUpload = file.item(0);
    this.obj2.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.panCardImg = event.target.result.substring(0, 10);;
      this.receiptUrl2 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.panCardImg = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation2 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation2 = "true"
        }
      }, 300);
    }
  }

  handleFileInput3(file: FileList) {
  
    this.fileToUpload = file.item(0);
    this.obj3.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.policeVerificationDoc = event.target.result.substring(0, 10);;
      this.receiptUrl3 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.policeVerificationDoc = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation3 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation3 = "true"
        }
      }, 300);
    }
  }
  handleFileInput4(file: FileList) {
  
    this.fileToUpload = file.item(0);
    this.obj4.name = file.item(0).name;
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.experienceLetter = event.target.result.substring(0, 10);;
      this.receiptUrl4 = event.target.result



    }
    reader.readAsDataURL(this.fileToUpload);
    let flag1 = false;
    if (file) {
      var img = new Image();

      img.src = window.URL.createObjectURL(file[0]);

      img.onload = function () {
        // var flag = false;
        var width = img.naturalWidth,
          height = img.naturalHeight;

        window.URL.revokeObjectURL(img.src);

        if (width < 1000 && height < 1000) {
          flag1 = true;
        }
        else {
          flag1 = false;

        }
      };

      setTimeout(() => {

        if (flag1) {

          this.fileToUpload = file.item(0);
          var reader = new FileReader();
          reader.onload = (event: any) => {
            this.experienceLetter = event.target.result;
          }
          reader.readAsDataURL(this.fileToUpload);
          this.imagevalidation4 = "false"
        }
        else {
          this.toastr.error("Please upload image upto 1 MB")
          this.imagevalidation4 = "true"
        }
      }, 300);
    }
  }

  updateLabourImage
  updateDocument(){
    let url = this.baseurl + '/updateLabourImage';
const tokens = this.token;
if(this.aadharStatus=='rejected'){
  var obj={
    "docId":this.docid,
"imagePath":this.aadharImg,
"documentName":'AdharCard'
  }
}
if(this.panStatus=='rejected'){
  var obj={
    "docId":this.docid,
"imagePath":this.panCardImg,
"documentName":'PanCard'
  }
}
if(this.panStatus=='rejected'){
  var obj={
    "docId":this.docid,
"imagePath":this.panCardImg,
"documentName":'PanCard'
  }
}
if(this.policeDocStatus=='rejected'){
  var obj={
    "docId":this.docid,
"imagePath":this.policeVerificationDoc,
"documentName":'Police Verification'
  }
}
if(this.experienceDocStatus=='rejected'){
  var obj={
    "docId":this.docid,
"imagePath":this.experienceLetter,
"documentName":'Experience Letter'
  }
  
}

const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpSer.put(url,obj,{headers}).subscribe(data => {
     this.toastr.success("document Upload successfully")
    },
    (err: HttpErrorResponse) => {

    });
  }

}
