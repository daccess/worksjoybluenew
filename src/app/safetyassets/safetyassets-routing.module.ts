import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SafetyassetsComponent } from './safetyassets.component';

const appRoutes: Routes = [
  { 
           
      path: '', component: SafetyassetsComponent,

          children:[
              {
                  path:'',
                  redirectTo : 'safetyapproval',
                  pathMatch :'full'
                  
              },
          
              {
                  path:'safetyapproval',
                  component: SafetyassetsComponent
              }

          ]

  }


]

export const safetyassetsRouting = RouterModule.forChild(appRoutes);