import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MainURL } from '../shared/configurl';

@Component({
  selector: 'app-safetyassets',
  templateUrl: './safetyassets.component.html',
  styleUrls: ['./safetyassets.component.css']
})
export class SafetyassetsComponent implements OnInit {
  baseurl = MainURL.HostUrl;
  user: string;
  users: any;
  token: any;
  empids: any;
  locationdata: any;
  locationId: any;
  safetyLabourData: any;
  labourId: any;
  manReqStatusIde: any;

  constructor(private httpservice:HttpClient,public Spinner: NgxSpinnerService,private toastr:ToastrService,private router:Router) { 
    this.user = sessionStorage.getItem('loggeduser')
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empids = this.users.roleList.empId;
    this.locationId=this.users.roleList.locationId
  }

  ngOnInit() {
    this.getlocationData()
  }

  getlocationData() {
    let url = this.baseurl + '/getAllLocations';
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.locationdata = data["result"];
      this.locationId = this.locationId;
    this.getSafetyEmployee();

    },
      (err: HttpErrorResponse) => {

      })

  }
  getSafetyEmployee(){
    let url = this.baseurl + `/getSafetyLabourList/${this.locationId}`;
    const tokens = this.token;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${tokens}`);
    this.httpservice.get(url, { headers }).subscribe(data => {

      this.safetyLabourData = data["result"];
      // this.locationId = this.locationdata[0].locationId
    

    },
      (err: HttpErrorResponse) => {

      })

  }
  gotoDocument1(e:any){
    this.labourId=e.requestStatus.labourInfo.labourPerId;
    this.manReqStatusIde=e.requestStatus.manReqStatusId;
    sessionStorage.setItem("manrequestIds",this.manReqStatusIde)
    sessionStorage.setItem("LabourPerIds",this.labourId)
    this.router.navigate(['/layout/safetyRemark'])
  }
  gotoDocument2(e:any){
  
    this.labourId=e.requestStatus.labourInfo.labourPerId;
    this.manReqStatusIde=e.requestStatus.manReqStatusId;
    sessionStorage.setItem("manrequestIds",this.manReqStatusIde)
    sessionStorage.setItem("LabourPerIds",this.labourId)
    sessionStorage.setItem('IseditFlag', 'true')
    this.router.navigate(['/layout/frontDeskAssets'])
    
  }
}
