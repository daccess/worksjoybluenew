import { NgModule } from '@angular/core';
import { CommonModule, FormStyle } from '@angular/common';

import {safetyassetsRouting } from './safetyassets-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafetyassetsComponent } from './safetyassets.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    safetyassetsRouting
  ],
  declarations: [
   SafetyassetsComponent
  ]
})
export class SafetyassetsModule { }
