import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyassetsComponent } from './safetyassets.component';

describe('SafetyassetsComponent', () => {
  let component: SafetyassetsComponent;
  let fixture: ComponentFixture<SafetyassetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyassetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyassetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
