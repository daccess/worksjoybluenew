import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavetakenreportdaywiseComponent } from './leavetakenreportdaywise.component';

describe('LeavetakenreportdaywiseComponent', () => {
  let component: LeavetakenreportdaywiseComponent;
  let fixture: ComponentFixture<LeavetakenreportdaywiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavetakenreportdaywiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavetakenreportdaywiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
