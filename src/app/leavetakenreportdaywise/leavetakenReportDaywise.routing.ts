import { Routes, RouterModule } from '@angular/router'
import { LeavetakenreportdaywiseComponent } from './leavetakenreportdaywise.component';

const appRoutes: Routes = [
    { 
             
        path: '', component: LeavetakenreportdaywiseComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'leaveTakenReportDaywise',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'leaveTakenReportDaywise',
                    component: LeavetakenreportdaywiseComponent
                }

            ]

    }
  
 
]

export const routing = RouterModule.forChild(appRoutes);