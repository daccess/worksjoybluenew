import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../absentreport/excel.service';

import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-leavetakenreportdaywise',
  templateUrl: './leavetakenreportdaywise.component.html',
  styleUrls: ['./leavetakenreportdaywise.component.css'],
  providers: [ExcelService,DatePipe]
})
export class LeavetakenreportdaywiseComponent implements OnInit {

  fromDate: any;
  toDate:  any;
  AllReportData: any;
  AllReport = [];

  empCheckbox = true;
  leaveNamecheckbox = true;
  leaveCodecheckbox = true;
  fromdateCheckbox = true;
  todateCheckbox = true;
  noofleavesCheckbox = true;
  leavetypeCheckbox = true;
  halfdayTypeCheckbox = true;
  pannedUnplanCheckbox = true;
  reasonCheckbox = true;

  marked = true;
  marked1 = true;
  marked2 = true;
  marked3 = true;
  marked4 = true;
  marked5 = true;
  marked6 = true;
  marked7 = true;
  marked8 = true;
  marked9 = true;
  marked10 = true
  AllData: any =[];
  loggedUser: any;
  compName: any;
  todaysDate: Date;

  constructor(private excelService: ExcelService,private datePipe: DatePipe) { 
    this.fromDate = sessionStorage.getItem("fromDate");
    this.toDate = sessionStorage.getItem("toDate");
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compName = this.loggedUser.compName
    this.todaysDate = new Date();
    this.AllReportData = sessionStorage.getItem('leaveTakenReportDaywise');
    this.AllReport = JSON.parse(this.AllReportData);
    var myArrayNew = this.AllReport.filter(function (el) {
      return el != null && el != "";
    });
    this.AllReport = myArrayNew;
    for (let i = 0; i < this.AllReport.length; i++) {
      const element = this.AllReport[i];
        for (let j = 0; j < element.length; j++) {
          const element1 = element[j];
          this.AllData.push(element1);
        }
    }
  }

  ngOnInit() {
  }

  capturePDF(){

let item = {
      header: "Sr.No",
      title: "ss"
    }
    let item1 = {
      header: "Employee",
      title: "abc"
    }
    let item2 = {
      header: "Leave Name",
      title: "abc1"
    }
    let item3 = {
      header: "Leave Code",
      title: "abc2"
    }
    let item4 = {
      header: "From date",
      title: "abc3"
    }

    let item5 = {
      header: "To date",
      title: "cde"
    }
    let item6 = {
      header: "No.Of.Leave",
      title: "efg"
    }
    let item7 = {
      header: "Leave Type",
      title: "ghi"
    }
    let item8 = {
      header: "Half day Type",
      title: "ijk"
    }
    let item9 = {
      header: "p/u",
      title: "klm"
    }
    let item10 = {
      header: "Reason",
      title: "mno"
    }

    var doc = new jsPDF('landscape', 'px', 'a4');
    var col = [];

    if (this.marked) {
      col.push(item);
    }
    if(this.marked1){
      col.push(item1);
    }
    if(this.marked2){
      col.push(item2);
    }
    if(this.marked3){
      col.push(item3);
    }
    if (this.marked4) {
      col.push(item4);
    }
    if (this.marked5) {
      col.push(item5);
    }
    if (this.marked6) {
      col.push(item6);
    }
    if (this.marked7) {
      col.push(item7);
    }
    if (this.marked8) {
      col.push(item8);
    }
    if (this.marked9) {
      col.push(item9);
    }
    if (this.marked10) {
      col.push(item10);
    }
   

    var rows = [];

    var rowCountModNew = this.AllData;

    rowCountModNew.forEach((element , index ) => {
      let obj = []
      if (this.marked) {
        obj.push(index+1);
      }

      if (this.marked1) {
        obj.push(element.fullName+' '+ element.lastName);
      }

      if(this.marked2){
        obj.push(element.leaveName);
      }
      if(this.marked3){
        obj.push(element.leaveCode);
      }
      if(this.marked4){
        obj.push(element.startDate);
      }
      if (this.marked5) {
        obj.push(element.endDate);
      }
      if (this.marked6) {
        obj.push(element.leaveCount);
      }
      if (this.marked7) {
        obj.push(element.leaveType);
      }
      if (this.marked8) {
        obj.push(element.typeOfLeave);
      }
      if (this.marked9) {
        obj.push(element.typeOfLeave);
      }
      if (this.marked10) {
        obj.push(element.typeOfLeave);
      }
     
      rows.push(obj);
    });

    doc.setTextColor(48, 80, 139)
    doc.setFontSize(10)
    doc.setFontStyle("Arial")
    doc.text(34, 14 ,'Company Name :'+ this.compName)

    // doc.setTextColor(0, 0, 0)
    // doc.setFontSize(7)
    // doc.setFontStyle("Arial")
    // doc.text(34, 44 ,'Abbreviation:- GS : General Shift, SS : Store Shift')
    
    doc.setTextColor(48, 80, 139)
    doc.setFontSize(12);
    doc.setFontStyle("Arial")
    doc.text(280, 14,'Leave_taken_report')

    doc.setTextColor(40, 80, 139)
    doc.setFontSize(8)
    doc.setFontStyle("Arial")
    doc.text(280, 24 ,'From' +' : '+ this.datePipe.transform(this.fromDate,"dd-MMM-yyyy") + ' ' +' To' + ' : ' + this.datePipe.transform(this.toDate,"dd-MMM-yyyy")
    )

    doc.setTextColor(0, 0, 0)
    doc.setFontSize(7)
    doc.setFontStyle("Arial")
    doc.text(520, 44 ,'Report date' + ' : '+ this.datePipe.transform(this.todaysDate,"dd-MMM-yyyy hh:mm:ss a"))
    doc.autoTable(col, rows,{
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      headerStyles: {
        fillColor: [103, 132, 130],   
    },
    
      styles: {
        halign: 'center'
      },
      theme: 'grid',
      margin: { top: 50, left: 5, right: 5, bottom: 50 }
    });
    doc.save('Leave_taken_report'+this.datePipe.transform(this.todaysDate,"dd-MM-yyyy hh:mm:ss")+'.pdf');



  }

  exportAsXLSX(): void {
    let excelData = new Array();
    for (let i = 0; i < this.AllData.length; i++) {

      let obj : any= { }

      // let excel = []
      if(this.marked){
        obj.Sr = i+1
        // obj.lastName = this.exceldata1[i].lastName
      }
      if(this.marked1){
        obj.fullName = this.AllData[i].fullName+' '+this.AllData[i].lastName;
        // obj.lastName = this.exceldata1[i].lastName
      }
      if(this.marked2){
        obj.leaveName = this.AllData[i].leaveName;
      }

      if(this.marked3){
        obj.leaveCode = this.AllData[i].leaveCode;
      }

      if(this.marked4){
        obj.startDate = this.AllData[i].startDate;
      }

      if(this.marked5){
        obj.endDate = this.AllData[i].endDate;
      }

      if(this.marked6){
        obj.leaveCount = this.AllData[i].leaveCount;
      }

      if(this.marked7){
        obj.leaveType = this.AllData[i].leaveType;
      }

      if(this.marked8){
        obj.TypeOfLeave = this.AllData[i].typeOfLeave;
      }

      if(this.marked9){
        obj.planned_unplannned = this.AllData[i].typeOfLeave;
      }

      if(this.marked10){
        obj.Reason = this.AllData[i].typeOfLeave;
      }

      excelData.push(obj);
    }
    this.excelService.exportAsExcelFile(excelData, 'Leave_taken_report');
  }


  toggleVisibility(e) {
    this.marked = e.target.checked;
  }

  toggleVisibility1(e) {
    this.marked1 = e.target.checked;
  }

  toggleVisibility2(e) {
    this.marked2 = e.target.checked;
  }

  toggleVisibility3(e) {
    this.marked3 = e.target.checked;
  }

  toggleVisibility4(e) {
    this.marked4 = e.target.checked;
  }


  toggleVisibility5(e) {
    this.marked5 = e.target.checked;
  }

  toggleVisibility6(e) {
    this.marked6 = e.target.checked;
  }

  toggleVisibility7(e) {
    this.marked7 = e.target.checked;
  }

  toggleVisibility8(e) {
    this.marked8 = e.target.checked;
  }

  toggleVisibility9(e) {
    this.marked9 = e.target.checked;
  }
}
