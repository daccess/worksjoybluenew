import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { DOCUMENT,DatePipe } from '@angular/common';
import { Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from "@angular/common/http";
import { MainURL } from '../shared/configurl';
import { EmpDashModel } from '../shared/model/EmpDashboardModel';
import { EmpDashSerivcService } from '../shared/services/EmpDashboardService'
import { ChangeDetectorRef } from '@angular/core';
import { NewsModel } from '../shared/model/EmpNews';
import { PayModel } from '../shared/model/PayModel'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Http } from '@angular/http';
import { BadyWishModel } from './Model/bady-wish-model';
import { SelfCertficationService } from './services/selfcertification.service';
import { selfCertification } from './Model/selfcerification.model';
import { selfcertificationservicestatus } from './services/selfcertificationstaus.service';
import { WorkWishModel } from './Model/workAnniModel';
import { EmployeeCouponModel } from './Model/coupon';
import { CalendarService } from '../shared/services/calendar.service';
import { InfoService } from "../shared/services/infoService";
import { coffDetails } from './Model/coff-details';
import {WeeklyPattern} from './Model/weeklypattern'
import { LeavedashboardServiceService } from './services/leavedashboard-service.service';
//import * as moment from 'moment';



@Component({
  selector: 'app-empdashboard',
  templateUrl: './empdashboard.component.html',
  styleUrls: ['./empdashboard.component.css', './empdashboard.component.scss'],
  providers: [SelfCertficationService, selfcertificationservicestatus, LeavedashboardServiceService]

})
export class EmpdashboardComponent implements OnInit {
  currentopenings: any=[];
  public selectUndefinedOptionValue: any;
  minDate: any;
  maxDate: any;
  validTillDate: any;
  selectedEmploymentType: any = [];
  selectedItems: any
  dropdownSettings: {}
  employeePerInfoList = []
  employeeListdata: any
  flag: boolean = true;
  flag2: boolean = false;
  monthStartDate: any;
  monthEndDate: any;
  payRollMonth: any;
  payrollM: any;
  baseurl = MainURL.HostUrl;
  AssetsPath=MainURL.AssetsPath;
  Aws_flag=MainURL.Aws_flag;
  date: Date = new Date();
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: any;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  absentDay: any = "17"
  currentMonth1: any;
  mapsurl: string = "https://www.facebook.com/"
  monthlyWeekly: string = "monthly"
  compId: any;
  TodaysBirthday: any;
  TodaysJoingData: any;
  HolidayData = [];
  todaysDate: Date;
  selfcertModel: selfCertification;
  holiDayName: any;
  startDate: any;
  fullName: any;
  lastName: any;

  TodolistData: any;
  entryTime: any;
  message: any;
  todoDate: any;
  dateTime: any;
  newdateTime: Date;
  CoffData = [];
  extraWorkedDay: any;
  typeOfCredit: any;
  LeavesData = [];
  totalLeaves: any;
  currentLeaves: any;
  leaveName: any;
  totalleave: any;
  leaveBalance: any;
  bardata: any;
  a: any;
  AttendanceData: any = [];
  attenDate: any;
  otduration: any;
  statusOfDay: any;
  checkOut: any;
  checkIn: any;
  attendanceStatus: boolean;
  percentage: any;

  customClass: String = ""
  ApprovalData: any;
  WorkFromHome: any;
  LateEarly: any;
  MissPunch: any;
  OnDuty: any;
  PermissionReq: any;
  leaves: any;
  empDashModel: EmpDashModel;
  newsModel: NewsModel;
  payM: PayModel
  Subject: any;
  getobj: any;
  gotObj: any;
  Datetime: any;
  finalDatetime: Date;
  isedit: boolean;
  editnwesflag: boolean
  TodaysNews = [];
  getobjj: any;
  f: boolean = false;
  monthAttendance: any = [];
  selectedNews: any = [];
  PendingLeaveReq: any;
  PendigWorkFromHome: any
  PendingLateEarly: any
  PendingMissPunch: any
  PendingOnDuty: any
  PendingPermReq: any;
  WorkFromObj: any;
  LateEarlyObj: any;

  lateEarlyObj: any

  CoffFlag: Boolean;

  MissPunchObj: any
  payStartt: any;
  payStart: any;
  payEnd: any;
  PendingPermissionReq: any;

  PresentDay: any;

  absentDays: any
  lateearly: any
  leave: any
  leavecoff: any
  onduty: any
  overTimeHours: any
  paidDays: any
  shortHours: any
  weekofholydays: any
  modelFlag: boolean;
  dltmodelFlag: boolean
  Today: any;
  colorflag: boolean;
  LateSittigData = [];
  imgflag: boolean;
  Pendingimgflag: boolean
  HolidayFlag: boolean;
  locationMaster = [];
  selectedGrade: any;
  locationData: any = [];
  dropdownSettingsLocation: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  loggedUser: any;
  empOfficialId: any;
  empPerId: any;
  PendingReq: any;
  fileToUpload: File;
  filesToUpload: File;
  imageUrl: any;
  OnDutyObj: any;
  DeletgotObj: any;
  filPath: any;
  GetFilePath: any;
  weekAttendance = [];
  dltWorkFromObj: any;
  dltPendingPermissionReq: any;
  dltMissPunchObj: any;
  dltOnDutyObj: any;
  dltLateEarlyObj: any;

  CurrntMonth: any;
  CurrentYear: any;
  now = new Date();
  ShiftData: any;
  ShiftAllownce: any;
  requestWFHId: number;
  newsFlag: boolean;
  toListFlag: boolean;
  TodoListFlag: boolean
  empFlag: boolean;
  searchQuery: any;
  Allemp: any;
  tododlt: boolean;
  selectedCompanyobj: number;
  locoid: any;
  todoDelt: boolean;
  dte: Date;
  newsFeedId: number;
  TotalLikes: any;
  likeCount: any;
  fromid: any;
  designation: any;
  desname: any;
  dltnewsmodelflag: boolean;
  isdelet: boolean;
  geteditnwesobj: any;
  roleHead: any;
  likeCountfalg: boolean
  likearr = [];
  loginFlag: any;
  GraduatyData: any;
  GradutyYear: any;
  aloginFlag = "false";
  gratuityStatus: boolean;
  empatendanceStatus: any;
  attendanceStatus1: any;
  empattendanceStatus: any;
  gratuityMsg: any;
  leaveCode: any;
  absd: string;
  used: any;
  avaliable: any;
  total: any;
  hidealertflag: boolean;
  usedLeaves: any;
  filPathImage: any;
  fileTypeImage: any;
  fileType: any;
  returnImageObjectFile: any = {};
  balanceLeaveType: string = "All"
  coffApproval: any;
  dltCoffObj: any;
  PendingCoffReq: any = [];
  PendingExtraWorkReq: any = [];
  extraWorkApproval: any;
  from: any;
  openingBalance: any;
  team: any;
  teamDeatails: any;
  profilePath: any;
  ProfilePath: any;
  BirthdayPopUp = [];
  BadyWishModel: BadyWishModel;
  workAnniMoel: WorkWishModel;
  employeeCouponModel: EmployeeCouponModel;
  employeeNameForBaday: string;
  fullname: any;
  //lastName: any;
  descName: any;
  DeptName: any;
  dateOfBirth: any;
  loggedUserEmpPerID: any;
  todayBadyEmpPerID: any;
  errorMessage: any;
  selfCertificationStatus: any;
  locationId: any;
  workAnniData: any;
  workAnnifullName: any;
  workAnniLastName: any;
  workAnniDescrip: any;
  workAnniDepat: any;
  workAnniWishID: any;
  WorkanniData: any;
  workAnniSatus: any;
  workAnniCmpltYear: any;
  workAniEmpId: any;
  completedYears: any;
  empId: any;
  proPath: any;
  joiningYear: any;
  deptName: any;
  birthdayPopData: any;
  birthDayMsg: any;
  birthDayStatus: any;
  allemp: string;
  selectemp: string;
  model: any;
  coupanType: string = "allemp"
  arrears: any;
  DepartmentListForCoupon: any;
  EmployeListForCoupon: any;
  selectedObj: any;
  selectedDep: number;
  deptId: number;
  coupounList: any;
  cpnName: string;
  deletecoupounList: any;
  cpnId: any;
  cpnIcon: string;
  editcoupoun: any;
  returnsImageObjectFile: any;
  current: string;
  dltsObje: any;
  dltmodelsFlag: boolean;
  deletObject: any;
  deletObjec: any;
  startDatefor: string;
  payEndfor: string;
  selfCertificationReqDtos = [];
  role: any;
  time: { hour: number; minute: number; };

  empTypeId: any;
  empIdBday: any;
  today: any;
  AllcoupounList: any;
  CouponempPerId: any;
  empPerIdCoupanAdmin: any;
  currentopening: Object;
  deparmentname: any;
  mrfid: any;
  currentopeningdetails:any;
  deptnames: any;
  descNames: any;
  locationname: any;
  noofvacancy: any;
  mandatoryqualification: any;
  experience: any;
  agegroup: any;
  typeofvacancy: any;
  tentetiveDate: any;
  initiatedDate: any;
  roleId: any;
  mydate: any;
  mydata: any;
  datedata: string;
  datedata12: string;
  fromdate1: string;
  locationFlag: boolean = false;
  Nflag: boolean = false;
  allnewsData: any=[];
  selectedYear: any;
  newDate: Date;
  weeklyPattern : WeeklyPattern
  coffDetails : coffDetails
  wen: any;
  tue: any;
  thu: any;
  sun: any;
  sat: any;
  fri: any;
  mon: any;
  currentY: number;
  name:any;
  token: any;
  user: string;
  users: any;
  empMasterId: string;
  empOfficialIdshree: any;


 
  ngOnInit() {
    this.getcurrentOpening();
    this.aloginFlag = sessionStorage.getItem('aloginFlag')
    sessionStorage.setItem('aloginFlag', 'false')
    this.aloginFlag = "false";
    this.isdelet = false
    this.isedit = false
    this.editnwesflag = false
    this.todaysDate = new Date();
    this.HideBirthdayflag = true

    
    // this.TodayEMPBirthday();
    // this.TodaysJoing();
    // this.getWorkAnniversary();
    // this.getBirthday();
    // this.Todolist();
    // this.CoffList();
    // this.Leaves();
    // this.Approval();
    // this.getallNews();
    // this.Pending();
    //  this.PostPay();
    // this.LateSitting();
    // this.getTeam()
    this.modelFlag = false
    this.dltmodelFlag = false
    this.colorflag = false;
    this.hidealertflag = false
    this.roleHead = this.loggedUser.mrollMaster.roleName;


    // let url1 = this.baseurl + '/Location/Active/';
    // this.httpService.get(url1 + this.compId).subscribe(data => {
    //     this.locationData = data["result"];
    //   },
    //   (err: HttpErrorResponse) => {
    //   });
    let url2 = this.baseurl + '/gratuityEligiblityController/';
    this.httpService.get(url2 + this.empPerId).subscribe(data => {
        this.aloginFlag = "true"
        this.GraduatyData = data["result"];
        this.GradutyYear = data['result']['gratuityear']
        this.gratuityStatus = data['result']['gratuityStatus']
        this.gratuityMsg = data['result']['gratuityMsg']
        this.joiningYear = data['result']['joiningYear']
      },
      (err: HttpErrorResponse) => {
      });
    let url3 = this.baseurl + '/EmpAttendanceStatus/';
    this.httpService.get(url3 + this.empPerId).subscribe(data => {
        this.aloginFlag = "true";
        this.empattendanceStatus = data['result']['attendanceStatus'];
      },
      (err: HttpErrorResponse) => {
      });
    this.dropdownSettingsLocation = {
      singleSelection: false,
      idField: 'locationId',
      textField: 'locationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'empPerId',
      textField: 'fullName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    if( this.roleHead=='Admin'){
     
      let url1 = this.baseurl + '/Location/Active/';
      this.httpService.get(url1 + this.compId).subscribe(data => {
          this.locationData =data["result"];
        
       
        },
        (err: HttpErrorResponse) => {
        });
    }
    else if( this. roleHead=='HR'){

      let url1 = this.baseurl + '/LocationWise/';
      this.httpService.get(url1 +this.loggedUser.locationId).subscribe(data => {
        this.locationData = data["result"];
     
      });
    }
  }
  @ViewChild('myInput')
  myInputVariable: ElementRef;
  constructor(@Inject(DOCUMENT) private document: any, public Spinner: NgxSpinnerService, private router: Router, public sanitizer: DomSanitizer, public ch: ChangeDetectorRef,
    public DshServic: EmpDashSerivcService, public httpService: HttpClient, private toastr: ToastrService, private http: Http, public selfService: SelfCertficationService,
    public selfCertStatus: selfcertificationservicestatus, private calendarService: CalendarService,private InfoService:InfoService) {
    this.allemp = "allemp";
    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.empDashModel = new EmpDashModel();
    this.newsModel = new NewsModel();
    this.payM = new PayModel()
    this.BadyWishModel = new BadyWishModel();
    this.BadyWishModel.birthdayWish = "Wish you happy birthday ..!";
    this.workAnniMoel = new WorkWishModel()
    this.employeeCouponModel = new EmployeeCouponModel()
    this.selfcertModel = new selfCertification()
    this.empDashModel.dateTime = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.newsModel.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.role = this.loggedUser.mrollMaster.roleName;
    this.teamDataFlag = false;
    this.imgflag = false;
    this.Pendingimgflag = false;
    this.modelFlag = false;
    this.dltmodelFlag = false;
    this.dltnewsmodelflag = false;
    this.CoffFlag = false;
    this.HolidayFlag = false;
    this.newsFlag = false;
    this.toListFlag = false;
    this.TodoListFlag = false;
    this.todoDelt = false;
    this.Today = new Date().getTime();
    var date = new Date();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    this.payrollM = m + '-' + y;
    this.dte = new Date();
    this.dte.setDate(this.dte.getDate() - 1);
    // this.compId = this.loggedUser.compId;
    // this.empOfficialId = this.loggedUser.empOfficialId;
   
    
    // this.empTypeId = this.loggedUser.empTypeId;
    // this.empPerId = this.loggedUser.empPerId;
    // this.locationId = this.loggedUser.locationId;
    // this.deptName = this.loggedUser.deptName;
    // this.empId=this.loggedUser.empId;
    // this.roleId=this.loggedUser.rollMasterId;
    // this.deptId=this.loggedUser.deptId;
    this.weeklyPattern = new WeeklyPattern();
   this.coffDetails = new coffDetails();
   this.currentY = new Date().getFullYear();
   this.selectedYear = this.currentY;

    var curr = new Date(); // get current date
    // this.getWorkAnni();
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6
    // this.Attendance();
    // this.getMonthLockData();
    this.weekObject = {
      empOfficialId: this.empOfficialId,
      fromDate: new Date(curr.setDate(first)).toUTCString(),
      toDate: new Date(curr.setDate(last)).toUTCString()
    };


    this.user = sessionStorage.getItem("loggeduser");
    this.users = JSON.parse(this.user);
    this.token = this.users.token;
    this.empMasterId = sessionStorage.getItem("empmasterId");
    // this.Holidays();
    this.getAttendanceOfMonth(new Date().getMonth() + 1, new Date().getFullYear());
    this.f = false;
    this.CurrentYear = this.now.getFullYear();
    this.CurrntMonth = this.now.getMonth() + 1;
    // this.shiftAllowns();
    // this.getAllEmployee();
    let start = new Date(new Date(curr.setDate(first)).toUTCString());
    let end = new Date(new Date(curr.setDate(last)).toUTCString());
    let year = start.getFullYear();
    let month = start.getMonth() + 1;
    let day = start.getDate();
    let dates = [start];
    let temp = new Array();
    while (dates[dates.length - 1] < end) {
      dates.push(new Date(year, month, ++day));
    }
    this.periodicDates = dates;
    for (let i = 0; i < this.periodicDates.length; i++) {
      temp.push(new Date(this.periodicDates[i]).getFullYear() + "-" + (new Date(this.periodicDates[i]).getMonth()) + "-" + new Date(this.periodicDates[i]).getDate())
    }
    this.periodicDates = temp;
    temp.splice(temp.length - 1, 1);
    this.weekDatesArray = temp;
    // this.getWeeklyCalendar1(this.weekObject);
    // this.notYetInToday();
    // this.todaysLeaveRequest();
    // this.getAllCoupons();
    window.addEventListener('popstate',() => {
    });
    // this.getDepartmentForCoupon();
    this.employeeCouponModel.cpnVariable = 'allemp'
    this.Today = new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getDate();
    this.current = (new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + new Date().getFullYear();
    this.employeeCouponModel.validTillDate = (new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + new Date().getFullYear();
    // this.getCoponList();
  }
  getWeekCal() {
    var curr = new Date; // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6
    var firstday = new Date(curr.setDate(first)).toUTCString();
    var lastday = new Date(curr.setDate(last)).toUTCString();
    this.weekObject = {
      empOfficialId: this.empOfficialId,
      fromDate: firstday,
      toDate: lastday
    };
    this.getWeeklyCalendar(this.weekObject);
  }

  getSelfcertificationStatus() {
    this.selfCertStatus.getSelfCertificationstatus(this.empTypeId, this.payStart, this.payEnd).subscribe(data => {
      if (data) {
        this.selfCertificationStatus = data.result;
      }
      this.selfCertificationStatus = true;
    }, (err) => {
      JSON.parse(err._body);
      this.selfCertificationStatus = err._body.result
    });
  }

  getMonthLockData() {
    this.selfService.getMonthLockData(this.empPerId, this.payrollM).subscribe(data => {
      if (data) {
        this.monthStartDate = data.result.startDate;
        this.monthEndDate = data.result.endDate;
        this.payRollMonth = data.result.payRollMonth;
        this.payPeriodDays(this.monthStartDate, this.monthEndDate);
        this.payStartt = this.monthStartDate;
        let now = new Date(this.payStartt);
        let isoString = now.toISOString().split('T')[0];
        this.payStart = isoString;
        this.payEnd = this.monthEndDate;
        sessionStorage.setItem("payStart", this.payStart);
        sessionStorage.setItem("payEnd", this.payEnd);
        this.PostPay();
        this.OtDetails();
        this.getSelfcertificationStatus();
        this.PaidDaysDetails();
        this.getShiftAllowance();
      }
      else {
        this.payEnd = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        this.payStart = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + 1);
        this.payPeriodDays(this.payStart, this.payEnd);
        this.PostPay();
        this.OtDetails();
        this.getSelfcertificationStatus();
        this.PaidDaysDetails();
        this.getShiftAllowance();
      }

    }, err => {
    });
  }
  getcurrentOpening(){
    let url = this.baseurl + '/mrfCurrentOpeningList';
    this.httpService.get(url).subscribe(data => {
this.currentopenings= data['result'];
this.deparmentname=this.currentopenings.result;

    })
  }
  //========================Weekly Calendar ======================
  weekStartDate: any;
  weekEndDate: any;
  count: number = 0;
  minusCount: number = 0;
  currentDateWeek: number = new Date().getTime();
  currentDateComp: number = new Date().getTime();
  saveSitePopup() {
    this.aloginFlag = "false";
    let url = this.baseurl + '/gratuityEligiblityController';
    let obj = {
      "empPerId": this.empPerId
    }
    this.httpService.put(url, obj).subscribe(
      data => {
      },
      (err: HttpErrorResponse) => {
      });
  }

  //added by priyanka k start
  callOnce() {
    this.aloginFlag = "false";
    let url = this.baseurl + '/updateAttenPopUpStatus';
    let obj = {
      empPerId: this.loggedUser.empPerId
    };
    this.httpService.put(url, obj).subscribe(data => {
    },(err: HttpErrorResponse) => {
    });
  }
  //added by priyanka k end
  periodicDates = [];
  weekDatesArray = [];
  goToLastWeek() {
    this.count = this.count - 7;
    var date = new Date().getTime() - this.count * 24 * 60 * 60 * 1000;
    this.currentDateWeek = this.currentDateWeek - 7 * 24 * 60 * 60 * 1000;
    var curr = new Date(this.currentDateWeek);
    var curr = new Date(this.currentDateWeek); // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6
    this.weekStartDate = new Date(curr.setDate(first)).toUTCString();
    this.weekEndDate = new Date(curr.setDate(last)).toUTCString();
    this.weekObject.fromDate = this.weekStartDate;
    this.weekObject.toDate = this.weekEndDate;
    let start = new Date(this.weekStartDate);
    let end = new Date(this.weekEndDate);
    let year = start.getFullYear();
    let month = start.getMonth();
    let day = start.getDate();
    let dates = [start];
    let temp = new Array();
    while (dates[dates.length - 1] < end) {
      dates.push(new Date(year, month, ++day));
    }
    this.periodicDates = dates;
    for (let i = 0; i < this.periodicDates.length; i++) {
      temp.push(new Date(this.periodicDates[i]).getFullYear() + "-" + (new Date(this.periodicDates[i]).getMonth() + 1) + "-" + new Date(this.periodicDates[i]).getDate())
    }
    this.periodicDates = temp;
    temp.splice(temp.length - 1, 1);
    this.weekDatesArray = temp;
    this.getWeeklyCalendar(this.weekObject);
  }
  goToNextWeek() {
    this.count = this.count + 7;
    var date = new Date().getTime() + this.count * 24 * 60 * 60 * 1000;
    this.currentDateWeek = this.currentDateWeek + 7 * 24 * 60 * 60 * 1000;
    var curr = new Date(this.currentDateWeek);
    var curr = new Date(this.currentDateWeek); // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6
    this.weekStartDate = new Date(curr.setDate(first)).toUTCString();
    this.weekEndDate = new Date(curr.setDate(last)).toUTCString();
    this.weekObject.fromDate = this.weekStartDate;
    this.weekObject.toDate = this.weekEndDate;
    let start = new Date(this.weekStartDate);
    let end = new Date(this.weekEndDate);
    let year = start.getFullYear();
    let month = start.getMonth();
    let day = start.getDate();
    let dates = [start];
    let temp = new Array();
    while (dates[dates.length - 1] < end) {
      dates.push(new Date(year, month, ++day));
    }
    this.periodicDates = dates;
    for (let i = 0; i < this.periodicDates.length; i++) {
      temp.push(new Date(this.periodicDates[i]).getFullYear() + "-" + (new Date(this.periodicDates[i]).getMonth() + 1) + "-" + new Date(this.periodicDates[i]).getDate())
    }
    this.periodicDates = temp;
    temp.splice(temp.length - 1, 1);
    this.weekDatesArray = temp;
    this.getWeeklyCalendar(this.weekObject);
  }

  weekAttendanceNew: any = [];
  NewWeekAttendance = [];
  weekObject: any = {};
  weekAttMonth = new Date();
  getWeeklyCalendar(obj) {
    this.NewWeekAttendance = [];
    this.weekAttendanceNew = new Array();
    this.DshServic.getWeeklyAttendance(obj).subscribe(data => {
      this.weekAttendanceNew = data.result;
      let flag: boolean = false;
      for (let j = 0; j < this.weekDatesArray.length; j++) {
        flag = false;
        let obj = {};
        for (let i = 0; i < this.weekAttendanceNew.length; i++) {
          obj = {
            attenDate: this.weekDatesArray[j],
            attendanceStatus: "A",
            checkIn: null,
            checkOut: null,
            deptName: this.weekAttendanceNew[i].deptName,
            empId: this.weekAttendanceNew[i].empId,
            empTypeName: this.weekAttendanceNew[i].empTypeName,
            fullName: this.weekAttendanceNew[i].fullName,
            locationName: this.weekAttendanceNew[i].locationName,
            otduration: null,
            shiftName: null,
            statusOfDay: null,
            workDuration: null
          }
          if (new Date(new Date(new Date(this.weekAttendanceNew[i].attenDate).setHours(0)).setMinutes(0)).getTime() === new Date(new Date(new Date(this.weekDatesArray[j]).setHours(0)).setMinutes(0)).getTime()) {
            this.NewWeekAttendance.push(this.weekAttendanceNew[i]);
            flag = true;
            break;
          }
        }
        if (!flag) {
          this.NewWeekAttendance.push(obj);
        }
      }
      this.weekAttMonth = this.NewWeekAttendance[0].attenDate;
      for (let i = 0; i < this.weekAttendanceNew.length; i++) {
        this.weekAttendanceNew[i].workDuration = this.timeConvert(this.weekAttendanceNew[i].workDuration);
      }
    }, err => {
      this.NewWeekAttendance = [];
      for (let j = 0; j < this.weekDatesArray.length; j++) {
        let obj = {
          attenDate: this.weekDatesArray[j],
          attendanceStatus: "A",
          checkIn: null,
          checkOut: null,
          deptName: null,
          empId: null,
          empTypeName: null,
          fullName: null,
          locationName: null,
          otduration: null,
          shiftName: null,
          statusOfDay: null,
          workDuration: null
        }
        this.NewWeekAttendance.push(obj);
      }
      this.weekAttMonth = this.NewWeekAttendance[0].attenDate;
    })
  }

  getWeeklyCalendar1(obj) {
    this.NewWeekAttendance = [];
    this.weekAttendanceNew = new Array();
    this.DshServic.getWeeklyAttendance(obj).subscribe(data => {
      this.NewWeekAttendance = data.result;
    }, err => {

    })
  }
  //=========================Weekly Calendar======================
  //==========================Location Start=========================

  onItemSelectL(item: any) {
    if (this.locationMaster.length == 0) {
      this.locationMaster.push(item);
    } else {
      this.locationMaster.push(item);
    }
  }
  onSelectAllL(items: any) {
    this.locationMaster = items;
  }
  OnItemDeSelectL(item: any) {
    for (var i = 0; i < this.locationMaster.length; i++) {
      if (this.locationMaster[i].locationId == item.locationId) {
        this.locationMaster.splice(i, 1)
      }
    }
  }
  onDeSelectAllL(item: any) {
    this.locationMaster = [];
  }
  postLike(object) {
    this.newsFeedId = object.newsFeedId
    let url = this.baseurl + '/likeCountPost';
    let obj = {
      "newsFeedId": this.newsFeedId,
      "empPerId": this.empPerId
    }
    this.httpService.post(url, obj).subscribe(data => {
        this.postLikeCount(object);
      },
      (err: HttpErrorResponse) => {
      });

  }
  postLikeCount(object) {
    this.filPath = object.filPath;
    // this.fileType = object.fileType;
    this.filPathImage = object.filPathImage;
    // this.fileTypeImage = object.fileTypeImage;
    let url = this.baseurl + '/getlikeCount';
    let obj = {
      "newsFeedId": this.newsFeedId,
      "empPerId": this.empPerId
    }
    this.httpService.post(url, obj).subscribe((data): any => {
        this.likeCountfalg = data['result']['like'];
        if (data['result']['newsLikeResDtos']) {
          this.likearr = data['result']['newsLikeResDtos']
        }
      },
      (err: HttpErrorResponse) => {
      });

  }
  deleteNews() {
    if (this.dltnewsmodelflag == true) {
      let url = this.baseurl + '/deletenewsfeed/';
      let obj = {
        "newsFeedId": this.newsFeedId
      }
      this.httpService.delete(url + this.newsFeedId).subscribe(data => {
          this.ch.detectChanges();
          this.getallNews();
          this.toastr.success('News deleted successfully');
          this.Spinner.hide();
        },
        (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete News');
          this.Spinner.hide();
        });
    }
  }
  clear() {
    this.isedit = false;
    this.empDashModel = new EmpDashModel();
    this.myInputVariable.nativeElement.value = "";
    this.searchQuery = '';
    this.empDashModel.dateTime = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    // this.empDashModel.entryTime = new Date().getHours() + ":" + new Date().getMinutes();
    this.empDashModel.message = ''
    this.empDashModel.subject = '';
  }
  //===========================Location End======================= 
  selectedNewsId: number;
  imageBase64: string;
  fileUrlToDownload: any;
  downloadFile(data1) {
    window.open(data1.filPath);
  }
  get(obj) {
    this.dltnewsmodelflag = false;
    this.newsFeedId = obj.newsFeedId;
    this.imageBase64 = obj.imageBase64;
    this.selectedNewsId = obj.newsFeedId;
    this.selectedNews = [];
    this.selectedNews.push(obj)
    this.desname = obj.descName;
    this.postLikeCount(obj)
    this.from = obj.fullName;
  }
  HideBirthdayflag: boolean
  TodayEMPBirthday() {
    let url = this.baseurl + '/ToDaysBday/';
    this.httpService.get(url + this.loggedUser.locationId).subscribe(data => {
        this.loggedUserEmpPerID = this.loggedUser.empPerId
        this.TodaysBirthday = data["result"];
        for (let index = 0; index < this.TodaysBirthday.length; index++) {
          if (this.TodaysBirthday[index].empPerId == this.loggedUser.empPerId) {
            this.TodaysBirthday.splice(index, 1);
          }
          this.ProfilePath = this.TodaysBirthday[index].profilePath;
          this.todayBadyEmpPerID = this.TodaysBirthday[index].empPerId;
        }
        if (this.TodaysBirthday.length > 0) {
          this.newsFlag = true;
        }
      },
      (err: HttpErrorResponse) => {
      });

  }
  Pending() {
    let url = this.baseurl + '/PendingRequest/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.PendingReq = data["result"];
        this.PendingLeaveReq = data['result']['reqLeaveResDto'];
        this.PendingCoffReq = data['result']['reqCoffResDto'];
        this.PendingExtraWorkReq = data['result']['extraWorking'];
        this.PendigWorkFromHome = data['result']['reqWFOResDtos'];
        this.PendingLateEarly = data['result']['requestLateEarlyResDtoList'];
        this.PendingMissPunch = data['result']['requestMissPunchReqDtos'];
        this.PendingOnDuty = data['result']['requestODResDtos'];
        for (let i = 0; i < this.PendingOnDuty.length; i++) {
          this.PendingOnDuty[i].fromDate = new Date(this.PendingOnDuty[i].fromDate).getTime();
          this.PendingOnDuty[i].toDate = new Date(this.PendingOnDuty[i].toDate).getTime();
        }
        this.PendingPermReq = data['result']['requestPRResDto'];
      },
      (err: HttpErrorResponse) => {
        if (err) {
          this.Pendingimgflag = true
        }
      });
  }
  LateSitting() {
    let url = this.baseurl + '/getLateSitting/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.LateSittigData = data["result"];
      },
      (err: HttpErrorResponse) => {
        if (err) {
          this.imgflag = true
        }
      });
  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    this.postImage(file.item(0));
  }
  returnImageObject: any = {};
  postprofileimage(files) {
    this.DshServic.postFile(this.fileToUpload).subscribe((data: any) => {
        this.filPathImage = JSON.parse(data._body).result.filPathImage;
        this.fileTypeImage = JSON.parse(data._body).result.fileTypeImage;
        this.returnImageObject = JSON.parse(data._body).result;
        files.value = null;
      }
    );
  }
  handleFilesInput(file: FileList) {
    this.filesToUpload = file.item(0);
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.filesToUpload);
    this.postFile(file.item(0));
  }
  ppostFile(fileToUpload) {
    let url = this.baseurl + '/uploadFile';
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint, formData).map(x => x);
  }
  postFile(files) {
    this.ppostFile(files).subscribe((data: any) => {
        this.filPath = JSON.parse(data._body).result.imageUrl;
        this.fileType = this.fileToUpload.type;
        this.filesToUpload = null;
        this.returnImageObjectFile = JSON.parse(data._body).result;
      }
    );
  }

  postImage(files) {
    this.ppostFile(files).subscribe((data: any) => {
        this.filPathImage = JSON.parse(data._body).result.imageUrl;
        this.fileTypeImage = this.fileToUpload.type;
        this.returnImageObjectFile = JSON.parse(data._body).result;
      }
    );
  }
  getAttendanceOfMonth(month, year) {
    debugger
    let obj = {
      "labourMasterId": this.empOfficialId,
      "month": month,
      "year": year
    }
    console.log('======', obj);
    
    
      this.DshServic.monthlyAttendance(obj).subscribe(data => {
        
        console.log('data result',data)
      this.monthAttendance = data.result;
      console.log('this month data',this.monthAttendance)
      var curr = new Date; // get current date
      var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
      var last = first + 6; // last day is the first day + 6
      var firstday = new Date(curr.setDate(first)).toUTCString();
      var lastday = new Date(curr.setDate(last)).toUTCString();
      this.weekAttendance = new Array();
      for (let i = 0; i < this.monthAttendance.length; i++) {
        if (new Date(this.monthAttendance[i].attenDate).getTime() > new Date(firstday).getTime() && new Date(this.monthAttendance[i].attenDate).getTime() < new Date(lastday).getTime()) {
          this.weekAttendance.push(this.monthAttendance[i]);
        }

      }
      this.getDaysOfMonth();
    }, (err => {
      this.monthAttendance = []
      this.getDaysOfMonth();
    })
  )}
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }
    let ServiceAttendanceDate = this.calendarService.getCalendarAttendance(this.date, this.monthAttendance, this.HolidayData, this.currentYear);
    this.daysInThisMonth = ServiceAttendanceDate.daysInThisMonth;
    this.daysInLastMonth = ServiceAttendanceDate.daysInLastMonth;
    this.daysInNextMonth = ServiceAttendanceDate.daysInNextMonth;
  }
  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    var ohours;
    var ominutes;
    if (rhours < 10) {
      ohours = "0" + rhours;
    }
    else {
      ohours = rhours;
    }

    if (rminutes < 10) {
      ominutes = "0" + rminutes;
    }
    else {
      ominutes = rminutes;
    }
    return ohours + ":" + rminutes;
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
    this.payrollM = (this.date.getMonth() + 1) + "-" + this.date.getFullYear();
    this.getMonthLockData();
    this.PostPay();
    this.shiftAllowns()
    // this.getDaysOfMonth();
    this.selectedMonthYear();
    // this.getSelfcertificationStatus()
  }
  goToNextMonth() {

    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getAttendanceOfMonth(this.date.getMonth() + 1, this.date.getFullYear());
    // this.payPeriodDays(startDate, lastDate);
    this.payrollM = (this.date.getMonth() + 1) + "-" + this.date.getFullYear();
    this.getMonthLockData();
    this.PostPay();
    this.shiftAllowns()
    this.selectedMonthYear();
    // this.getSelfcertificationStatus()
  }
  payPeriodDays(start, end) {
    this.payStart = start;
    this.payEnd = end;
    this.startDatefor = new Date(this.payStart).getFullYear() + "-" + (new Date(this.payStart).getMonth() + 1) + "-" + new Date(this.payStart).getDate();
    this.payEndfor = new Date(this.payEnd).getFullYear() + "-" + (new Date(this.payEnd).getMonth() + 1) + "-" + new Date(this.payEnd).getDate();
  }

  selectedMonthYear() {
    var currentM = this.date.getMonth() + 1;
    var currentY = this.date.getFullYear();
    var pay = currentM + '-' + currentY;
    this.payrollM = pay;
    // this.getMonthLockData();
    // this.getSelfcertificationData();
    // this.getSelfCertificationPendingData();
  }
  //=============Cancel From Calendar Starts==============
  odListOfDay = [];
  leaveListOfDay = [];
  lateEarlyDays = [];
  coffListdays = [];
  request: string = "";
  item: any = {};
  //=========Od starts============
  cancelOd(item) {
    this.modelFlag = true;
    // this.editOnDuty(item);
    this.request = "OD";
    this.item = item;
    this.OnDutyObj = item.requestApproveODId;
  }
  getOdCancelDate(item) {
    let obj = {
      empPerId: this.empPerId,
      fromDate: item.date
    }
    this.odListOfDay = new Array();
    this.DshServic.getOnDutyOfDate(obj).subscribe(data => {

      this.odListOfDay = data.result;
    }, err => {
    })
  }
  //=========Od ends============

  //=========coff starts vinod============

  cancelCoff(item) {
    this.request = 'CO';
    this.item = item;
  }

  getCoffCancelDate(days) {
    let obj = {
      empPerId: this.empPerId,
      dateAndTime1: days.date
    }
    this.coffListdays = new Array();
    //  this.item = days;
    this.DshServic.getCoffDate(obj).subscribe(data => {
      this.coffListdays = data.result;
    }, err => {
    })
  }
  //=========coff ends============

  //=========MissPunch starts============

  cancelMissPunch(item) {
    this.request = 'MP';
    this.item = item;
  }
  //=========MissPunch ends============
  //=========LateEarly starts vinod============
  cancelLateEarly(item) {
    this.request = 'LTE';
    this.item = item;
  }
  geLateEarlyCancelDate(days) {
    let obj = {
      empPerId: this.empPerId,
      dateAndTime1: days.date
    }
    this.selectedDate = new Date(days.day + "-" + this.currentMonth + "-" + this.currentYear);
    this.requestDate = this.selectedDate;
    this.lateEarlyDays = new Array();
    this.DshServic.getLateEarlycancelDate(obj).subscribe(data => {
      this.lateEarlyDays = data.result;

    //   if (new Date().getTime() < new Date(this.selectedDate).getTime()) {
    //     this.futureDatesFlagMissPunch = true;
    //   }
    //   else {
    //     this.futureDatesFlagMissPunch = false;
    //   }
    //   this.requestDate = this.selectedDate;
      
    // }
      // this.applyLeave(days)
    }, err => {
    })
    this.selectedDate =days.date
    // this.selectedDate = new Date(day.day + "-" + this.currentMonth + "-" + this.currentYear);
    if (new Date().getTime() < new Date(this.selectedDate).getTime()) {
      this.futureDatesFlagMissPunch = true;
    }
    else {
      this.futureDatesFlagMissPunch = false;
    }
    this.requestDate = this.selectedDate;
    
  
  }
  //=========LateEarly ends============
  //=========WorkFromHome starts============
  cancelWorkFromHome(item) {
    this.request = 'WFH';
    this.item = item;
  }
  //=========WorkFromHome ends============

  //=========Extra Work starts============
  cancelExtraWork(item) {
    this.request = 'EWR';
    this.item = item;
  }
  //=========Extra Work ends============
  //=========Permission starts============
  cancelPermission(item) {
    this.request = 'PR';
    this.item = item;
  }
  //=========Permission ends============
  //=========leave starts============
  cancelLeave(item) {
    this.item = item;
    this.request = 'L';
  }
  getLeaveCancelDate(item) {
    let obj = {
      empPerId: this.empPerId,
      startDate: item.date
    }
    this.leaveListOfDay = new Array();
    this.DshServic.getLeaveOfDate(obj).subscribe(data => {
      this.leaveListOfDay = data.result;
    }, err => {
    })
  }
  //=========leave ends============
  cancelRequests() {
    if (this.request == "OD") {
      this.updateOdFinal(this.item.requestApproveODId);
    }
    else if (this.request == "L") {
      this.updateLeaveFinal(this.item);
    }
    else if (this.request == "CO") {
      this.updateCoffFinal(this.item.requesCoffApproveId);
    }
    else if (this.request == "LTE") {
      this.updateLateEarlyFinal(this.item.requestApproveLateEarlyId);
    }
    else if (this.request == "MP") {
      this.updateMissPunchFinal(this.item.requestApproveMissPunchId);
    }
    else if (this.request == "WFH") {
      this.updateWorkFromHomeFinal(this.item.requestApproveWFHId);
    }
    else if (this.request == "PR") {
      this.updatePermissionFinal(this.item.requestApprovePRId);
    }
    else if (this.request == "EWR") {
      this.updateExtraWorkFinal(this.item.extraWorkingApproveRequestID);
    }
  }

  updateOdFinal(id) {
    let obj = {
      requestApproveODId: id
    }
    this.DshServic.updateOd(obj).subscribe(data => {
      this.toastr.success("On Duty cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel On Duty");
    })
  }
  updateLeaveFinal(item) {
    this.DshServic.updateLeave(item.leaveApproveRequestId).subscribe(data => {
      this.toastr.success(item.leaveCode + " cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel " + item.leaveCode);
    })
  }
  updateCoffFinal(id) {
    let obj = {
      requesCoffApproveId: id
    }
    this.DshServic.updatecoff(id).subscribe(data => {
      this.toastr.success("C-Off cancelled successfully");

    }, err => {
      this.toastr.error("Failed to cancel C-Off..!");
    })
  }
  updateExtraWorkFinal(id) {
    this.DshServic.updateExtraWork(id).subscribe(data => {
      this.toastr.success("Extra Work request cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel Extra Work request");
    })
  }
  updateMissPunchFinal(id) {
    let obj = {
      requestApproveMissPunchId: id
    }
    this.DshServic.updateMissPunch(obj).subscribe(data => {
      this.toastr.success("Miss Punch cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel Miss Punch request");
    })
  }
  updateLateEarlyFinal(id) {
    let obj = {
      requestApproveLateEarlyId: id
    }
    this.DshServic.updateLateEarly(obj).subscribe(data => {
      this.Approval();
      this.toastr.success("Late/Early cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel Late/Early");
    })
  }
  updateWorkFromHomeFinal(id) {
    let obj = {
      requestApproveWFHId: id
    }
    this.DshServic.updateWorkFromHome(obj).subscribe(data => {
      this.toastr.success("Work From Home cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel Work From Home");
    })
  }
  updatePermissionFinal(id) {
    let obj = {
      requestApprovePRId: id
    }
    this.DshServic.updatePermission(obj).subscribe(data => {
      this.toastr.success("Permission request cancelled successfully");
    }, err => {
      this.toastr.error("Failed to cancel permission request");
    })
  }
  //=============Cancel From Calendar Ends==============

  //=============Apply From Calendar Starts====================
  selectedDate: any;
  requestDate: any;
  futureDatesFlagMissPunch: boolean = false;
  getApplyDate(day) {
    
    this.selectedDate =day.date
    // this.selectedDate = new Date(day.day + "-" + this.currentMonth + "-" + this.currentYear);
    if (new Date().getTime() < new Date(this.selectedDate).getTime()) {
      this.futureDatesFlagMissPunch = true;
    }
    else {
      this.futureDatesFlagMissPunch = false;
    }
    this.requestDate = this.selectedDate;
    
  }
  applyLeave(val) {
    
    if (val == 'LR') {
      sessionStorage.setItem('requestedDate', this.requestDate);
      sessionStorage.setItem('from', 'yearly');
      this.router.navigateByUrl('layout/leaverequest/leaverequest');
    }
    else {
      sessionStorage.setItem('from', 'yearly');
      sessionStorage.setItem('requestedDate', this.requestDate);
      sessionStorage.setItem('requestedType', val);
      this.router.navigateByUrl('/layout/otherrequests/otherrequests');
    }
  }
  //=============Apply From Calendar Ends====================

  selectDate(day) {
    this.currentDate = day;
  }
  dltmodelnewsEvent() {
    this.Spinner.show();
    this.dltnewsmodelflag = true;
    this.deleteNews();
  }
  dltmodelEvent() {
    this.dltmodelFlag = true;
    this.deleteWorkFrom(this.dltWorkFromObj);
    this.deletePerReq(this.dltPendingPermissionReq);
    this.DeleteMissPunch(this.dltMissPunchObj);
    this.deleteOnDuty(this.OnDutyObj);
    this.deleteLateEarly(this.dltLateEarlyObj);
    this.deleteCoff(this.dltCoffObj);
  }
  //=========================delete coff start vinod======================
  dltcoff(object) {
    this.dltCoffObj = object.requesCoffIdId
    // this.deleteLateEarly( this.dltLateEarlyObj);
    this.dltmodelFlag = false
  }
  deleteCoff(dltCoffObj) {
    if (this.dltmodelFlag == true) {
      this.DshServic.deleteCoffRequest(this.dltCoffObj).subscribe(data => {
        this.toastr.success('C-Off request deleted successfully');
      }, err => {
        this.toastr.error('Failed to delete C-Off request');
      });

    }
  }
  //=========================delete coff end======================
  editWorkFrom(object) {
    {
      this.modelFlag = false
      this.requestWFHId = object.requestWFH
      this.updateWorkFrom(this.requestWFHId);
    }
  }
  updateWorkFrom(lateEarlyobj) {
    if (this.modelFlag == true) {
      this.empDashModel.requestWFHId = this.requestWFHId
      let url1 = this.baseurl + '/cancelWFHRequest';
      let obj = {
        requestWFHId: this.requestWFHId
      }
      this.httpService.post(url1, obj).subscribe(data => {
          this.ch.detectChanges();
          this.Pending();
        }, (err: HttpErrorResponse) => {
        });
    }
  }
  DltWorkFrom(object) {
    this.dltmodelFlag = false
    this.dltWorkFromObj = object.requestWFH
  }
  deleteWorkFrom(dleworkfromobj) {
    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/RequestWFH/';
      this.empDashModel.requestWFH = this.dltWorkFromObj
      this.httpService.delete(url1 + this.dltWorkFromObj).subscribe(data => {
          this.ch.detectChanges();
          this.toastr.success('Work From Home request deleted successfully');
          this.Pending();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete Work From Home request');
        });
    }
  }
  editMissPunch(object) {
      this.MissPunchObj = object.requesMissPunchId;
      this.updateMissPunch(this.MissPunchObj);
      this.modelFlag = false;
  }
  updateMissPunch(MissPunchObj) {
    if (this.modelFlag == true) {
      let url1 = this.baseurl + '/cancelMissPunchRequest';
      this.empDashModel.requesMissPunchId = this.MissPunchObj
      let obj = {
        requesMissPunchId: this.MissPunchObj
      }
      this.httpService.post(url1, obj).subscribe(data => {
          this.ch.detectChanges();
          this.Pending();
        }, (err: HttpErrorResponse) => {
        });
    }
  }
  dltMissPunch(object) {
      this.dltmodelFlag = false;
      this.dltMissPunchObj = object.requesMissPunchId;
  }
  DeleteMissPunch(dltMissPunchObj) {
    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/RequestMissPunch/';
      this.empDashModel.requesMissPunchId = this.dltMissPunchObj
      this.httpService.delete(url1 + this.dltMissPunchObj).subscribe(data => {
          this.ch.detectChanges();
          this.toastr.success('Miss Punch request deleted successfully');
          this.Pending();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete Miss Punch request');
        });
    }
  }
  editOnDuty(object) {
    this.modelFlag = false;
    this.OnDutyObj = object.requesODId;
    this.updateOnDuty(this.OnDutyObj);
  }
  updateOnDuty(OnDutyObj) {
    if (this.modelFlag == true) {
      let url1 = this.baseurl + '/cancelODRequest';
      this.empDashModel.requesODId = this.OnDutyObj
      let obj = {
        requesODId: this.OnDutyObj
      }
      this.httpService.post(url1, obj).subscribe(data => {
          this.ch.detectChanges();
          this.Pending();
        }, (err: HttpErrorResponse) => {
        });
    }
  }
  dltOnDuty(object) {
    this.dltmodelFlag = false;
    this.dltOnDutyObj = object.requesODId;
  }
  deleteOnDuty(OnDutyObj) {
    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/RequestOD/';
      this.empDashModel.requesODId = this.dltOnDutyObj
      this.httpService.delete(url1 + this.dltOnDutyObj).subscribe(data => {
          this.ch.detectChanges();
          this.toastr.success('On Duty request deleted Successfully');
          this.Pending();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete On Duty request');
        });
    }
  }

  editLateEarly(object) {
    this.modelFlag = false;
    this.LateEarlyObj = object.requesLateEarlyId;
    this.updateLateEarly(this.LateEarlyObj);
  }
  updateLateEarly(LateEarlyObj) {
    if (this.modelFlag == true) {
      let url1 = this.baseurl + '/cancelLateEarlyRequest';
      this.empDashModel.requesLateEarlyId = this.LateEarlyObj
      let obj = {
        requesLateEarlyId: this.LateEarlyObj
      }
      this.httpService.post(url1, obj).subscribe(data => {
          this.ch.detectChanges();
        }, (err: HttpErrorResponse) => {
        });
    }
  }
  dltLateEarly(object) {
    this.dltLateEarlyObj = object.requesLateEarlyId;
    this.dltmodelFlag = false;
  }
  deleteLateEarly(LateEarlyObj) {
    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/RequestLateEarly/';
      this.empDashModel.requesLateEarlyId = this.dltLateEarlyObj;
      this.httpService.delete(url1 +this.dltLateEarlyObj).subscribe(data => {
          this.ch.detectChanges();
          this.toastr.success('Late/Early request deleted Successfully');
          this.Pending();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete Late/Early request');
        });
    }
  }
  editPermissionReq(object) {
    this.modelFlag = false;
    this.PendingPermissionReq = object.requestPRId;
    this.updatePermissionReq(this.PendingPermissionReq);
  }
  updatePermissionReq(PendingPermissionReq) {
    if (this.modelFlag == true) {
      let url1 = this.baseurl + '/cancelPrRequest';
      this.empDashModel.requestPRId = this.PendingPermissionReq;
      let obj = {
        requestPRId: this.PendingPermissionReq
      }
      this.httpService.post(url1, obj).subscribe(data => {
          this.ch.detectChanges();
          this.Pending();
        }, (err: HttpErrorResponse) => {
        });
    }
  }
  DltPerReq(object) {
      this.dltmodelFlag = false;
      this.dltPendingPermissionReq = object.requestPRId;
  }
  deletePerReq(dleworkfromobj) {
    if (this.dltmodelFlag == true) {
      let url1 = this.baseurl + '/RequestPR/';
      this.empDashModel.requestPRId = this.dltPendingPermissionReq
      this.httpService.delete(url1 + this.dltPendingPermissionReq).subscribe(data => {
          this.ch.detectChanges();
          this.toastr.success('Permission request deleted Successfully');
          this.Pending();
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete Permission request');
        });
    }
  }

  Approval() {
    let url = this.baseurl + '/RequestApprove/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.ApprovalData = data["result"];
        this.WorkFromHome = data['result']['requestWFHApproveRes'];
        let WorkFromHome = data['result']['requestWFHApproveRes'];
        // <!-- ===================starting point================= -->
        if(WorkFromHome){
          for (let i = 0; i < WorkFromHome.length; i++) {
            WorkFromHome[i].forDate = new Date(WorkFromHome[i].forDate).getTime();
            WorkFromHome[i].toDate = new Date(WorkFromHome[i].toDate).getTime();
          }
        }          
          this.LateEarly = data['result']['requestLateEarlyApproveRes'];               
        this.MissPunch = data['result']['requestMissPunchApproveRes'];
        this.OnDuty = data['result']['requestODApproveRes'];
       if(this.OnDuty){
          for (let i = 0; i < this.OnDuty.length; i++) {
            this.OnDuty[i].fromDate = new Date(this.OnDuty[i].fromDate).getTime();
            this.OnDuty[i].toDate = new Date(this.OnDuty[i].toDate).getTime();
            
          }
        }
        // <===================ending point=================>
        this.PermissionReq = data['result']['requestPRApproveRes']
        this.leaves = data['result']['leaveRequestApproveRes'];
        this.coffApproval = data['result']['coffRequestApproveRes'];
        this.extraWorkApproval = data['result']['extraWorkingApproveResDtos'];
       
      },
      (err: HttpErrorResponse) => {
      });

  }
  postNews(f) {
    this.Spinner.show()
    if (this.editnwesflag == false) {
      if (this.newsModel.newsBy == 'BySelf') {
        this.newsModel.compId = this.compId
        this.newsModel.empPerId = this.searchEmpPerId;
        this.newsModel.fromId = this.loggedUser.empPerId;
        // console.log("fromID123",     this.newsModel.empPerId);
        // console.log("fromID@",   this.newsModel.fromId);
        this.newsModel.filPath = this.filPath;
        this.newsModel.fileType = this.fileType;
        this.newsModel.filPathImage = this.filPathImage;
        this.newsModel.fileTypeImage = this.fileTypeImage;
        this.DshServic.postnews(this.newsModel).subscribe(data => {
            this.Spinner.hide();
            f.reset();
            this.ch.detectChanges();
            this.getallNews();
            this.newsModel = new NewsModel();
            this.returnImageObject.filPath = '';
            this.returnImageObject.fileType = '';
            this.returnImageObject.fileName = '';
            this.fileTypeImage = '';
            this.filPathImage = '';
            this.fileName = "";
            this.imageName = "";
            this.myInputVariable.nativeElement.value = "";
            this.searchQuery = '';
       
            this.ch.detectChanges();
            this.newsModel.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            this.toastr.success('News added successfully');
            this.ch.detectChanges();
            $("#file1").val('');
            $("#file").val('');
            this.newsModel = new NewsModel();
          }, (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Failed to add News');
          });
      } else {
        this.newsModel.compId = this.compId;
        this.newsModel.empPerId = this.loggedUser.empPerId
        this.newsModel.filPath = this.filPath;
        this.newsModel.fileType = this.fileType;
        this.newsModel.filPathImage = this.filPathImage;
        this.newsModel.fileTypeImage = this.fileTypeImage;
        this.DshServic.postnews(this.newsModel).subscribe(data => {
            this.Spinner.hide();
            f.reset();
            this.ch.detectChanges();
            this.getallNews();
            this.newsModel = new NewsModel();
            this.fileTypeImage = '';
            this.filPathImage = '';
            this.fileName = "";
            this.imageName = "";
            this.returnImageObjectFile.filPath = '';
            this.returnImageObjectFile.fileType = '';
            this.returnImageObjectFile.fileName = '';
            this.ch.detectChanges();
            this.newsModel.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            this.ch.detectChanges();
            this.toastr.success('News added successfully');
            $("#file1").val('');
            $("#file").val('');
          }, (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Failed to add News');
          });
      }
    } else {
      if (this.newsModel.newsBy == 'BySelf') {
        this.newsModel.compId = this.compId;
        this.newsModel.empPerId = this.searchEmpPerId;
        this.newsModel.filPath = this.filPath;
        this.newsModel.fileType = this.fileType;
        this.newsModel.fileName = this.returnImageObjectFile.fileName;
        this.newsModel.filPathImage = this.filPathImage;
        this.newsModel.fileTypeImage = this.fileTypeImage;
        this.newsModel.newsFeedId = this.geteditnwesobj
        let url1 = this.baseurl + '/editNewsFeed';
        this.httpService.put(url1, this.newsModel).subscribe(data => {
            this.Spinner.hide();
            f.reset();
            this.getallNews();
            this.empDashModel = new EmpDashModel()
            this.ch.detectChanges();
            this.fileName = "";
            this.imageName = "";
            this.toastr.success('News updated successfully');
            $("#file1").val('');
            $("#file").val('');

          }, (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Failed to update News');
          });
      }
      else {
        this.newsModel.compId = this.compId;
        this.newsModel.empPerId = this.empPerId;
        this.newsModel.filPath = this.filPath;
        this.newsModel.fileType = this.fileType;
        this.newsModel.filPathImage = this.filPathImage;
        this.newsModel.fileTypeImage = this.fileTypeImage;
        this.newsModel.newsFeedId = this.geteditnwesobj
        let url1 = this.baseurl + '/editNewsFeed';
        this.httpService.put(url1, this.newsModel).subscribe(data => {
            this.Spinner.hide();
            f.reset();
            this.getallNews();
            this.empDashModel = new EmpDashModel()
            this.ch.detectChanges();
            this.fileName = "";
            this.imageName = "";
            this.toastr.success('News updated successfully');
            $("#file1").val('');
            $("#file").val('');

          }, (err: HttpErrorResponse) => {
            this.Spinner.hide();
            this.toastr.error('Failed to update News');
          });
      }
    }
  }
  getallNews() {

    var todayDt=new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
       //console.log("todate",todayDt);
  
  

    this.locoid = this.loggedUser.locationId
    let url = this.baseurl + '/RequestNewsFeed/';
    this.httpService.get(url + this.locoid).subscribe(data => {
       
        this.TodaysNews = data["result"];
     //console.log("datanews*****",  this.TodaysNews );
          
        // console.log("displayid1",this.TodaysNews[i].empPerId );
        // console.log("displayid2",this.loggedUser.empPerId );
      
        // for (let i = 0; i < this.TodaysNews.length; i++) {
       
        //    if(this.TodaysNews[i].empPerId ==this.loggedUser.empPerId){
          
        //     this.allnewsData.push(this.TodaysNews[i])
          
        //     //console.log("this is selected data",this.allnewsData)
                
          

        //    }
          

        //   }






        //   this.datedata  = this.TodaysNews[i].fromDate
        //   console.log("fromdate123",   this.datedata );
        //   this.fromdate1=(moment(this.datedata ).format('YYYY-MM-DD'));
        //   console.log("fromdate%%%%%",this.fromdate1);

          
        //     this.newsFeedId =this.TodaysNews[i].newsFeedId;
        //     console.log("fromdate345",   this.newsFeedId);
            
          
      
          
         
        // this.TodaysNews.filter(item=>{
        //   if(item.newsFeedId){
        //     if(item.fromDate1<=todayDt)
        //     {

        //       item['tempValue']=false;
        //     }
        //     else{
        //       item['tempValue']=true;
        //     }
        //   }
        // })
       this.ch.detectChanges();
        if (this.TodaysNews.length == 0) {
          this.newsFlag = true;
        }
        else {
          this.newsFlag = false;
        }
        if (data['result'][0]) {
          this.likeCountfalg = data['result'][0]['like'];

        }

      },
      (err: HttpErrorResponse) => {
        this.newsFlag = true
      });
     


  }
  selectClickLocation() {
    
    console.log("locationdataemp",this.newsModel.locationMaster.length);
    if (this.newsModel.locationMaster.length == 0) {
      this.locationFlag = true;
    }
    else {
      this.locationFlag = false;
    }
  }
  


  clearnews() {
    this.empFlag = false;
    this.isdelet = false;
    this.editnwesflag = false;
    this.newsModel = new NewsModel();
    this.newsModel.newsBy = "BySelf"
    this.searchQuery = '';
    this.empDashModel = new EmpDashModel();
    this.myInputVariable.nativeElement.value = "";
    this.searchQuery = '';
    this.returnImageObject.filPath = '';
    this.returnImageObject.fileType = '';
    this.returnImageObject.fileName = '';
    this.fileName = "";
    this.imageName = "";
    this.fileTypeImage = '';
    this.filPathImage = '';
    this.newsModel.fromDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.empDashModel.dateTime = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.empDashModel.entryTime = new Date().getHours() + ":" + new Date().getMinutes();
  }

  workFromHome: any;
  prCount: any;
  PostPay() {

    this.payM.empOfficialId = this.empOfficialId
    this.payM.fromDate = this.payStart
    this.payM.toDate = this.payEnd;
    this.payM.empPerId = this.empPerId;
    this.payM.empTypeId = this.loggedUser.empTypeId;
    this.payM.subEmpTypeId=this.loggedUser.subEmpTypeId;
    this.PresentDay = 0;
    this.absentDays = 0;
    this.lateearly = 0;
    this.leave = 0;
    this.leavecoff = 0;
    this.onduty = 0;
    this.overTimeHours = 0;
    this.paidDays = 0;
    this.shortHours = 0;
    this.weekofholydays = 0;
    this.workFromHome = 0;
    this.prCount = 0;
    this.arrears = 0;
    this.DshServic.postPay(this.payM).subscribe(data => {
        this.PresentDay = data['result']['presentDays'];
        this.absentDays = data['result']['absentDays'];
        this.lateearly = data['result']['lateearly'];
        this.leave = data['result']['leave'];
        this.leavecoff = data['result']['leavecoff'];
        this.onduty = data['result']['onduty'];
        this.overTimeHours = data['result']['overTimeHours'];
        this.paidDays = data['result']['paidDays'];
        this.shortHours = data['result']['shortHours'];
        this.weekofholydays = data['result']['weekofholydays'];
        this.workFromHome = data['result']['workFromHome'];
        this.prCount = data['result']['prCount'];
        this.arrears = data['result']['arrearsCount'];
        //this.getallNews();
        this.ch.detectChanges();
      }, (err: HttpErrorResponse) => {
        this.PresentDay = 0;
        this.absentDays = 0;
        this.lateearly = 0;
        this.leave = 0;
        this.leavecoff = 0;
        this.onduty = 0;
        this.overTimeHours = 0;
        this.paidDays = 0;
        this.shortHours = 0;
        this.weekofholydays = 0;
        this.workFromHome = 0;
        this.prCount = 0;
        this.arrears = 0;
      });
  }
  postTodo(f) {
    this.Spinner.show();
    if (!this.isedit) {
      this.empDashModel.empPerId = this.empPerId;
      this.DshServic.postToDoMaster(this.empDashModel).subscribe(data => {
          this.Spinner.hide();
          this.Todolist()
          this.ch.detectChanges();
          this.Spinner.hide();
          this.empDashModel.message = "";
          f.reset();
          this.toastr.success('To-do task added successfully');
          this.myInputVariable.nativeElement.value = "";
          this.empDashModel = new EmpDashModel();
          setTimeout(() => {
            this.empDashModel.dateTime = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
          }, 100);
          this.ch.detectChanges();
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Failed to add To-do task');

        });
    } else {
      this.empDashModel.empPerId = this.empPerId
      let url1 = this.baseurl + '/ToDoList';
      this.empDashModel.toDoListMasterId = this.gotObj;
      this.httpService.put(url1, this.empDashModel).subscribe(data => {
          this.Spinner.hide();
          this.Todolist()
          this.empDashModel = new EmpDashModel()
          this.ch.detectChanges();
          f.reset();
          this.toastr.success('To-do task updated successfully');
          setTimeout(() => {
            this.empDashModel.dateTime = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
          }, 100);
        }, (err: HttpErrorResponse) => {
          this.Spinner.hide();
          this.toastr.error('Failed to update To-do task');
        });
    }
  }

  ngValue(event) {
    this.selectedCompanyobj = parseInt(event.target.value)
    this.newsModel.empPerId = this.selectedCompanyobj;
  }
  postnews(f) {
    this.Spinner.show();
    this.DshServic.postToDoMaster(this.empDashModel).subscribe(data => {
        this.Spinner.hide()
        this.ch.detectChanges();
        f.reset();
        this.Todolist();
        this.getallNews();
        this.GetFilePath = ['result']['filPath']
        this.ch.detectChanges();

      }, (err: HttpErrorResponse) => {
        this.Spinner.hide();
      });

  }
  modelEvent() {
    this.modelFlag = true;
    this.updateLateEarly(this.lateEarlyObj);
    this.updateMissPunch(this.MissPunchObj)
    this.updateOnDuty(this.OnDutyObj)
    this.updatePermissionReq(this.PermissionReq)
    this.updateWorkFrom(this.requestWFHId);
  }
  delet(object) {
    this.DeletgotObj = object.toDoListMasterId;
    this.todoDelt = false;
  }
  DeletemodelEvent() {
    this.todoDelt = true;
    this.DleteTodo(this.DeletgotObj);
  }
  DleteTodo(DeletgotObj) {
    if (this.todoDelt = true) {
      let url1 = this.baseurl + '/DeleteToDoListById/';
      this.empDashModel.toDoListMasterId = this.DeletgotObj;
      this.httpService.delete(url1 + this.DeletgotObj).subscribe(data => {
          this.ch.detectChanges();
          this.Todolist();
          this.toastr.success('To-do task deleted successfully');
        }, (err: HttpErrorResponse) => {
          this.toastr.error('Failed to delete To-do task');
        });
    }
  }
  Holidays() {
    this.HolidayData = [];
    let url = this.baseurl + '/Holiday?empOfficialId=' + this.loggedUser.empOfficialId + '&year=' + this.currentHolidayYear;
    this.httpService.get(url).subscribe(data => {
        this.HolidayData = data["result"];
        this.holiDayName = data['result']['holiDayName']
        this.startDate = new Date(data['result']['startDate']).getTime();
        for (let i = 0; i < this.HolidayData.length; i++) {
          if (new Date(this.HolidayData[i].startDate).getTime() >= new Date().getTime()) {
            this.weekAttendance.push(this.monthAttendance[i]);
            this.HolidayData[i].status = 'greater'
          }
          else {
            this.HolidayData[i].status = 'less'
          }
        }
      },
      (err: HttpErrorResponse) => {
        this.HolidayData = [];
        if (err) {
          this.HolidayFlag = true
        }
      });
  }
  TodaysJoing() {
    let url = this.baseurl + '/ToDaysJoining/';
    this.httpService.get(url + this.compId).subscribe(data => {
        this.TodaysJoingData = data["result"];
        this.fullName = data['result']['fullName']
        this.newsFlag = true;
      },
      (err: HttpErrorResponse) => {
      });
  }

  Todolist() {
    let url = this.baseurl + '/ToDoList/';
    this.httpService.get(url + this.empPerId).subscribe((data: any) => {
        this.ch.detectChanges();
        this.TodolistData = data["result"];
        this.TodolistData.sort((a, b) => new Date(a.dateTime).getTime() - new Date(b.dateTime).getTime())
        this.entryTime = data['result']['entryTime'];
        this.message = data['result']['message'];
        this.dateTime = data['result']['dateTime'];
        this.Subject = data['result']['subject'];
        this.TodoListFlag = false;
        let data1 = new Array();
        data1 = data.result;
        new Date(data1[0].entryTime);
      },
      (err: HttpErrorResponse) => {
        this.TodoListFlag = true;
      });

  }
  getObj(object) {
    this.gotObj = object.toDoListMasterId;
    this.isedit = true;
    this.TodoByID(this.gotObj);
  }
  getdltObj(object) {
    this.editnwesflag = true;
    this.geteditnwesobj = object.newsFeedId;
    this.isdelet = true;
    this.newsByID(this.geteditnwesobj);
  }
  reset() {
    this.myInputVariable.nativeElement.value = "";
  }

  imageName: string;
  fileName: string;
  newsByID(getnwesobj) {
    let url = this.baseurl + '/getnewsfeed/';
    this.httpService.get(url + getnwesobj).subscribe((data: any) => {
        this.ch.detectChanges();
        this.newsModel.fromDate = data['result']['fromDate'];
        this.newsModel.message = data['result']['message'];
        this.newsModel.newsBy = data['result']['newsBy'];
        this.newsModel.subject = data['result']['subject'];
        this.newsModel.locationMaster = data['result']['locationMaster'];
        this.newsModel.fileTypeImage = data['result']['fileTypeImage'];
        this.newsModel.filPath = data['result']['filPath'];
        this.newsModel.filPathImage = data['result']['filPathImage'];
        this.fileType = data['result']['fileType'];
        this.fileTypeImage = data['result']['fileTypeImage'];
        this.filPath = data['result']['filPath'];
        this.filPathImage = data['result']['filPathImage'];
        let imageName = data['result']['filPathImage'].lastIndexOf('/');
        this.imageName = data['result']['filPathImage'].substr(imageName + 1);
        let fileName = data['result']['filPath'].lastIndexOf('/');
        this.fileName = data['result']['filPath'].substr(fileName + 1);
        this.newsModel.fromId = data['result']['employeePerInfo']['empPerId'];
        this.searchEmpPerId = data['result']['employeePerInfo']['empPerId'];
        for (let i = 0; i < this.Allemp.length; i++) {
          if (this.Allemp[i].empPerId == data['result']['employeePerInfo']['empPerId']) {
            this.searchQuery = this.Allemp[i].fullName + " " + this.Allemp[i].lastName;
          }
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  TodoByID(gotOb) {
    let url = this.baseurl + '/ToDoListById/';
    this.httpService.get(url + gotOb).subscribe((data: any) => {
        this.ch.detectChanges();
        this.Datetime = data['result']['dateTime']
        this.finalDatetime = new Date(this.Datetime);
        this.empDashModel.dateTime = this.finalDatetime
        this.entryTime = data['result']['entryTime'].split(':');
        this.empDashModel.entryTime = this.entryTime[0] + ":" + this.entryTime[1];
        this.empDashModel.message = data['result']['message']
        this.empDashModel.dateTime = data['result']['dateTime']
        this.empDashModel.subject = data['result']['subject']
      },
      (err: HttpErrorResponse) => {
      });
  }
  Attendance() {
    let url = this.baseurl + '/EmpAttendanceDetails/';
    this.httpService.get(url + this.empPerId).subscribe((data: any) => {
        this.AttendanceData = data["result"];
        this.attendanceStatus = data['result']['attendanceStatus'];
        this.checkIn = data['result']['checkIn'];
        this.checkOut = data['result']['checkOut'];
        this.statusOfDay = data['result']['statusOfDay'];
        this.otduration = data['result']['otduration'];
        this.attenDate = data['result']['attenDate'];
        let data1 = new Array();
        data1 = data.result;
      },
      (err: HttpErrorResponse) => {
      });
  }

  CoffList() {
    let url = this.baseurl + '/CoffDetails/';
    this.httpService.get(url + this.empPerId).subscribe((data: any) => {
        this.CoffData = data["result"];
        this.extraWorkedDay = data['result']['extraWorkedDay'];
        this.extraWorkedDay = data['result']['extraWorkedType'];
        this.typeOfCredit = data['result']['typeOfCredit'];
        for (let i = 0; i < this.CoffData.length; i++) {
          this.CoffData[i].daysRemaining = this.inBetween(new Date(), new Date(this.CoffData[i].collapsDate));
        }
      },
      (err: HttpErrorResponse) => {
        if (err) {
          this.CoffFlag = true;
        }
      });
  }
  public inBetween(date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;
    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;
    // Convert back to days and return
    return Math.round(difference_ms / one_day);
  }
  applyCoff(i) {
      let id = i;
     this.InfoService.changeMessage(id);
     this.router.navigateByUrl('/layout/leaverequest/leaverequest');
  }
  allLeaveData: any = [];
  Leaves() {
    let url = this.baseurl + '/EmpLeavesBalanceDetails/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.LeavesData = data['result']['list'];
        console.log("leavesdata",this.LeavesData);
        this.balanceLeaveType = this.LeavesData[0].leaveCode;
        this.allLeaveData = data['result'];
        this.value(this.balanceLeaveType);
        this.totalLeaves = data['result']['list']['totalLeaves'];
        this.currentLeaves = data['result']['list']['currentLeaves'];
        this.leaveName = data['result']['list']['leaveName'];
        this.totalleave = data['result']['totalleave'];
        this.leaveBalance = data['result']['leaveBalance'];
        this.usedLeaves = this.LeavesData['usedLeaves'];
        this.customClass = "c100 p" + Math.floor(this.percentage) + " big green";
      },
      (err: HttpErrorResponse) => {
      });

  }
  calendarType(type) {
    this.monthlyWeekly = type;
  }
  shiftAllowns() {
    this.empDashModel.empOfficialId = this.empPerId;
    this.empDashModel.month = (new Date(this.date).getMonth()) + 1;
    this.empDashModel.year = new Date(this.date).getFullYear();
    let url = this.baseurl + '/employeeShiftAllowance';
    this.httpService.post(url, this.empDashModel).subscribe(data => {
        this.ShiftData = data['result'];
      },
      (err: HttpErrorResponse) => {
      });
  }

  filterItem(value) {
    if (!value) {
      this.empFlag = false;
    }
    if (value) {
      this.empFlag = true;
    }
  }
  searchEmpPerId: any;
  suggestThis(item) {
    this.searchQuery = item.fullName + " " + item.lastName;
    this.searchEmpPerId = item.empPerId;
    this.designation = item.descName;
    this.empFlag = false;
  }

  getAllEmployee() {
    let url = this.baseurl + '/EmployeeMasterNoticeBoradOperation/';
    this.httpService.get(url + this.compId +'/'+this.locationId).subscribe(data => {
        this.Allemp = data["result"];
       // console.log("empdata***",  this.Allemp )
        for (let i = 0; i < this.Allemp.length; i++) {
          this.Allemp[i].index = i;
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  transform(value: any, args?: any): any {
    if (!value) return null;
    if (!args) return value;
    args = args.toLowerCase();
    return value.filter(function (item) {
      return JSON.stringify(item).toLowerCase().includes(args);
    });
  }
  arrowkeyLocation = 0;
  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }
  //================edit All Requests Starts ===============
  editLeave(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'L');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/leaverequest/leaverequest');
  }
  editLTE(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'LTE');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  editWFH(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'WFH');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  editMP(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'MP');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  editOD(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'OD');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  editPR(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'PR');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  editEWR(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'EWR');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  editCO(i) {
    sessionStorage.setItem('from', 'empd');
    sessionStorage.setItem('requestedType', 'CO');
    sessionStorage.setItem('data', JSON.stringify(i));
    this.router.navigateByUrl('/layout/otherrequests/otherrequests');
  }
  //================edit All Requests Ends ===============

  //================delete All Requests Starts ===============
  toDelete: string = "";
  deleteData: any;
  deleteLeave(i) {
    this.toDelete = "L";
    this.deleteData = i;
  }
  deleteLTE(i) {
    this.toDelete = "LTE";
    this.deleteData = i;
  }
  deleteWFH(i) {
    this.toDelete = "WFH";
    this.deleteData = i;
  }
  deleteMP(i) {
    this.toDelete = "MP";
    this.deleteData = i;
  }
  deleteOD(i) {
    this.toDelete = "OD";
    this.deleteData = i;
  }
  deletePR(i) {
    this.toDelete = "PR";
    this.deleteData = i;
  }
  deleteEWR(i) {
    this.toDelete = "EWR";
    this.deleteData = i;
  }
  deleteCO(i) {
    this.toDelete = "CO";
    this.deleteData = i;
  }
  deleteRequest() {
    if (this.toDelete == "L") {
      this.DshServic.deleteLeave(this.deleteData.leaveRequestId).subscribe(data => {
        this.Pending();
        this.toastr.success("Leave request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete Leave request");
      })
    } else if (this.toDelete == "LTE") {
      this.DshServic.deleteLTE(this.deleteData, this.loggedUser.empOfficialId).subscribe(data => {
        this.Pending();
        this.toastr.success("Late/Early request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete Late/Early request");
      })

    } else if (this.toDelete == "WFH") {
      this.DshServic.deleteWFH(this.deleteData.requestWFH).subscribe(data => {
        this.Pending();
        this.toastr.success("Work From Home request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete Work From Home request");
      })
    } else if (this.toDelete == "MP") {
      this.DshServic.deleteMP(this.deleteData, this.loggedUser.empOfficialId).subscribe(data => {
        this.Pending();
        this.toastr.success("Miss Punch request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete Miss Punch request");
      })
    } else if (this.toDelete == "OD") {
      this.DshServic.deleteOD(this.deleteData.requesODId).subscribe(data => {
        this.Pending();
        this.toastr.success("On Duty request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete On Duty request");
      })

    } else if (this.toDelete == "PR") {
      this.DshServic.deletePR(this.deleteData.requestPRId).subscribe(data => {
        this.Pending();
        this.toastr.success("Permission request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete Permission request");
      })

    } else if (this.toDelete == "EWR") {
      this.DshServic.deleteEWR(this.deleteData.extraWorkingRequestID).subscribe(data => {
        this.Pending();
        this.toastr.success("Extra Work request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete Extra Work request");
      })

    } else if (this.toDelete == "CO") {
      this.DshServic.deleteCO(this.deleteData.requesCoffIdId).subscribe(data => {
        this.Pending();
        this.toastr.success("C-Off request deleted successfully");
      }, err => {
        this.toastr.error("Failed to delete C-Off request");
      })
    }

  }
  //================delete All Requests Ends ===============
  goTo() {
    this.Spinner.show();
    this.router.navigateByUrl('/layout/yearlycalendar');
    this.Spinner.hide();
  }
  //===============Holidays Change Year Starts===============
  flagH: boolean = false;
  holi() {
    this.HolidayData = [];
    this.flagH = true
  }
  holi1() {
    this.Holidays();
    this.flagH = false
  }
  attendanceReport() {
    this.router.navigateByUrl('/layout/attendencereportview');
  }
  //===============Holidays Change Year Ends===============
  value(e) {
    this.leaveCode = e;
      for (let i = 0; i < this.LeavesData.length; i++) {
        if (this.leaveCode == this.LeavesData[i].leaveCode) {
          this.used = this.LeavesData[i].usedLeaves
          this.avaliable = this.LeavesData[i].currentLeaves
          this.total = this.LeavesData[i].totalleave
          this.openingBalance = this.LeavesData[i].openingBalance
        }
        if (this.used == null) {
          this.used = 0
        }
        if (this.avaliable == null) {
          this.avaliable = 0
        }
        if (this.total == null) {
          this.total = 0
        }
        if (this.openingBalance == null) {
          this.openingBalance = 0
        }

      }
  }
  teamDataFlag: boolean = false
  getTeam() {
    this.DshServic.getEmpTeam(this.loggedUser.deptId,this.loggedUser.locationId).subscribe(data => {
      this.team = data.result
      this.teamDataFlag = true
      function compare(a, b) {
        if (a.fullName < b.fullName) {
          return -1;
        }
        if (a.fullName > b.fullName) {
          return 1;
        }
        return 0;
      }
      this.team.sort(compare);
    });
  }
  setDetails(data) {
    this.teamDeatails = data;
  }

  holidayYear: any = new Date().getFullYear() - 1;
  currentHolidayYear: any = new Date().getFullYear();
  changeYear() {
    if (this.holidayYear == new Date().getFullYear()) {
      this.holidayYear = new Date().getFullYear() - 1;
      this.currentHolidayYear = new Date().getFullYear();
    }
    else {
      this.holidayYear = new Date().getFullYear();
      this.currentHolidayYear = new Date().getFullYear() - 1;
    }
    this.Holidays();
  }
  getBadyData(i) {
    this.fullname = i.fullName;
    this.lastName = i.lastName;
    this.descName = i.descName;
    this.DeptName = i.deptName;
    this.empIdBday = i.empId;
    this.ProfilePath = i.profilePath;
    this.dateOfBirth = i.dateOfBirth;
    this.empPerId = i.empPerId;
  }
  postBdayWish(f) {
    let url = this.baseurl + '/birtdayWish';
    this.BadyWishModel.employeeName = this.loggedUser.fullName + " " + this.loggedUser.lastName;
    this.BadyWishModel.empPerId = this.empPerId;
    this.BadyWishModel.deptName = this.loggedUser.deptName.toString().trim();
    this.BadyWishModel.fromId = this.loggedUser.empPerId;
    this.httpService.post(url, this.BadyWishModel).subscribe((data): any => {
        this.BadyWishModel.birthdayWish = "Wish you happy birthday ..!";
        f.reset();
        this.toastr.success("Birthday wish sent successfully");
      },
      (err: HttpErrorResponse) => {
        this.toastr.error("Failed to send Birthday wish");
      });
  }
  clearBdayWish() {
    this.BadyWishModel.birthdayWish = "Wish you happy birthday ..!";
  }
  getWorkAnniversary() {
    let url = this.baseurl + '/toDaysWorkAnniversary/';
    this.httpService.get(url + this.locationId).subscribe((data: any) => {
      this.workAnniData = data.result;
      for (let index = 0; index < this.workAnniData.length; index++) {
        let element = this.workAnniData[index].empPerId;
        if (this.empPerId == element) {
          this.workAnniData.splice(index, 1);
        }
      }
    })
  }

  getBirthday() {
    this.empPerId = this.loggedUser.empPerId;
    let url = this.baseurl + '/birthdayPopup/';
    this.httpService.get(url + this.empPerId).subscribe((data: any) => {
      this.birthdayPopData = data.result;
      this.birthDayMsg = this.birthdayPopData.birthDayMsg;
      this.birthDayStatus = this.birthdayPopData.birthDayStatus;
    })
  }

  cancelbirthDay() {
    this.empPerId = this.loggedUser.empPerId;
    let url = this.baseurl + '/birthdayPopupClose?empPerId=' + this.empPerId;
    this.httpService.put(url, '').subscribe((data: any) => {
        this.birthDayStatus = false;
      },
      (err: HttpErrorResponse) => {
      });
  }

  getWorkAnnyData(i) {
    this.workAnnifullName = i.fullName;
    this.workAnniLastName = i.lastName;
    this.workAnniDescrip = i.descName;
    this.workAnniDepat = i.deptName;
    this.workAnniWishID = i.empPerId;
    this.completedYears = i.years;
    this.empId = i.empId;
    this.proPath = i.profilePath;
  }
  postWorkAnniWish(f) {
    let url = this.baseurl + '/workAnniversary';
    this.workAnniMoel.employeeName = this.loggedUser.fullName + " " + this.loggedUser.lastName;
    this.workAnniMoel.empPerId = this.workAnniWishID;
    this.httpService.post(url, this.workAnniMoel).subscribe((data): any => {
        f.reset();
        this.toastr.success("Work Anniversary wish sent successfully");
      },
      (err: HttpErrorResponse) => {
        f.reset();
        this.toastr.error("Failed to send Work Anniversary wish")
      });
  }
  ClearWorkAnni(f) {
    f.reset();
  }
  // ==================emplist ends===============
  raiseQuery() {
    this.router.navigateByUrl('/layout/querypage/querypage');
  }
  getWorkAnni() {
    let url = this.baseurl + '/popAnniversary/';
    this.httpService.get(url + this.empPerId).subscribe(data => {
        this.WorkanniData = data['result'];
        this.workAnniSatus = this.WorkanniData.status;
        this.workAnniCmpltYear = this.WorkanniData.years;
      }, (err: HttpErrorResponse) => {
        this.workAnniSatus = false;
      });
  }
  cancelWorkAnni() {
    let url = this.baseurl + '/popAnniversary';
    let obj = {
      "empPerId": this.empPerId
    }
    this.httpService.put(url, obj).subscribe((data: any) => {
        this.getWorkAnni();
      },
      (err: HttpErrorResponse) => {
      });
  }

  resetNews(f) {
    f.reset();
    this.fileName = "";
    this.imageName = "";
  }

  selfCertification() {
    this.selfCertificationReqDtos = []
    this.selfcertModel.compId = this.compId;
    this.selfcertModel.empOfficialId = this.empOfficialId;
    this.selfcertModel.fromDate = this.payStart;
    this.selfcertModel.toDate = this.payEnd;
    this.selfcertModel.payrollMonth = this.payRollMonth;
    this.selfcertModel.selfcertificationStatus = this.selfCertificationStatus
    this.selfcertModel.empTypeId = this.loggedUser.empTypeId
    let obj = {
      compId: this.compId,
      fromDate: this.payStart,
      toDate: this.payEnd,
      payrollMonth: this.payRollMonth,
      selfcertificationStatus: this.selfCertificationStatus,
      empOfficialId: this.empOfficialId,
      empTypeId: this.loggedUser.empTypeId,
    }
    this.selfCertificationReqDtos.push(obj)
    let tempObj = {
      selfCertificationReqDtos: this.selfCertificationReqDtos
    }
    this.selfService.postSelfCertificationData(tempObj).subscribe(data => {
        this.toastr.success("Self Certification Done Successfully");
        this.selfCertificationReqDtos = []
      },(err: HttpErrorResponse) => {
          this.errorMessage = JSON.parse(err['_body']);
          this.toastr.error(this.errorMessage.result.message);
          this.selfCertificationReqDtos = []
        });
  }

  lanuageFlag: boolean = true;
  changelanguageFlag() {
    
  }

  goToFinance() {
    sessionStorage.setItem("payStart", this.payStart);
    sessionStorage.setItem("payEnd", this.payEnd);
  }
  notYetInTodayData = [];

  todaysLeaveRequestData = [];
  notYetInToday() {
    // let obj = {
    //   "empId": this.empId,
    //   "locationId":this.locationId,
    //   "roleId": this.roleId,
    //   "compId": this.compId,
    //   "deptId":this.deptId
    // }

    this.DshServic.notYetInToday(this.deptId,this.loggedUser.locationId).subscribe(data => {
      this.notYetInTodayData = data.result;
     // console.log("  notYetInTodays data",this.notYetInTodayData);
    }, err => {
    })
  }

  todaysLeaveRequest() {
    this.DshServic.todaysLeaveRequest(this.loggedUser.deptId,this.loggedUser.locationId).subscribe(data => {
      this.todaysLeaveRequestData = data.result;
    }, err => {
    })
  }

  // ==============OT AND PAYROLL COST STARTS =============================
  otData = [];
  totalOtAmount: number = 0;
  totalPayRollCost: number = 0;
  OtDetails() {
    this.totalOtAmount = 0;
    this.payStart = new Date(this.payStart).getFullYear() + "-" + (new Date(this.payStart).getMonth() + 1) + "-" + new Date(this.payStart).getDate();
    this.payEnd = new Date(this.payEnd).getFullYear() + "-" + (new Date(this.payEnd).getMonth() + 1) + "-" + new Date(this.payEnd).getDate();
    let url = this.baseurl + '/otDetails?empOfficialId=' + this.empOfficialId + '&fromDate=' + this.payStart + '&toDate=' + this.payEnd;
    this.httpService.get(url).subscribe(data => {
        let otTotalAmount: number = 0;
        this.otData = data["result"];
        for (let i = 0; i < this.otData.length; i++) {
          otTotalAmount = otTotalAmount + this.otData[i].amount;
        }
        this.totalOtAmount = otTotalAmount;

      }, (err: HttpErrorResponse) => {
      }
    );

  }
  totalAmount = 0;
  PaidDaysDetails() {
    this.totalPayRollCost = 0;
    this.payStart = new Date(this.payStart).getFullYear() + "-" + (new Date(this.payStart).getMonth() + 1) + "-" + new Date(this.payStart).getDate();
    this.payEnd = new Date(this.payEnd).getFullYear() + "-" + (new Date(this.payEnd).getMonth() + 1) + "-" + new Date(this.payEnd).getDate();
    let url = this.baseurl + '/payRollCost?empOfficialId=' + this.empOfficialId + '&empTypeId=' + this.loggedUser.empTypeId + '&fromDate=' + this.payStart + '&toDate=' + this.payEnd;
    this.httpService.get(url).subscribe((data: any) => {
        this.totalAmount = data.result.totalAmount.toFixed(2);
        this.totalPayRollCost = this.totalAmount;
      }, (err: HttpErrorResponse) => {
    });

  }

  totalShiftAllowance = 0;
  getShiftAllowance() {

    this.payStart = new Date(this.payStart).getFullYear() + "-" + (new Date(this.payStart).getMonth() + 1) + "-" + new Date(this.payStart).getDate();
    this.payEnd = new Date(this.payEnd).getFullYear() + "-" + (new Date(this.payEnd).getMonth() + 1) + "-" + new Date(this.payEnd).getDate();
    let url = this.baseurl + '/employeeShiftAllowance?empOfficialId=' + this.empOfficialId + '&fromDate=' + this.payStart + '&toDate=' + this.payEnd
    this.totalShiftAllowance = 0;
    this.httpService.get(url).subscribe((data: any) => {
        if (data.result) {
          for (let i = 0; i < data.result.length; i++) {
            this.totalShiftAllowance = this.totalShiftAllowance + data.result[i].totalShiftAllowance;
          }
        }

      }, (err: HttpErrorResponse) => {
        this.totalShiftAllowance = 0;
      });
  }
  // ==============OT AND PAYROLL COST ENDS =============================
  //  ============Coupan Starts ================
  openCoupanModal(fCo) {
    this.coupanType = "allemp";
    fCo.reset();
    setTimeout(() => {
      this.employeeCouponModel.cpnVariable = 'allemp'
      this.employeeCouponModel.employeePerInfoList = [];
    }, 100);
  }
  //  ============Coupan Starts ================
  getDepartmentForCoupon() {
    let busiurl = this.baseurl + '/Department/Active/';
    this.httpService.get(busiurl + this.compId).subscribe((data: any) => {
        this.DepartmentListForCoupon = data.result;
      },
      (err: HttpErrorResponse) => {
      }
    );
  }

  getEmployeeListForCoupon(deptId) {
    this.deptId = deptId

    let url1 = this.baseurl + '/empListByDepartment/';
    this.httpService.get(url1 + this.deptId+'/'+this.locationId).subscribe((data: any) => {
        this.EmployeListForCoupon = data.result;
        for (let i = 0; i < this.EmployeListForCoupon.length; i++) {
          const element = this.EmployeListForCoupon[i];
          this.EmployeListForCoupon[i].fullName = this.EmployeListForCoupon[i].empId + " - " + this.EmployeListForCoupon[i].fullName + "  " + this.EmployeListForCoupon[i].lastName;
        }
      },
      (err: HttpErrorResponse) => {
      });
  }
  dropValue(event) {
    this.selectedObj = parseInt(event.target.value);
  }
  ngValueDep(event) {
    this.selectedDep = parseInt(event.target.value)
    this.selectedDep
    this.getEmployeeListForCoupon(this.selectedDep);
  }
  SaveCoupon(fCo) {
    this.employeeCouponModel.compId = this.loggedUser.compId;
    this.employeeCouponModel.empPerIdCoupanAdmin = this.loggedUser.empPerId;
    let url1 = this.baseurl + '/createCoupon';
    this.httpService.post(url1, this.employeeCouponModel).subscribe((data: any) => {
        this.getAllCoupons();
        this.getCoponList();
        this.toastr.success(" Coupon Created Successfully");
        fCo.reset();
        this.employeeCouponModel.employeePerInfoList = [];
        this.hideButn = false;
        this.shwButton = false;
        this.imgclick = false;
      },
      (err: HttpErrorResponse) => {
        this.toastr.error("Something went wrong")
      });
  }

  getCoponList() {
    this.coupounList = [];
    this.compId = this.loggedUser.compId;
    this.empPerId = this.loggedUser.empPerId;
    let url = this.baseurl + '/couponListData/?empPerId=' + this.empPerId;
    this.httpService.get(url).subscribe((data: any) => {
      this.coupounList = data.result;
     //console.log("test1233",this.coupounList );
    },
    (err: HttpErrorResponse) => {
    
      this.coupounList = [];
    });
  }
  getAllCoupons() {
    let url = this.baseurl + '/couponByList/';
    this.httpService.get(url).subscribe((data: any) => {
      this.AllcoupounList = data.result;
    
    },
    (err: HttpErrorResponse) => {
    });
  }
  dltmodelsEvent() {
    this.dltmodelsFlag = true;
    this.deletCoupon(this.deletObjec);
  }
  dltsObj(obj) {
    this.dltmodelsFlag = false;
    this.deletObjec = obj.cpnId;
    this.deletCoupon(this.deletObjec);
  }
  deletCoupon(object) {
    if (this.dltmodelsFlag == true) {
      this.employeeCouponModel.cpnId = this.deletObjec;
      let url1 = this.baseurl + '/deleteCoupon/';
      this.httpService.delete(url1 + this.deletObjec).subscribe((data: any) => {
          this.getAllCoupons();
          this.getCoponList();
          this.toastr.success(" Coupon Deleted Successfully");
        },
        (err: HttpErrorResponse) => {
          this.toastr.error("Failed to delete coupon..!");
        });
    }
  }
  myurl: any;
  shwButton: boolean
  imgclick: boolean
  handleCouponFileInput(file: FileList) {
    this.shwButton = false;
    this.imgclick = true;
    this.imageUrl = this.baseurl + '/uploadFile';
    this.filesToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.filesToUpload);
    this.postCouponimage(file.item(0));
  }
  returnsImageObject: any = {};
  imagePath: any;
  ppostCouponimage(filesToUpload) {
    let url = this.baseurl + '/uploadFile';
    const endpoint = url;
    const formData: FormData = new FormData();
    formData.append('uploadfile', filesToUpload, filesToUpload.name);
    return this.http.post(endpoint, formData).map(x => x);
  }
  imageUrls: any;
  postCoupon(files) {
    this.ppostCouponimage(files).subscribe((data: any) => {
      this.filPathImage = JSON.parse(data._body).result.imageUrl;
      this.fileTypeImage = this.filesToUpload.type;
      this.returnsImageObjectFile = JSON.parse(data._body).result;
    });
  }
  hideButn: boolean = false
  postCouponimage(files) {
    this.DshServic.onUploadCoupon(this.filesToUpload).subscribe((data: any) => {
        this.imageUrls = JSON.parse(data._body).result.imageUrl;
        this.employeeCouponModel.cpnIcon = this.imageUrls;
        this.filPathImage = JSON.parse(data._body).result.imageUrl;
        this.fileTypeImage = this.filesToUpload.type;
        this.returnImageObjectFile = JSON.parse(data._body).result;
        this.filesToUpload = null;
        this.returnsImageObjectFile = JSON.parse(data._body).result;
        this.hideButn = true;
        this.shwButton = true;
        this.imgclick = true;
      });
  }
  valueYear(e){
    this.selectedYear = e
    this.coffDetails.empPerId = this.loggedUser.empPerId;
    let date = e + "-01" + "-01";
    this.newDate = new Date(date);
    this.coffDetails.startDate = this.newDate;
    let endDate = e + "-12" + "-31";
    let newEndDate = new Date(endDate);
    this.coffDetails.endDate = newEndDate;
  }




  // ---------------------------------------- Start_EplyoeeSelector-----------------------
  onItemSelect(item: any) {
    if (this.employeePerInfoList.length == 0) {
      this.employeePerInfoList.push(item)
    } else {
      this.employeePerInfoList.push(item)
    }
  }
  onSelectAll(items: any) {
    this.employeePerInfoList = items;
  }
  OnItemDeSelect(item: any) {
    for (var i = 0; i < this.employeePerInfoList.length; i++) {
      if (this.employeePerInfoList[i].empPerId == item.empPerId) {
        this.employeePerInfoList.splice(i, 1);
      }
    }
  }
  onDeSelectAll(item: any) {
    this.employeePerInfoList = [];
  }
  // ===========Face detection starts===================
  searchPhoto() {
    let url = "https://api.luxand.cloud/photo/search";
    var myHeaders = new Headers();
    myHeaders.append("token", "00c5ead0fb844d95b7f2500098f7a954");
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    var urlencoded = new URLSearchParams();
    urlencoded.append("photo", "http://storage.luxand.cloud/737f0be2786d4a25a8767da7c685fcf8.jpg");
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
      async: true,
      crossDomain: true
    };
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }
  transformImg(path){
    let temp='data:image/png;base64,'+path;
    return this.sanitizer.bypassSecurityTrustResourceUrl(temp);
  }
  getmrfid(e){
this.mrfid=e.target.value

let url = this.baseurl + '/mrfInfoByMrfId/';
this.httpService.get(url+this.mrfid).subscribe(data => {
this.currentopeningdetails= data['result'];

  this.deptnames=this.currentopeningdetails.departmentMaster.deptName;
this.descNames=this.currentopeningdetails.designationMaster.descName;
 this.agegroup=this.currentopeningdetails.ageGroup;
 this.initiatedDate=this.currentopeningdetails.applyDate;
// this.typeofvacancy=this.currentopeningdetails.recVacancyType;
  this.noofvacancy=this.currentopeningdetails.currentOpeningCount;
 this.mandatoryqualification=this.currentopeningdetails.mandatoryQualification;
  this.experience=this.currentopeningdetails.experienceInYear;
   this.locationname=this.currentopeningdetails.locationMaster.locationName;
this.tentetiveDate=this.currentopeningdetails.tentativeDate;
//console.log(this.currentopeningdetails)
  })
}
//   getCurrentOpenings(){
//     let url1 = this.baseurl + '';
//     this.httpService.get(url1).subscribe((data: any) => {
//      // this.currentopeningdata=data.result
//   })
// }
}
// ======================== Coupon end ========================