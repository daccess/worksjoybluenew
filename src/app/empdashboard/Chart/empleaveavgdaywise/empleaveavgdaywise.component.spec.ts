import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleaveavgdaywiseComponent } from './empleaveavgdaywise.component';

describe('EmpleaveavgdaywiseComponent', () => {
  let component: EmpleaveavgdaywiseComponent;
  let fixture: ComponentFixture<EmpleaveavgdaywiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleaveavgdaywiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleaveavgdaywiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
