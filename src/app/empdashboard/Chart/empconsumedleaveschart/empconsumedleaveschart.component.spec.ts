import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpconsumedleaveschartComponent } from './empconsumedleaveschart.component';

describe('EmpconsumedleaveschartComponent', () => {
  let component: EmpconsumedleaveschartComponent;
  let fixture: ComponentFixture<EmpconsumedleaveschartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpconsumedleaveschartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpconsumedleaveschartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
