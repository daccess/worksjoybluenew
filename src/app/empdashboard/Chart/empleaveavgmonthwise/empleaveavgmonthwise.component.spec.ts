import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleaveavgmonthwiseComponent } from './empleaveavgmonthwise.component';

describe('EmpleaveavgmonthwiseComponent', () => {
  let component: EmpleaveavgmonthwiseComponent;
  let fixture: ComponentFixture<EmpleaveavgmonthwiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleaveavgmonthwiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleaveavgmonthwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
