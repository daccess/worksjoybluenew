import { Component, OnInit } from '@angular/core';
import { EmpDashSerivcService } from 'src/app/shared/services/EmpDashboardService';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

  constructor(private service: EmpDashSerivcService) {
    service.AgeGroupWiseTotalCountOfEmp().subscribe((data: any) => {
      let absentDays = data.result.absentDays;
      let coff = data.result.coff;
      let leave= data.result.leave;
      let prCount = data.result.prCount;
      let presentDays = data.result.presentDays;
      let wekkHoliDay  = data.result.wekkHoliDay;
      let obj: any = {
         absentDays, 
         coff,
         leave, 
         prCount, 
         presentDays,
         wekkHoliDay
      }
      this.pieChartData =  [
          absentDays, 
          coff ,
          leave, 
          prCount, 
          presentDays
    
      ];
    })
  }

  ngOnInit() {
  }

  public barChartLabels: string[] = [' absentDays', ' coff', ' leave', 'prCount', 'presentDays'];
  public barChartType: string = 'pie';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [1,2,3,4,5];
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }



  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

  public pieChartLabels: string[] = ['absentDays', 'coff', 'leave', 'prCount', 'presentDays','wekkHoliDay'];
  public pieChartData: number[] = [0,0,0,0,0,0];
  // public pieChartData: number[] = [];
  public pieChartType: string = 'pie';
}
