import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { EmpdashboardComponent } from 'src/app/empdashboard/empdashboard.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: EmpdashboardComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'empdashboard',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'empdashboard',
                    component: EmpdashboardComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);