import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MainURL } from '../../shared/configurl';
import { from } from 'rxjs';


@Injectable()
export class selfcertificationservicestatus {
 
  baseurl = MainURL.HostUrl
  constructor(private http : Http) { }

  getSelfCertificationstatus(empTypeId : any , payStart :  string, payEnd :  string){
    let url = this.baseurl+'/selfcertification/selfcertificationStatus?empTypeId='+ empTypeId +'&endDate='+ payEnd +'&startDate=' + payStart;
    //let url = this.baseurl+'/selfcertification/selfcertificationStatus?empTypeId='+ empTypeId +'&startDate='+ payStart +'&endDate=' + payEnd;
    // var body = JSON.stringify(resignation);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Get,headers : headerOptions});
    return this.http.get(url,requestOptions).map(x => x.json());
  }

}