import { TestBed, inject } from '@angular/core/testing';

import { LeavedashboardServiceService } from './leavedashboard-service.service';

describe('LeavedashboardServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeavedashboardServiceService]
    });
  });

  it('should be created', inject([LeavedashboardServiceService], (service: LeavedashboardServiceService) => {
    expect(service).toBeTruthy();
  }));
});
