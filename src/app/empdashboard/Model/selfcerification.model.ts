export class selfCertification{
    selfcertificationId: number;
    compId: number;
    empOfficialId: number;
    fromDate: Date;
    toDate: Date;
    payrollMonth: string;
    selfcertificationStatus  : boolean;
    empTypeId: number
}
