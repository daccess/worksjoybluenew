import { routing } from './empdashboard.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import{CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { EmpDashSerivcService } from '../shared/services/EmpDashboardService';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SearchPipeForEmpDash } from './SearchPipe/Serach';
import { ShContextMenuModule } from 'context-menu-angular6';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { LeavedashboardServiceService}  from './services/leavedashboard-service.service';



@NgModule({
    declarations: [   
],
    imports: [
       
    routing,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    NgMultiSelectDropDownModule.forRoot(),
    ShContextMenuModule,
    ChartsModule,
    NgxChartsModule,
   
    ],
    providers: [EmpDashSerivcService, LeavedashboardServiceService],
  


    

    
    
  })
  
  export class EmpdashboardModule { 
      constructor(){

      }
  }
  