import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js'
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-coffchart',
  templateUrl: './coffchart.component.html',
  styleUrls: ['./coffchart.component.css']
})
export class CoffchartComponent {
  loggedUser: any;

  constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.getDailySkilledUnskilled();
    this.getDepartmentsById();
  }

 title = 'doughnutChart';
  public doughnutChartLabels:string[] = ['Unused Coff Count', 'Used Coff Count', 'Lapsed Coff Count'];
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';
  public backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"];

  public chartClicked(e:any):void {
  }
 
  public chartHovered(e:any):void {
  }
  coffCount = [];
  getDepartmentsById(){
    this.service.getDepartmentsById(this.loggedUser.compId).subscribe(data=>{
      this.getCoffCountByDepartmentId(data.result[3].deptId);
    },err=>{
      
    })
  }
  getCoffCountByDepartmentId(id){
    this.service.getCoffCountByDepartmentId(id).subscribe(data=>{
      this.doughnutChartData = [data.result.coffUnused,data.result.coffUsed,data.result.coffLapsed];
    },err=>{ 
    })
  }
}
