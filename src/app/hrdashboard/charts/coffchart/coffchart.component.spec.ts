import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffchartComponent } from './coffchart.component';

describe('CoffchartComponent', () => {
  let component: CoffchartComponent;
  let fixture: ComponentFixture<CoffchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
