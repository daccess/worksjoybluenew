import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffsupervsorchartComponent } from './coffsupervsorchart.component';

describe('CoffsupervsorchartComponent', () => {
  let component: CoffsupervsorchartComponent;
  let fixture: ComponentFixture<CoffsupervsorchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffsupervsorchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffsupervsorchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
