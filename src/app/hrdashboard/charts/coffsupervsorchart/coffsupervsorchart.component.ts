import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coffsupervsorchart',
  templateUrl: './coffsupervsorchart.component.html',
  styleUrls: ['./coffsupervsorchart.component.css']
})
export class CoffsupervsorchartComponent  {

 

  title = 'doughnutChart';
  public doughnutChartLabels:string[] = ['Unused', 'Used', 'Lapsed'];
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';
  public backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"];

  public chartClicked(e:any):void {
  }
 
  public chartHovered(e:any):void {
  }

}
