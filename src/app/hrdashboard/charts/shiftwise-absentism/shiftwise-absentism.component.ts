import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-shiftwise-absentism',
  templateUrl: './shiftwise-absentism.component.html',
  styleUrls: ['./shiftwise-absentism.component.css']
})
export class ShiftwiseAbsentismComponent implements OnInit {
  loggedUser: any;
  shiftWiseAbsentism: any;
  chartFlag: boolean;
  EmployeeLocationwise: any;
 labelObj = new Array();
  rollName: any;
  locationid: any=0;

  constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.rollName=this.loggedUser.roleHead;
  this.getShiftWiseAbsentism(this.locationid);
  
  }

  ngOnInit() {
    this.getallLocation()
  }
  public barChartLabels: string[] = [ 'First Shift', 'Second Shift', 'Third Shift'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
 

  public barChartData: any[] = [
    { data: [208, 384, 160], label: 'Employee and Contractor' },
    // {data: [344, 23, 0, 14], label: 'Temp'}
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#23cd4a",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { 
      backgroundColor: "#fe9c85",
      borderColor: 'white',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },

  ]

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    scales: {
     yAxes: [
      {
       display: true,
       labels: {
        show: true,
        },
       ticks: {
        beginAtZero: true,
        userCallback: function(label, index, labels) {
          if (Math.floor(label) === label) {
            return label;
          }
        }
      },
       scaleLabel: {
        display: true,
        labelString: "Employee Count",
       },
      },
     ],
     xAxes: [
      {
       scaleLabel: {
        display: true,
        labelString: "Shift Name",
        
       },
       barPercentage: 0.2
      },
     ],
    },
   };
  
  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    console.log("barChartData8888", this.barChartData);
  }
  getallLocation(){
    
    var url = "/Location/";
    this.service.getlocationname(url + this.loggedUser.compId).subscribe((data) => {
      this.EmployeeLocationwise = data["result"];
      

    });
  
 
    
  }
  // getLocationid(e){
  //   this.locationid=e.target.value
  //   this.getShiftWiseAbsentism(this.locationid)
  // }
  getShiftWiseAbsentism(e:any){
 
    this.locationid=e
    if(this.loggedUser.roleHead=='HR'){
    let obj ={
      compId : this.loggedUser.compId
    }
    let url="/getShiftWisePresentism/"
    this.service.getShiftWisePresentism(url+this.loggedUser.locationId).subscribe(data=>{
      this.shiftWiseAbsentism = data['result'];
      console.log("this is the employee list",this.shiftWiseAbsentism);
      
      let dataObj = new Array();
    
      for (let i = 0; i < this.shiftWiseAbsentism.length; i++) {
        dataObj.push(this.shiftWiseAbsentism[i].presentCount);
        this.labelObj.push(this.shiftWiseAbsentism[i].shiftName);
      }
      this.barChartLabels = this.labelObj;
      console.log("lablescontain", this.labelObj)
      this.barChartData = [
        { data:  dataObj , label: 'Employee and Contractor' },
      ];
      this.chartFlag = true;
      
    },err=>{
      this.barChartLabels = [];
      this.barChartData = [
        { data:  [] , label: 'Employee and Contractor' },
      ];
    })
  }
  else if(this.loggedUser.roleHead=='Admin'){
    let obj ={
      compId : this.loggedUser.compId
    }
    console.log("locationid",this.locationid)
    let url="/getShiftWisePresentism/"
    this.service.getShiftWisePresentism(url+ this.locationid).subscribe(data=>{
      this.shiftWiseAbsentism = data['result'];
      console.log("this is the employee list",this.shiftWiseAbsentism);
      
      let dataObj = new Array();
      let labelObj = new Array();
      for (let i = 0; i < this.shiftWiseAbsentism.length; i++) {
        dataObj.push(this.shiftWiseAbsentism[i].presentCount);
        labelObj.push(this.shiftWiseAbsentism[i].shiftName);
      }
      
      
    
      this.barChartLabels = labelObj;
      console.log("labledata****",this.barChartLabels);
      this.barChartData = [
        { data:  dataObj , label: 'Employee and Contractor' },
        
      ];
      this.chartFlag = true;
     
    },err=>{

      this.barChartLabels = [];
      this.barChartData = [
        { data:  [] , label: 'Employee and Contractor' },
      ];
    })
  }
}
}
