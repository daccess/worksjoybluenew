import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftwiseAbsentismComponent } from './shiftwise-absentism.component';

describe('ShiftwiseAbsentismComponent', () => {
  let component: ShiftwiseAbsentismComponent;
  let fixture: ComponentFixture<ShiftwiseAbsentismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftwiseAbsentismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftwiseAbsentismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
