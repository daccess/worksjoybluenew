import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OthrsincreaseunplannedComponent } from './othrsincreaseunplanned.component';

describe('OthrsincreaseunplannedComponent', () => {
  let component: OthrsincreaseunplannedComponent;
  let fixture: ComponentFixture<OthrsincreaseunplannedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OthrsincreaseunplannedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OthrsincreaseunplannedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
