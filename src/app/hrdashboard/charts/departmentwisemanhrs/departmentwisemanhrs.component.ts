import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-departmentwisemanhrs',
  templateUrl: './departmentwisemanhrs.component.html',
  styleUrls: ['./departmentwisemanhrs.component.css']
})
export class DepartmentwisemanhrsComponent implements OnInit {
  loggedUser : any;
  ngOnInit() {
  }

  constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    // this.getManHoursCountByDepartmentId();
   }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['First', 'Second', 'Third', 'Fourth'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [80, 100, 60, 75, 90, 55, 70], label: 'Total Man Hours' },
    { data: [60, 75, 56, 70, 89, 50, 70], label: 'Actual Department Worked' }
  ];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#46aae9",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: '#f1be5d',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

  manHoursCount =[];
  getManHoursCountByDepartmentId(id){
    this.service.getManHoursCountByDepartmentId(id).subscribe(data=>{
      this.manHoursCount = data.result;
      let temp = new Array();
      for (let i = 0; i < this.manHoursCount.length; i++) {
        let total = 0;
        if(this.manHoursCount[i].totalManHoursCntHF){
          total + this.manHoursCount[i].totalManHoursCntHF;
        }
        if(this.manHoursCount[i].totalManHoursCntP){
          total + this.manHoursCount[i].totalManHoursCntP;
        }
        this.manHoursCount[i].total =  total;
        for (let j = 0; j < this.manHoursCount.length; j++) {
          if(this.manHoursCount[i].shiftMasterId==this.manHoursCount[j].shiftMasterId && i != j){
            
            this.manHoursCount.splice(i,1);
          }
        }
        
      }
      
    },err=>{
      
    })
  }

}

