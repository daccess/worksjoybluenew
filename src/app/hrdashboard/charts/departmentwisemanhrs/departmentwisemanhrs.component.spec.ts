import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentwisemanhrsComponent } from './departmentwisemanhrs.component';

describe('DepartmentwisemanhrsComponent', () => {
  let component: DepartmentwisemanhrsComponent;
  let fixture: ComponentFixture<DepartmentwisemanhrsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentwisemanhrsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentwisemanhrsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
