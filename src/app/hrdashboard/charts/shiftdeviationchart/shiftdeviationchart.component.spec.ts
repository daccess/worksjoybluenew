import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftdeviationchartComponent } from './shiftdeviationchart.component';

describe('ShiftdeviationchartComponent', () => {
  let component: ShiftdeviationchartComponent;
  let fixture: ComponentFixture<ShiftdeviationchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftdeviationchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftdeviationchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
