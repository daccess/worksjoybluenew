import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LateEarlyLateChartComponent } from './late-early-late-chart.component';

describe('LateEarlyLateChartComponent', () => {
  let component: LateEarlyLateChartComponent;
  let fixture: ComponentFixture<LateEarlyLateChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LateEarlyLateChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LateEarlyLateChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
