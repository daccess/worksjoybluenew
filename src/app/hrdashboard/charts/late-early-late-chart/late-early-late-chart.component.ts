import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-late-early-late-chart',
  templateUrl: './late-early-late-chart.component.html',
  styleUrls: ['./late-early-late-chart.component.css']
})
export class LateEarlyLateChartComponent implements OnInit {

  loggedUser: any;
  monthlyLateSittingEmployee: any;
  constructor(private service: HrdashboardService) {
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
   // this.getMonthlyLateSittingEmployee();
    this.getDailyLateSittingEmployee();

  }
  ngOnInit() {
  }
  public barChartLabels: string[] = ['IT', 'Sales', 'Account', 'HR', 'Marketing'];
  // 'IT', 'Sales', 'Account', 'HR', 'Marketing'
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    // { data: [800, 200, 190, 180, 120, 250, 80], label: 'Late Coming' },
    // { data: [400, 200, 130, 220, 130, 270, 60], label: 'Early Going' },
    { data: [150, 141, 100, 120, 140, 230, 75], label: 'Monthly Late Sitting' }
  ];


  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    scales: {
     yAxes: [
      {
       display: true,
       labels: {
        show: true,
        },
       ticks: {
        beginAtZero: true,
        userCallback: function(label, index, labels) {
          if (Math.floor(label) === label) {
            return label;
          }
        },
      },
       scaleLabel: {
        display: true,
        labelString: "Employee count",
       },
      },
     ],
     xAxes: [
      {
       scaleLabel: {
        display: true,
        labelString: "Department Name",
       },
       barPercentage: 0.1
      },
     ],
    },
   };


  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#ff753f",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey

      backgroundColor: '#FF4500',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    },
    { // grey

      backgroundColor: '#ffbd3f',
      borderColor: '#fff',
      pointBackgroundColor: 'blue',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
  ]

  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

  chartFlag: boolean = false;
  getMonthlyLateSittingEmployee() {
    let obj = {
      compId: this.loggedUser.compId,
      fromDate: new Date(new Date().setDate(1)),
      // fromDate : new Date("01-05-2019"),
      toDate: new Date()
    }
    this.service.getMonthlyLateSittingEmployee(obj).subscribe(data => {
      this.monthlyLateSittingEmployee = data.result;
      let earlyGoingObj = new Array();
      let lateCommingObj = new Array();
      let lateSittingObj = new Array();
      let labelObj = new Array();

      if (data.result.length) {
        for (let i = 0; i < data.result.length; i++) {
          // earlyGoingObj.push(this.monthlyLateSittingEmployee[i].earlyGoing);
          // lateCommingObj.push(this.monthlyLateSittingEmployee[i].lateComming);
          lateSittingObj.push(this.monthlyLateSittingEmployee[i].lateSitting);
          // labelObj.push(this.monthlyLateSittingEmployee[i].deptName.trim());
          labelObj.push(this.monthlyLateSittingEmployee[i].deptName);
        }
        this.monthlyBarChartData = [
          // { data: lateCommingObj, label: 'Late Coming' },
          // { data: earlyGoingObj, label: 'Early Going' },
          { data: lateSittingObj, label: 'Late Sitting Employee Count' }
        ];
      }
      else {
        this.monthlyBarChartData = [
          // { data: [0], label: 'Late Coming' },
          // { data: [0], label: 'Early Going' },
          { data: [0], label: 'Late Sitting Employee Count' }
        ];
      }
      // this.getMonthlyLateSittingEmployees();
      this.chartFlag = true;
      this.barChartLabels = labelObj;
    }, err => {
      this.monthlyBarChartData = [
        // { data: [0], label: 'Late Coming' },
        // { data: [0], label: 'Early Going' },
        { data: [0], label: 'Late Sitting' }
      ];
    })
  }

  getDailyLateSittingEmployee() {
    let obj = {
      compId: this.loggedUser.compId,
      fromDate: new Date(new Date().setDate(1)),
      toDate: new Date()
    }
    this.service.getDailyLateSittingEmployee(obj).subscribe(data => {
      this.monthlyLateSittingEmployee = data.result;
      let lateSittingObj = new Array();
      let labelObj = new Array();

      if (data.result.length) {
        for (let i = 0; i < data.result.length; i++) {
          lateSittingObj.push(this.monthlyLateSittingEmployee[i].lateSitting);
          labelObj.push(this.monthlyLateSittingEmployee[i].deptName);
        }
        this.dailyBarChartData = [
          { data: lateSittingObj, label: 'Late Sitting Employee Count' }
        ];
      }
      else {
        this.dailyBarChartData = [
          // { data: [0], label: 'Late Coming' },
          // { data: [0], label: 'Early Going' },
          { data: [0], label: 'Late Sitting Employee Count' }
        ];
      }
      this.getDailyLateSittingEmployees();
      this.chartFlag = true;
      this.barChartLabels = labelObj;
    }, err => {
      this.dailyBarChartData = [
        // { data: [0], label: 'Late Coming' },
        // { data: [0], label: 'Early Going' },
        { data: [0], label: 'Late Sitting Employee Count' }
      ];
    })
  }
  monthlyBarChartData = [];
  dailyBarChartData = [];
  chartFlagD: boolean = false;
  chartFlagM: boolean = false;
  getMonthlyLateSittingEmployees() {
    //  this.barChartData = this.monthlyBarChartData;
    this.chartFlagD = false;
    this.chartFlagM = true;
  }
  getDailyLateSittingEmployees() {
    // this.barChartData = this.dailyBarChartData;
    this.chartFlagM = false;
    this.chartFlagD = true;
  }

}
