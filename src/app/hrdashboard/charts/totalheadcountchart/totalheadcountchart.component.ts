import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-totalheadcountchart',
  templateUrl: './totalheadcountchart.component.html',
  styleUrls: ['./totalheadcountchart.component.css']
})
export class TotalheadcountchartComponent implements OnInit {
  locationwiseHeadCount: any = [];
  loggedUser: any;
  EmployeeLocationwise: any;
  rollName: any;
  selectedlocationIds: any=0;
  constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.rollName=this.loggedUser.roleHead;
  
  }
  ngOnInit() {
    
    this.getallLocation();
    this.getLocationwiseHeadCounts(this.selectedlocationIds)
  }
  // public barChartLabels: string[] = ['18-20', '21-25', '26-30', '31-35', '36-40', '41-45', '46-50', '51-55', 'more than 55'];
  public barChartLabels: string[] = []
  // 'Chandigarh', 'Coimbatore', 'Pune', 'Wing';
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
  public barChartColors: Array<any> = [
    { // grey
      backgroundColor: "#8ed866",
      borderColor: 'black',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    // { // grey

    //   backgroundColor: '#4a96d5',
    //   borderColor: '#fff',
    //   pointBackgroundColor: 'blue',
    //   pointBorderColor: '#fff',
    //   pointHoverBackgroundColor: '#fff',
    //   pointHoverBorderColor: '#fff'
    // }
  ]

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    scales: {
     yAxes: [
      {
       display: true,
       labels: {
        show: true,
        },
       ticks: {
        beginAtZero: true,
        userCallback: function(label, index, labels) {
          if (Math.floor(label) === label) {
            return label;
          }
        },
      },
       scaleLabel: {
        display: true,
        labelString: "Employee count",
       },
      },
     ],
     xAxes: [
       
      {
        ticks: {
          autoSkip: false,
          
        },
       scaleLabel: {
        display: true,
        labelString: "Location Name",
       },
       barPercentage: 0.1
      },
     ],
    },
   };


  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

  chartFlag : boolean = false;
  getLocationwiseHeadCounts(e){
    // let obj ={
    //   compId : this.loggedUser.compId
    // }
    
    this.selectedlocationIds=e;
    if(this.loggedUser.roleHead=='HR'){
    let url='/getLocationWisePrsenetCount1/'
    this.service.getLocationWithCounts(url+this.loggedUser.compId+'/'+this.loggedUser.locationId).subscribe(data=>{
  
      this.locationwiseHeadCount = data['result']
      let dataObj = new Array();
      let labelObj = new Array();
      for (let i = 0; i < this.locationwiseHeadCount.length; i++) {
        dataObj.push(this.locationwiseHeadCount[i].totalCount);
        labelObj.push(this.locationwiseHeadCount[i].locationName);
      }
      
      this.barChartLabels = labelObj;
      this.barChartData = [
        { data:  dataObj , label: 'Present Employees' },
      ];
      this.chartFlag = true;
      
    },err=>{
      this.barChartLabels = [];
      this.barChartData = [
        { data:  [] , label: 'Present Employees' },
      ];
    })
  }
  else  if(this.loggedUser.roleHead=='Admin'){
    let url='/getLocationWisePrsenetCount1/'
    this.service.getLocationWithCounts(url+this.loggedUser.compId+'/'+this.selectedlocationIds).subscribe(data=>{
  
      this.locationwiseHeadCount = data['result']
      let dataObj = new Array();
      let labelObj = new Array();
      for (let i = 0; i < this.locationwiseHeadCount.length; i++) {
        dataObj.push(this.locationwiseHeadCount[i].totalCount);
        labelObj.push(this.locationwiseHeadCount[i].locationName);
      }
      
      this.barChartLabels = labelObj;
      this.barChartData = [
        { data:  dataObj , label: 'Present Employees' },
      ];
      this.chartFlag = true;
      
    },err=>{
      this.barChartLabels = [];
      this.barChartData = [
        { data:  [] , label: 'Present Employees' },
      ];
    })
  }

  }
  getallLocation(){
    
    var url ="/Location/";
    this.service.getallLocations(url ,this.loggedUser.compId).subscribe((data) => {
      this.EmployeeLocationwise = data["result"];
      

    });
  
  }
}
