import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalheadcountchartComponent } from './totalheadcountchart.component';

describe('TotalheadcountchartComponent', () => {
  let component: TotalheadcountchartComponent;
  let fixture: ComponentFixture<TotalheadcountchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalheadcountchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalheadcountchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
