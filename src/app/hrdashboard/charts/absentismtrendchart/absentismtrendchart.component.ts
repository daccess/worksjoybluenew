import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-absentismtrendchart',
  templateUrl: './absentismtrendchart.component.html',
  styleUrls: ['./absentismtrendchart.component.css']
})
export class AbsentismtrendchartComponent {

  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'}
   
  ];
  public lineChartLabels:Array<any> = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug','Sep','Oct','Nov','Dec'];
  public lineChartOptions:any = {
    responsive: true
  };
  loggedUser: any;
  constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getAbsentismTrend();
   }
   getAbsentismTrend(){
     this.service.getAbsentismTrend(this.loggedUser.compId).subscribe(data=>{
      //  this.pieChartData = [data.result.absentPer,data.result.presentPer];     
      let obj = [data.result[0].count,data.result[1].count,data.result[2].count,data.result[2].count,data.result[3].count,data.result[4].count,data.result[5].count,data.result[6].count,data.result[7].count,data.result[8].count,data.result[9].count,data.result[10].count,data.result[11].count];

      this.lineChartData = [
        {data: obj, label: ''}
      ]
     },err=>{
      this.lineChartData = [];
      // this.pieChartData = [];
     })
   }
   public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: '#ffa9a9',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }
 
  // events
  public chartClicked(e:any):void {
  }
 
  public chartHovered(e:any):void {
  }
}


