import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbsentismtrendchartComponent } from './absentismtrendchart.component';

describe('AbsentismtrendchartComponent', () => {
  let component: AbsentismtrendchartComponent;
  let fixture: ComponentFixture<AbsentismtrendchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbsentismtrendchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbsentismtrendchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
