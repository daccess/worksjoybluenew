import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FtechartComponent } from './ftechart.component';

describe('FtechartComponent', () => {
  let component: FtechartComponent;
  let fixture: ComponentFixture<FtechartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FtechartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FtechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
