import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-ftechart',
  templateUrl: './ftechart.component.html',
  styleUrls: ['./ftechart.component.css']
})
export class FtechartComponent  {

  title = 'doughnutChart';
   public pieChartLabels:string[] = ['FTE', 'TEMP'];
   public pieChartData:number[] = [200, 450];
   public pieChartType:string = 'pie';
   public pieChartColor:string[]= ["red", "green"];
   public pieChartOptions:any ={'backgroundColor': [
    "red",
 "green",
 
 ]};
 loggedUser: any;
   // events

   constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getFteVsTempCount();
   }
   fteVsTempData : any = [];
   getFteVsTempCount(){
    this.service.getFteVsTempCount(this.loggedUser.compId).subscribe(data=>{
      this.fteVsTempData = data.result;
      this.pieChartData = [data.result.ftePerCntD,data.result.tempPerCntD];       
    },err=>{
     this.pieChartData = [];
    });
   }
   getFteVsTempMonthly(){
    this.pieChartData = [this.fteVsTempData.fteCountMonth,this.fteVsTempData.tempCountMonth];  
   }
   getFteVsTempDaily(){
    this.pieChartData = [this.fteVsTempData.ftePerCntD,this.fteVsTempData.tempPerCntD];  
  }
   // events
   public chartClicked(e:any):void {
   }
  
   public chartHovered(e:any):void {
   }

}
