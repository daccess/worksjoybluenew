import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkilledchartComponent } from './skilledchart.component';

describe('SkilledchartComponent', () => {
  let component: SkilledchartComponent;
  let fixture: ComponentFixture<SkilledchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkilledchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkilledchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
