import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-skilledchart',
  templateUrl: './skilledchart.component.html',
  styleUrls: ['./skilledchart.component.css']
})
export class SkilledchartComponent implements OnInit {
  loggedUser: any;
  skilledUnskilled = [];
  dailyBarChartData: { data: any[]; label: string; }[];
  chartFlag: boolean = false;

  constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getDailySkilledUnskilled();
  }
      ngOnInit() {
      }
    
      public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
      };
      public barChartLabels: string[] = [];
      public barChartType: string = 'bar';
      public barChartLegend: boolean = true;
    
      public barChartData: any[] = [
        { data: [], label: 'Skilled' },
        { data: [], label: 'Unskilled' },
        { data: [], label: 'Semiskilled' }
      ];
      // { data: [65, 59, 80, 81, 56, 55, 40], label: 'Skilled' },
      // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Unskilled' },
      // { data: [25, 40, 35, 15, 40, 20, 50], label: 'Semiskilled' }
      // events
      public chartClicked(e: any): void {
      }
    
      public chartHovered(e: any): void {
      }
      public barChartColors: Array<any> = [
        { // grey
          backgroundColor: "#74b87b",
          borderColor: 'black',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // grey
    
          backgroundColor: '#4a96d5',
          borderColor: '#fff',
          pointBackgroundColor: 'blue',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: '#fff'
        },
        { // grey
    
          backgroundColor: '#ff9a9a',
          borderColor: '#fff',
          pointBackgroundColor: 'blue',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: '#fff'
        }
      ]
    
    
      public randomize(): void {
        // Only Change 3 values
        let data = [
          Math.round(Math.random() * 100),
          59,
          80,
          (Math.random() * 100),
          56,
          (Math.random() * 100),
          40];
        let clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = data;
        this.barChartData = clone;
      }

      getDailySkilledUnskilled(){
        let obj ={
          compId : this.loggedUser.compId
          // fromDate :new Date(new Date().setDate(1)),
          // toDate : new Date()
        }
        this.chartFlag = false;
        this.service.getDailySkilledUnskilled(obj).subscribe(data=>{
          this.skilledUnskilled = data.result;
          let skilled = new Array();
          let unskilled = new Array();
          let semiSkilled = new Array();
          let labelObj = new Array();
    
          for (let i = 0; i < this.skilledUnskilled.length ; i++) {
            skilled.push(this.skilledUnskilled[i].skilled);
            unskilled.push(this.skilledUnskilled[i].unSkilled);
            semiSkilled.push(this.skilledUnskilled[i].semiSkilled);
            labelObj.push(this.skilledUnskilled[i].shiftName.trim());
          }
          this.dailyBarChartData = [
            { data: skilled, label: 'Skilled' },
            { data: unskilled, label: 'Unskilled' },
            { data: semiSkilled, label: 'Semiskilled' }
          ];
          // this.barChartLabels = labelObj;
          this.barChartData = this.dailyBarChartData;
          
          // this.getDailyLateSittingEmployees();
          this.chartFlag = true;
          // this.barChartLabels = labelObj;
        },err=>{
          this.barChartData = [];
          this.barChartLabels = [];
          this.chartFlag = true;
        })
      }
    }
