import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-apsentismchart2',
  templateUrl: './apsentismchart2.component.html',
  styleUrls: ['./apsentismchart2.component.css']
})
export class Apsentismchart2Component {

  title = 'doughnutChart';
  //  public doughnutChartLabels:string[] = ['Unused', 'Used', 'Lapsed'];
   public doughnutChartData:number[] = [250, 380];
   public doughnutChartType:string = 'pie';
   public pieChartLabels : string[] = ["Absent %","Present %"];
  loggedUser: any;
   
   constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getConvertedAbsentism();
   }
   getConvertedAbsentism(){
     this.service.getConvertedAbsentism(this.loggedUser.compId).subscribe(data=>{
       this.doughnutChartData = [data.result.conAbsent,data.result.conPresent];       
     },err=>{
      this.doughnutChartData = [];
     })
   }
   // events
   public chartClicked(e:any):void {
   }
  
   public chartHovered(e:any):void {
   }
}
