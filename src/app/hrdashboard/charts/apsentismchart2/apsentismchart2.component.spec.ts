import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Apsentismchart2Component } from './apsentismchart2.component';

describe('Apsentismchart2Component', () => {
  let component: Apsentismchart2Component;
  let fixture: ComponentFixture<Apsentismchart2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Apsentismchart2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Apsentismchart2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
