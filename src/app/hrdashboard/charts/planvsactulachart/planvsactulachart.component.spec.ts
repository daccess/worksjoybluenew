import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanvsactulachartComponent } from './planvsactulachart.component';

describe('PlanvsactulachartComponent', () => {
  let component: PlanvsactulachartComponent;
  let fixture: ComponentFixture<PlanvsactulachartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanvsactulachartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanvsactulachartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
