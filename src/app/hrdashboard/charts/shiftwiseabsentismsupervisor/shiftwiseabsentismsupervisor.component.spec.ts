import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftwiseabsentismsupervisorComponent } from './shiftwiseabsentismsupervisor.component';

describe('ShiftwiseabsentismsupervisorComponent', () => {
  let component: ShiftwiseabsentismsupervisorComponent;
  let fixture: ComponentFixture<ShiftwiseabsentismsupervisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftwiseabsentismsupervisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftwiseabsentismsupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
