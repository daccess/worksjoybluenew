import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-shiftwiseabsentismsupervisor',
  templateUrl: './shiftwiseabsentismsupervisor.component.html',
  styleUrls: ['./shiftwiseabsentismsupervisor.component.css']
})
export class ShiftwiseabsentismsupervisorComponent  {

  title = 'doughnutChart';
   public pieChartLabels:string[] = ['First', 'Second', 'Third'];
   public pieChartData:number[] = [200, 300,250];
   public pieChartType:string = 'pie';
  //  public pieChartColor:string[]= ["red", "orange","yellow"];
   public pieChartOptions:any ={'backgroundColor': [
    "orange",
 "yellow",
 "red"
 
 ]}

 loggedUser: any;
 shiftWiseAbsentism: any;
 chartFlag: boolean;

 constructor(private service : HrdashboardService){
   let user = sessionStorage.getItem('loggedUser');
   this.loggedUser = JSON.parse(user);
 this.getDailyShiftWiseAbsentism();
 }
   // events
   public chartClicked(e:any):void {
   }
  
   public chartHovered(e:any):void {
   }

   getDailyShiftWiseAbsentism(){
    let obj ={
      compId : this.loggedUser.compId
    };
    this.chartFlag = false;
    this.service.getShiftWiseAbsentism(obj).subscribe(data=>{
      this.shiftWiseAbsentism = data.result;
      let dataObj = new Array();
      let labelObj = new Array();
      for (let i = 0; i < this.shiftWiseAbsentism.length; i++) {
        dataObj.push(this.shiftWiseAbsentism[i].absent);
        labelObj.push(this.shiftWiseAbsentism[i].shiftName.trim());
      }
      this.pieChartLabels = labelObj;
      this.pieChartData = dataObj;
      this.chartFlag = true;
      
    },err=>{
      this.pieChartLabels = [];
      this.pieChartData = [];
      this.chartFlag = false;
    })
  }
  getMonthlyShiftWiseAbsentism(){
   let obj ={
    compId : this.loggedUser.compId,
    fromDate :new Date(new Date().setDate(1)),
    toDate : new Date()
  };
  this.chartFlag = false;
  this.service.getMonthlyShiftWiseAbsentism(obj).subscribe(data=>{
    this.shiftWiseAbsentism = data.result;
    let dataObj = new Array();
    let labelObj = new Array();
    for (let i = 0; i < this.shiftWiseAbsentism.length; i++) {
      dataObj.push(this.shiftWiseAbsentism[i].absent);
      labelObj.push(this.shiftWiseAbsentism[i].shiftName.trim());
    }
    this.pieChartLabels = labelObj;
    this.pieChartData = dataObj;
    this.chartFlag = true;
    
  },err=>{
    this.pieChartLabels = [];
    this.pieChartData = [];
    this.chartFlag = false;
  })
  }
  getWeeklyShiftWiseAbsentism(){
    var curr = new Date(); // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6
    var weekStartDate = new Date(curr.setDate(first)).toUTCString();
    var weekEndDate = new Date(curr.setDate(last)).toUTCString();
    let obj ={
      compId : this.loggedUser.compId,
      fromDate : weekStartDate,
      toDate : weekEndDate
    };
    this.chartFlag = false;
    this.service.getWeeklyShiftWiseAbsentism(obj).subscribe(data=>{
      this.shiftWiseAbsentism = data.result;
      let dataObj = new Array();
      let labelObj = new Array();
      for (let i = 0; i < this.shiftWiseAbsentism.length; i++) {
        dataObj.push(this.shiftWiseAbsentism[i].absent);
        labelObj.push(this.shiftWiseAbsentism[i].shiftName.trim());
      }
      this.pieChartLabels = labelObj;
      this.pieChartData = dataObj;
      this.chartFlag = true;
      
    },err=>{
      this.pieChartLabels = [];
      this.pieChartData = [];
      this.chartFlag = false;
    })
  }
  dailyShiftWiseAbsentismData = [];
  monthlyShiftWiseAbsentismData = [];
  weeklyShiftWiseAbsentismData = [];

  dailyShiftWiseAbsentismLabels = [];
  monthlyShiftWiseAbsentismLabels = [];
  weeklyShiftWiseAbsentismLabels = [];
}