import { Component, OnInit } from '@angular/core';
import { HrdashboardService } from '../../services/hrdashboard.service';

@Component({
  selector: 'app-absentismchart',
  templateUrl: './absentismchart.component.html',
  styleUrls: ['./absentismchart.component.css']
})
export class AbsentismchartComponent  {

  title = 'doughnutChart';
  //  public doughnutChartLabels:string[] = ['Unused', 'Used', 'Lapsed'];
   public pieChartData:number[] = [100,150];
   public pieChartType:string = 'pie';
   public pieChartLabels : string[] = ["Absent %","Present %"]
  loggedUser: any;
   // events
   constructor(private service : HrdashboardService){
    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.getActualAbsentism();
   }
   getActualAbsentism(){
     this.service.getActualAbsentism(this.loggedUser.compId).subscribe(data=>{
       this.pieChartData = [data.result.absentPer,data.result.presentPer];       
     },err=>{
      this.pieChartData = [];
     })
   }
   public chartClicked(e:any):void {
   }
  
   public chartHovered(e:any):void {
   }
 
 }