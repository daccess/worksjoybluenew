import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbsentismchartComponent } from './absentismchart.component';

describe('AbsentismchartComponent', () => {
  let component: AbsentismchartComponent;
  let fixture: ComponentFixture<AbsentismchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbsentismchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbsentismchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
