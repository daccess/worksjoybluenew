import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreedaysabsentismchartComponent } from './threedaysabsentismchart.component';

describe('ThreedaysabsentismchartComponent', () => {
  let component: ThreedaysabsentismchartComponent;
  let fixture: ComponentFixture<ThreedaysabsentismchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreedaysabsentismchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreedaysabsentismchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
