import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from '../../app/login/login.component';

import { LayoutComponent } from 'src/app/layout/layout.component';
import { HolidaymasterComponent } from 'src/app/holidaymaster/holidaymaster.component';
import { HrdashboardComponent } from './hrdashboard.component';


const appRoutes: Routes = [
    { 
             
        path: '', component: HrdashboardComponent,

            children:[
                {
                    path:'',
                    redirectTo : 'hrdashboard',
                    pathMatch :'full'
                    
                },
            
                {
                    path:'hrdashboard',
                    component: HrdashboardComponent
                }

            ]

    }
  
    // otherwise redirect to home
 
]

export const routing = RouterModule.forChild(appRoutes);