import { Component, OnDestroy, OnInit } from '@angular/core';
import { HrdashboardService } from './services/hrdashboard.service';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { MainURL } from '../shared/configurl';
import { Http } from '@angular/http';
import * as moment from 'moment';
import {dataService} from '../shared/services/dataService';
declare var $: any;
@Component({
  selector: 'app-hrdashboard',
  templateUrl: './hrdashboard.component.html',
  styleUrls: ['./hrdashboard.component.css']
})
export class HrdashboardComponent implements OnInit {
  baseurl = MainURL.HostUrl;

  LocationList: any = [];
  loggedUser: any;
  MonthlyARCount = {};
  maxArRequests = [{
    totalCount: "",
    approvedCount: "",
    pendingCount: ""
  }];
  genderDailyCount = [
    {
      maleCount: "",
      femaleCount: ""
    }
  ];
  genderMonthlyCount = [];
  maleCount: number = 0;
  femaleCount: number = 0;
  gratuityEntitlementCount: any = [];

  totalHeadcount: boolean = false;
  allRequests: boolean = false;
  shiftDeviationHabitualAbsentism: boolean = false;
  shiftwiseAbsentism: boolean = false;
  coffDetails: boolean = false;
  absentism: boolean = false;
  gratuityEntitlement: boolean = false;
  lateSitting: boolean = false;
  skiledUnskilled: boolean = false;
  fteVsTemp: boolean = false;
  gender: boolean = false;
  allowances: boolean = false;
  absentismTrend: boolean = false;
  absentismTrendContinuously: boolean = false;
  coffDetailsSupervisor: boolean = false;
  shiftwiseAbsentismSupervisor: boolean = false;
  esicApplicable: boolean = false;
  shiftDeviation: boolean = false;
  lateSittingOtList: boolean = false;
  OtHoursIncrease: boolean = false;
  DepartmentWiseManHoursCalculation: boolean = false;
  shiftwiseAttendance: boolean = false;

  themeColor: string = "nav-pills-blue";

  usersSummaryInfo: any;
  emp_list: any;
  chkTime: any;
  searchQuery: string = "";
  FirstLevelAlldata = [];
  totEmpCnt: number = 0;
  totContCnt: number = 0;
  allTotCnt: number = 0;
  totalVisiCnt: number = 0;
  empTodayOnLeavedata: any;
  emplatemarkedData: any;
  rollName: any;
  AllLocationwisedata: any[];
  Employeedata: any[];
  orgEmpList: any;
  compId: any;
  empPerId: any;
  EmployeedataDepartmentbyid: any[];
  roleId: any;
  empId: any;
  EmployeeLocationwise: any[];
  selectedLocationId: any=0;
  selectedlocationids: any=0;
  selectedLocationobj: any=0;
  locationdata: any;
  roHead: any;
  selectedemps: any;

  constructor(private service: HrdashboardService, private http: Http,  private dataService: dataService,public httpService: HttpClient) {

    let user = sessionStorage.getItem('loggedUser');
    this.loggedUser = JSON.parse(user);
    this.compId = this.loggedUser.compId;
    this.empPerId = this.loggedUser.empPerId;
    this.roHead=this.loggedUser.roleHead;
    this.Employeedata = this.EmployeedataDepartmentbyid;
    this.roleId=this.loggedUser.rollMasterId
    this.empId=this.loggedUser.empId,
    // console.log("values",this.compId)
    // console.log("values",this.roleId)  
    // console.log("values",this.empId) 


    this.getMonthlyARCount();
    this.getMaxArRequests();
    this.getGenderDailyCount();
    this.getGratuityEntitlementCount();
    this.getTodaysPlannedAbsentism();
    // this.getTodaysUnPlannedAbsentism();
    // this.getHabitualAbsentism();
    this.getDepartmentsById();
    this.getEsicCountById();
    this.getLateSittingEmpList();
    this.getoverTimeEmpList();
    this.getLocationList();
    this.filterItem(this.selectedLocationobj)
  

    let themeColor = sessionStorage.getItem('themeColor');
    if (themeColor) {
      this.themeColor = themeColor;
    }
  }

  ngOnInit() {
    // this.empFlag = true;
    this.searchQuery = this.loggedUser.empId + "-" + this.loggedUser.fullName + " " + this.loggedUser.lastName;
    let obj = {
      "fullName": this.loggedUser.fullName,
      "lastName": this.loggedUser.lastName,
      "empId": this.loggedUser.empId
    }
    // this.getAllPendReq(obj);
    this.selectRole();
   
    this.summaryInfoOfEmpAndContractor(this.selectedLocationId);
    this.LocationNameWithCount(this.selectedLocationId);
    this.getEmpListOnLeaveToday(this.selectedlocationids)
    this.todaysLateMarkedEmpList(this.selectedLocationId)
  this.getallLocation()
 
  }
  
  getallLocation(){
    
    var url = this.baseurl + "/Location/";
    this.httpService.get(url + this.compId).subscribe((data) => {
      this.EmployeeLocationwise = data["result"];
      

    });
  
 
    
  }


  selectRole() {
    this.totalHeadcount = false;
    this.allRequests = false;
    this.shiftDeviationHabitualAbsentism = false;
    this.shiftwiseAbsentism = false;
    this.coffDetails = false;
    this.absentism = false;
    this.gratuityEntitlement = false;
    this.lateSitting = false;
    this.skiledUnskilled = false;
    this.fteVsTemp = false;
    this.gender = false;
    this.allowances = false;
    this.absentismTrend = false;
    this.absentismTrendContinuously = false;
    this.coffDetailsSupervisor = false;
    this.shiftwiseAbsentismSupervisor = false;
    this.esicApplicable = false;
    this.shiftDeviation = false;
    this.lateSittingOtList = false;
    this.OtHoursIncrease = false;
    this.DepartmentWiseManHoursCalculation = false;
    this.shiftwiseAttendance = false;

    let url = this.baseurl + '/getcustomerhrdashboard/';
    this.http.get(url + this.loggedUser.rollMasterId).subscribe(data => {
      let allData = data.json().result;
      for (let i = 0; i < allData.length; i++) {
        if (allData[i].reportHrId == 1) {
          this.totalHeadcount = true;
        }
        if (allData[i].reportHrId == 2) {
          this.allRequests = true;
        }
        if (allData[i].reportHrId == 3) {
          this.shiftDeviationHabitualAbsentism = true;
        }
        if (allData[i].reportHrId == 4) {
          this.shiftwiseAbsentism = true;
        }
        if (allData[i].reportHrId == 5) {
          this.coffDetails = true;
        }
        if (allData[i].reportHrId == 6) {
          this.absentism = true;
        }
        if (allData[i].reportHrId == 7) {
          this.gratuityEntitlement = true;
        }
        if (allData[i].reportHrId == 8) {
          this.lateSitting = true;
        }
        if (allData[i].reportHrId == 9) {
          this.skiledUnskilled = true;
        }
        if (allData[i].reportHrId == 10) {
          this.fteVsTemp = true;
        }
        if (allData[i].reportHrId == 11) {
          this.gender = true;
        }
        if (allData[i].reportHrId == 12) {
          this.allowances = true;
        }
        if (allData[i].reportHrId == 13) {
          this.absentismTrend = true;
        }
        if (allData[i].reportHrId == 14) {
          this.absentismTrendContinuously = true;
        }
        if (allData[i].reportHrId == 15) {
          this.coffDetailsSupervisor = true;
        }
        if (allData[i].reportHrId == 16) {
          this.shiftwiseAbsentismSupervisor = true;
        }
        if (allData[i].reportHrId == 17) {
          this.esicApplicable = true;
        }
        if (allData[i].reportHrId == 18) {
          this.shiftDeviation = true;
        }
        if (allData[i].reportHrId == 19) {
          this.lateSittingOtList = true;
        }
        if (allData[i].reportHrId == 20) {
          this.OtHoursIncrease = true;
        }
        if (allData[i].reportHrId == 21) {
          this.DepartmentWiseManHoursCalculation = true;
        }
        if (allData[i].reportHrId == 21) {
          this.shiftwiseAttendance = true;
        }
      }
    }, (err: HttpErrorResponse) => {
      this.totalHeadcount = false;
      this.allRequests = false;
      this.shiftDeviationHabitualAbsentism = false;
      this.shiftwiseAbsentism = false;
      this.coffDetails = false;
      this.absentism = false;
      this.gratuityEntitlement = false;
      this.lateSitting = false;
      this.skiledUnskilled = false;
      this.fteVsTemp = false;
      this.gender = false;
      this.allowances = false;
      this.absentismTrend = false;
      this.absentismTrendContinuously = false;
      this.coffDetailsSupervisor = false;
      this.shiftwiseAbsentismSupervisor = false;
      this.esicApplicable = false;
      this.shiftDeviation = false;
      this.lateSittingOtList = false;
      this.OtHoursIncrease = false;
      this.DepartmentWiseManHoursCalculation = false;
      this.shiftwiseAttendance = false;
    }
    );
    
   
  }
 


  selectEmpId: string = "";

  allPendingReqData = [];
  empFlag: boolean = false;
  filterItem(selectedLocationobj) {
  
    if(this.loggedUser.roleHead=='HR'){
   
      this.empFlag = false;
      this.searchQuery = null;
      this.allRequestData = [];
      this.allPendingReqData = [];
   
   
      let url = this.baseurl + '/EmployeeOperationforAdmin';
      this.http.get(url +'/'+this.loggedUser.locationId).subscribe(data => {
        this.FirstLevelAlldata = data.json()["result"];
        this.empFlag = true;
      },
        (err: HttpErrorResponse) => {
          this.FirstLevelAlldata = [];
          this.empFlag = false;
        });
    
  }
  else  if(this.loggedUser.roleHead=='Admin'){
    if (!selectedLocationobj) {
      this.empFlag = false;
      this.searchQuery = null;
      this.allRequestData = [];
      this.allPendingReqData = [];
    }
   
      let url = this.baseurl + '/EmployeeOperationforAdmin/';
      this.http.get(url +'/'+this.selectedLocationobj).subscribe(data => {
        this.FirstLevelAlldata = data.json()["result"];
        //console.log("pendingrequestdata",this.FirstLevelAlldata );
        this.empFlag = true;
      },
        (err: HttpErrorResponse) => {
          this.FirstLevelAlldata = [];
          this.empFlag = false;
        });
    }
  }
  

  arrowkeyLocation = 0;
  keyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
        this.arrowkeyLocation--;
        break;
      case 40: // this is the ascii of arrow down
        this.arrowkeyLocation++;
        break;
    }
  }
  allRequestData = [];
  getAllPendReq(item) {
    this.selectedemps=item.target.value
    let url = this.baseurl + '/getPendingRegularisation/' + this.selectedemps;

    this.http.get(url).subscribe(data => {
      this.empFlag = false;
      this.searchQuery = item.fullName + " " + item.lastName;
      // this.FirstLevelAlldata = [];
      this.allRequestData = [];
      this.allPendingReqData = data.json()["result"][0];
      if (data.json()["result"][0].leaveCount) {
        this.allRequestData.push({ request: "Leave Request", count: data.json()["result"][0].leaveCount })
      }
      if (data.json()["result"][0].missPunchCount) {
        this.allRequestData.push({ request: "Miss Punch", count: data.json()["result"][0].missPunchCount })
      }
      if (data.json()["result"][0].permissionCount) {
        this.allRequestData.push({ request: "Permission Request", count: data.json()["result"][0].permissionCount })
      }
      if (data.json()["result"][0].lateEarlyCount) {
        this.allRequestData.push({ request: "Late Early Request", count: data.json()["result"][0].lateEarlyCount })
      }
      if (data.json()["result"][0].onDutyCount) {
        this.allRequestData.push({ request: "On Duty Request", count: data.json()["result"][0].onDutyCount })
      }
      if (data.json()["result"][0].workFromHomeCount) {
        this.allRequestData.push({ request: "Work From Home Request", count: data.json()["result"][0].workFromHomeCount })
      }
      if (data.json()["result"][0].extraWorkCount) {
        this.allRequestData.push({ request: "Extra Work Request", count: data.json()["result"][0].extraWorkCount })
      }
      if (data.json()["result"][0].coffCount) {
        this.allRequestData.push({ request: "Compensatory Off Request", count: data.json()["result"][0].coffCount })
      }
      if (data.json()["result"][0].soffCount) {
        this.allRequestData.push({ request: "Stagger Off Request", count: data.json()["result"][0].soffCount })
      }
      if (data.json()["result"][0].safetyCount) {
        this.allRequestData.push({ request: "Safety Count", count: data.json()["result"][0].safetyCount })
      }
      if (data.json()["result"][0].clearanceCount) {
        this.allRequestData.push({ request: "Resignation Count", count: data.json()["result"][0].clearanceCount })
      }
      if (data.json()["result"][0].medicalCount) {
        this.allRequestData.push({ request: "Medical Count", count: data.json()["result"][0].medicalCount })
      }
      if (data.json()["result"][0].optionalHoliCount) {
        this.allRequestData.push({ request: "Medical Count", count: data.json()["result"][0].optionalHoliCount })
      }
      if (data.json()["result"][0].totalCancelCount) {
        this.allRequestData.push({ request: "Total Cancel Request", count: data.json()["result"][0].totalCancelCount })
      }
      if (data.json()["result"][0].expenseCount) {
        this.allRequestData.push({ request: "Expense Count", count: data.json()["result"][0].expenseCount })
      }
    },
      (err: HttpErrorResponse) => {
        this.allRequestData = [];
        this.allPendingReqData = [];
        this.empFlag = false;
        this.searchQuery = item.fullName + " " + item.lastName;
      });

  }
  getMonthlyARCount() {
    this.service.getMonthlyARCount(this.loggedUser.compId).subscribe(data => {
      this.MonthlyARCount = data.result;
    }, err => {
    })
  }

  getMaxArRequests() {
    this.service.getMaxArRequests(this.loggedUser.compId).subscribe(data => {
      this.maxArRequests = data.result;
      let temp;
      let count = 0;
      for (let i = 0; i < data.result.hrDashboardRes1Dto.length; i++) {
        for (let j = 0; j < data.result.hrDashboardRes1Dto.length; j++) {
          if (i != j) {
            if (data.result.hrDashboardRes1Dto[i].totalCount > data.result.hrDashboardRes1Dto[j].totalCount) {
              temp = data.result.hrDashboardRes1Dto[i];
              data.result.hrDashboardRes1Dto[i] = data.result.hrDashboardRes1Dto[j];
              data.result.hrDashboardRes1Dto[j] = temp;
              count++;
            }
          }
        }
        count = 0;
      }
      this.maxArRequests = data.result.hrDashboardRes1Dto;
    }, err => {
    })
  }
  genderActive: string = 'daily';
  getGenderDailyCount() {
    this.genderActive = 'daily';
    let obj = {
      compId: this.loggedUser.compId
    }
    this.service.getGenderDailyCount(obj).subscribe(data => {

      this.genderDailyCount = data.result;
      if (data.result.length) {
        this.maleCount = data.result[0].maleCount;
        this.maleCount = data.result[1].maleCount;
        this.femaleCount = data.result[0].femaleCount;
        //  this.femaleCount = data.result[1].femaleCount;
      }
      else {
        this.maleCount = 0;
        this.femaleCount = 0;
      }

    }, err => {
      this.maleCount = 0;
      this.femaleCount = 0;
    }
    )
  }

  getGenderMonthlyCount() {
    this.genderActive = 'monthly';
    let obj = {
      compId: this.loggedUser.compId,
      fromDate: new Date(new Date().setDate(1)),
      toDate: new Date()
    }
    this.service.getGenderMonthlyCount(obj).subscribe(data => {
      this.genderDailyCount = data.result;
      this.maleCount = data.result.maleCount;
      this.femaleCount = data.result.femaleCount;
    }, err => {
      this.maleCount = 0;
      this.femaleCount = 0;
    })
  }
  widthS1 = 10;
  widthS2 = 10;
  widthS3 = 10;
  getGratuityEntitlementCount() {
    let obj = {
      compId: this.loggedUser.compId
    }
    this.service.getGratuityEntitlementCount(obj).subscribe(data => {
      this.gratuityEntitlementCount = data.result;
      let total = this.gratuityEntitlementCount.employee3to5Years + this.gratuityEntitlementCount.employee5Years + this.gratuityEntitlementCount.employeeLess3Years;
      this.widthS1 = (this.gratuityEntitlementCount.employee3to5Years / total) * 100;
      this.widthS2 = (this.gratuityEntitlementCount.employee5Years / total) * 100;
      this.widthS3 = (this.gratuityEntitlementCount.employeeLess3Years / total) * 100;
      if (this.widthS1 < 30) {
        this.widthS1 = this.widthS1 + 30;
      }
      if (this.widthS2 < 30) {
        this.widthS2 = this.widthS2 + 30;
      }
      if (this.widthS3 < 30) {
        this.widthS3 = this.widthS3 + 30;
      }
    }, err => {
      this.gratuityEntitlementCount = [];
      this.widthS1 = 50;
      this.widthS2 = 50;
      this.widthS3 = 50;
    })
  }
  lateEarlyActive: string = "daily";
  lateEarlyMonthly(val) {
    this.lateEarlyActive = val;
  }
  skilledUnskilledActive: string = "daily";
  skilledUnskilledActivity(val) {
    this.skilledUnskilledActive = val;
  }
  todaysPlannedAbsentism = [];
  todaysUnPlannedAbsentism = [];
  habitualAbsentism: any;
  getTodaysPlannedAbsentism() {
    this.service.getTodaysPlannedAbsentism(this.loggedUser.compId).subscribe(data => {
      this.todaysPlannedAbsentism = data.result[1];
    }, err => {
    })
  }

  // getTodaysUnPlannedAbsentism() {
  //   var table;
  //   // var table = $('#unplannedAbsentismId').DataTable().destroy(true);
  //   // $('#unplannedAbsentismId').DataTable().search('');
  //   this.todaysUnPlannedAbsentism = [];

  //   this.service.getTodaysUnPlannedAbsentism(this.loggedUser.compId).subscribe(data => {
  //     this.todaysUnPlannedAbsentism = data.result[1];
  //     var t2 = $('#unplannedAbsentismId').DataTable().search('');
  //     setTimeout(function () {
  //       $(function () {
  //          table = $('#unplannedAbsentismId').DataTable({
  //           "language": {
  //             "info": "Showing page _PAGE_ of _PAGES_"
  //           },
  //           columnDefs: [{
  //             targets: "_all",
  //             orderable: false
  //          }],
  //          destroy: true
  //          });
  //       });
  //     }, 1000);
  //   }, err => {
  //   })
  // }
  habitualAbsentismList = [];
  // getHabitualAbsentism() {
  //   var table;
  //   this.service.getHabitualAbsentism(this.loggedUser.compId).subscribe(data => {
  //     var t3 = $('#unplannedAbsentismId').DataTable().search('');
  //     this.habitualAbsentism = data.result.length;
  //     this.habitualAbsentismList = data.result;
  //     setTimeout(function () {
  //       $(function () {
  //          table = $('#habitualAbsentismId').DataTable({
  //           destroy: true,
  //           "language": {
  //             "info": "Showing page _PAGE_ of _PAGES_"
  //           },
  //           // "order": [[ 2, "desc" ]],
  //           columnDefs: [{
  //             targets: "_all",
  //             orderable: false
  //          }]
  //          });
  //       });
  //     }, 10);

  //   }, err => {
  //   })
  // }
  departmentList = [];
  selectedDepertment: any;
  getDepartmentsById() {
    this.service.getDepartmentsById(this.loggedUser.compId).subscribe(data => {
      this.departmentList = data.result;
      this.selectedDepertment = data.result[2].deptId;
      this.selectedDepertmentManHours = data.result[2].deptId;
    }, err => {
    })
  }


  value(e) {
    this.selectedDepertment = e;
  }

  selectedDepertmentManHours: any;
  valueChange(e) {
    this.selectedDepertmentManHours = e;
  }
  esicCount: any;
  getEsicCountById() {
    this.service.getEsicCountById(this.loggedUser.compId).subscribe(data => {
      this.esicCount = data.result;
    }, err => {
    });
  }
  fteActive: string = 'daily';
  makefFteActive(val) {
    this.fteActive = val;
  }

  lateSittingList = [];
  overTimeList = [];
  getLateSittingEmpList() {
    this.service.getLateSittingEmpList(this.loggedUser.compId).subscribe(data => {
      this.lateSittingList = data.result;
    }, err => {
    });
  }

  getoverTimeEmpList() {
    this.service.getoverTimeEmpList(this.loggedUser.compId).subscribe(data => {
      this.overTimeList = data.result;
    }, err => {
    });
  }
  shiftWiseAbsentismActiveFlag: string = 'daily';
  shiftWiseAbsentismActive(val) {
    this.shiftWiseAbsentismActiveFlag = val;
  }

  //To get the Summary Information of Employee and contractor
  summaryInfoOfEmpAndContractor(e) {
    this.selectedLocationId=e;
    if(this.loggedUser.roleHead=='HR'){
      let url = '/summaryOfEmpAndContractor/'
      this.service.getsummaryOfEmpAndContractor(url + this.loggedUser.locationId).subscribe(data => {
        this.usersSummaryInfo = data['result']
        console.log("admincount", this.usersSummaryInfo);
  
  
      });
   
  }
  else if(this.loggedUser.roleHead=='Admin'){
    let url = '/summaryOfEmpAndContractor/'
      this.service.getsummaryOfEmpAndContractor(url + this.selectedLocationId).subscribe(data => {
        this.usersSummaryInfo = data['result']
        //console.log("admincount", this.usersSummaryInfo);
  
  
      });

  }
}
  //modals
  showCountModal(name) {

    if (name == 'insideEmployee') {
      this.emp_list = [];
      var table1 = $('#Emp').DataTable().destroy();
      this.emp_list = this.usersSummaryInfo.insideEmployeeList;
      setTimeout(function () {
        $("#insideEmployeeList").modal('show');
        $(function () {
          var table = $('#Emp').DataTable().destroy();
        });
      }, 1000);
    }
    else if (name == 'insideContractor') {
      this.emp_list = [];
      var table1 = $('#Emp_contractor').DataTable().destroy();
      this.emp_list = this.usersSummaryInfo.insideContractorList;
      setTimeout(function () {
        $("#insideContractorList").modal('show');
        $(function () {
          var table = $('#Emp_contractor').DataTable().destroy();
        });
      }, 1000);
    }
    else if (name == 'yesterSinglePunchContractor') {
      this.emp_list = [];
      var table1 = $('#Emp_cont_single').DataTable().destroy();
      this.emp_list = this.usersSummaryInfo.yesterSinglePunchContractorList;
      setTimeout(function () {
        $("#yesterSinglePunchContractor").modal('show');
        $(function () {
          var table = $('#Emp_cont_single').DataTable().destroy();
        });
      }, 1000);
    }
    else if (name == 'yesterSinglePunchEmp') {
      this.emp_list = [];
      var table1 = $('#Emp_cont_yest').DataTable().destroy();
      this.emp_list = this.usersSummaryInfo.yesterSinglePunchEmpList;
      setTimeout(function () {
        $("#yesterSinglePunchEmp").modal('show');
        $(function () {
          var table = $('#Emp_cont_yest').DataTable().destroy();
        });
      }, 1000);
    }
  }
  getCheckInTime(timestamp) {
    if (timestamp != null) {
      let temp_in_time = new Date(timestamp);
      this.chkTime = moment(temp_in_time).format('HH.mm');
      return this.chkTime;
    }

  }

  closeModal() {

    var t2 = $('#unplannedAbsentismId').DataTable().search('');
    // this.getTodaysUnPlannedAbsentism();
  }
  closeHabitualModal() {
    var t3 = $('#habitualAbsentismId').DataTable().search('');
    //  this.getHabitualAbsentism();
  }

  LocationNameWithCount(e) {
    this.selectedLocationId=e;
    if(this.loggedUser.roleHead=='HR'){

    let url = '/getLocationWisePrsenetCount1/';
    //  this.dataTableFlag = false;
    //console.log("locid",this.loggedUser.locationId);
  
    this.service.getLocationWithCounts(url+this.compId+'/'+this.loggedUser.locationId).subscribe(
      data => {
        this.LocationList = data.result;
        //console.log("LocationListadmin ",this.LocationList);
       

        for (let i = 0; i < this.LocationList.length; i++) {
          this.totEmpCnt = this.totEmpCnt + this.LocationList[i].employeeCount;
          this.totContCnt = this.totContCnt + this.LocationList[i].contractorCount;
          this.totalVisiCnt = this.totalVisiCnt + this.LocationList[i].visitorCount;
        }
        this.allTotCnt = this.totEmpCnt + this.totContCnt;
        //  this.LocationList[this.LocationList.length].employee = this.totEmpCnt;
        //  this.LocationList[this.LocationList.length].contractor =  this.totContCnt;
        //  this.LocationList[this.LocationList.length].locationName = "";
        //  this.LocationList[this.LocationList.length].count =  this.allTotCnt;
        //  let obj = {
        //   "employee": this.totEmpCnt,
        //   "contractor":  this.totContCnt,
        //   "locationName": "",
        //   "count": this.allTotCnt
        //  }
        //  this.LocationList.push(obj);

        //onsole.log("Locationlistdata123", data)
        // this.dataTableFlag = true
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable({ searching: false });
          });
        }, 1000);
      },
      (err: HttpErrorResponse) => {
        console.log("data",err.message);
      }
    );
    }
    else if(this.loggedUser.roleHead=='Admin'){
      let url = '/getLocationWisePrsenetCount1/';
    //  this.dataTableFlag = false;
    //console.log("locid",this.loggedUser.locationId);
  
    this.service.getLocationWithCounts(url+this.compId+'/'+this.selectedLocationId).subscribe(
      data => {
        this.LocationList = data.result;
       // console.log("LocationListadmin***** ",this.LocationList);
       

        for (let i = 0; i < this.LocationList.length; i++) {
          this.totEmpCnt = this.totEmpCnt + this.LocationList[i].employeeCount;
          this.totContCnt = this.totContCnt + this.LocationList[i].contractorCount;
          this.totalVisiCnt = this.totalVisiCnt + this.LocationList[i].visitorCount;
        }
        this.allTotCnt = this.totEmpCnt + this.totContCnt;
        //  this.LocationList[this.LocationList.length].employee = this.totEmpCnt;
        //  this.LocationList[this.LocationList.length].contractor =  this.totContCnt;
        //  this.LocationList[this.LocationList.length].locationName = "";
        //  this.LocationList[this.LocationList.length].count =  this.allTotCnt;
        //  let obj = {
        //   "employee": this.totEmpCnt,
        //   "contractor":  this.totContCnt,
        //   "locationName": "",
        //   "count": this.allTotCnt
        //  }
        //  this.LocationList.push(obj);

        //onsole.log("Locationlistdata123", data)
        // this.dataTableFlag = true
        setTimeout(function () {
          $(function () {
            var table = $('#CompTable').DataTable({ searching: false });
          });
        }, 1000);
      },
      (err: HttpErrorResponse) => {
        console.log("data",err.message);
      }
    );









  }
}
  getEmpListOnLeaveToday(e) {
    this.selectedlocationids=e;
    if(this.loggedUser.roleHead=='HR'){
    let url = '/TodaysOnLeaveHRDashborad/'
    this.service.getAbsentTodaysEmp(url + this.loggedUser.locationId).subscribe(data => {
      this.empTodayOnLeavedata = data['result']

    })
  }
  else   if(this.loggedUser.roleHead=='Admin'){
    let url = '/TodaysOnLeaveHRDashborad/'
    this.service.getAbsentTodaysEmp(url + this.selectedlocationids).subscribe(data => {
      this.empTodayOnLeavedata = data['result']

    })
  }
  }

  todaysLateMarkedEmpList(e) {
    this.selectedLocationId=e;
    if(this.loggedUser.roleHead=='HR'){
    let url = '/TodaysLateMarked/'
    this.service.getTodaysLateMarkEmp(url + this.loggedUser.locationId).subscribe(data => {
      this.emplatemarkedData = data['result']
      console.log('emp data of leave on today***',this.emplatemarkedData);


    })
  }
  else if(this.loggedUser.roleHead=='Admin'){
    let url = '/TodaysLateMarked/'
    this.service.getTodaysLateMarkEmp(url + this.selectedLocationId).subscribe(data => {
      this.emplatemarkedData = data['result']
           console.log('emp data of leave on today***',this.emplatemarkedData);
    })
  }
}
ngValue1(event){
  this.selectedLocationobj = parseInt(event.target.value);
  console.log("selectedlocation",this.selectedLocationobj);
  this.filterItem(this.selectedLocationobj)
}
  
getLocationList(){

  let url = this.baseurl + '/Location/Active/'+this.compId;
  this.httpService.get(url).subscribe(data => {
      this.locationdata = data["result"];
     

    },
    (err: HttpErrorResponse) => {
    });
}

}