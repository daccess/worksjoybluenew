import { routing } from './hrdashboard.routing';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgCircleProgressModule } from 'ng-circle-progress';


import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { HrdashboardComponent } from './hrdashboard.component';
import { ChartsModule } from 'ng2-charts';
import { ShiftwiseAbsentismComponent } from './charts/shiftwise-absentism/shiftwise-absentism.component';
import { CoffchartComponent } from './charts/coffchart/coffchart.component';
import { AbsentismchartComponent } from './charts/absentismchart/absentismchart.component';
import { Apsentismchart2Component } from './charts/apsentismchart2/apsentismchart2.component';
import { SkilledchartComponent } from './charts/skilledchart/skilledchart.component';
import { FtechartComponent } from './charts/ftechart/ftechart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AbsentismtrendchartComponent } from './charts/absentismtrendchart/absentismtrendchart.component';
import { ThreedaysabsentismchartComponent } from './charts/threedaysabsentismchart/threedaysabsentismchart.component';
import { CoffsupervsorchartComponent } from './charts/coffsupervsorchart/coffsupervsorchart.component';
import { ShiftwiseabsentismsupervisorComponent } from './charts/shiftwiseabsentismsupervisor/shiftwiseabsentismsupervisor.component';
import { TotalheadcountchartComponent } from './charts/totalheadcountchart/totalheadcountchart.component';
import { PlanvsactulachartComponent } from './charts/planvsactulachart/planvsactulachart.component';
import { LateEarlyLateChartComponent } from './charts/late-early-late-chart/late-early-late-chart.component';
import { ShiftdeviationchartComponent } from './charts/shiftdeviationchart/shiftdeviationchart.component';
import { DepartmentwisemanhrsComponent } from './charts/departmentwisemanhrs/departmentwisemanhrs.component';
import { OthrsincreaseunplannedComponent } from './charts/othrsincreaseunplanned/othrsincreaseunplanned.component';
import { HrdashboardsharedModule } from '../hrdashboardshared/hrdashboardshared.module';
import { MinuteSecondsPipe } from '../supervisorleave-request/pipe/custom-time-pipe';


@NgModule({
    declarations: [
        HrdashboardComponent,
        // ShiftwiseAbsentismComponent,
        // CoffchartComponent,
        // AbsentismchartComponent,
        // Apsentismchart2Component,
        // SkilledchartComponent,
        // FtechartComponent,
        // AbsentismtrendchartComponent,
        // ThreedaysabsentismchartComponent,
        // CoffsupervsorchartComponent,
        // ShiftwiseabsentismsupervisorComponent,
        // TotalheadcountchartComponent,
        // PlanvsactulachartComponent,
        // LateEarlyLateChartComponent,
        // ShiftdeviationchartComponent,
        // DepartmentwisemanhrsComponent,
        // OthrsincreaseunplannedComponent
        MinuteSecondsPipe,
    ],
    imports: [
        routing,
        ToastrModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CommonModule,
        HttpClientModule,
        DataTablesModule,
        ChartsModule,
        NgxChartsModule,
        HrdashboardsharedModule,
       
         NgCircleProgressModule.forRoot({

        }),
        
    ],
    providers: []
})
export class HrdashboardModule {
    constructor() {

    }
}
