import { Injectable } from '@angular/core';
import { MainURL } from 'src/app/shared/configurl';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HrdashboardService {

  baseurl = MainURL.HostUrl
  requesLateEarlyId: any;
  constructor(private http : Http, private httpService:HttpClient) { 
    
  }

  // postCoffRequest(l){
  //   let url = this.baseurl + '/coffrequest';
  //   var body = JSON.stringify(l);
  //   var headerOptions = new Headers({'Content-Type':'application/json'});
  //   var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
  //   return this.http.post(url,body,requestOptions).map(x => x.json());
  // }
  getMonthlyARCount(id){
    let url = this.baseurl + '/atrequestcount/'+id;
    return this.http.get(url).map(x => x.json());
  }
  getMaxArRequests(id){
    let url = this.baseurl + '/maxatrequestcount/'+id;
    return this.http.get(url).map(x => x.json());
  }
  getActualAbsentism(id){
    let url = this.baseurl + '/getactualabsentism/'+id;
    return this.http.get(url).map(x => x.json());
  }
  getConvertedAbsentism(id){
    let url = this.baseurl + '/getconvertedabsentism/'+id;
    return this.http.get(url).map(x => x.json());
  }
  getGenderDailyCount(model){
    let url = this.baseurl + '/genderDailyCount';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getGenderMonthlyCount(model){
    let url = this.baseurl + '/genderMonthly';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getLocationwiseHeadCount(model){
    let url = this.baseurl + '/getLocationwiseHeadCount';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
 
  getGratuityEntitlementCount(model){
    let url = this.baseurl + '/gratuityEntitlement';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getMonthlyLateSittingEmployee(model){
    let url = this.baseurl + '/getMonthlyLateSittingEmployee';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getDailyLateSittingEmployee(model){
    let url = this.baseurl + '/getDailyLateSittingEmployee';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  
  getDailySkilledUnskilled(model){
    let url = this.baseurl + '/getDailySkilledEmployee';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getMonthlySkilledUnskilled(model){
    let url = this.baseurl + '/getDailySkilledEmployee';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }

  getShiftWiseAbsentism(model){
    let url = this.baseurl + '/getShiftWisePresentism/';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getShiftWisePresentism(url: string): Observable<any>
      {
            return this.httpService.get(`${this.baseurl + url}`);
        }

        getlocationname(url: string):Observable<any>{
       return this.httpService.get(`${this.baseurl + url}`)
        }
  getMonthlyShiftWiseAbsentism(model){
    let url = this.baseurl + '/getShiftwiseAbsentismMonthly';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }
  getWeeklyShiftWiseAbsentism(model){
    let url = this.baseurl + '/getShiftwiseAbsentismWeekly';
    var body = JSON.stringify(model);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(url,body,requestOptions).map(x => x.json());
  }


  getTodaysPlannedAbsentism(id){
    let url = this.baseurl + '/gettodaysabsentism/'+id;
    return this.http.get(url).map(x => x.json());
  }
  // getTodaysUnPlannedAbsentism(id){
  //   let url = this.baseurl + '/gettodaysunplannedabsentism/'+id;
  //   return this.http.get(url).map(x => x.json());
  // }
  // getHabitualAbsentism(id){
  //   let url = this.baseurl + '/habitualabsentism/'+id;
  //   return this.http.get(url).map(x => x.json());
  // }

  getDepartmentsById(id){
    let url = this.baseurl + '/Department/'+id;
    return this.http.get(url).map(x => x.json());
  }
  getCoffCountByDepartmentId(id){
    let url = this.baseurl + '/CountCOff/'+id;
    return this.http.get(url).map(x => x.json());
  }

  getEsicCountById(id){
    let url = this.baseurl + '/CountEsic/'+id;
    return this.http.get(url).map(x => x.json());
  }

  getFteVsTempCount(id){
    let url = this.baseurl + '/FteVsTempCount/'+id;
    return this.http.get(url).map(x => x.json());
  }

  getAbsentismTrend(id){
    let url = this.baseurl + '/CountAbsentismTrend/'+id;
    return this.http.get(url).map(x => x.json());
  }
  
  getLateSittingEmpList(id){
    let url = this.baseurl + '/LateSittingEmpList/'+id;
    return this.http.get(url).map(x => x.json());
  }

  getoverTimeEmpList(id){
    let url = this.baseurl + '/overTimwEmpList/'+id;
    return this.http.get(url).map(x => x.json());
  }

  getManHoursCountByDepartmentId(id){
    let url = this.baseurl + '/deparmentwiseManHours/'+id;
    return this.http.get(url).map(x => x.json());
  }

  //Service to get Summary of Employee and contractor
  summaryOfEmpAndContractor(){
    let loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    let locationid = loggedUser.locationId;
    let url = this.baseurl + '/summaryOfEmpAndContractor/'+locationid;
    return this.http.get(url).map(x => x.json());
  }
  getsummaryOfEmpAndContractor(url: string): Observable<any>
  {
        return this.httpService.get(`${this.baseurl + url}`);
    }
  getLocationWithCount(url: string): Observable<any>
  {
       return this.httpService.get(`${this.baseurl + url}`);
   }
   getLocationWithCounts(url: string): Observable<any>
   {
         return this.httpService.get(`${this.baseurl + url}`);
     }
    getAbsentTodaysEmp(url: string): Observable<any>
    {
          return this.httpService.get(`${this.baseurl + url}`);
      }
  getTodaysLateMarkEmp(url: string): Observable<any>
      {
            return this.httpService.get(`${this.baseurl + url}`);
        }
        getallLocations(url:string,id):Observable<any>{
          return this.httpService.get(this.baseurl+url+id)
        }
  }



