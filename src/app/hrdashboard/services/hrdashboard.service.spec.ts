import { TestBed, inject } from '@angular/core/testing';

import { HrdashboardService } from './hrdashboard.service';

describe('HrdashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HrdashboardService]
    });
  });

  it('should be created', inject([HrdashboardService], (service: HrdashboardService) => {
    expect(service).toBeTruthy();
  }));
});
